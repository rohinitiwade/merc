<?php include("header.php"); ?>
<link rel="stylesheet" type="text/css" href="styles/jquery.datetimepicker.css">
<style type="text/css">
	.input-group{margin-bottom:0}.select-due-date{display:flex;align-items:center}.pagination{float:right}
</style>
<body>
	<?php if($_SESSION['account'] == 'Individual'){
		$conditionquery1   = "`user_id`='" . $_SESSION['user_id'] . "' AND `remove_status`=1";
	}
	?>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>
			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title"> <b>Create Todo </b></h5>
							</div>
							<div class="page-body">
								<div class="card form-group">
									<div class="card-block">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon1"><i class="feather icon-edit createi"></i></span>
												<textarea class="form-control create" id="desc" aria-label="With textarea" placeholder="Click here to write to-do list description"></textarea>
											</div>
										</div>
										<?php $expirydate = mysqli_query($connection, "SELECT `expiry_date_to_do` FROM `setting` WHERE `user_id`='".$_SESSION['user_id']."'");
										$selexpiry = mysqli_fetch_array($expirydate);?>
										<?php if($selexpiry['expiry_date_to_do']=='Expiry On'){?>
										<div class="col-sm-12 row select-due-date form-group" style="float: left;">
											<!-- <h6 class="col-sm-12 p-0">Please select due date</h6>	 -->						
											<!-- <div class="col-sm-3 p-0">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon1"><i class="feather icon-calendar createi"></i></span>
													<input type="text" class="form-control" id="datepicker" name="startdate" aria-describedby="basic-addon2" value="">
												</div>
											</div>

											<div class="col-sm-1 p-0">
												<span style="text-align: center;"><h6><b>to</b></h6></span>
											</div>
											<div class="col-sm-3 p-0">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon1"><i class="feather icon-calendar createi"></i></span>
													<input type="text" class="form-control" id="datepicker1" name="enddate"  aria-describedby="basic-addon2" value="">
												</div>
											</div> -->
											<div class="col-sm-1 p-0">
												<h6><b>Expiry Date</b></h6>
											</div>
											<div class="col-sm-3 p-0">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon1"><i class="feather icon-calendar createi"></i></span>
													<input type="text" class="form-control" id="datepickerdata" aria-describedby="basic-addon2" name="datepickerdata" value="" placeholder="Enter Expiry Date Here">
												</div>
											</div>
											<?php $privatemark = mysqli_query($connection, "SELECT `private_to_do` FROM `setting` WHERE `user_id`='".$_SESSION['user_id']."'");
										$selpriate = mysqli_fetch_array($privatemark);?>
										<?php if($selpriate['private_to_do']=='Private On'){?>
										<div class=" form-group p-3">
											<div class="checkbox-zoom zoom-primary" style="padding-top: 20px;">
												<label>
													<input type="checkbox" value="private" id="private" class="private">
													<span class="cr">
														<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
													</span>
													<span>Mark As Private</span>
												</label>
											</div>
										</div>
                                        <?php } ?>
										</div>
										<?php }else{ ?>
                                         <input type="hidden" class="form-control" id="datepickerdata" aria-describedby="basic-addon2" name="datepickerdata" value="<?php echo date("Y-m-d");?>" placeholder="Enter Expiry Date Here">
										<?php } ?>
										
										<div class="col-sm-6 row select-due-date" style="float: left;">
											
										</div>
										<div class="row col-sm-12">
											<div class="col-sm-4 p-0">
												<div class="form-group">
													<label for="usr" class="col-form-label"><b>Relate To</b></label><br>
													<select id="relate" name="framework[]" multiple="multiple"  class="form-control" placeholder="Please Select">
														<option value="">Please Select</option>
													</select>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group framework-div">
													<label for="usr" class="col-form-label"><b>Assign To</b></label><br>
													<select id="framework" name="framework[]" multiple class="form-control" >

													</select> 
												</div>
											</div>
											<!--  -->
											<!-- <div class="col-sm-4 p-0">
												<?php if($_SESSION['account'] == 'Organization'){?>
													<div class="form-group framework-div">
														<label for="usr" class="col-form-label"><b>Assign To Advocates</b></label><br>
														<select id="advocate" name="framework[]" multiple class="form-control" >
														</select> 
													</div>
												<?php } ?>
											</div> -->
											<!--  -->
											<div class="todo-btn" style="padding-top: 31px;"> 
												<button class="btn button_submit btn-sm btn-primary" type="button"  onclick="todo()">Submit</button>
											</div> 
										</div>
										<hr/>
										<style>
											.list_active{
												background-color: #0028ff57 !important;
											}
										</style>
										<div class="todo_content">
											<div class="todo_nav">
												<nav class="navbar navbar-expand-lg">
													<div class="collapse navbar-collapse" id="navb">
														<ul class="navbar-nav mr-auto" id="action-span">
															<li class="nav-item">
																<div class="nav-link <?php if($_GET['name']=='All'){ echo 'list_active'; }?>">
																	<a href="create-todo.php?name=All">
																		<b>To Do's</b> 
																		<span class="main_back ">
																			<?php  $all = mysqli_query($connection, "SELECT  COUNT(todo_id) As total FROM `todo_list` WHERE ".$samequery." AND `todo_status`!='Completed'"); 
																			$count = mysqli_fetch_array($all); ?>
																			<b style="color: black !important;">(<?php echo $count['total'];?>)</b>
																		</span>
																	</a>
																</div>
															</li>
															<!-- <li class="nav-item">
																<div class="nav-link <?php if($_GET['name']=='pending'){ echo 'list_active'; }?>">
																	<a href="create-todo.php?name=pending">
																		<b>Pending</b>
																		<span class="main_pending ">
																			<?php $Pending = mysqli_query($connection, "SELECT COUNT(todo_id) AS Totals FROM `todo_list` WHERE ".$conditionquery1." AND `send_status`!='success'");  
																			$selPending = mysqli_fetch_array($Pending); ?>
																			<b style="color: black !important;">(<?php echo $selPending['Totals'];?>)</b>
																		</span>
																	</a>
																</div>
															</li>  -->
															<!-- <li class="nav-item">
																<div class="nav-link <?php if($_GET['name']=='Upcomming'){ echo 'list_active'; }?>">
																	<a href="create-todo.php?name=Upcomming">
																		<b>Upcoming</b> 
																		<span class="main_upcomming "><?php $Upcoming = mysqli_query($connection, "SELECT COUNT(todo_id) AS Totalss FROM ".$conditionquery1." AND `to_date`>'".date("Y-m-d H:i:s")."'");
																		$Upcoming1 = mysqli_fetch_array($Upcoming); ?>
																		<b style="color: black !important;">(<?php echo $Upcoming1['Totalss'];?>)</b>
																	</span>
																</a>
															</div>
														</li> -->
														<li class="nav-item">
															<div class="nav-link <?php if($_GET['name']=='Completed'){ echo 'list_active'; }?>">
																<a href="create-todo.php?name=Completed"><b>Completed</b>
																	<span class="main_completed list"><?php $complete = mysqli_query($connection, "SELECT * FROM `todo_list` WHERE ".$samequery." AND `todo_status`='Completed'"); $complete1 = mysqli_num_rows($complete); ?><b style="color: black !important;">(<?php echo $complete1;?>)</b>
																	</span></a></div>
																</li>
															</ul>
															<?php if(isset($_POST['toAll'])){ 
																extract($_POST);
															 $sql =  mysqli_query($connection, "SELECT * FROM `todo_team` WHERE `assign_to`='".$toAll."'");
															} ?>
															<form class="form-inline my-2 my-lg-0" method="POST">
																<select  class="form-control" id="" name="toAll" onchange='this.form.submit()'>
																	<option value="<?php echo $_POST['toAll'];?>">--Select To-Do--</option>
																	
																	<option value="<?php echo $_SESSION['user_id'];?>" selected> My to-dos  </option>
																</select>&nbsp;&nbsp;
																<div class="input-group">

																	<input class="form-control search_input" type="text" placeholder="Search" name="">
																	<span class="input-group-addon" id="basic-addon1" onclick="this.form.submit()"><i class="feather icon-search createi"></i>Search</span>
																</div>
															</form>
														</div>
													</nav>
												</div>
											</div>
											<form name="Myform" method="POST" action="deleteall.php">
												<div class="table-responsive">
												<table  class="table table-striped table-bordered" border="2">
													<tbody>
														<tr>
															<th style="background-color: #5AD9E9;">Sr.No.</th>
															<th style="background-color: #5AD9E9;width: 25%;">Description</th>
															<th style="background-color: #5AD9E9;width: 34%;">Case</th>
															<th style="background-color: #5AD9E9;">Expiry Date</th>
															<th style="background-color: #5AD9E9;">Assigned To</th>
															<!-- <th style="background-color: #5AD9E9;">Assigned to Advocates</th> -->
															<th style="background-color: #5AD9E9;">Action</th>
														</tr>
														<?php 
												if (isset($_GET['page_no']) && $_GET['page_no']!="") {
						                              $page_no = $_GET['page_no'];
						                            } else {
						                              $page_no = 1;
						                            }
						                            $total_records_per_page = 5;
						                            $offset = ($page_no-1) * $total_records_per_page;
						                            $previous_page = $page_no - 1;
						                            $next_page = $page_no + 1;
						                            $adjacents = "2"; 
						                            $result_count = mysqli_query($connection,"SELECT COUNT(case_id) As total_records FROM `todo_list` ");
						                            $total_records = mysqli_fetch_array($result_count);
						                            $total_records = $total_records['total_records'];
						                            $total_no_of_pages = ceil($total_records / $total_records_per_page);
						                            $second_last = $total_no_of_pages - 1;
						                             if($_SESSION['user_id']!='77'){
														if($_GET['name']=="Completed"){
															$list =  mysqli_query($connection,"SELECT * FROM `todo_list` WHERE ".$samequery." AND `todo_status`='Completed' AND `user_id`!='77' ORDER BY todo_id DESC LIMIT  $offset, $total_records_per_page");
														}elseif($_POST['toAll']!=""){
															$list =  mysqli_query($connection,"SELECT * FROM `todo_list` WHERE `user_id`='".$toAll."' AND `user_id`!='77' ORDER BY todo_id DESC LIMIT  $offset, $total_records_per_page");
														}else{
															$list =  mysqli_query($connection,"SELECT * FROM `todo_list` WHERE ".$samequery." AND `todo_status`!='Completed'  AND `user_id`!='77' ORDER BY todo_id DESC LIMIT  $offset, $total_records_per_page");
														}
													}else{
														if($_GET['name']=="Completed"){
															$list =  mysqli_query($connection,"SELECT * FROM `todo_list` WHERE ".$samequery." AND `todo_status`='Completed'  AND `user_id`='77' ORDER BY todo_id DESC LIMIT  $offset, $total_records_per_page");
														}elseif($_POST['toAll']!=""){
															$list =  mysqli_query($connection,"SELECT * FROM `todo_list` WHERE `user_id`='".$toAll."' AND `user_id`='77' ORDER BY todo_id DESC LIMIT  $offset, $total_records_per_page");
														}else{
															$list =  mysqli_query($connection,"SELECT * FROM `todo_list` WHERE ".$samequery." AND `todo_status`='Running' AND `user_id`='77' ORDER BY todo_id DESC LIMIT  $offset, $total_records_per_page");
														}
													}
														$counts = mysqli_num_rows($list);
														$i=1+$offset;
														while($assigns = mysqli_fetch_array($list)){
															$sql1 = mysqli_query($connection, "SELECT DISTINCT `case_id` FROM `todo_reminder` WHERE `todo_id`='".$assigns['todo_id']."' AND `division`='".$_SESSION['cityName']."' LIMIT  $offset, $total_records_per_page");
															$rows = mysqli_fetch_array($sql1);
															if($counts!=0){
																?>
																<tr <?php if($_GET['name']=='Completed'){ echo "class='main_completed'"; }elseif($_GET['name']=='Upcomming'){ echo "class='main_upcomming'";}else{ echo "class='main_back'";} ?> id="todo_tr<?php echo $i;?>">
																	<td><p><?php echo $i;?>)</p></td>
																	<td>
																		<div class="checkbox-zoom zoom-primary">
																			<label>
																				<input type="checkbox" value="<?php echo base64_encode($assigns['todo_id']);?>" id="private" class="todo-list-checkbox" name="checked_id[]" onclick="markCompleted(<?php echo $i;?>,<?php echo $assigns['todo_id'];?>)">
																				<span class="cr">
																					<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																				</span>
																				<span><b><?php echo $assigns['content'];?></b></span>
																			</label>
																		</div> 
																		<?php if($assigns['private']!=''){?><p class="m-b-0"><i><b>Visibility:</b><?php echo ucfirst($assigns['private']);?></i> </p><?php } ?>
																		</td>
																		<td>
																			<?php $todocases = mysqli_query($connection, "SELECT DISTINCT `relate_to`,`case_id` FROM `todo_reminder` WHERE `todo_id`='".$assigns['todo_id']."'");
																			while($seltodocases = mysqli_fetch_array($todocases)){
																				$todocasess = mysqli_query($connection, "SELECT `case_type`,`case_no`,`case_no_year`,`case_title`,`supreme_court`,`diary_no`,`diary_year` FROM `reg_cases` WHERE `case_id`='".$seltodocases['case_id']."'");
																				 $seltodocasess = mysqli_fetch_array($todocasess);?>
																				<?php if($seltodocasess['supreme_court'] == 'Diary Number'){echo $seltodocasess['diary_no'].' / '.$seltodocasess['diary_year'].' '.$seltodocasess['case_title'];}else{ echo $seltodocasess['case_type']; echo " "; echo $seltodocasess['case_no']; echo " / "; echo $seltodocasess['case_no_year']; echo " "; echo $seltodocasess['case_title']."<hr/>";}}?>
																		</td>
																		<td><?php echo date("jS M, Y", strtotime($assigns['expiry_date'])); ?>
																			</td>
																			<td><p class="assign m-b-0"><i><?php
																			$teams = mysqli_query($connection, "SELECT DISTINCT `assign_to` FROM `todo_team` WHERE `todo_id`='".$assigns['todo_id']."'");
																			while($teams_fetch = mysqli_fetch_array($teams)){
																				$teamss =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$teams_fetch['assign_to']."' ");
																				$teams_fetchs = mysqli_fetch_array($teamss);?><?php echo $teams_fetchs['name'];?> <?php echo $teams_fetchs['last_name']; echo ", "; 
																			}?></i>
																		</p></td>
																		<!-- <td>	<p class="assign m-b-0"><i><b> Assigned to Advocates: </b>
																			<?php    $advocate = mysqli_query($connection, "SELECT DISTINCT `advocate_id` FROM `assign_toadvocates` WHERE `todo_id`='".$assigns['todo_id']."'");
																			while($seladvocate = mysqli_fetch_array($advocate)){
																				$teamss1 =mysqli_query($connection,"SELECT * FROM `advocate` WHERE `id`='".$seladvocate['advocate_id']."' ");
																				$teams_fetchs1 = mysqli_fetch_array($teamss1);?><?php echo ucfirst($teams_fetchs1['full_name']); echo ", ";}?></i></p></td> -->

																				<td><a href="edit_todo.php?id=<?php echo base64_encode($assigns['todo_id']);?>" class="btn btn-info btn-mini form-group"><i class="feather icon-edit createtodolist" cursor="pointer" title="Edit" ></i></a>

																					<a href="delete_todo.php?id=<?php echo base64_encode($assigns['todo_id']);?>" class="btn btn-danger btn-mini form-group"><i class="feather icon-trash createtodolist" cursor="pointer" title="Delete" onclick="return confirm('Are you sure you want to Delete?');"></i></a>
																					<?php if($_GET['name']=="Completed"){?>
																					<a href="undo-todo.php?id=<?php echo base64_encode($assigns['todo_id']);?>" class="btn btn-primary btn-mini"><i class="fa fa-refresh createtodolist" cursor="pointer" title="Undo" onclick="return confirm('Are you sure you want to Undo?');"></i></a>
																					<?php } ?>

																					<!-- <button class="btn btn-primary btn-mini" type="button" onclick="markToDoDone()">Todo Done</button> -->
																				</td>

																			</tr>
																			<?php $i++; } ?>
																		<?php } ?>
																		<!-- <tr>
																			<td colspan="7"><b>No Reminder Added</b></td>
																		</tr> -->
																	</tbody>
																	<tfoot>
																		<tr>
																			<!-- <td colspan="6">
																				<?php if($counts!=0){?>
																					<div class="checkbox-zoom zoom-primary">
																						<label>
																							<input type="checkbox" name="" class="selectall_checkbox">
																							<span class="cr">
																								<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																							</span>
																							<span><b>Select/Unselect All</span>
																							</label>
																						</div>

																						<button class="btn btn-primary btn-sm" type="button" onclick="markCompleted()" id="selectall_complete">Mark Completed</button>
																						<button type="submit" onclick="return confirm('Are you sure you want to Delete All Reminders?');" class="btn-danger btn btn-sm" type="button">Delete</button>
																					<?php } ?>
																				</td> -->
																				<td colspan="7">
																					<ul class="pagination">
														                                 <?php if ($page_no > 1){
														                                  echo "<li><a href='?page_no=1&&name=".$_REQUEST['name']."'>First Page</a></li>";} ?>
														                                  <li <?php if ($page_no <= 1){
														                                    echo "class='disabled'";} ?>>
														                                    <a <?php if ($page_no > 1){
														                                      echo "href='?page_no=$previous_page&&name=".$_REQUEST['name']."'";} ?>>Previous</a>
														                                    </li>
														                                    <?php
														                                    if ($total_no_of_pages <= 10){
														                                      for ($counter = 1;$counter <= $total_no_of_pages;$counter++)
														                                      {
														                                        if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>"; }
														                                        else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&name=".$_REQUEST['name']."'>$counter</a></li>";}
														                                      }
														                                    }elseif ($total_no_of_pages > 10){
														                                      if ($page_no <= 4){
														                                        for ($counter = 1;$counter < 8;$counter++){
														                                          if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>";
														                                        }else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&name=".$_REQUEST['name']."'>$counter</a></li>";}
														                                      }
														                                      echo "<li><a>...</a></li>";
														                                      echo "<li><a href='?page_no=$second_last&&name=".$_REQUEST['name']."'>$second_last</a></li>";
														                                      echo "<li><a href='?page_no=$total_no_of_pages&&page=$total_no_of_pages&&name=".$_REQUEST['name']."'>$total_no_of_pages</a></li>";
														                                    }elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4){
														                                      echo "<li><a href='?page_no=1&&name=".$_REQUEST['name']."'>1</a></li>";
														                                      echo "<li><a href='?page_no=2&&name=".$_REQUEST['name']."'>2</a></li>";
														                                      echo "<li><a>...</a></li>";
														                                      for ($counter = $page_no - $adjacents;$counter <= $page_no + $adjacents;$counter++)
														                                      {
														                                        if ($counter == $page_no){
														                                          echo "<li class='active'><a>$counter</a></li>";
														                                        }else{
														                                          echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&name=".$_REQUEST['name']."'>$counter</a></li>";
														                                        }
														                                      }
														                                      echo "<li><a>...</a></li>";
														                                      echo "<li><a href='?page_no=$second_last&&name=".$_REQUEST['name']."'>$second_last</a></li>";
														                                      echo "<li><a href='?page_no=$total_no_of_pages&&name=".$_REQUEST['name']."'>$total_no_of_pages</a></li>";
														                                    }else{
														                                      echo "<li><a href='?page_no=1&&name=".$_REQUEST['name']."'>1</a></li>";
														                                      echo "<li><a href='?page_no=2&&name=".$_REQUEST['name']."'>2</a></li>";
														                                      echo "<li><a>...</a></li>";
														                                      for ($counter = $total_no_of_pages - 6;$counter <= $total_no_of_pages;$counter++)
														                                      {
														                                        if ($counter == $page_no){
														                                          echo "<li class='active'><a>$counter</a></li>";
														                                        }else{
														                                          echo "<li><a href='?page_no=$counter&&name=".$_REQUEST['name']."'>$counter</a></li>";
														                                        }
														                                      }
														                                    }
														                                  }
														                                  ?>
														                                  <li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
														                                    <a <?php if($page_no < $total_no_of_pages) { echo "href='?page_no=$next_page&&name=".$_REQUEST['name']."'"; } ?>>Next</a>
														                                  </li>
														                                  <?php if($page_no < $total_no_of_pages){
														                                    echo "<li><a href='?page_no=$total_no_of_pages&&name=".$_REQUEST['name']."'>Last &rsaquo;&rsaquo;</a></li>";
														                                  } ?>
														                                </ul>
																					</td>
																				</tr>
																			</tfoot>
																		</table>
																	</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<?php include 'footer.php'; ?>

								<script type="text/javascript" src="scripts/jquery.datetimepicker.full.min.js"></script>
								<script src='scripts/todo_list.js' type='text/javascript'></script>
								<script src='scripts/create_todo.js' type='text/javascript'></script>
								<script>
									$( function() {
										$( "#datepickerdata" ).datepicker({
											minDate: 0,
											dateFormat: 'yy-mm-dd' 
										});

									} );
								</script>
