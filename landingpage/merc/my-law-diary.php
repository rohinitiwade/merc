<?php include("header.php") ?>
<?php include("phpfile/sql_home.php") ?>
<!-- <link rel="stylesheet" href="css/vertical-layout-light/add_cases.css"> -->
<body>

	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<?php if(isset($_POST['submit'])){
									extract($_POST);
									$fromdate = $_POST['form_date'];
									$todate = $_POST['to_date'];
								}?>
								<?php 
								if($fromdate!="" && $todate!=""){
									$court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE ".$dailycases_query." AND `hearing_date` BETWEEN '".$fromdate."' AND  '".$todate."'  ORDER BY `hearing_date`  ASC");
								}else{
									$court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE ".$dailycases_query." AND  `hearing_date`='".date("Y-m-d")."' ORDER BY `hearing_date` ASC");
								}
								?>
								<h5 class="card-title">
									Daily Cases (
										<?php echo $counts = mysqli_num_rows($court); 
										?>
										)
									</h5>
								</div>
								<div class="page-body">
									<div class="form-group">
										<div class="card col-sm-12">
											<div class="card-block form-group row">
												<div class="col-sm-12 form-group">
													<form name="Myform" method="POST">
														<div class="col-sm-12 form-group row">
															<div class="col-sm-4 col-lg-4 col-md-12 col-xs-12 form-group row">
																<label for="from_datepicker"
																class="col-sm-4 col-lg-4 col-md-12 col-xs-12 form-group row lawlabel col-form-label" > <b>From</b></label>
																<div class="col-sm-8">
																	<input type="text" id="datepicker" name="form_date"
																	class="form-control" value="<?php echo $_POST['form_date'];?>" placeholder="From Date"
																	autocomplete="OFF">
																</div>
															</div>
															<div class="col-sm-4 col-lg-4 col-md-12 col-xs-12 form-group row">
																<label for="to_datepicker"
																class="col-sm-4 col-lg-4 col-md-12 col-xs-12 form-group row lawlabel col-form-label"> <b>To</b></label>
																<div class="col-sm-8">
																	<input type="text" id="datepicker1" value="<?php echo $_POST['to_date'];?>" name="to_date"
																	class="form-control" placeholder="To Date"
																	autocomplete="OFF">
																</div>
															</div>
															<div class="col-sm-2 col-lg-2 col-md-12  col-xs-12 form-group text-center">
																<button type="submit" id="submit_date"
																class="btn btn-primary btn-sm" name="submit">Submit</button>
															</div>
															<div class="col-sm-4 col-md-12 col-xs-12" style="text-align: right">
																<!-- <div class="col-sm-4"> -->
																	
																	<!-- </div> -->
																	<!-- <div class="col-sm-4"> -->
																		<button class="btn btn-sm btn-info" id="myBtn"
																		data-target="#filter" type="button" data-toggle="modal" style="">Filter</button>
																		<!-- </div> -->
																		<!-- <div class="col-sm-4"> -->
																			<button class="btn btn-sm btn-warning" id="myBtn"
																			data-target="#export" type="button" data-toggle="modal" style="">Export</button>
																			<!-- </div> -->
																		</div>
																	</div>
																</form>
															</div>
															<div class="table-responsive col-sm-12">
																<input type="hidden" name="" id="caseid">
																<table id="example" class="table table-bordered">
																	<thead class="bg-primary">
																		<tr>
																			<tr>
																				<th>Sr.No.</th>
																				<th>Court</th>
																				<th>Case No / Year</th>
																				<th>Title</th>
																				<th>Team Member(s)</th>
																				<th>Hearing Date</th>
																				<!-- <th>Last Hearing Date</th> -->
																				<!-- <th>Stage</th> -->
																				<!-- <th>Case Description</th> -->
																				<!-- <th>CNR</th> -->
																			</tr>
																		</tr>
																	</thead>
																	<tbody>
																		<?php 
																		
																		$i=1;
																		while($selcourt = mysqli_fetch_array($court)){
																			if($selcourt['supreme_court'] == 'Diary Number')
																				$case= $selcourt['diary_no'].'/ '.$selcourt['diary_year'];
																			else
																				$case= $selcourt['case_type'].' '.$selcourt['case_no'].'/ '.$selcourt['case_no_year'];
																			$empQuery_join = mysqli_query($connection,"select * from todo_team tt WHERE tt.case_id = '".$selcourt['case_id']."' AND tt.flag='add'");
																			$team_member = array();
																			while($empquery = mysqli_fetch_array($empQuery_join)){
																				$law_reg = mysqli_query($connection,"select * from law_registration  WHERE reg_id = '".$empquery['assign_to']."' ");
																				$law_fetch = mysqli_fetch_array($law_reg);
									      							// $empRecords_join = mysqli_query($connection, $empQuery_join);


																				$team_member[] = $law_fetch['name'].' '.$law_fetch['last_name'];		
																			}		
																			$team_member_list = implode(', ', $team_member);
																			// echo $selcourt['court_name']
																			?>


																			<tr style="cursor: pointer;"
																			style="background-color: red;" class="rowclickable"
																			data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>">
																			<td>
																				<?php echo $i; ?>
																			</td>
																			<td>
																				<?php if($selcourt['court_id']==1){
																					echo $selcourt['court_name'].' -'. $selcourt['supreme_court'];}
																					elseif($selcourt['court_id']==2){
																						echo $selcourt['court_name'].' -'. $selcourt['high_court'].'- '. $selcourt['bench'].'- '. $selcourt['side'].'- '. $selcourt['hc_stamp_register'];}
																						elseif($selcourt['court_id']==3){
																							echo $selcourt['court_name'].' -'. $selcourt['state'].'- '. $selcourt['district'].'- '. $selcourt['court_establishment'];}else{
																								echo $selcourt['court_name'];
																							}?>
																						</td>
																						<td>
																							<?php echo $case;?> 
																						</td>
																						<td>
																							<?php echo $selcourt['case_title'];?>
																						</td>
																						<td>
																							<?php echo $selcourt['team_member']; echo $team_member_list;?>
																						</td>
																						<td>
																							<?php if($selcourt['hearing_date']!=''){echo date('F d,Y',strtotime($selcourt['hearing_date']));}else{echo " ";}?>
																						</td>
																						<!-- <td><?php echo $selcourt['last_hearing_date'];?></td> -->
																						<!-- <td>
																							<?php echo $selcourt['Stage'];?>
																						</td> -->
																					<!-- 	<td>
																							<?php echo $selcourt['case_description'];?>
																						</td> -->
																						<!-- <td>
																							<?php echo $selcourt['CNR'];?>
																						</td> -->

																					</tr>

																					<?php $i++; } ?>
																				</tbody>
																				<tfoot>
																				</table>

																			</div>

																			<div class="modal" id="filter">
																				<div class="modal-dialog modal-lg">
																					<div class="modal-content" style="">
																						<div class="modal-header" style="">
																							<h4>Filter</h4>
																							<button type="button" class="close" data-dismiss="modal">&times;</button>
																						</div>
																						<div class="modal-body">
																							<div class="row">
																								<div class="col-sm-4">
																									<label>Title</label><br> <input type="text"
																									class="form-control" placeholder="Enter Title">
																									<label>Case#</label><br> <input type="text"
																									class="form-control" placeholder="Enter Case">
																									<label>Case Year</label><br> <input type="text"
																									class="form-control"
																									placeholder="put case year dropdown"> <label>Before
																									Hon'ble Judge/s</label><br> <input type="text"
																									class="form-control" placeholder=""> <label>Section/Category</label><br>
																									<input type="text" class="form-control"
																									placeholder="Enter Section/Category"> <label>Posted
																									For</label><br> <input type="text" class="form-control"
																									placeholder=""> <label>Opponents</label><br>
																									<input type="text" class="form-control"
																									placeholder="put opponent dropdown">
																								</div>
																								<div class="col-sm-4">
																									<label>Advocate</label><br> <input type="text"
																									class="form-control" placeholder="Enter Advocate Name">
																									<label>Hearing Date</label><br> <input type="text"
																									class="form-control" id="datepicker2"
																									placeholder="Choose Date"> <label>Case
																									Filing Date Form</label><br> <input type="text"
																									class="form-control" id="datepicker3"
																									placeholder="Choose Date"> <label>Referred
																									By</label><br> <input type="text" class="form-control"
																									placeholder="Enter Referred By"> <label>Priority</label><br>
																									<input type="text" class="form-control"
																									placeholder="Put dropdown"> <label>Action
																									Taken</label><br> <input type="text" class="form-control"
																									placeholder=""> <label>CNR#</label><br> <input
																									type="text" class="form-control"
																									placeholder="Enter CNR#">
																								</div>
																								<div class="col-sm-4">
																									<label>Team Member</label><br> <input type="text"
																									class="form-control" placeholder="Enter Team Member">
																									<label>Hearing Date To</label><br> <input
																									type="text" class="form-control" id="datepicker4"
																									placeholder="Choose Date"> <label>Case
																									Filing Date To</label><br> <input type="text"
																									class="form-control" id="datepicker5"
																									placeholder="Choose Date"> <label>File#</label><br>
																									<input type="text" class="form-control"
																									placeholder="Enter File#"> <label>Stage</label><br>
																									<input type="text" class="form-control"
																									placeholder="Enter Stage"> <label>Sessions</label><br>
																									<input type="text" class="form-control"
																									placeholder="Put dropdown"> <label>Court</label><br>
																									<input type="text" class="form-control"
																									placeholder="Put dropdown">
																								</div>
																							</div>
																						</div>
																						<div class="modal-footer">
																							<button class="btn btn-warning btn-sm" type="button"
																							style="">Reset</button>
																							<button class="btn btn-primary btn-sm" type="button"
																							style="">Submit</button>
																							<button class="btn btn-primary btn-sm" type="button"
																							data-dismiss="modal" style="">Close</button>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="modal" id="export">
																				<div class="modal-dialog modal-lg">
																					<div class="modal-content">
																						<div class="modal-header">
																							<h4>Export</h4>
																							<button type="button" class="close" data-dismiss="modal"
																							style="color: white;">&times;</button>
																						</div>
																						<div class="modal-body">

																							<div class="col-sm-12 row">
																								<label class="col-sm-2 p-0">Download as:</label>
																								<div class="form-radio col-sm-3">
																									<div class="radio radio-primary radio-inline">
																										<label>
																											<input type="radio" name="downloadas" value="pdf" checked>

																											<i class="helper"></i>PDF
																										</label>
																									</div>
																									<div class="radio radio-primary radio-inline">
																										<label>
																											<input type="radio" name="downloadas" value="excel">

																											<i class="helper"></i>Excel
																										</label>
																									</div>
																									<div id="document-table-div-export"></div>
																								</div>

																								<div class="col-sm-4">
																									<button type="button" type="button" class="btn btn-info btn-sm" onclick="download_doc()">Download</button>
																								</div>
																							</div>
																						</div>
																						<div class="modal-footer">
																							<button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php include 'footer.php'; ?>

								<script type="text/javascript">
									$(".add-client-li").addClass('active pcoded-trigger');
								</script>
								<script type='text/javascript' src='scripts/jspdf.min.js'></script>
								<script type='text/javascript'
								src="scripts/jspdf.plugin.autotable.min.js"></script>
								<script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
								<script type="text/javascript" src="scripts/all_export.js"></script>
								<script type="text/javascript" src="scripts/my-law-diary.js"></script>