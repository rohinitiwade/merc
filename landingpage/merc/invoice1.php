<?php session_start(); include("includes/dbconnect.php");
 $_SESSION['razorpay_payment_id'];
 $_SESSION['user_id'];
  if($_SESSION['razorpay_payment_id']==""){ ?>
  <script type="text/javascript">
    window.location.href='failed';
  </script>
<?php }
  ?>
   <?php $reg = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
         $selreg = mysqli_fetch_array($reg);?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com//polygon/adminty/default/invoice.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Dec 2019 12:20:22 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<title>Maharashtra Litigation Management System</title>


<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="#">
<meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
<meta name="author" content="#">

<link rel="icon" href="https://colorlib.com//polygon/adminty/files/assets/images/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://colorlib.com//polygon/adminty/files/bower_components/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com//polygon/adminty/files/assets/icon/themify-icons/themify-icons.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com//polygon/adminty/files/assets/icon/icofont/css/icofont.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com//polygon/adminty/files/assets/icon/feather/css/feather.css">

<link rel="stylesheet" type="text/css" href="https://colorlib.com//polygon/adminty/files/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="https://colorlib.com//polygon/adminty/files/assets/css/jquery.mCustomScrollbar.css">
</head>
<body>
<div class="pcoded-content">
<div class="pcoded-inner-content">
<div class="main-body">
<div class="page-wrapper">
<div class="page-body">
<div class="container">
<div>
        <div class="row text-right">
<div class="col-sm-12 invoice-btn-group text-right">
<button type="button" class="btn btn-primary btn-print-invoice m-b-10 btn-sm waves-effect waves-light m-r-20" id="printInvoice">Print</button>

</div>
</div>
<div class="card invoice">

<div class="row invoice-contact">
<div class="col-md-8">
<div class="invoice-box row">
<div class="col-sm-12">
<table class="table table-responsive invoice-table table-borderless">
<tbody>
<tr>
<td><img src="https://colorlib.com//polygon/adminty/files/assets/images/logo-blue.png" class="m-b-10" alt=""></td>
</tr>
<tr>
<td><b>AIR INFINITY PVT. LTD.</b></td>
</tr>
<tr>
<td>84B, Pandey Layout,New Sneh Nagar</td>
</tr>
<tr>
<td>Nagpur - 440015. Nagpur. India</a>
</td>
</tr>
<tr>
<td>+91 919-91-91-919</td>
</tr>

 </tbody>
</table>
</div>
</div>
</div>
<div class="col-md-4">
</div>
</div>
<div class="card-block">
<div class="row invoive-info">
<div class="col-md-4 col-xs-12 invoice-client-info">
<h6>Client Information :</h6>
<h6 class="m-0"><?php echo ucfirst($selreg['name']);?> <?php echo ucfirst($selreg['last_name']);?></h6>
<p class="m-0 m-t-10">796 Silver Harbour, TX 79273, US</p>
<p class="m-0">(1234) - 567891</p>
<p><a href="mailto:john@example.com">john@example.com</a></p>
</div>
<div class="col-md-4 col-sm-6">
<h6>Order Information :</h6>
<table class="table table-responsive invoice-table invoice-order table-borderless">
<tbody>
<tr>
<th>Date :</th>
<td><?php echo date("Y/m/d")?></td>
</tr>
<tr>
<th>Status :</th>
<td>
<span class="label label-warning">Pending</span>
</td>
</tr>
<tr>
<th>Id :</th>
<td>
#145698
</td>
</tr>
</tbody>
</table>
</div>
<div class="col-md-4 col-sm-6">
<h6 class="m-b-20">Invoice Number <span>#12398521473</span></h6>
<h6 class="text-uppercase text-primary">Total Due :
<span>$900.00</span>
</h6>
</div>
</div>
 <div class="row">
<div class="col-sm-12">
<div class="table-responsive">
<table class="table  invoice-detail-table">
<thead>
<tr class="thead-default">
<th>Description</th>
<th>Quantity</th>
<th>Amount</th>
<th>Total</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<h6>Logo Design</h6>
<p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
</td>
<td>6</td>
<td>$200.00</td>
<td>$1200.00</td>
</tr>
<!-- <tr>
<td>
<h6>Logo Design</h6>
<p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
</td>
<td>7</td>
<td>$100.00</td>
<td>$700.00</td>
</tr>
<tr>
<td>
<h6>Logo Design</h6>
<p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt </p>
</td>
<td>5</td>
<td>$150.00</td>
<td>$750.00</td>
</tr> -->
</tbody>
</table>
 </div>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<table class="table table-responsive invoice-table invoice-total">
<tbody>
<tr>
<th>Sub Total :</th>
<td>$4725.00</td>
</tr>
<tr>
<th>Taxes (10%) :</th>
<td>$57.00</td>
</tr>
<tr>
<th>Discount (5%) :</th>
<td>$45.00</td>
</tr>
<tr class="text-info">
<td>
<hr />
<h5 class="text-primary">Total :</h5>
</td>
<td>
<hr />
<h5 class="text-primary">$4827.00</h5>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<h6>Terms And Condition :</h6>
<p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
</div>
</div>
</div>
</div>


</div>
</div>

</div>

</div>
</div>

<div id="styleSelector">
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="../files/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="../files/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="../files/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="../files/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="../files/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->

<script>
     $('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data) 
            {
                window.print();
                return true;
            }
        });
    </script> 
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="f425266ff53050dfc10c573d-|49" defer=""></script></body>

<!-- Mirrored from colorlib.com//polygon/adminty/default/invoice.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 07 Dec 2019 12:20:22 GMT -->
</html>
