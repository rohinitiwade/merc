<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="css/vertical-layout-light/recent_case_activity.css"> 
      <?php include("menu.php");?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="card form-group">
            <div class="card-body">
              <h4 class="card-header" style="padding-top: 0px">Upcoming To-Do's
                <button class="btn btn-sm btn-secondary" data-target="#filter" data-toggle="modal">Filter</button>
              </h4>
              <div class="card">
                <article>
                  <span>November 08, 2019 13:08 By </span><a href="">Sandesh Narayan Bhandarkar</a><span> in </span><a href="">WP 6473 / 2011 - CR/2018/EST-14/ MR. BHAGVAN SITARAM KHANDAGALE -</a><br>
                  <b>Closed Date</b><span> August 25, 2011</span>
                </article>
              </div>
              
              <div class="card">
                <article>
                  <span>November 08, 2019 13:08 By </span><a href="">Sandesh Narayan Bhandarkar</a><span> in </span><a href="">WP 6473 / 2011 - CR/2018/EST-14/ MR. BHAGVAN SITARAM KHANDAGALE -</a><br>
                  <b>Closed Date</b><span> August 25, 2011</span>
                </article>
              </div>
              
              <div class="card">
                <article>
                  <span>November 08, 2019 13:08 By </span><a href="">Sandesh Narayan Bhandarkar</a><span> in </span><a href="">WP 6473 / 2011 - CR/2018/EST-14/ MR. BHAGVAN SITARAM KHANDAGALE -</a><br>
                  <b>Closed Date</b><span> August 25, 2011</span>
                </article>
              </div>
              
            </div>
          </div>
          <?php include("footer.php");?>
        </div>
        <div class="modal" id="filter">
          <div class="modal-dialog">
            <div class="modal-content" style="">
              <div class="modal-header" style="">
                <h4>Filter</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="row">
                    <div class="col-sm-4">
                      <label>From</label><br>
                      <input type="text" class="form-control" id="datepicker" placeholder="Choose Date">
                      <label>Cases</label><br>
                      <input type="text" class="form-control" placeholder="Put Dropdown">
                    </div>
                    <div class="col-sm-4">
                      <label>To</label><br>
                      <input type="text" class="form-control" id="datepicker1" placeholder="Choose Date">
                    </div>
                    <div class="col-sm-4">
                      <label>Activity By</label><br>
                      <input type="text" class="form-control" placeholder="Put Dropdown">
                    </div>
                  </div><br><br>
                  <div class="row ">
                    <button class="btn btn-sm btn-secondary" style="">Submit</button>
                    <input class="btn btn-sm btn-secondary" type="reset" name="Reset" style="">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
  $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
  });
  $(document).ready(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy/mm/dd',
      changeYear: true,
      changeMonth: true
    });
    $( "#datepicker1" ).datepicker({
      dateFormat: 'yy/mm/dd',
      changeYear: true,
      changeMonth: true
    });
  });
</script>