<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<link rel="stylesheet" href="styles/case-report.css">
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title">
									<b>Legal Research</b>
								</h5>
							</div>
							<div class="page-body">
								<div class="card form-group">
									<div class="card-block">
										<form class="forms-sample row">
											<div class="row">
          										<?php $caseno = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
          											$selcaseno = mysqli_fetch_array($caseno);
          											$getinfo = mysqli_query($connection,"SELECT `email`,`name`,`last_name`,`mobile` FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
          											$fetch_getinfo = mysqli_fetch_array($getinfo);?>
          									<div class="head_div"><?php echo $selcaseno['case_type']; echo " "; echo $selcaseno['case_no']; echo " "; echo $selcaseno['case_no_year'];?></div>
											<div class="col-sm-6 row form-group">
												<label class="col-sm-4 col-form-label">Subject</label>
												<div class="col-sm-8">
													<select name="matter_type" id="matter"  class="form-control" style="font-size: 14px; color: gray; border: 1px solid #babfc7;">
                  										<option value="">---- Select Subject ---</option>
                  											<?php $nameofmatter = mysqli_query($connection, "SELECT `nameofmatter` FROM `name_of_matter` WHERE `status`=1 ORDER BY nameofmatter ASC");
                  												while($nameofmatterss = mysqli_fetch_array($nameofmatter)){?>
                    									<option value="<?php echo $nameofmatterss['nameofmatter'];?>"><?php echo $nameofmatterss['nameofmatter'];?></option>
                  										<?php } ?>
                									</select>
												</div>
											</div>
											<div class="col-sm-6 row form-group">
												<label class="col-sm-4 col-form-label">Court Name</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" value="<?php echo $selcaseno['court_name'];?>" class="form-control" placeholder="Enter Case Nmber Here">
												</div>
											</div>
											<?php if($selcaseno['court_id'] == 1){?>
            								<div class="col-sm-6 form-group row">
               									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Diary Number:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="diary_no" id="diary_no" value="<?php echo $selcaseno['diary_no'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
              								</div>
              								<div class="col-sm-6 row form-group">
              									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Diary Year:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="diary_year" id="diary_year" value="<?php echo $selcaseno['diary_year'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
             								</div>
          									<?php } else{?>
											<div class="col-sm-6 row form-group">
												<label class="col-sm-4 col-form-label">Case No.</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" value="<?php echo $selcaseno['case_no'];?>" placeholder="Enter Case Nmber Here">
												</div>
											</div>
											<div class="col-sm-6 row form-group">
												<label class="col-sm-4 col-form-label">Case Year</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" value="<?php echo $selcaseno['case_no_year'];?>" placeholder="Enter Case Nmber Here">
												</div>
											</div>
											<?php } ?>
											<div class="col-sm-6 row form-group">
												<label class="col-sm-4 col-form-label">Case Type</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" value="<?php echo $selcaseno['case_type'];?>">
												</div>
											</div>
											<div class="col-sm-6 row form-group">
												<label class="col-sm-4 col-form-label">Department</label>
												<div class="col-sm-8">
													<select name="department" id="dept" class="form-control" style="font-size: 14px; color: gray; border: 1px solid #babfc7;">
                      									<option value="<?php echo $selcaseno['name_of_matter']; ?>"><?php if($selcaseno['name_of_matter']!=''){echo $selcaseno['name_of_matter'];}else{?>----- Select Department ----- <?php } ?>
                      									</option>
                      									<?php $document = mysqli_query($connection, "SELECT `department_name` FROM `department` WHERE `status`=0 ORDER BY department_name ASC");
                      									while($seldocment = mysqli_fetch_array($document)){?>
                        								<option value="<?php echo $seldocment['department_name'];?>"><?php echo $seldocment['department_name'];?>
                        									
                        								</option>
                      									<?php } ?>
                    								</select>
												</div>
											</div>
											<?php if($selcaseno['court_id'] == 2){?>
            								<div class="col-sm-6 form-group row">
              									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>High Court:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="high_court" id="high_court" value="<?php echo $selcaseno['high_court'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
              								</div>
              								<div class="col-sm-6 row form-group">
              									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Bench:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="bench" id="bench" value="<?php echo $selcaseno['bench'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
            								</div>
             								<div class="col-sm-6 form-group row">
              									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Side:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="side" id="side" value="<?php echo $selcaseno['side'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
               								</div>
               								<div class="col-sm-6 row form-group">
               									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Stamp/ Register:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="hc_stamp_register" id="hc_stamp_register" value="<?php echo $selcaseno['hc_stamp_register'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
            								</div>
          									<?php } ?>
           									<?php if($selcaseno['court_id'] == 3){?>
            								<div class="col-sm-6 form-group row">
              									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>State:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="state" id="state" value="<?php echo $selcaseno['state'];?>" class="form-control" placeholder="Enter Case Nmber Here"  >
              									</div>
              								<div class="col-sm-6 row form-group">
              									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>District:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="district" id="district" value="<?php echo $selcaseno['district'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
              								</div>
              								<div class="col-sm-6 row form-group">
              									<label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Court Establishment:</b></label>
              									<div class="col-sm-8">
                									<input type="text" name="court_establishment" id="court_establishment" value="<?php echo $selcaseno['court_establishment'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
              									</div>
            								</div>
          									<?php } ?>
											<div class="col-sm-6 row form-group">
												<label class="col-sm-4 col-form-label">Document</label>
												<div class="col-sm-8">
													<input type="file" name="">
													<button type="button" class="btn btn-sm btn-primary" onclick="addDocument()">
														<i class="fa fa-plus" aria-hidden="true"></i>
													</button>
													<div id="addedDocument"></div>
												</div>
											</div>
											<div class="form-group col-sm-12" id="" >
												<label for="exampleInputUsername1" class="col-form-label">
													<div class="form-radio col">
														<div class="radio radio-primary radio-inline">
															<label>
																<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('English')" value="English" checked>
																<i class="helper"></i>English
															</label>
														</div>
														<div class="radio radio-primary radio-inline">
															<label>
																<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('Marathi')" value="Marathi">			
																<i class="helper"></i>Marathi
															</label>
														</div>
													</div>
												</label>
												<textarea name="details" id="query" class="ckeditor"><?php echo $_POST['details'];?></textarea>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/legal_research.js"></script>




	