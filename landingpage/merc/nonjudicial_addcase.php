<?php session_start(); include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<link rel="stylesheet" href="css/vertical-layout-light/add_cases.css">
<style type="text/css">
.fetchdataloader {
	border: 16px solid #f3f3f3;
	border-radius: 50%;
	border-top: 16px solid #3498db;
	width: 60px;
	float: right;
	height: 60px;
	-webkit-animation: spin 2s linear infinite; /* Safari */
	animation: spin 2s linear infinite;
}
/*.select2-results__option[aria-selected="true"] {
	display: none;
	}*/
	#sc_info{
		color: red;
		width: 100%;
		float: left;
		/*text-align: right;*/
		/*position: absolute;*/
		/*z-index: 1;*/
		/*top: 54px;*/
		display: none;
	}
	.full-name{
		width: 55%;
	}
	.fetchdata-dialog{
		max-width:80%;
	}
	#submit_data{
		text-align: right;
	}
	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
</style>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title"> Non Judicial Add Case </h5>
							</div>

							<div class="page-body">
								<div class="card form-group">
								 
									
									<div class="card-block">
										<form class="forms-sample row" autocomplete="off">
			
	
								<div class="col-sm-6 row form-group" id="case-div">
									<label for="exampleInputUsername1" placeholder="Enter Client Name" class="col-sm-4 col-form-label" style="text-align: left;"> Client Name
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									<span style="color: red;">*</span></label>
									<div class="col-sm-8"><input type="text" name="case_no" id="client_name" value="" class="form-control" autocomplete="OFF" required ></div>
								</div>
								<div class="col-sm-6 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"> Type of Work </label>
									<div class="col-sm-8">
                              <textarea class="form-control" id="type_ofwork" placeholder="Enter Type of Work " name="description"></textarea>
								</div>
							</div>

<div class="form-group col-sm-6 row" >

	<div class="col-sm-4 col-form-label">
		<label>Your Team</label>
	</div>
	<div class="col-sm-8">
		<select id="framework" name="framework[]" multiple class="form-control select2" >

		</select>

</div>
</div>
<div class="form-group col-sm-6 row" >

	<div class="col-sm-4 col-form-label">
		<label>Documents</label>
	</div>
	<div class="col-sm-8">
		<div style="position:relative;">
                            <a class="btn btn-primary col-sm-12" href="javascript:;">
                              <input type="file" class="col-sm-12" id="upload" name="image" size="30" required="">
                            </a>
                            &nbsp;
                            <br>
                            <span class="" id="upload-file-info" style="color: black;"></span>
                          </div>

</div>
</div>





	
	<div class="form-group col-sm-12" id="" >
		<label for="exampleInputUsername1" class="col-form-label"> Description 
			<!-- <span class="lbl-short-desc"><i>(Please enter primary details about the case, subject, CNR Number,etc)</i></span> -->
		</label>

		<!-- <textarea name="details" id="txtEngToHindi" class="form-control"></textarea>  -->
		<textarea name="details" id="query" class="ckeditor"></textarea> 
	</div>


			<div class="col-sm-12 row">
				<div class="add_casesubmitdiv" style="
				padding-right: 0;
				">
				<button class="btn btn-info btn-sm add_casesubmit" id="organization_submit" name="add_case" type="button" onclick="submitNonjudicial()"> Submit </button>&nbsp;&nbsp;
				</div>
				<!-- <button class="btn btn-info btn-sm" id="individual_submit" name="add_case" type="button" onclick="submitIndividualCase()">समावेश करा / Submit </button> -->
<!-- 					<div class="col-sm-6"> -->
				<a class="btn btn-danger btn-sm" href="nonjudicial_addcase.php"> Cancel </a>
			<!-- </div> -->
			</div>
		</form>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal" id="data">
	<div class="modal-dialog modal-lg fetchdata-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><b>Please Map the Following Details</b></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" style="padding: 0px;" id="modal-data">

			</div>

		</div>

	</div>
</div>
<?php include 'footer.php'; ?>

<script src="scripts/aes.js"></script>
<script src="scripts/pbkdf2.js"></script>
<script type="text/javascript" src="scripts/jsapi.js"></script>
<script type="text/javascript" src="scripts/nonjudicial.js"></script>
<script type="text/javascript" src="scripts/add_individual_case.js"></script>