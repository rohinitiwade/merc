<?php
include('includes/dbconnect.php');
session_start();
$params    = file_get_contents('php://input');
// $_POST   = json_decode($params, true);
// print_r($_POST);
$arr       = array();
$date      = date('Y-m-d H:i:s');
$case      = $_POST['complete_details'];
$only_date = date('Y-m-d');
// $get    = json_decode($case, true);
if ($_SESSION['user_id'] != '') {
    
    $getcaseuser      = mysqli_query($connection, "SELECT `user_id`,`case_district` FROM `reg_cases` WHERE `case_id`='" . $_POST['case_id'] . "'");
    $fetchuserid      = mysqli_fetch_array($getcaseuser);
    $user_id          = $fetchuserid['user_id'];
    //edit activity log
    $insert_editlog   = mysqli_query($connection, "INSERT INTO `edit_activity_log`(`user_id`,`case_id`,`court_id`,`edit_by`,`date_time`) VALUES('" . $user_id . "','" . $_POST['case_id'] . "','" . $_POST['court_name'] . "','" . $_SESSION['user_id'] . "','" . $date . "')");
    
    
    $edit_case = mysqli_query($connection, "UPDATE `reg_cases` SET `case_title`='" . addslashes(TRIM($_POST['title'])) . "',`case_description`='" . addslashes(TRIM($_POST['query'])) . "',`date_of_filling`='" . addslashes(TRIM($_POST['dateoffilling'])) . "', `case_no`='" . addslashes(TRIM($_POST['case_no'])) . "', `case_no_year`='" . addslashes(TRIM($_POST['case_no_year'])) . "', `court_name`='" . addslashes(TRIM($_POST['court_name'])) . "', `type_of_litigation`='" . addslashes(TRIM($_POST['type_of_litigation'])) . "' , `risk_category`='" . addslashes(TRIM($_POST['risk_category'])) . "' WHERE `case_id`='" . $_POST['case_id'] . "'");
   
    
    // echo $getpetitioners['count_pet'];
    $del_pet = mysqli_query($connection,"DELETE FROM `petitioner_respondent` WHERE `case_id`='".$_POST['case_id']."'  AND `designation`='Petitioner' AND `organisation`='".$_SESSION['organisation']."'");
    foreach ($_POST['petArr'] as $petitioner_array) {     
        if($del_pet){
                $insert_resadvArr = mysqli_query($connection, "INSERT INTO `petitioner_respondent`(`user_id`,`case_id`,`case_no`,`full_name`,`email`,`mobile`,`date_time`,`case_no_year`,`designation`,`add_flag`,`organisation`) VALUES('" . $user_id . "','" . $_POST['case_id'] . "','" . $case_no . "','" . addslashes(TRIM($petitioner_array['pet_name'])) . "','" . addslashes(TRIM($petitioner_array['pet_email'])) . "','" . addslashes(TRIM($petitioner_array['pet_mobile'])) . "','" . $date . "','" . $_POST['case_no_year'] . "','Petitioner','".$petitioner_array['pet_add_flag']."','".$_SESSION['organisation']."')");
            }       
    }
    //================================= respondent =============================================
     $del_res = mysqli_query($connection,"DELETE FROM `petitioner_respondent` WHERE `case_id`='".$_POST['case_id']."' AND `designation`='Respondent' AND `organisation`='".$_SESSION['organisation']."'");
    foreach ($_POST['resArr'] as $keyres) {   
        if($del_res){
                $ins = mysqli_query($connection, "INSERT INTO `petitioner_respondent`(`user_id`,`case_id`,`case_no`,`full_name`,`email`,`mobile`,`date_time`,`case_no_year`,`designation`,`add_flag`,`organisation`) VALUES('" . $user_id . "','" . $_POST['case_id'] . "','" . $case_no . "','" . addslashes(TRIM($keyres['res_name'])) . "','" . addslashes(TRIM($keyres['res_email'])) . "','" . addslashes(TRIM($keyres['res_mobile'])) . "','" . $date . "','" . $_POST['case_no_year'] . "','Respondent','".$keyres['res_flag']."','".$_SESSION['organisation']."')"); 
                }     
    }
    $encrypt_caseid = base64_encode($_POST['case_id']);
$arr = array(
    'status' => 'success',
    'caseid' => $encrypt_caseid,
    'case_id' => $_POST['case_id']
);
     
}
  else {
        $arr = array(
            'status' => 'session expired'
        );
    }

echo json_encode($arr);

?>