<?php include("header.php"); 
// session_start();//header('Content-type: text/html; charset=UTF-8'); ?>
<!-- <link rel="stylesheet" href="bootstrap.min.css"> -->
  <!--   <link rel="stylesheet" href=  
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">   -->
    <!-- <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css"> -->
    <link rel="stylesheet" href="/admin/css/vertical-layout-light/case-report.css">
    <link rel="stylesheet" href="css/vertical-layout-light/jquery-ui.css">
    <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"> -->
    </script>
    <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css"> -->
    <!-- <script type="text/javascript" src="js/case_search.js"></script> -->
    <?php include("menu.php");?>
    <style>
    #legal_data{
      background-image: url("https://www.airinfotech.in/img/logo.png");
      background-position: 65% 50%;
      background-repeat: no-repeat;
      background-size: 36%;
    }
    .legal_report_btn{
      padding: 0px 5px;
      text-align: left;
    }
    .legal_table{
      background: rgba(255,255,255,.92);
    }
    .case_btn{padding:1px 3px;font-size:11px;background-color:#7571f9;border-color:#7571f9;color:#fff;margin:1px 11px;font-weight:700}.case_btns{padding:1px 2px;font-size:10px;background-color:#17a2b8;border-color:#17a2b8;color:#fff;margin:1px 11px;font-weight:700}.modal .modal-dialog .modal-content .modal-body{padding:0 25px}.pagination>li>a, .pagination>li>span {
     position: relative;
     float: left;
     padding: 6px 12px;
     margin-left: -1px;
     line-height: 1.42857143;
     color: #337ab7;
     text-decoration: none;
     background-color: #fff;
     border: 1px solid #ddd;
   }
   .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
     z-index: 3;
     color: #fff;
     cursor: default;
     background-color: #337ab7;
     border-color: #337ab7;
   }
   .legalReportbtn{
     padding: 7px;
     background-color: #ef8832 !important;
     border-color: #ee8227 !important;
     font-size: 12px;
     /*margin: 9px;*/
     color:white;
   }
   .legalReportbtn:hover{
     text-decoration: none;
     color:white;
   }
   .order-listing_paginate,#example_info
   {
     display: none !important;
   }
   div.dataTables_wrapper div.dataTables_paginate{
     display: none !important;
   }
   .pagination{
     float: right !important;
   }
   .srno{
    width: 6%;
    text-align: center;
  }
  .subhead_legal{
    width: 23%;
    font-weight: bold;
  }
  .heading_legal{
    text-align: center;
    font-size: 20px;
    /*margin: 4% 0 2% 0;*/
  }
  .legal_table td{
    padding: 7px;
  }
  .fetchdataloader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 60px;
    float: right;
    height: 60px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
  }

  /* Safari */
  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  .selallchk{
    display: inline-block!important;
  }

  @media print{
    .navbar,#sidebar,.buttons{
      display: none;
    }
    .content-wrapper{
      width: 100%;
      margin: 0px;
    }
  }

</style>
<div class="main-panel">
  <div class="content-wrapper">
   <div class="card">
    <?php 
    extract($_POST);
    $name = $_POST['name'];
    ?>
    <div class="card-body">
     <div class="col-sm-12 buttons">
      <button class="btn btn-primary btn-sm" type="button" onclick="window.print()">Print</button>
      <button class="btn btn-warning btn-sm" type="button" onclick="exportLegalAudit()">Export</button>
    </div>
    <div id="legal_data">
     <!-- <div id="showclr_data"></div> -->
   </div>

 </div>

</div>
<?php include("footer.php");?>

<script type="text/javascript" src="js/legal_audit_report.js"></script>
<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->
<script type="text/javascript" src="js/pdfmake.min.js"></script>
<script type="text/javascript" src="js/html2canvas.min.js"></script>













