<?php  include('includes/dbconnect.php');  session_start();
header('Access-Control-Allow-Origin: *');
$law = $_POST['caselaw'];
$data = json_decode($law, true);
$date = date('Y-m-d H:i:s');
$law = array();
if($_SESSION['user_id'] != ''){
	if($_POST['caseid']!=''){
		$get_cases = mysqli_query($connection,"SELECT case_id,citation,product_type,judgement_href,dod,search FROM `case_law_search` WHERE `case_id`='".$_POST['caseid']."' AND `remove_status`=1");
		$count_law = mysqli_num_rows($get_cases);
		while($get = mysqli_fetch_array($get_cases)){
			$view_law = new stdClass();
			$view_law->case_id = $get['case_id'];
			$view_law->citation = $get['citation'];
			$view_law->search = $get['search'];
			$view_law->product_type = $get['product_type'];
			$view_law->judgement_href = $get['judgement_href'];
			$view_law->dod = $get['dod'];
			
			$case_law[] = $view_law;
		}
		$law = array('data' => $case_law,'count' => $count_law);
	}
} 
echo json_encode($law);
?>