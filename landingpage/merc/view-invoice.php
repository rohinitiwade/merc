<?php session_start(); ?> 
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <style type="text/css">
      .row{
        margin-left: 0;
        margin-right: 0;
      }
      .form-group {
        margin-bottom: 10px;
      }
      .action-th{
        width: 1%;
      }
      .particulars-th{
        width: 40%;
      }
      .tax-th{
        width: 20%;
      }
      .total-th{
        width: 10%;
      }
      .bg-heading{
        background-color: #f7f4f4 !important;
        color: #404E67;
        font-weight: bold;
      }
      .invoice-faded-text{
        color: #eeeeee;
        font-size: 25px;
        font-weight: bold;
        padding: 10px 0;
        text-align: center;
        text-transform: uppercase;
      }
      .page-body{
        margin: auto;
        width: 85%;
      }
    </style>
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header m-b-10">
              <h5 class="card-title">Invoice <span></span></h5>
              <div class="col-sm-12 text-right">
               <button class="btn btn-info btn-mini" type="button">Edit</button>
               <button class="btn btn-warning btn-mini" type="button">Enter Payment</button>
               <button class="btn btn-primary btn-mini" type="button">Download</button>
             </div>
           </div>
           <div class="page-body">
            <div class="card form-group">
              <div class="card-header">
               <div class="invoice-faded-text">Invoice</div>

             </div>
             <?php 
             $invoice = mysqli_query($connection,"SELECT * FROM `invoices` WHERE `invoice_id`='".base64_decode($_GET['createdid'])."'");
             $getInvoice= mysqli_fetch_array($invoice);

             $client = mysqli_query($connection,"SELECT full_name FROM `petitioner_respondent` WHERE `pet_res_id`='".$getInvoice['client_id']."'");
             $i=1;
             $getClient= mysqli_fetch_array($client);
             ?> 
             <div class="card-block">
              <div class="row">
                <?php $profile = mysqli_query($connection, "SELECT `image`,`gender`,`name`,`last_name` FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."' ") or die (mysqli_error());
                      $selprofile = mysqli_fetch_array($profile);?>
                      
                <div class="col-sm-6">
                  <div class="col-sm-12 form-group">
                    <label><b>Bill From: </b></label>
                    <h6 class="m-0"><?php echo $selprofile['name'];?>  <?php echo $selprofile['last_name'];?></h6>
                    <p class="m-0"><?php echo $getInvoice['billing_address']; ?></p>
                  </div>
                  <div class="col-sm-12 form-group">
                    <label><b>Bill To:</b> </label>
                    <h6 class="m-0"><?php echo $getClient['full_name']; ?></h6>
                    <p class="m-0"></p>
                  </div>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                  <div class="col-sm-6 form-group f-right">
                    <?php if($selprofile['image']!=''){ ?>
                    <img src="uploads_profile/<?php echo $selprofile['image'];?>" width="auto" height="115px">
                    <?php }else{ 
                    if($selprofile['gender'] == 'Male'){?>
                      <img src="uploads_profile/1576303937.png" alt="profile"/ class="userimage_mobile">
                    <?php }else{?>
                      <img src="uploads_profile/femaleicon.png" alt="profile"/ class="userimage_mobile">
                    <?php } } ?>
                  </div>
                  <div class="col-sm-8 form-group f-right" style="text-align: left;">
                    <table class="table table-bordered">
                      <tbody><tr>
                        <td class="bg-heading p-1">Invoice Number:</td>
                        <td class="p-1"><?php echo $getInvoice['invoice_number']; ?></td>
                      </tr>
                      <tr>
                        <td class="bg-heading p-1">Issue Date:</td>
                        <td class="p-1"><?php echo date('F d, Y',strtotime($getInvoice['date_of_issue'])); ?></td>
                      </tr>
                      <tr>
                        <td class="bg-heading p-1">Balance Due:</td>
                        <td class="p-1">INR <?php echo money_format('%i',$getInvoice['total_amount']); ?></td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="table table-resposive">
                    <table class="table table-bordered">
                      <thead>
                        <tr class="bg-heading">
                          <th>Particulars</th>
                          <th>Quantity</th>
                          <th>Cost</th>
                          <th>Tax %</th>
                          <th>Total(INR)</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        $global_subtotal = '';
                        $global_total = '';
                        $getinvoice = mysqli_query($connection,"SELECT * FROM `invoice_listing` WHERE `invoice_id`='".$getInvoice['invoice_id']."' ORDER BY invoice_id DESC");
                        $i=1;
                        while($fetch= mysqli_fetch_array($getinvoice)){
                          $global_subtotal = $fetch['subtotal'];
                          $global_total = $fetch['total_amount'];
                          ?>
                          <tr>
                            <td> <?php echo $fetch['particulars']; ?></td>
                            <td> <?php echo $fetch['quantity']; ?></td>
                            <td> <?php echo $fetch['cost']; ?></td>
                            <td> <?php echo $fetch['tax_value']. ' ('.$fetch['tax_value_in_per'].' % )'; ?></td>
                            <td> <?php echo $fetch['amount']; ?></td>
                          </tr>
                          <?php $i++; } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                            <td colspan="2" rowspan="8"></td>
                            <th colspan="2">Subtotal</th>
                            <th> <?php echo $global_subtotal; ?></th>
                          </tr>
                          <?php  $getView = mysqli_query($connection,"SELECT * FROM `invoice_listing` WHERE `invoice_id`='".$getInvoice['invoice_id']."' ");
                          while($getViews= mysqli_fetch_array($getView)){ 
                             $getinvoicedata = mysqli_query($connection,"SELECT SUM(tax_total) as total FROM `invoice_listing` WHERE `invoice_id`='".$getInvoice['invoice_id']."' AND `tax_value`='".$getViews['tax_value']."' ORDER BY invoice_id DESC");
                          $getinvoicedatas= mysqli_fetch_array($getinvoicedata);
                             ?>
                           
                           <tr>
                             
                            <td colspan="2">Total <?php echo $getViews['tax_value'];?></td>
                            <td> <?php echo $getinvoicedatas['total']; ?></td>
                          </tr>

                            <?php } ?>









                          <!-- <?php 
                          $getinvoice = mysqli_query($connection,"SELECT * FROM `invoice_listing` WHERE `invoice_id`='".$getInvoice['invoice_id']."' ORDER BY invoice_id DESC");
                          $fetch= mysqli_fetch_array($getinvoice);?>
                          <tr>
                            <td colspan="2" rowspan="5"></td>
                            <th colspan="2">Subtotal</th>
                            <th> <?php echo $fetch['subtotal']; ?></th>
                          </tr>
                          <?php  $getView = mysqli_query($connection,"SELECT * FROM `invoice_listing` WHERE `invoice_id`='".$getInvoice['invoice_id']."' ");
                          while($getViews= mysqli_fetch_array($getView)){ 
                            if($getViews['tax_value'] == 'GST'){?>
                          <tr>
                             <?php 
                            
                          $getinvoice = mysqli_query($connection,"SELECT SUM(tax_total) as total FROM `invoice_listing` WHERE `tax_value`='GST' AND `invoice_id`='".$getViews['invoice_id']."'");
                          $fetch= mysqli_fetch_array($getinvoice);?>
                            <td colspan="2">Total GST</td>
                            <td> <?php echo $fetch['total']; ?></td>
                          </tr>
                        <?php } if($getViews['tax_value'] == 'IGST'){?>
                          <tr>
                             <?php 
                          $getinvoice = mysqli_query($connection,"SELECT SUM(tax_total) as total FROM `invoice_listing` WHERE `tax_value`='IGST' AND `invoice_id`='".$getViews['invoice_id']."' ORDER BY invoice_id DESC");
                          $fetch= mysqli_fetch_array($getinvoice);?>
                            <td colspan="2">Total IGST</td>
                            <td> <?php echo $fetch['total']; ?></td>
                          </tr>
                          <?php } if($getViews['tax_value'] == 'Tax'){?>
                          <tr>
                             <?php 
                          $getinvoice = mysqli_query($connection,"SELECT SUM(tax_total) as total FROM `invoice_listing` WHERE `tax_value`='Service Tax' AND `invoice_id`='".$getViews['invoice_id']."' ORDER BY invoice_id DESC");
                          $fetch= mysqli_fetch_array($getinvoice);?>
                            <td colspan="2">Total Service Tax</td>
                            <td> <?php echo $fetch['total']; ?></td>
                          </tr>
                          <?php } if($getViews['tax_value'] == 'VAT'){?>
                          <tr>
                             <?php 
                          $getinvoice = mysqli_query($connection,"SELECT SUM(tax_total) as total FROM `invoice_listing` WHERE `tax_value`='VAT' AND `invoice_id`='".$getViews['invoice_id']."' ORDER BY invoice_id DESC");
                          $fetch= mysqli_fetch_array($getinvoice);?>
                            <td colspan="2">Total VAT</td>
                            <td> <?php echo $fetch['total']; ?></td>
                          </tr>
                        <?php } }?>
                          <tr> -->
                           <!--  <td colspan="2"></td> -->
                           <th colspan="2">Total</th>
                           <th> <?php echo $global_total; ?></th>
                         </tr>
                         <tr>
                          <td colspan="2">Amount Paid</td>
                          <td> </td>
                        </tr>
                        <tr>

                          <td colspan="2" class="bg-heading">Balance Due</td>
                          <td class="bg-heading"><?php echo $fetch['total_amount']; ?></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
                 <!--  <div class="col-sm-12 form-group f-right">
                    <label>Payment History</label>
                    <table class="table table-bordered">
                      <thead>
                        <tr class="bg-primary">
                          <th>1</th>
                          <th>2</th>
                        
                        </tr>
                      </thead>
                      <tbody><tr>
                        <td>Invoice Number:</td>
                        <td>3</td>
                      </tr>
                      <tr>
                        <td>Issue Date:</td>
                        <td>February 03, 2020</td>
                      </tr>
                      <tr>
                        <td>Balance Due:</td>
                        <td>INR 2.00</td>
                      </tr>
                    </tbody></table>
                  </div> -->
                 <!--  <div class="col-sm-12 form-group f-right">
                    <label>Invoice History</label>
                    <table class="table table-bordered">
                      <thead>
                        <tr class="bg-primary">
                          <th>1</th>
                          <th>2</th>
                         
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Invoice Number:</td>
                          <td>3</td>
                        </tr>
                        <tr>
                          <td>Issue Date:</td>
                          <td>February 03, 2020</td>
                        </tr>
                        <tr>
                          <td>Balance Due:</td>
                          <td>INR 2.00</td>
                        </tr>
                      </tbody>
                    </table>
                  </div> -->
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/create-invoice.js"></script>