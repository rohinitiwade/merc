<?php  include("phpfile/sql_home.php"); ?>
<link rel="stylesheet" href="styles/index-block-data.css">
<style type="text/css">
	.upcoming_class{
		background: #404E67;
		color: white;
		padding: 14px 0px;
		/* margin: 11px; */
		text-align: center;
	}
	.card-title{
		text-align: center;
	}
</style>
<div class="col-xl-3 col-md-6">
	<div class="card statustic-progress-card uploadcasesback update-card">
		<a href="cases-report.php" style="color: white"> <div class="card-block">

			<div class="row align-items-center dashboard-blocks-div">
				<div class="col">
					<h5 class="text-white subcount mb-0">
					 <?php 
						 
							$list = mysqli_query($connection,"SELECT count(case_id) as case_count FROM `reg_cases` WHERE ".$conditionquery."") or die (mysqli_error($connection));
							 
						 
						$allcount = mysqli_fetch_array($list);
						 
						
						echo $allcount['case_count'];?>
						<span class="float-right ">
							<i class="icofont icofont-briefcase-alt-1 dashboardicon"></i>
						</span>
					</h5>
					<h5 class="subhead">Uploaded Cases</h5> 
					<!-- <p>(अपलोड केलेली प्रकरणे )</p> -->
					<div class="progress m-t-15">
						<div class="progress-bar uploadcasesprogress" style="width:<?php  echo $allcount['case_count'];?>%">
						</div>

					</div>
					<!-- <div class="row"><div class="col-sm-6" style="">Judicial <?php echo $judcount['jud_count'];?></div>
					<div style="text-align: right;" class="col-sm-6">Non-Judicial <?php echo $noncount['non_count'];?></div></div> -->
				</div>
			</div>

		</div> </a>

	</div>
</div>
<!-- <div class="col-xl-3 col-md-6">
	<div class="card statustic-progress-card advocateback update-card">
		<a href="team.php" style="color: white">
			<div class="card-block">
				<div class="row align-items-center dashboard-blocks-div">
					<div class="col">
						<h1 class="subcount">
							<?php 
							if($_SESSION['user_id']!='77'){
								$team = mysqli_query($connection,"SELECT count(reg_id) as case_count FROM `law_registration` WHERE `reg_id`!='77'");
							}else{
								$team = mysqli_query($connection,"SELECT count(reg_id) as case_count FROM `law_registration` WHERE `reg_id`='77'");
							}
							$teamsel = mysqli_fetch_array($team);
							echo $teamsel['case_count'];?>
							<span class="float-right "><i class="icofont icofont-businessman dashboardicon"></i></span>
						</h1> 
						<h5 class="subhead">  Team  </h5>
						<div class="progress m-t-15">
							<div class="progress-bar advocateprogress" style="width:<?php echo $contadvocate*100/$allcount['case_count'];?>%">
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div> -->
<div class="col-xl-3 col-md-6">
	<div class="card  statustic-progress-card documentback update-card">
		<a href="manage-document.php" style="color: white">
			<div class="card-block">
				<div class="row align-items-center dashboard-blocks-div">
					<div class="col">
						<h1 class="subcount">
						 <?php 

							$doc = mysqli_query($connection, "SELECT ad.doc_id FROM add_document ad LEFT JOIN reg_cases reg ON reg.case_id = ad.case_id WHERE ".$conditionquerydocument."") or die (mysqli_error($connection));

							echo $seldoc = mysqli_num_rows($doc) ?>
  
							<span class="float-right ">
								<i class="icofont icofont-file-document dashboardicon">
								</i>
							</span>
						</h1>
						<h5 class="subhead"> Documents 
						</h5>
          <!-- <h6 class="marathi_head">(कागदपत्रे)
          </h6> -->
          <div class="progress m-t-15">
          	<div class="progress-bar documentprogress" style="width:<?php echo $seldoc*100/$allcount['case_count'];?>%">
          	</div>
          </div>
      </div>
  </div>
</div>
</a>
</div>
</div>
 

<!-- <div class="col-xl-3 col-md-6">
	<div class="card statustic-progress-card advocateback update-card">
		<a href="legal-support.php" style="color: white">
			<div class="card-block">
				<div class="row align-items-center dashboard-blocks-div">
					<div class="col">
						<h1 class="subcount">
							
							<span class="float-right "><i class="icofont icofont-businessman dashboardicon"></i></span>
						</h1> 
						<h5 class="subhead">  Request Legal Support  </h5>
						<div class="progress m-t-15">
							<div class="progress-bar advocateprogress" style="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>  -->
<!-- <div class="col-xl-3 col-md-6">
	<div class="card statustic-progress-card advocateback update-card">
		<a href="team.php" style="color: white">
			<div class="card-block">
				<div class="row align-items-center dashboard-blocks-div">
					<div class="col">
						<h1 class="subcount">
							 
							<span class="float-right "><i class="icofont icofont-businessman dashboardicon"></i></span>
						</h1> 
						<h5 class="subhead">  View Response  </h5>
						<div class="progress m-t-15">
							<div class="progress-bar advocateprogress" style="width:<?php echo $contadvocate*100/$allcount['case_count'];?>%">
							</div>
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>  --> 
<div class="col-xl-3 col-md-6">
	<div class="card  statustic-progress-card bg-c-pink update-card">
		<a href="viewLegalSupport.php" style="color: white">
			<div class="card-block">
				<div class="row align-items-center dashboard-blocks-div">
					<div class="col">
						<h1 class="subcount">
							  <?php 

							$legal = mysqli_query($connection, "SELECT * FROM reg_cases rc LEFT JOIN case_registration cr ON cr.case_id=rc.case_id WHERE  ".$view_support."") or die (mysqli_error($connection));

							echo $legals = mysqli_num_rows($legal) ?>
							<span class="float-right ">
								<i class="icofont icofont-meeting-add dashboardicon">
								</i>
							</span>
						</h1>
						<h5 class="subhead">Legal Support  </h5> 
           <!--  <h6 class="marathi_head">(स्मरणपत्रे) 
           </h6>  --> 
           <div class="progress m-t-15">
           	<div class="progress-bar remindprogress" style="width:">
           	</div>
           </div>
       </div>
   </div>
</div>
</a>
</div>
</div>	
<div class="col-xl-3 col-md-6">
  <div class="card  statustic-progress-card bg-c-lite-green update-card">
    <a href="tree-dashboard.php" style="color: white;">
      <div class="card-block">
        <div class="row align-items-center dashboard-blocks-div">
          <div class="col">
            <h1 class="subcount">
              <span class="float-right ">
                <i class="icofont icofont-chart-flow-alt-1 dashboardicon">
                </i>
              </span>
            </h1>
            <h5 class="subhead">Consolidated Legal Audit Report</h5> 
            <!-- <h6 class="marathi_head">(एकत्रित विषयनिहाय कायदेशीर अंकेक्षण अहवाल)</h6> -->
            <div class="progress m-t-15">
              <div class="progress-bar legalprogress" style="width:35%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </a>
    <!-- <div class="card-footer">
<p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p>
</div> -->
</div>
</div>
<!-- <div class="row col-12">
	<div class="col-xl-4 col-sm-6 grid-margin stretch-card marqueelist ">
		<div class="card-dashboard recent-revenue-card" style="box-shadow: 0 1px 15px 1px rgb(161, 170, 193,.5);
		border: 1px solid #d0c4c4;">
		<div class=" upcomingHearing">
			<div class="upcoming_class"><h6 class="card-title">Upcoming Hearing Dates
			</h6></div>
			<div class="upcoing_hearingfetch">
			</div>

		</div>
	</div>
</div> -->
<!-- <div class="col-xl-4 col-sm-6 grid-margin stretch-card marqueelist ">
	<div class="card-dashboard recent-revenue-card" style="box-shadow: 0 1px 15px 1px rgb(161, 170, 193,.5);
	border: 1px solid #d0c4c4;">
	<div class="  upcomingHearing">
   
  <div class="upcoming_class"><h6 class="card-title">Upcoming To Do's
  </h6></div>
  <marquee onMouseOver="this.stop()" onMouseOut="this.start()" class="background_marquee" behavior="scroll" scrollamount="2" direction="up">
  	<div class="news-content" tabindex="0">
  		<div class="upcoing_reminder">

  			</div>
  		</div>
  	</marquee>
  	<?php if($count >0){?>
  		<p class="view-all-activities"><a href="" class="marquee_view" style="padding:15px;">View All
  		</a></p>
  	<?php } ?>
  </div>
</div>
</div> -->
<!-- <div class="col-xl-4 col-sm-6 grid-margin stretch-card marqueelist ">
	<div class="card-dashboard recent-revenue-card" style="box-shadow: 0 1px 15px 1px rgb(161, 170, 193,.5);
	border: 1px solid #d0c4c4;">
	<div class="upcomingHearing">
		 
		<div class="upcoming_class"><h6 class="card-title">Recent Case Activities
		</h6></div>
		<div class="news-content" tabindex="0">
			<div class="recent_activity">
				<ul class="hearing_ul form-group">
					 
		</ul>
	</div>
	<?php if($cnt > 10){?>
		<p class="view-all-activities"><a href="viewall_recentact.php" target="_blank" class="marquee_view" style="padding:15px;">View All</a></p>
	<?php } ?>
</div>
</div>
</div>
</div>   -->
<div class="col-sm-12">
	<div class="card-dashboard">
		<div class="col-sm-12">
			<div id="full_calendar">
			</div>
			<br/>
		</div>
	</div>
</div>
<div class="col-sm-12 mt-5">
	<div class="modal" id="myModal">
		<div class="modal-dialog modal-md" style="max-width: 750px;">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header hearingTodoHeader">
					<h4 class="modal-title">Please provide Reminder list or Hearing Date
					</h4>
					<button type="button" class="close hearingTodoclose" data-dismiss="modal">&times;
					</button>
				</div>
				<!-- Modal body -->
				<div class="modal-body hearingTodoData">
					<div class="col-sm-12" style="text-align: center;">
						<label class="col-sm-1">Type
						</label>
						<label class="col-sm-3">
							<input type="radio" name="new_event" checked value="hearing_radio"> Hearing
						</label>
						<label class="col-sm-3">
							<input type="radio" name="new_event" value="todo_radio"> Reminder
						</label>
						<label id="view_case_button" class="col-sm-3">
							<a id="view_case_anchor_id" href="" title="View" target="_blank">
								<button type="button" class="btn btn-success btn-sm" style="height: 30px;padding: 0 10px;">View Case
								</button>
							</a>
						</label>
					</div>
					<form id="hearing_data">
						<div class="col-sm-12 row" id="hearing"> 
							<input id="hearing_data_id" name="id" value="" type="hidden"/>               
							<div class="col-sm-12 form-group">
								<b>Belong To: </b>
								<select id="belong_to" class="select2 form-control" name="belong_to" style="width:100%;">
								</select>
							</div>
							<div class="col-sm-6 form-group">
								<b>Stage: </b>
								<input id="stage" class="form-control" type="text" value="" name="stage" />
							</div>
							<div class="col-sm-6 form-group">
								<b>Posted For: </b>
								<input id="posted_for" class="form-control" type="text" value="" name="posted_for" />
							</div>
							<div class="col-sm-6 form-group">
								<b>Action Taken: </b>
								<input id="action_taken" class="form-control" type="text" value="" name="action_taken" />
							</div>
							<div class="col-sm-6 form-group">
								<b>Next Hearing Date: </b>
								<input id="next_hearing_date" class="form-control" type="text" value="" onFocus="blur();" />
								<input id="next_hearing_date_alt" type="hidden" value="" name="next_hearing_date" />
							</div>
							<div class="col-sm-6 form-group">
								<b>Session: 
								</b>
								<!-- <select class="select2 form-control"></select> -->
								<select id="session_phase" class="form-control" name="session_phase">
									<option value="" selected>-- Please Select --</option>
									<option value="1">Morning</option>
									<option value="2">Evening</option>
								</select>
							</div>
							<div class="col-sm-12 form-group">
								<b style="float: left;width: 100%;">Who Attended: </b>
								<div id="who_attended_record">
								</div>
								<input id="who_attended_other" class="form-control" type="text" value="" name="who_attended[]" style="display:none;" />
							</div>
							<div class="col-sm-12">
								<textarea id="hearing_description" class="ckeditor">
								</textarea>
								<textarea id="hearing_description_data" name="hearing_description" style="display:none;">
								</textarea>                       
							</div>                    
						</div>
					</form>
					<form id="to_do_data">
						<div class="col-sm-12 row" id="to-do-list">
							<input id="todo_data_id" name="id" value="" type="hidden"/> 
							<div class="col-sm-12 form-group">
								<b>Description </b>
								<textarea id="desc" class="form-control" rows="4">
								</textarea>
							</div>
							<div class="col-sm-12 form-group">
								<b class="col-sm-12" style="padding: 0">Would you like to assign this to-do to any case?</b> &nbsp;
								<label>
									<input type="radio" name="assign_other"> Yes
								</label>
								<label>
									<input type="radio" name="assign_other" checked> No
								</label>
							</div>
							<div class="col-sm-12 form-group">
								<b>Relate To: </b>
								<select id="relate" name="framework[]" multiple="multiple"  class="form-control" placeholder="Please Select">
									<option value="">Please Select
									</option>
									<?php $courtname = mysqli_query($connection, "SELECT `case_id`,`case_title` FROM `reg_cases` WHERE `case_title`!='' ORDER BY case_id DESC") or die (mysqli_error($connection));
									while($selcourtlist = mysqli_fetch_array($courtname)){?>
										<option value="<?php echo $selcourtlist['case_id'];?>">
											<?php echo $selcourtlist['case_title'];?>
										</option>
									<?php } ?>
								</select>
							</div>
							<div class="col-sm-12 form-group">
								<b>Assign To: </b>
								<select id="framework" name="framework[]" multiple class="form-control" >
									<?php $team = mysqli_query($connection, "SELECT `name`,`last_name` FROM `law_registration` WHERE `parent_id`='".$_SESSION['user_id']."' ORDER BY reg_id DESC") or die (mysqli_error($connection));
									while($teams = mysqli_fetch_array($team)){?>
										<option value="<?php echo $teams['reg_id'];?>">
											<?php echo  $teams['name'];?>  
											<?php echo  $teams['last_name'];?> 
										</option>
									<?php } ?>
								</select>
							</div>
							<div class="col-sm-12 form-group row">
								<b class="col-sm-12">Please select due date: </b>
								<div class="col-sm-6">
									<input type="text" id="due_date_from" class="form-control" />
									<input type="hidden" id="due_date_from_alt" />
								</div>
								<div class="col-sm-1">
									<span>To</span>
								</div>
								<div class="col-sm-5">
									<input type="text" id="due_date_to" class="form-control" />
									<input type="hidden" id="due_date_to_alt" />
								</div>
							</div>
							<div class="col-sm-12 form-group">
								<input type="checkbox" style="position: relative;top: 1px;" id="private">
								<b> &nbsp;Mark As Private</b>
							</div>
							<div class="col-sm-12">
								<b>Please set your auto reminders </b>
								<div class="form-group" id="reminder_clickhere">
									<p>
										<b id="reminder_click_id">Click here</b> to add auto reminders
									</p>
								</div>
								<div id="reminder_input" style="display: none;">
									<div class="row" id="input">
										<div class="col-sm-1">
											<i id="removereminder_click_id" class='fa fa-minus-circle' style='color: red;cursor: pointer;margin-top: 9px;font-size: 20px;'>
											</i>
										</div>
										<div class="col-sm-3">
											<select class="form-control" id="email_option_0" name='minute[]'>
												<option value="Email">Email
												</option>
												<option value="SMS">SMS
												</option>
											</select>
										</div>
										<div class="col-sm-2">
											<select class="form-control" id="reminder_format_value_0" name='subject[]'>
												<option value='5'>5</option>
												<option value='10'>10</option>
												<option value='15'>15</option>
												<option value='20'>20</option>
												<option value='25'>25</option>
												<option value='30'>30</option>
												<option value='35'>35</option>
												<option value='40'>40</option>
												<option value='45'>45</option>
												<option value='50'>50</option>
												<option value='55'>55</option>
											</select>
										</div>
										<div class="col-sm-3">
											<select class="form-control reminder_format" id="minute_0" name='minute[]'>
												<option value="minutes">minute(s)</option>
												<option value="hours">hour(s)</option>
												<option value="days">day(s)</option>
												<option value="week">week(s)</option>
											</select>
										</div>
										<div class="col-sm-3">
											<span style="margin-top: 8px;">before the due date & time
											</span>
										</div>
									</div>
									<i id="add_field" class="fa fa-plus-circle" style="color: green;cursor: pointer;margin-right: 20px;font-size: 19px;">
									</i>Add an additional reminder
									<br>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" id="submit_btn_id" class="btn btn-info btn-sm newsubmit">Submit
					</button>
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
</div> 

