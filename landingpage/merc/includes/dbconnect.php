<?php session_start(); date_default_timezone_set('Asia/Calcutta');
function db_connect() {
    static $connection;
    if(!isset($connection)) {
       // $config = parse_ini_file('config.ini'); 
        $connection = mysqli_connect('localhost','root','root','case_tracker');
        mysqli_set_charset( $connection, 'utf8');
    }
    if($connection === false) {
        return mysqli_connect_error(); 
    }
    return $connection;
}
$connection = db_connect();
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}else{
    //echo "done";
}
// $urls = 'http://15.206.144.246/mhlms/landingpage/merc/';
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
    $link = "https"; 
else
    $link = "http"; 
// Here append the common URL characters. 
$link .= "://"; 
  
// Append the host(domain name, ip) to the URL. 
$link .= $_SERVER['HTTP_HOST']; 
// $urls = 'http://15.206.144.246/mhlms/landingpage/merc/';
$urls = $link. '/merc/landingpage/merc/';
$docUrl = '/mnt/S3storage/cms_document/';
// echo $urls;
$qtool = 'http://103.252.168.76:8080/qtool/';
$legalResearch = 'http://34.228.88.76:8080/LegalResearch/';
// $legalResearch = 'http://192.168.0.191:8080/LegalResearch/';
$mon = Date('Y-m-d', StrToTime("Last Monday"));
$sun = Date('Y-m-d', StrToTime("Next Sunday"));

 
?> 
