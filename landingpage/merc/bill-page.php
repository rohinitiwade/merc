<?php include("header.php") ?>
<link rel="stylesheet" type="text/css" href="styles/bill-page.css">
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<?php session_start(); ?> 
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header m-b-10">
              <h5 class="card-title"> Bills </h5>
            </div>
            <div class="page-body">
              <div class="card">
                <div class="card-block">
                  <div class="col-sm-12 form-group" style="text-align: right;">
                    <a class="add_new_bill btn btn-primary btn-sm" href="">New Bill</a>
                    <button class="btn btn-warning btn-sm" type="button" data-target="#export-modal" data-toggle="modal">Export</button>
                    <button class="btn btn-info btn-sm" type="button">Filter</button>
                  </div>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th class="checkbox_th"></th>
                          <th class="sorting">Bill Number</th>
                          <th class="bill-for">Bill For</th>
                          <th class="sorting">Date of Receipt</th>
                          <th class="index-table-total sorting">Total (INR)</th>
                          <th>Status</th>
                          <th class="action-column">Action(s)</th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr>
                          <td class="checkbox_td" data-th="">
                            <span class="show_checkbox">
                              <input type="checkbox" value="" name="checked_id[]" class="checked_checkbox">
                            </span>
                          </td>
                          <td data-th="Bill Number">02</td>
                          <td data-th="Bill From">1 MADHUKAR S/D/W/Thru:- VITTHALRAO NISTANE&nbsp;&nbsp;SOCIAL WORK R/O VASANT NAGAR GHATANJI , DIST. YAVATMAL , R , YAVATMAL , MAHARASHTRA</td>
                          <td data-th="Date of Receipt">Feb 03, 2020</td>
                          <td class="index-table-total" data-th="Total (INR)">246.42</td>
                          <td data-th="Status">Partially Paid</td>
                          <td class="invoice-actions-section" data-th="Action">
                            <a href="bill-payment" title="Enter Payment"><span class="fa fa-rupee"></span></a>
                            <span class="feather icon-eye"></span>
                            <span class="feather icon-edit"></span>
                            <span class="feather icon-download"></span>
                            <span class="feather icon-trash"></span>
                          </td>
                        </tr>                            
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--</div> -->
<?php include 'footer.php'; ?>