<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
      <?php include("menu.php");?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="card">
            <div class="card-body">
              <h4 class="card-header" style="padding-top: 0px"><b style="color: green;">Import Cases</b></h4>
              <div class="row" style="margin-top: 25px;text-align: center;">
                <div class="col-12">
                    <h4><b>WOULD YOU LIKE TO IMPORT FROM?</b></h4><br><br>
                    <span style="font-size: 18px;"><u style="color: green;">Bombay</u> High Court</span><br><br>
                    <a href="highcourt.php?courtnam=Bombay High Court" style="padding: 9px 11px;background-color: #2d4866;color: white;border-radius: 30px;" class="btn">CLICK HERE</a><br><br><br>
                    <span style="font-size: 18px;">All the District Courts</span><br><br>
                    <a href="districtcourt.php?courtname=District Courts" style="padding: 9px 11px;background-color: #2d4866;color: white;border-radius: 30px;" class="btn">CLICK HERE</a>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php include("footer.php");?>