<!-- <link rel="stylesheet" type="text/css" href="styles/jquery.steps.css"> -->
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<?php $pageNam =  basename($_SERVER["PHP_SELF"]);
$insert = mysqli_query($connection, "INSERT INTO `frequency_report` SET `user_id`='".$_SESSION['user_id']."',`case_id`='".base64_decode($_GET['caseid'])."',`division`='".$_SESSION['cityName']."',`page_name`='".$pageNam."',`date_time`='".date("Y-m-d H:i:s")."',`dates`='".date("Y-m-d")."'");?>
<link rel="stylesheet" type="text/css" href="styles/add-causelist.css">
<body>
	<?php $case = mysqli_query($connection, "SELECT `case_id`,`case_no`,`court_id`,`case_title`,`user_id` FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
	$selcase = mysqli_fetch_array($case);
	$get_cases = mysqli_query($connection,"SELECT * FROM `case_law_search` WHERE `case_id`='".base64_decode($_GET['caseid'])."' AND `remove_status`=1");
	$count_law = mysqli_num_rows($get_cases);
	$todo_cases = mysqli_query($connection,"SELECT DISTINCT`todo_id` FROM `todo_list` WHERE  `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' ORDER BY todo_id DESC");
	$cnt_todocases = mysqli_num_rows($todo_cases);?>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title">Causelist</h5>
							</div>
							<div class="page-body">
								<div class="form-group">
									<div class="card col-sm-12">
										<div class="card-block form-group row">
											<div class="col-sm-12 row">
												<div class="col-sm-6 row form-group">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><b>Court</b></label>
													<div class="col-sm-8">
														<select id="cases-court_id" class="form-control" name="court_id">
															<option value="">Please select</option>
															<?php $courtname = mysqli_query($connection, "SELECT * FROM `court_list` ORDER BY court_id ASC");
															while($selcourtlist = mysqli_fetch_array($courtname)){?>
																<option value="<?php echo $selcourtlist['court_id'];?>"><?php echo $selcourtlist['court_name'];?></option>
															<?php } ?>
															<option value="9">Other</option>
														</select>
													</div>
												</div>

												<div class="col-sm-6 row form-group" id="datepicker-causelist">
													<label class="col-sm-4"><b>Date</b></label>
													<div class="col-sm-8">
														<input type="text" class="form-control" id="causeListDate" placeholder="Select date" name="">
													</div>
												</div>

												<div id="comingsoon" class="col-sm-12" style="display: none;">
													<h5 style="text-align: center;"><b>Coming Soon</b></h5>
												</div>
											</div>
											<div id="highcourt" class="col-sm-12 row" style="display: none;">
												<div class="form-group col-sm-6 row">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><b>High court</b></label>
													<div class="col-sm-8">
														<select id="cases-high_court_ids" class="form-control" name="high_court_id">
															<option value="">Please select</option>
															<?php $highcourt= mysqli_query($connection,"SELECT * FROM `high_court_list` where `court_id` ='3' ORDER BY high_court_name ASC");
															while($seligh = mysqli_fetch_array($highcourt)){?>
																<option value="<?php echo $seligh['court_id'];?>"><?php echo $seligh['high_court_name'];?></option>
															<?php } ?>
														</select>
													</div>
												</div>

												<div class="form-group col-sm-6 row" id="bench">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><b>Bench</b></label>
													<div class="col-sm-8">
														<select  class="form-control highcourtidslist" name="hc_bench_id" id="high_court_idslist" >
															<option value="10">Please select</option>
														</select>
													</div>
												</div>
												
											</div>
											<div class="col-sm-12 row form-group" id="advocate-btn" style="display: none;">
												<div class="form-group col-sm-6 row" id="advocatename">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><b>Advocate Name</b></label>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="advocate_name" id="advocate_name" onkeyup="getAdvocateName()" placeholder="Enter Advocate Name">
														<!-- <select class="form-control" id="advocate_name"></select> -->
														<div id="suggesstion-adv" style="display: none;"></div>
													</div>  
												</div>

												<div class="form-group col-sm-6 row" id="saveandcancel" style="display: none;"> 
													<button class="btn btn-sm btn-primary newsubmit" type="button" name="submit" type="button"  onclick="submitCauseList()">Submit</button>
													&nbsp; &nbsp;
													<a href="manage-causelist.php" class="btn btn-danger btn-sm">Cancel</a>
												</div>
											</div>
											<div id="causelit-div" class="table-responsive"></div>
											<div id="causelist-download"></div>
										</div>
										
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include 'footer.php'; ?>
	<script src="scripts/jspdf.min.js"></script>
	<script src="scripts/jspdf.plugin.autotable.min.js"></script>
	<script type="text/javascript">
		$(".add-client-li").addClass('active pcoded-trigger');
	</script>
	<script type="text/javascript" src="scripts/causelist.js"></script>









