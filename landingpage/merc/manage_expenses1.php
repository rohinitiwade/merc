<?php include("header.php");?>
<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<?php include("menu.php");?>
<div class="main-panel">
 <div class="content-wrapper">
  <?php include("slider.php");?>
  <?php if(isset($_POST['submit'])){
  	extract($_POST);
  }?>
  <div class="card">
    <div class="card-body">
      <h4 class="card-header" style="padding-top: 0px">My Expenses</h4>
      <a href="add-expenses.php" class="btn" style="padding: 6px;position: absolute;left: 603px;z-index: 1;background-color:#ECECEC;top: 87px;"><i class="fa fa-plus" style="position: relative;left: -3px;"></i>Add Expenses</a>
      <button class="btn" id="myBtn" data-target="#filter" data-toggle="modal" style="position: absolute;left: 728px;z-index: 1;background-color: #ECECEC;padding: 6px 9px;top: 87px;">Filter</button>
      <button class="btn" id="myBtn" data-target="#export" data-toggle="modal" style="position: absolute;left: 785px;z-index: 1;background-color: #ECECEC;padding: 6px 9px;top: 87px;">Export</button>
      <div class="row" style="margin-top: 25px;">
        <div class="col-12">
          <div class="table-responsive">
            <table id="order-listing" class="table">
             <thead>
               <tr>
                 <th style="background-color: #353eb3; color: white;width: 5px!important;">Id</th>
                 <th style="background-color: #353eb3; color: white;">Case</th>
                 <th style="background-color: #353eb3; color: white;">Date</th>
                 <th style="background-color: #353eb3; color: white;">Particulars</th>
                 <th style="background-color: #353eb3; color: white;width: 100px!important;">Money Spent (INR)</th>
                 <th style="background-color: #353eb3; color: white;width: 80px!important;">Payment Method</th>
                 <th style="background-color: #353eb3; color: white;width: 80px!important;">Member</th>
                 <th style="background-color: #353eb3; color: white;">Image</th>
                 <th style="background-color: #353eb3; color: white;width: 45px!important;">Actions</th>
               </tr>
             </thead>
             <tbody>
              <?php 
              if($caseno!=""){
  	           $add_expenses = mysqli_query($connection, "SELECT * FROM `add_expenses` WHERE `caseno`='".$caseno."' AND `method`='".$payment."' AND `datetime`   BETWEEN '".$from_date."' AND '".$to_date."' ");
              }else{
              $add_expenses = mysqli_query($connection, "SELECT * FROM `add_expenses` where `userid`='".$_SESSION['user_id']."' ORDER BY exp_id DESC");
          }
              $i=1;
              while($manageexp = mysqli_fetch_array($add_expenses)){
                 $addexpdoc = mysqli_query($connection, "SELECT * FROM `addexpdoc` WHERE `exp_id`='".$manageexp['exp_id']."'");
                 $rowexpe = mysqli_fetch_array($addexpdoc);?>
                <tr>
                  <td style="padding: 10px;"><?php echo $i; ?></td>
                  <td style="padding: 10px;"><?php echo $manageexp['caseno'];?></td>
                  <td style="padding: 10px;"><?php echo $newDate = date("d-m-Y", strtotime($manageexp['datetime']));?></td>
                  <td style="padding: 10px;"><?php echo $manageexp['particulars'];?></td>
                  <td style="padding: 10px;"><?php echo $manageexp['moneyspent'];?></td>
                  <td style="padding: 10px;"><?php echo $manageexp['method'];?></td>
                  <td style="padding: 10px;"><?php $member = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='".$manageexp['userid']."'"); $selteam = mysqli_fetch_array($member); echo $selteam['name']; echo "&nbsp;";echo $selteam['last_name'];?></td>
                 <td style="padding: 10px;"><?php  $ext = pathinfo($rowexpe['image'], PATHINFO_EXTENSION); if($ext=='pdf'){?> 
                   <a href="upload_expimage/<?php echo $rowexpe['image'];?>" target="_blank">
                    <img id="" src="images/pdf.png" alt="Snow" width="250"></a><?php }else{?>
                  <img src="upload_expimage/<?php echo $rowexpe['image'];?>" onclick="viewimage(<?php echo $rowexpe['exp_id'];?>)" width="250" data-toggle="modal" data-target="#abcd">
                  <?php } ?>
                    <td>
                      <a href="update_expenses.php?id=<?php echo $manageexp['exp_id'];?>"><i class="fa fa-edit action-css" style="" title="Edit Expenses"></i></a>
                      <a href="delete_exp.php?id=<?php echo $manageexp['exp_id'];?>" onclick="return confirm('Are you sure you want to delete?');"><i class="fa fa-trash action-css" style="" title="Delete"></i></a>
                    </td>
                  </tr> 
                  <?php $i++; } ?>
                </tbody>
              </table>
              <tr>
                <td colspan="10">
                </td>
              </tr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
  </div>
  <script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");;
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<style>
  body {font-family: Arial, Helvetica, sans-serif;}

  #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }

  #myImg:hover {opacity: 0.7;}

  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }

  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }

  label{
    margin-bottom: 0px!important;
    margin-top: 12px;
  }

  .table th{
    white-space: unset;
  }

  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }
</style>
<div class="modal" id="export">
    <form name="myform" method="POST" action="downloadExpenses.php">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-left: -185px;width: 180%;">
                <div class="modal-header" style="background-color: #2d4866;color: white;">
                    <h4>Export</h4>
                    <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-3">
                        <!-- <input type="checkbox" style="margin-right: 5px;" />Member<br> -->
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" checked="true" value="moneyspent" />Money Spent
                    </div>
                      <div class="col-sm-3">
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" checked="true" value="caseno" />Case<br>
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" checked="true" value="method" />Payment Mode
                    </div>
                      <div class="col-sm-3">
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" checked="true" value="particulars" />Particulars<br>
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="notes" />Notes
                      </div>
                      <div class="col-sm-3">
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" checked="true" value="date" />Date<br>
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="datetime" />Entered Date
                      </div>
                  </div><br><br>
                  <div class="row">
                      <h4>Download as:</h4>
                      <input type="radio" name="downloadas" value="pdf" checked="true" style="margin-top: 4px;margin-left: 20px;">PDF
                      <input type="radio" name="downloadas" value="excel" style="margin-top: 4px;margin-left: 10px;">Excel
                      <button style="margin-top: -9px;border: 0px;margin-left: 35px;padding: 5px;background-color: #2d4866;color: white;">Download</button>
                  </div>
                </div>
            </div>
        </div>
      </form>
</div>
<div class="modal" id="filter">
  <div class="modal-dialog">
    <div class="modal-content" style="margin-left: -225px;width: 195%;">
      <div class="modal-header" style="background-color: #2d4866;color: white;">
        <h4>Filter</h4>
        <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
      </div>
      <div class="modal-body">
        <form method="POST">
          <div class="row">
            <div class="col-sm-4">
              <label>Cases</label><br>
              <select class="form-control" name="caseno" id="state" required>
                        <option value="">Please Case No</option>
                        <?php $caseno = mysqli_query($connection, "SELECT * FROM `add_expenses` WHERE `caseno`!=''"); 
                             while($selcases = mysqli_fetch_array($caseno)){?>
                        <option value="<?php echo $selcases['caseno'];?>"><?php echo $selcases['caseno'];?></option>
                        <?php } ?>
                        
                      </select>
              <label>From</label><br>
              <input type="text" id="datepicker" class="form-control" name="from_date" placeholder="Enter From Date">
            </div>
            <div class="col-sm-4">
              <label>Team Member</label><br>
              <select name="team_member" class="form-control">
              	<option value="">--Select Team Member--</option>
              	<?php $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `parent_id`='".$_SESSION['user_id']."'");
              	      while($selteam = mysqli_fetch_array($team)){?>
              	<option value="<?php echo $selteam['reg_id'];?>"><?php echo $selteam['name'];?> <?php echo $selteam['last_name'];?></option>
              	<?php } ?>
              </select>
              <label>To</label><br>
              <input type="text" id="datepicker1" class="form-control" name="to_date" placeholder="Enter to Date">
            </div>
            <div class="col-sm-4">
              <label>Payment Method</label><br>
                    <select class="form-control" name="payment" id="state" required>
                        <option value="">Please select</option>
                        <option value="Bank Transfer">Bank Transfer</option>
                        <option value="Cash">Cash</option>
                        <option value="Cheque">Cheque</option>
                        <option value="Online Payment">Online Payment</option>
                        <option value="Other">Other</option>
                      </select>            </div>
          </div><br><br>
          <div class="row">
            <input type="reset" name="Reset" style="margin-top: -9px;border: 0px;margin-left: 25px;padding: 5px;background-color: #2d4866;color: white;">
            <input type="submit" style="margin-top: -9px;border: 0px;margin-left: 5px;padding: 5px;background-color: #2d4866;color: white;" value="Submit" name="submit">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div id="abcd" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
 
      </div>
      <div class="modal-body" style="padding:0px;">
       <img src="images/female2.jpg" style="width: 100%;
    float: left;">
      </div>
 
    </div>

  </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true
    });
    $( "#datepicker1" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true
    });
  } );
</script>
<script>
  function viewimage(){
    alert("OK");
  }
</script>
<?php include("footer.php");?>