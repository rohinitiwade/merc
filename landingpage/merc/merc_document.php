<?php include("header.php"); ?>
<?php //include("chat-sidebar.php"); ?>
<?php //include("chat-inner.php"); ?> 
<?php include("phpfile/sql_home.php");?>
<style type="text/css">
  .files {
    position: relative;
    width: 100%;
    float: left;
  }
  .files input {
    outline: 2px dashed #92b0b3;
    outline-offset: -10px;
    -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
    transition: outline-offset .15s ease-in-out, background-color .15s linear;
    padding: 120px 0px 85px 35%;
    text-align: center !important;
    margin: 0;
    width: 100% !important;
  }
  .fetchdataloader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 60px;
    /*float: right;*/
    height: 60px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
  }
  .upload_btn{
    width: 14%;
    margin: 0 auto;
  }
  .border-right{
    border-right: 1px solid #c7c5c5;
  }
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php"); ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Add Documents   </h5>
              </div>
              <div class="page-body">

                <div class="card form-group">
                  <div class="card-block">
                    <div class="media">
                      <div class="media-body col-sm-12">
                       <div class="tab-pane" id="documenttab" role="tabpanel" aria-labelledby="documenttab">
                        <form method="post" action="#" id="#">
                         <div class="form-group files">
                          <label>Upload Your Case Papers </label>
                          <?php 
                          $sel = mysqli_query($connection,"SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
                          $sels = mysqli_fetch_array($sel);
                          ?>
                          <!-- <input type="hidden" name="case_id"  id="case_ids" value="<?php echo base64_decode($_GET['caseid']);?>"> -->

                          <input type="hidden" name="case_no"  id="case_no" value="<?php echo $sels['case_no'];?>">
                          <input type="file" class="form-control form-group" multiple="" id="upload_doc"  accept="image/x-png,image/jpeg,application/pdf,application/msword" />
                        </div>
                        <div class=" col-sm-12 row  ">
                          <?php if($_GET['caseid']!=''){?>
                           <input type="hidden" name="case_no"  id="caseid" value="<?php echo base64_decode($_GET['caseid']);?>">
                         <?php } else{?>
                           <div class="col-sm-3 form-group row m-1 border-right">
                             <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp; Case</label>
                             <div class=" col-sm-8">
                               <select class="form-control" id="caseid" name="caseid">
                                <option value=""> Select Case</option>
                                <?php $casedocument = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE ".$cases_document." ORDER BY case_id DESC");
                                while($casedocuments = mysqli_fetch_array($casedocument)){?>
                                 <option value="<?php echo addslashes($casedocuments['case_id']);?>"><?php echo addslashes($casedocuments['case_no']);?>/<?php echo addslashes($casedocuments['case_no_year']);?></option>
                               <?php } ?>
                             </select>
                           </div>
                         </div>
                       <?php } ?>
                       <div class="col-sm-4 form-group row m-1 border-right">
                         <label for="exampleInputUsername1" class="col-sm-5 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp; Document type</label>
                         <div class=" col-sm-7">
                           <select class="form-control" id="doc_type" name="doc_type">
                            <option value="">Please select document type</option>
                            <?php $document = mysqli_query($connection, "SELECT doc_id,documents_type FROM `documents_type` WHERE `organisation_id`='".$_SESSION['organisation_id']."' ORDER BY documents_type ASC");
                            while($seldocmnet = mysqli_fetch_array($document)){?>
                             <option value="<?php echo addslashes($seldocmnet['doc_id']);?>"><?php echo addslashes($seldocmnet['documents_type']);?></option>
                           <?php } ?>
                         </select>
                       </div>
                     </div>


                     <div class="form-group col-sm-5 row" id="areyouappearingas">
                      <label for="exampleInputUsername1" class="col-sm-5 col-form-label">&nbsp;&nbsp;Have you completely uploaded case papers</label>
                      <div class="form-radio col-sm-7">
                        <div class="radio radio-primary radio-inline col-sm-5">
                          <label>
                            <input type="radio" class="chkPassport" name="complete_papers"  id="appearing_radio_one" value="Yes" checked>

                            <i class="helper"></i>Yes
                          </label>
                        </div>
                        <div class="radio radio-primary radio-inline col-sm-4">
                          <label>
                            <input type="radio" class="chkPassport"  name="complete_papers" id="appearing_radio_two" value="No">

                            <i class="helper"></i>No
                          </label>
                        </div>
                      </div>
                    </div>


                  </div>

                  <div class="col-sm-12 upload_btn">
                    <div class="add_casesubmitdiv" style="padding-right: 0; ">
                     <?php if($_GET['caseid']!=''){?>
                       <button class="btn btn-sm btn-primary newsubmit add_casesubmit"  type="button" id="upload_documents" value="Upload" name="Upload" onclick="getUploadedDoc('addcase')">Upload</button> </div>
                     <?php } else{?>
                       <button class="btn btn-sm btn-primary newsubmit add_casesubmit"  type="button" id="upload_documents" value="Upload" name="Upload" onclick="getUploadedDoc('adddoc')">Upload</button>
                     <?php } ?>
                     <?php if(!empty($_GET['caseid'])) {?>
                       <a class="btn btn-danger btn-sm" href="cases-report.php" id="casewise_doc" > Cancel </a>
                     <?php }else{?>
                       <a class="btn btn-danger btn-sm" href="manage-document.php" id="all_doc" > Cancel </a>
                     <?php } ?>
                                                        <!--  <a class="btn btn-danger btn-sm" href="manage-document.php" id="all_doc" style="display: none;"> Cancel </a>
                                                         <a class="btn btn-danger btn-sm" href="cases-report.php" id="casewise_doc" style="display: none;"> Cancel </a> -->


                                                       </div>
                                                   <!-- <hr>
                                                   <div class="row col-sm-12">
                                                      <div class="col-sm-3 form-group">
                                                         <select class="form-control" id="search-doc-filter">
                                                            <option value="">NA</option>
                                                            <option value="caseno">Case No</option>
                                                            <option value="petitioner">Petitioner</option>
                                                            <option value="respondent">Respondent</option>

                                                         </select>
                                                      </div>
                                                      <div class="col-sm-3 form-group" id="search-document-div" style="display: none;">
                                                        <input type="text" class="form-control" id="search-in-document">
                                                     </div>
                                                     <div class="col-sm-3 form-group">
                                                      <button type="button" class="btn btn-warning btn-sm">Search in Document</button>
                                                   </div>
                                                 </div> -->
                                                 <hr>

                                                
                                               </form>
                                              
                                             </div>
                                           </div>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                       <?php include 'footer.php'; ?>
                       <!-- <script type="text/javascript" src="scripts/add_documents.js"></script> -->
                       <script>
                        $("#doc_type").select2();
                        $("#caseid").select2();
// $(document).ready(function(){
//   getUploadedDoc('m');

// })
function getUploadedDoc(type){
  debugger;
  /*$(".tab-pane fade").removeClass("active show");
  $("#documents").addClass('active show');*/
  $(".add_casesubmitdiv").addClass("fetchdataloader");
  $(".add_casesubmit").hide();
  var file = '',doc_type='';



  doc_type = $("#doc_type").val();
  var doc_value = $( "#doc_type option:selected" ).text();
  var caseid = $("#caseid").val();
  var case_no = $("#case_no").val();

  if ($("input[name='complete_papers']").is(":checked")) {
    var uploaded_papers = $("input[name='complete_papers']:checked").val();
  }
  file = document.getElementById("upload_doc").files[0];
  // var case_id = caseid;
  if(isEmpty(file)){
    toastr.error("","Please Select Atleast One Document",{timeout:5000});
    $(".add_casesubmitdiv").removeClass("fetchdataloader");
    $(".add_casesubmit").show();
    return false;
  }else if(isEmpty(caseid)){
   toastr.error("","Please Select Case",{timeout:5000});
   $(".add_casesubmitdiv").removeClass("fetchdataloader");
   $(".add_casesubmit").show();
   return false;
 }else if(isEmpty(doc_type)){
   toastr.error("","Please Select Document Type",{timeout:5000});
   $(".add_casesubmitdiv").removeClass("fetchdataloader");
   $(".add_casesubmit").show();
   return false;
 }

 var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','doc','docx'];
 if ($.inArray($("#upload_doc").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
  toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls' formats are allowed.",{timeout:5000});
  $(".add_casesubmitdiv").removeClass("fetchdataloader");
  $(".add_casesubmit").show();
  return false;
}

var formData = new FormData();
formData.append('file', file);
formData.append('case_no', case_no);
formData.append('case_id',caseid);
formData.append('doc_type',doc_type);
formData.append('doc_value',doc_value);
formData.append('type',type);
formData.append('uploaded_papers',uploaded_papers);
formData.append('flag',type);
var ajaxReq = $.ajax({
  url : host+"/submit_mercdocument.php",
  type : 'POST',
  data : formData,
  cache : false,
  contentType : false,
  processData : false,
  success:function(response){
   $(".add_casesubmitdiv").removeClass("fetchdataloader");
   $(".add_casesubmit").show();
   $("#documentList").html('');
   $('input[type=file]').val('');

   var response_val = JSON.parse(response);
   var tr ='';
   if(type == 'addcase'){
    if(response_val.status == 'success'){
      toastr.success("",'File Uploaded Successfully',{timeout:3000});
       window.location.href='cases-report.php';
     }
     else
      toastr.error("","Error in uploading files",{timeout:5000});
  }else if(type == 'adddoc'){
    if(response_val.status == 'success'){
      toastr.success("",'File Uploaded Successfully',{timeout:3000});
       window.location.href='manage-document.php';
     }
     else
      toastr.error("","Error in uploading files",{timeout:5000});
  }
  $.each(response_val.data,function(i,obj){
    $("#view_case_document").text(obj.doc_count);
    var filesize = bytesToSize(obj.size);
    tr +='<tr>';
    tr +='<td class="case-title"><span id="'+obj.doc_path+'" onclick=viewcases('+obj.doc_id+',this.id) class="viewDoc"><i class="feather icon-file-pdf-o "></i> '+obj.file_name+'</span></td>';
    tr +='<td class="case-title">'+filesize +'</td>';
    tr +='<td class="case-title">'+obj.type+'</td>';
    tr +='<td class="case-title">'+obj.uploaded_by+'</td>';
    tr +='<td class="case-title">'+obj.date_time+'</td>';
    tr+='<td><a class="btn btn-outline-warning doc_action btn-mini form-group" style="margin-bottom:0px;"  href="'+host+'/editMercDocument.php?id='+obj.encrypted_case_id+'" title="Edit"><i class="feather icon-edit"></i></a>';
    if(obj.title == 'doc'){
     tr+= '<a class="btn btn-outline-warning doc_action btn-mini form-group" href="'+obj.doc_path+'" title="Download" style="margin-bottom:0px;" download><i class="feather icon-eye"></i></a>'; 
   }else{
    tr+= ' <button title="Download" type="button" class="doc_action viewBtn btn btn-outline-info btn-mini form-group" ><a href="'+getDocUrl(obj.doc_path)+'" class="viewDoc" target="_blank" download><i class="feather icon-download"></i></a></button>';
  }
  tr+= ' <button type="button" onclick="deleteDoc('+obj.doc_id+')" title="Delete" class="doc_action btn btn-outline-danger btn-mini form-group"><i class="feather icon-trash"></i></button></td>';
});

  if(tr !=''){
    var table = '<table class="table table-bordered" id="documentList-table">';
    table +='<thead><tr><th>Title</th><th>Size</th><th>Type</th><th>Uploaded By</th><th>Uploaded Date</th><th class="action_th">Action</th></tr></thead><tbody>';
    table+= tr +'</tbody></table>';
  }
  $("#documentList").html(table);
  $("#documentList-table").dataTable();

},error:function(){
  toastr.error("",'Error in uploading document',{timeout:5000});
  $(".add_casesubmitdiv").removeClass("fetchdataloader");
  $(".add_casesubmit").show();
}
});
}

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function getDocUrl(doc_name){
  if(!isEmpty(doc_name)){
         // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
      // $('#pdfUrl').attr('href',host+"/casepdf"+response.pdfFile); 
      return host+'/upload_document/'+doc_name;
    }
    else
     return 'images/business+costume+male+man+office+user+icon-1320196264882354682.png';
 }



 function deleteDoc(doc_id){
  var yes = confirm("Are you sure you want to delete document?");
  if(yes){
    $.ajax({
      url : host+"/delete_view_document.php",
      type:'POST',
      data : {
        doc_id : doc_id
      },
      dataType: 'json',
      cache : false,
      success:function(response){
        // console.log(response);
        if(response.status == 'success'){
          getUploadedDoc('m');
          toastr.success("","Successfully deleted the document",{timeout:5000});
        }
        else
          toastr.error("","Error in deleting the document",{timeout:5000});
      },
      error:function(){
        toastr.error("","Error in deleting the document",{timeout:5000});
      }
    });
  }else
  return false
}
</script>







