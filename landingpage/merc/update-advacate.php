<link rel="stylesheet" type="text/css" href="styles/jquery.steps.css">
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<body>
<?php 
  $editadvocates = mysqli_query($connection,"SELECT * FROM `advocate` WHERE `id`='".base64_decode($_GET['id'])."'");
  $seladvocates = mysqli_fetch_array($editadvocates);

  $todo_cases = mysqli_query($connection,"SELECT DISTINCT`todo_id` FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' ORDER BY todo_id DESC");
  $cnt_todocases = mysqli_num_rows($todo_cases);?>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
  <?php include("menu.php") ?>
  <div class="pcoded-content">
  <div class="pcoded-inner-content">
  <div class="main-body">
    <div class="page-wrapper">
        <div class="page-header m-b-10"><h5 class="card-title">Edit Advocate &nbsp; &nbsp;<a href="manage-client.php" class="btn-warning btn-sm">Manage Advocates</a></h5>
       <div class="page-body">
        <div class="form-group">
          <div class="card-block col-sm-12">
          <div id="wizard">
            <section>
            <form class="wizard-form" id="example-advanced-form" method="POST" action="edit_advocate.php">
            <h3> Personal Details </h3>
            <fieldset>
            <div class="form-group row">
            <label for="exampleInputUsername2" class="col-sm-2 col-form-label"><b>Full Name <span class="asterisk">*</span>&nbsp;: </b></label>
            <div class="col-sm-4">
            <input type="hidden" value="<?php echo $seladvocates['id'];?>" name="advId">
            <input type="text" class="form-control" id="full_name" placeholder="Full Name" name="full_name" required="" value="<?php echo $seladvocates['full_name'];?>"></div>
              <label for="exampleInputEmail2" class="col-sm-2 col-form-label"><b>Email Address:</b></label>
            <div class="col-sm-4">
            <input type="email" class="form-control" id="email" placeholder="Email Address" name="email" value="<?php echo $seladvocates['email'];?>"></div></div>
              <div class="form-group row">
            <label for="exampleInputMobile" class="col-sm-2 col-form-label"><b>Phone Number:</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="mobile" placeholder="Phone Number" name="mobile" maxlength="10" minlength="10" onkeypress="return onlyNumberAllowed(event)" value="<?php echo $seladvocates['mobile'];?>"></div>  
            <label for="exampleInputPassword2" class="col-sm-2 col-form-label"><b>Age</b></label>
            <div class="col-sm-4"><input type="text" class="form-control" id="age" placeholder="Age" name="age" maxlength="2" minlength="2" onkeypress="return onlyNumberAllowed(event)" value="<?php echo $seladvocates['age'];?>"></div></div>
            <div class="form-group row">
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Father's Name:</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="father_name" placeholder="Father's Name" name="father_name" value="<?php echo $seladvocates['father_name'];?>"></div>
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Company Name:</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="company_name" placeholder="Company Name" name="company_name" value="<?php echo $seladvocates['company_name'];?>"></div>
            </div>
              <div class="form-group row">
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Website:</b></label>
                <div class="col-sm-4">
            <input type="text" class="form-control" id="website" placeholder="Website" name="website" value="<?php echo $seladvocates['website'];?>"></div>
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>TIN#:</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="tin" placeholder="TIN" name="tin" value="<?php echo $seladvocates['tin'];?>"></div>
            </div>
            <div class="form-group row">
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>GST Identification Number (GSTIN):</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="gst" placeholder="GST Identification Number (GSTIN)" name="gst" maxlength="15" value="<?php echo $seladvocates['gst'];?>">
            </div>
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Permanent Account Number (PAN):</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="pan_no" placeholder="Permanent Account Number (PAN)" name="pan_no" value="<?php echo $seladvocates['pan_no'];?>"> 
            </div>
            </div>
            <div class="form-group row">
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Hourly Rate (INR):</b></label>
            <div class="col-sm-10">
            <input type="text" class="form-control" id="hourly_rate" placeholder="Hourly Rate (INR)" name="hourly_rate" value="<?php echo $seladvocates['hourly_rate'];?>">
            </div>
            </div>
        </fieldset>
            <h3> Office Address </h3>
        <fieldset>
            <div class="form-group row">
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 1:</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="office_line_1" placeholder="Address Line 1" name="office_line_1" value="<?php echo $seladvocates['office_line_1'];?>">
            </div>
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 2:</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="office_line_2" placeholder="Address Line 2" name="office_line_2" value="<?php echo $seladvocates['office_line_2'];?>">
            </div>
            </div>
            <div class="form-group row">
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Country:</b></label>
            <div class="col-sm-4">
        <select name="country_office" class="form-control" id="country">
            <option value="<?php echo $seladvocates['country_office'];?>"><?php if($seladvocates['country_office']!=""){ echo $seladvocates['country_office']; }else{?>Select Country<?php } ?></option>
            <option value="India">India</option>
        </select>
                      </div>
          <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>State:</b></label>
          <div class="col-sm-4">
            <select name="office_state" class="form-control" id="state">
              <option value="<?php echo $seladvocates['office_state'];?>"><?php if($seladvocates['office_state']!=""){  $states = mysqli_query($connection,"SELECT `name` FROM `geo_locations` WHERE `id`='".$seladvocates['office_state']."'"); $selsatte = mysqli_fetch_array($states); echo $selsatte['name']; }else{?>Select State<?php } ?></option>
              <?php $geolocation = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `parent_id`=0");
              while($sellocation = mysqli_fetch_array($geolocation)){ ?>
                <option value="<?php echo $sellocation['id'];?>"><?php echo $sellocation['name'];?></option>
              <?php } ?>
            </select>
          </div>
          </div>
          <div class="form-group row">
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>City:</b></label>
            <div class="col-sm-4">
              <select id="cityadv" name="office_city" class="form-control">
                <option value="<?php echo $_POST['office_city'];?>"><?php if($seladvocates['office_city']!=""){ $city = mysqli_query($connection,"SELECT `name` FROM `geo_locations` WHERE `id`='".$seladvocates['office_city']."'"); $selcity = mysqli_fetch_array($city); echo $selcity['name']; }else{?>Select State first<?php } ?></option>
              </select>
            </div>
            <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Zip/Postal Code:</b></label>
            <div class="col-sm-4">
            <input type="text" class="form-control" id="office_zip" placeholder="Zip/Postal Code" name="office_zip" value="<?php echo $seladvocates['office_zip'];?>"> 
            </div>
          </div>
          </fieldset>
         <h3> Home Address </h3>
     <fieldset>
      <div class="form-group row">
        <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 1:</b></label>
        <div class="col-sm-4">
          <input type="text" class="form-control" id="home_line_1" placeholder="Address Line 1" name="home_line_1" value="<?php echo $seladvocates['home_line_1'];?>">
        </div>
        <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 2:</b></label>
        <div class="col-sm-4">
          <input type="text" class="form-control" id="home_line_2" placeholder="Address Line 2" name="home_line_2" value="<?php echo $seladvocates['home_line_2'];?>">
        </div>
      </div>
      <div class="form-group row">
        <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Country:</b></label>
        <div class="col-sm-4">
          <select name="home_country" class="form-control">
            <option value="<?php echo $seladvocates['home_country'];?>"><?php if($seladvocates['home_country']!=""){ echo $seladvocates['home_country']; }else{?>Select Country<?php } ?></option>
            <option value="India">India</option>
          </select>
        </div>
        <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>State:</b></label>
        <div class="col-sm-4">
          <select name="home_state" class="form-control" id="state1">
              <option value="<?php echo $seladvocates['home_state'];?>"><?php if($seladvocates['home_state']!=""){  $states1 = mysqli_query($connection,"SELECT `name` FROM `geo_locations` WHERE `id`='".$seladvocates['home_state']."'"); $selsatte1 = mysqli_fetch_array($states1); echo $selsatte1['name']; }else{?>Select State<?php } ?></option>
              <?php $geolocation = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `parent_id`=0");
              while($sellocation = mysqli_fetch_array($geolocation)){ ?>
                <option value="<?php echo $sellocation['id'];?>"><?php echo $sellocation['name'];?></option>
              <?php } ?>
            </select>
        
        </div>
      </div>
      <div class="form-group row">
        <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>City:</b></label>
        <div class="col-sm-4">
          <select name="home_city" class="form-control" id="cityadv1">
                <option value="<?php echo $_POST['office_city'];?>"><?php if($seladvocates['home_city']!=""){  $city1 = mysqli_query($connection,"SELECT `name` FROM `geo_locations` WHERE `id`='".$seladvocates['home_city']."'"); $selcity1 = mysqli_fetch_array($city1); echo $selcity1['name']; }else{?>Select State<?php } ?></option>
              </select>
        </div>
        <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Zip/Postal Code:</b></label>
        <div class="col-sm-4">
          <input type="text" class="form-control" id="home_zip" placeholder="Zip/Postal Code" name="home_zip" value="<?php echo $seladvocates['home_zip'];?>">
        </div>
      </div>
    </fieldset>
        <h3> Point of Contacts </h3>
    <fieldset>
      <table id="myTable" class=" table order-list advocatelist" width="100">
        <thead>
          <tr>
            <td>Full Name</td>
            <td>Email Address</td>
            <td>Phone Number</td>
            <td>Designation</td>
            <td>Action</td>
          </tr>
        </thead>
        <tbody>
          <?php  $sql = mysqli_query($connection, "SELECT * FROM `contact_point_adv` WHERE `adv_id`='".base64_decode($_GET['id'])."'");
          $i=1;
                while($rows = mysqli_fetch_array($sql)){?>
          <tr>
            <td><input type="text" name="fullname[]" class="form-control" value="<?php echo $rows['full_name'];?>" /></td>
            <td><input type="mail" name="email_id[]"  class="form-control" value="<?php echo $rows['email_id'];?>" /></td>
            <td><input type="text" name="contact_no[]"  class="form-control" value="<?php echo $rows['contact_no'];?>" /></td>
            <td><input type="text" name="designation[]"  class="form-control" value="<?php echo $rows['designation'];?>" /></td>
            <?php  if($i==1){ ?>
            <td><a class="deleteRow"></a>
              <button type="button" class="btn btn-primary btn-sm add-opponent add-more-button" id="addrow"> Add More </button>
            </td>
            <?php }?>
            <?php  if($i>1){ ?>
            <td><a class="deleteRow"></a>
              <button type="button" class=" ibtnDel btn btn-sm btn-danger deleteadv">Delete</button>
            </td>
            <?php }?>
          </tr>

          <?php $i++; } ?>
        </tbody>
      </table>
    </fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php include 'footer.php'; ?>
<script type="text/javascript">
  $(".add-client-li").addClass('active pcoded-trigger');
</script>
<script src="scripts/jquery.steps.js" type="text/javascript"></script>
<script src="scripts/jquery.validate.js" type="text/javascript"></script>
<script src="scripts/form-wizard.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="scripts/add-advocate.js"></script> -->

<script>
$(document).ready(function(){
var counter = 1;

  $("#addrow").on("click", function () {
    var newRow = $("<tr>");
    var cols = "";

    cols += '<td><input type="text" class="form-control" name="fullname' + counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" name="email_id' + counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" name="contact_no' + counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" name="designation' + counter + '"/></td>';

    cols += '<td class="advdeletemiddle"><button type="button" class=" ibtnDel btn btn-sm btn-danger deleteadv">Delete</button></td>';
    newRow.append(cols);
    $("table.order-list").append(newRow);
    counter++;
   
        //alert(counter);
      });



  $("table.order-list").on("click", ".ibtnDel", function (event) {
    $(this).closest("tr").remove();       
    counter -= 1
  });



});
</script>
<script>
$(document).ready(function(){
    $(document).on('change','#country', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate.php',
                data:'country_id='+countryID,
                success:function(html){
                    $('#state').html(html);
                     $("#state").select2();
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
           
        }
    });
    $(document).on('change','#state', function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate.php',
                data:'state_id='+stateID,
                success:function(html){
                    $('#cityadv').html(html);
                    $("#cityadv").select2();
                }
            }); 
        }else{
        }
    });



    $(document).on('change','#country1', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate1.php',
                data:'country_id='+countryID,
                success:function(html){
                     $('#state1').html(html);
                     $("#state1").select2();
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
        }
    });


$(document).on('change','#state1', function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate1.php',
                data:'state_id='+stateID,
                success:function(html){
                    $('#cityadv1').html(html);
                    $("#cityadv1").select2();
                }
            }); 
        }else{
        }
    });
  
});
</script>





