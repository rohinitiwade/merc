<?php

//Include the database configuration file
include 'includes/dbconnect.php';

if(!empty($_POST["state_id"])){
    //Fetch all state data
     $query = mysqli_query($connection, "SELECT * FROM `appearing_model` WHERE `app_id`='".$_POST['state_id']."'");
    //Count total number of rows
    $rowCount = mysqli_num_rows($query);
    
    //State option list
    if($rowCount > 0){
        while($row = mysqli_fetch_assoc($query)){  ?>

          <label for="exampleInputUsername1" class="col-sm-4 col-form-label">Are you appearing as?</label>

<div class="form-radio col">
      <div class="radio radio-primary radio-inline">
        <label id="appearing_radio_one_label">
         <input type="radio" class="chkPassport" name="appearing_radio" id="appearing_radio_one" onclick="getAppearingInput()" value="<?php echo $row['data1']; ?>" checked>

          <i class="helper"></i><?php echo $row['data1']; ?>
        </label>
      </div>
      <div class="radio radio-primary radio-inline">
        <label id="appearing_radio_two_label">
          <input type="radio" class="chkPassport"  name="appearing_radio" onclick="getAppearingInput()" id="appearing_radio_two" value="<?php echo $row['data2']; ?>">

          <i class="helper"></i><?php echo $row['data2']; ?>
        </label>
      </div>
    </div>

        <?php
            //echo '<option value="'.$row['app_id'].'">'.$row['appearing_modal'].'</option>';
        }
    }else{
        echo '<option value="">Appearing modal not available</option>';
    }
}
?>