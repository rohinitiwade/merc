<?php include("header.php");?>
<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/law/admin/css/vertical-layout-light/casediary.css">

<?php include("menu.php");?>
<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <?php include("slider.php");?>
    <div class="card law-diary">
      <div class="card-body">
        <h4 class="card-header" style="">My Law Diary 
          <div class="card_header_btn">
            <button class="btn btn-sm btn-secondary" id="myBtn" data-target="#filter" data-toggle="modal" style="">Filter</button>
            <button class="btn btn-sm btn-secondary" id="myBtn" data-target="#export" data-toggle="modal" style="">Export</button>
          </div>
        </h4>

        <div class="form-row">
          <div class="col-sm-2">
            <label for="exampleInputUsername1" class=" col-form-label">
              <b>From</b></label>
              <input type="text" id="datepicker" class="form-control" placeholder="From Date">
            </div>
            <div class="col-sm-2">
              <label for="exampleInputUsername1" class=" col-form-label">
                <b>To</b></label>
                <input type="text" id="datepicker1" class="form-control" placeholder="To Date">
              </div>
              <div class="col-sm-3 btn-law-diary" style="">
                <button class="btn btn-secondary" id="myBtn" style="">Submit</button>
                
             

              </div>
            </div>           

            <!-- <button class="btn" id="myBtn" data-target="#filter" data-toggle="modal" style="position: absolute;left: 658px;z-index: 1;background-color: #2d4866;padding: 6px 9px;top: 152px;color: white;">Submit</button> -->
            <div class="row table-div" style="">
              <div class="col-12">
                <div class="table-responsive">
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                        <th style="background-color: #353eb3; color: white;">#</th>
                        <th style="background-color: #353eb3; color: white;">Court</th>
                        <th style="background-color: #353eb3; color: white;">Case</th>
                        <th style="background-color: #353eb3; color: white;">Title</th>
                        <th style="background-color: #353eb3; color: white;">Team Member(s)</th>
                        <th style="background-color: #353eb3; color: white;">Hearing Date</th>
                        <th style="background-color: #353eb3; color: white;">Last Hearing Date</th>
                        <th style="background-color: #353eb3; color: white;">Stage</th>
                        <th style="background-color: #353eb3; color: white;">Case Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $diary = mysqli_query($connection, "SELECT * FROM `reg_cases` LIMIT 5");
                      $i=1;
                      while($diarysql = mysqli_fetch_array($diary)){?>
                       



                       <tr>
                        <td style="padding: 10px;"><?php echo $i; ?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['court_name'];?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['case_no'];?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['title'];?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['team_member'];?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['hearing_date'];?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['last_hearing_date'];?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['stage'];?></td>
                        <td style="padding: 10px;"><?php echo $diarysql['case_description'];?></td>
                      </tr>
                      <?php $i++; } ?>
                    </tbody>
                  </table>
                  <tr>
                    <td colspan="10">
                    </td>
                  </tr>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal" id="filter">
        <div class="modal-dialog">
          <div class="modal-content" style="">
            <div class="modal-header" style="">
              <h4>Filter</h4>
              <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-4">
                  <label>Title</label><br>
                  <input type="text" class="form-control" placeholder="Enter Title">
                  <label>Case#</label><br>
                  <input type="text" class="form-control" placeholder="Enter Case">
                  <label>Case Year</label><br>
                  <input type="text" class="form-control" placeholder="put case year dropdown">
                  <label>Before Hon'ble Judge/s</label><br>
                  <input type="text" class="form-control" placeholder="">
                  <label>Section/Category</label><br>
                  <input type="text" class="form-control" placeholder="Enter Section/Category">
                  <label>Posted For</label><br>
                  <input type="text" class="form-control" placeholder="">
                  <label>Opponents</label><br>
                  <input type="text" class="form-control" placeholder="put opponent dropdown">
                </div>
                <div class="col-sm-4">
                  <label>Advocate</label><br>
                  <input type="text" class="form-control" placeholder="Enter Advocate Name">
                  <label>Hearing Date</label><br>
                  <input type="text" class="form-control" id="datepicker2" placeholder="Choose Date">
                  <label>Case Filing Date Form</label><br>
                  <input type="text" class="form-control" id="datepicker3" placeholder="Choose Date">
                  <label>Referred By</label><br>
                  <input type="text" class="form-control" placeholder="Enter Referred By">
                  <label>Priority</label><br>
                  <input type="text" class="form-control" placeholder="Put dropdown">
                  <label>Action Taken</label><br>
                  <input type="text" class="form-control" placeholder="">
                  <label>CNR#</label><br>
                  <input type="text" class="form-control" placeholder="Enter CNR#">
                </div>
                <div class="col-sm-4">
                  <label>Team Member</label><br>
                  <input type="text" class="form-control" placeholder="Enter Team Member">
                  <label>Hearing Date To</label><br>
                  <input type="text" class="form-control" id="datepicker4" placeholder="Choose Date">
                  <label>Case Filing Date To</label><br>
                  <input type="text" class="form-control" id="datepicker5" placeholder="Choose Date">
                  <label>File#</label><br>
                  <input type="text" class="form-control" placeholder="Enter File#">
                  <label>Stage</label><br>
                  <input type="text" class="form-control" placeholder="Enter Stage">
                  <label>Sessions</label><br>
                  <input type="text" class="form-control" placeholder="Put dropdown">
                  <label>Court</label><br>
                  <input type="text" class="form-control" placeholder="Put dropdown">
                </div>
              </div><br><br>
              <div class="row">
                <button class="reset-btn" style="">Reset</button>
                <button class="submit-btn" style="">Submit</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
    <form name="myform" method="POST" action="downloadCaseDiary.php">
          <div class="modal" id="export">
              <div class="modal-dialog">
                <div class="modal-content" style="margin-left: -185px;width: 180%;">
                 <div class="modal-header" style="background-color: #2d4866;color: white;">
                  <h4>Export</h4>
                  <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row">
                
                   <div class="col-sm-3">
                     <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="court_name" />Court<br>
                     <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="hearing_date" />Hearing Date<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="stage" />Stage<br>
                   </div>
                   <div class="col-sm-3">
                     <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="case_no" />Case<br>
                     <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="title" />Title<br>
                   </div>
                   <div class="col-sm-3">
                     <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="last_hearing_date" />Last Hearing Date<br>
                     <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="case_description" />Case Discription<br>
                   </div>
                 </div><br><br>
                 <div class="row">
                  <h4>Download as:</h4>
                  <input type="radio" name="downloadas" value="pdf" style="margin-top: 4px;margin-left: 20px;">PDF
                  <input type="radio" name="downloadas" value="excel" style="margin-top: 4px;margin-left: 10px;">Excel
                  <button style="margin-top: -9px;border: 0px;margin-left: 35px;padding: 5px;background-color: #2d4866;color: white;">Download</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>

  <?php include("footer.php");?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script type="text/javascript">
    $("#checkAll").click(function () {
      $(".check").prop('checked', $(this).prop('checked'));
    });
  </script>
<script>
  alert('dilip');
  function encryptData(data){
       var key = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef');
        var iv  = CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210');
//        var key = CryptoJS.enc.Hex.parse('5678943210fdecba5678943210fdecba');
//        var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');
        var encrypted = CryptoJS.AES.encrypt((data), key, { iv: iv });
        var encrypted_data = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
            return encrypted_data;
}

$.ajax({
    url:"http://203.192.219.179:8081/LegalResearch/", //the page containing php script
    type: "GET",
    data:{cino:encryptData('MHOS120000112019'),courtType:'HC'} //request type
    success:function(result){
    alert(result);
    }
  });

</script>
<script type='text/javascript'>
function encryptData(data){
       var key = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef');
        var iv  = CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210');
//        var key = CryptoJS.enc.Hex.parse('5678943210fdecba5678943210fdecba');
//        var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');
        var encrypted = CryptoJS.AES.encrypt((data), key, { iv: iv });
        var encrypted_data = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
            return encrypted_data;
}

  alert('dilipxxx');
$.ajax({
   type: "GET",
   url: "http://203.192.219.179:8081/LegalResearch/search_by_cnr",
   data: {cino:encryptData('MHOS120000112019'),courtType:'HC'},
   success: function(msg){
     alert( "Data Saved: " + msg );
   }
 });
</script>


  <script type="text/javascript">
  	

    $(document).ready(function() {
      $( "#datepicker" ).datepicker({
        dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
      });
      $( "#datepicker1" ).datepicker({
        dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
      });
      $( "#datepicker2" ).datepicker({
        dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
      });
      $( "#datepicker3" ).datepicker({
        dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
      });
      $( "#datepicker4" ).datepicker({
        dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
      });
      $( "#datepicker5" ).datepicker({
        dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
      });
    });
  </script>
  <style type="text/css">
  .table td{
    white-space: normal;
  }
</style>
  <script src="js/aes.js"></script>
 <script src="js/pbkdf2.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
