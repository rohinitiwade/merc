<?php
include('includes/dbconnect.php');
session_start();
header('Access-Control-Allow-Origin: *');
$edit      = $_POST['edit_data'];
$data      = json_decode($edit, true);
// print_r($data);
$arr       = array();
$date      = date('Y-m-d H:i:s');
$view_case = mysqli_query($connection, "SELECT case_id,type_of_litigation,user_id,court_name,case_no,risk_category,case_no_year,case_title,case_description,date_of_filling FROM reg_cases  WHERE case_id='" . $_POST['case_id'] . "'");
while ($view_cases = mysqli_fetch_assoc($view_case)) {
  
    $edit_data_object                                  = new stdClass();
    $edit_data_object->case_id                         = TRIM($view_cases['case_id']);
     $edit_data_object->type_of_litigation             = TRIM($view_cases['type_of_litigation']);
    $edit_data_object->user_id                         = TRIM($view_cases['user_id']);
    $edit_data_object->court_name                      = TRIM($view_cases['court_name']);
    $edit_data_object->case_no                         = TRIM($view_cases['case_no']);
     $edit_data_object->risk_category                         = TRIM($view_cases['risk_category']);
    $edit_data_object->case_no_year                    = TRIM($view_cases['case_no_year']);
    $edit_data_object->case_title                      = TRIM($view_cases['case_title']);
    $edit_data_object->case_description                = TRIM($view_cases['case_description']);
   

    $edit_data_object->date_of_filling                 = TRIM($view_cases['date_of_filling']);
     $case_details[]                                    = $edit_data_object;
    //respondent
    $respondent = mysqli_query($connection, "SELECT `pet_res_id`,`case_id`,`court_id`,`user_id`,`full_name`,`email`,`mobile`,`date_time`,`case_no` FROM `petitioner_respondent` WHERE `case_id`='" . $_POST['case_id'] . "'  AND  `full_name`!='' AND `designation`='Respondent' ORDER BY pet_res_id ASC");
    while ($respondents = mysqli_fetch_array($respondent)) {
        $respondent_data[] = array(
            'res_id' => $respondents['pet_res_id'],
            'case_id' => $respondents['case_id'],
            'court_id' => $respondents['court_id'],
            'user_id' => $respondents['user_id'],
            'respondent_name' => TRIM($respondents['full_name']),
            'respondent_email' => TRIM($respondents['email']),
            'respondent_mobile' => TRIM($respondents['mobile']),
            'date_time' => $respondents['date_time'],
            'case_no' => $respondents['case_no']
        );
    }
    //petitioner
    $petitioner = mysqli_query($connection, "SELECT `pet_res_id`,`case_id`,`court_id`,`user_id`,`full_name`,`email`,`mobile`,`date_time`,`case_no` FROM `petitioner_respondent` WHERE `case_id`='" . $_POST['case_id'] . "'   AND  `full_name`!='' AND `designation`='Petitioner' ORDER BY pet_res_id ASC");
    while ($petitioners = mysqli_fetch_array($petitioner)) {
        $petitioners_data[] = array(
            'pet_id' => $petitioners['pet_res_id'],
            'case_id' => $petitioners['case_id'],
            'court_id' => $petitioners['court_id'],
            'user_id' => $petitioners['user_id'],
            'petitioner_name' => TRIM($petitioners['full_name']),
            'petitioner_email' => TRIM($petitioners['email']),
            'petitioner_mobile' => TRIM($petitioners['mobile']),
            'date_time' => $petitioners['date_time'],
            'case_no' => $petitioners['case_no']
        );
    }
     
    
    $arr = array(
        'data' => $case_details,
        'respondent_data' => $respondent_data,
        'petitioners_data' => $petitioners_data,
        'res_advocate_data' => $res_advs_data,
        'pet_advocate_data' => $pet_advs_data,
        'appearing_modal' => $appear['appearing_modal']
    );
}
echo json_encode($arr, true);

?>