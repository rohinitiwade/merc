<?php include("header.php"); //include("chat-sidebar.php"); include("chat-inner.php"); 
include("phpfile/sql_home.php"); ?>
<body>
  <style>
   /*th {
    background-color: #01A9AC;color:white;
  }*/

   #case-list {
    float: left;
    list-style: none;
    /* margin-top: 4px; */
    padding: 0;
    width: 230px;
    position: absolute;
    max-height: 275px;
    overflow: auto;
    z-index: 9999;
    margin-top: -16px;
  }
  #case-list li {
    padding: 1px 8px;
    background: #ccc;
    color: #2d4866;
    border-bottom: #bbb9b9 1px solid;
    width: 100%;
    float: left;
    line-height: 1.8;
    font-weight: bold;
    font-size: 14px;
    cursor: pointer;
  }
</style>
<!-- <link rel="stylesheet" href="styles/case-report.css"> -->
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header m-b-10">

            <h5 class="card-title">Documents (<span id="documentCount"></span>) 
            </h5>
          </div>


          <form method="POST">
           <div class="page-body">
            <div class="card form-group">                 
             <div class="card-block">
              <div class="form-group row client_topbuttons">
                      <!-- <div class="col-sm-2 form-group" >
                        <label><b style="color: green; font-size: 14px;" >Search Document</b></label>
                      </div> -->
                      <!-- <div class="col-sm-2 form-group">
                        <select class="form-control" id="search_document" name="search_document">
                          <option value="">All</option>
                          <option value="caseno">Case No</option>
                          <option value="petitioner">Petitioner</option>
                          <option value="respondent">Respondent</option>
                        </select>
                      </div> -->
                      <!-- <div class="col-sm-3 form-group seacrhoption caseno" style="display: none;">
                        <input type="text" class="form-control" placeholder="Enter Case Number" id="casenosearch" name="casenosearch">
                      </div>
                      <div class="col-sm-3 form-group seacrhoption petitioner" style="display: none;">
                        <input type="text" class="form-control" placeholder="Enter Petitioner" id="petitionersearch" name="petitionersearch">
                      </div>
                      <div class="col-sm-3 form-group seacrhoption respondent" style="display: none;">
                        <input type="text" class="form-control" placeholder="Enter Respondent" id="respondentsearch" name="respondentsearch">
                      </div>
                      <div class="col-sm-2 form-group" >
                        <input type="submit" name="advocatesearch" value="Search" class="btn btn-primary btn-sm">
                      </div> -->
                    <!--  <div class="col-sm-3 form-group seacrhoption respondent" >
                        <input type="text" class="form-control" placeholder="Enter Search Keyword" id="" name="serchkeyword">
                      </div>
                      <div class="col-sm-2 form-group" >
                        <input type="submit" name="advocatesearch" value="Search" class="btn btn-primary btn-sm">
                      </div> -->
                      <!-- </form> -->
                                 <!-- <div class="col-sm-2 form-group" >
                        <label><b style="color: green; font-size: 14px;" ><a href="manage-document">All Document</a></b></label>
                      </div> -->
                      <form name="zips" action="createzip.php" method="post" class="col-sm-12">
                        <div class="col-sm-12 form-group" style="text-align: right;">
                          <!-- <button type="button" class="btn btn-warning btn-sm">Search in Document</button> -->
                          <a href="manage-document.php" class="btn btn-sm btn-primary form-group add-documents">All Document</a>
                          <a href="merc_document.php" class="btn btn-sm btn-warning add-documents form-group">
                           <i class="feather icon-plus" style=""></i>Add Documents</a>
                           <button class="btn btn-info btn-sm" type="button" id="myBtn-filter" data-target="#filter" data-toggle="modal" style="margin-bottom: 12px;">Filter</button>
                           <!-- <input type="submit" id="submit"  name="createzip" value="Download Zip" class="btn btn-primary btn-sm form-group" > -->
                         </div>
                        <!--  <?php  if($_GET['status']=='remove'){?>
                          <div class="col-sm-5 form-group" >
                           <div class="alert warning">
                             <span class="closebtn">&times;</span>  
                             <strong><a href="undudocument.php?id=<?php echo $_GET['docs']; ?>">Undo!</a></strong> Indicates a warning that might need attention.
                           </div>
                         </div>
                       <?php } ?> -->


               <!-- <div class="col-sm-2 form-group" >
                <a href="download-zip" class="btn-primary btn-sm">Download Zip</a>
              </div>  -->
              <!-- <div class="col-sm-2 form-group" >
               
              </div> -->

              <div class="col-sm-4 form-group" >
              </div>
              <div class="col-sm-8 form-group" id="showfilter" style="display: none;">
               
               <div class="modal-content">
                <div class="modal-header">
                  <h5 class="card-title">Filter</h5>
                  <button type="button" class="close" data-dismiss="modal" onclick="closeFilter();">&times;</button>
                </div>
                <div class="modal-body"> <div class="row">
                   <form method="POST" autocomplete="off">
                  <div class="col-sm-4">
                   <label>Case No.</label>
                   <input type="text" class="form-control form-group" placeholder="Enter Case" name="case_no" id="case_no">
                   <div id="managecase-box" class="managecase-box animated fadeInLeft"></div>
                 </div>

                 <div class="col-sm-4"> 
                   <label>Uploaded Date </label>
                   <input type="text" class="form-control form-group" id="uploaded_date" name="uploaded_date" placeholder="Hearing Date ">
                 </div>

                 <div class="col-sm-4">
                   <label>Court/ Forum</label>
                   <select name="court_name" class="form-control" id="court_name"  >
                     <option value=""> Please select</option>
                            <?php $courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `organisation_id`='".$_SESSION['organisation_id']."' ORDER BY court_id ASC");
                            while($selcourtlist = mysqli_fetch_array($courtname)){?>
                              <option value="<?php echo $selcourtlist['court_name'];?>"><?php echo $selcourtlist['court_name'];?></option>
                            <?php } ?>

</select>
</div>
</form>

<div class="col-sm-12" style="text-align: right;">
  <input type="button" name="fillter" class="btn-info btn-sm btn" style="" value="Submit" onclick="filterCase(1,'filter')" >
  <!-- <button class="btn-danger btn btn-sm" type="button" onclick="resetFilter()">Reset -->
  <!-- </button> -->
  <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal" onclick="closeFilter();">Close</button>
</div>
</div>
</div>

</div>
</div>



<div class="table-responsive">
  <div id="show_data"></div>
  <div class="pagination"></div>
</form>

</div>
<div class="modal" id="editteam">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="edit_team">

      <!-- Modal Header -->
         <!--  <div class="modal-header">
            <h4 class="modal-title">Edit Team Member Details</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div> -->

          <!-- Modal body -->
         <!--  <div class="modal-body">
            <div></div>
          </div> -->

          <!-- Modal footer -->
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
          </div>
        -->
      </div>
    </div>
  </div>
  <div class="modal" id="export">
    <div class="modal-dialog modal-lg">

      <div class="modal-content" >
        <div class="modal-header">
          <h4>Export</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row form-group col-sm-12">
            <input type="hidden" name="checkboxvar[]" value="image">
            <input type="hidden" name="checkboxvar[]" value="size">
            <!-- <input type="hidden" name="checkboxvar[]" value="case_no"> -->
            <!-- <input type="hidden" name="checkboxvar[]" value="case_no_year"> -->
            <!-- <input type="hidden" name="checkboxvar[]" value="case_type"> -->
            <!-- <input type="hidden" name="checkboxvar[]" value="case_title"> -->
            <input type="hidden" name="checkboxvar[]" value="user_id">
            <input type="hidden" name="checkboxvar[]" value="type">

            <div class="form-radio col">
              <div class="col-sm-5 radio radio-primary radio-inline">
                <label>
                  <input type="radio" class="chkPassport" name="downloadas"  value="excel"> 
                  <i class="helper"></i>Excel
                </label>
              </div>
              <div class="col-sm-6 radio radio-primary radio-inline">
               <label>
                 <input type="radio" class="chkPassport" name="downloadas" value="pdf">      
                 <i class="helper"></i>PDF
               </label>
             </div>
           </div>  
           <div id="document-table-div-export" style="display: none;"></div>
           <button type="button" class="btn btn-warning btn-sm" name="download_advocate" onclick="download_doc()">Download</button>
         </div>
       </div>
     </div>

   </div>
 </div>

 <div class="modal fade" id="undodocument" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Delete Document Till</label>
            <input type="date" class="form-control" id="recipient-name" placeholder="Select Date">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" onclick="return confirm('Are you sure you want to Remove?');">Delete</button>
      </div>
    </div>
  </div>
</div>


<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
 $('#submit').prop("disabled", true);
 $("#checkAll").change(function () {
   $("input:checkbox").prop('checked', $(this).prop("checked"));
   $('#submit').prop("disabled", false);
   if ($('.chk').filter(':checked').length < 1){

     $('#submit').attr('disabled',true);}
   });

 $('input:checkbox').click(function() {
   if ($(this).is(':checked')) {
     $('#submit').prop("disabled", false);
   } else {
     if ($('.chk').filter(':checked').length < 1){
       $('#submit').attr('disabled',true);
     }
   }
 });   
</script>
<?php include 'footer.php'; ?>
<script type='text/javascript' src='scripts/jspdf.min.js'></script>
<script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
<script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
<script type="text/javascript" src="scripts/all_export.js"></script>
<script type="text/javascript" src="scripts/manage-document.js"></script>
<script>

  function editteam (id) {
    var id = +id;
    $.ajax({
      type:'POST',
      url:'profile_details.php',
      data:'ids='+id,
      beforeSend : function(){
        $(".flip-square-loader").show();
      },
      success:function(html){
        $('#edit_team').html(html);
      },
      complete:function(){
       $(".flip-square-loader").hide();
     },
     error:function(e){
       $(".flip-square-loader").hide();
       toastr.error("","Unable to Load","Sorry for inconvinence",{timeout:5000});
     }
   }); 
  }


  var close = document.getElementsByClassName("closebtn");
  var i;

  for (i = 0; i < close.length; i++) {
    close[i].onclick = function(){
      var div = this.parentElement;
      div.style.opacity = "0";
      setTimeout(function(){ div.style.display = "none"; }, 600);
    }
  }
</script>
<style>
  .alert {
    /*padding: 20px;*/
    background-color: #f44336;
    color: #404E67;
    opacity: 1;
    transition: opacity 0.6s;
    font-weight: bold;
    /*margin-bottom: 15px;*/
  }

  .alert.success {background-color: #4CAF50;}
  .alert.info {background-color: #2196F3;}
  .alert.warning {background-color: #ccc;}

  .closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
  }

  .closebtn:hover {
    color: black;
  }
  .allcr{
    border: 2px solid white!important;
  }
</style>