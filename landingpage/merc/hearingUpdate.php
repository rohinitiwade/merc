<?php include("header.php"); 
// session_start();//header('Content-type: text/html; charset=UTF-8'); ?>

<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/law/admin/css/vertical-layout-light/case-report.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <script type="text/javascript" src="js/case_search.js"></script> -->
<?php include("menu.php");?>
<br/><br/><br/>
<br/><br/><br/>
<br/><br/><br/>
<div class="main-panel">
	<div class="content-wrapper">
		<div class="card">
			<?php 
			extract($_POST);
			$name = $_POST['name'];
			?>

			<div class="card-body">
				<h4 class="card-header" style="    padding-top: 0px;"><?php  echo $name; ?>  Cases (<?php if($name!="" && $name!="All"){$sql = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_district`='".$_SESSION['cityName']."' AND `case_status`='".$name."' ORDER BY `case_id`  DESC");
			}elseif($name!="" && $name=="All"){
				$sql = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_district`='".$_SESSION['cityName']."' AND `remove_status`=1 ORDER BY `case_id`  DESC");
			}else{
			 $sql = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_district`='".$_SESSION['cityName']."' AND `case_status`='Running'  ORDER BY `case_id`  DESC");
			}
			echo $count = mysqli_num_rows($sql);?>) 
			<button type="button" class="btn btn-primary btn-sm " onclick="getData()">Find Case Law</button>
			<!-- <button type="button" class="btn btn-info btn-sm " onclick="getData()">Search Case</button> -->
			<!-- <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#searchanycases">Search Case</button> -->

			<div class="button_div row">
				<div class="col-sm-5">
					<form method="POST">
						<select name="name" class="form-control" id="state" onchange='this.form.submit()'>
							<option value="<?php echo $name; ?>"><?php  if($name!=""){ echo $name; }elseif($name==""){ echo "Running"; }else{?>--Select Option--<?php } ?></option>
							<option value="All">All</option>
							<option value="Running" >Running</option>
							<option value="Closed">Closed</option>
							<option value="Transfer/NOC Given">Transfer/NOC Given</option>
							<option value="Direction Matters">Direction Matters</option>
							<option value="Order Reserved">Order Reserved</option>
							<option value="Contempt Petition">Contempt Petition</option>         
						</select>
					</form>
				</div>
				<div class="col-sm-3"><button class="btn btn-secondary btn-sm" type="button" onclick="redirectToAddCases()">Add Case</button></div>
				<div class="col-sm-2"><button class="btn btn-sm btn-secondary" id="myBtn" data-target="#filter" data-toggle="modal" style="">Filter</button></div>
				<div class="col-sm-2"><button class="btn btn-sm btn-secondary" id="myBtn" data-target="#export" data-toggle="modal" style="">Export</button></div>
			</div>
		</h4>
		<!-- <form class="form-inline  " method="POST"> 
          <input type="text" class="form-control col-sm-3 searchanyinput" placeholder="Search any cases here" id="casenoSearch" >
          <i class="fa fa-search searchicon" aria-hidden="true" ></i>
          <div id="suggesstion-box" class="animated fadeInLeft"></div>
        </form> -->
 
<div class="col-sm-12  casereportdata">
			<?php if(isset($_POST['submit'])){
				extract($_POST);
				 $names = $_POST['search'];
			}?>
			<form  method="POST" name="MyFome">
				<div class="col-sm-12 row searchcase"> <label for="exampleFormControlTextarea4" class="searchanylabel"><b>Search Cases</b></label>
<!--   <textarea class="form-control" id="exampleFormControlTextarea4" rows="7"></textarea> -->

  <input type="text" class="form-control col-sm-3 searchanyinput" placeholder="Search any cases here" name="search" >
<i class="fa fa-search searchanyicon" aria-hidden="true" ></i>
          <div id="suggesstion-box11" class="animated fadeInLeft"></div>
  <input type="submit" class="btn btn-warning btn-sm searchanyfinal" value="submit" name="submit">
		</div>
	</form>
		<div class="main_body">
			<div class="col-12">


				<div class="table-responsive">
					<table id="example" class="table" border="0.25" width="100%">
						<thead>

							<tr>
								<th style="" class="srno-report">ID</th>
								<th style="" class="checkclass"><input type="checkbox" class="selectall" id="checkAll" /></th>
								<th style="" class="court-report">Name of Court</th>
								<th style="" class="case-report">Case No / Year</th>                                  
								<th style="" class="title-report">Name of Dept</th>
								<th style="" class="title-report">Subject</th>
								<!-- <th style="" class="title-report">Subject</th> -->
								<th style="" class="title-report">Case Description</th>
								<th style="" class="hearing-report">Hearing Date</th>
								<th style="" class="team-report">Team Member(s)</th>
								<!-- <th style="" class="case-details-report">Assign To</th> -->
								<!-- <th style="" class="case-details-report">Case Law Research</th> -->
								<th style="" class="action-report">Activity</th>
								
							</tr>
						</thead>
						<tbody>
							<?php 
							
							$showRecordPerPage = 50;
							if(isset($_GET['page']) && !empty($_GET['page'])){
								$currentPage = $_GET['page'];
							}else{
								$currentPage = 1;
							}
							$startFrom = ($currentPage * $showRecordPerPage) - $showRecordPerPage;
							$totalEmpSQL = "SELECT * FROM reg_cases";
							$allEmpResult = mysqli_query($conn, $totalEmpSQL);
							$totalEmployee = mysqli_num_rows($allEmpResult);
							$lastPage = ceil($totalEmployee/$showRecordPerPage);
							$firstPage = 1;
							$nextPage = $currentPage + 1;
							$previousPage = $currentPage - 1;
							if($names!=""){
								$court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND  `case_no`='".$names."' AND `remove_status`=1 ORDER BY `case_id`  DESC LIMIT $startFrom, $showRecordPerPage");
							
							}else{
								$court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."'  AND `remove_status`=1 ORDER BY `case_id` DESC LIMIT $startFrom, $showRecordPerPage");
							}
						
							$i=1;
							while($selcourt = mysqli_fetch_array($court)){
								$casecode = mysqli_query($connection, "SELECT * FROM `activity_priority` WHERE `priority_name`='".$selcourt['priority']."'");
								$setcolorecount = mysqli_num_rows($casecode);
								$selcolor = mysqli_fetch_array($casecode);
								?>
								<input type="hidden" name="" id="caseid">
								<tr style="cursor:pointer;background-color: <?php if($setcolorecount==1){ echo  $selcolor['priority_color']; }  ?>"; >
									<td class="rowclickable id_case" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>"><?php echo $i;?></td>
									<td class="rowclickable checkboxtimesheet" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>"><input type="checkbox" class="check" name="checkAll[]" / style="padding: 12px;"></td>
									<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
										
										<?php  if(is_numeric ($selcourt['court_name']) AND $selcourt['court_name']==2){ 
											$courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname);echo $courtlist['court_name']; echo " - "; echo $selcourt['high_court']; echo " - "; echo $selcourt['hc_bench']; echo " - "; echo $selcourt['hc_side']; echo " - "; echo $selcourt['hc_stamp_register'];}elseif($selcourt['court_name']==1){
												$courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname);
												echo $courtlist['court_name']; }elseif($selcourt['court_name']==3){   $courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname); echo $courtlist['court_name'];}else{echo $selcourt['court_name'];}?> 
											</td>                                 
											<td class="rowclickable"  data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
												
												<?php if(is_numeric ($selcourt['court_name'])){ 
													$courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname);echo $selcourt['hc_case_type']; echo " "; echo $selcourt['case_no']; echo " / "; echo $selcourt['case_no_year'];  } else{ echo $selcourt['case_no'];}?>
												</td>
												<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
													<?php echo $selcourt['name_of_matter'];?></td>
												<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
													<?php echo $selcourt['case_title'];?></td>
													<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
													<?php echo $selcourt['case_description'];?></td>
													<!-- <td is="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
														<?php echo $selcourt['case_description'];?></td> -->
														<td class="rowclickable" style="padding: 10px;"><?php if($selcourt['hearing_date']!=''){echo date('F d,Y',strtotime($selcourt['hearing_date']));}else{echo " ";}?></td>
														<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;"><?php echo $selcourt['team_member'];?></td>

														<td style="padding: 10px;">
															<a href="editData.php"  data-toggle="modal" data-target="#myModal" onclick="edithearing(<?php echo $selcourt['case_id'];?>)">Edit</a>
															

<!-- The Modal -->
															<div class="modal" id="myModal">
															  <div class="modal-dialog">
															    <div id="edithearing"></div>
															  </div>
															</div>
														</td>
													</tr> 

													<?php $i++; } ?>
												</tbody>
												<tfoot>
													<tr>
														<td colspan="9">
															<input type="checkbox" class="selectall" name="" style="position: relative;top: 3px;">
															<b style="position: relative;top: 2px;">Select/Unselect All</b>
															<button class="btn" style="background-color: #2d4866;color: white!important;margin-left: 10px" onclick="recordHearing()">Record Hearing</button>
															<button class="btn" style="background-color: #2d4866;color: white!important;margin-left: 10px"  data-target="#assignteam" data-toggle="modal">Assign Team</button>
															<button class="btn btn-danger" style="margin-left: 10px">Delete</button>
														</td>
													</tr>
												</tfoot>
											</table>
											<nav aria-label="Page navigation">
												<ul class="pagination">
													<?php if($currentPage == $firstPage) { ?>
														<li class="page-item">
															<a class="page-link" href="?page=<?php echo $firstPage ?>" tabindex="-1" aria-label="Previous">
																<span aria-hidden="true">First</span>
															</a>
														</li>
													<?php } ?>
													<?php if($currentPage >= 10) { ?>
														<li class="page-item"><a class="page-link" href="?page=<?php echo $previousPage ?>"><?php echo $previousPage ?></a></li>
													<?php } ?>
													<li class="page-item active"><a class="page-link" href="?page=<?php echo $currentPage ?>"><?php echo $currentPage ?></a></li>
													<?php if($currentPage != $lastPage) { ?>
														<li class="page-item"><a class="page-link" href="?page=<?php echo $nextPage ?>"><?php echo $nextPage ?></a></li>
														<li class="page-item">
															<a class="page-link" href="?page=<?php echo $lastPage ?>" aria-label="Next">
																<span aria-hidden="true">Last</span>
															</a>
														</li>
													<?php } ?>
												</ul>
											</nav>

										</div>
										
										<style>
										.order-listing_paginate
										{
											display: none !important;
										}
										div.dataTables_wrapper div.dataTables_paginate{
											display: none !important;
										}
										.pagination{
											float: right !important;
										}
									</style>
									

                      <!-- <div class="table-responsive">
                        <table id="order_listing" class="table">
                         <thead>
                          <tr>
                           <th style="" class="srno-report">ID</th>
                           <th style="" class="checkclass"><input type="checkbox" class="check" id="checkAll" /></th>
                           <th style="" class="court-report">Name of Court</th>
                           <th style="" class="case-report">Case No / Year</th>                                  
                           <th style="" class="title-report">Name of Dept</th>
                           <th style="" class="title-report">Subject</th>
                           <th style="" class="hearing-report">Hearing Date</th>
                           <th style="" class="team-report">Team Member(s)</th> -->
                           <!-- <th style="" class="case-details-report">Assign To</th> -->
                           <!-- <th style="" class="case-details-report">Case Law Research</th> -->
                          <!--  <th style="" class="action-report">Activity</th>
                         </tr>
                       </thead>


                   </table> -->

                    <!--  <table class="tbl" >
                      <tr>
                        <td style="" >
                          <input type="checkbox" name="" class="checkbox ">
                          <b style="position: relative;top: 2px;">Select/Unselect All</b>
                          <button class="btn assign-btn" style="" data-target="#record" data-toggle="modal">Record Hearing</button>
                          <button class="btn assign-btn" style=""  data-target="#assignteam" data-toggle="modal">Assign Team</button>
                          <button class="btn btn-danger btn-sm delete-btn" style="">Delete</button>
                        </td>
                      </tr>
                    </table>
                  </div>
              -->
                <!--  
                 <script type="text/javascript" language="javascript" class="init"> 
                   $(document).ready(function() {   
                    $('#order_listing').DataTable({
                      'processing': true,
                      'serverSide': true,
                      'serverMethod': 'post',
                      'ajax': {
                       'url':'cases-report_datatable.php'
                     },
                     "order": [[ 0, "desc" ]],
                     'columns': [
                     { data: 'case_id' },
                     { data: 'check_box', "orderable": false},
                     { data: 'court_name' },
                     { data: 'case_no' },
                     { data: 'case_title' },
                     { data: 'case_description' },
                     { data: 'hearing_date' },
                     { data: 'team_member' },
                  
                     { data: 'view_case' , "orderable": false},
                     ]
                   });
                  }); 
              </script> -->


<!-- 
              </div>
            </div>
          </div>
      </div> -->
  </div>
  <?php include("footer.php");?>

  <script type="text/javascript" src="js/cases-report.js"></script>
</div>


<form name="myform" method="POST" action="downloadexcel.php">
	<div class="modal" id="export">
		<div class="modal-dialog">
			<div class="modal-content" style="">
				<div class="modal-header" style="">
					<h4>Export</h4>
					<button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-3 export-div">
							<input type="checkbox" name='checkboxvar[]' style="" value="case_id" />File<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="user_id" checked="" />Team Member(s)<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="Stage"/>Stage<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="court_name" checked=""/>Court<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="hearing_date" value="Hearing Date" checked=""/>Hearing Date<br>
							<!-- <input type="checkbox" name='checkboxvar' style="margin-right: 5px;" value=""/>Last Posted for<br> -->
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="court_hall"/>CourtHall#<br> -->
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Opponent Advocate(s)<br>-->
							<input type="checkbox" name='checkboxvar[]' style="" value="priority"/>Priority<br> 
							<!--  <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="Connected"/>Connected -->
						</div>
						<div class="col-sm-3">
							<input type="checkbox" name='checkboxvar[]' style="" value="case_no" checked=""/>Case No.<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="case_no_year"/>Session<br>
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Last Action Taken<br> -->
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="Team Member"/>Floor#<br> -->
							<input type="checkbox" name='checkboxvar[]' style="" value="date_of_filling"/>Date of Filling<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="case_description"/>Case Discription<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="section_category"/>Section/Category
						</div>
						<div class="col-sm-3">
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Client(s)/External Advocate(s)<br> -->
							<input type="checkbox" name='checkboxvar[]' style="" value="hearing_date"/>Last Hearing Date<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="classification"/>Classification<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="case_title" checked=""/>Title<br>
                     <!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Before Hon'ble Judge(s)<br>
                     	<input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Related Case(s) -->
                     </div>
                     <div class="col-sm-3">

                     	<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="Team Member"/>Opponnt(s)<br> -->
                     	<input type="checkbox" name='checkboxvar[]' style="" value="reffered_by"/>Referred By<br>
                     	<input type="checkbox" name='checkboxvar[]' style="" value="CNR" />CNR

                     </div>
                 </div><br><br>
                 <div class="row">
                 	<h4>Download as:</h4>
                 	<input type="radio" class="pdf" name="downloadas" value="pdf" style="">PDF
                 	<input type="radio" class="excel" name="downloadas" value="excel" style="" checked="">Excel
                 	<input type="submit" class="download" style="" value="Download">
                 </div>
             </div>
         </div>
     </div>
 </div>
</form>

<div class="modal" id="filter">
	<div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header" style="">
				<h4>Filter</h4>
				<button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
			</div>
			<form name="MyName" id="filter_form" action="POST">
				<div class="modal-body filterlabel">
					<div class="row">
						<div class="col-sm-4">
							<label>Title</label>
							<input type="text" class="form-control form-group" placeholder="Enter Title" name="title">
							<label>Case</label>
							<input type="text" class="form-control form-group" placeholder="Enter Case" name="case_no">
							<label>Case Year</label>
							<select id="year" class="form-control form-group">
							<?php $caseyear = mysqli_query($connection, "SELECT  DISTINCT `case_no_year` FROM `reg_cases` ORDER BY `case_no_year` DESC");
							foreach($caseyear as $value){
							 echo "<option value='".$value['case_no_year']."'>".$value['case_no_year']."</option>";
							 } ?>
							</select>
							<!-- <label>Before Hon'ble Judge/s</label>
							<input type="text" class="form-control form-group" placeholder="Before Hon'ble Judge/s" name="before_hudge"> -->
							<!-- <label>Section/Category</label>
							<input type="text" class="form-control form-group" placeholder="Enter Section/Category" name="category"> -->
							<!-- <label>Posted For</label>
							<input type="text" class="form-control form-group" placeholder="Posted For" name="posted_for"> -->
							<label>Opponents</label>
							<input type="text" class="form-control form-group" placeholder="Enter Opponents" name="opponents">
							<label>CNR</label>
							<input type="text" class="form-control form-group" placeholder="Enter CNR" name="cnr">
                            <span id="dvPassport1" style="display: none;">
							<label>Supreme Court of India</label>
							<select class="form-control">
								<option value="All">All</option>
								<option value="case_no">Case Number</option>
								<option value="diary_no">Diary Number</option>
							</select>
						    </span>
						    
						</div>
						<div class="col-sm-4">
							<label>Advocate</label>
							<input type="text" class="form-control form-group" placeholder="Enter Advocate Name" name="Advocate">
							<label>Hearing Date</label>
							<input type="text" class="form-control form-group" id="datepicker" name="hearing_date" placeholder="Hearing Date">
							<label>Case Filing Date Form</label>
							<input type="text" class="form-control form-group" id="datepicker2" name="datepicker2" placeholder="Case Filing Date Form">
							
							<!-- <label>Referred By</label>
							<input type="text" class="form-control form-group" placeholder="Enter Referred By" name="referred_by"> -->
							<label>Priority</label>
							<select class="form-control">
								<option value="">--Select Priority--</option>
								<?php $priority = mysqli_query($connection, "SELECT `priority_name` FROM `activity_priority` ORDER BY priority_name ASC");
								while($prioritysel = mysqli_fetch_array($priority)){?>
									<option value="<?php echo $prioritysel['priority_id'];?>"><?php echo $prioritysel['priority_name'];?></option>
								<?php } ?>
							</select>
							<label>Stage</label>
							<input type="text" class="form-control form-group" placeholder="Enter Stage">
                             <span id="dvPassport" style="display: none;">
							<label>Case Type</label>
							<select class="form-control">
								<option value="All">All</option>
								<option value="case_no">Case Number</option>
								<option value="diary_no">Diary Number</option>
							</select>
						    </span>
							<!-- <input type="text" class="form-control form-group" placeholder="Put dropdown" name="priority"> -->
							<!-- <label>Action Taken</label>
							<input type="text" class="form-control form-group" placeholder="Action Taken"> -->
							
						</div>
						<div class="col-sm-4">
							<label>Team Member</label>
							<input type="text" class="form-control form-group" placeholder="Enter Team Member">
							<label>Hearing Date To</label>
							<input type="text" class="form-control form-group" id="datepicker1" placeholder="Hearing Date To">
							<label>Case Filing Date To</label>
							<input type="text" class="form-control form-group" id="datepicker4" placeholder="Case Filing Date To">
							<label>File</label>
							<input type="text" class="form-control form-group" placeholder="Enter File">
							<label>Court</label>
							<select name="court_list" class="form-control" id="courtNamecourt">
						    <option value="">-- Select Court Name --</option>
							<?php $courtlist = mysqli_query($connection, "SELECT `court_name`,`court_id` FROM `court_list` ORDER BY court_id ASC");
							    foreach($courtlist as $values){?>
							    <option value="<?php echo $values['court_id'];?>"><?php echo $values['court_name'];?></option>
							    <?php } ?>
							</select>

							<!-- <label>Sessions</label>
							<select name="sessions" style="" class="form-control">
								<option value="">Please Select</option>
								<option value="1">Morning</option>
								<option value="2">Evening</option>
							</select> -->
							 <script type="text/javascript">
						    $(function () {
						        $("#courtNamecourt").change(function () {
						            if ($(this).val() == "1") {
						                $("#dvPassport").show();
						            } else {
						                $("#dvPassport").hide();
						            }
						            if ($(this).val() == "1") {
						                $("#dvPassport1").show();
						            } else {
						                $("#dvPassport1").hide();
						            }

						        });
						    });
						</script>
<!-- <span>Do you have Passport?</span>
<select id="ddlPassport">
    <option value="N">No</option>
    <option value="Y">Yes</option>
</select>
<hr />
<div id="dvPassport" style="display: none">
    Passport Number:
    <input type="text" id="txtPassportNumber" />
</div> -->
						</div>
					</div><br><br>
				</div>
			</form>
			<script>
				  $( function() {
    $( "#datepicker" ).datepicker();
    $( "#datepicker1" ).datepicker();
    $( "#datepicker2" ).datepicker();
    $( "#datepicker3" ).datepicker();
    $( "#datepicker4" ).datepicker();
  } );
  </script>
			</script>
			<div class="modal-footer">
				<div class="row">
					<button class="btn btn-danger btn-sm reset-btn" type="button" onclick="resetFilter()" style="">Reset</button>
					<button class="submit-btn" style="">Submit</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="record">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Record Hearing</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-4">
						<label>Next Hearing Date</label><br>
						<input type="text" class="form-control" id="datepicker">
						<label>Action Taken</label><br>
						<input type="text" class="form-control">
					</div>
					<div class="col-sm-4">
						<label>Stage</label><br>
						<input type="text" class="form-control">
						<label>Session</label><br>
						<select name="sessions" style="" class="form-control">
							<option value="">Please Select</option>
							<option value="1">Morning</option>
							<option value="2">Evening</option>
						</select>
					</div>
					<div class="col-sm-4">
						<label>Posted For</label><br>
						<input type="text" class="form-control" >
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<button class="btn btn-info btn-sm" type="button">Record</button>
				<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>

			</div>
		</div>
	</div>
</div>
<div class="modal" id="assignteam">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #2d4866;color: white;">
				<h4>Assign Team</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-8">
						<select name="country" class="form-control">
							<option >Select members to assign to this case</option>
						</select>
					</div>
					<div class="col-sm-4">
						<button class="assign" style="">Assign</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
    <!--     <script type="text/javascript">
          $("#checkAll").click(function () {
            $(".check").prop('checked', $(this).prop('checked'));
          });

      </script> -->
<!--         <div class="modal" id="myModal">
          <div class="modal-dialog">
            <div class="modal-content">

          
              <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">
                Modal body..
              </div>

          
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>

            </div>
          </div>
      </div> -->

      <div class="modal" id="processtheMatter">
      	<div class="modal-dialog modal-md">
      		<div class="modal-content">

      			<div class="modal-header">
      				<h4 class="modal-title">Seek Legal Help</h4>
      				<button type="button" class="close" data-dismiss="modal">&times;</button>
      			</div>

      			<div class="modal-body">

      				<form id="formSubmit" class="smart-form client-forms" method="POST" enctype="multipart/form-data" autocomplete="off" data-select2-id="formSubmit">
      					<div class="row" data-select2-id="9">
      						<div class="col-sm-6">
      							<?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."' LIMIT 10");
      							$row = mysqli_fetch_array($fetch);?>
      							<div class="form-group" data-select2-id="31">
      								<input type="hidden" id="fname" value="<?php echo $row['name'];?>">
      								<input type="hidden" id="lname" value="<?php echo $row['last_name'];?>">
      								<input type="hidden" id="email" value="<?php echo $row['email'];?>">
      								<input type="hidden" id="mobile" value="<?php echo $row['mobile'];?>">
      								<label for="">Subject</label>
      								<div class="input-group mb-3" data-select2-id="30">

      									<select id="subjectlist" class="form-control sideclass" name="Cases[high_court_id]">
      										<option value="">Please select / कृपया निवडा</option>
      									</select>       
      								</div>
      							</div>
      						</div>

      						<div class="col-sm-6">
      							<div class="form-group">
      								<label for="">Acts</label>
      								<div class="input-group mb-3 " data-select2-id="6">

      									<select id="actlist" class="form-control sideclass" name="Cases[high_court_id]">
      										<option value="">Please select / कृपया निवडा</option>
      									</select>
      								</div>
      							</div>
      						</div>

      						<div class="col-sm-6">
      							<div class="form-group">
      								<label for="">Section</label>   <input type="text" class="form-control same" id="section" name="section" placeholder="Enter Section">
      							</div>
      						</div>

      						<div class="col-sm-6">
      							<div class="form-group">
      								<label for="">State<span class="text-danger"> *</span></label> <input type="text" class="form-control same" id="state" name="state" placeholder="Enter State" required="">
      							</div>
      						</div>

      						<div class="col-sm-6">
      							<div class="form-group">
      								<label for="">District<span class="text-danger"> *</span></label> <input type="text" class="form-control same" id="district" name="district" placeholder="Enter District" required=""></div>
      							</div>

      							<div class="col-sm-12">
      								<div class="form-group">
      									<label for="">Question<span class="text-danger"> *</span></label>
      									<textarea class="form-control rounded-0" id="question_id" rows="10"></textarea>

      								</div> 
      							</div>
      						</div>
      						<div class="form-group col-sm-12" style="text-align: center;">
      							<button class="btn btn-sm close_btn newsubmit form-group" title="Add" id="submit" onclick="getclr()">Submit </button>

      						</div>
      					</form>
      				</div>
      				<div class="modal-footer">
      					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      				</div>

      			</div>
      		</div>
      	</div> 
<!-- 
      	<div class="modal" id="searchanycases">
  <div class="modal-dialog">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title">Search Any Cases Here</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

 
      <div class="modal-body">
        <div class="form-group purple-border">
  <label for="exampleFormControlTextarea4"><b>Search Cases</b></label>

  <input type="text" class="form-control" id="usr">

</div>
      </div>


      <div class="modal-footer">
    
      	 <button type="button" class="btn btn-sm btn-danger searchanysubmit" id="submitanycase" >Submit</button>
        <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div> -->



      	<div class="modal" id="processtheMatter">
      		<div class="modal-dialog clrmodal">
      		</div>
      	</div>
    <!--   	<script>
      		$(document).ready(function(){
      			$('#empTable').DataTable({
      				'processing': true,
      				'serverSide': true,
      				'serverMethod': 'post',
      				'ajax': {
      					'url':'ajareport.php'
      				},
      				'columns': [
      				{ data: 'emp_name' },
      				{ data: 'email' },
      				{ data: 'gender' },
      				{ data: 'salary' },
      				{ data: 'city' },
      				]
      			});
      		});

      	</script> -->
      	<script>
      		$(document).ready(function() {
      			$('#example').DataTable( {
      				"lengthMenu": [50, 10, 25, 100,200 ]
      			} );
      		} );
      	</script>

      	<div class="modal" id="myModal">

      	</div>
      	
      </div>
  </div>
</div>
<script>
	function edithearing (id) {
    var id = +id;
    $.ajax({
      type:'POST',
      url:'ajaxdata/edithearing.php',
      data:'id='+id,
      success:function(html){
        $('#edithearing').html(html);
      }
    }); 
  }

</script>