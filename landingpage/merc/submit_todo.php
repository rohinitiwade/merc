
<?php
include('includes/dbconnect.php');
include('phpfile/sql_home.php');
session_start();
$_SESSION['cityName'];
header('Access-Control-Allow-Origin: *');
$params = $_POST['tododetail'];
$data   = json_decode($params, true);
//print_r($data);
$arr    = array();
$team_data = array();
$date   = date('Y-m-d H:i:s');

// view todo submit
if ($data['flag'] == 'view') {
    if ($data['desc'] != '') {
        if ($data['datepicker'] != '') {
            $fromdate = date('Y-m-d', strtotime($data['datepicker']));
        } else {
            $fromdate = '';
        }
        $dates       = date('Y-m-d');
        $select_reg  = mysqli_query($connection, "SELECT `case_title` FROM `reg_cases` WHERE `case_id`='" . $data['case_id'] . "'");
        $get_reg     = mysqli_fetch_array($select_reg);
        $sql         = "INSERT INTO `todo_list` (`content`,`expiry_date`,`private`,`date_time`,`user_id`,`division`,`case_id`,`under_division`) VALUES('" . $data['desc'] . "','" . $fromdate . "','" . $data['private'] . "','" . $date . "','" . $_SESSION['user_id'] . "','" . $_SESSION['cityName'] . "','" . $data['case_id'] . "','" . $_SESSION['under_division'] . "')";
        //echo $sql;exit;
        $insert_todo = mysqli_query($connection, $sql);
        $lastto      = mysqli_insert_id($connection);
        
        foreach ($data['assign_to_array'] as $assign) {
            // foreach ($data['relateto'] as $relate) {
            $insert_todorelate = mysqli_query($connection, "INSERT INTO `todo_team` (`todo_id`,`assign_to`,`date_time`,`case_id`,`user_id`,`division`,`relate_to`,`dates`) VALUES('" . $lastto . "','" . $assign . "','" . $date . "','" . $data['case_id'] . "','" . $_SESSION['user_id'] . "','" . $_SESSION['cityName'] . "','" . $get_reg['case_title'] . "','" . $dates . "')");
            $lastteam          = mysqli_insert_id($connection);
            // } 
        }
        foreach ($data['reminder_array'] as $reminder) {
            
            $insert_todoreminder = mysqli_query($connection, "INSERT INTO `todo_reminder` (`team_id`,`todo_id`,`relate_to`,`assign_to`,`date_time`,`case_id`,`user_id`,`email_option`,`time_no`,`minute`,`division`,`relate_toid`) VALUES('" . $lastteam . "','" . $lastto . "','" . $get_reg['case_title'] . "','" . $assign . "','" . $date . "','" . $data['case_id'] . "','" . $_SESSION['user_id'] . "','" . $reminder['email_option'] . "','" . $reminder['time'] . "','" . $reminder['minute'] . "','" . $_SESSION['cityName'] . "','" . $data['case_id'] . "')");
            $lastadv             = mysqli_insert_id($insert_todoreminder);
        }
        // foreach ($data['advocates'] as $advocate) {
        //     $adv = mysqli_query($connection, "INSERT INTO `assign_toadvocates`(`user_id`, `case_id`, `todo_id`, `division`, `advocate_id`, `date_time`,`todo_reminder`) VALUES ('" . $_SESSION['user_id'] . "','" . $data['case_id'] . "','" . $lastto . "','" . $_SESSION['cityName'] . "','" . $advocate . "','" . $date . "','" . $lastadv . "')");
        // }
    }
    if ($data['list'] == 'all') {
        $list = mysqli_query($connection, "SELECT tl.content,tl.todo_id,tl.private,tr.case_id,tl.expiry_date FROM todo_list tl LEFT JOIN todo_reminder tr ON tr.todo_id=tl.todo_id WHERE " . $viewtodo . " AND tl.todo_status!='Completed' AND tr.case_id='" . $data['case_id'] . "' ORDER BY tl.todo_id DESC ");
    } elseif ($data['list'] == 'Completed') {
        $list = mysqli_query($connection, "SELECT tl.content,tl.todo_id,tl.private,tr.case_id,tl.expiry_date FROM todo_list tl LEFT JOIN todo_reminder tr ON tr.todo_id=tl.todo_id WHERE " . $viewtodo . " AND tl.todo_status='Completed' AND tr.case_id='" . $data['case_id'] . "' ORDER BY tl.todo_id DESC ");
    }
    while ($assigns = mysqli_fetch_array($list)) {
        
        $todocases    = mysqli_query($connection, "SELECT `case_type`,`case_no`,`case_no_year`,`case_title`,`supreme_court`,`diary_no`,`diary_year` FROM `reg_cases` WHERE `case_id`='" . $assigns['case_id'] . "'");
        $seltodocases = mysqli_fetch_array($todocases);
        
        if ($seltodocases['supreme_court'] == 'Diary Number') {
            $case = $seltodocases['diary_no'] . ' / ' . $seltodocases['diary_year'] . ' ' . $seltodocases['case_title'];
        } else {
            $case = $seltodocases['case_type'] . ' ' . $seltodocases['case_no'] . ' ' . $seltodocases['case_no_year'] . ' ' . $seltodocases['case_title'];
        }
        
        $teams = mysqli_query($connection, "SELECT DISTINCT `assign_to` FROM `todo_team` WHERE `todo_id`='" . $assigns['todo_id'] . "'");
        while ($teams_fetch = mysqli_fetch_array($teams)) {
            $teamss                 = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='" . $teams_fetch['assign_to'] . "' ");
            $teams_fetchs           = mysqli_fetch_array($teamss);
            // echo $teams_fetchs['name']; echo $teams_fetchs['last_name']; echo ", "; 
            // $team_object            = new stdClass();
            // $team_object->name      = TRIM($teams_fetchs['name']);
            // $team_object->last_name = TRIM($teams_fetchs['last_name']);
            // $team_details[]         = $team_object;
            $team_data[] = $teams_fetchs['name'] ." ". $teams_fetchs['last_name'];
        }
        if ($assigns['expiry_date'] != '')
            $expiry = date("jS M, Y", strtotime($assigns['expiry_date']));
        $view_data_object                    = new stdClass();
        $view_data_object->case_id           = TRIM($assigns['case_id']);
        $view_data_object->todo_id           = TRIM($assigns['todo_id']);
        $view_data_object->encrypted_todo_id = TRIM(base64_encode($assigns['todo_id']));
        $view_data_object->content           = TRIM($assigns['content']);
        $view_data_object->private           = TRIM($assigns['private']);
        $view_data_object->expiry_date       = TRIM($expiry);
        $view_data_object->case              = TRIM($case);
        $view_data_object->team              = $team_data;
        $case_details[]                      = $view_data_object;
    }
    // array_push($case_details,array('team'=>$teams));
    // array_push($case_details,$team_details);
    
    $all   = mysqli_query($connection, "SELECT  COUNT(tl.todo_id) As allcount FROM todo_list tl LEFT JOIN todo_reminder tr ON tr.todo_id=tl.todo_id  WHERE " . $viewtodo . " AND tr.case_id='" . $data['case_id'] . "' AND tl.todo_status  !='Completed'");
    $count = mysqli_fetch_array($all);
    $all   = $count['allcount'];
    
    $complete  = mysqli_query($connection, "SELECT COUNT(tl.todo_id) As completed FROM todo_list tl LEFT JOIN todo_reminder tr ON tr.todo_id=tl.todo_id WHERE " . $viewtodo . " AND tr.case_id='" . $data['case_id'] . "' AND tl.todo_status='Completed'");
    $complete1 = mysqli_fetch_array($complete);
    $completed = $complete1['completed'];

    $todo_cases = mysqli_query($connection,"SELECT DISTINCT tl.todo_id FROM todo_list tl LEFT JOIN todo_reminder tr ON tr.todo_id=tl.todo_id WHERE " . $viewtodo . " AND tr.case_id='".$data['case_id']."' ORDER BY tl.todo_id DESC");
   $cnt_todocases = mysqli_num_rows($todo_cases);
}
// create todo submit
else {
    // $fromdate = $data['datepicker'];//date('Y-m-d',strtotime($data['datepicker']));
    // $todate = $data['datepicker1'];//date('Y-m-d',strtotime($data['datepicker1']));
    $fromdate = date('Y-m-d H:i', strtotime($data['datepicker']));
    $todate   = date('Y-m-d H:i', strtotime($data['datepicker1']));
    
    $sql         = "INSERT INTO `todo_list` (`content`,`from_date`,`to_date`,`private`,`date_time`,`user_id`,`division`,`under_division`,`expiry_date`) VALUES('" . $data['desc'] . "','" . $fromdate . "','" . $todate . "','" . $data['private'] . "','" . $date . "','" . $_SESSION['user_id'] . "','" . $_SESSION['cityName'] . "','" . $_SESSION['under_division'] . "','" . $data['datepickerdata'] . "')";
    //echo $sql;exit;
    $insert_todo = mysqli_query($connection, $sql);
    $lastto      = mysqli_insert_id($connection);
    // print_r($data['relateto']);
    foreach ($data['relateto'] as $relate) {
        foreach ($data['assign_to_array'] as $assign) {
            $insert_todorelate = mysqli_query($connection, "INSERT INTO `todo_team` (`todo_id`,`relate_to`,`assign_to`,`date_time`,`case_id`,`user_id`,`relate_toid`,`division`) VALUES('" . $lastto . "','" . $relate['text'] . "','" . $assign . "','" . $date . "','" . $relate['id'] . "','" . $_SESSION['user_id'] . "','" . $relate['id'] . "','" . $_SESSION['cityName'] . "')");
            // $update_list = mysqli_query($connection,"UPDATE `todo_list` SET `case_id`='".$relate['id']."' WHERE `todo_id`='".$lastto."'");
            $lastteam          = mysqli_insert_id($connection);
        }
        foreach ($data['reminder_array'] as $reminder) {
            $insert_todoreminder = mysqli_query($connection, "INSERT INTO `todo_reminder` (`team_id`,`todo_id`,`relate_to`,`assign_to`,`date_time`,`case_id`,`user_id`,`relate_toid`,`email_option`,`time_no`,`minute`,`division`) VALUES('" . $lastteam . "','" . $lastto . "','" . $relate['text'] . "','" . $assign . "','" . $date . "','" . $relate['id'] . "','" . $_SESSION['user_id'] . "','" . $relate['id'] . "','" . $reminder['email_option'] . "','" . $reminder['time'] . "','" . $reminder['minute'] . "','" . $_SESSION['cityName'] . "')");
            $lastadv             = mysqli_insert_id($connection);
        }
        // foreach ($data['advocates'] as $advocate) {
        //     $adv = mysqli_query($connection, "INSERT INTO `assign_toadvocates`(`user_id`, `case_id`, `todo_id`, `division`, `advocate_id`, `date_time`,`todo_reminder`) VALUES ('" . $_SESSION['user_id'] . "','" . $relate['id'] . "','" . $lastto . "','" . $_SESSION['cityName'] . "','" . $advocate . "','" . $date . "','" . $lastadv . "')");
        // }
    }
    
}
// if ($insert_todo) {
$arr = array(
    'status' => 'success',
    'data' => $case_details,
    
    'all' => $all,
    'complete' => $completed,
    'count' => $cnt_todocases
);
// }
echo json_encode($arr);
?>