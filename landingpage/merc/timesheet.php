<!-- <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css"> -->

<div class="col-sm-12 row ">
  <div class="col-sm-6 form-group">
  </div>
  <div class="col-sm-6 form-group" style="text-align: right;">
    <button type="button" class="btn btn-outline-primary btn-sm form-group" id="addtimesheet" >Add New</button>&nbsp;&nbsp;
    <button type="button" class="btn btn-outline-warning btn-sm form-group" >Export</button>&nbsp;&nbsp;
    <button type="button" class="btn btn-outline-info btn-sm form-group" >Filter</button>&nbsp;&nbsp;
  </div>
</div>
<div class="col-sm-12 row" id="error_message" style="text-align: center;">    
</div>
<div class="row" id="addtimesheetform" style="display: none;"> 
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <form class="forms-sample" method="POST">
          <div class="form-group row forms-sample-div" style="">
            <div class="col-sm-4 col-md-6 col-lg-4 col-xs-12 form-group">              
              <div class="form-group row">
                <label for="exampleInputUsername1" class=" col-form-label col-sm-4"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Particulars</b></label>
                <div class="col">
                  <input type="text" name="particulars" id="particulars" class="form-control" autocomplete="off">
                </div>
              </div>
              <input type="hidden" name="case" id="case" value="<?php echo $selcase['case_id'];?>" class="form-control" autocomplete="off">
              <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['user_id'];?>" class="form-control" autocomplete="off">
              <input type="hidden" name="id1" id="id1" class="form-control" autocomplete="off">
            </div>
            
            <div class="col-sm-4 col-md-6 col-lg-4 col-xs-12 form-group">
              <div class="form-group row">
                <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Date</b></label>
                <div class="col-sm-8">
                  <input type="text" name="date1"  placeholder="Enter Date" id="datepickertimesheet" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-md-12 col-lg-4 col-xs-12 form-group row">           
              <label for="exampleInputUsername1" class="col-form-label" style="text-align: left;"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Time Spent</b><br><span>(in Hours)</span></label>
              <div class="col-sm-4 form-group">
                <select class="form-control" name="timespent" id="timespent">
                  <option value="">HH</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                  <option value="13">13</option>
                  <option value="14">14</option>
                  <option value="15">15</option>
                  <option value="16">16</option>
                  <option value="17">17</option>
                  <option value="18">18</option>
                  <option value="19">19</option>
                  <option value="20">20</option>
                  <option value="21">21</option>
                  <option value="22">22</option>
                  <option value="23">23</option>
                </select>                 
              </div>
              <div class="col-sm-4">
                <select class="form-control" name="timeinmin" id="timeinmin">
                  <option value="">MM</option>
                  <option value="00">00</option>
                  <option value="05">05</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                  <option value="20">20</option>
                  <option value="25">25</option>
                  <option value="30">30</option>
                  <option value="35">35</option>
                  <option value="40">40</option>
                  <option value="45">45</option>
                  <option value="50">50</option>
                  <option value="55">55</option>
                </select>
              </div>

            </div>

          </div>
          <div class="col-sm-12 row p-0 form-group">
           <label class="col-sm-1 col-md-2 col-lg-1 form-group"><b>Type</b></label>
           <div class="form-radio col">
            <div class="radio radio-primary radio-inline">
              <label>
                <input type="radio" value="Effective" id="type" name="type">  
                <i class="helper"></i>Effective
              </label>
            </div>
            <div class="radio radio-primary radio-inline">
              <label>
                <input type="radio" value="Non Effective / Procedural Appearance" id="type" name="type">      
                <i class="helper"></i>Non Effective / Procedural Appearance
              </label>
            </div>
          </div>          
        </div>
        <div class="col-sm-12">
          <button class="btn btn-sm timesheetsubmit btn-info" type="button" value="submit" name="submit1" id="timesheetsubmit" onclick="submitTimesheet()">Submit </button>&nbsp;
          <button class="btn btn-sm timesheetsubmit btn-warning" type="button" value="submit" name="submit2" id="updatetimesheet" onclick="updateTimesheet()">Update </button>
          <a href="#" class="btn btn-sm btn-danger" onclick="CancelTimesheet()">Cancel</a>
        </div>
      </form>       
    </div>
  </div>
</div>
</div>
<div class="row col-sm-12 table-responsive" id="timesheetlist" >

</div>

<!-- <script src="js/jquery.dataTables.min.js"></script> -->
