<?php session_start(); include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<link rel="stylesheet" href="css/vertical-layout-light/add_cases.css">
<style type="text/css">
	.fetchdataloader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 60px;
		float: right;
		height: 60px;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}
/*.select2-results__option[aria-selected="true"] {
	display: none;
	}*/
	#sc_info{
		color: red;
		width: 100%;
		float: left;
		/*text-align: right;*/
		/*position: absolute;*/
		/*z-index: 1;*/
		/*top: 54px;*/
		display: none;
	}
	.full-name{
		width: 55%;
	}
	.fetchdata-dialog{
		max-width:80%;
	}
	#submit_data{
		text-align: right;
	}
	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
</style>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title"> Add Case </h5>
							</div>

							<div class="page-body">
								<div class="card form-group">
									
									
									<div class="card-block">
										<input type="hidden" name="flag" id="flag" value="<?php echo $_GET['flag']?>">
										<form class="forms-sample row" autocomplete="off">
											
											<div class="form-group col-sm-8 row" id="areyouappearingas">
												<label for="exampleInputUsername1" class="col-sm-3 col-form-label">&nbsp;&nbsp;Type of Litigation</label>
												<div class="form-radio col">
													<div class="radio radio-primary radio-inline col-sm-5">
														<label>
															<input type="radio" class="chkPassport" name="appearing_radio"  id="appearing_radio_one" value="Litigations Filed by MSEDCL" checked>

															<i class="helper"></i>Litigations Filed by MSEDCL</label>
														</div>
														<div class="radio radio-primary radio-inline col-sm-5">
															<label>
																<input type="radio" class="chkPassport"  name="appearing_radio"  id="appearing_radio_two" value="Litigations Filed against MSEDCL">

																<i class="helper"></i>Litigations Filed against MSEDCL</label>
															</div>
														</div>
													</div>
								<!-- <div class="col-sm-6 row form-group" id="case-div">
									<label for="exampleInputUsername1" placeholder="Enter Client Name" class="col-sm-4 col-form-label" style="text-align: left;"> Client Name
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									<!-- <span style="color: red;">*</span></label>
									<div class="col-sm-8"> <input type="checkbox" id="litigation" name="litigation" class="cb" value="Litigation" onclick="litigationfunction('Yes')" id="view"><span class="cr">
                          <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                        </span></div>
                    </div> -->

                    <div class="form-group col-sm-6 row"  id="merc_by_id" >

                    	<div class="col-sm-4 col-form-label">
                    		<label>&nbsp;&nbsp;Court / Forum
                    		</label>
                    	</div>
                    	<div class="col-sm-8">
                    		<select id="merc_by" name="merc_by"   class="form-control select2" >
                    			<option value=""> Please select</option>
														<?php $courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `organisation_id`='".$_SESSION['organisation_id']."' ORDER BY court_id ASC");
														while($selcourtlist = mysqli_fetch_array($courtname)){?>
															<option value="<?php echo $selcourtlist['court_name'];?>"><?php echo $selcourtlist['court_name'];?></option>
														<?php } ?>
														<option value="Others">Others</option>
                    			 
                    		</select>
                    		<input type="text" id="court_other" name="case_number" class="form-control"  style="display: none;" >
                    	</div>
                    </div>

<!-- <div class="form-group col-sm-6 row"   style="display: none;" id="merc_against_id" >

	<div class="col-sm-4 col-form-label">
		<label>Court / Forum</label>
	</div>
	<div class="col-sm-8">
		<select id="merc_against" name="merc_against"   class="form-control select2" >
			<option value="">Select</option>
			<option value="Consumers
">Consumers
</option>
			<option value="Consumer Representative
">Consumer Representative

</option>
			<option value="Contractors
">Contractors

</option>
			<option value="Vendors

">Vendors

</option>
			<option value="Employees

">Employees

</option>
			<option value="PIL

">PIL

</option>
			<option value="Before Arbitrator Tribunal

">CBefore Arbitrator Tribunal

</option>
			 
<option value="Others
">Others
</option>

		</select>

</div>
</div> -->


<div class="col-sm-6 row form-group">
	<label for="exampleInputUsername1" class="col-sm-4 col-form-label"> Case Number </label>
	<div class="col-sm-8">
		<!-- <textarea class="form-control" id="type_ofwork" placeholder="Enter Type of Work " name="description"></textarea> -->

		<input type="text" id="case_number" name="case_number" class="form-control"   ></div>
	</div>
	<div class="col-sm-6 row form-group">
		<label for="exampleInputUsername1" class="col-sm-4 col-form-label"> Case Year </label>
		<div class="col-sm-8">
			<select name="year" id="case_no_year" class="form-control ">
				<option value=''>Select Year</option>
				<?php $year = mysqli_query($connection, "SELECT * FROM `year` ORDER BY date_time DESC");
				while($yearsel = mysqli_fetch_array($year)){?>
					<option value='<?php echo $yearsel['date_time'];?>'><?php echo $yearsel['date_time'];?></option>
				<?php } ?>

			</select>	
		</div>
	</div>
	<div class="col-sm-6 row form-group">
		<label for="exampleInputUsername1" class="col-sm-4 col-form-label"> Date of Filing </label>
		<div class="col-sm-8">
			<div class="input-group">
				
				<input type="text" id="dateoffilling" class="form-control" name="dateoffiling" value=""/>
				<i class="fa fa-calendar input-group-addon" id="basic-addon1"></i>
				
			</div>
		</div>
	</div>
	
	<div class="col-sm-6 row form-group">
		<label for="exampleInputUsername1" class="col-sm-4 col-form-label"> Subject/ Matter </label>
		<div class="col-sm-8">
			<div class="input-group">
				
				<select name="subject_matter" id="subject_matter" class="form-control ">
					<option value='0'>Select Subject</option>
					<?php $category = mysqli_query($connection, "SELECT * FROM `name_of_matter` ORDER BY id ASC");
					while($categorys = mysqli_fetch_array($category)){?>
						<option value='<?php echo $categorys['nameofmatter'];?>'><?php echo $categorys['nameofmatter'];?></option>
					<?php } ?>

				</select>	
				
			</div>
		</div>
	</div>
	<div class="col-sm-6 row form-group">
		<label for="exampleInputUsername1" class="col-sm-4 col-form-label"> Risk Category </label>
		<div class="col-sm-8">
			<div class="input-group">
				
				<select name="risk_category" id="risk_category" class="form-control ">
					<option value='0'>Select Risk Category</option>
					<?php $category = mysqli_query($connection, "SELECT * FROM `categories` ORDER BY category_id ASC");
					while($categorys = mysqli_fetch_array($category)){?>
						<option value='<?php echo $categorys['category_id'];?>'><?php echo $categorys['category_name'];?></option>
					<?php } ?>

				</select>	
				
			</div>
		</div>
	</div>
	<div class="col-sm-6 row form-group">
		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
			<!-- <span style="color: red;">*</span> -->
			&nbsp;&nbsp;Title (Please enter the title which you can remember easily)

		</label>
		<div class="col-sm-8">
			<input type="text" name="title" class="form-control" id="title"  >	
		</div>

	</div>

<!-- <div class="form-group col-sm-6 row" >

	<div class="col-sm-4 col-form-label">
		<label>Documents</label>
	</div>
	<div class="col-sm-8">
		<div style="position:relative;">
                            <a class="btn btn-primary col-sm-12" href="javascript:;">
                              <input type="file" class="col-sm-12" id="upload" name="image" size="30" required="">
                            </a>
                            &nbsp;
                            <br>
                            <span class="" id="upload-file-info" style="color: black;"></span>
                          </div>

</div>
</div> -->






<div class="form-group col-sm-12" id="" >
	<label for="exampleInputUsername1" class="col-form-label"> Description 
		<!-- <span class="lbl-short-desc"><i>(Please enter primary details about the case, subject, CNR Number,etc)</i></span> -->
	</label>

	<!-- <textarea name="details" id="txtEngToHindi" class="form-control"></textarea>  -->
	<textarea name="details" id="query" class="ckeditor"></textarea> 
</div>
<div class="col-sm-12 grid-margin stretch-card">
	
	<div class="accordion accordion-solid-header" id="organization-accordion-4" role="tablist">
		

		<div class="card border-bottom" id="petitioner_div" style="display: none;">
			<div class="card-header" role="tab" id="heading-13">
				<h6 class="mb-0">
					<a class="collapsed" data-toggle="collapse" href="#collapse-13" aria-expanded="true" aria-controls="collapse-13">
						Petitioner 
					</a>
				</h6>
			</div>
			<div id="collapse-13" class="collapse show" role="tabpanel" aria-labelledby="heading-13" data-parent="#organization-accordion-4">
				
				<table id="petitioner_table" class="table order-list1" width="100">
					<thead>
						<tr>
							<th class="full-name">Full Name</th>
							<th>Email Address</th>
							<th>Phone Number</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> 
								<input type="text" name="name" class="form-control"  id="your_name0" autocomplete="off"/>
							</td>
							<td>
								<input type="mail" name="mail"  class="form-control" id="your_email0" autocomplete="off"/>
							</td>
							<td>
								<input type="text" name="phone"  class="form-control" id="your_mobile0" autocomplete="off"/>
							</td>
							<td>
							</td>
						</tr>
					</tbody>      
					<tfoot>
						<td colspan="4" style="text-align: right;"><button type="button" class="btn btn-sm btn-info add-opponent add-more-button" onclick="addPetitioner()" id="petitioner_add">  Add More </button>
						</td>
					</tfoot>                              
				</table>

			</div>
		</div>

		<div class="card border-bottom" id="respondent_div"  >
			<div class="card-header" role="tab" id="heading-11">
				<h6 class="mb-0">
					<a class="collapsed" data-toggle="collapse " href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">
						Respondent 
					</a>
				</h6>
			</div>
			<div id="collapse-11" class="collapse show" role="tabpanel" aria-labelledby="heading-11" data-parent="#organization-accordion-4">
				
				<table id="repondant_table" class="table order-list1" width="100">
					<thead>
						<tr>
							<th class="full-name">Full Name</th>
							<th>Email Address</th>
							<th>Phone Number</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td> 
								<input type="text" name="name" class="form-control"  id="opponent_name0" autocomplete="off"/>
							</td>
							<td>
								<input type="mail" name="mail"  class="form-control" id="opponent_email0" autocomplete="off"/>
							</td>
							<td>
								<input type="text" name="phone"  class="form-control" id="opponent_mobile0" autocomplete="off"/>
							</td>
							<td>
							</td>
						</tr>
					</tbody>      
					<tfoot>

						<td colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button btn btn-sm btn-info " onclick="addRespondant()" id="respondant_add">  Add More </button>
						</td>
					</tfoot>                              
				</table>

			</div>
		</div>

		
	</div>




</div>
<div class="col-sm-12 row">
	<div id="document-table" class="col-sm-12"></div>
</div>
<div class="col-sm-12 row">
	<div class="add_casesubmitdiv" style="
	padding-right: 0;
	">
	<button class="btn btn-info btn-sm add_casesubmit" id="organization_submit" name="add_case" type="button" onclick="submitMerc()"> Submit </button>&nbsp;&nbsp;
</div>
<!-- <button class="btn btn-info btn-sm" id="individual_submit" name="add_case" type="button" onclick="submitIndividualCase()">समावेश करा / Submit </button> -->
<!-- 					<div class="col-sm-6"> -->
	<a class="btn btn-danger btn-sm" href="add-casesmerc.php"> Cancel </a>
	<!-- </div> -->
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal" id="data">
	<div class="modal-dialog modal-lg fetchdata-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><b>Please Map the Following Details</b></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" style="padding: 0px;" id="modal-data">

			</div>

		</div>

	</div>
</div>
<?php include 'footer.php'; ?>

<script src="scripts/aes.js"></script>
<script src="scripts/pbkdf2.js"></script>
<script type="text/javascript" src="scripts/jsapi.js"></script>
<script type="text/javascript" src="scripts/nonjudicial.js"></script>
<script type="text/javascript" src="scripts/add_merc_case.js"></script>