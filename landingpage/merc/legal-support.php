 <?php include("header.php");  
 include("phpfile/sql_home.php");?> 
 <link rel="stylesheet" href="styles/case-report.css">
 <style type="text/css">
  th, td {white-space: normal;}
  .report-icon{
    font-size: 18px;
  }
  .case-input{
    margin-top: 9px;
  }
  .fetchdataloader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 25px;
    /*float: right;*/
    height: 25px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
  }
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <?php extract($_POST);
  //     if(isset($_POST['finalsearch'])){
  //      if(!empty($_POST['entercaseno'])){
  //       $conditionquery.= " AND `case_no`='".$_POST['entercaseno']."'";
  //     } 
  //     elseif(!empty($_POST['enterusername'])){
        
  //         $conditionquery.= " AND `user_id`='".$_POST['enterusername']."'";
        
  //     } 
  //     elseif(!empty($_POST['entercourt'])){
  //       $conditionquery.= " AND `court_id`='".$_POST['entercourt']."'";
  //     }   

  //   }
  //   if(isset($_POST['judicial_dropdown'])){
  //    $_POST['judicial_dropdown'];
  //  }
  //  if($_POST['Searchcaseno']!=''){
  //   $conditionquery.= " AND `case_no`='".$_POST['Searchcaseno']."' OR `court_name` Like '".$_POST['Searchcaseno']."'";
  // }if($_POST['fillter']!=''){
  //   $conditionquery.= " AND `case_title`='".$_POST['case_title']."' OR `case_no`='".$_POST['case_no']."'";
  // }
  // $status = $_REQUEST['status'];
  // if($status=="" && $Searchcaseno==''){
  //   $conditionquery .= " AND `case_status`='Running' "; 
  // }elseif($status=="All"){
  //   $conditionquery; 
  //   $status = $_REQUEST['status'];
  // }elseif($status!='All' && $status!='Contempt Petition' && $status!='judicial' && $_POST['Searchcaseno']==''){
  //   $conditionquery.= " AND `case_status`='".$status."' ";
  // }elseif($status=='Contempt Petition'){
  //   $conditionquery.= " AND `case_type`='".$status."'";
  // }elseif($status=='judicial'){
  //   $conditionquery.= " AND `new_status`='".$status."'";
  // }
  ?>
  <div class="pcoded-content">
    <div class="pcoded-inner-content">
      <div class="main-body">
        <div class="page-wrapper">
          <div class="page-header m-b-10">
            <h5 class="card-title">

              <b>Request Legal Support
              </b>  
            </h5>
          </div>
          <div class="page-body">
            <div class="card form-group">
              <div class="card-block">         
               <div class="col-sm-12">
                <!--  <ul class="nav nav-tabs form-group" role="tablist">
                  <li class="nav-item">
                   <a class="nav-link active" id="case-details-tab" data-toggle="tab" href="#activity-case-details" role="tab" aria-controls="home-1" aria-selected="true" onclick="judicial()">Judicial Cases</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link" id="case-details-tab" data-toggle="tab" href="#party-details" role="tab" aria-controls="home-1" aria-selected="true" onclick="nonJudicial()">Non-Judicial Cases</a>
                 </li>

               </ul> -->
               <div class="tab-content">
                <div class="tab-pane fade active show" id="activity-case-details">

                    <div class="  row " >
 
 <div class="col-sm-6 card-title" >
                 
                  <h5>Cases
                    (<span id="casesCounts"></span>)  </h5>
                    </div>
                <div class="col-sm-6 form-group" style="text-align: right;">
                  <!-- <a class="btn btn-secondary btn-sm  manage-buttons" href="add-casesmerc.php" >Add Case</a> -->
                  <!-- <button class="btn btn-sm btn-secondary" id="myBtn" onclick="casefilter()" style="">Filter</button> -->
                  <button class="btn btn-info btn-sm" type="button" id="myBtn-filter" data-target="#filter" data-toggle="modal">Filter</button>
                  <!-- <button class="btn btn-sm btn-secondary manage-buttons " id="myBtn" data-target="#export" data-toggle="modal" style="">Export</button> -->
                  <a class="btn btn-primary btn-sm manage-buttons  " href="viewLegalSupport.php" >View Legal Response</a>
                </div>

                

                <div class="col-sm-4 form-group" >
                </div>
                <div class="col-sm-8 form-group" id="showfilter" style="display: none;">

                 <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="card-title">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" onclick="closeFilter();">&times;</button>
                  </div>
                  <div class="modal-body"> <div class="row">
                    <form autocomplete="off" class="row col-sm-12" >
                   <div class="col-sm-4">
                     <label>Type of Litigation</label>
                     <select name="type_of_litigation" class="form-control" id="type_of_litigation"  >
                       <option value="">Select</option>
                       <option value="Litigations Filed by MSEDCL">Litigations Filed by MSEDCL</option>
                       <option value="Litigations Filed against MSEDCL">Litigations Filed against MSEDCL</option>
                     </select>
                   </div>


                   <div class="col-sm-4">
                     <label>Court/ Forum</label>
                     <select name="court_name" class="form-control" id="court_name"  >
                       <option value="">Select</option>
                       <option value="Recovery of Arrears (Civit Suite)">Recovery of Arrears (Civit Suite)</option>
                       <option value="CGRF">CGRF</option>
                       <option value="Electricty  Ombudsman">Electricty  Ombudsman</option>
                       <option value="High Court Orders ">High Court Orders</option>
                       <option value="MERC Orders">MERC Orders</option>
                       <option value="APLET Orders">APLET Orders</option>
                       <option value="CERC Orders">CERC Orders</option>
                       <option value="Civil Courts">Civil Courts</option>
                       <option value="District Courts">District Courts</option>
                       <option value="Consumer Forums">Consumer Forums </option>
                       <option value="SCDRC / NCDRC">SCDRC / NCDRC </option>
                       <option value="Labour  /Industrial Court">Labour  /Industrial Court</option>
                       <option value="Appellet Authority u/s. 127">Appellet Authority u/s. 127</option>
                       <option value="NCLT /NCLAT">NCLT /NCLAT</option>
                       <option value="Before Arbitrator , Tribunal">Before Arbitrator , Tribunal</option>
                       <option value="Others">Others</option>

                     </select>
                   </div>
                   <div class="col-sm-4">
                     <label>Case No.</label>
                     <input type="text" class="form-control form-group" placeholder="Enter Case" name="case_no" id="case_no">
                      <div id="managecase-box" class="managecase-box animated fadeInLeft"></div>
                   </div>
                   <div class="col-sm-4">
                     <label>Case Year</label>
                       <select name="case_no_year" id="case_no_year" class="form-control ">
                    <option value=''>Select Year</option>
                    <?php $year = mysqli_query($connection, "SELECT * FROM `year` ORDER BY date_time DESC");
                    while($yearsel = mysqli_fetch_array($year)){?>
                      <option value='<?php echo $yearsel['date_time'];?>'><?php echo $yearsel['date_time'];?></option>
                    <?php } ?>

                  </select> 
                   </div>

                   <div class="col-sm-4">
                     <label>Risk Category</label>
                     <select name="risk_category" id="risk_category" class="form-control ">
                    <option value=''>Select Risk Category</option>
                    <?php $category = mysqli_query($connection, "SELECT * FROM `categories` ORDER BY category_id ASC");
                    while($categorys = mysqli_fetch_array($category)){?>
                      <option value='<?php echo $categorys['category_id'];?>'><?php echo $categorys['category_name'];?></option>
                    <?php } ?>

                  </select>
                   </div>

                   <div class="col-sm-4"> 
                     <label>Title/ Subject Matter </label>
                     <input type="text" class="form-control form-group" id="case_title" name="case_title" placeholder=" ">
                   </div>


                   <div class="col-sm-12" style="text-align: right;">
                    <input type="button" name="fillter" class="btn-info btn-sm btn" style="" value="Submit" onclick="legalReportList(1,'filter')" >
                    <!-- <button class="btn-danger btn btn-sm" type="button" onclick="resetFilter()">Reset -->
                      <!-- </button> -->
                      <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal" onclick="closeFilter();">Close</button>
                    </div>
                    </form>
                  </div>
                </div>

              </div>

            </div>
          </div>
                <!--  <h5 class="card-title" style="text-align: right;">
                  <?php  echo $status; ?>  
                  <b>Cases
                    </b> (<?php  
                      $runnig = mysqli_query($connection, "SELECT COUNT(case_id) as merc_cases FROM `reg_cases` WHERE  ".$legal_support." ");  

                      $selrunnig = mysqli_fetch_array($runnig); echo $selrunnig['merc_cases'];?> ) 
                    </h5> -->
                    
              
            <div class="table-responsive judicialtable">
               <div id="showlegal_data"></div>
            <div class="pagination"></div>
              
                               
                              </div>
                            </div>
                             
         <div class="modal" id="export">
            <div class="modal-dialog modal-lg">

              <div class="modal-content" >
                <div class="modal-header">
                  <h4>Export</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row form-group col-sm-12">
                    <input type="hidden" name="checkboxvar[]" value="image">
                    <input type="hidden" name="checkboxvar[]" value="size">
                    <!-- <input type="hidden" name="checkboxvar[]" value="case_no"> -->
                    <!-- <input type="hidden" name="checkboxvar[]" value="case_no_year"> -->
                    <!-- <input type="hidden" name="checkboxvar[]" value="case_type"> -->
                    <!-- <input type="hidden" name="checkboxvar[]" value="case_title"> -->
                    <input type="hidden" name="checkboxvar[]" value="user_id">
                    <input type="hidden" name="checkboxvar[]" value="type">

                    <div class="form-radio col">
                      <div class="col-sm-5 radio radio-primary radio-inline">
                        <label>
                          <input type="radio" class="chkPassport" name="downloadas"  value="excel"> 
                          <i class="helper"></i>Excel
                        </label>
                      </div>
                      <div class="col-sm-6 radio radio-primary radio-inline">
                        <label>
                          <input type="radio" class="chkPassport" name="downloadas" value="pdf">      
                          <i class="helper"></i>PDF
                        </label>
                      </div>
                    </div>  
                    <div id="document-table-div-export" style="display: none;"></div>
                    <button type="button" class="btn btn-warning btn-sm" name="download_advocate" onclick="download_doc()">Download</button>
                  </div>
                </div>
              </div>

            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
 
 
 
  
                                                <?php include 'footer.php'; ?>
                                                <script type="text/javascript">
                                                  $(document).ready(function(){
                                                    $(".manage-case-li").addClass('active pcoded-trigger');
                                                  });
                                                </script>
                                                <script type='text/javascript' src='scripts/jspdf.min.js'></script>
                                                <script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
                                                <script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
                                                <!-- <script type="text/javascript" src="scripts/cases-report.js"></script> -->
                                                <script type="text/javascript" src="scripts/jquery.twbsPagination.js"></script>
                                                <script type="text/javascript" src="scripts/legal-support.js"></script>

