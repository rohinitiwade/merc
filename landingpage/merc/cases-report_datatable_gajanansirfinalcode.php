<?php
session_start();
$status = $_POST['id'];
## Database configuration
include 'includes/dbconnect.php';

## Read value
$draw = $_POST['draw'];
 $name = $_POST['name'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

## Search 
$searchQuery = " ";
if($searchValue != ''){
   $searchQuery = " and (court_name like '%".$searchValue."%' or 
		case_no like '%".$searchValue."%' or
		case_title like '%".$searchValue."%' or
		case_description like '%".$searchValue."%' or
		hearing_date like '%".$searchValue."%' or
        team_member like'%".$searchValue."%' ) ";
}

## Total number of records without filtering
$sel = mysqli_query($connection,"select count(*) as allcount from reg_cases WHERE case_district='".$_SESSION['cityName']."' AND `case_status`='".$status."'");
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of record with filtering
$sel = mysqli_query($connection,"select count(*) as allcount from reg_cases WHERE case_district='".$_SESSION['cityName']."' AND `case_status`='".$status."'");
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from reg_cases WHERE 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = mysqli_query($connection, $empQuery);

$data = array();
$i=$row+1;
while ($rows = mysqli_fetch_assoc($empRecords)) {
	$color_code = mysqli_query($connection,"SELECT * FROM `activity_priority` WHERE `priority_name`='".$rows['priority']."'");
 $colors = mysqli_fetch_array($color_code);
	$empQuery_join = "select * from todo_team tt LEFT JOIN law_registration lr ON tt.case_id = lr.reg_id WHERE tt.case_id = ".$rows['case_id'];
	$empRecords_join = mysqli_query($connection, $empQuery_join);
	$team_member = array();
	while ($rows_join = mysqli_fetch_assoc($empRecords_join)) {
		$team_member[] = $rows_join['name'].$rows_join['last_name'];		
	}		
	$team_member_list = implode(', ', $team_member);
	
	$data[] = array(
	  "case_id"=>$i,
	  "check_box"=>"<input type='checkbox' id='checkbox_".$rows['case_id']."' class='check' name='checkAll[]' data-id='".$rows['case_id']."' /><input type='hidden' name='color_code' id='color_code' value='".$colors['priority_color']."'>",
	  	  
      "court_name"=>$rows['court_name'],
      "case_no"=>$rows['hc_case_type']." ".$rows['case_no']." / ".$rows['case_no_year'],
      "case_title"=>$rows['name_of_matter'],
      "case_description"=>$rows['case_title'],
      "hearing_date"=>$rows['hearing_date'],
      "team_member"=>$team_member_list,//$rows['team_member'],
	  // "assign_to"=>"",
  //     "button_group"=>'<button type="button" class="btn btn-primary btn-xs form-group">Connected Cases</button>
		// <button type="button" class="btn btn-warning btn-xs" onclick="clrResponse('.$rows['case_id'].')">Seek Legal Help</button>',
      "view_case"=>'<a href="view_case.php?caseid='.base64_encode($rows['case_id']).'&&id='.$i.'" class="fa fa-eye" title="View" style="color: #402f2f;"></a>
		<span class="fa fa-edit" title="Edit"></span>
		<i class="fa fa-bar-chart-o" title="Analysis" style="margin: 0px 8px"></i>
		<i class="fa fa-trash" title="Delete"></i>',
   );
   //print_r($data);
	$i++;
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecordwithFilter,
  "iTotalDisplayRecords" => $totalRecords,
  "aaData" => $data
);
//print_r($response);
echo json_encode($response);


