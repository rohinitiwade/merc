<?php session_start(); ?> 
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <style type="text/css">
      .row{
        margin-left: 0;
        margin-right: 0;
      }
      .form-group {
        margin-bottom: 10px;
      }
      .action-th{
        width: 1%;
      }
      .particulars-th{
        width: 40%;
      }
      .tax-th{
        width: 20%;
      }
      .total-th{
        width: 10%;
      }
    </style>
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header m-b-10">
              <h5 class="card-title">Invoice <span></span></h5>
            </div>
            <div class="page-body">
              <div class="card form-group">
                <div class="card-header">
                  <div class="col-sm-12 text-right">
                     <button class="btn btn-info btn-mini" type="button">Edit</button>
                     <button class="btn btn-warning btn-mini" type="button">Enter Payment</button>
                     <button class="btn btn-primary btn-mini" type="button">Download</button>
                  </div>
                 
                </div>
                <div class="card-block">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="col-sm-12 form-group">
                        <label>Bill From: </label>
                        <h6 class="m-0">Adv. Vidhi Sharma</h6>
                        <p class="m-0">Nagpur India</p>
                      </div>
                      <div class="col-sm-12 form-group">
                        <label>Bill To: </label>
                        <h6 class="m-0">Madhukar</h6>
                        <p class="m-0">Nagpur India</p>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="col-sm-6 form-group f-right">
                        <img src="uploads_profile/femaleicon.png" width="100%" height="115px">
                      </div>
                      <div class="col-sm-8 form-group f-right">
                        <table class="table table-bordered">
                          <tbody><tr>
                            <td class="bg-primary p-1">Invoice Number:</td>
                            <td class="p-1">3</td>
                          </tr>
                          <tr>
                            <td class="bg-primary p-1">Issue Date:</td>
                            <td class="p-1">February 03, 2020</td>
                          </tr>
                          <tr>
                            <td class="bg-primary p-1">Balance Due:</td>
                            <td class="p-1">INR 2.00</td>
                          </tr>
                        </tbody></table>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="table table-resposive">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Particulars</th>
                              <th>Quantity</th>
                              <th>Cost</th>
                              <th>Tax %</th>
                              <th>Total(INR)</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Invoice Number:</td>
                              <td>3</td>
                              <td>Invoice Number:</td>
                              <td>3</td>
                              <td>3</td>
                            </tr>
                            <tr>
                              <td>Issue Date:</td>
                              <td>February 03, 2020</td>
                              <td>Invoice Number:</td>
                              <td>3</td>
                              <td>3</td>
                            </tr>
                            <tr>
                              <td>Balance Due:</td>
                              <td>INR 2.00</td>
                              <td>Invoice Number:</td>
                              <td>3</td>
                              <td>3</td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td colspan="2" rowspan="4"></td>
                              <th colspan="2">Subtotal</th>
                              <th>52.00</th>
                            </tr>
                            <tr>
                             <!--  <td colspan="2"></td> -->
                             <th colspan="2">Total</th>
                             <th>52.00</th>
                           </tr>
                           <tr>
                            <!-- <td colspan="2"></td> -->
                            <td colspan="2">Amount Paid</td>
                            <td>50.00</td>
                          </tr>
                          <tr>
                            <!-- <td colspan="2"></td> -->
                            <td colspan="2" class="bg-primary">Balance Due</td>
                            <td class="bg-primary">2.00</td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                  <div class="col-sm-12 form-group f-right">
                    <label>Payment History</label>
                    <table class="table table-bordered">
                      <thead>
                        <tr class="bg-primary">
                          <th>1</th>
                          <th>2</th>
                          <!-- <th>3</th> -->
                        </tr>
                      </thead>
                      <tbody><tr>
                        <td>Invoice Number:</td>
                        <td>3</td>
                      </tr>
                      <tr>
                        <td>Issue Date:</td>
                        <td>February 03, 2020</td>
                      </tr>
                      <tr>
                        <td>Balance Due:</td>
                        <td>INR 2.00</td>
                      </tr>
                    </tbody></table>
                  </div>
                  <div class="col-sm-12 form-group f-right">
                    <label>Invoice History</label>
                    <table class="table table-bordered">
                      <thead>
                        <tr class="bg-primary">
                          <th>1</th>
                          <th>2</th>
                          <!-- <th>3</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Invoice Number:</td>
                          <td>3</td>
                        </tr>
                        <tr>
                          <td>Issue Date:</td>
                          <td>February 03, 2020</td>
                        </tr>
                        <tr>
                          <td>Balance Due:</td>
                          <td>INR 2.00</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


<?php include ('footer.php'); ?>
<!-- <script type="text/javascript" src="scripts/create-invoice.js"></script> -->