 <?php include("header.php");  
 include("phpfile/sql_home.php");?> 
 <!-- <link rel="stylesheet" href="styles/case-report.css"> -->
 <link rel="stylesheet" type="text/css" href="styles/tree_style.min.css">
 <style type="text/css">
  th, td {white-space: normal;}
  .report-icon{
    font-size: 18px;
  }
  .case-input{
    margin-top: 9px;
  }
  .viewDoc{
    font-size: 12px;
    text-decoration: underline;
    color: #01a9ac;
  }
  .heading_legal{
    font-size: 16px;
    text-align: center;
    padding-bottom: 2%;
  }
  #LegalResponse td{
    font-size: 14px;
    padding: 0.7rem;
  }
  .subhead_legal{
    width: 25%;
  }
  .viewanswer{
    font-size: 16px;
  }
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <?php extract($_POST);
  //     if(isset($_POST['finalsearch'])){
  //      if(!empty($_POST['entercaseno'])){
  //       $conditionquery.= " AND `case_no`='".$_POST['entercaseno']."'";
  //     } 
  //     elseif(!empty($_POST['enterusername'])){

  //         $conditionquery.= " AND `user_id`='".$_POST['enterusername']."'";

  //     } 
  //     elseif(!empty($_POST['entercourt'])){
  //       $conditionquery.= " AND `court_id`='".$_POST['entercourt']."'";
  //     }   

  //   }
  //   if(isset($_POST['judicial_dropdown'])){
  //    $_POST['judicial_dropdown'];
  //  }
  //  if($_POST['Searchcaseno']!=''){
  //   $conditionquery.= " AND `case_no`='".$_POST['Searchcaseno']."' OR `court_name` Like '".$_POST['Searchcaseno']."'";
  // }if($_POST['fillter']!=''){
  //   $conditionquery.= " AND `case_title`='".$_POST['case_title']."' OR `case_no`='".$_POST['case_no']."'";
  // }
  // $status = $_REQUEST['status'];
  // if($status=="" && $Searchcaseno==''){
  //   $conditionquery .= " AND `case_status`='Running' "; 
  // }elseif($status=="All"){
  //   $conditionquery; 
  //   $status = $_REQUEST['status'];
  // }elseif($status!='All' && $status!='Contempt Petition' && $status!='judicial' && $_POST['Searchcaseno']==''){
  //   $conditionquery.= " AND `case_status`='".$status."' ";
  // }elseif($status=='Contempt Petition'){
  //   $conditionquery.= " AND `case_type`='".$status."'";
  // }elseif($status=='judicial'){
  //   $conditionquery.= " AND `new_status`='".$status."'";
  // }
      ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">

                  <b>View Legal Response
                  </b>  
                </h5>
              </div>
              <div class="page-body">
                <div class="card form-group">
                  <div class="card-block">         
                    <div class="col-sm-12">
                    </ul>  
                    <div class="tab-content">
                      <div class="tab-pane fade active show" id="activity-case-details">
                       <h5 class="card-title" style="text-align: right;">

                        <b>Cases
                        </b> (<span id="viewResponseCount"></span> ) 
                      </h5>

                      <div id="show_data"></div>
                      <div class="pagination"></div>                 
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="tree-modal" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 928px;">
          <div class="modal-content">
            <div class="" style="background-color: white;">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color: #f10038;
              text-align: center;">Legal Audit Report</h4>

            </div>
            <hr>
            <div class="modal-body">

              <div id="legal_data"></div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>


        </div>


      </div>

      <div id="view-answer" class="modal fade" role="dialog">
        <div class="modal-dialog" style="max-width: 928px;">
          <div class="modal-content">
            <div class=" " style="background-color: white;">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color: #f10038;
              text-align: center;">Answers</h4>

            </div>

            <div class="modal-body">
              <div class="col-sm-12">
                <div class="col-sm-12 viewanswer"><b> <span class="queclass">Question: </span>
                  <span>one two three four</span></b></div>
                  <div class="col-sm-12 viewanswer"><b> <span class="queclass">Answer:  </span></b>
                    <span>one two three four</span></div>
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div>


            </div>


          </div>


          <?php include 'footer.php'; ?>
          <script type="text/javascript">
            $(document).ready(function(){
                                                    // $(".manage-case-li").addClass('active pcoded-trigger');
                                                  });
                                                </script>
                                                <script type='text/javascript' src='scripts/jspdf.min.js'></script>
                                                <script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
                                                <script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
                                                <!-- <script type="text/javascript" src="scripts/cases-report.js"></script> -->
                                                <script type="text/javascript" src="scripts/jquery.twbsPagination.js"></script>
                                                <script type="text/javascript" src="scripts/legal-support.js"></script>

