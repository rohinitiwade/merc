<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> 
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Edit Timesheet </h5>
              </div>
              <div class="page-body">
                <div class="card form-group">                 
                  <div class="card-block">
                <?php
                $seltimesheet=mysqli_query($connection,"SELECT * from `add_timesheet` where id='".base64_decode($_GET['id'])."'");
                $row=mysqli_fetch_array($seltimesheet);
                $selcase=mysqli_query($connection,"SELECT at.*,rg.* from add_timesheet as at,reg_cases as rg where  at.case_id=rg.case_id AND at.case_id='".$row['case_id']."'");
                $fetcase = mysqli_fetch_array($selcase);
                $selmm=mysqli_query($connection,"SELECT * FROM `add_timesheet` WHERE `case_id`='".$row['case_id']."' AND  `id`='".base64_decode($_GET['id'])."'");
                $fetmm = mysqli_fetch_array($selmm);

                if(isset($_POST['submit'])){
                extract($_POST);
                $date = date('Y-m-d H:i:s');
                if($case!=""){
               $update = mysqli_query($connection, "UPDATE `add_timesheet` SET `case_id`='".$case."',`edate`='".$edate."',`particulars`='".$particulars."',`timespent`='".$timespent."',`timeinmin`='".$timeinmin."',`type`='".$type."',`datetime`='".$date."'
     WHERE `id`='".base64_decode($_GET['id'])."' AND `user_id` ='".$_SESSION['user_id']."'");
                if($update){
                ?>
                <script type="text/javascript">
                    //alert('Timesheet update Succefully');
                    window.location.href='manage-timesheets';
                 </script>
          <?php } } }?>
                    <form class="forms-sample" method="POST" action="">
                      <div class="form-group row forms-sample-div" style="">
                        <div class="col-sm-6">
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Case
                            </label>
                            <div class="col-sm-8">
                               <select class="form-control" id="document" name="case">
                       <option value="<?php echo $row['case_id'];?>"><?php if($fetcase['case_id']){ echo $fetcase['case_type'];?> / <?php echo $fetcase['case_no'];?> / <?php echo $fetcase['case_no_year'];?> / <?php echo $fetcase['case_title'];}else{?>Select Case<?php }?></option>
                       <?php $case = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_no`!='' AND `case_district`='".$_SESSION['cityName']."' AND `remove_status`=1");
                       while($casesel = mysqli_fetch_array($case)){?>
                         <option value="<?php echo $casesel['case_id'];?>"><?php echo $casesel['case_type'];?> / <?php echo $casesel['case_no'];?> / <?php echo $casesel['case_no_year'];?> / <?php echo $casesel['case_title'];?></option>
                       <?php } ?>
                     </select>

                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;"><b>*</b>
                              </span>&nbsp;&nbsp;Particulars
                            </label>
                            <div class="col-sm-8">
                              <textarea class="form-control" name="particulars" placeholder="Enter Particulars Here" required=""><?php echo $row['particulars']; ?></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;">
                                <b>*</b>
                              </span>&nbsp;&nbsp;Date
                            </label>
                            <div class="col-sm-8">
                              <input type="text" name="edate" placeholder="Enter Date" id="datepickertime" class="form-control" required="" value="<?php echo $row['edate']; ?>">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;">
                                <b>*</b>
                              </span>&nbsp;&nbsp;Time Spent(in Hours)
                            </label>
                            <div class="col-sm-4 form-group">
                              <input type="hidden" id="timespent_hidden" value="<?php echo $row['timespent'];?>">
                              <select class="form-control" name="timespent" id="time-spent">
                              </select>
                            </div>
                            <div class="col-sm-4 form-group">
                              <input type="hidden" id="timeinmin_hidden" value="<?php echo $row['timeinmin'];?>">
                              <select class="form-control" name="timeinmin" id="time-in-min">
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12 row">
                        <label class="col-sm-2">Type</label>
                        <div class="col form-radio">
                          <div class="radio radio-inline">
                            <label>
                              <input type="radio" value="Effective" name="type" <?php echo ($row['type'] =='Effective')? 'checked':'' ?>>
                              <i class="helper"></i> Effective
                            </label>
                          </div>
                          <div class="radio radio-inline">
                            <label>
                              <input type="radio" value="Non Effective / Procedural Appearance" name="type" <?php echo ($row['type'] =='Non Effective / Procedural Appearance')? 'checked':'' ?>>
                              <i class="helper">
                              </i> Non Effective / Procedural Appearance
                            </label>
                          </div>
                        </div>
                        <div class="col-sm-4">
                        <input class="btn btn-sm btn-info newsubmit" value="Submit" type="submit" name="submit"> &nbsp;
                        <a href="manage-timesheets.php" class="btn btn-sm btn-danger">Cancel
                        </a>
                      </div>
                      </div>
                    </form> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php'; ?>
  <script type="text/javascript" src="scripts/update-timesheet.js"></script>
  