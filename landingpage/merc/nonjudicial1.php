<?php include("header.php") ?>
<!-- <?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?>  -->
<?php $pageNam =  basename($_SERVER["PHP_SELF"]);
$insert = mysqli_query($connection, "INSERT INTO `frequency_report` SET `user_id`='".$_SESSION['user_id']."',`case_id`='".base64_decode($_GET['caseid'])."',`division`='".$_SESSION['cityName']."',`page_name`='".$pageNam."',`date_time`='".date("Y-m-d H:i:s")."',`dates`='".date("Y-m-d")."'");?>
<!-- <link rel="stylesheet" href="css/vertical-layout-light/add_cases.css"> -->
<link rel="stylesheet" type="text/css" href="styles/view_case.css">
<!-- <link rel="stylesheet" type="text/css" href="styles/j-forms.css"> -->
<link rel="stylesheet" type="text/css" href="styles/jquery.datetimepicker.css">

<style type="text/css">
   .mobile-view-tr{
      display: none;
   }
   .action_th{
      width: 13%!important;
   }
</style>
<body>
   <?php $case = mysqli_query($connection, "SELECT `case_id`,`client_name`,`type_of_work`,`case_title`,`user_id` FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
   $selcase = mysqli_fetch_array($case);
   // $get_cases = mysqli_query($connection,"SELECT * FROM `case_law_search` WHERE `case_id`='".base64_decode($_GET['caseid'])."' AND `remove_status`=1");
   // $count_law = mysqli_num_rows($get_cases);
   // $todo_cases = mysqli_query($connection,"SELECT DISTINCT `todo_id` FROM `todo_list` WHERE `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' ORDER BY todo_id DESC");
   // $cnt_todocases = mysqli_num_rows($todo_cases);
   ?>
   <div class="pcoded-main-container">
      <div class="pcoded-wrapper">
         <?php include("menu.php") ?>
         <div class="pcoded-content">
            <div class="pcoded-inner-content">
               <div class="main-body">
                  <div class="page-wrapper">
                     <div class="page-header m-b-10">
                        <h5 class="card-title"></h5>
                        <div class="page-body">
                           <div class="card form-group">
                              <div class="row">
                                 <input type="hidden" id="account_type" name="account_type" value="<?php echo $_SESSION['account'];?>">
                                 <div id="note_export"></div>
                                 <div id="complete_view_data" style="display: none;">
                                    <h2 id="case_type_export_report" style="text-align: center;"></h2>
                                    <div id="details_main_left"></div>

                                    <div id="team-member_export_report" ></div>
                                    <div id="activity_export_report"></div>
                                    <div id="hearindata_export_report" ></div>
                                    <div id="documentList_export" ></div>
                                    <div id="connected_case_export"></div>
                                    <div id="timesheet_export"></div>
                                    <div id="notes_export"></div>
                                 </div>
                                 <div class="card-header col-sm-12 row form-group p-0">
                                    <h4 class="col-sm-10 p-0 court_title_heading">
                                       <b id="district_court_title"><?php echo $selcase['client_name']?></b>
                                    </h4>
                                    <!-- <div class="col-sm-2 p-0 form-group">
                                       <div class="col-sm-12 row p-0" id="right-side-div-view" style="margin: 0px">

                                          <div class="col-sm-12 p-0">
                                             <select class="form-control" id="set_priority_select">
                                                <option value="">Set priority</option>
                                                <option value="Super Critical">Super Critical</option>
                                                <option value="Critical">Critical</option>
                                                <option value="Important">Important</option>
                                                <option value="Routine">Routine</option>
                                                <option value="Others">Others</option>
                                                <option value="Normal">Normal</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div> -->
                                 </div>
                                 <div class="col-sm-3 form-group p-r-0" style="text-align: left;">
                                    <a href="edit-nonjudicialcase.php?caseid=<?php echo base64_encode($selcase['case_id']); ?>" class="p-0 btn btn-link btn-md left_buttons viewcasebuttons" style="color: black"><i class="feather icon-edit viewcasebuttons" aria-hidden="true"></i> Edit</a>
                                    <?php if($selcase['user_id'] == $_SESSION['user_id']){?><button type="button" style="color: black" onclick="deletecase(<?php echo $selcase['case_id']; ?>)" class="p-0 left_buttons btn btn-link btn-md viewcasebuttons" ><i class="feather icon-trash viewcasebuttons"></i> Delete </button><?php } ?>
                                    
                                    <a href="nonjudicial_addcase.php" style="color: black" class="p-0 btn btn-link btn-md left_buttons viewcasebuttons" ><i class="feather icon-plus viewcasebuttons" aria-hidden="true"></i> Add </a>
                                    <button class="p-0 btn btn-link btn-md left_buttons viewcasebuttons" type="button" onclick="printCase()" style="color: black"><i class="feather icon-printer" aria-hidden="true"></i> Print</button>
                                    <!-- <span style="cursor:pointer" class="opensideview" onclick="openNav()">&#9776; Click to open side panel</span> -->
                               <!--   <div id="mySidenav" class="sidenav">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                    <div class="court_data"></div>
                                    <div class="casetype"></div>
                                    <div class="stage"></div>
                                    <div class="petitioner"></div>
                                    <div class="petitionerAdv"></div>
                                    <div class="respondent"></div>
                                    <div class="respondent_adv"></div> 
                                    <div class="history_date"></div> 
                                 </div> -->
                                <!--  <div class="col-sm-12 p-0">
                                    <a href="cases-report.php" class="btn btn-warning btn-sm left_buttons viewcasebuttons"><i class="feather icon-file-text-o viewcasebuttons" aria-hidden="true"></i> Manage Cases</a>
                                 </div> -->
                              </div>
                              <div class="col-sm-7 form-group" style="text-align: center;">
                                 <!-- <h4 class="fetch_case_title" id="sub_case_nominal" style="display: none;"></h4> -->
                                 <!-- <h4 class="fetch_case_title p-0" id="sub_case_title"><?php echo $selcase['type_of_work'];?></h4> -->
                                 <!-- <h5 class="fetch_case_desc p-0" id="sub_case_desc"><?php echo $selcase['case_title'];?></h5> -->
                              </div>
                             <!--  <div class="col-sm-3 autoupdate" id="auto-update-div" style="text-align: right;"><span>Auto Update</span>
                                 <label class="toggle-switch toggle-switch-danger">                
                                    <input type="checkbox" checked="" id="auto_update" onchange="getAutoUpdate()">
                                    <span class="toggle-slider round"></span>
                                     
                                 </label>
                              </div> -->
                              <!--  <div class="col-sm-3" id="case_disposed" style="text-align: right;display: none;">
                                    <h5 id="closed-status"></h5>
                                 </label>
                              </div> -->
                              <input type="hidden" name="caseid" id="caseid" value="<?php echo base64_decode($_GET['caseid']);?>">
                              <input type="hidden" name="courtid" id="court-id" value="<?php echo $selcase['court_id'];?>">
                              <input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>">
                              <input type="hidden" name="stamp" id="stamp" value="">
                              <input type="hidden" name="side" id="side" value="">
                              <input type="hidden" name="bench" id="bench" value="">
                              <input type="hidden" name="casetype" id="case_type" value="">
                              <input type="hidden" name="case_no_year" id="case_no_year" value="">
                              <input type="hidden" name="case_no" id="case_no" value="">
                              <h3 class="card-title" style="font-size: 20px;"></h3>
                              <div class="col-sm-12">
                                 <ul class="nav nav-tabs form-group" role="tablist">
                                    <li class="nav-item">
                                       <a class="nav-link active" id="case-details-tab" data-toggle="tab" href="#activity-case-details" role="tab" aria-controls="home-1" aria-selected="true">Case Details</a>
                                    </li>
                                       <li class="nav-item">
                                       <a class="nav-link" id="team-member-tab" data-toggle="tab" href="#team-member" role="tab" aria-controls="profile-1" aria-selected="false"> Team Members (<span id="team_member_count"></span>)</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" onclick="getUploadedDoc('d')" id="document-tab" data-toggle="tab" href="#documents_li" role="tab" aria-controls="contact-1" aria-selected="false">Documents (<span id="view_case_document"></span>)</a>
                                    </li>
                                    
                                 </ul>
                                 <div class="tab-content">
                                    <div class="tab-pane fade active show" id="activity-case-details">
                                     <div id="desktop-view-div">
                                       <div class="sidebar-case-section utype-lawyers row">
                                          <div class="court_data col-sm-4"></div>
                                          <div class="casetype col-sm-4">                       
                                          </div>
                                          <!-- <div class="history_date col-sm-4"></div> -->
                                          <div id="desktop-view-div " class="col-sm-4">
                                             <div class="sidebar-case-section row utype-lawyers">
                                                <div class="petitioner col-sm-12" style="display: none;"></div>
                                                <div class="petitionerAdv col-sm-12" style="display: none;"></div>
                                                <div class="respondent col-sm-12" style="display: none;"></div>
                                                <div class="respondent_adv col-sm-12" style="display: none;"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                   <!--  <div class="tab-pane fade" id="party-details">
                                       
                                   </div> -->
                                   <div class="tab-pane fade" id="activity-history" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="media col-sm-12 p-l-0">
                                       <div class="media-body col-sm-12 p-l-0">
                                          <div class="form-group row" style="float: right;">
                                             <select class="form-control" style="width: 104px;" id="export-to-pdf">
                                                <option disabled selected hidden>Export</option>
                                                <option value="exprt">as a PDF</option>
                                             </select>
                                             <button style="border: 0px;padding: 9px 12px;background-color: #dddddd;margin-left: 10px;">Filter</button>
                                          </div>
                                          <div id="show_view_case_default"></div>
                                          <div id="show_view_case"></div>
                                          <div id="show_hearing_data_list" class="table-responsive"></div>
                                          <textarea name="seeklegal" id="query" ></textarea>
                                         <!--  <table id="inform-to-client-tbl" class="table table-bordered">
                                             <tbody>
                                                <tr>
                                                   <th class="client_inform">Client Name</th>
                                                   <th class="email_inform">Email Address</th>
                                                   <th class="phone_inform">Phone Number
                                                      <span class="sms-helper-info">(Maximum character limit is 1,500 for SMS)</span>
                                                   </th>
                                                </tr>
                                                <tr id="artivity-inform-client-wrapper-activity-client-87642" class="disabled ">
                                                   <td data-th="Client">1) Triratna Baudha Mahasangh Sahayak Gan1</td>
                                                   <td data-th="Email Address">
                                                      <input type="checkbox" disabled><label for="email-activity-client-87642"> (no email)</label>
                                                   </td>
                                                   <td data-th="Phone Number (Max character limit is 1,500 for SMS)">
                                                      <input type="checkbox" disabled>
                                                      <label for="mobile-activity-client-87642"> (no phone number)</label>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table> -->
                                         <!--  <div class="col-sm-12 row p-0">
                                             <b class="col-sm-5 d-flex">Would you like to record next hearing date or resolution?</b>
                                             <div class="form-radio">
                                                <div class="radio radio-inline">
                                                   <label>
                                                      <input type="radio" class="chkPassport" name="recordHearingradio" value="Yes">
                                                      <i class="helper"></i> Yes
                                                   </label>
                                                </div>
                                                <div class="radio radio-inline">
                                                   <label>
                                                      <input type="radio" class="chkPassport" name="recordHearingradio" value="No" checked>
                                                      <i class="helper"></i> No
                                                   </label>
                                                </div>
                                             </div>
                                          </div> -->
                                         <!--  <div class="case-activity-info" style="">
                                             <div class="form-group col-sm-3" style="">
                                                <label class="control-label" for="caseactivityinfo-case_status">Status:</label>
                                                <div class="selector hover" id="uniform-caseactivityinfo-case_status">
                                                   <select id="record_status" class="form-control" name="CaseActivityInfo[case_status]" style="">
                                                      <option value="Next Hearing" selected="">Running</option>
                                                      <option value="Disposed">Closed</option>
                                                      <option value="Transferred/NOC">Transferred/NOC Given</option>
                                                      <option value="Direction Matter">Direction Matters</option>
                                                      <option value="Order">Order Reserved</option>
                                                   </select>
                                                </div>
                                             </div>
                                             <div class="row col-sm-12" error="dd-error">
                                                <label class="control-label col-sm-2 d-flex">Who Attended:</label>
                                                <div id="who_attended_div"></div>
                                                   
                                                   <div class="col-sm-4" id="who_attended_others_div">
                                                      <input type="text" class="form-control" id="who_attended_others" name="">
                                                   </div>
                                                </div>
                                                <div class="col-sm-12 row">
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" for="caseactivityinfo-stage">Stage:</label>
                                                      <input type="text" id="caseactivityinfo-stage" class="form-control" name="CaseActivityInfo[stage]" maxlength="255">
                                                   </div>
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" for="caseactivityinfo-posted_for">Posted For:</label>
                                                      <input type="text" id="caseactivityinfo-posted_for" class="form-control" name="CaseActivityInfo[posted_for]" maxlength="255">
                                                   </div>
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" for="caseactivityinfo-action_taken">Action Taken:</label>
                                                      <input type="text" id="caseactivityinfo-action_taken" class="form-control" name="CaseActivityInfo[action_taken]" maxlength="255">
                                                   </div>
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" id="hearing-date-label"> Next Hearing Date:</label>
                                                      <input type="text" id="caseactivityinfo-next_hearing" class="form-control">
                                                   </div>
                                                   
                                                </div>
                                             </div> -->
                                             <!-- <button class="btn btn-primary btn-sm" type="button" style="float: right;" onclick="submitCaseComment('submit')"> Submit</button> -->
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="team-member" role="tabpanel" aria-labelledby="tm-tab">
                                       <div class="media">
                                          <input type="hidden" id="hidSelectedOptions" value="1,44" name="">
                                          <div class="media-body col-sm-12 table-responsive" id="team-member-table-div">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade"  id="documents_li" role="tabpanel" aria-labelledby="document-tab" >
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <div class="tab-pane" id="documenttab" role="tabpanel" aria-labelledby="documenttab">
                                                <form method="post" action="#" id="#">
                                                   <div class="form-group files">
                                                      <label>Upload Your File </label>
                                                      <input type="file" class="form-control form-group" multiple="" id="upload_doc"  accept="image/x-png,image/jpeg,application/pdf,application/msword" />
                                                   </div>
                                                   <div class="form-group row col-sm-12">
                                                      <div class="col-sm-4">
                                                         <select class="form-control" id="doc_type" name="doc_type">
                                                            <option value="">Please select document type</option>
                                                            <?php $document = mysqli_query($connection, "SELECT * FROM `documents_type` ORDER BY documents_type ASC");
                                                            while($seldocmnet = mysqli_fetch_array($document)){?>
                                                               <option value="<?php echo addslashes($seldocmnet['documents_type']);?>"><?php echo addslashes($seldocmnet['documents_type']);?></option>
                                                            <?php } ?>
                                                         </select>
                                                      </div>
                                                      <div class="col-sm-3">
                                                         <button class="btn btn-sm btn-primary newsubmit"  type="button" id="upload_documents" value="Upload" name="Upload" onclick="getUploadedDoc('u')">Upload</button>
                                                      </div>
                                                   </div>
                                                   <!-- <hr>
                                                   <div class="row col-sm-12">
                                                      <div class="col-sm-3 form-group">
                                                         <select class="form-control" id="search-doc-filter">
                                                            <option value="">NA</option>
                                                            <option value="caseno">Case No</option>
                                                            <option value="petitioner">Petitioner</option>
                                                            <option value="respondent">Respondent</option>

                                                         </select>
                                                      </div>
                                                      <div class="col-sm-3 form-group" id="search-document-div" style="display: none;">
                                                        <input type="text" class="form-control" id="search-in-document">
                                                     </div>
                                                     <div class="col-sm-3 form-group">
                                                      <button type="button" class="btn btn-warning btn-sm">Search in Document</button>
                                                   </div>
                                                </div> -->
                                                <hr>
                                                <div id="documentList" class="table-responsive col-sm-12"></div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal" id="view_case_document_modal">
   <div class="modal-dialog modal-lg">
      <div id="casedocument"></div>
   </div>
</div>



<!-- <div class="modal" id="notes_image_modal">
   <div class="modal-dialog">

      <div class="modal-content">
       <div class="modal-header">
        <h5 class="modal-title" style="font-weight: bold;">Notes Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">×</span>
      </button>
   </div>
   <div class="modal-body">
     <iframe src="" frameborder="0" width="100%" height="400px" id="notes-file-iframe"></iframe>
  </div>

  <div class="modal-footer">
   <button type="button" class="btn btn-info close_btn" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button> -->
</div>
</div>

</div>
</div>  

<?php include 'footer.php'; ?>
<script type="text/javascript">
   $(".view-case-li").addClass('active pcoded-trigger');
</script>
<script type='text/javascript' src='scripts/jspdf.min.js'></script>
<script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
<script type='text/javascript' src="scripts/aes.js"></script>
<script type='text/javascript' src="scripts/pbkdf2.js"></script>
<script type="text/javascript" src="scripts/jquery.datetimepicker.full.min.js"></script>
<!-- <script type="text/javascript" src="scripts/jsapi.js"></script> -->
<script type="text/javascript" src="scripts/view_noncase.js"></script>
<script type="text/javascript" src="scripts/export_view_case.js"></script>
<script type="text/javascript" src="scripts/print_view_case.js"></script>
<!-- <script type="text/javascript" src="scripts/seeklegal.js"></script> -->
<!-- <script type="text/javascript" src="scripts/connected_cases_add.js"></script> -->
<!-- <script type="text/javascript" src="scripts/timesheet.js"></script> -->
<!-- <script type='text/javascript' src='scripts/create_todo.js'></script> -->

<script src="scripts/jquery.dataTables.min.js"></script>
<script src="scripts/dataTables.bootstrap4.min.js" ></script>   
