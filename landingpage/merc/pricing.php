<link rel="stylesheet" type="text/css" href="styles/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="styles/datatable-buttons/buttons.dataTables.min.css">
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <?php $reg = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
$selreg = mysqli_fetch_array($reg);?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Subscription  Pricing 
                </h5>
              </div>
              <div class="page-body">
                <div class="card form-group">
                  <div class="card-block">
                    <div class="col-12 form-group">
                      <!-- <button type="button" class="btn btn-primary btn-sm" data-target="#logindetails" data-toggle="modal">User All Login History</button> -->
                      <!-- <a href="add-team.php" class="btn btn-info btn-sm  teamMember" ><i class="fa fa-plus" ></i>Add Team Member</a> -->
                      <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                    </div>
                    <div class="col-12 table-responsive">
                      <style>
                       .panel.price,.panel.price>.panel-heading{border-radius:0;-moz-transition:all .3s ease;-o-transition:all .3s ease;-webkit-transition:all .3s ease}.panel.price:hover{box-shadow:0 0 30px rgba(0,0,0,.2)}.panel.price:hover>.panel-heading{box-shadow:0 0 30px rgba(0,0,0,.2) inset}.panel.price>.panel-heading{box-shadow:0 5px 0 rgba(50,50,50,.2) inset;text-shadow:0 3px 0 rgba(50,50,50,.6)}.panel.price .list-group-item:last-child{border-bottom-right-radius:0;border-bottom-left-radius:0}.panel.price .list-group-item:first-child{border-top-right-radius:0;border-top-left-radius:0}.price .panel-footer{color:#fff;border-bottom:0;background-color:rgba(0,0,0,.1);box-shadow:0 3px 0 rgba(0,0,0,.3)}.panel.price .btn{box-shadow:0 -1px 0 rgba(50,50,50,.2) inset;border:0}.price.panel-green>.panel-heading{color:#fff;background-color:#57ac57;border-color:#71df71;border-bottom:1px solid #71df71}.price.panel-green>.panel-body{color:#fff;background-color:#65c965}.price.panel-green>.panel-body .lead{text-shadow:0 3px 0 rgba(50,50,50,.3)}.price.panel-green .list-group-item{color:#333;background-color:rgba(50,50,50,.01);font-weight:600;text-shadow:0 1px 0 rgba(250,250,250,.75)}.price.panel-blue>.panel-heading{color:#fff;background-color:#608bb4;border-color:#78aee1;border-bottom:1px solid #78aee1}.price.panel-blue>.panel-body{color:#fff;background-color:#73a3d4}.price.panel-blue>.panel-body .lead{text-shadow:0 3px 0 rgba(50,50,50,.3)}.price.panel-blue .list-group-item{color:#333;background-color:rgba(50,50,50,.01);font-weight:600;text-shadow:0 1px 0 rgba(250,250,250,.75)}.price.panel-red>.panel-heading{color:#fff;background-color:#d04e50;border-color:#ff6062;border-bottom:1px solid #ff6062}.price.panel-red>.panel-body{color:#fff;background-color:#ef5a5c}.price.panel-red>.panel-body .lead{text-shadow:0 3px 0 rgba(50,50,50,.3)}.price.panel-red .list-group-item{color:#333;background-color:rgba(50,50,50,.01);font-weight:600;text-shadow:0 1px 0 rgba(250,250,250,.75)}.price.panel-grey>.panel-heading{color:#fff;background-color:#6d6d6d;border-color:#b7b7b7;border-bottom:1px solid #b7b7b7}.price.panel-grey>.panel-body{color:#fff;background-color:grey}.price.panel-grey>.panel-body .lead{text-shadow:0 3px 0 rgba(50,50,50,.3)}.price.panel-grey .list-group-item{color:#333;background-color:rgba(50,50,50,.01);font-weight:600;text-shadow:0 1px 0 rgba(250,250,250,.75)}
                      </style>
                      <div class="container">
                        <div class="row">
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <div class="panel price panel-red">
                              <div class="panel-heading  text-center">
                                <h3>1 Year Subscrition</h3>
                              </div>
                              <div class="panel-body text-center">
                                <p class="lead" style="font-size:22px">
                                  <strong>8,500 /- Per  Year</strong>
                                </p>
                              </div>
                              <ul class="list-group list-group-flush text-center">
                                <li class="list-group-item">
                                  <i class="icon-ok text-danger"></i> Personal use
                                </li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-danger"></i> Unlimited Cases
                                </li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-danger"></i> 24/7 support
                                </li>
                              </ul>
                              <div class="panel-footer">
                                <form id="checkout-selection" action="pay.php" method="POST">
                                  <input type="hidden" name="item_name" value="1 Year Subscription">
                                  <input type="hidden" name="item_description" value="My Test Product Description">
                                  <input type="hidden" name="item_number" value="<?php echo $selreg['reg_id'];?>">
                                  <input type="hidden" name="amount" value="8500">
                                  <input type="hidden" name="address" value="<?php echo $selreg['address'];?>">
                                  <input type="hidden" name="currency" value="INR">
                                  <input type="hidden" name="cust_name" value="<?php echo ucfirst($selreg['name']);?> <?php echo ucfirst($selreg['last_name']);?>">
                                  <input type="hidden" name="email" value="<?php echo $selreg['email'];?>">
                                  <input type="hidden" name="contact" value="<?php echo $row['mobile'];?>">
                                  <input type="submit" class="btn btn-lg btn-block btn-danger" value="Buy Now!">
                                  </div>
                              </div>
                              <!-- /PRICE ITEM -->
                            </div>
                            </form>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <!-- PRICE ITEM -->
                            <div class="panel price panel-blue">
                              <div class="panel-heading arrow_box text-center">
                                <h3>2 Year Subscription</h3>
                              </div>
                              <div class="panel-body text-center">
                                <p class="lead" style="font-size:22px">
                                  <strong>15,000 /- 2 Year</strong>
                                </p>
                              </div>
                              <ul class="list-group list-group-flush text-center">
                                <li class="list-group-item">
                                  <i class="icon-ok text-info"></i> Personal use
                                </li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-info">
                                  </i> Unlimited Cases
                                </li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-info"></i> 24/7 support
                                </li>
                              </ul>
                              <div class="panel-footer">
                                 <form id="checkout-selection" action="pay.php" method="POST">
                                  <input type="hidden" name="item_name" value="2 Year Subscription">
                                  <input type="hidden" name="item_description" value="My Test Product Description">
                                  <input type="hidden" name="item_number" value="<?php echo $selreg['reg_id'];?>">
                                  <input type="hidden" name="amount" value="15000">
                                  <input type="hidden" name="address" value="<?php echo $selreg['address'];?>">
                                  <input type="hidden" name="currency" value="INR">
                                  <input type="hidden" name="cust_name" value="<?php echo ucfirst($selreg['name']);?> <?php echo ucfirst($selreg['last_name']);?>">
                                  <input type="hidden" name="email" value="<?php echo $selreg['email'];?>">
                                  <input type="hidden" name="contact" value="<?php echo $row['mobile'];?>">
                                  <input type="submit" class="btn btn-lg btn-block btn-info" value="Buy Now!">
                              </div>
                            </div>
                            <!-- /PRICE ITEM -->
                          </div>
                          </form>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <!-- PRICE ITEM -->
                            <div class="panel price panel-green">
                              <div class="panel-heading arrow_box text-center">
                                <h3>3 Year Subscription</h3>
                              </div>
                              <div class="panel-body text-center">
                                <p class="lead" style="font-size:22px">
                                  <strong>22,000 /- 3 Year</strong>
                                </p>
                              </div>
                              <ul class="list-group list-group-flush text-center">
                                <li class="list-group-item">
                                  <i class="icon-ok text-success"></i> Personal use</li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-success">
                                  </i> Unlimited Cases
                                </li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-success"></i> 24/7 support
                                </li>
                              </ul>
                              <div class="panel-footer">
                                <form id="checkout-selection" action="pay.php" method="POST">
                                  <input type="hidden" name="item_name" value="3 Year Subscription">
                                  <input type="hidden" name="item_description" value="My Test Product Description">
                                  <input type="hidden" name="item_number" value="<?php echo $selreg['reg_id'];?>">
                                  <input type="hidden" name="amount" value="22000">
                                  <input type="hidden" name="address" value="<?php echo $selreg['address'];?>">
                                  <input type="hidden" name="currency" value="INR">
                                  <input type="hidden" name="cust_name" value="<?php echo ucfirst($selreg['name']);?> <?php echo ucfirst($selreg['last_name']);?>">
                                  <input type="hidden" name="email" value="<?php echo $selreg['email'];?>">
                                  <input type="hidden" name="contact" value="<?php echo $row['mobile'];?>">
                                  <input type="submit" class="btn btn-lg btn-block btn-success" value="Buy Now!">
                               
                              </div>
                            </div>
                          </div>
                        </form>
                          <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <!-- PRICE ITEM -->
                            <div class="panel price panel-grey">
                              <div class="panel-heading arrow_box text-center">
                                <h3>5 Year Subscription</h3>
                              </div>
                              <div class="panel-body text-center">
                                <p class="lead" style="font-size:22px">
                                  <strong>40,000 /- 5 Year</strong>
                                </p>
                              </div>
                              <ul class="list-group list-group-flush text-center">
                                <li class="list-group-item">
                                  <i class="icon-ok text-success"></i> Personal use
                                </li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-success"></i> Unlimited Cases
                                </li>
                                <li class="list-group-item">
                                  <i class="icon-ok text-success"></i> 24/7 support
                                </li>
                              </ul>
                              <div class="panel-footer">
                                <form id="checkout-selection" action="pay.php" method="POST">
                                  <input type="hidden" name="item_name" value="5 Year Subscription">
                                  <input type="hidden" name="item_description" value="My Test Product Description">
                                  <input type="hidden" name="item_number" value="<?php echo $selreg['reg_id'];?>">
                                  <input type="hidden" name="amount" value="40000">
                                  <input type="hidden" name="address" value="<?php echo $selreg['address'];?>">
                                  <input type="hidden" name="currency" value="INR">
                                  <input type="hidden" name="cust_name" value="<?php echo ucfirst($selreg['name']);?> <?php echo ucfirst($selreg['last_name']);?>">
                                  <input type="hidden" name="email" value="<?php echo $selreg['email'];?>">
                                  <input type="hidden" name="contact" value="<?php echo $row['mobile'];?>">
                                  <input type="submit" class="btn btn-lg btn-block btn-primary" value="Buy Now!">
                               
                              </div>
                            </div>
                          </form>
                            <!-- /PRICE ITEM -->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php'; ?>
  <!--  <script src="scripts/buttons/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="scripts/buttons/pdfmake.min.js" type="text/javascript"></script>
<script src="scripts/buttons/jszip.min.js" type="text/javascript"></script>
<script src="scripts/buttons/buttons.print.min.js" type="text/javascript"></script>
<script src="scripts/buttons/buttons.html5.min.js" type="text/javascript"></script> -->
  <script type='text/javascript' src='scripts/jspdf.min.js'>
  </script>
  <script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js">
  </script>
  <script type='text/javascript' src="scripts/jquery.table2excel.min.js">
  </script>
  <script type="text/javascript" src="scripts/all_export.js">
  </script>
  <script type="text/javascript" src="scripts/manage-team.js">
  </script>
