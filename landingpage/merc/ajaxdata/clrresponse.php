    <?php  include('../includes/dbconnect.php');  session_start();?>
    <style type="text/css">
    .seekLegalheader{
      background-color: #2d4866;
      color: white;
    }
    .closeseeklegal{
      color: white;
    }
  </style>
  <script type="text/javascript">
    getState();
    // getDistrict();
    function getState(){
      $.ajax({
        type: "POST",
        url: "fetch_state.php",
        cache: false,
        success: function(result){
      // $("#processtheMatter").modal('show');
      var result = JSON.parse(result);
      var option = '<option value="0">Select State</option>';
      $.each(result,function(i,obj){
        option +='<option value='+obj.state_id+'>'+obj.state_name+'</option>';
      });
      $("#clr_state").html(option);
      $("#clr_state").select2();
      console.log(result);
    }
  });
  }

    $("#clr_state").on('change',function(){
      var val = $(this).val();
     $.ajax({
      type: "POST",
      url: "fetch_district.php",
      data : "state="+val,
      cache: false,
      success: function(result){
      // $("#processtheMatter").modal('show');
      var result = JSON.parse(result);
      var option = '<option value="0">Select District</option>';
      $.each(result,function(i,obj){
        option +='<option value='+obj.district_id+'>'+obj.district_name+'</option>';
      });
      $("#clr_district").html(option);
      $("#clr_district").select2();
    }
  });
   });

 </script>
 <div class="modal-content">

  <!-- Modal Header -->
  <div class="modal-header seekLegalheader">
    <h4 class="modal-title">Seek Legal Help</h4>
    <button type="button" class="close closeseeklegal" data-dismiss="modal">&times;</button>
  </div>

  <!-- Modal body -->
  <div class="modal-body">

    <form id="formSubmit" class="smart-form client-forms" method="POST" enctype="multipart/form-data" autocomplete="off" data-select2-id="formSubmit">
     <?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
     $row = mysqli_fetch_array($fetch);
     $reg_cases =mysqli_query($connection,"SELECT * FROM `reg_cases` WHERE `case_id`='".$_POST['id']."'");
     $regs = mysqli_fetch_array($reg_cases);
     $regs['name_of_matter'];
     $fullname = $row['name']." ".$row['last_name']; ?>
     <input type="hidden" id="fname" value="MHMLS-<?php echo $_POST['id'];?>">
     <input type="hidden" id="lname" value="<?php echo $fullname;?>">
     <input type="hidden" id="email" value="MHMLS-<?php echo $_POST['id'];?>">
     <input type="hidden" id="mobile" value="<?php echo $row['mobile'];?>">
     <div class="row clrresponse_row" data-select2-id="9">
                    <!-- <div class="col-sm-6">
                       
                      <div class="form-group" data-select2-id="31">
                       
                        <label for="">Subject</label>
                        <div class="input-group mb-3" data-select2-id="30">

                          <select id="subjectlist" class="form-control sideclass" name="Cases[high_court_id]">
                            <option value="">Please select / कृपया निवडा</option>
                          </select>       
                        </div>
                      </div>
                    </div> -->

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="">Name of Department</label>
                        <div class="input-group mb-3 " data-select2-id="6">

                         <!--  <select id="actlist" class="form-control sideclass" name="Cases[high_court_id]">
                            <option value="">Please select / कृपया निवडा</option>
                          </select> -->
                          <input type="text" class="form-control same" id="actlist" name="state" value="<?php echo $regs['name_of_matter'];?>">
                        </div>
                      </div>
                    </div>

                    <!-- <div class="col-sm-6">
                      <div class="form-group">
                        <label for="">Section</label>   <input type="text" class="form-control same" id="section" name="section" placeholder="Enter Section">
                      </div>
                    </div> -->

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="">State<span class="text-danger"> *</span></label>
                        <select class="form-control" id="clr_state"></select>
                        <!-- <input type="text" class="form-control same" id="state" name="state" placeholder="Enter State"> -->
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="">District<span class="text-danger"> *</span></label> 
                        <select class="form-control" id="clr_district">
                          <option>Select District</option>
                        </select>
                        <!-- <input type="text" class="form-control same" id="district" name="district" placeholder="Enter District"></div> -->
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="">Question<span class="text-danger"> *</span></label>
                        <textarea class="form-control rounded-0" id="" rows="10"></textarea>

                      </div> 
                    </div>
                  </div>
                  <div class="form-group col-sm-12" style="text-align: center;">
                    
                    <!-- <button class="btn btn-sm btn-danger" title="Add" id="submit" onclick="getclr()">Cancle </button> -->
                  </div>
                </form>




              </div>

              <!-- Modal footer -->
              <div class="modal-footer">
                <button class="btn btn-sm close_btn newsubmit" title="Add" id="submit" onclick="getclr()">Submit </button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
              </div>

            </div>
            <script type="text/javascript" src="js/cases-report.js"></script>

