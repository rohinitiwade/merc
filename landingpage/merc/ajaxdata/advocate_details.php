<?php include("../includes/dbconnect.php"); ?>
  <div class="modal-content">
	<div class="modal-header">
		<?php $sql = mysqli_query($connection, "SELECT * FROM `advocate` WHERE `id`='".$_POST['id']."'");
		      $row = mysqli_fetch_array($sql);?>
	<h5 class="modal-title">Advocate Details for <?php echo ucfirst($row['full_name']);?></h5>
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	</div>
	<div class="modal-body">
   <div class="form-group col-md-12">
     <div id="tab" class="table-responsive">
<table class="table" style="background-color: #ea5916;" width="100%">
       <tbody><tr>
        <td rowspan="2"><img src="images/new_logo_small.png" width="70"></td>
        <td>
        <h5 style="color: white;"><b><center>Maharashtra Litigation Management System</center></b></h5>
       </td></tr>
     </tbody></table>
    <table style=" width: 100%; border-collapse: collapse" class="table" border="1">
      <tbody>
        <tr style="background-color: #a4a4a4; color: white;"><td colspan="2" style="text-align: center;"><strong>Full Name:  <?php echo ucfirst($row['full_name']);?> </strong></td> 
        </tr>
        <tr>
            <td><div style=" color: #0000FF">Mobile Number:</div></td>
            <td><?php echo $row['mobile'];?></td>
        </tr>
        <tr>
            <td><div style=" color: #0000FF">Email Id: </div></td>
            <td><?php echo $row['email'];?></td>
        </tr>
        <tr>
         <td><div style=" color: #0000FF">Company Name:</div></td>
            <td><?php echo $row['company_name'];?></td>
        </tr>
        <tr>
            <td><div style=" color: #0000FF">Website</div></td>
            <td><?php echo $row['website'];?></td>
          </tr>
          <tr>
            <td><div style=" color: #0000FF">Office Address  </div></td>
            <td><?php echo $row['office_line_1'];?> , <?php echo $row['office_line_2'];?>   
            	<?php $city = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `id`='".$row['office_city']."'"); 
            	$selcity = mysqli_fetch_array($city);  echo ",";
            	 echo $selcity['name'];?>
            	<?php $satat = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `id`='".$row['office_state']."'"); 
            	$selsatte = mysqli_fetch_array($satat);  echo ",";
            	 echo $selsatte['name'];?>, <?php echo $row['country_office'];?></td>
        </tr>
    </tbody>
</table>
<table style=" width: 100%; border-collapse: collapse" class="table" border="1">
	<tbody>
		<tr style="background-color: #00ff9f;">
        <td colspan="2">
        <h6><b>Point of contacts</b></h6>
       </td>
   </tr>
 </tbody>
</table>
<table class="table table-striped">
  <tr>
    <td>Sr.No.</td>
    <td>full Name</td>
    <td>Email Id</td>
    <td>Contact No.</td>
    <td>Designation</td>
    <td>Date Time</td>
  </tr>

   <?php  $contact_point_adv = mysqli_query($connection, "SELECT * FROM `contact_point_adv` WHERE `adv_id`='".$_POST['id']."'"); 
   $i=1;
        while($elcontact = mysqli_fetch_array($contact_point_adv)){?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $elcontact['full_name'];; ?></td>
            <td><?php echo $elcontact['email_id'];; ?></td>
            <td><?php echo $elcontact['contact_no'];; ?></td>
            <td><?php echo $elcontact['designation'];; ?></td>
            <td><?php echo $elcontact['date_time'];; ?></td>
        </tr>
        <?php $i++; } ?>
    </tbody>
</table>
</div>

                        </div>
	</div>
	<div class="modal-footer">
	<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
</div>