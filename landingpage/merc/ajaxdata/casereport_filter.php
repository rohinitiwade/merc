<?php session_start(); include("../phpfile/sql_home.php"); require_once('../includes/dbconnect.php'); ?>
  <form name="MyName" id="filter_form" method="POST" action="manage_case1.php">
                                                         <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="card-title">Filter</h5>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          </div>
                                                          <div class="modal-body"> <div class="row">
      <div class="col-sm-3">
         <label>Title</label>
         <input type="text" class="form-control form-group" placeholder="Enter Title" name="case_title">
      </div>
      <div class="col-sm-3">
         <label>Case No.</label>
         <input type="text" class="form-control form-group" placeholder="Enter Case" name="case_no">
      </div>
      <div class="col-sm-3">
         <label>Case Year</label>
         <select id="case_no_year" class="form-control form-group"></select>
      </div>
      <!-- <div class="col-sm-3">
         <label>Opponents</label>
         <select class="form-control form-group">
            <option>Select Opponent</option>
         </select>
      </div> -->
      <div class="col-sm-3">                
         <label>CNR #</label>
         <input type="text" class="form-control form-group" placeholder="Enter CNR" name="CNR">
      </div>
     <!--  <div class="col-sm-3">
         <label>Advocate</label>
         <select id='advocate' class="form-control form-group" name="full_name">
            <option value=''>- Select Advocate -</option>
         </select>
      </div> -->
      <div class="col-sm-3"> 
         <label>Hearing Date From </label>
         <input type="text" class="form-control form-group" id="datepicker" name="hearing_date" placeholder="Hearing Date ">
      </div>
       <div class="col-sm-3">
         <label>Hearing Date To</label>
         <input type="text" class="form-control form-group" id="datepicker1" placeholder="Hearing Date To" name="hearing_date_to">
      </div>
     <!--  <div class="col-sm-3"> 
         <label>Case Filing Date From</label>
         <input type="text" class="form-control form-group" id="datepicker2" name="filling_date_from" placeholder="Case Filing Date Form">
      </div> -->
     <!--  <div class="col-sm-3">
         <label>Priority</label>
         <select class="form-control form-group" name="priority" id="priority">
            <option value="">--Select Priority--</option>
         </select>
      </div>
      <div class="col-sm-3">
         <label>Session
         </label>
         <select class="form-group form-control">
            <option>All</option>
            <option>Morning</option>
            <option>Evening</option>
         </select>
      </div> -->
      <div class="col-sm-3">
         <label class="filter_team">Team Member
         </label>
         <select name="court_list" class="form-control" id="selUser">
            <option value="">-- Select Team Member Name --</option>
            <option value="Other">Other
            </option>
         </select>
      </div>
     
     <!--  <div class="col-sm-3">                
         <label>Case Filing Date To</label>
         <input type="text" class="form-control form-group" id="datepicker4" placeholder="Case Filing Date To" name="case_filling_date_to">
      </div> -->
      <!-- <div class="col-sm-3">  
         <label>File #</label>
         <input type="text" class="form-control form-group" placeholder="Enter File" name="file">
      </div> -->
      <div class="col-sm-3">
         <label>Court</label>
         <select name="court_name" class="form-control" id="courtNamecourt" onchange="courtChange(this.options[this.selectedIndex].value)">
            <option value="">-- Select Court Name --</option>
            <?php $courtlist = mysqli_query($connection, "SELECT `court_name`,`court_id` FROM `court_list` ORDER BY court_id ASC");
               foreach($courtlist as $values){?>
            <option value="<?php echo $values['court_id'];?>"><?php echo $values['court_name'];?></option>
            <?php } ?>
         </select>
      </div>
      <div class="col-sm-3 2 court-wise-div" id="highcourtselect" style="display: none;">
         <label>High Court</label>
         <select class="form-control" id="" name="high_court">
            <option value=""> --Select High Court Name --</option>
            <?php $court_list = mysqli_query($connection, "SELECT * FROM `high_court_list` WHERE `court_id`=3");
               while($courtsel =mysqli_fetch_array($court_list)){?>
            <option value="<?php echo $courtsel['high_court_name'];?>"><?php echo $courtsel['high_court_name'];?></option>
            <?php } ?>
         </select>
      </div>
      <div class="col-sm-3 2 court-wise-div" style="display: none;">
         <label>Bench</label>
         <select class="form-control" id="bench_name" name="bench">
            <option value=""> --Select Bench --</option>
            <?php $bench = mysqli_query($connection, "SELECT * FROM `bench`");
               while($bench1 =mysqli_fetch_array($bench)){?>
            <option value="<?php echo $bench1['bench_name'];?>"><?php echo $bench1['bench_name'];?></option>
            <?php } ?>
         </select>
      </div>
      <div class="col-sm-3 2 court-wise-div" style="display: none;">
         <label>Side</label>
         <select class="form-control" id="side" name="side">
            <option value=""> --Select Side --</option>
         </select>
      </div>
      <div class="col-sm-3 2 court-wise-div" style="display: none;">
         <label>Stamp/Register</label>
         <select class="form-control" id="stamp" name="high_court">
            <option value=""> --Select Stamp/Register --</option>
            <option value="stamp">Stamp</option>
            <option value="register">Register</option>
         </select>
      </div>
      <div class="col-sm-3 2 court-wise-div" style="display: none;">
         <label>Case Type
         </label>
         <select class="form-control" id="casetype_list">
            <option value="All">All</option>
         </select>
         <label>
         </label>
         <input type="text" class="form-control form-group" id="otherinput" placeholder="" style="display: none;">
      </div>
      <div class="col-sm-3 3 court-wise-div" id="districtcourtselect" style="display: none;">
         <label>State
         </label>
         <select class="form-control" id="state" name="high_court">
            <option value=""> --Select State Name --</option>
            <?php $geo_locations = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `id`=21");
               while($geo_locations1 =mysqli_fetch_array($geo_locations)){?>
            <option value="<?php echo $geo_locations1['name'];?>">
               <?php echo $geo_locations1['name'];?>
            </option>
            <?php } ?>
         </select>
      </div>
      <div class="col-sm-3 3 court-wise-div" style="display: none;">
         <label>District
         </label>
         <select class="form-control" id="state" name="high_court">
            <option value=""> --Select District --</option>
         </select>
      </div>
      <div class="col-sm-3 3 court-wise-div" style="display: none;"> 
         <label>Court Establishment</label>
         <select class="form-control" id="state" name="high_court">
         </select>
      </div>
      <div class="col-sm-3 3 court-wise-div" style="display: none;">
         <label>Case Type </label>
         <select class="form-control" id="casetype_list">
            <option value="All">All</option>
         </select>
         <label></label>
         <input type="text" class="form-control form-group" id="otherinput" placeholder="" style="display: none;">
      </div>
      <div class="col-sm-3 4" id="commisionselect" style="display: none;">
         <label>Commissions (Consumer Forum):
         </label>
         <select class="form-control" name="forum">
            <option value="All">All</option>
            <option value="case_no">District Forum</option>
            <option value="diary_no">National Commission NCDRC </option>
            <option value="diary_no">State Commission</option>
         </select>
      </div>
      <div class="col-sm-3 5" id="tribunalselect" style="display: none;">
         <label>Tribunals and Authorities:
         </label>
         <select class="form-control" name="authorities">
            <option value="All">All
            </option>
            <option value="case_no">Tribunals
            </option>
         </select>
      </div>
      <div class="col-sm-3 6" id="revenueselect" style="display: none;">
         <label>Revenue Court:
         </label>
         <select class="form-control" name="revenue_court">
            <option value="All">All
            </option>
            <option value="case_no">Revenue
            </option>
         </select>
      </div>
      <div class="col-sm-3 7" id="commissionerateselect" style="display: none;">
         <label>Commissionerate:
         </label>
         <select class="form-control" name="commissionerate">
            <option value="All">All
            </option>
            <option value="case_no">Commissionerate
            </option>
         </select>
      </div>
      <!-- <div class="col-sm-3" id="otherselect" style="display: none;">
         <label>Case Type:</label>
         <select class="form-control" name="case_type">
         <option value="All">All</option>
         <option value="case_no">Case Number</option>
         <option value="diary_no">Diary Number</option>
         </select>
         </div> -->
      <div class="col-sm-3 1 court-wise-div" style="display: none;">
         <label>Dairy/Case No.
         </label>
         <select class="form-control" onchange="getdiary_Case()">
            <option value="case_no">Case Number
            </option>
            <option value="diary_no">Diary Number
            </option>
         </select>
      </div>
      <div class="col-sm-3 court-wise-div" id="diary" style="display: none;">
         <label>Diary No
         </label>
         <input type="text" class="form-control" id="diary-no-val" name="">
      </div>
      <div class="col-sm-3 1 court-wise-div" id="case-no-div" style="display: none;">
         <label>Case Number
         </label>
         <input type="text" class="form-control" id="diary-no-val" name="">
      </div>
      <div class="col-sm-3 1 court-wise-div" style="display: none;">
         <label>Case Type
         </label>
         <select class="form-control" id="casetype_list" name="high_case_type">
            <option value="All">All
            </option>
         </select>
         <label>
         </label>
         <input type="text" class="form-control form-group" id="otherinput" placeholder="" style="display: none;">
      </div>
   </div>


</div>
                                                          <div class="modal-footer">
                                                           <input type="submit" name="fillter" class="btn-info btn-sm btn" style="" value="Submit">
                                                           <button class="btn-danger btn btn-sm" type="button" onclick="resetFilter()">Reset
                                                           </button>
                                                           <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Close</button>
                                                         </div>
                                                       </div>
                                                     </form>