<?php include("../includes/dbconnect.php");
$result = mysqli_query($connection, "SELECT * FROM high_court_list ORDER BY high_court_name ASC");

$json = array();
$total_records = mysqli_num_rows($result);

if($total_records > 0){
  while ($row = mysqli_fetch_array($result)){
    $json[] = $row;
  }
}

echo json_encode($json);

?>