<?php  error_reporting(); session_start(); include_once("../includes/dbconnect.php");?>
<!-- <div class="col-sm-12">

	<b>This is sub heading of audit report</b>
		<p>The Zilla Parishad in this case is a formal party – as the deceased, whose heirs are demanding compassionate appointment was a Class II employee of Government of Maharashtra (As per Sec 97 of Maharashtra Zilla Parishad and Panchayat Samiti Act, 1961) , working as a BDO and not of Zilla Parishad.</p>
</div> -->
<style>
	li{
 padding-bottom: 5px;
	}
</style>
<?php $reg_cases = mysqli_query($connection,"SELECT * FROM `reg_cases` WHERE `case_id`='".$_POST['caseid']."'");
$fetch_cases = mysqli_fetch_array($reg_cases);?>
 <?php if($fetch_cases['case_no'] == '5'){ ?>
	<div class="col-sm-12">
		<h4>CASE NUMBER: <b>Appeal P.G.A No:5/2019</b> </h4>
		
	</div>
<div class="col-sm-12">
	<p><b>General Opinion for all Payment of Gratuity Cases:</b></p>
	<p><b>Applicability to Payment of Gratuity Act, 1972 to Zilla Parishad employees</b></p>
	<ul>
		<li>1. S. 1(3) of 1972 Act stipulates about applicability of the Act to factory, mines, etc. shop or establishment as defined under Bombay Shops and Establishment Act, where 10 or more persons employed in calender year.
			<br>
				S. 2(f) of 1972 Act defines ‘employer’ which includes establishment, factories, etc. The said definition also includes local authority. 
				<br>
				In respect of classes of employment stated above, 1972 Act shall have overriding effect.
		</li>

			<li>2. Zilla Parishad is established under provisions of under provisions of Maharashtra Zilla Parishads and Panchayat Samitis Act, 1961. It is a statutory body. S. 248 of 1961 Act provides for recruitment and conditions of service of employees of Zilla Parishad S. 248(b) specially gives power to State Govt. to make rules for regulating pension, gratuity and other benefits. S. 274 of 1961 Act also gives comprehensive powers to State Govt. to make rules, regulations and bye laws.</li>
			<li>3. U/S. 274 of 1961 Act, Maharashtra Zilla Parishad District Service Rules, 1968 are promulgated.
				<br>
			R. 6 of 1968 Rules stipulates that Maharashtra Civil Services (Pension) Rules, 1982 shall mutatis mutandis apply to employees of Zilla Parishad. 
			<br>
			R. 7-A of 1968 Rules states that Maharashtra Civil Services (Pension) Rules, 1982 shall govern employees of Zilla Parishad. 
			<br>
			R. 8 of Maharashtra Zilla Parishad District Service Rules, 1968 clearly states that employees of Zilla Parishad shall be governed by Maharashtra Civil Services (Pension) Rules, 1982.
			<br>
			Maharashtra Civil Services (Pension) Rules, 1982 are framed under Art. 309 of Constitution of India. Chapter X of 1982 Rules deal with grant of pension and gratuity. Rr. 126, 127, 128, 129-A, 129-B of 1982 Rules categorically deal with aspect of gratuity. 
			<br>
			Similarly Chapters XI and XII describe procedure for determination and sanction of gratuity. Thus, employees of Zilla Parishad are governed by provisions of Maharashtra Civil Services (Pension) Rules, 1982.
		</li>
			</ul>
		<p><b>As such provisions of Payment of Gratuity Act, 1972, not applicable.</b> </p>
		<p>In<b> Chief Executive Officer v. Vasant Maruti Chavhan [AIR online 2015 Bom 1]</b>, the Hon’ble Bombay High Court held that, the order passed by the Controlling Authority under PGA against Zilla Parishad, Sangli holding the employees entitled to the benefit under PGA is erroneous by referring to the law laid down in cases of Municipal Committee and Assistant Executive Engineer. Hon’ble High Court quashed and set aside the order granting pension to the employees of ZP, Sangli. The amount of gratuity was directed to be calculated in accordance with Rule 129 A of the Maharashtra Civil Services (Pension) Rules 1982. 
The aforesaid judgment has thus clearly observed that the employees of ZP are governed by MCS (Pension) Rules and not the provisions of PGA. </p>

<p>Similarly in the case of Dattatraya Ramchandra Phadnis v. State of Maharashtra [AIR online 2002 Bom 1] an employee of the State Government absorbed into Zilla Parishad was held entitled to gratuity and interest thereon under MCS(Pension) Rules. </p>
<p>In Asst. Executive Engineer v. Deputy Labour Commissioner [2013 LAB. I.C. 110], the Karnataka High Court laid down that when specific rules providing for payment of gratuity to employees are available; the Controlling Authority under PGA is not vested with power to order payment of gratuity. Similar view can be found in Municipal Committee v. The Appellate Authority [AIR online 1994 P&H 1]</p>


	<p>Thus by interpreting the provisions of the Act properly, ZP can take a definite stand to the aforesaid effect as a legal defence in respect of the cases under the Payment of Gratuity Act. 
1. Payment of Gratuity Act, 1972 
2. Maharashtra Zilla Parishads and Panchayat Samitis Act, 1961
3. Maharashtra Zilla Parishad District Service Rules, 1968
i. Chief Executive Officer v. Vasant Maruti Chavhan [AIR online 2015 Bom 1]
ii. Dattatraya Ramchandra Phadnis v. State of Maharashtra [AIR online 2002 Bom 1]
iii. Asst. Executive Engineer v. Deputy Labour Commissioner [2013 LAB. I.C. 110]
Municipal Committee v. The Appellate Authority [AIR online 1994 P&H 1]</p>
</div>
<?php }elseif($fetch_cases['case_no'] == '24'){ ?>
	<div class="col-sm-12">
		<h4>CASE NUMBER: <b>R. C. S. No. 24/2017 </b></h4>
		
	</div>
<div class="col-sm-12">
	
	<ul>
		<li>It is clear from the plaint map that the southern boundaries of the plaintiff's Plot Nos. 2495 & 2496 are protruded in the plot of defendants. The southern boundary of plaintiff's plots appears to be pushed southwards as compared to the southern boundary of the adjoining plot owned by Shri. Jivan K. Sahu. This fact supports the allegation of encroachment by plaintiffs as pleaded by defendants. If by proper evidence including measurement by TILR of all the adjoining plots, and procuring appropriate maps is lead to prove he may not get any relief. As per ratio laid down in Premji Ratansey Shah -V/s- Union of India and Others [1995 AIR SCW 2425] Injunction cannot be issued in favour of a trespasser or a person in unlawful possession, as against the true owner.
		</li>

			<li>The defendant has to convince the Court by concrete evidence that the plaintiff has alternate approach way other than that passing through plot of defendant. The existence of 10 ft. wide road on the western side of the premises of the plaintiff which connects to the bachelor road which is the second main road of the city and based on that existence, the north facing of the constructions of the plaintiffs needs to be proved with clear evidence, so that plaintiffs claim of easement of necessity is rejected. -" An easement of necessity being an easement without which a property cannot be used at all and not being one merely necessary to reasonable enjoyment of property, a plaintiff cannot claim on the ground of necessity a right of way over the land of another where another mode of access to his property exists." - Narayana Gajapathiraju -V/s- Janaki Rathayyammaji [AIR 1930 Mad 609]</li>
			<li>Malafide intentions of plaintiff to grab Govrnment land by deliberately keeping gates of his premises opening into vacant plot of Government and then by claiming easementary right over the same needs to be brought to the notice of the Court by emphasizing on the documents of partition and conveyance relating to the plot No. 2495 and 2496 and right of ingress and egress described in those documents.
		</li>
		<li>1) Premji Ratansey Shah -V/s- Union of India &Otrs [1995 AIR SCW 2425]  https://www.aironline.in/WW/fullContent.html
2)Narayana Gajapathiraju -V/s- Janaki Rathayyammaji [AIR 1930 Mad 609]</li>
			</ul>
</div>
<?php } elseif($fetch_cases['case_no'] == '131'){ ?>
	<div class="col-sm-12">
		<h4><b>CASE NO.  ULP 131 of 2018</b> </h4>
		
	</div>
<div class="col-sm-12">
	<p>Zilla Parishad is established under provisions of Maharashtra Zilla Parishads and Panchayat Samitis Act, (5 of 1962). It is a statutory body. S. 248 of 1961 Act provides for recruitment and conditions of service of employees of Zilla Parishad S. 248(a) specially gives power to State Govt. to make rules for recruitment. S. 274 of 1961 Act also gives comprehensive powers to State Govt. to make rules, regulations and bye laws.
U/S. 274 of 1961 Act, Maharashtra Zilla Parishad District Service (Recruitment) Rules, 1967 are promulgated. Rule 1(2) shall apply to the recruitment to all posts of Class III and IV employees. According to these rules, the following procedure should be followed for recruitment (as per Rule 3, 4, 5, 7)</p>
	<ul>
		<li> Necessary vacant post
		</li>

			<li> Eligibility criteria given under Rule 3 – Age, Qualification and Experience</li>
			<li> Advertisement of post
		</li>
		<li> Examination of applicant</li>
		<li>Interview of the candidate</li>
		<li> Medical examination of the candidate</li>
		<li>Appointment </li>
			</ul>
			<p>In this particular case, no such procedure for statutory recruitment was followed and recruitment was done by a third party contractor. Accordingly thE complainant cannot be said to be appointed by following rules and regulations. The Apex Court in <b>State of Karnataka vs. Uma Devi (AIR 2006 SC 1806)</b> observed,
“Unless the appointment is in terms of the relevant rules and after a proper competition among qualified persons, the same would not confer any right on the appointee. If it is a contractual appointment, the appointment comes to an end at the end of the contract, if it were an engagement or appointment on daily wages or casual basis, the same would come to an end when it is discontinued. Similarly, a temporary employee could not claim to be made permanent on the expiry of his term of appointment. It has also to be clarified that merely because a temporary employee or a casual wage worker is continued for a time beyond the term of his appointment, he would not be entitled to be absorbed in regular service or made permanent, merely on the strength of such continuance, if the original appointment was not made by following a due process of selection as envisaged by the relevant rules. It is not open to the Court to prevent regular recruitment at the instance of temporary employees whose period of employment has come to an end or of ad hoc employees who by the very nature of their appointment, do not acquire any right. High Courts acting under Art. 226 of the Constitution of India, should not ordinarily issue directions for absorption, regularization, or permanent continuance unless the recruitment itself was made regularly and in terms of the constitutional scheme. Merely because, an employee had continued under cover of an order of Court, which is described as 'litigious employment', he would not be entitled to any right to be absorbed or made permanent in the service. In fact, in such cases, the High Court may not be justified in issuing interim directions, since, after all, if ultimately the employee approaching it is found entitled to relief, it may be possible for it to mould the relief in such a manner that ultimately no prejudice will be caused to him, whereas an interim direction to continue his employment would hold up the regular procedure for selection or impose on the State the burden of paying an employee who is really not required. 
The Courts must be careful in ensuring that they do not interfere unduly with the economic arrangement of its affairs by the State or its instrumentalities or lend themselves the instruments to facilitate the bypassing of the constitutional and statutory mandates.”</p>
<p>The complainant in the present case couldn't be regularized as they were not appointed as per rules and regulations. An appointment made without following appropriate procedures under rules/ government circulars and without advertisements or without inviting applications would be in breach of <b>Art. 14 and 16 of the Constitution of India.</b> Such appointment would be a backdoor entry <b>(</b>Ref : <b>Cooperative Bank Ltd , Bhopal vs. Nannuram Yadav and Ors. (2007 AIR SCW 6036)</b></p>
<p>Maharashtra Government GR 16 Jan 2003<br>
Maharashtra Government GR 1st Feb 2006<br>
Maharashtra Government GR dated 25th August</p>
</div>
<?php } elseif($fetch_cases['case_no'] == '3718'){ ?>
	<div class="col-sm-12">
		<h4>CASE NUMBER: <b>Writ Petition No. 3718 of 2018</b> </h4>
		
	</div>
<div class="col-sm-12">
	
	<ul>
		<li>The Zilla Parishad in this case is a formal party – as the deceased, whose heirs are demanding compassionate appointment was a Class II employee of Government of Maharashtra (As per Sec 97 of Maharashtra Zilla Parishad and Panchayat Samiti Act, 1961) , working as a BDO and not of Zilla Parishad.
		</li>

			<li><b>The Present suit is bad for non-joinder of necessary party as The Government of Maharashtra is a necessary party in this case, the petition may be quashed.</b></li>
			<li>The petitioner filed for the application of appointment on compassionate ground after one year of the death of her husband, going beyond the period of limitation, as per the Maharashtra GR dated 23.04.1976, hence her application was rejected
		</li>
		<li>Compassionate employment cannot be granted after a lapse of reasonable period- Umesh Kumar Nagpal v. State of Haryana and Ors.[1994 AIR SCW 2305]</li>
		<li>Therefore no liability of Zilla Parishad arises in this present suit.</li>
		<li>Sec 97, The Maharashtra Zilla Parishad and Panchayat Samiti Act, 1961</li>
		<li>GR dated  23.04.1976 </li>
			</ul>
			<p><b>Umesh Kumar Nagpal v. State of Haryana and Ors. .[1994 AIR SCW 2305]</b></p>
</div>
<?php } elseif($fetch_cases['case_no'] == '20'){ ?>
<div class="col-sm-12">
		<h4>CASE NUMBER: <b>Appeal P.G.A No:5/2019</b> </h4>
		
	</div>
<div class="col-sm-12">
	<p><b>General Opinion for all Payment of Gratuity Cases:</b></p>
	<p><b>Applicability to Payment of Gratuity Act, 1972 to Zilla Parishad employees</b></p>
	<ul>
		<li>1. S. 1(3) of 1972 Act stipulates about applicability of the Act to factory, mines, etc. shop or establishment as defined under Bombay Shops and Establishment Act, where 10 or more persons employed in calender year.
			<br>
				S. 2(f) of 1972 Act defines ‘employer’ which includes establishment, factories, etc. The said definition also includes local authority. 
				<br>
				In respect of classes of employment stated above, 1972 Act shall have overriding effect.
		</li>

			<li>2. Zilla Parishad is established under provisions of under provisions of Maharashtra Zilla Parishads and Panchayat Samitis Act, 1961. It is a statutory body. S. 248 of 1961 Act provides for recruitment and conditions of service of employees of Zilla Parishad S. 248(b) specially gives power to State Govt. to make rules for regulating pension, gratuity and other benefits. S. 274 of 1961 Act also gives comprehensive powers to State Govt. to make rules, regulations and bye laws.</li>
			<li>3. U/S. 274 of 1961 Act, Maharashtra Zilla Parishad District Service Rules, 1968 are promulgated.
				<br>
			R. 6 of 1968 Rules stipulates that Maharashtra Civil Services (Pension) Rules, 1982 shall mutatis mutandis apply to employees of Zilla Parishad. 
			<br>
			R. 7-A of 1968 Rules states that Maharashtra Civil Services (Pension) Rules, 1982 shall govern employees of Zilla Parishad. 
			<br>
			R. 8 of Maharashtra Zilla Parishad District Service Rules, 1968 clearly states that employees of Zilla Parishad shall be governed by Maharashtra Civil Services (Pension) Rules, 1982.
			<br>
			Maharashtra Civil Services (Pension) Rules, 1982 are framed under Art. 309 of Constitution of India. Chapter X of 1982 Rules deal with grant of pension and gratuity. Rr. 126, 127, 128, 129-A, 129-B of 1982 Rules categorically deal with aspect of gratuity. 
			<br>
			Similarly Chapters XI and XII describe procedure for determination and sanction of gratuity. Thus, employees of Zilla Parishad are governed by provisions of Maharashtra Civil Services (Pension) Rules, 1982.
		</li>
			</ul>
		<p><b>As such provisions of Payment of Gratuity Act, 1972, not applicable.</b> </p>
		<p>In<b> Chief Executive Officer v. Vasant Maruti Chavhan [AIR online 2015 Bom 1]</b>, the Hon’ble Bombay High Court held that, the order passed by the Controlling Authority under PGA against Zilla Parishad, Sangli holding the employees entitled to the benefit under PGA is erroneous by referring to the law laid down in cases of Municipal Committee and Assistant Executive Engineer. Hon’ble High Court quashed and set aside the order granting pension to the employees of ZP, Sangli. The amount of gratuity was directed to be calculated in accordance with Rule 129 A of the Maharashtra Civil Services (Pension) Rules 1982. 
The aforesaid judgment has thus clearly observed that the employees of ZP are governed by MCS (Pension) Rules and not the provisions of PGA. </p>

<p>Similarly in the case of Dattatraya Ramchandra Phadnis v. State of Maharashtra [AIR online 2002 Bom 1] an employee of the State Government absorbed into Zilla Parishad was held entitled to gratuity and interest thereon under MCS(Pension) Rules. </p>
<p>In Asst. Executive Engineer v. Deputy Labour Commissioner [2013 LAB. I.C. 110], the Karnataka High Court laid down that when specific rules providing for payment of gratuity to employees are available; the Controlling Authority under PGA is not vested with power to order payment of gratuity. Similar view can be found in Municipal Committee v. The Appellate Authority [AIR online 1994 P&H 1]</p>


	<p>Thus by interpreting the provisions of the Act properly, ZP can take a definite stand to the aforesaid effect as a legal defence in respect of the cases under the Payment of Gratuity Act. 
1. Payment of Gratuity Act, 1972 
2. Maharashtra Zilla Parishads and Panchayat Samitis Act, 1961
3. Maharashtra Zilla Parishad District Service Rules, 1968
i. Chief Executive Officer v. Vasant Maruti Chavhan [AIR online 2015 Bom 1]
ii. Dattatraya Ramchandra Phadnis v. State of Maharashtra [AIR online 2002 Bom 1]
iii. Asst. Executive Engineer v. Deputy Labour Commissioner [2013 LAB. I.C. 110]
Municipal Committee v. The Appellate Authority [AIR online 1994 P&H 1]</p>
</div>
<?php } elseif($fetch_cases['case_no'] == '8314'){ ?>
	<div class="col-sm-12">
		<h4>CASE NUMBER: <b>Writ Petition No. 8314 of 2018 </b></h4>
		
	</div>
<div class="col-sm-12">
	<p>The question as to who gives approvals for additional posts of teachers needs to be settled. In a similar case pending before HC:  Raghobaji Bachale Shikshan Prasarak Mandal, Wardha v. State of Maharashtra (Order dated 11.01.2018 in Writ Petition No. 6023 of 2017), a School sought directions against the Government to fix correct sanctioned staffing pattern and strength in accordance with the Right of Children to Free and Compulsory Education Act, 2009. In Dr. Ambedkar Shikshan Sanstha and Ors v. State of Maharashtra (Order dated 05.02.2018 in Writ Petition No. 625 of 2018)) , the school filed petition for inaction on the part of the Director of Education to correct the Staff Justification order for the said year and issue direction to Education officer (Primary) to correct the same.</p>

	<p>The High Court specifically directed the Schools to place on record all the necessary compliances along with necessary documents to show that their schools fulfilled the requirements of Right of Children to Free and Compulsory Education Act, 2009. Vide GR dated 13.12.2013 it was notified that new posts can be approved only after the proposal was sent by the Director of Education. Based on this it could be interpreted that the Zilla Parishad could not approve these posts without the proposal sent by the Director of Education.</p>
	<p>Further as per GR dated 20 June 2014 it was notified that the following procedure shall be followed to partially amend the Government decision.</p>
	<ul>
		<li> School-wise vacancies will be determined on the basis of the validity determined by the Commissioner Education, Education Officer (Primary / Secondary), Zilla Parishad
		</li>

			<li>  No new recruitment will be allowed in any vacant post without the first adjustment of the above additional teachers / non-teaching staff. Without the priorities of the Government, such posts cannot be filled if the posts are to be filled beyond the basic posts</li>
			
			</ul>
			<p>In all such cases where neither relief is claimed from Zilla Parishad nor Zilla Parishad is liable to take any action, the Zilla Parishad is a Formal Party. These cases are in bulk and causing huge burden on Public Exchequer, as a lot of time and money is invested on a case where rather Zilla Parishad has no role to play. Such cases could be easily dealt with a good and detailed written statement or para-wise reply. The counsel dealing with such cases should be strictly instructed to reduce unnecessary delays by filing a good defense.</p>
</div>
<?php } elseif($fetch_cases['case_no'] == '8314'){ ?>
	<div class="col-sm-12">
		<h4>CASE NUMBER: <b>Writ Petition No. 8314 of 2018 </b></h4>
		
	</div>
<div class="col-sm-12">
	<p>The question as to who gives approvals for additional posts of teachers needs to be settled. In a similar case pending before HC:  Raghobaji Bachale Shikshan Prasarak Mandal, Wardha v. State of Maharashtra (Order dated 11.01.2018 in Writ Petition No. 6023 of 2017), a School sought directions against the Government to fix correct sanctioned staffing pattern and strength in accordance with the Right of Children to Free and Compulsory Education Act, 2009. In Dr. Ambedkar Shikshan Sanstha and Ors v. State of Maharashtra (Order dated 05.02.2018 in Writ Petition No. 625 of 2018)) , the school filed petition for inaction on the part of the Director of Education to correct the Staff Justification order for the said year and issue direction to Education officer (Primary) to correct the same.</p>

	<p>The High Court specifically directed the Schools to place on record all the necessary compliances along with necessary documents to show that their schools fulfilled the requirements of Right of Children to Free and Compulsory Education Act, 2009. Vide GR dated 13.12.2013 it was notified that new posts can be approved only after the proposal was sent by the Director of Education. Based on this it could be interpreted that the Zilla Parishad could not approve these posts without the proposal sent by the Director of Education.</p>
	<p>Further as per GR dated 20 June 2014 it was notified that the following procedure shall be followed to partially amend the Government decision.</p>
	<ul>
		<li> School-wise vacancies will be determined on the basis of the validity determined by the Commissioner Education, Education Officer (Primary / Secondary), Zilla Parishad
		</li>

			<li>  No new recruitment will be allowed in any vacant post without the first adjustment of the above additional teachers / non-teaching staff. Without the priorities of the Government, such posts cannot be filled if the posts are to be filled beyond the basic posts</li>
			
			</ul>
			<p>In all such cases where neither relief is claimed from Zilla Parishad nor Zilla Parishad is liable to take any action, the Zilla Parishad is a Formal Party. These cases are in bulk and causing huge burden on Public Exchequer, as a lot of time and money is invested on a case where rather Zilla Parishad has no role to play. Such cases could be easily dealt with a good and detailed written statement or para-wise reply. The counsel dealing with such cases should be strictly instructed to reduce unnecessary delays by filing a good defense.</p>
</div>
<?php } ?>



 