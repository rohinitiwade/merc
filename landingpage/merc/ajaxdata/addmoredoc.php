    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<?php include("../includes/dbconnect.php"); 
    $sql = mysqli_query($connection, "SELECT * FROM `case_registration` WHERE `case_id`='".$_POST['id']."'");
    $rows = mysqli_fetch_array($sql);
    $regcases = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".$_POST['id']."'");
    $selregcases = mysqli_fetch_array($regcases);
       ?>

      <div class="modal-header" style="background-color: rgba(82, 117, 155, 0.5);padding: 5px;">
        <h5 class="modal-title"><span><b>ADD CASE MATTER</b></span> (<?php echo $selregcases['case_title'];?>)</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>  
            <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div>
     <main>          
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="clearfix2"></div>
                        </div>
                    
                        <div class="col-sm-12">
                        <div class="well p-0">
                        <form id="" class="smart-form client-form" method="POST" enctype="multipart/form-data" action="add_docment.php">
                            <input type="hidden" name="email_id" value="<?php echo $rows['email_id'];?>">
                            <input type="hidden" value="<?php echo $_POST['id']?>" name="AddcaseId">
                            <fieldset>
                              <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="">Name of Matter</label>
                                    
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" name="subject" id="case_title" placeholder="Enter Name of Matter" value="<?php echo $rows['case_name'];?>" readonly>
                                       <!--  <div class="input-group-append">
                                            <span class="input-group-text" ><i class="fas fa-gavel"></i></span>
                                        </div> -->
                                    </div>
                                </div>
                                  <div class="col-sm-6 form-group" style="float: right;">
                                    <label for="">Department</label>
                                    <div class="input-group mb-3">
                                         <input type="text" class="form-control" name="department" id="case_title"  value="<?php echo $rows['department'];?>" readonly>
                                    </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label for="">Document Type</label>
                                    <div class="input-group mb-3">
                                       <select name="cas_type" class="form-control"  id="document_type">
                                         <option value="">---- Select Case Type ----</option>
                                         <option value="Writ petition">Writ petition</option>
                                         <option value="SLP">SLP</option>
                                         <option value="Other Petitions">Other Petitions</option>
                                         <option value="Application">Application</option>
                                         <option value="Complaint">Complaint</option>
                                         <option value="Civil Suit">Civil Suit</option>
                                         <option value="Appeal">Appeal</option>
                                         <option value="Revision Application">Revision Application</option>
                                       </select>
                                    </div>
                                </div>
                                  <div class="col-sm-6 form-group" style="float: right;">
                                    <label for="">Attached Document</label>
                                    <div class="input-group mb-3 ">
                                     
                                      <table width="100%" id="example">
                                        <thead>
                                        <tr>
                                          <td style="background-color: #8cc877;">Sr.No.</td>
                                          <td style="background-color: #8cc877;">Document Type</td>
                                          <td style="background-color: #8cc877;">Remove</td>
                                        </tr>
                                         </thead>
                                        <tbody>
                                         <?php $docment = mysqli_query($connection, "SELECT * FROM `add_document` WHERE `case_id`='".$_POST['id']."' AND  `remove_status`=1");
                                         $i=1; 
                                      while($seldocument = mysqli_fetch_array($docment)){?>
                                        <tr>
                                          <td><?php echo $i; ?></td>
                                          <td><?php echo $seldocument['type']; ?></td>
                                          <td><button type="button" class="btn btn-danger btn-xs delete" id='<?php echo $seldocument['doc_id']; ?>'>Remove</button></td>
                                        </tr>
                                          <?php $i++; } ?>
                                      </table>
                                    
                                       </div>
                                  </div>
                              </div>
                                <div class="form-group">
                                   <span style="float: right;"><input type="submit" class="btn btn-primary signinR" id="add_button" value="Add" name="addDocment" style="margin-right: 10px" ><button type="button" id="close_button" class="btn btn-danger" data-dismiss="modal">Close</button></span> 
                                </div>
                                <br/>
                            </fieldset>
                        </form>
                        </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <style>
       #add_button:hover {
        background-color: #fda103 !important;
        color: black !important;
      }
      #close_button:hover {
        background-color: #fda103 !important;
        color: black !important;
      }
      #add_button{
        border: 0px;
      }
      #close_button{
        border: 0px;
      }
      </style>
      <script>
        $(document).ready(function(){

 // Delete 
 $('.delete').click(function(){
   var el = this;
   var id = this.id;
   $.ajax({
     url: 'ajaxdata/remove.php',
     type: 'POST',
     data: { id:id },
     success: function(response){
     $( '#showcase' ).html(response);
     $( '#hide' ).hide(response);

    }
   });

 });

});
      </script>
      <script type="text/javascript">
        $(document).ready(function() {
    $('#example').DataTable();
} );
      </script>