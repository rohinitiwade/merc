<?php
include("../includes/dbconnect.php");
session_start();
$avail_get = array();

$avail = mysqli_query($connection, "SELECT `avail_id`,`avail_name` FROM `avail_services_master` ORDER BY avail_id ASC");
foreach ($avail as $getAvail) {
    array_push($avail_get, array(
        'avail_id' => $getAvail['avail_id'],
        'avail_name' => $getAvail['avail_name']
    ));
}
echo json_encode($avail_get);
mysqli_close($connection);
?>