<?php
//Include the database configuration file
include 'includes/dbconnect.php';

if(!empty($_POST["country_id"])){
    //Fetch all state data
       $query = mysqli_query( $connection, "SELECT * FROM `geo_locations` WHERE `parent_id`='".$_POST['country_id']."'");
    //Count total number of rows
    $rowCount = mysqli_num_rows($query);
    
    //State option list
    if($rowCount > 0){
        echo '<option value="">Select City</option>';
        while($row = mysqli_fetch_assoc($query)){ 
            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
    }else{
        echo '<option value="">State not available</option>';
    }
}
?>