<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<style type="text/css">
.input-group{
	margin-bottom: 0px;
}
.select-due-date{
	display: flex;
	align-items: center;
}
.pagination{
	float: right;
}
</style>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title">Non Litigation Matter </h5>
							</div>
							<div class="page-body">
								<div class="card form-group">
									<div class="card-block">
										<form id="formSubmit" class="smart-form client-forms" method="POST" enctype="multipart/form-data" autocomplete="off">
											<div class="col-sm-12 p-0 row">
												<div class="col-sm-6 form-group row">											
													<?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
													$row = mysqli_fetch_array($fetch);?>
													<input type="hidden" id="fname" value="<?php echo $row['name'];?>">
													<input type="hidden" id="lname" value="<?php echo $row['last_name'];?>">
													<input type="hidden" id="email" value="<?php echo $row['email'];?>">
													<input type="hidden" id="mobile" value="<?php echo $row['mobile'];?>">
													<label for="" class="col-sm-4"><b>Department</b></label>
													<div class="col-sm-8" >
														<select name="subject" id="subject" class="form-control" style="font-size: 14px;color: gray; border: 1px solid #babfc7;">
															<option value="">---- Select Case Subject ----</option>
															<?php $nameofmatter = mysqli_query($connection, "SELECT * FROM `department` ORDER BY department_name ASC");
															while($nameofmatterss = mysqli_fetch_array($nameofmatter)){?>
																<option value="<?php echo $nameofmatterss['department_name'];?>"><?php echo $nameofmatterss['department_name'];?></option>
															<?php } ?>
														</select>
													</div>
												</div>

												<div class="col-sm-6 form-group row">
													<label for="" class="col-sm-4"><b>File No.</b></label>
													<div class="col-sm-8">
														<input type="text" class="form-control same"  id="fileno" name="file" placeholder="Enter File No / नस्ती क्रमांक" >
													</div>
												</div>
											</div>
											<div class="col-sm-6 form-group row p-0">
												<!-- <div class="form-group"> -->
													<label for="" class="col-sm-4"><b>Documents</b></label><br>
													<div class="col-sm-8">
														<input type="file" id="non_doc_0" class=" same" aria-label="File browser example">
														<button class="btn btn-sm btn-primary non_lit_button" type="button" onclick="nonLit()"> <i class="feather icon-plus" aria-hidden="true"></i></button> 
													<div id="nonlitShow" class=""></div>
													</div>
												</div>
													<!-- <div class="input-group form-group" id="copy_of_petition">
														<div class="col-sm-3"> <label><b>a)</b>Copy of Petition</label></div>
														<input type="file" id="pet_file_0" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc(0)">
														<button class="btn btn-sm btn-primary" type="button" onclick="writePetition('copy_petition')"> <i class="feather icon-plus" aria-hidden="true"></i></button>
													</div> -->
													
													<div class="col-sm-12">
														<div class="form-group">
															<label for=""><b>Raise Your Non Litigation Matter Here </b><span class="text-danger"> *</span>
																<div class="form-radio">
																	<form>
																		<div class="radio radio-inline">
																			<label>
																				<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('English')" value="English" checked>
																				<i class="helper"></i>English
																			</label>
																		</div>
																		<div class="radio radio-inline">
																			<label>
																				<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('Marathi')" value="Marathi">
																				<i class="helper"></i>Marathi
																			</label>
																		</div>
																	</div>
																</label>
																<textarea name="details" id="query" required=""><?php echo $_POST['details'];?></textarea>
															</div> 
														</div>
													</div>
													<div class="form-group col-sm-12" style="text-align: center;">
														<button class="btn btn-sm btn-primary close_btn  form-group" title="Add" id="submit" onclick="getclr()">Submit </button>
													</div>
												</form>
												<section class="body-section" style="display: none;">
													<div class="bread_crumbs" style="text-align: center;"> 
														<span class="replyheading">Case Legal Research Response</span> 
													</div>

													<div class="container" style="margin-bottom: 4%;">
														<!-- <div class="row"> -->
															<div class="col-sm-12  ">
																<div class="panel panel-primary">
																	<div class="">
																		<div class="panel-body">
																			<ul class="chat">
																				<li class="left "><span class="chat-img pull-left" style="float: left;">

																				</span>
																				<div class="chat-body ">
																					<?php  $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
																					$row = mysqli_fetch_array($fetch);?>
																					<input type="hidden" id="email" value="MHMLS-<?php echo base64_decode($_GET['caseid']);?>">
																					<div class="header">
																						<small class="pull-right text-muted" style="width: 100%; float: left;"> </small>



																						<div id="queryshow"></div>


																					</div>
																				</div>
																			</li>
																		</ul>
																	</div>

																</div>

															</div>
															<div id="pagen"></div>

														</div>


														<!-- </div> -->
													</div>
												</section> 
											</div><!-- Card Block End -->
										</div><!-- Card End -->
									</div><!-- Page body End -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php include 'footer.php'; ?>

			<script src=scripts/non_litigation.js></script>