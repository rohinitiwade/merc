<?php
session_start();
include('includes/dbconnect.php');
$conn               = array();
$select_act         = mysqli_query($connection, "SELECT `color_code`,`priority` FROM `activity_log` WHERE  `case_id`='" . $_POST['case_id'] . "' AND `user_id`='" . $_SESSION['user_id'] . "' AND `remove_status`=1 AND `activity_log_type`!='h' ORDER BY `activity_log_id` DESC LIMIT 1");
$select_acts        = mysqli_fetch_array($select_act);
$select_activitylog = mysqli_query($connection, "SELECT * FROM `activity_log` WHERE  `case_id`='" . $_POST['case_id'] . "' AND `user_id`='" . $_SESSION['user_id'] . "' AND `remove_status`=1  ORDER BY `activity_log_id` DESC");

while ($fetch_activity = mysqli_fetch_array($select_activitylog)) {
    $reg  = mysqli_query($connection, "SELECT CONCAT(name, ' ', last_name) AS user_name,gender,image FROM `law_registration` WHERE `reg_id`='" . $fetch_activity['user_id'] . "'");
    $regs = mysqli_fetch_array($reg);
     $reg_edit  = mysqli_query($connection, "SELECT CONCAT(name, ' ', last_name) AS user_name,gender,image FROM `law_registration` WHERE  `reg_id`='" . $fetch_activity['edit_by'] . "'");
    $editReg = mysqli_fetch_array($reg_edit);
    if ($fetch_activity['date_time'] != '') {
        $activity_date = date('d-m-Y', strtotime($fetch_activity['date_time']));
        $activity_time = date('H:i', strtotime($fetch_activity['date_time']));
    }
    if ($fetch_activity['next_hearing_date'] != '') {
        $hearing_date = date('F d, Y', strtotime($fetch_activity['next_hearing_date']));
    }
    
    $calender       = mysqli_query($connection, "SELECT * FROM `calendar_hearing_who_attended` WHERE `case_id`='" . $_POST['case_id'] . "'");
    $attended_other = '';
    while ($fetch_calender = mysqli_fetch_array($calender)) {
        $reg  = mysqli_query($connection, "SELECT CONCAT(name, ' ', last_name) AS user_name,gender,image FROM `law_registration` WHERE `reg_id`='" . $fetch_calender['who_attended'] . "'");
        $regs = mysqli_fetch_array($reg);
        // $attended_other[] = $fetch_calender;
        
        $data_list_object                      = new stdClass();
        $data_list_object->id                  = $fetch_calender['id'];
        $data_list_object->activity_log_id     = $fetch_calender['activity_log_id'];
        $data_list_object->case_id             = $fetch_calender['case_id'];
        $data_list_object->calendar_hearing_id = $fetch_calender['calendar_hearing_id'];
        $data_list_object->who_attended        = $fetch_calender['who_attended'];
        $data_list_object->who_attended_other  = $fetch_calender['who_attended_other'];
        $data_list_object->other               = $fetch_calender['other'];
        $data_list_object->user_id             = $fetch_calender['user_id'];
        $data_list_object->remove_status       = $fetch_calender['remove_status'];
        $data_list_object->date_time           = $fetch_calender['date_time'];
        $data_list_object->who_attended_name   = $regs['user_name'];
        // $data_list_object->session_phase = $data_list['session_phase'];
        // $data_list_object->hearing_description = $data_list['hearing_description'];
        $attended_other[]                      = $data_list_object;
    }
    
    $user_image = 'uploads_profile/' . $regs['image'];
    array_push($conn, array(
        'case_id' => $fetch_activity['case_id'],
        'user_id' => $fetch_activity['user_id'],
        'CNR' => $fetch_activity['CNR'],
        'court_id' => $fetch_activity['court_id'],
        'edit_by' => $editReg['user_name'],
        'case_no' => $fetch_activity['case_no'],
        'color_code' => $fetch_activity['color_code'],
        'priority' => $fetch_activity['priority'],
        'user_comment' => $fetch_activity['user_comment'],
        'activity_log_type' => $fetch_activity['activity_log_type'],
        'status' => $fetch_activity['status'],
        'date_time' => $fetch_activity['date_time'],
        'last_priority' => $fetch_activity['last_priority'],
        'created_date' => $activity_date,
        'created_time' => $activity_time,
        'document' => $fetch_activity['image'],
        'user_name' => $regs['user_name'],
        'gender' => $regs['gender'],
        'activity_log_id' => $fetch_activity['activity_log_id'],
        'stage' => $fetch_activity['stage'],
        'posted_for' => $fetch_activity['posted_for'],
        'action_taken' => $fetch_activity['action_taken'],
        'next_hearing_date' => $hearing_date,
        'session_phase' => $fetch_activity['session_phase'],
        ' hearing_description' => $fetch_activity[' hearing_description'],
        'hearing_id' => $fetch_activity['hearing_id'],
        'who_attended' => $attended_other,
        'user_image' => $user_image,
        'code' => $select_acts['color_code'],
        'latest_priority' => $select_acts['priority']
    ));
}
echo json_encode($conn);
?>