      <div class="media">
                                          <div class="media-body col-sm-12">
                                             <div class="row col form-group">
                                                <input type="hidden" value="<?php echo $_GET['caseid']; ?>" id="caseid"/>
                                                <textarea rows="5" id="notes" name="notes" class="form-control"></textarea>						
                                             </div>
                                             <div class="row">
                                                <div class="col row">
                                                   <b class="col">Mark this note as private </b>
                                                   <div class="form-radio col">
                                                      <div class="radio radio-inline">
                                                         <label>
                                                         <input type="radio" name="np" value="yes">
                                                         <i class="helper"></i> Yes
                                                         </label>
                                                      </div>
                                                      <div class="radio radio-inline">
                                                         <label>
                                                         <input type="radio" name="np" value="no" checked>
                                                         <i class="helper"></i> No
                                                         </label>
                                                      </div>
                                                   </div>
                                                </div>
                                                 <div class="col row form-group">
                                                   <input type="file" name="notes-file" id="notes-file">
                                                </div>
                                                <div class="col-sm-2">
                                                  
                                                   <button class="btn btn-primary btn-sm" type="button" id="btnNoteSave">Submit</button>
                                                    <button class="btn btn-danger btn-sm" type="button" id="btnNoteClear">Clear</button>
                                                </div>
                                             </div>
                                             <div  id="note_block">
                                              
                                             </div>
                                          </div>
                                       </div>