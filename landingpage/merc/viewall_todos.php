<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="css/vertical-layout-light/viewall_todos.css"> 
      <?php include("menu.php");?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="card form-group">
            <div class="card-body">
              <h4 class="card-header" style="padding-top: 0px">Upcoming To-Do's</h4>
              <div class="card">
                <label><b>November 08, 2019</b></label>
                <ul>
                  <li id="cases">
                    <span>11:00</span>
                    <a href="">WP 5257 / 2018 - Nyayapra 2518/cr. 457/A-14_SAU. CHANCHALA AJAY MALPE</a>
                  </li>
                  <li id="cases">
                    <span>11:00</span>
                    <a href="">WP 5257 / 2018 - Nyayapra 2518/cr. 457/A-14_SAU. CHANCHALA AJAY MALPE</a>
                  </li>
                </ul>
              </div>
              <div class="card">
                <label><b>November 08, 2019</b></label>
                <ul>
                  <li id="cases">
                    <span>11:00</span>
                    <a href="">WP 5257 / 2018 - Nyayapra 2518/cr. 457/A-14_SAU. CHANCHALA AJAY MALPE</a>
                  </li>
                  <li id="cases">
                    <span>11:00</span>
                    <a href="">WP 5257 / 2018 - Nyayapra 2518/cr. 457/A-14_SAU. CHANCHALA AJAY MALPE</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <?php include("footer.php");?>
        </div>