<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); session_start(); ?>
      <?php include("menu.php");?>
    <div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
  <div class="pcoded-content">
    <div class="pcoded-inner-content">
      <div class="main-body">
        <div class="page-wrapper">
          <div class="page-body">
              <h4 class="card-header" style="padding-top: 0px">Upcoming Hearing Dates</h4>
               <?php  $hearingdates = date('Y-m-d');
        $history_of_case_hearing = mysqli_query($connection, "SELECT DISTINCT(hearing_date) FROM `reg_cases` WHERE `hearing_date` >= '".$hearingdates."' AND `case_district`='".$_SESSION['cityName']."' ORDER BY hearing_date ASC");
        while($history=mysqli_fetch_array($history_of_case_hearing))  { 
          ?>
              <div class="card hearing_card">
                <label class="card_date"><b><?php   echo date("F d, Y",strtotime($history['hearing_date']));?></b></label>
                <ul>
                <?php $view_case =mysqli_query($connection,"SELECT rc.case_no, rc.case_no_year, rc.case_title, rc.case_type,rc.court_id,rc.high_court,rc.side,rc.hc_stamp_register  FROM reg_cases as rc  WHERE rc.hearing_date='".$history['hearing_date']."'");
            while($fetch_data = mysqli_fetch_array($view_case)){
             if($fetch_case_type['Initials']!=''){
            $sel_case_type =mysqli_query($connection,"SELECT Initials FROM `Case_confirmation` WHERE `name` like '".trim($fetch_data['case_type'])."%'");
            $fetch_case_type = mysqli_fetch_array($sel_case_type);
            $initialss = $fetch_case_type['Initials'];
          }else{
            $initials='';
          }?>
           
                  <li id="cases">
                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                    <a href="" class="case_link"><?php echo $initialss; echo " "; echo $fetch_data['case_no'];?> / <?php echo $fetch_data['case_no_year'];?> - <?php echo $fetch_data['case_title'];?></a><br>
                   
                    <span id="courts"><?php if($fetch_data['court_id'] == 2){ echo $fetch_data['high_court']; echo "-"; echo $fetch_data['side']; echo "-"; echo $fetch_data['hc_stamp_register']; }elseif($fetch_data['court_id'] == 3){ echo $fetch_data['court_name']; echo "-"; echo $fetch_data['state']; echo "-"; echo $fetch_data['district']; echo "-"; echo $fetch_data['case_type']; }elseif($fetch_data['court_id'] == 1){ echo $fetch_data['court_name']; }?></span>
                  </li><hr>
                   <?php  } ?> 
                </ul>
              </div>
            <?php  } ?>
            </div>
          </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <style type="text/css">
   .case_link{
    color: #0d07de;
    font-size: 16px;
   }
   .card_date{
      background-color: #404e67;
      color: white;
      font-size: 16px;
   }
 </style>
      
         <?php include("footer.php");?>