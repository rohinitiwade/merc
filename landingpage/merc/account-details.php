<?php include("header.php");?>
<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<?php include("menu.php");?>
<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <?php include("slider.php");?>
    <div class="card account-details" style="">
      <div class="card-body">
        <h4 class="card-header" style=""><b style="color: green;">Account Details</b></h4>
        <div class="form-group row account-div" style="">
          <?php $account = mysqli_query($cnnection, "SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                $selaccont = mysqli_fetch_array($account); ?>
          <div class="col-sm-6" style="text-align: left;">
            <span><b>REGISTERED DATE:</b>  <?php echo $selaccont['date_time']; ?>DECEMBER 13, 2018</span><br>
            <span><b>TRIAL PERIOD:</b> DECEMBER 13, 2018 - DECEMBER 13, 2018</span><br>
            <span><b>CURRENT SUBSCRIPTION PERIOD:</b>  TILL DECEMBER 13, 2019</span><br>
            <span><b>NEXT RENEWAL DATE:</b>  DECEMBER 13, 2019</span><br><br>
            <button class="renew-sub" style="">RENEW SUBSCRIPTION</button>
          </div>
          <div class="col-sm-6" style="text-align: right;">
            <br><br>
            <span><b>TOTAL # OF LICENSES PURCHASED:</b> 44</span><br>
            <span><b>TOTAL # OF REGISTERED USERS:</b> 44</span><br><br>
            <button class="purchase-add" style="">PURCHASE ADDITIONAL LICENSES</button>
          </div>
        </div>
      </div>
    </div>
    <div class="card payment-history">
      <div class="card-body">
        <h4 class="card-header" style=""><b style="color: green;">Payment History</b></h4>
        <div class="row payment-div" style="">
          <div class="col-12">
            <div class="table-responsive">
              <table id="order-listing" class="table">
                <thead>
                  <tr>
                    <th style="background-color: #353eb3; color: white;">Sr.No.</th>
                    <th style="background-color: #353eb3; color: white;">Invoice no.</th>
                    <th style="background-color: #353eb3; color: white;">Type</th>
                    <th style="background-color: #353eb3; color: white;">Subcription Period</th>
                    <th style="background-color: #353eb3; color: white;">Licenses Purchased</th>
                    <th style="background-color: #353eb3; color: white;">Amount Paid (INR)</th>
                    <th style="background-color: #353eb3; color: white;">Paid on</th>
                    <th style="background-color: #353eb3; color: white;">Payment Method</th>
                    <th style="background-color: #353eb3; color: white;">Transaction #</th>
                    <th style="background-color: #353eb3; color: white;">Receipt/Invoice</th>
                  </tr>
                </thead>
                <tbody>
                 
                </tbody>
              </table>
              <tr>
                <td colspan="10">
                </td>
              </tr>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <style type="text/css">
    .renew-sub{
      padding: 9px 11px;border-radius: 5px;background-color: #2d4866;color: white;border: 0px;
    }
    .purchase-add{
      padding: 9px 11px;border-radius: 5px;background-color: #2d4866;color: white;border: 0px;
    }
    .account-details{
      margin-bottom: 30px;
    }
    .account-details .card-header{
      padding-top: 0px
    }
    .account-div{
      margin-top: 30px;color: black;
    }
    .payment-history{
      padding-top: 0px
    }
    .payment-div{
      margin-top: 25px;
    }
  </style>
  <?php include("footer.php");?>
