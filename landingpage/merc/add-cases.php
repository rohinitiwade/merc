<?php session_start(); include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<link rel="stylesheet" href="css/vertical-layout-light/add_cases.css">
<style type="text/css">
	.fetchdataloader {
		border: 16px solid #f3f3f3;
		border-radius: 50%;
		border-top: 16px solid #3498db;
		width: 60px;
		float: right;
		height: 60px;
		-webkit-animation: spin 2s linear infinite; /* Safari */
		animation: spin 2s linear infinite;
	}
/*.select2-results__option[aria-selected="true"] {
	display: none;
	}*/
	#sc_info{
		color: red;
		width: 100%;
		float: left;
		/*text-align: right;*/
		/*position: absolute;*/
		/*z-index: 1;*/
		/*top: 54px;*/
		display: none;
	}
	.full-name{
		width: 55%;
	}
	.fetchdata-dialog{
		max-width:80%;
	}
	#submit_data{
		text-align: right;
	}
	.fetch_file{
		color: red;
    font-size: 17px;
	}
	#pet_file{
		width: 72%;
	}
	/* Safari */
	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}
</style>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title">Judicial Add Case </h5>
							</div>

							<div class="page-body">
								<div class="card form-group">
									<!-- <div class="card-header ">
									</div> -->
									<input type="hidden" id="state_code" name="">
									<input type="hidden" id="court_code" name="">
									<input type="hidden" id="dist_code" name="">
									<input type="hidden" id="court_type" name="">
									<input type="hidden" id="stamp_id" name="">
									
									<div class="card-block">
										<form class="forms-sample row" method="POST" autocomplete="off">
											<div class="col-sm-6 row form-group">
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp; Court</label>
												<div class="col-sm-8">
													<select id="cases-court_id" class="form-control" name="court_id">
														<option value=""> Please select</option>
														<?php $courtname = mysqli_query($connection, "SELECT * FROM `court_list` ORDER BY court_id ASC");
														while($selcourtlist = mysqli_fetch_array($courtname)){?>
															<option value="<?php echo $selcourtlist['court_id'];?>"><?php echo $selcourtlist['court_name'];?></option>
														<?php } ?>
														<!-- <option value="9">Other</option> -->
													</select>
												</div>
											</div>
											<div id="supremecourt" class="col-sm-6 common-select-div row form-group" style="display: none;">
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Supreme Court of India</label>
												<div class="col-sm-8"><select id="supreme_court" class="form-control select" name="cases_diary_no[]">
													<option value=""> Please Select </option>
													<option value="Case Number">Case Number</option>
													<option value="Diary Number">Diary Number</option>
												</select>
											</div>

										</div>
										<div id="supreme_court_casetype" class="col-sm-6 common-select-div row form-group" style="display: none;">        
											<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>
											<div class="col-sm-8 form-group" style="float: left;">
												<select id="cases_case_type" class="form-control form-group" name="case_type" data-pullbtnflag="0" style="width: 100%" style="background-color: #353eb3; color: white;" aria-invalid="true">
													<option value=""> Please select</option>
													<?php $casetype = mysqli_query($connection, "SELECT * FROM `case_type` ORDER BY case_type ASC");
													while($selcasetype = mysqli_fetch_array($casetype)){?>
														<option value="<?php echo $selcasetype['master_id'];?>"><?php echo $selcasetype['case_type'];?></option>
													<?php } ?>
													<option value="0">Other</option>
												</select>
											</div>
											<input type="text" class="form-control" id="sc_casetype_other" name="">
										</div>
										<div id="supreme_court_diary" class="col-sm-6 common-select-div row form-group" style="display: none;">
											<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Diary Number
												<br/>
												<span class="lbl-short-desc">(Please enter the diary number assigned by court)</span></label>
												<div class="col-sm-4" style="float: left;">
													<input type="text" name="diary_number" id="diary_number" value="" class="form-control" autocomplete="OFF" >
												</div>
												<div class="col-sm-4" style="float: left;">
													<select name="year" id="diary_year" class="form-control ">
														<option value=''>Select Year</option>
														<?php $year = mysqli_query($connection, "SELECT * FROM `year` ORDER BY date_time DESC");
														while($yearsel = mysqli_fetch_array($year)){?>
															<option value='<?php echo $yearsel['date_time'];?>'><?php echo $yearsel['date_time'];?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-sm-6 row form-group common-select-div" id="cnr_div" style="display: none;">
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
													Do you have CNR #?
												</label>
												<div class="form-radio col">
													<div class="radio radio-primary radio-inline">
														<label>
															<input type="radio" class="chkPassport" name="membershipRadios" onclick="cnr_allow('Yes')" id="membershipRadios" value="Yes">

															<i class="helper"></i>Yes
														</label>
													</div>
													<div class="radio radio-primary radio-inline">
														<label>
															<input type="radio" class="chkPassport"  name="membershipRadios" onclick="cnr_allow('No')" id="membershipRadios" value="No" checked>

															<i class="helper"></i>No
														</label>
													</div>
												</div>
												
											</div>
											<div id="highcourt" class="col-sm-6 common-select-div row form-group" style="display: none;">
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;High court</label>
												<div class="col-sm-8"><select id="high-court-list" class="form-control " name="Cases[high_court_id]">
													<option value=""> Please select</option>
													<?php $highcourt= mysqli_query($connection,"SELECT * FROM `high_court_list`  ORDER BY high_court_name ASC");
													while($seligh = mysqli_fetch_array($highcourt)){?>
														<option value="<?php echo $seligh['court_id']; echo","; echo $seligh['master_id']; ?>"><?php echo $seligh['high_court_name'];?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group col-sm-6 row common-select-div" style="display: none;" id="bench">
											<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Bench</label>
											<div class="col-sm-8"><select  class="form-control highcourtidslist " name="Cases[hc_bench_id]" id="high_court_bench_list">
												<option value="">Please select</option>
											</select>
										</div>
									</div>
									<div class="form-group col-sm-6 row common-select-div" style="display: none;" id="highcourt_side"> 
										<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Side</label>
										<div class="col-sm-8"><select id="high_court_side_list" class="form-control sideclass " name="Cases[high_court_id]">
											<option value=""> Please select</option>
										</select></div>
									</div>
									<div class="form-group col-sm-6 row common-select-div" style="display: none;" id="stamp_register"> 
										<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Stamp/Register</label>
										<div class="col-sm-8"><select id="cases-high_court_id" class="form-control select" name="Cases[high_court_id]">
											<option value=""> Please select</option>
											<option value="1">Register</option>
											<option value="2">Stamp</option>
										</select></div>
									</div>
									<div class="form-group col-sm-6 row common-select-div" style="display: none;" id="highcourt_casetype"> 
										<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>
										<div class="col-sm-8 form-group" style="float: left;">
											<select id="high_court_casetype" class="form-control " name="Cases[high_court_id]">
												<option value="">Please select</option>
											</select>
											<input type="text" class="form-control" id="hc_casetype_other" name="" style="display: none;"></div>
										</div>
										<div id="district_court" class="form-group col-sm-6 row" style="display: none;">      
											<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;State</label>
											<div class="col-sm-8"><select name="state" id="districtcourt_state" class="form-control">
												<option value="">  Please select</option>
												<?php $sates = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `location_type`='STATE' AND `id`=21 ORDER BY `name` ASC");
												while($selsstae = mysqli_fetch_array($sates)){ ?>
													<option value="<?php  echo $selsstae['id']; echo"_"; echo $selsstae['master_id'];?>"><?php  echo $selsstae['name'];?></option>
												<?php } ?>
											</select></div>
										</div>
										<div class="form-group col-sm-6 row common-select-div" id="district-court-district" style="display: none;">
											<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;District</label>
											<div class="col-sm-8"><select id="district_court_city" name="district" class="form-control esta">
												<option value="">Select State first</option>
											</select></div>
										</div>
										<div class="form-group col-sm-6 row common-select-div" id="district-court-establishment"  style="display: none;">
											<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Court Establisment</label>
											<div class="col-sm-8"><select id="district_court_establishment" name="court_establishment" class="form-control">
												<option value="">Select District First</option>
											</select>

											<input type="text" class="form-control" id="dc_establishment_other" name=""></div>

										</div>
										<div class="form-group col-sm-6 row common-select-div" id="district-court-casetype-div" style="display: none;">
											<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>
											<div class="col-sm-8"><select id="district_court_casetype" name="court_establishment" class="form-control">

											</select>
											<input type="text" class="form-control" id="district_court_casetype_other" name="">
										</div>
									</div>
									<div id="commission_forum" class="form-group col-sm-6 common-select-div row" style="display: none;">        
										<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Commissions (Consumer Forum)</label>
										<div class="col-sm-8"><select name="commissions" id="commission" class="form-control">
											<option value=""> Please select</option>
											<option value="District Forum">District Forum</option>
											<option value="National Commission - NCDRC">National Commission - NCDRC</option>
											<option value="State Commission">State Commission</option>
										</select></div>
									</div>
									<div id="NCDRC" class="col-sm-6 row common-select-div form-group" style="display: none;">
										<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Bench</label>
										<div class="col-sm-8"><select name="state" id="commission_bench" class="form-control select">
											<option value="">Please select</option>
											<option value="New Delhi">New Delhi</option>
										</select>
									</div>
								</div>
								<div id="state_commission_div" class="col-sm-6 row common-select-div form-group" style="display: none;"> 
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;State</label>
									<div class="col-sm-8"><select name="state" class="form-control" id="commission_state">                        
									</select></div>
								</div>
								<div id="commission_state_div" class="form-group common-select-div col-sm-6 row" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;District</label>
									<div class="col-sm-8"><select name="district1" id="commission_district" class="form-control">
										<option value=""> Please select</option>
										<option value="1">North Goa</option>
										<option value="2">South Goa</option>
									</select></div>
								</div>
								<div class="form-group col-sm-6 row common-select-div" id="ms" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;District</label>
									<div class="col-sm-8"><select name="state" class="form-control select" onchange="mahastate(this.options[this.selectedIndex].value)">
										<option value=""> Please select</option>
										<?php $District_court = mysqli_query($connection, "SELECT * FROM `District_court` ORDER BY dist_name ASC");
										while($selDistrict_court = mysqli_fetch_array($District_court)){?>
											<option value="<?php echo $selDistrict_court['dist_id'];?>"><?php echo $selDistrict_court['dist_name'];?></option>
										<?php } ?>

									</select></div>
								</div>
								<div class="form-group col-sm-6 common-select-div row" style="display: none;" id="circuit">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case type</label>
									<div class="col-sm-8"><select name="state" class="form-control" id="commission_state_casetype">
										<option value="">Please select</option>
										<option value="1">APPEAL EXECUTION(AE)</option>
										<option value="2">APPEAL EXECUTION(ae)</option>
										<option value="3">Appeal(A)</option>
										<option value="4">Appeal(a)</option>
										<option value="5">Caveat Cases(CV)</option>
										<option value="6">Caveat Cases(cv)</option>
										<option value="7">consumer case(CC)</option>
										<option value="8">consumer case(cc)</option>
										<option value="9">Execution Application(EA)</option>
										<option value="10">Execution Application(ea)</option>
										<option value="11">EXECUTION REVISION PETITION(ERP)</option>
										<option value="12">EXECUTION REVISION PETITION(erp)</option>
										<option value="13">First Appeal(FA)</option>
										<option value="14">First Appeal(fa)</option>
										<option value="15">Interlocutory Application(IA)</option>
										<option value="16">Interlocutory Application(ia)</option>
										<option value="17">Miscellaneous Application(ma)</option>
										<option value="18">Miscellaneous Application(MA)</option>
										<option value="19">Review Application(ra)</option>
										<option value="20">Review Application(RA)</option>
										<option value="21">Revision Petition(rp)</option>
										<option value="22">Revision Petition(RP)</option>
										<option value="23">Transfer Application(ta)</option>
										<option value="24">Transfer Application(TA)</option>
										<option value="0">Other</option>                          
									</select>
									<input type="text" class="form-control" id="commision_casetype_other" name="" placeholder="Other" autocomplete="off"></div>
								</div>
								<div id="tribunal_authority" class="form-group common-select-div col-sm-6 row" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Tribunals & Authorites</label>
									<div class="col-sm-8"><select id="tribunal_select" name="district2" class="form-control select">
										<option value=""> Please Select </option>
										<?php $Tribunals = mysqli_query($connection, "SELECT * FROM `tribunals` ORDER BY tribunals_name ASC");
										while($tribulanssel = mysqli_fetch_array($Tribunals)){?>
											<option value="<?php echo $tribulanssel['tribunals_id'];?>"><?php echo $tribulanssel['tribunals_name'];?></option>
										<?php } ?>
									</select></div>
								</div>
								<div class="form-group common-select-div col-sm-6 row" id="tribunal_state_div" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;State</label>
									<div class="col-sm-8"><select id="tribunal_state" class="form-control select">
										<option value="">  Please Select </option>
									</select></div>
								</div>
								<div class="form-group common-select-div col-sm-6 row" id="tribunal_bench_div" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Bench</label>
									<div class="col-sm-8"><select id="tribunal_bench" class="form-control select">
										<option value=""> Please Select</option>
									</select></div>
								</div>
								<div class="form-group common-select-div col-sm-6 row" id="tribunal_section_div" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Section</label>
									<div class="col-sm-8">
										<select id="tribunal_section" class="form-control select">
											<option value=""> Please Select</option>
										</select>
									</div>
								</div>
								<div class="form-group common-select-div col-sm-6 row" id="tribunal_location_div" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Location</label>
									<div class="col-sm-8">
										<select id="tribunal_location" class="form-control select">
											<option value=""> Please Select</option>
										</select>
									</div>
								</div>
								<div class="form-group common-select-div col-sm-6 row" id="case-type-tribunal" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>
									<div id="tribunal_casetype" class="col-sm-8">
										<select id="tribunal_case" class="form-control form-group select">
										</select>
										<input type="text" class="form-control" name="other" id="tribunal_other" placeholder="Tribunal Other Casetype" autocomplete="off" style="display: none;">
									</div>
								</div>

								<div id="revenue_court" class="form-group common-select-div col-sm-6 row" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Revenue Court</label>
									<div class="col-sm-8"><select id="revenue_states" name="district5" class="form-control select">
										<option value="">  Please Select </option>
										<option>Maharashtra</option>
									</select></div>
								</div>
								<div  class="form-group common-select-div col-sm-6 row" id="revenue_casetype_div" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>  
									<div class="col-sm-8"><input type="text" class="form-control" id="revenue_other" placeholder="Other" name="" autocomplete="off"></div>
								</div>
								<div id="commissionerate_div" class="form-group common-select-div col-sm-6 row" style="display: none;">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Commissionerate</label>
									<div class="col-sm-8"><select name="state" id="commissionerate_select" class="form-control select">
										<option value="">  Please select </option>
										<option value="Charity Commissionerate">Charity Commissionerate</option>
										<option value="Commissionerate of Commercial Taxes">Commissionerate of Commercial Taxes</option>
									</select></div>
								</div>  
								<div class="form-group common-select-div col-sm-6 row" style="display: none;" id="commissionerate_state_div">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;State</label>
									<div class="col-sm-8">
										<select name="state" id="commissionerate_state" class="form-control select">
											<option value="">  Please select</option>
											<option value="Maharashtra">Maharashtra</option>
										</select>
									</div>
								</div>
								<div class="form-group common-select-div col-sm-6 row" style="display: none;" id="courtside">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Court Side ID</label>
									<div class="col-sm-8"><select name="state" class="form-control select" id="commisionrate_courtside">
										<option value="">  Please Select </option>
										<?php $Court_Side = mysqli_query($connection, "SELECT * FROM `Court_Side` ORDER BY city_name ASC");
										while($selcourtlist=mysqli_fetch_array($Court_Side)){?>
											<option value="<?php echo $selcourtlist['court_sideId'];?>"><?php echo $selcourtlist['city_name'];?></option>
										<?php } ?>

									</select></div>
								</div>
								<div class="form-group common-select-div col-sm-6 row" id="commissionerate_authority" style="display: none;">
									<label class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Authority</label>
									<div class="col-sm-8"><select id="authority_select" class="form-control select">
										<option value="">Please select</option>
										<?php $authority = mysqli_query($connection, "SELECT * FROM `authority` ORDER BY authority_name ASC");
										while($authrisesel = mysqli_fetch_array($authority)){?>
											<option value="<?php echo $authrisesel['authority_id'];?>"><?php echo $authrisesel['authority_name'];?></option>
										<?php } ?>
									</select></div>
								</div>
								<div class="form-group col-sm-6 common-select-div row" style="display: none;" id="courtsidecasetype">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>
									<div class="col-sm-8">
										<select name="state" id="commissionerate_casetype" class="form-control select">
											<option value="">  Please select </option>
											<option value="419636">Appeal Or Caveat Application</option>
											<option value="419634">Change Report Application</option>
											<option value="419635">Scheme Change Application</option>
											<option value="419633">Society Registration Application</option>
											<option value="419632">Trust Registration Application</option>
											<option value="0">Other</option>
										</select>
									</div>
								</div>

								<div id="court_other" class="col-sm-6 row form-group" style="display: none;">
									<label class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" id="court_other_input" placeholder="Other" name="" autocomplete="off">
									</div>
								</div>
								<div class="col-sm-6 row form-group" id="case-div">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;"> Case Number 
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8"><input type="text" name="case_no" id="case_no" value="" class="form-control" autocomplete="OFF" ></div>
								</div>
								<div class="col-sm-6 row form-group" id="year-div">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp; Year </label>
									<div class="col-sm-8"><select name="year" id="case_no_year" class="form-control ">
										<option value=''>Select Year</option>
										<?php $year = mysqli_query($connection, "SELECT * FROM `year` ORDER BY date_time DESC");
										while($yearsel = mysqli_fetch_array($year)){?>
											<option value='<?php echo $yearsel['date_time'];?>'><?php echo $yearsel['date_time'];?></option>
										<?php } ?>

									</select>
								</div>
							</div>
							<div class="form-group col-sm-6 row" id="appearing_model" style="display: none;">
								<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Appearing  Model</label>
								<div class="col-sm-8 highcourtselection">
									<select name="state" id="appearing_modal" class="form-control appearing_modals select">

									</select>
								</div>
							</div>
							<div class="form-group col-sm-6 row" id="areyouappearingas" style="display: none;">
								<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Are you appearing as?</label>
								<div class="form-radio col">
									<div class="radio radio-primary radio-inline">
										<label>
											<input type="radio" class="chkPassport" name="appearing_radio" onclick="getAppearingInput()" checked id="appearing_radio_one" value="Petitioner">

											<i class="helper"></i>Petitioner
										</label>
									</div>
									<div class="radio radio-primary radio-inline">
										<label>
											<input type="radio" class="chkPassport"  name="appearing_radio" onclick="getAppearingInput()" id="appearing_radio_two" value="Respondent">

											<i class="helper"></i>Respondent
										</label>
									</div>
								</div>
	<!-- <div class="col-sm-4">
		<div class="form-check">
			<label class="form-check-label checkboxlabel" id="appearing_radio_one_label">            
				<input type="radio" class="chkPassport" name="appearing_radio" onclick="getAppearingInput()" checked id="appearing_radio_one" value="Petitioner">
				Petitioner
			</label>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-check">
			<label class="form-check-label checkboxlabel" id="appearing_radio_two_label">            
				<input type="radio" class="chkPassport"  name="appearing_radio" onclick="getAppearingInput()" id="appearing_radio_two" value="Respondent">
				Respondent
			</label>
		</div>
	</div> --> 
</div>
<div class="form-group row col-sm-6" id="appearingas_input" style="display: none;"></div>
<div id="cnr_yes"  class="form-group row col-sm-6" style="display: none;">
	<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;CNR</label>
	<div class="col-sm-8 row">
		<div class="col-sm-9"><input class="form-control" type="text" name="cnr_number" id="name" minlength="16" maxlength="16"> </div>
		<div class="col-sm-3" id="fetchDistrict">
			<div class="fetchLoaderDiv"></div>
			<button type="button" class=" btn btn-info btn-mini" id="fetch_cnr_data_id" onclick="datafetch()"> Fetch Data </button>
		</div>
	</div> 
</div>
<div class="form-group row col-sm-6" id="casedetails-div" style="display: none;">
	<div class="col-sm-4">
		<label>Case Details</label>
	</div>
	<div class="col-sm-8 form-group row" id="casedetails"></div>
</div>
<!-- <div class="form-group col-sm-6 row" id="ministry_div">
	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
	Ministry Desk Number </label>
	<div class="col-sm-8">
		
	<input type="text" class="form-control" id="mantralay_no" name="">
</div>
</div> -->
<!-- <div class="form-group col-sm-6 row" id="divisional_comm_level_div">
	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
	Divisional Commissioner Level</label> -->
	<!-- <div class="col-sm-4" style="float: left;">
		<div class="form-check">
			<label class="form-check-label checkboxlabel" style="text-align: left;">
				<input type="radio" class="chkPassport" name="commissioner_level"  value="Establishment">
				आस्थापना शाखा / Establishment Branch
			</label>
		</div>
	</div>

	<div class="col-sm-4" style="float: left;">
		<div class="form-check">
			<label class="form-check-label checkboxlabel" style="text-align: left;">
				<input type="radio" class="chkPassport" name="commissioner_level" value="Development">
				विकास शाखा / Development Branch 
			</label>

		</div>
	</div> --> 
	<!-- <div class="col"> 
		<div class="checkbox-zoom zoom-primary">
			<label>
				<input type="checkbox" class="chkPassport" name="commissioner_level"  value="Establishment">
				<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary">
					</i>
				</span>
				<span>Establishment Branch</span>
			</label>
		</div>
		<div class="checkbox-zoom zoom-primary">
			<label>
				<input type="checkbox" class="chkPassport" name="commissioner_level"  value="Development">
				<span class="cr">
					<i class="cr-icon icofont icofont-ui-check txt-primary">
					</i>
				</span>
				<span> Development Branch</span>
			</label>
		</div>
	</div> -->
	<!-- <div class="form-radio col">
		<div class="radio radio-primary radio-inline">
			<label>
				<input type="radio" class="chkPassport" name="commissioner_level"  value="Establishment">	
				<i class="helper"></i>आस्थापना शाखा / Establishment Branch
			</label>
		</div>
		<div class="radio radio-primary radio-inline">
			<label>
				<input type="radio" class="chkPassport" name="commissioner_level" value="Development">			
				<i class="helper"></i>विकास शाखा / Development Branch 
			</label>
		</div>
	</div> -->    
	<!-- </div> -->
<!-- <div class="form-group col-sm-6 row" id="department_div">
	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
	 Department </label>
	<div class="col-sm-8"><select class="form-control select" id="department"></select></div>
</div> -->
<div class="form-group col-sm-6 row" >
	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
	Date of filing</label>
	<div class="col-sm-8">
		<div class="input-group">
			
			<input type="text" id="dateoffilling" class="form-control" name="dateoffiling" value=""/>
			<i class="fa fa-calendar input-group-addon" id="basic-addon1"></i>
			
		</div>
	</div>
</div>
<!-- <div class="col-sm-6 form-group row" id="court_hall_div">
	<label class="col-sm-4 col-form-label"> Court Hall # </label>
	<div class="col-sm-8"><input type="text" id="court_hall" name="courthall" class="form-control" autocomplete="off"/></div>
</div>
<div class="col-sm-6 form-group row" id="floor_div">
	<label class="col-sm-4 col-form-label">Floor#</label>
	<div class="col-sm-8"><input type="text" id="floor" name="floor" class="form-control" autocomplete="off"/></div>
</div> -->
<!-- <div class="form-group row col-sm-6" id="file-no-div" >
	<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red">*</span>  File No. 
	</label>
	<div class="col-sm-8"><input type="text" id="nastikramank" class="form-control" value="" name="title" autocomplete="off"/></div>

</div> -->
<div class="form-group col-sm-6 row" id="subject-div">
	<label for="exampleInputUsername1" class="col-form-label col-sm-4"> Subject </label>
	<div class="col-sm-8">
		<textarea rows="3" id="classification" class="form-control" name="classif" value="" autocomplete="off"></textarea>
	</div>
</div>
<div class="form-group col-sm-6 row" id="nominal-div">
	<label for="exampleInputUsername1" class="col-form-label col-sm-4">Nominal </label>
	<div class="col-sm-8">
		<input type="text" id="nominal" class="form-control" name="nominal" value="" autocomplete="off">
	</div>
</div>
<!-- <div class="form-group row col-sm-12">
	<label for="exampleInputUsername1" class="col-sm-2 col-form-label" style="float: left;">
		 Judicial</label>
			 
			<div class="form-radio col">
				<div class="checkbox-zoom zoom-primary">
                                    <label>
                                     <span class="srno">Judicial&nbsp;</span> <input type="checkbox" class="check case_checkbox" name="select_case_single" id="case_checkbox" value="Judicial">
                                     <span class="cr">
                                      <i class="cr-icon icofont icofont-ui-check txt-primary">
                                      </i>
                                    </span>
                                  </label>
                                </div>
                                <div class="checkbox-zoom zoom-primary">
                                    <label>
                                     <span class="srno">Non-Judicial&nbsp;</span> <input type="checkbox" class="check case_checkbox" name="select_case_single" id="case_checkbox" value="Non-Judicial">
                                     <span class="cr">
                                      <i class="cr-icon icofont icofont-ui-check txt-primary">
                                      </i>
                                    </span>
                                  </label>
                                </div>
				 
			</div>
		</div> -->
		<div class="form-group col-sm-12 row">
			<label for="exampleInputUsername1" class="col-sm-2 col-form-label"><!-- <span style="color: red;">*</span> --> Title 
				<span class="lbl-short-desc"><i>(Please enter the title which you can remember easily)</i></span></label>
				<div class="col-sm-8 col-md-10"><textarea rows="3" id="title" class="form-control" value="" name="title" autocomplete="off"></textarea></div>

			</div>

			<div class="form-group col-sm-12" id="" >
				<label for="exampleInputUsername1" class="col-form-label"> Description 
					<span class="lbl-short-desc"><i>(Please enter primary details about the case, subject, CNR Number,etc)</i></span>
			<!-- <div class="form-radio col">
				<div class="radio radio-primary radio-inline">
					<label>
						<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('English')" value="English" checked>
						<i class="helper"></i>English
					</label>
				</div>
				<div class="radio radio-primary radio-inline">
					<label>
						<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('Marathi')" value="Marathi">			
						<i class="helper"></i>Marathi
					</label>
				</div>
			</div> -->
		</label>

		<!-- <textarea name="details" id="txtEngToHindi" class="form-control"></textarea>  -->
		<textarea name="details" id="query" class="ckeditor"></textarea> 
	</div>
	<!-- <div class="form-group col-sm-6 row" id="appearing">
		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
		Before Hon'ble Judge(s) </label>
		<div class="col-sm-8">
			<input type="text" id="judge" name="judge" class="form-control" value="" autocomplete="off"/>
		</div>
	</div> -->
	<div class="col-sm-12 form-group row">
		<label for="exampleInputUsername1" class="col-sm-2 col-form-label">
		Priority </label>
		<div class="col-sm-4"><select id="cases-priority" class="form-control " name="Cases_priority" aria-invalid="false">
			<option value=""> Please select</option>
			<option value="Super Critical">Super Critical</option>
			<option value="Critical">Critical</option>
			<option value="Important">Important</option>
			<option value="Routine">Routine</option>
			<option value="Others">Others</option>
			<option value="Normal">Normal</option>
		</select>
	</div>	
	<label for="exampleInputUsername1" class="col-sm-2 col-form-label">
	Your Team</label>	
	<div class="col-sm-4">
		<select id="framework" name="framework[]" multiple class="form-control select2" >

		</select>
	</div>							
</div>
<!-- <div class="col-sm-12 form-group row">
	<label for="exampleInputUsername1" class="col-sm-2 col-form-label">
	Your Team</label>
	<div class="col-sm-8">
		<select id="framework" name="framework[]" multiple class="form-control select2" >

		</select>
	</div>
</div> -->
<!-- <div class="form-group row col-sm-12">
	<label for="exampleInputUsername1" class="col-sm-2 col-form-label" style="float: left;">
		Is the Vakalatnama filed? <span style="color: red;">*</span></label>
			 
			<div class="form-radio col">
				<div class="radio radio-primary radio-inline">
					<label>
						<input type="radio" class="chkPassport" name="vakalathRadios" id="vakalathYes" onclick="vakalathShow('Yes')" value="Yes">
						<i class="helper"></i> Yes
					</label>
				</div>
				<div class="radio radio-primary radio-inline">
					<label>
						<input type="radio" class="chkPassport" onclick="vakalathShow('No')" name="vakalathRadios" id="vakalathNo" value="No" checked>		
						<i class="helper"></i> No
					</label>
				</div>
			</div>
		</div> -->


		<!-- <div class="form-group col-sm-6 row" id="vakalathDate" style="display: none;">
			<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Vakalatnama filling Date</label>
			<div class=" col-sm-8">
				<div class="input-group">
					<input type="text" class="form-control" id="vakalath_filling_date" name="startdate" aria-describedby="basic-addon2" autocomplete="off">
					<span class="input-group-addon ">
						<span class="icofont icofont-ui-calendar"></span>
					</span>
				</div>
			</div>
		</div> -->
		<!-- <div class="col-sm-6 form-group row" id="vakalathBrowse" style="display: none;">
			<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Attach Vakalatnama Document </label>
			<div class="col-sm-8"><input type="file" id="vakalat_file" aria-label="File browser example"></div>
		</div> -->
		<!-- <div class="form-group col-sm-12 row">
			<label for="exampleInputUsername1" class="col-sm-2 col-form-label" style="float: left;">
				Is the affidavit filed? <span style="color: red;">*</span></label>
				
				<div class="form-radio col">
					<div class="radio  radio-primary radio-inline">
						<label>
							<input type="radio" class="chkPassport" name="affidaviteRadios" id="affidavitYes" onclick="affidavitShow('Yes')" value="Yes">
							<i class="helper"></i> Yes
						</label>
					</div>
					<div class="radio  radio-primary radio-inline">
						<label>
							<input type="radio" class="chkPassport" onclick="affidavitShow('No')" name="affidaviteRadios" id="affidavitNo" value="No" checked>
							<i class="helper"></i> No
						</label>
					</div>
					<div class="radio  radio-primary radio-inline">
						<label>
							<input type="radio" class="chkPassport" onclick="affidavitShow('No')" name="affidaviteRadios" id="affidavitNotApplicable" value="Not Applicable" >	
							<i class="helper"></i>Not Applicable
						</label>
					</div>
				</div>
			</div> -->
			<!-- <div class="form-group col-sm-6 row" id="affidavitDate" style="display: none;">

				<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Affidavit filling Date</label>
				<div class="form-group col-sm-8">
					<div class="input-group">
						<input type="text" class="form-control" id="affidavit_filling_date" name="startdate" aria-describedby="basic-addon2" autocomplete="off">
						<span class="input-group-addon ">
							<span class="icofont icofont-ui-calendar"></span>
						</span>
					</div>

				</div>
			</div> -->
		<!-- 	<div class="form-group col-sm-6 row" id="affidavitBrowse" style="display: none;">
				<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Attach Affidavit Document </label>
				<div class="col-sm-8"><input type="file" id="attach_file" aria-label="File browser example"></div>
			</div> -->
			<div class="col-sm-12 grid-margin stretch-card">
				
				<div class="accordion accordion-solid-header" id="organization-accordion-4" role="tablist">
					<div class="card border-bottom" id="petitioner_adv_div">
						<div class="card-header" role="tab" id="heading-10">
							<h6 class="mb-0">
								<a data-toggle="collapse" href="#collapse-10" aria-expanded="true" aria-controls="collapse-10">Client Details (<span class="appearing_as_first">Petitioner</span>) </a>
							</h6>
						</div>
						<div id="collapse-10" class="collapse show" role="tabpanel" aria-labelledby="heading-10" data-parent="#organization-accordion-4">
							<table id="petitioneradv_table" class=" table order-list" width="100">
								<thead>
									<tr>
										<th class="full-name">Full Name</th>
										<th>Email Address</th>
										<th>Phone Number</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>

										<td> 
											<input type="text" name="name" id="your_adv_name0" class="form-control" autocomplete="off"/>
										</td>
										<td>
											<input type="mail" name="mail" id="your_adv_email0" class="form-control" autocomplete="off"/>
										</td>
										<td>
											<input type="text" name="phone" id="your_adv_mobile0" class="form-control" autocomplete="off"/>
										</td>
										<td>

										</td>
									</tr>
								</tbody>
								<tfoot>
									<td colspan="4" style="text-align: right;"><button type="button" class="btn btn-sm btn-info add-opponent add-more-button" id="petitioner_advocate_add" onclick="addPetitionerAdv()"> Add More </button>
									</td>
								</tfoot> 
							</table>
						</div>
					</div>

					<div class="card border-bottom" id="petitioner_div" style="display: none;">
						<div class="card-header" role="tab" id="heading-13">
							<h6 class="mb-0">
								<a class="collapsed" data-toggle="collapse" href="#collapse-13" aria-expanded="true" aria-controls="collapse-13">
									Client Details (<span class="appearing_as_first">Petitioner</span>)
								</a>
							</h6>
						</div>
						<div id="collapse-13" class="collapse show" role="tabpanel" aria-labelledby="heading-13" data-parent="#organization-accordion-4">
							
							<table id="petitioner_table" class="table order-list1" width="100">
								<thead>
									<tr>
										<th class="full-name">Full Name</th>
										<th>Email Address</th>
										<th>Phone Number</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> 
											<input type="text" name="name" class="form-control"  id="your_name0" autocomplete="off"/>
										</td>
										<td>
											<input type="mail" name="mail"  class="form-control" id="your_email0" autocomplete="off"/>
										</td>
										<td>
											<input type="text" name="phone"  class="form-control" id="your_mobile0" autocomplete="off"/>
										</td>
										<td>
										</td>
									</tr>
								</tbody>      
								<tfoot>
									<td colspan="4" style="text-align: right;"><button type="button" class="btn btn-sm btn-info add-opponent add-more-button" onclick="addPetitioner()" id="petitioner_add">  Add More </button>
									</td>
								</tfoot>                              
							</table>

						</div>
					</div>

					<div class="card border-bottom" id="respondent_div">
						<div class="card-header" role="tab" id="heading-11">
							<h6 class="mb-0">
								<a class="collapsed" data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">
									Opposite Party (<span class="appearing_as_second">Respondent</span>)
								</a>
							</h6>
						</div>
						<div id="collapse-11" class="collapse" role="tabpanel" aria-labelledby="heading-11" data-parent="#organization-accordion-4">
							
							<table id="repondant_table" class="table order-list1" width="100">
								<thead>
									<tr>
										<th class="full-name">Full Name</th>
										<th>Email Address</th>
										<th>Phone Number</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> 
											<input type="text" name="name" class="form-control"  id="opponent_name0" autocomplete="off"/>
										</td>
										<td>
											<input type="mail" name="mail"  class="form-control" id="opponent_email0" autocomplete="off"/>
										</td>
										<td>
											<input type="text" name="phone"  class="form-control" id="opponent_mobile0" autocomplete="off"/>
										</td>
										<td>
										</td>
									</tr>
								</tbody>      
								<tfoot>

									<td colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button btn btn-sm btn-info " onclick="addRespondant()" id="respondant_add">  Add More </button>
									</td>
								</tfoot>                              
							</table>

						</div>
					</div>

					<div class="card" id="respondent_adv_div">
						<div class="card-header" role="tab" id="heading-12">
							<h6 class="mb-0">
								<a class="collapsed" data-toggle="collapse" href="#collapse-12" aria-expanded="false" aria-controls="collapse-12">
									Opposing Counsels (<span class="appearing_as_second">Respondent</span>)
								</a>
							</h6>
						</div>
						<div id="collapse-12" class="collapse" role="tabpanel" aria-labelledby="heading-12" data-parent="#organization-accordion-4">
							
							<table id="respondant_adv_table" class=" table order-list" width="100">
								<thead>
									<tr>
										<th class="full-name">Full Name</th>
										<th>Email Address</th>
										<th>Phone Number</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>

											<input type="text" name="name"  id="opponent_adv_name0" class="form-control" autocomplete="off" />
										</td>
										<td>
											<input type="mail" name="mail"  id="opponent_adv_email0"  class="form-control" autocomplete="off"/>
										</td>
										<td>
											<input type="text" name="phone"  id="opponent_adv_mobile0"  class="form-control" autocomplete="off"/>
										</td>
										<td>
										</td>
									</tr>
								</tbody>  
								<tfoot>
									<td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info add-opponent add-more-button" onclick="respondantAdvAdd()" id="respondant_advocate_add">  Add More </button></td>
								</tfoot>                                
							</table>							
						</div>
					</div>
				</div>




			</div>
			<div class="col-sm-12 row">
				<div class="add_casesubmitdiv" style="
				padding-right: 0;
				">
				<button class="btn btn-info btn-sm add_casesubmit" id="organization_submit" name="add_case" type="button" onclick="submitCase()" style="/* display: none; */"> Submit </button>&nbsp;&nbsp;
			</div>
			<!-- <button class="btn btn-info btn-sm" id="individual_submit" name="add_case" type="button" onclick="submitIndividualCase()">समावेश करा / Submit </button> -->
			<!-- <div class="col-sm-6"> -->
				<a class="btn btn-danger btn-sm" href="add-cases.php"> Cancel </a></div>
			<!-- </div> -->
		</form>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal" id="data">
	<div class="modal-dialog modal-lg fetchdata-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><b>Please Map the Following Details</b></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" style="padding: 0px;" id="modal-data">

			</div>

		</div>

	</div>
</div>
<?php include 'footer.php'; ?>

<script src="scripts/aes.js"></script>
<script src="scripts/pbkdf2.js"></script>
<script type="text/javascript" src="scripts/jsapi.js"></script>
<script type="text/javascript" src="scripts/add_cases_new.js"></script>
<script type="text/javascript" src="scripts/add_individual_case.js"></script>