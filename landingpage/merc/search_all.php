<link rel="stylesheet" type="text/css" href="styles/caselaw-search.css">
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<?php session_start(); ?> 
<style type="text/css">
  .fetchdataloader {
    border: 10px solid #f3f3f3;
    border-radius: 50%;
    border-top: 10px solid #3498db;
    width: 39px;
    float: right;
    height: 39px;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;


}
</style>
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <?php $case = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".$_GET['caseid']."'");
          $selcase = mysqli_fetch_array($case);
           if($selcase['supreme_court'] == 'Diary Number')
                                     $case =  $selcase['diary_no'].'/ '.$selcase['diary_year'];
                                    else
                                     $case = $selcase['case_type'].' '.$selcase['case_no'].'/ '.$selcase['case_no_year']; ?>
          <div class="page-wrapper">
            <div class="page-body">
             <div class="card">
              <div class="card-block">
                <div class="row">
                   <input type="hidden" class="count" name="">
                  <h3 class="card-header form-group title_h3 col-sm-10" style="padding-top: 1%" id="district_court_title"><?php echo $case;?> </h3>
                  <span class="col-sm-2 form-group" style="text-align: right;"><a href="view-case.php?caseid=<?php echo base64_encode($_GET['caseid']);?>" target="_BLANK">Back To Case</a></span>
                  <div class="col-sm-12" style=" font-size: 15px;">Research case law & judgement from web regarding your case</div>
                  <div class="input-group col-sm-8 mb-3" style="float: left;">
                    <input type="text" class="form-control  " placeholder="Please enter here yours search query" name="search" id="searchInput">
                    <input type="hidden" class="form-control inputBox" id="case_id" name="text" value="<?php echo $_GET['caseid'];?>" >
                    <div class="input-group-append add_casesubmitdiv">
                      <span class="input-group-text btn add_casesubmit" style="background: blue;color: white" onclick="getquery('1')"><i class="fa fa-search"></i></span>
                    </div>
                  </div>
                  <div class="col-12 form-group " style="float: left;"><span id="count"><b></b></span></div>
                 <!--  <div class="col-sm-4 mb-3" style="float: left;">
                   <button class="btn btn-info btn-sm" type="button" onclick="viewCase('<?php echo $_GET['caseid']; ?>')">View Case</button> 
                 </div> -->
                 <!-- <div id="show_data" class="col-sm-12"></div> -->
                 <div class=" col-sm-12 onlineShortnote"></div>
                 <div class="pagination" class="col-sm-12"></div>
               </div>
             </div>
           </div>
         </div>
<!-- <div id="styleSelector">
</div> -->
</div>
</div>
</div>
</div>
</div>
<!-- </div>
</div> -->
<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/search_all.js"></script>

