<?php
include('includes/dbconnect.php');
session_start();
header('Access-Control-Allow-Origin: *');
// $edit       = $_POST['edit_data'];
// $data       = json_decode($edit, true);
// print_r($data);
$arr        = array();
$date       = date('Y-m-d H:i:s');
$edit_quasi = mysqli_query($connection, "SELECT `case_id`,`user_id`,`court_id`,`complaint_authority`,`ministry`,`zp_name`,`case_district`,`under_division`,`div_comm_level`,`name_of_matter`,`classification`,`date_of_filling`,`case_type`,`case_no`,`case_no_year`,`case_title`,`Stage`,`hearing_date`,`hearing_nextstage`,`pet_address`,`pet_mobile`,`pet_email_id`,`res_address`,`res_email_id`,`res_name`,`res_mobile`,`pet_name`,`suggetion` FROM `reg_cases` WHERE `case_id`='" . $_POST['case_id'] . "'");
while ($fetch_quasi = mysqli_fetch_array($edit_quasi)) {
    $edit_data_object                            = new stdClass();
    $edit_data_object->case_id             = TRIM($fetch_quasi['case_id']);
    $edit_data_object->user_id             = TRIM($fetch_quasi['user_id']);
    $edit_data_object->court_id            = TRIM($fetch_quasi['court_id']);
    $edit_data_object->competant_authority = TRIM($fetch_quasi['complaint_authority']);
    $edit_data_object->ministry            = TRIM($fetch_quasi['ministry']);
    $edit_data_object->desk_number         = TRIM($fetch_quasi['zp_name']);
    $edit_data_object->division            = TRIM($fetch_quasi['case_district']);
    $edit_data_object->zp_name             = TRIM($fetch_quasi['under_division']);
    $edit_data_object->div_comm_level      = TRIM($fetch_quasi['div_comm_level']);
    $edit_data_object->department          = TRIM($fetch_quasi['name_of_matter']);
    $edit_data_object->subject             = TRIM($fetch_quasi['classification']);
    $edit_data_object->date_of_filling     = TRIM($fetch_quasi['date_of_filling']);
    $edit_data_object->case_type           = TRIM($fetch_quasi['case_type']);
    $edit_data_object->case_no             = TRIM($fetch_quasi['case_no']);
    $edit_data_object->case_no_year        = TRIM($fetch_quasi['case_no_year']);
    $edit_data_object->case_title          = TRIM($fetch_quasi['case_title']);
    $edit_data_object->Stage               = TRIM($fetch_quasi['Stage']);
    $edit_data_object->hearing_date        = TRIM($fetch_quasi['hearing_date']);
    $edit_data_object->hearing_nextstage   = TRIM($fetch_quasi['hearing_nextstage']);
    $edit_data_object->pet_address         = TRIM($fetch_quasi['pet_address']);
    $edit_data_object->pet_mobile          = TRIM($fetch_quasi['pet_mobile']);
    $edit_data_object->pet_email_id        = TRIM($fetch_quasi['pet_email_id']);
    $edit_data_object->res_address         = TRIM($fetch_quasi['res_address']);
    $edit_data_object->res_email_id        = TRIM($fetch_quasi['res_email_id']);
    $edit_data_object->res_name            = TRIM($fetch_quasi['res_name']);
    $edit_data_object->res_mobile          = TRIM($fetch_quasi['res_mobile']);
    $edit_data_object->pet_name            = TRIM($fetch_quasi['pet_name']);
    $edit_data_object->suggetion           = TRIM($fetch_quasi['suggetion']);
    
    $case_details[] = $edit_data_object;
    // print_r($case_details);
    $arr          = array(
        'data' => $case_details
    );
}
echo json_encode($arr,true);
?>