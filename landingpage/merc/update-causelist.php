<?php include("header.php");?>
<?php include("menu.php");?>
<div class="main-panel">        
  <div class="content-wrapper">
    <?php include("slider.php");?>

  <?php
  //include("../includes/dbconnect.php");
  if(isset($_POST['submit'])){
 


      $court=$_POST['court_id'];
      $highcourt=$_POST['high_court_id'];
      $hcbench=$_POST['hc_bench_id'];
      $advocatename=$_POST['advocate_name'];
      $date = date('Y-m-d H:i:s');
      if($court!='')
      {
         $Update=mysqli_query($connection,"UPDATE `causelist` SET `user_id`='".$_SESSION['user_id']."',`court`='".$court."',`highcourt_name`='".$highcourt."',`bench`='".$hcbench."',`advocate_name`='".$advocatename."',
          `date_time`='".$date."' WHERE `cause_listid`='".$_GET['id']."'");
        if($Update){
                    ?>
                    <script type="text/javascript">
                      alert("Causelist Update Successflly!");
                      window.location.href='manage-causelist.php';
                    </script>
                    <?php
                  }else{
                    ?>
                    <script type="text/javascript">
                      alert("Causelist Update Failed!");
                    </script>
                    <?php
                  }
                }
    }
  ?>
  <?php 
   $sql1=mysqli_query($connection,"SELECT * FROM `causelist` where `cause_listid`='".$_GET['id']."'");
  $row1=mysqli_fetch_array($sql1);
  ?>

    <div class="card">
      <div class="card-header">
       <h4>Update Cause List</h4>
     </div>
      <form class="forms-sample" method="POST" enctype="multipart/form-data">
     <div class="card-body">
       <div class="form-group row">
         <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Court</b></label>
         <div class="col-sm-8">
           <select id="cases-court_id" class="form-control" name="court_id" onchange="courts(this.options[this.selectedIndex].value)">
            <?php $sql=mysqli_query($connection,"SELECT * from `court_list` WHERE `court_id`='".$row1['court']."'");
            $fetchsql=mysqli_fetch_array($sql);?>
            <option value="<?php echo  $row1['court'];?>"><?php if($fetchsql['court_name']){echo $fetchsql['court_name'];} else{?>Please select<?php } ?></option>
            <?php $courtname = mysqli_query($connection, "SELECT * FROM `court_list` ORDER BY court_id ASC");
            while($selcourtlist = mysqli_fetch_array($courtname)){?>
             <option value="<?php echo $selcourtlist['court_id'];?>"><?php echo $selcourtlist['court_name'];?></option>
           <?php } ?>
           <option value="9">Other</option>
         </select>
       </div>
     </div>
     <div class="form-group row comingSoon" id="comingsoon" >
       <h2>Coming Soon...</h2>
     </div>
     <div id="highcourt">
       <div class="form-group row">
        <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>High court</b></label>
        <div class="col-sm-8">
         <select id="cases-high_court_ids" class="form-control" name="high_court_id" onchange="showbench(this.options[this.selectedIndex].value)">
          <?php $sql2=mysqli_query($connection,"SELECT * from `high_court_list` WHERE `court_id`='".$row1['highcourt_name']."'");
            $fetchsql2=mysqli_fetch_array($sql2);?>
           <option value="<?php echo $row1['highcourt_name'];?>"><?php if($fetchsql2['high_court_name']){echo $fetchsql2['high_court_name'];}else{?>Please select<?php }?></option>
           <?php $highcourt= mysqli_query($connection,"SELECT * FROM `high_court_list` ORDER BY high_court_name ASC");
           while($seligh = mysqli_fetch_array($highcourt)){?>
             <option value="<?php echo $seligh['court_id'];?>"><?php echo $seligh['high_court_name'];?></option>
           <?php } ?>
         </select>
       </div>
     </div>

     <div class="form-group row comingSoon" id="comingsoonHighCourt">
       <h2>Coming Soon...</h2>
     </div>
     <div class="form-group row" id="bench">
      <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Bench</b></label>
      <div class="col-sm-8">
       <select  class="form-control highcourtidslist" name="hc_bench_id" id="high_court_idslist" >
        <?php $sql3=mysqli_query($connection,"SELECT * from `bench` WHERE `bench_id`='".$row1['bench']."'");
            $fetchsql3=mysqli_fetch_array($sql3);?>
         <option value="<?php echo $row1['bench'] ;?>"><?php if($fetchsql3['bench_name']){echo $fetchsql3['bench_name'];}else{?>Please select<?php }?></option>

          <?php $bench= mysqli_query($connection,"SELECT * FROM `bench` ORDER BY bench_name ASC");
           while($seligh1 = mysqli_fetch_array($bench)){?>
             <option value="<?php echo $seligh1['bench_id'];?>"><?php echo $seligh1['bench_name'];?></option>
           <?php } ?>
       </select>
     </div>
   </div>
   <div class="form-group row" id="advocatename">
     <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Advocate Name</b></label>
     <div class="col-sm-8">
      <input type="text" class="form-control" value="<?php echo $row1['advocate_name']; ?>" name="advocate_name" id="advocate_name" placeholder="Enter Advocate Name">
    </div>  
  </div>
</div>
<div class="form-group row" id="saveandcancel">               
  <button class="btn btn-sm newsubmit" name="submit"  onclick="submitCauseList()">Update</button>
  &nbsp; &nbsp;
  <a href="manage-causelist.php" class="btn btn-danger btn-sm">Cancel</a>
</form>
</div>
</div>
</div>
</div>
<?php include("footer.php");?>
<script type="text/javascript" src="js/causelist.js"></script>

</div>