<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
      <?php include("menu.php");?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="card">
            <div class="card-body">
              <h4 class="card-header" style="padding-top: 0px">Import Cases</h4>
              <div class="row" style="margin-top: 25px;text-align: center;">
                <div class="col-12">
                  <div class="form-group row" id="searchby">
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Search by</b></label>
                    <div class="col-sm-8">
                      <select id="import-type" class="form-control" name="Import[type]" aria-invalid="false">
                        <option value="byadvname">Advocate Name</option>
                        <option value="bybarcode">Bar Code</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row" id="advocatename">
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Advocate</b></label>
                    <div class="col-sm-8">
                      <input type="text" name="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row" id="barcode">
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Bar Code</b></label>
                    <div class="col-sm-2">
                      <input type="text" name="" class="form-control" placeholder="State Code">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="" class="form-control" placeholder="Bar Code">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="" class="form-control" placeholder="Bar Year">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php include("footer.php");?>