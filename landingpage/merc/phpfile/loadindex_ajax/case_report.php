<?php session_start(); include("../includes/dbconnect.php");?>
<link rel="stylesheet" href="../vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="../law/admin/css/vertical-layout-light/case-report.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="button_div row">
				<div class="col-sm-5">
					<form method="POST">
						<select name="name" class="form-control" id="state" onchange='this.form.submit()'>
							<option value="<?php echo $name; ?>"><?php  if($name!=""){ echo $name; }elseif($name==""){ echo "Running"; }else{?>--Select Option--<?php } ?></option>
							<option value="All">All</option>
							<option value="Running" >Running</option>
							<option value="Closed">Closed</option>
							<option value="Transfer/NOC Given">Transfer/NOC Given</option>
							<option value="Direction Matters">Direction Matters</option>
							<option value="Order Reserved">Order Reserved</option>
							<option value="Contempt Petition">Contempt Petition</option>         
						</select>
					</form>
				</div>
				<div class="col-sm-3"><button class="btn btn-secondary btn-sm" type="button" onclick="redirectToAddCases()">Add Case</button></div>
				<div class="col-sm-2"><button class="btn btn-sm btn-secondary" id="myBtn" data-target="#filter" data-toggle="modal" style="">Filter</button></div>
				<div class="col-sm-2"><button class="btn btn-sm btn-secondary" id="myBtn" data-target="#export" data-toggle="modal" style="">Export</button></div>
			</div>
		</h4>
		<!-- <form class="form-inline  " method="POST"> 
          <input type="text" class="form-control col-sm-3 searchanyinput" placeholder="Search any cases here" id="casenoSearch" >
          <i class="fa fa-search searchicon" aria-hidden="true" ></i>
          <div id="suggesstion-box" class="animated fadeInLeft"></div>
        </form> -->
 <script>
       $(document).ready(function(){
        $("#casenoSearch").keyup(function(){
     
          $.ajax({
            type: "POST",
            url: "search_caseNo.php",
            data:'keyword='+$(this).val(),
            beforeSend: function(){
              $("#caseno_search").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
            },
            success: function(data){
            	// alert(data);
              $("#suggesstion-box1").show();
              $("#suggesstion-box1").html(data);

              $("#caseno_searchData").css("background","#FFF");
            }
          });
        });
      });
  </script>
<div class="col-sm-12  casereportdata">
			
			<form  method="POST">
				<div class="col-sm-12 row searchcase"> <label for="exampleFormControlTextarea4" class="searchanylabel"><b>Search Cases</b></label>
<!--   <textarea class="form-control" id="exampleFormControlTextarea4" rows="7"></textarea> -->

  <input type="text" class="form-control col-sm-3 searchanyinput" placeholder="Search any cases here" id="casenoSearch" >
<i class="fa fa-search searchanyicon" aria-hidden="true" ></i>
          <div id="suggesstion-box1" class="animated fadeInLeft"></div>
  <input type="submit" class="btn btn-warning btn-sm searchanyfinal" value="submit" name="submit">
		</div>
		<div class="main_body">
			<div class="col-12">


				<div class="table-responsive">
					<table id="example" class="table" border="0.25" width="100%">
						<thead>

							<tr>
								<th style="" class="srno-report">ID</th>
								<th style="" class="checkclass"><input type="checkbox" class="selectall" id="checkAll" /></th>
								<th style="" class="court-report">Name of Court</th>
								<th style="" class="case-report">Case No / Year</th>                                  
								<th style="" class="title-report">Name of Dept</th>
								<th style="" class="title-report">Subject</th>
								<!-- <th style="" class="title-report">Subject</th> -->
								<th style="" class="title-report">Case Description</th>
								<th style="" class="hearing-report">Hearing Date</th>
								<th style="" class="team-report">Team Member(s)</th>
								<!-- <th style="" class="case-details-report">Assign To</th> -->
								<!-- <th style="" class="case-details-report">Case Law Research</th> -->
								<th style="" class="action-report">Activity</th>
								
							</tr>
						</thead>
						<tbody>
							<?php 
							if($_GET['caseid']==""){
							if($name!="" && $name!="All"){
								$court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_district`='".$_SESSION['cityName']."'  AND `case_status`='".$name."' AND `remove_status`=1 ORDER BY `case_id`  DESC");
							}elseif($name!="" && $name=="All"){
								$court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_district`='".$_SESSION['cityName']."' AND `remove_status`=1 ORDER BY `case_id` DESC ");
							}else{
								$court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_district`='".$_SESSION['cityName']."' AND `case_status`='Running' AND `remove_status`=1 ORDER BY `case_id` DESC");
							}
						}else{
							 $court = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_district`='".$_SESSION['cityName']."' AND  `remove_status`=1 AND `case_id`='".base64_decode($_GET['caseid'])."'");
						}
							$i=1;
							while($selcourt = mysqli_fetch_array($court)){
								$casecode = mysqli_query($connection, "SELECT * FROM `activity_priority` WHERE `priority_name`='".$selcourt['priority']."'");
								$setcolorecount = mysqli_num_rows($casecode);
								$selcolor = mysqli_fetch_array($casecode);
								?>
								<input type="hidden" name="" id="caseid">
								<tr style="cursor:pointer;background-color: <?php if($setcolorecount==1){ echo  $selcolor['priority_color']; }  ?>"; >
									<td class="rowclickable id_case" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>"><?php echo $i;?></td>
									<td class="rowclickable checkboxtimesheet" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>"><input type="checkbox" class="check" name="checkAll[]" / style="padding: 12px;"></td>
									<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
										
										<?php  if(is_numeric ($selcourt['court_name']) AND $selcourt['court_name']==2){ 
											$courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname);echo $courtlist['court_name']; echo " - "; echo $selcourt['high_court']; echo " - "; echo $selcourt['hc_bench']; echo " - "; echo $selcourt['hc_side']; echo " - "; echo $selcourt['hc_stamp_register'];}elseif($selcourt['court_name']==1){
												$courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname);
												echo $courtlist['court_name']; }elseif($selcourt['court_name']==3){   $courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname); echo $courtlist['court_name'];}else{echo $selcourt['court_name'];}?> 
											</td>                                 
											<td class="rowclickable"  data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
												
												<?php if(is_numeric ($selcourt['court_name'])){ 
													$courtname = mysqli_query($connection, "SELECT * FROM `court_list` WHERE `court_id`='".$selcourt['court_name']."'"); $courtlist = mysqli_fetch_array($courtname);echo $selcourt['hc_case_type']; echo " "; echo $selcourt['case_no']; echo " / "; echo $selcourt['case_no_year'];  } else{ echo $selcourt['case_no'];}?>
												</td>
												<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
													<?php echo $selcourt['name_of_matter'];?></td>
												<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
													<?php echo $selcourt['case_title'];?></td>
													<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
													<?php echo $selcourt['case_description'];?></td>
													<!-- <td is="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
														<?php echo $selcourt['case_description'];?></td> -->
														<td class="rowclickable" style="padding: 10px;"><?php if($selcourt['hearing_date']!=''){echo date('F d,Y',strtotime($selcourt['hearing_date']));}else{echo " ";}?></td>
														<td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;"><?php echo $selcourt['team_member'];?></td>

														<td style="padding: 10px;">
															<a href="view_case.php?caseid=<?php echo base64_encode($selcourt['case_id']); ?>&&id=<?php echo $selcourt['case_id'];?>"class="fa fa-eye viewdetailscase" title="View"></a>&nbsp;&nbsp;&nbsp;<span class="fa fa-edit" title="Edit"></span><i class="fa fa-bar-chart-o" title="Analysis" style="margin: 0px 8px"></i><i class="fa fa-trash" title="Delete" onclick="deletecase(<?php echo $selcourt['case_id']; ?>)"></i>
														</td>
													</tr> 

													<?php $i++; } ?>
												</tbody>
												<tfoot>
													<tr>
														<td colspan="9">
															<input type="checkbox" class="selectall" name="" style="position: relative;top: 3px;">
															<b style="position: relative;top: 2px;">Select/Unselect All</b>
															<button class="btn" style="background-color: #2d4866;color: white!important;margin-left: 10px" onclick="recordHearing()">Record Hearing</button>
															<button class="btn" style="background-color: #2d4866;color: white!important;margin-left: 10px"  data-target="#assignteam" data-toggle="modal">Assign Team</button>
															<button class="btn btn-danger" style="margin-left: 10px">Delete</button>
														</td>
													</tr>
												</tfoot>
											</table>
											

										</div>
										
										
  </div>
  <?php include("footer.php");?>

  <script type="text/javascript" src="js/cases-report.js"></script>
</div>


<form name="myform" method="POST" action="downloadexcel.php">
	<div class="modal" id="export">
		<div class="modal-dialog">
			<div class="modal-content" style="">
				<div class="modal-header" style="">
					<h4>Export</h4>
					<button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-3 export-div">
							<input type="checkbox" name='checkboxvar[]' style="" value="case_id" />File<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="user_id" checked="" />Team Member(s)<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="Stage"/>Stage<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="court_name" checked=""/>Court<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="hearing_date" value="Hearing Date" checked=""/>Hearing Date<br>
							<!-- <input type="checkbox" name='checkboxvar' style="margin-right: 5px;" value=""/>Last Posted for<br> -->
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="court_hall"/>CourtHall#<br> -->
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Opponent Advocate(s)<br>-->
							<input type="checkbox" name='checkboxvar[]' style="" value="priority"/>Priority<br> 
							<!--  <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="Connected"/>Connected -->
						</div>
						<div class="col-sm-3">
							<input type="checkbox" name='checkboxvar[]' style="" value="case_no" checked=""/>Case No.<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="case_no_year"/>Session<br>
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Last Action Taken<br> -->
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="Team Member"/>Floor#<br> -->
							<input type="checkbox" name='checkboxvar[]' style="" value="date_of_filling"/>Date of Filling<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="case_description"/>Case Discription<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="section_category"/>Section/Category
						</div>
						<div class="col-sm-3">
							<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Client(s)/External Advocate(s)<br> -->
							<input type="checkbox" name='checkboxvar[]' style="" value="hearing_date"/>Last Hearing Date<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="classification"/>Classification<br>
							<input type="checkbox" name='checkboxvar[]' style="" value="case_title" checked=""/>Title<br>
                     <!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Before Hon'ble Judge(s)<br>
                     	<input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value=""/>Related Case(s) -->
                     </div>
                     <div class="col-sm-3">

                     	<!-- <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="Team Member"/>Opponnt(s)<br> -->
                     	<input type="checkbox" name='checkboxvar[]' style="" value="reffered_by"/>Referred By<br>
                     	<input type="checkbox" name='checkboxvar[]' style="" value="CNR" />CNR
                     </div>
                 </div><br><br>
                 <div class="row">
                 	<h4>Download as:</h4>
                 	<input type="radio" class="pdf" name="downloadas" value="pdf" style="">PDF
                 	<input type="radio" class="excel" name="downloadas" value="excel" style="" checked="">Excel
                 	<button class="download" style="">Download</button>
                 </div>
             </div>
         </div>
     </div>
 </div>
</form>
<div class="modal" id="filter">
	<div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header" style="">
				<h4>Filter</h4>
				<button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
			</div>
			<form name="MyName" id="filter_form" action="POST">
				<div class="modal-body filterlabel">
					<div class="row">
						<div class="col-sm-4">
							<label>Title</label>
							<input type="text" class="form-control form-group" placeholder="Enter Title" name="title">
							<label>Case</label>
							<input type="text" class="form-control form-group" placeholder="Enter Case" name="case_no">
							<label>Case Year</label>
							<select id="year" class="form-control form-group">
							</select>
							<label>Before Hon'ble Judge/s</label>
							<input type="text" class="form-control form-group" placeholder="Before Hon'ble Judge/s" name="before_hudge">
							<label>Section/Category</label>
							<input type="text" class="form-control form-group" placeholder="Enter Section/Category" name="category">
							<label>Posted For</label>
							<input type="text" class="form-control form-group" placeholder="Posted For" name="posted_for">
							<label>Opponents</label>
							<input type="text" class="form-control form-group" placeholder="put opponent dropdown" name="opponents">
						</div>
						<div class="col-sm-4">
							<label>Advocate</label>
							<input type="text" class="form-control form-group" placeholder="Enter Advocate Name" name="Advocate">
							<label>Hearing Date</label>
							<input type="text" class="form-control form-group" id="datepicker1" name="hearing_date" placeholder="Hearing Date">
							<label>Case Filing Date Form</label>
							<input type="text" class="form-control form-group" id="datepicker2" name="filling_date" placeholder="Case Filing Date Form">
							<label>Referred By</label>
							<input type="text" class="form-control form-group" placeholder="Enter Referred By" name="referred_by">
							<label>Priority</label>
							<select class="form-control">
								<option value="">--Select Priority--</option>
								<?php $priority = mysqli_query($connecton, "SELECT `priority_name` FROM `activity_priority` ORDER BY priority_name ASC");
								while($prioritysel = mysqli_fetch_array($priority)){?>
									<option value="<?php echo $prioritysel['priority_id'];?>"><?php echo $prioritysel['priority_name'];?></option>
								<?php } ?>
							</select>
							<!-- <input type="text" class="form-control form-group" placeholder="Put dropdown" name="priority"> -->
							<label>Action Taken</label>
							<input type="text" class="form-control form-group" placeholder="Action Taken">
							<label>CNR</label>
							<input type="text" class="form-control form-group" placeholder="Enter CNR">
						</div>
						<div class="col-sm-4">
							<label>Team Member</label>
							<input type="text" class="form-control form-group" placeholder="Enter Team Member">
							<label>Hearing Date To</label>
							<input type="text" class="form-control form-group" id="datepicker3" placeholder="Hearing Date To">
							<label>Case Filing Date To</label>
							<input type="text" class="form-control form-group" id="datepicker4" placeholder="Case Filing Date To">
							<label>File</label>
							<input type="text" class="form-control form-group" placeholder="Enter File">
							<label>Stage</label>
							<input type="text" class="form-control form-group" placeholder="Enter Stage">
							<label>Sessions</label>
							<select name="sessions" style="" class="form-control">
								<option value="">Please Select</option>
								<option value="1">Morning</option>
								<option value="2">Evening</option>
							</select>
							<label>Court</label>
							<input type="text" class="form-control form-group" placeholder="Put dropdown">
						</div>
					</div><br><br>
				</div>
			</form>
			<div class="modal-footer">
				<div class="row">
					<button class="btn btn-danger btn-sm reset-btn" type="button" onclick="resetFilter()" style="">Reset</button>
					<button class="submit-btn" style="">Submit</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="record">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Record Hearing</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-4">
						<label>Next Hearing Date</label><br>
						<input type="text" class="form-control" id="datepicker">
						<label>Action Taken</label><br>
						<input type="text" class="form-control">
					</div>
					<div class="col-sm-4">
						<label>Stage</label><br>
						<input type="text" class="form-control">
						<label>Session</label><br>
						<select name="sessions" style="" class="form-control">
							<option value="">Please Select</option>
							<option value="1">Morning</option>
							<option value="2">Evening</option>
						</select>
					</div>
					<div class="col-sm-4">
						<label>Posted For</label><br>
						<input type="text" class="form-control" >
					</div>
				</div>
			</div>
			<div class="modal-footer">

				<button class="btn btn-info btn-sm" type="button">Record</button>
				<button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>

			</div>
		</div>
	</div>
</div>
<div class="modal" id="assignteam">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: #2d4866;color: white;">
				<h4>Assign Team</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-8">
						<select name="country" class="form-control">
							<option >Select members to assign to this case</option>
						</select>
					</div>
					<div class="col-sm-4">
						<button class="assign" style="">Assign</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>