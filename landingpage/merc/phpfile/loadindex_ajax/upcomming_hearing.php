<?php
session_start();
include("../includes/dbconnect.php");
?>
<!-- <marquee class="bottommarquee" style="font-size:1em;align:left;" scrollamount="10" direction="up" onmouseover="this.stop();" onmouseout="this.start();"> -->
  <!-- <span style="color: #2d4866; font-weight: 900;"><?php
$hearingdates = date('Y-m-d');
echo date("F d, Y", strtotime($hearingdates));
?></span> -->
<ul class="hearing_ul">
  <?php
  include_once("../phpfile/sql_home.php");
  $mon                     = Date('Y-m-d');
  $sun                     = Date('Y-m-d', StrToTime("+1 week"));
  $history_of_case_hearing = mysqli_query($connection, "SELECT DISTINCT(hearing_date) FROM `reg_cases` WHERE `hearing_date` BETWEEN '" . $mon . "' AND '" . $sun . "' AND " . $conditionquery . " ORDER BY hearing_date ASC LIMIT 3");
  $cnt_hearing             = mysqli_num_rows($history_of_case_hearing);
  if ($cnt_hearing > 0) {
    while ($history = mysqli_fetch_array($history_of_case_hearing)) {
      ?>
      <span style="color: #2d4866; font-weight: 900;">
        <?php
        echo date('F d,Y', strtotime($history['hearing_date']));
        ?>
      </span>
      <?php
      $view_case = mysqli_query($connection, "SELECT rc.case_no, rc.case_no_year, rc.case_title, rc.case_type,rc.court_id,rc.high_court,rc.side,rc.hc_stamp_register,rc.case_id,rc.supreme_court,rc.diary_no,rc.diary_year  FROM reg_cases as rc  WHERE rc.hearing_date='" . $history['hearing_date'] . "'");
      while ($fetch_data = mysqli_fetch_array($view_case)) {
        if($fetch_data['supreme_court'] == 'Diary Number')
          $case= $fetch_data['diary_no'].'/ '.$fetch_data['diary_year'];
        else
          $case= $fetch_data['case_type'].' '.$fetch_data['case_no'].'/ '.$fetch_data['case_no_year']; 
        ?>
        <li class=" recent_class">
          <i class='fa fa-angle-double-right' aria-hidden='true'>
          </i>&nbsp;&nbsp;
          <a href="view-case.php?caseid=<?php echo base64_encode($fetch_data['case_id']);?>" style="color:#01a9ac;" href="_blank" class="index_marquee">
          <?php echo $case;?>  - <?php echo substr($fetch_data['case_title'], 0, 80);
          ?>
        </a>
        <br>
      </li>
      <?php
    }
    ?>
    <?php
  }
} else {
  ?>
  <p class="mb-4 text-muted">There are no upcoming Hearing Dates.
  </p>
  <?php
}
?>
</ul>

<?php
if ($cnt_hearing > 3) {
  ?>
  <p class="view-all-activities"><a href="viewall_hearingdates.php" class="marquee_view">View All
  </a></p>
  <?php
}
?>
