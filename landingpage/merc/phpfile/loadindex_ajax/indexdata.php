<?php session_start(); include("includes/dbconnect.php"); ?>
<style type="text/css">
  a{text-decoration: none!important;}
</style>
<div class="row">
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="cases-report">
        <div class="text-white bg-info">
          <div class="col-sm-12 upcases">
            <h1 class="subcount">
              <?php include("phpfile/sql_home.php"); // for super admin
              $list = mysqli_query($connection,"SELECT count(case_id) as case_count FROM `reg_cases` WHERE ".$conditionquery." ") or die (mysqli_error($connection));
              $allcount = mysqli_fetch_array($list);
              echo $allcount['case_count'];
              ?>
              <!-- 1420 -->
            </h1> 
            <h3 class="subhead">Uploaded Cases </h3>
           <!--  <span class="marathi_head">(अपलोड केलेली प्रकरणे)</span> -->
            <i class="fa fa-briefcase icon uploadbrief">
            </i>
            <?php 
              $running = mysqli_query($connection,"SELECT count(case_id) as case_count FROM `reg_cases` WHERE ".$conditionquery."  AND `case_status`='Running'") or die (mysqli_error($connection)); 
              $runningc = mysqli_fetch_array($running);
              $closed = mysqli_query($connection,"SELECT count(case_id) as case_count FROM `reg_cases` WHERE ".$conditionquery."  AND `case_status`='Disposed'") or die (mysqli_error($connection));
              $closedc = mysqli_fetch_array($closed);
              $direction = mysqli_query($connection,"SELECT count(case_id) as case_count FROM `reg_cases` WHERE ".$conditionquery."  AND `case_status`='Direction'") or die (mysqli_error($connection));
              $directionc = mysqli_fetch_array($direction);
              $contempt = mysqli_query($connection,"SELECT count(case_id) as case_count FROM `reg_cases` WHERE ".$conditionquery."  AND `case_type`='Contempt Petition'") or die (mysqli_error($connection));
               $contemptc = mysqli_fetch_array($contempt);
              ?>
              Running: 
            <?php echo $runningc['case_count'];?> | Closed: 
            <?php  echo $closedc['case_count']; ?> | 
            <span class="form-group" >Transferred/NOC: 0 | Direction Matters: 
              <?php  echo $directionc['case_count'];?> | Order Reserved: 0   | Contempt Petition: 
              <?php  echo $contemptc['case_count'];?> 
            </span>
          </div>
          <span  class="vm commonviewmore viewmoreupload">View More
            <i class='fa fa-arrow-circle-right'>
            </i>
          </span>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="add-advocates">
        <div class="text-white bg-primary">
          <div class="col-sm-12 zpadvocate">
            <h1 class="subcount">
              <?php 
            $advocate = mysqli_query($connection, "SELECT DiSTINCT `full_name` FROM `advocate`  WHERE `division`='".$_SESSION['cityName']."' AND `user_id`='".$_SESSION['user_id']."'") or die (mysqli_error($connection));
            ?>
              <?php   echo $contadvocate = mysqli_num_rows($advocate);?>
            </h1> 
            <h3 class="subhead">  Team Member  </h3>
            <!-- <span class="marathi_head">(ZP पॅनलचे वकील ) </span> -->
            <i class="fa fa-user icon" style="color: #c57c3f;"></i>
          </div> 
          <p>
            <span class=" vm commonviewmore viewmoreadvocate col-sm-12">View More 
              <i class="fa fa-arrow-circle-right" aria-hidden="true">
              </i>
            </span>
          </p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="manage-document" >
        <div class="text-white bg-warning">
          <div class="col-sm-12 documentpanel">
            <h1 class="subcount">
              <?php
$documentss = mysqli_query($connection, "SELECT ad.image,ad.case_id,ad.user_id,ad.type,ad.date_time,ad.doc_id,rc.div_comm_level FROM add_document ad LEFT JOIN reg_cases rc ON ad.case_id=rc.case_id WHERE ".$documentquery." AND ad.direction!='matter' ORDER BY ad.doc_id DESC") or die (mysqli_error($connection));;
$add_documents = mysqli_num_rows($documentss);
echo $add_documents;?>
            </h1> 
            <h3 class="subhead"> Documents </h3>
           <!--  <span class="marathi_head">(कागदपत्रे)</span> -->
            <i class="fa fa-file icon" style="color: #B33C58;"></i>
          </div>
          <span class=" vm viewmoredoc commonviewmore col-sm-12">View More
            <i class='fa fa-arrow-circle-right'>
            </i>
          </span>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="team">
        <div class="text-white bg-success">
          <div class="col-sm-12 teampanel">
            <h1 class="subcount">
              <?php 
              $Member =mysqli_query($connection,"SELECT count(reg_id) as team_count FROM `law_registration` WHERE (`parent_id`='".$_SESSION['user_id']."' OR `division`='".$_SESSION['cityName']."')") or die (mysqli_error($connection)); 
              $countMember = mysqli_fetch_array($Member);
              echo $countMember['team_count'];?>
            </h1> 
            <h3 class="subhead">Assigned Team Members</h3> 
            <!-- <span class="marathi_head">(असाइन केलेले कार्यसंघ सभासद)</span> -->
            <i class="fa fa-users icon" style="color: #836a61;"></i>
          </div>
          <p>
            <span class=" vm commonviewmore col-sm-12" style="background-color: #836a61;">View More 
              <i class='fa fa-arrow-circle-right'>
              </i>
            </span>
          </p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="create-todo?name=All">
        <div class="text-white bg-info1">
          <div class="col-sm-12 remindpanel">
            <h1 class="subcount">
              <?php  $remindes = mysqli_query($connection, "SELECT count(todo_id) as todo_count FROM `todo_list` WHERE ".$samequery." ") or die (mysqli_error($connection));
                $remindess = mysqli_fetch_array($remindes); echo $remindess['todo_count'];?>
            </h1> 
            <h3 class="subhead">To Do</h3> 
           <!--  <span class="marathi_head">(स्मरणपत्रे) 
            </span>  -->            
            <i class="fa fa-clipboard icon" style="color: #749744;">
            </i>
            Pending:  | Upcoming: 
            <?php  $Upcoming = mysqli_query($connection, "SELECT count(todo_id) as todo_count FROM `todo_list` WHERE `to_date`>='".date("y-m-d H:i:s")."' AND ".$samequery."") or die (mysqli_error($connection));
              $Upcoming1 = mysqli_fetch_array($Upcoming); echo $Upcoming1['todo_count'];?> | Completed: 
            <?php  $Completed = mysqli_query($connection, "SELECT count(todo_id) as todo_count FROM `todo_list` WHERE `to_date`<='".date("y-m-d H:i:s")."' AND ".$samequery."") or die (mysqli_error($connection));;
             $Completed1 = mysqli_fetch_array($Completed); echo $Completed1['todo_count'];?>
          </div>
          <p>
            <span class=" vm commonviewmore col-sm-12" style="background-color: #749744;">View More
              <i class='fa fa-arrow-circle-right'>
              </i>
            </span>
          </p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="my-law-diary">
        <div class="text-white bg-danger">
          <div class="col-sm-12 diarypanel">
            <h1 class="subcount">
              <?php $diary = mysqli_query($connection, "SELECT count(case_id) as daily_count FROM `reg_cases` WHERE ".$dailycases_query."") or die (mysqli_error($connection));
               $diarycount = mysqli_fetch_array($diary);  echo $diarycount['daily_count'];  ?>
            </h1> 
            <h3 class="subhead">Daily Cases
            </h3>
           <!--  <span class="marathi_head">(कोर्टासमोरील आजच्या याचिका) 
            </span>  -->                
            <i class="fa fa-book icon" style="color: #23bab5;">
            </i>
            Supreme Court: 
          <?php $supremcourt = mysqli_query($connection, "SELECT count(case_id) as daily_count FROM `reg_cases` WHERE `court_name`='Supreme Court of India' AND ".$dailycases_query."") or die (mysqli_error($connection)); 
          $SupremeCourtIndia = mysqli_fetch_array($supremcourt);
          echo $SupremeCourtIndia['daily_count'];?> 
          | High Court:  
          <?php $High = mysqli_query($connection, "SELECT count(case_id) as daily_count FROM `reg_cases` WHERE `court_name`='High Court' AND ".$dailycases_query."") or die (mysqli_error($connection)); 
          $High1 = mysqli_fetch_array($High);
          echo $High1['daily_count'];?> 
          | District Court:  
          <?php $District = mysqli_query($connection, "SELECT count(case_id) as daily_count FROM `reg_cases` WHERE `court_name`='District Court' AND ".$dailycases_query."") or die (mysqli_error($connection)); 
          $District1 = mysqli_fetch_array($District);
          echo $District1['daily_count'];?> 
          | Other:  
          <?php $other = mysqli_query($connection, "SELECT count(case_id) as daily_count FROM `reg_cases` WHERE `court_name`='Other' AND ".$dailycases_query."") or die (mysqli_error($connection)); 
          $other1 = mysqli_fetch_array($other);
          echo $other1['daily_count'];?>

          </div>
          <p>
            <span class="  commonviewmore viewmorediary vm col-sm-12">View More 
              <i class='fa fa-arrow-circle-right'>
              </i>
            </span>
          </p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="tree-dashboard">
        <div class="text-white bg-success">
          <div class="col-sm-12 teampanel">
            <h1 class="subcount">
            </h1>
            <h3 class="subhead col-sm-9">Consolidated Topic wise Legal Audit Report
            </h3> 
            <!-- <span class="marathi_head">(एकत्रित विषयनिहाय कायदेशीर अंकेक्षण अहवाल)
            </span> -->
            <i class="fa fa-sitemap icon" style="color: #836a61;">
            </i>
          </div>
          <p>
            <span class=" vm commonviewmore col-sm-12" style="background-color: #836a61;">View More 
              <i class='fa fa-arrow-circle-right'>
              </i>
            </span>
          </p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
    <div class="card-dashboard recent-revenue-card">
      <a href="quasi_judicial_matters">
        <div class="text-white bg-success">
          <div class="col-sm-12 teampanel">
            <h1 class="subcount">
     <?php         
$judical = mysqli_query($connection, "SELECT count(case_id) as quasi_count FROM `reg_cases` WHERE `complaint_authority`!='' AND ".$conditionquery."") or die (mysqli_error($connection));
  $comstatus = mysqli_fetch_array($judical);
  echo $comstatus['quasi_count']; ?>
            </h1>
            <h3 class="subhead col-sm-9">Quasi Judicial Matter
            </h3> 
            <span class="marathi_head">(अर्धन्यायायिक प्रकरण 
              )
            </span>
            <i class="fa fa-sitemap icon" style="color: #836a61;">
            </i>
          </div>
          <p>
            <span class=" vm commonviewmore col-sm-12" style="background-color: #836a61;">View More 
              <i class='fa fa-arrow-circle-right'>
              </i>
            </span>
          </p>
        </div>
      </a>
    </div>
  </div>
</div>
