<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="plugins/tinymce/js/tinymce/tinymce.min.js"></script>

<script type="text/javascript" src="scripts/popper.min.js"></script>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="scripts/bootstrap-datetimepicker.min.js"></script> -->
<script type="text/javascript" src="scripts/bootstrap-datepicker.min.js"></script>

<script type="text/javascript" src="scripts/jquery.slimscroll.js"></script>

<script type="text/javascript" src="scripts/modernizr.js"></script>

<script src="scripts/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>

<!-- <script type="text/javascript" src="scripts/SmoothScroll.js"></script> -->
<script src="scripts/pcoded.min.js" type="text/javascript"></script>

<script src="scripts/vertical-layout.min.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/custom-dashboard.js"></script>
<script type="text/javascript" src="scripts/script.min.js"></script>
<script type="text/javascript" src="scripts/rocket-loader.min.js"></script>
<script type="text/javascript" src="scripts/select2.min.js"></script>

<script type="text/javascript" src="scripts/toastr.min.js"></script>
<script type="text/javascript" src="scripts/connectfile.js"></script>
<script type="text/javascript" src="scripts/common.js"></script>
<script type="text/javascript" src="scripts/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="scripts/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="scripts/header.js"></script>
<script type="text/javascript" src="scripts/jsapi.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
   var account_type = $("#account-type-input").val();
   // if(account_type == 'Individual'){
    $("#divisional_comm_level_div").hide();
    $("#department_div").hide();
    $("#ministry_div").hide();
    $("#petitioner-suffix").text('Clients');
    //$("#petitioner-suffix-marathi").text(' पक्षकार ');
    // $("#organization_submit").hide();
    // $("#individual_submit").show();
    $("#petitioner_adv_div").hide();
    $("#petitioner_div").show();
    $("#file-no-div").hide();
    $("#subject-div").hide();
    $("#file-no-th").hide();
    $("#name-of-dept-th").hide();
    $(".nastikramank-td").hide();
    $(".name-of-dept-td").hide();
  /*}
  else{
   $("#subject-div").show();
   $("#divisional_comm_level_div").show();
   $("#department_div").show();
   $("#ministry_div").show();
   $("#petitioner-suffix").text('Advocates');
   $("#petitioner_adv_div").show();
   $("#petitioner_div").hide();
   $("#file-no-div").show();
   $("#file-no-th").show();
   $(".name-of-dept-th").show();
   $(".name-of-dept-td").show();
 }*/
 if(localStorage.getItem('smallFont') !=''){
  smallFont();
 }
 if(localStorage.getItem('largeFont') !=''){
  largeFont();
 }
 if(localStorage.getItem('defaultFont') !=''){
  defaultFont();
 }
});

  var resize = new Array('body');
  resize = resize.join(',');
  
  //decrease font size when "-" is clicked

  function smallFont(){
    var originalFontSize = $(resize).css('font-size');
    var originalFontNumber = parseFloat(originalFontSize, 10);
    var newFontSize = originalFontNumber * 0.8;
    $(resize).css('font-size', newFontSize);
    localStorage.setItem('largeFont','');
    localStorage.setItem('smallFont',newFontSize);
    localStorage.setItem('defaultFont','');
    return false;
  }
 //increases font size when "+" is clicked
 function largeFont(){
  var originalFontSize = $(resize).css('font-size');
  var originalFontNumber = parseFloat(originalFontSize, 10);
  var newFontSize = originalFontNumber * 1.2;
  $(resize).css('font-size', newFontSize);
  localStorage.setItem('largeFont',newFontSize);
  localStorage.setItem('smallFont','');
  localStorage.setItem('defaultFont','');
  return false;
}
  //resets the font size when "reset" is clicked
  function defaultFont(){
     //var resetFont = $(resize).css('font-size');
    $(resize).css('font-size', 14);
    localStorage.setItem('largeFont','');
    localStorage.setItem('smallFont','');
    localStorage.setItem('defaultFont',14);
  }

</script>