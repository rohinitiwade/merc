<?php include("header.php"); ?>
<?php //include("chat-sidebar.php"); ?>
<?php //include("chat-inner.php"); ?> 
<?php include("phpfile/sql_home.php");?>
<style type="text/css">
  .files {
    position: relative;
    width: 100%;
    float: left;
  }
  .files input {
    outline: 2px dashed #92b0b3;
    outline-offset: -10px;
    -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
    transition: outline-offset .15s ease-in-out, background-color .15s linear;
    padding: 68px 0px 85px 35%;
    text-align: center !important;
    margin: 0;
    width: 100% !important;
  }
  .fetchdataloader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 60px;
    /*float: right;*/
    height: 60px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
  }
  .upload_btn{
  /*width: 14%;
  margin: 0 auto;*/
  text-align: center;
  padding: 3% 11px;
}
.border-right{
  border-right: 1px solid #c7c5c5;
}
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php"); ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Add Documents   </h5>
              </div>
              <div class="page-body">

                <div class="card form-group">
                  <div class="card-block">
                    <div class="media">
                      <div class="media-body col-sm-12">
                       <div class="tab-pane" id="documenttab" role="tabpanel" aria-labelledby="documenttab">
                        <form class=" row" method="post" action="#" id="#">
                         <div class="col-sm-5 form-group  ">
                          <label class="col-sm-6 col-form-label">Upload Your Case Papers &nbsp;&nbsp;<span style="color: red;">*</span></label>
                          <?php 
                          $sel = mysqli_query($connection,"SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
                          $sels = mysqli_fetch_array($sel);
                          ?>
                          <!-- <input type="hidden" name="case_id"  id="case_ids" value="<?php echo base64_decode($_GET['caseid']);?>"> -->

                          <input type="hidden" name="case_no"  id="case_no" value="<?php echo $sels['case_no'];?>">
                          <input type="file" class="form-control form-group" multiple="" id="upload_doc"  accept="image/x-png,image/jpeg,application/pdf,application/msword" required/>
                        </div>
                        <br><br>
                        <div class=" col-sm-3">

                          <div class="  form-group m-1 ">
                           <label for="exampleInputUsername1" class="col-form-label"> Document type &nbsp;&nbsp;<span style="color: red;">*</span></label>
                           <div class="">
                             <select class="form-control" id="doc_type" name="doc_type" required="">
                              <option value="">Please select document type</option>
                              <?php $document = mysqli_query($connection, "SELECT doc_id,documents_type FROM `documents_type` WHERE `organisation_id`='".$_SESSION['organisation_id']."' ORDER BY documents_type ASC");
                              while($seldocmnet = mysqli_fetch_array($document)){?>
                               <option value="<?php echo addslashes($seldocmnet['doc_id']);?>"><?php echo addslashes($seldocmnet['documents_type']);?></option>
                             <?php } ?>
                           </select>
                         </div>
                       </div>
                    </div>

                    <div class="col-sm-4 upload_btn row">
                      <br><br>
                      <div class="col-sm-2 add_casesubmitdiv" style="padding-right: 0; ">
                      
                         <button class="btn btn-sm btn-primary newsubmit add_casesubmit"  type="button" id="upload_documents" value="Upload" name="Upload" onclick="getUploadedDoc('muldoc')">Upload</button> </div>
                      
                       <div class="col-sm-6" style="text-align: left;">
                         <a class="btn btn-danger btn-sm" href="multipleDocument.php" id="all_doc" > Cancel </a>
                       </div>
                                                        <!--  <a class="btn btn-danger btn-sm" href="manage-document.php" id="all_doc" style="display: none;"> Cancel </a>
                                                         <a class="btn btn-danger btn-sm" href="cases-report.php" id="casewise_doc" style="display: none;"> Cancel </a> -->


                                                       </div>
                                                   <!-- <hr>
                                                   <div class="row col-sm-12">
                                                      <div class="col-sm-3 form-group">
                                                         <select class="form-control" id="search-doc-filter">
                                                            <option value="">NA</option>
                                                            <option value="caseno">Case No</option>
                                                            <option value="petitioner">Petitioner</option>
                                                            <option value="respondent">Respondent</option>

                                                         </select>
                                                      </div>
                                                      <div class="col-sm-3 form-group" id="search-document-div" style="display: none;">
                                                        <input type="text" class="form-control" id="search-in-document">
                                                     </div>
                                                     <div class="col-sm-3 form-group">
                                                      <button type="button" class="btn btn-warning btn-sm">Search in Document</button>
                                                   </div>
                                                 </div> -->
                                                 <hr style="width: 100%;">
                                                 <div id="documentList" class="table-responsive  "></div>
                                               </form>
                                             </div>
                                           </div>
                                         </div>
                                       </div>
                                     </div>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                       <?php include 'footer.php'; ?>
                       <!-- <script type="text/javascript" src="scripts/add_documents.js"></script> -->
                       <script src="scripts/multipleDocument.js">

</script>







