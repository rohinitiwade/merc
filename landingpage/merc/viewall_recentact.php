<?php  include("phpfile/sql_home.php");?> 
      <!-- partial -->
      <?php include("header.php");  session_start(); ?>
      <link rel="stylesheet" href="styles/recent_case.css">
      <?php include("menu.php");?>

    <div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
  <div class="pcoded-content">
    <div class="pcoded-inner-content">
      <div class="main-body">
        <div class="page-wrapper">
          <div class="page-body">
              <h4 class="card-header" style="padding-top: 0px">Recent Case Activities</h4>
               
              <div class=" hearing_card">
                <!-- <label><b><?php   echo date("F d, Y",strtotime($history['hearing_date']));?></b></label> -->
                <ul>
                 <?php 
                  if (isset($_GET['page_no']) && $_GET['page_no']!="") {
                  $page_no = $_GET['page_no'];
                } else {
                  $page_no = 1;
                }
                $total_records_per_page = 50;
                $offset = ($page_no-1) * $total_records_per_page;
                $previous_page = $page_no - 1;
                $next_page = $page_no + 1;
                $adjacents = "2"; 
                            // if($_SESSION['user_id']!='77'){
                $result_count = mysqli_query($connection,"SELECT COUNT(ea.edit_log_id) As total_records FROM edit_activity_log ea LEFT JOIN reg_cases rc ON rc.case_id=ea.case_id WHERE  ".$recentactivity."");
                          // }else{
                          //   $result_count = mysqli_query($connection,"SELECT COUNT(case_id) As total_records FROM `reg_cases` WHERE `user_id`='77'");
                          // }
                $total_records = mysqli_fetch_array($result_count);
                $total_records = $total_records['total_records'];
                $total_no_of_pages = ceil($total_records / $total_records_per_page);
                $second_last = $total_no_of_pages - 1;


          $acticityCase = mysqli_query($connection,"SELECT rc.supreme_court,rc.diary_no,rc.diary_year,rc.case_type,rc.case_no,rc.case_no_year,rc.judicial,rc.case_id,rc.client_name,rc.case_title,ea.date_time,ea.case_id,ea.edit_by FROM edit_activity_log ea LEFT JOIN reg_cases rc ON rc.case_id=ea.case_id WHERE  ".$recentactivity." ORDER BY ea.edit_log_id DESC LIMIT $offset, $total_records_per_page");
           while($acticityCases = mysqli_fetch_array($acticityCase)){
            $reg = mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$acticityCases['edit_by']."'");
            $regFetch = mysqli_fetch_array($reg);
            $full_name = $regFetch['name'].' '.$regFetch['last_name'];
         
            if($acticityCases['supreme_court'] == 'Diary Number')
                                      $case= $acticityCases['diary_no'].'/ '.$acticityCases['diary_year'];
                                    else
                                      $case= $acticityCases['case_type'].' '.$acticityCases['case_no'].'/ '.$acticityCases['case_no_year']; ?>
                 <li class="recent_class">
              <i class='fa fa-angle-double-right' aria-hidden='true'>
              </i>&nbsp;&nbsp;
            <?php echo date('F d, Y H:i',strtotime($acticityCases['date_time']))?> By <span><?php echo ucfirst($full_name);?></span><br>
               in <span>
               <?php if($acticityCases['judicial'] == 'Non-Judicial'){?>
              <a href="view-nonjudicialcase.php?caseid=<?php echo base64_encode($acticityCases['case_id'])?>" style="color:#01a9ac;" target="_blank" class="index_marquee">
             <?php echo substr($acticityCases['client_name'],0,80);?>
          </a>
      <?php }else{?>  <a href="view-case.php?caseid=<?php echo base64_encode($acticityCases['case_id'])?>" style="color:#01a9ac;" target="_blank" class="index_marquee"><?php echo $case;?> 
                <?php if($acticityCases['case_title'] !=''){?>- 
                <?php echo substr($acticityCases['case_title'],0,80);}?>
              <?php } ?>
           </a>
       </span>
            <br>
          </li>
                   <?php } ?> 
                </ul>
                 <ul class="pagination">
                                 <?php if ($page_no > 1){
                                  echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>First Page</a></li>";} ?>
                                  <li <?php if ($page_no <= 1){
                                    echo "class='disabled'";} ?>>
                                    <a <?php if ($page_no > 1){
                                      echo "href='?page_no=$previous_page&&status=".$_REQUEST['status']."'";} ?>>Previous</a>
                                    </li>
                                    <?php
                                    if ($total_no_of_pages <= 10){
                                      for ($counter = 1;$counter <= $total_no_of_pages;$counter++)
                                      {
                                        if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>"; }
                                        else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
                                      }
                                    }elseif ($total_no_of_pages > 10){
                                      if ($page_no <= 4){
                                        for ($counter = 1;$counter < 8;$counter++){
                                          if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>";
                                        }else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
                                      }
                                      echo "<li><a>...</a></li>";
                                      echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
                                      echo "<li><a href='?page_no=$total_no_of_pages&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
                                    }elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4){
                                      echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
                                      echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
                                      echo "<li><a>...</a></li>";
                                      for ($counter = $page_no - $adjacents;$counter <= $page_no + $adjacents;$counter++)
                                      {
                                        if ($counter == $page_no){
                                          echo "<li class='active'><a>$counter</a></li>";
                                        }else{
                                          echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";
                                        }
                                      }
                                      echo "<li><a>...</a></li>";
                                      echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
                                      echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
                                    }else{
                                      echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
                                      echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
                                      echo "<li><a>...</a></li>";
                                      for ($counter = $total_no_of_pages - 6;$counter <= $total_no_of_pages;$counter++)
                                      {
                                        if ($counter == $page_no){
                                          echo "<li class='active'><a>$counter</a></li>";
                                        }else{
                                          echo "<li><a href='?page_no=$counter&&status=".$_REQUEST['status']."'>$counter</a></li>";
                                        }
                                      }
                                    }
                                  }
                                  ?>
                                  <li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
                                    <a <?php if($page_no < $total_no_of_pages) { echo "href='?page_no=$next_page&&status=".$_REQUEST['status']."'"; } ?>>Next</a>
                                  </li>
                                  <?php if($page_no < $total_no_of_pages){
                                    echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>Last &rsaquo;&rsaquo;</a></li>";
                                  } ?>
                                </ul>
              </div>
            
            </div>
          </div>
         </div>

         
        </div>
      </div>
    </div>
  </div>
  
         <?php include("footer.php");?>