<?php session_start();include("header.php");?>
<!-- <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css"> -->
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="css/vertical-layout-light/team.css">
<link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
<script src='select2/dist/js/select2.min.js' type='text/javascript'></script>
<link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
<?php include("menu.php");?>
<div class="main-panel">
  <div class="content-wrapper">
    <div class="card form-group" style="height: 500px;">
      <div class="card-body">
        <h4 class="card-header team_header"><b style="color: green;">Total Pages Accessed
          (<?php  $select = mysqli_query($connection, "SELECT * FROM `frequency_report` WHERE `user_id`='".base64_decode($_GET['id'])."' AND `case_id`!=0");
                        echo  $count = mysqli_num_rows($select);?>)</b>
              <div class="row" style="margin-top: 25px;">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="manage_team_dataTable" class="table">
                      <thead>
                        <tr>
                          <th class="team_tableheader">Sr.No.</th>
                          <th class="team_tableheader">Page Accessed</th>
                          <th class="team_tableheader">Date</th>
                          <th class="team_tableheader">Division</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $team = mysqli_query($connection, "SELECT DISTINCT `dates` FROM `frequency_report` WHERE `user_id`='".base64_decode($_GET['id'])."' AND `case_id`!=0 ");
                        $i=1; 
                        while($selteam = mysqli_fetch_array($team)){
                         $teams =  mysqli_query($connection, "SELECT *  FROM `frequency_report` WHERE `dates`='".$selteam['dates']."' AND `user_id`='".base64_decode($_GET['id'])."' AND `case_id`!=0");
                          $selteam = mysqli_num_rows($teams);
                          $selteamd = mysqli_fetch_array($teams)
                          ?>
                          <tr>
                            <td class="team_tablebody"><?php echo $i;?></td>
                            <td class="team_tablebody"><a data-toggle="modal" data-target="#frequency_report" onclick="viewcases(<?php echo $selteamd['user_id'];?>)">View Case (<?php echo $selteam;?>)</a></td>
                            <td class="team_tablebody"><?php echo $selteamd['dates'];?></td>
                            <td class="team_tablebody"><?php echo $selteamd['division'];?></td>
                              </tr>
                              <?php $i++; } ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal" id="export">
                <div class="modal-dialog">
                  <div class="modal-content" style="margin-left: -185px;width: 180%;">
                    <div class="modal-header" style="background-color: #2d4866;color: white;">
                      <h4>Export</h4>
                      <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-sm-3">
                          <input type="checkbox" style="margin-right: 5px;" />First Name<br>
                          <input type="checkbox" style="margin-right: 5px;" />Address<br>
                          <input type="checkbox" style="margin-right: 5px;" />Status
                        </div>
                        <div class="col-sm-3">
                          <input type="checkbox" style="margin-right: 5px;" />Last Name<br>
                          <input type="checkbox" style="margin-right: 5px;" />Created Date<br>
                          <input type="checkbox" style="margin-right: 5px;" />Designation
                        </div>
                        <div class="col-sm-3">
                          <input type="checkbox" style="margin-right: 5px;" />Email Address<br>
                          <input type="checkbox" style="margin-right: 5px;" />GSTIN
                        </div>
                        <div class="col-sm-3">
                          <input type="checkbox" style="margin-right: 5px;" />Mobike Number<br>
                          <input type="checkbox" style="margin-right: 5px;" />Brief About Yourself
                        </div>
                      </div><br><br>
                      <div class="row">
                        <h4>Download as:</h4>
                        <input type="radio" name="downloadas" style="margin-top: 4px;margin-left: 20px;">PDF
                        <input type="radio" name="downloadas" style="margin-top: 4px;margin-left: 10px;">Excel
                        <button style="margin-top: -9px;border: 0px;margin-left: 35px;padding: 5px;background-color: #2d4866;color: white;">Download</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal" id="logindetails">
                <div class="modal-dialog">
                  <div class="modal-content" style="margin-left: -225px;width: 195%;">
                    <div class="modal-header">
                      <h4>Login History</h4>
                      <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                    </div>
                    <div class="modal-body">
                      <form>
                        <div class="row">
                          <div class="col-sm-4">
                            <label for="email">User Name</label><br>
                            <select name="username" class="form-control" id="state">
                              <option value="">----Please Select-----</option>
                              <?php $teammember = mysqli_query($connection, "SELECT * FROM `team` ORDER BY  full_name ASC"); 
                              while($seectteammem = mysqli_fetch_array($teammember)){?>
                                <option value="<?php echo $seectteammem['id'];?>"><?php echo $seectteammem['full_name'];?>  <?php echo $seectteammem['last_name'];?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-sm-4">
                            <label for="email">From Date</label><br>
                            <input type="date" class="form-control" id="pwd" placeholder="Enter From Date" name="edate">
                          </div>
                          <div class="col-sm-4">
                            <label for="pwd">To Date</label>
                            <input type="date" class="form-control" id="pwd" placeholder="Enter To Date" name="edate">
                          </div>
                        </div><br><br>
                        <div class="row">
                          <input name="Reset" type="reset" onclick="reset()" style="margin-top: -9px;border: 0px;margin-left: 25px;padding: 5px;background-color: #2d4866;color: white;">
                          <button style="margin-top: -9px;border: 0px;margin-left: 5px;padding: 5px;background-color: #2d4866;color: white;">Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal" id="editteam">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                      <h4 class="modal-title">Edit Team Member Details</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                      <div id="edit_team"></div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                  </div>
                </div>
              </div>



              <div class="modal" id="viewhistory">
                <div class="modal-dialog">
                  <div id="loghisto"></div>
                </div>
              </div>

              <script type="text/javascript">
                $("#checkAll").click(function () {
                  $(".check").prop('checked', $(this).prop('checked'));
                });

              </script>


              <div class="modal" id="comment-modal">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div id="teamcomment"></div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                  </div>
                </div>
              </div>


              <div class="modal" id="frequency_report">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div id="casedetails"></div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                  </div>
                </div>
              </div>


              <div class="modal" id="assignmember">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                      <h5 class="modal-title">Assign Team Member</h5>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <?php if(isset($_POST['add_member'])){
                      extract($_POST);
                      if($Teammember!=""){
                        foreach ($Teammember as  $value) {
                          $update = mysqli_query($connection, "UPDATE `law_registration` SET `parent_id`='".$member."' WHERE `reg_id`='".$value."'");

                        }
                      }
                    }?>

    </div>
  </div>
</div>


<script>
 $('#team-member').select2({
  placeholder: 'Select...',
  closeOnSelect: false,
  multiple: true
});
 $('#team-member1').select2({
  placeholder: 'Select...',
  closeOnSelect: false,
  multiple: true
});
 function reset() {
  var dropDown = document.getElementById("state");
  dropDown.selectedIndex = 0;
}

</script>
 

<div class="modal" id="viewhistory">
  <div class="modal-dialog">
   <div id="casedetails"></div>
 </div>
</div>


  <style type="text/css">
    .select2-container{
      width: inherit!important;
    }
    body{
      position: relative;
    }
    i{
      cursor: pointer;
    }
  </style>
<script type="text/javascript">
 function viewcases (id) {

  var id = +id;
    alert(id);
  $.ajax({
    type:'POST',
    url:'ajaxdata/viewcases.php',
    data:'ids='+id,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(html){
      $('#casedetails').html(html);
    },
    complete:function(){
     $(".flip-square-loader").hide();
   },
   error:function(e){
     $(".flip-square-loader").hide();
     toastr.error("","Unable to Load","Sorry for inconvinence",{timeout:5000});
   }
  }); 
}
</script>
 <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
 <script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
 <script type="text/javascript" src="js/jszip.min.js"></script>
 <script type="text/javascript" src="js/pdfmake.min.js"></script>
 <script type="text/javascript" src="js/vfs_fonts.js"></script>
 <script type="text/javascript" src="js/buttons.html5.min.js"></script> -->

