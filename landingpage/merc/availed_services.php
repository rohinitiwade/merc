<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php");
include("phpfile/sql_home.php");?> 
<link rel="stylesheet" href="styles/case-report.css">
<style type="text/css">
th, td {white-space: normal;}
.court-report, .title-report, .team-report, .hearing-report, .case-details-report, .action-report {
  width: 10%!important;}
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
        <div class="pcoded-content">
          <div class="pcoded-inner-content">
            <div class="main-body">
              <div class="page-wrapper">
                <div class="page-header m-b-10">
                  <h5 class="card-title">
                    <b>
                      Availed Services
                    </b>
                  </h5>
                </div>
                  <div class="page-body">
                    <div class="card form-group">
                      <div class="card-block">
                      <div class="table-responsive" id="availed-services-table">

                        <table class="table table-bordered">
                          <thead class="bg-primary">
                            <tr>
                              <th class="court-report">File No</th>
                              <th class="court-report">Name of Court</th>
                              <th class="case-report">Case No / Year</th>                                  
                              <th class="title-report">Name of Dept</th>
                              <th class="title-report">Title</th>
                              <th class="hearing-report">Hearing Date</th>
                              <th class="team-report">Team Member(s)</th>
                              <th class="services-report">Services</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            if (isset($_GET['page_no']) && $_GET['page_no']!="") {
                              $page_no = $_GET['page_no'];
                            } else {
                              $page_no = 1;
                            }
                            $total_records_per_page = 50;
                            $offset = ($page_no-1) * $total_records_per_page;
                            $previous_page = $page_no - 1;
                            $next_page = $page_no + 1;
                            $adjacents = "2"; 
                            $result_count = mysqli_query($connection,"SELECT COUNT(case_id) As total_records FROM `reg_cases` WHERE ".$conditionquery."");
                            $total_records = mysqli_fetch_array($result_count);
                            $total_records = $total_records['total_records'];
                            $total_no_of_pages = ceil($total_records / $total_records_per_page);
                            $second_last = $total_no_of_pages - 1;
                            $court =mysqli_query($connection, "SELECT `priority`,`case_id`,`court_name`,`supreme_court`,`high_court`,`bench`,`side`,`hc_stamp_register`,`state`,`district`,`court_name`,`complaint_authority`,`case_type`,`case_no`,`case_no_year`,`name_of_matter`,`case_title`,`case_description`,`hearing_date`,`user_id`,`nastikramank` FROM `reg_cases` WHERE ".$conditionquery."  ORDER BY `case_id` DESC LIMIT  $offset, $total_records_per_page");
                            $i=1+$offset;
                            while($selcourt = mysqli_fetch_array($court)){
                              $casecode = mysqli_query($connection, "SELECT `priority_color` FROM `activity_priority` WHERE `priority_name`='".$selcourt['priority']."'");
                              $setcolorecount = mysqli_num_rows($casecode);
                              $selcolor = mysqli_fetch_array($casecode);
                              $empQuery_join = mysqli_query($connection,"select `assign_to` from todo_team tt WHERE tt.case_id = '".$selcourt['case_id']."' AND tt.flag='add' AND tt.division='".$_SESSION['cityName']."'");
                              $team_member = array();
                              while($empquery = mysqli_fetch_array($empQuery_join)){
                                $law_reg = mysqli_query($connection,"select `name`,`last_name` from law_registration  WHERE reg_id = '".$empquery['assign_to']."' ");
                                $law_fetch = mysqli_fetch_array($law_reg);
                                $team_member[] = $law_fetch['name'].' '.$law_fetch['last_name'];    
                              }   
                              $team_member_list = implode(', ', $team_member);
                              ?>
                              <input type="hidden" name="" id="caseid">
                              <tr style="cursor:pointer;background-color: <?php if($setcolorecount==1){ echo  $selcolor['priority_color']; }  ?>">
                              <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
                                <?php echo $selcourt['nastikramank'];?>
                              </td>
                              <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
                                <?php  
                                if($selcourt['court_id']==1){
                                  echo $selcourt['court_name'].' -'. $selcourt['supreme_court'];}
                                  elseif($selcourt['court_id']==2){
                                    echo $selcourt['court_name'].' -'. $selcourt['high_court'].'- '. $selcourt['bench'].'- '. $selcourt['side'].'- '. $selcourt['hc_stamp_register'];}
                                    elseif($selcourt['court_id']==3){
                                      echo $selcourt['court_name'].' -'. $selcourt['state'].'- '. $selcourt['district'].'- '. $selcourt['court_establishment'];}else{
                                        echo $selcourt['court_name'].' '. $selcourt['complaint_authority'];
                                      }
                                      ?> 
                                    </td>                                 
                                    <td class="rowclickable"  data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
                                      <?php if(is_numeric ($selcourt['court_name'])){ 
                                       echo $selcourt['case_type']; echo " "; echo $selcourt['case_no']; echo " / "; echo $selcourt['case_no_year'];  } else{ echo $selcourt['case_type']; echo "-"; echo $selcourt['case_no']; echo " / "; echo $selcourt['case_no_year']; }?>
                                     </td>
                                     <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;"><?php echo $selcourt['name_of_matter'];?></td> 
                                     <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;"><?php echo $selcourt['case_title'];?>
                                     <?php  $selcourt['case_description'];?></td>
                                     <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;">
                                      <?php if($selcourt['hearing_date']!=''){echo date('F d,Y',strtotime($selcourt['hearing_date']));}else{echo " ";}?></td>
                                      <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcourt['case_id'])?>" style="padding: 10px;"><?php echo $selcourt['team_member']; echo $team_member_list;?></td>
                                      <td style="padding: 10px;">
                                        <a href="drafting.php"><i class="icofont icofont-file-file services_logo" title="Drafting"></i></a>
                                        <a href="legal_opinion.php"><i class="icofont icofont-file-text services_logo" title="Legal Opinion"></i></a>
                                        <a href="legal_research.php"><i class="icofont icofont-document-search services_logo" title="Legal Research"></i></a>
                                        <a href="legal_audit.php"><i class="icofont icofont-law-document services_logo" title="Legal Audit"></i></a>
                                        <a href="petition_reply.php"><i class="icofont icofont-court services_logo" title="Petition Reply"></i></a>
                                        <a href="translation_services.php"><i class="icofont icofont-radio-mic services_logo" title="Translation Services"></i></a>
                                        <a href="q&a.php"><i class="icofont icofont-social-google-talk services_logo" title="Q & A"></i></a>
                                        <a href="pre-litigation_adr.php"><i class="icofont icofont-law-alt-2 services_logo" title="Pre-Litigation ADR"></i></a>
                                        <a href="post-litigation_adr.php"><i class="icofont icofont-order services_logo" title="Post-Litigation ADR"></i></a>
                                        <a href="legal_vetting.php"><i class="icofont icofont-law-book services_logo" title="Legal Vetting"></i></a>
                                      </td>
                                    </tr> 
                                    <?php $i++; } ?>
                                  </tbody>
                              </table>
                              <ul class="pagination">
                               <?php if ($page_no > 1){
                                echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>First Page</a></li>";} ?>
                                <li <?php if ($page_no <= 1){
                                  echo "class='disabled'";} ?>>
                                  <a <?php if ($page_no > 1){
                                    echo "href='?page_no=$previous_page&&status=".$_REQUEST['status']."'";} ?>>Previous</a>
                                  </li>
                                  <?php
                                  if ($total_no_of_pages <= 10){
                                    for ($counter = 1;$counter <= $total_no_of_pages;$counter++)
                                    {
                                      if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>"; }
                                      else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
                                    }
                                  }elseif ($total_no_of_pages > 10){
                                    if ($page_no <= 4){
                                      for ($counter = 1;$counter < 8;$counter++){
                                        if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>";
                                      }else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
                                    }
                                    echo "<li><a>...</a></li>";
                                    echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
                                    echo "<li><a href='?page_no=$total_no_of_pages&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
                                  }elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4){
                                    echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
                                    echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
                                    echo "<li><a>...</a></li>";
                                    for ($counter = $page_no - $adjacents;$counter <= $page_no + $adjacents;$counter++)
                                    {
                                      if ($counter == $page_no){
                                        echo "<li class='active'><a>$counter</a></li>";
                                      }else{
                                        echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";
                                      }
                                    }
                                    echo "<li><a>...</a></li>";
                                    echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
                                    echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
                                  }else{
                                    echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
                                    echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
                                    echo "<li><a>...</a></li>";
                                    for ($counter = $total_no_of_pages - 6;$counter <= $total_no_of_pages;$counter++)
                                    {
                                      if ($counter == $page_no){
                                        echo "<li class='active'><a>$counter</a></li>";
                                      }else{
                                        echo "<li><a href='?page_no=$counter&&status=".$_REQUEST['status']."'>$counter</a></li>";
                                      }
                                    }
                                  }
                                }
                                ?>
                                <li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
                                  <a <?php if($page_no < $total_no_of_pages) { echo "href='?page_no=$next_page&&status=".$_REQUEST['status']."'"; } ?>>Next</a>
                                </li>
                                <?php if($page_no < $total_no_of_pages){
                                  echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>Last &rsaquo;&rsaquo;</a></li>";
                                } ?>
                              </ul>
                            </div>
                            <form name="myform" method="POST" action="downloadexcel.php">
                              <div class="modal" id="export">
                                <div class="modal-dialog modal-lg">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h4>Export</h4>
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="row form-group col-sm-12">
                                        <div class="col-sm-3 export-div">
                                          <div class="checkbox-zoom zoom-primary">
                                            <label>
                                              <input type="checkbox" name='checkboxvar[]' style="" value="case_id" />
                                              <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>File</span>
                                              </label>
                                            </div>
                                            <div class="checkbox-zoom zoom-primary">
                                              <label>
                                                <input type="checkbox" name='checkboxvar[]' style="" value="user_id" checked="" />
                                                <span class="cr">
                                                  <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Team Member(s)</span>
                                                </label>
                                              </div>
                                              <div class="checkbox-zoom zoom-primary">
                                                <label>
                                                  <input type="checkbox" name='checkboxvar[]' style="" value="Stage"/>
                                                  <span class="cr">
                                                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Stage</span> </label>
                                                  </div>
                                                  <div class="checkbox-zoom zoom-primary">
                                                    <label>
                                                      <input type="checkbox" name='checkboxvar[]' style="" value="court_name" checked=""/>
                                                      <span class="cr">
                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Court</span> </label>
                                                      </div>
                                                    </div>
                                                    <div class="col-sm-3 export-div">
                                                      <div class="checkbox-zoom zoom-primary">
                                                        <label>
                                                          <input type="checkbox" name='checkboxvar[]' style="" value="case_no" checked=""/>
                                                          <span class="cr">
                                                            <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Case No.</span></label>
                                                          </div>
                                                          <div class="checkbox-zoom zoom-primary">
                                                            <label>
                                                              <input type="checkbox" name='checkboxvar[]' style="" value="case_no_year"/>
                                                              <span class="cr">
                                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Session</span></label>
                                                              </div>
                                                              <div class="checkbox-zoom zoom-primary">
                                                                <label>
                                                                  <input type="checkbox" name='checkboxvar[]' style="" value="date_of_filling"/>
                                                                  <span class="cr">
                                                                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Date of Filling</span></label>
                                                                  </div>
                                                                  <div class="checkbox-zoom zoom-primary">
                                                                    <label>
                                                                      <input type="checkbox" name='checkboxvar[]' style="" value="case_description"/>
                                                                      <span class="cr">
                                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Case Discription</span></label>
                                                                      </div>
                                                                    </div>
                                                                    <div class="col-sm-3 export-div">
                                                                      <div class="checkbox-zoom zoom-primary">
                                                                        <label>
                                                                          <input type="checkbox" name='checkboxvar[]' style="" value="hearing_date"/>
                                                                          <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Last Hearing Date</span></label>
                                                                        </div>
                                                                        <div class="checkbox-zoom zoom-primary">
                                                                          <label>
                                                                            <input type="checkbox" name='checkboxvar[]' style="" value="classification"/>
                                                                            <span class="cr">
                                                                              <i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Classification</span></label>
                                                                            </div>
                                                                            <div class="checkbox-zoom zoom-primary">
                                                                              <label>
                                                                                <input type="checkbox" name='checkboxvar[]' style="" value="case_title" checked=""/>
                                                                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Title</span></label>
                                                                              </div>
                                                                              <div class="checkbox-zoom zoom-primary">
                                                                                <label>
                                                                                  <input type="checkbox" name='checkboxvar[]' style="" value="section_category"/>
                                                                                  <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Section/Category</span></label>
                                                                                </div>
                                                                              </div>
                                                                              <div class="col-sm-3 export-div">
                                                                                <div class="checkbox-zoom zoom-primary">
                                                                                  <label>
                                                                                    <input type="checkbox" name='checkboxvar[]' style="" value="reffered_by"/>
                                                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Referred By</span>
                                                                                  </label>
                                                                                </div>
                                                                                <div class="checkbox-zoom zoom-primary">
                                                                                  <label>
                                                                                    <input type="checkbox" name='checkboxvar[]' style="" value="CNR" />
                                                                                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>CNR</span></label>
                                                                                  </div>
                                                                                  <div class="checkbox-zoom zoom-primary">
                                                                                    <label>
                                                                                      <input type="checkbox" name='checkboxvar[]' style="" value="hearing_date" value="Hearing Date" checked=""/>
                                                                                      <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span><span>Hearing Date</span></label>
                                                                                    </div>
                                                                                    <div class="checkbox-zoom zoom-primary">
                                                                                      <label>
                                                                                        <input type="checkbox" name='checkboxvar[]' style="" value="priority"/>
                                                                                        <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"> </i></span><span>Priority</span></label>
                                                                                      </div>
                                                                                    </div>
                                                                                  </div>
                                                                                  <div class="row col-12">
                                                                                    <h5>Download as:</h5>
                                                                                    <div class="col form-radio">
                                                                                      <div class="radio radio-inline">
                                                                                        <label><input type="radio" class="pdf" name="downloadas" value="pdf" style=""><i class="helper"></i> PDF</label>
                                                                                      </div>
                                                                                      <div class="radio radio-inline">
                                                                                        <label>
                                                                                          <input type="radio" class="excel" name="downloadas" value="excel" style="" checked="">
                                                                                          <i class="helper"></i> Excel</label>
                                                                                        </div>
                                                                                      </div>
                                                                                      <input type="submit" class="btn btn-info btn-sm download" value="Download">
                                                                                    </div>
                                                                                  </div>
                                                                                </div>
                                                                              </div>
                                                                            </div>
                                                                          </form>
                                                                        </div>
                                                                      </div>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="modal" id="record">
                                                         <div class="modal-dialog modal-lg">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h5 class="card-title">Record Hearing</h5>
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            </div>
                                                            <div class="modal-body">
                                                             <div id="casereport_hear">
                                                             </div>
                                                           </div>
                                                           <div class="modal-footer">
                                                            <button class="btn btn-info btn-sm" type="button" onclick="recordHearing()">Record</button>
                                                             <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
                                                           </div>
                                                         </div>
                                                       </div>
                                                     </div>
                                                       <div class="modal" id="assign_team_modal">
                                                        <div class="modal-dialog modal-lg">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h5 class="card-title">Team</h5>
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            </div>
                                                            <div class="modal-body">
                                                             <div id="casereport_team"></div>
                                                           </div>
                                                           <div class="modal-footer">
                                                            <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div class="modal" id="filter">
                                                      <div class="modal-dialog modal-lg">
                                                       <div class="modal-content">
                                                        <div class="modal-header">
                                                          <h5 class="card-title">Filter</h5>
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                          <div id="show_filter"></div>
                                                        </div>
                                                        <div class="modal-footer">
                                                          <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <?php include 'footer.php'; ?>
                                                  <script type="text/javascript" src="scripts/availed_services.js">
                                                  </script>
