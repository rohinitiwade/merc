<?php include("header.php"); ?>
<?php //include("chat-sidebar.php"); ?>
<?php //include("chat-inner.php"); ?> 
<?php include("phpfile/sql_home.php");?>

<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php"); ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Add Documents   </h5>
              </div>
              <div class="page-body">
                <?php
                $date = date('Y-m-d');
                if(isset($_POST['submit'])){
                 extract($_POST);
                  $images = $_FILES["image"]["name"];
                 if($images!=""){
                  $target_dir = "upload_document/";
                  $ext = pathinfo($images, PATHINFO_EXTENSION);
                  $time = time($images).'_'.$images;
                  $target_file = $target_dir . $time;
                  $size = $_FILES["image"]["size"];
                  $up = move_uploaded_file($_FILES["image"]["tmp_name"], $target_file); 
                }
                if($images!="")
                {
                  $caseid =  strstr($case_id, '_', true); 
                   $case_no =substr($case_id, strpos($case_id, "_") + 1);  
                   // echo $case_year =substr($case_id, strpos($case_id, "/") + 1);  
                   if($case_type == 'Other'){
                    $final_type = $_POST['other_doc'];
                    $inserttype = mysqli_query($connection, "INSERT INTO `documents_type` (`documents_type`,`date_time`)VALUES('".$final_type."','".$date."')");
                   }
                   else{
                    $final_type =$case_type;
                   }
                    $insert = mysqli_query($connection, "INSERT INTO `add_document` (`case_id`,`type`,`size`,`title`,`link_document`,`term`,`description`,`judgment_date`,`expiry_date`,`purpose`,`first_party`,`second_party`,`headed_by`,`image`,`date_time`,`case_no`,`user_id`,`division`,`under_division`,`image_name`)VALUES('".$caseid."','".$final_type."','".$size."','".$ext."','".$documents."','".$term."','".$description."','".$judgment_date."','".$expiry_date."','".$purpose."','".$first_pary."','".$second_party."','".$headed_by."','".$time."','".date("Y-m-d H:i:s")."','".$case_no."','".$_SESSION['user_id']."','".$_SESSION['cityName']."','".$_SESSION['under_division']."','".$_FILES["image"]["name"]."')");

                   if($insert){ ?>
                    <script type="text/javascript">
                      window.location.href='manage-document.php';
                    </script>
                  <?php }
                }
              }

              ?>
              <div class="card form-group">
                <div class="card-block">
                  <form class="forms-sample row" method="POST" autocomplete="off" enctype="multipart/form-data">
                    <div class="col-sm-6 form-group row file-div" style="">
                      <div class="col-sm-4 file-text" style="">
                        <span style="color: red">*&nbsp;</span>Click on choose files to select one or many files:</div>
                        <div class="col-sm-8 file-iput" style="">
                          <div style="position:relative;">
                            <a class='btn btn-primary col-sm-12' href='javascript:;'>
                              <input type="file" class="col-sm-12" name="image" size="30" required="">
                            </a>
                            &nbsp;
                            <br/>
                            <span class='' id="upload-file-info" style="color: black;"></span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group col-sm-6 row type-div">
                        <label for="exampleInputUsername1" class="col-sm-4 col-form-label" style=""><span style="color: red;">*</span>&nbsp;&nbsp;Document Type</label>
                        <div class="col-sm-8">
                          <select class="form-control" id="type-select" name="case_type" required="">
                            <option value="<?php echo $_POST['case_type'];?>"><?Php if($_POST['case_type']!=""){ echo $_POST['case_type']; }else{?>Please select<?php } ?></option>
                            <?php $document = mysqli_query($connection, "SELECT * FROM `documents_type` ORDER BY documents_type ASC");
                            while($seldocmnet = mysqli_fetch_array($document)){?>
                              <option value="<?php echo addslashes($seldocmnet['documents_type']);?>"><?php echo addslashes($seldocmnet['documents_type']);?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group col-sm-6 row document-div" style="">
                        <div class="col-sm-4">Would you like to link these documents with any of the cases: </div>
                        <div class="form-radio col col-sm-8">
                          <div class="radio radio-primary radio-inline">
                            <label>
                              <input type="radio" id="chkYes" name="documents" value="Yes">  
                              <i class="helper"></i>Yes
                            </label>
                          </div>
                          <div class="radio radio-primary radio-inline">
                            <label>
                              <input class="form-check form-chk" type="radio" id="chkNo" name="documents" value="No" checked>      
                              <i class="helper"></i>No 
                            </label>
                          </div>
                          <div class="col-sm-12 p-0" style="display: none;" id="title_select">
                          <select class="form-control" id="document-select" name="case_id">
                          <option value="<?php echo $_POST['case_id'];?>"><?php if($_POST['case_id']!=""){ echo $_POST['case_id']; }else{?>Please Select Case<?php } ?></option>
                          <?php  
                          $casedocuments = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE ".$conditionquery."");
                          while($selcses = mysqli_fetch_array($casedocuments)){
                            if($selcses['judicial'] == 'Non-Judicial'){
                            $case= $selcses['client_name'];
                          }else{
                          if($selcses['supreme_court'] == 'Diary Number')
                                      $case= $selcses['diary_no'].'/ '.$selcses['diary_year'].'/ '.$selcses['case_title'];
                                    else
                                    $case= $selcses['case_type'].' '.$selcses['case_no'].'/ '.$selcses['case_no_year'].'/ '.$selcses['case_title'];}?>
                            <option value="<?php echo $selcses['case_id'];?>_<?php echo $selcses['case_no'];?>"><?php echo $case?> </option>
                          <?php } ?>
                        </select>
                        </div>
                        </div>
                      </div>         
                      <!-- <div class="col-sm-12 form-group row">               
                      <div class="col-sm-6 form-group">
                        
                      </div>
                    </div> -->
                        
                          <!-- <div class="col-sm-6 row form-group">
                            <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;Term:</label>
                            <div class="col-sm-8">
                             <input type="text" class="form-control" placeholder="Term" name="term">
                           </div>
                         </div> -->
                         <div class="col-sm-6 row form-group" style="display: none;" id="show_other">
                        <label for="exampleInputUsername1" class="col-sm-4 col-form-label">Other Document Type:</label>
                        <div class="col-sm-8">
                         <input type="text" class="form-control" placeholder="Enter Document Type" id="other_doc" name="other_doc">
                       </div>
                     </div>
                        <div class="col-sm-6 row form-group">
                    <label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;Description:</label>
                    <div class="col-sm-8">
                     <textarea class="form-control" placeholder="Description" name="description"></textarea>
                   </div>
                 </div>
                         
                      <div class="col-sm-6 row form-group">
                        <label for="exampleInputUsername1" class="col-sm-4 col-form-label">Purpose:</label>
                        <div class="col-sm-8">
                         <input type="text" class="form-control" placeholder="Purpose" name="purpose">
                       </div>
                     </div>
                    <!--  <div class="col-sm-6 row">
                          <label for="exampleInputUsername1" class="col-sm-4 col-form-label">Judgement Date:</label>
                          <div class="col-sm-8 input-group date">
                           <input type="text" class="form-control" id="datepicker" placeholder="Select Judgement Date" name="judgment_date">
                           <span class="input-group-addon ">
                            <span class="icofont icofont-ui-calendar"></span>
                          </span>
                        </div>
                      </div> -->
                     <!--  <div class="col-sm-6 row">
                  <label for="exampleInputUsername1" class="col-sm-4 col-form-label">Expiry Date:</label>
                  <div class="col-sm-8 input-group date">
                    <input type="text" class="form-control" id="datepicker1" placeholder="Select Expiry Date" name="expiry_date">
                    <span class="input-group-addon ">
                      <span class="icofont icofont-ui-calendar"></span>
                    </span>
                  </div>
                </div> -->
               <!--  <div class="col-sm-6 row form-group">
                  <label for="exampleInputUsername1" class="col-sm-4 col-form-label">First Party:</label>
                  <div class="col-sm-8">
                   <input type="text" class="form-control" name="first_pary" placeholder="First Party">
                 </div>
               </div>
                     <div class="col-sm-6 row form-group">
                      <label for="exampleInputUsername1" class="col-sm-4 col-form-label">Second Party:</label>
                      <div class="col-sm-8">
                       <input type="text" class="form-control" placeholder="Second Party" name="second_party">
                     </div>
                   </div> -->
               <!-- <div class="col-sm-6 row form-group">
                <label for="exampleInputUsername1" class="col-sm-4 col-form-label">Headed By:</label>
                <div class="col-sm-8">
                 <input type="text" class="form-control" name="headed_by" placeholder="Headed By">
               </div>
             </div> -->
           
         <div class="col-sm-12 row form-group" style="text-align: right;display: inline-block;">
          <button type="submit" class="btn btn-sm btn-info newsubmit" value="Upload" name="submit">Submit</button>
          <button class="btn btn-sm btn-danger" type="button" style="color: white"><a href="manage-document.php"  onclick="return confirm('Are you sure you Want to Cancel?');">Cancel</a> </button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/add_documents.js"></script>






