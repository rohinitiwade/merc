<?php include("header.php"); include("chat-sidebar.php"); include("chat-inner.php"); include("phpfile/sql_home.php");?> 
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<!-- <link rel="stylesheet" href="styles/case-report.css"> -->
			<link rel="stylesheet" href="styles/manage_client.css">	
			<?php include("menu.php") ?>
			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<?php 
							if($_POST['advocatesearch']!=''){
								$advocatequerymanage.= " AND `full_name` Like '%".$_POST['advocatesearch']."%'";
							}?>
							<div class="page-header m-b-10"><h5 class="card-title"><b>Manage <?php if($_SESSION['account'] == 'Individual'){ echo "Client";}else{?> Advocates <?php } ?>(<?php
								$advocate = mysqli_query($connection, "SELECT COUNT(full_name) As TotalAdvocates FROM `advocate` WHERE ".$advocatequerymanage."  ORDER BY id DESC ");
								$count = mysqli_fetch_array($advocate); echo $count['TotalAdvocates']; ?>) </b></h5></div>
								<div class="page-body">

									<div class="card form-group">
										<form name="" method="POST">									
											<div class="card-block">
												<div class="form-group row client_topbuttons">
													<div class="col-sm-2 form-group" >
														<label><b style="color: green;">Search <?php if($_SESSION['account'] == 'Individual'){ echo "Client";}else{?> Advocate <?php } ?></b></label>
													</div>
													<div class="col-sm-3 form-group" >
														<input type="text" name="advocatesearch" id="advocatesearch-id" class="form-control" placeholder="Enter <?php if($_SESSION['account'] == 'Individual'){ echo "Client";}else{?>Advocate <?php } ?> Name" value="<?php echo $_POST['advocatesearch'];?>">
													</div>
													<div class="col-sm-2 form-group" >
														<input type="submit" class="btn-primary btn-sm btn" value="Search" name="Searchcaseno">
													</div>
							<!-- <div class="col-sm-2 form-group" >
								<a href="manage-client" class="btn btn-success btn-sm add-advocates_button">All Advocates</a>
							</div> -->
						</form>
						<div class="col-sm-3 form-group" style="text-align: right;">
							<!-- <button class="btn btn-info btn-sm export_button" id="myBtn" data-target="#export" data-toggle="modal">Export</button> -->
							<button type="button" class="btn btn-info btn-sm" id="myBtn" data-target="#export" data-toggle="modal">Export</button>
							<a href="add-advocates.php" class="btn btn-warning btn-sm add-advocates_button"><i class="feather icon-plus plus" ></i>Add Advocate</a></div>
							<div class="table-responsive">
								<table id="order-listing" class="table table-bordered">
									<thead>
										<tr>
											<th class="court-report">Sr.No.</th>
											<th class="checkclass">
												<div class="checkbox-zoom zoom-primary"><label>
													<input type="checkbox" class="check" id="checkAll" />
													<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span></label></div></th>
													<th class="court-report">Full Name</th>
													<th class="court-report">Company Name</th>
													<th class="court-report">E-mail Address</th>
													<th class="court-report">Phone no.</th>
													<th class="court-report">Actions</th>
												</tr>
											</thead>
											<tbody><?php
											if (isset($_GET['page_no']) && $_GET['page_no']!="") {
												$page_no = $_GET['page_no'];
											} else {
												$page_no = 1;
											}
											$total_records_per_page = 50;
											$offset = ($page_no-1) * $total_records_per_page;
											$previous_page = $page_no - 1;
											$next_page = $page_no + 1;
											$adjacents = "2"; 
											$result_count = mysqli_query($connection,"SELECT * FROM advocate  WHERE ".$advocatequerymanage."");
											$total_records = mysqli_num_rows($result_count);
	                            //$total_records = $total_records['total_records'];
											$total_no_of_pages = ceil($total_records / $total_records_per_page);
											$second_last = $total_no_of_pages - 1;
											$advocate = mysqli_query($connection, "SELECT * FROM advocate WHERE ".$advocatequerymanage."  ORDER BY `id` DESC LIMIT  $offset, $total_records_per_page");
											$count = mysqli_num_rows($advocate);
											if($count!=0){
												$i=1+$offset;
												while($seladvf = mysqli_fetch_array($advocate)){
													?>
													<tr>
														<td class="srno"><span><?php echo $i;?></span></td>
														<td>
															<div class="checkbox-zoom zoom-primary">
																<label><input type="checkbox" class="check" name="checkAll[]" />
																	<span class="cr">
																		<i class="cr-icon icofont icofont-ui-check txt-primary"></i></span></label></div>
																	</td>
																	<td><?php echo utf8_decode($seladvf['full_name']);?> <?php echo utf8_decode($seladvf['father_name']);?></td>
																	<td ><?php echo $seladvf['company_name'];?></td>
																	<td ><?php echo $seladvf['email'];?></td>
																	<td ><?php echo $seladvf['mobile'];?></td>
																	<td>
																		<i class="feather icon-user" data-toggle="modal" data-target="#viewdetailadv" onclick="viewdetail(<?php echo $seladvf['id'];?>)" title="View Advocate Cases" style="color: orange;"></i>
																		<i class="feather icon-briefcase" data-toggle="modal" data-target="#viewadvocate" onclick="viewcases(<?php echo $seladvf['case_id'];?>,<?php echo $seladvf['id'];?>)" title="View Advocate Cases" style="color: blue;"></i>
																		<a href="update-advacate.php?id=<?php echo base64_encode($seladvf['id']);?>">
																			<i class="feather icon-edit"  style="margin-right: 5px;color: black;" title="Edit Advocate" style="color: orange;"></i></a>
																			<a href="delete-advacate.php?id=<?php echo $seladvf['id'];?>">
																				<i class="feather icon-trash" style="margin-right: 5px; color: red;" onclick="return confirm('Are you sure you want to Delete?');" title="Delete" >
																				</i></a></td>
																			</tr> 
																			<?php $i++; } ?>
																			<tr>
																			<?php }else{ ?>
																				<td colspan="7"><b><?php if($_SESSION['account'] == 'Individual'){ echo "Client";}else{?>Advocate <?php } ?> Not Found</b></td>
																			<?php } ?>
																		</tr>
																	</tbody>
																</table>

																<div class="col-sm-12">
																	<nav aria-label="Page navigation" style="float: right;">
																		<ul class="pagination">
																			<?php if ($page_no > 1){
																				echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>First Page</a></li>";} ?>
																				<li <?php if ($page_no <= 1){
																					echo "class='disabled'";} ?>>
																					<a <?php if ($page_no > 1){
																						echo "href='?page_no=$previous_page&&status=".$_REQUEST['status']."'";} ?>>Previous</a>
																					</li>
																					<?php
																					if ($total_no_of_pages <= 10){
																						for ($counter = 1;$counter <= $total_no_of_pages;$counter++)
																						{
																							if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>"; }
																							else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
																						}
																					}elseif ($total_no_of_pages > 10){
																						if ($page_no <= 4){
																							for ($counter = 1;$counter < 8;$counter++){
																								if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>";
																							}else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
																						}
																						echo "<li><a>...</a></li>";
																						echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
																						echo "<li><a href='?page_no=$total_no_of_pages&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
																					}elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4){
																						echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
																						echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
																						echo "<li><a>...</a></li>";
																						for ($counter = $page_no - $adjacents;$counter <= $page_no + $adjacents;$counter++)
																						{
																							if ($counter == $page_no){
																								echo "<li class='active'><a>$counter</a></li>";
																							}else{
																								echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";
																							}
																						}
																						echo "<li><a>...</a></li>";
																						echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
																						echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
																					}else{
																						echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
																						echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
																						echo "<li><a>...</a></li>";
																						for ($counter = $total_no_of_pages - 6;$counter <= $total_no_of_pages;$counter++)
																						{
																							if ($counter == $page_no){
																								echo "<li class='active'><a>$counter</a></li>";
																							}else{
																								echo "<li><a href='?page_no=$counter&&status=".$_REQUEST['status']."'>$counter</a></li>";
																							}
																						}
																					}
																				}
																				?>
																				<li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
																					<a <?php if($page_no < $total_no_of_pages) { echo "href='?page_no=$next_page&&status=".$_REQUEST['status']."'"; } ?>>Next</a>
																				</li>
																				<?php if($page_no < $total_no_of_pages){
																					echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>Last &rsaquo;&rsaquo;</a></li>";
																				} ?>
																			</ul>
																		</nav>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal" id="export">
							<div class="modal-dialog modal-lg">
								
								<div class="modal-content" >
									<div class="modal-header">
										<h4>Export</h4>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>
									<div class="modal-body">
										<form method="POST" action="downloadClient.php">

											<div class="row form-group col-sm-12">
												<input type="hidden" name="checkboxvar[]" value="full_name">
												<input type="hidden" name="checkboxvar[]" value="mobile">
												<input type="hidden" name="checkboxvar[]" value="email">
												<input type="hidden" name="checkboxvar[]" value="website">
												<input type="hidden" name="checkboxvar[]" value="company_name">
												<input type="hidden" name="checkboxvar[]" value="gst">
												<input type="hidden" name="checkboxvar[]" value="pan_no">
												<input type="hidden" name="checkboxvar[]" value="tin">
												<div class="form-radio col">
													<div class="col-sm-5 radio radio-primary radio-inline">
														<label>
															<input type="radio" class="chkPassport" name="downloadas"  value="excel">	
															<i class="helper"></i>Excel
														</label>
													</div>
													<div class="col-sm-6 radio radio-primary radio-inline">
														<label>
															<input type="radio" class="chkPassport" name="downloadas" value="pdf">			
															<i class="helper"></i>PDF
														</label>
													</div>
													<div id="document-table-div-export"></div>
												</div>  
												<!-- <div class="col-sm-3">
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="radio" class="chkPassport" name="downloadas"  value="excel">	
															<input type="checkbox" name='downloadas' value="excel"  /> -->
															<!-- <span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span> 
															<span>Excel</span>
														</label>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="radio" class="chkPassport" name="downloadas"  value="pdf">	
															<!-- <input type="checkbox" name='downloadas' value="pdf" /> -->
															<!-- <span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span> 
															<span>PDF</span>
														</label>
													</div>
												</div> -->
												<button type="button" class="btn btn-warning btn-sm" name="download_advocate" onclick="download_doc()">Download</button>
											</div>
										</form>
									</div>
								</div>
								
							</div>
						</div>
						<!-- <div class="modal" id="export">
							<div class="modal-dialog modal-lg">
								<form name="myform" method="POST" action="downloadClient.php">
									<div class="modal-content" >
										<div class="modal-header">
											<h4>Export</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										<div class="modal-body">
											<div class="row form-group col-sm-12">
												<div class="col-sm-3">
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="full_name" checked="" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Full Name</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="website" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Website</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="office_address" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Office Address</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="age" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Age</span>
														</label>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="email" checked=""/>
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Email Address</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="gst" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>GSTIN</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="home_adress" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Home Address</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="father_name" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Father's Name</span>
														</label>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="mobile" checked=""/>
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Phone Number</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="hourly_rate" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Hourly Rate (INR)</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="date_time" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Created Date</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="tin" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>TIN #</span>
														</label>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="company_name" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Company Name</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="pan_no" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>PAN Number</span>
														</label>
													</div>
													<div class="checkbox-zoom zoom-primary">
														<label>
															<input type="checkbox" name='checkboxvar[]' value="poin_of_contact" />
															<span class="cr">
																<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
															</span>
															<span>Point of Contact(s)</span>
														</label>
													</div>
												</div>
											</div>
											<div class="row col-sm-12">
												<div class="downlab"><h4>Download as:</h4></div>
												<div class="col-sm-6 form-radio">
													<div class="radio radio-inline">
														<label>
															<input type="radio" name="downloadas" value="pdf">
															<i class="helper"></i> PDF
														</label>
													</div>
													<div class="radio radio-inline">
														<label>
															<input type="radio" name="downloadas" value="excel" checked="">
															<i class="helper"></i> Excel
														</label>
													</div>
												</div>						
												
												<button class="btn btn-warning btn-sm" type="submit">Download</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div> -->
						<div class="modal" id="viewadvocate">
							<div class="modal-dialog modal-lg">
								<div id="casedetails"></div>
							</div>
						</div>

						<div class="modal" id="viewdetailadv">
							<div class="modal-dialog modal-lg">
								<div id="advdetails"></div>
							</div>
						</div>
						<?php include 'footer.php'; ?>
						<script type='text/javascript' src='scripts/jspdf.min.js'></script>
						<script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
						<script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
						<script type="text/javascript" src="scripts/all_export.js"></script>
						<script type="text/javascript" src="scripts/manage-advocate.js"></script>

						

						