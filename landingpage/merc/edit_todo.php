<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<link rel="stylesheet" type="text/css" href="styles/jquery.datetimepicker.css">
<style type="text/css">
	.input-group{margin-bottom:0}.select-due-date{display:flex;align-items:center}.pagination{float:right}
</style>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>
			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title"> <b>Edit To Do </b>&nbsp;&nbsp;<a href="create-todo" class="btn-info btn-sm">Back to Create To Do</a></h5>
							</div>

							<?php $createtodo = mysqli_query($connection, "SELECT `content`,`private`,`user_id`,`from_date`,`to_date` FROM `todo_list` WHERE `todo_id`='".base64_decode($_GET['id'])."'");
							$selcreatetodo = mysqli_fetch_array($createtodo);?>
							<div class="page-body">
								<div class="card form-group">
									<div class="card-block">
										<div class="form-group">
											<input type="hidden" name="todoid" id="todoid" value="<?php echo base64_decode($_GET['id'])?>">
											<div class="input-group">
												<span class="input-group-addon" id="basic-addon1"><i class="feather icon-edit createi"></i></span>
												<textarea class="form-control create" id="desc" aria-label="With textarea" placeholder="Click here to write to-do description"></textarea>
											</div>
										</div>
										<div class="col-sm-12 row select-due-date" style="float: left;">
									<!-- 		<h6 class="col-sm-12 p-0">Please select due date</h6>             
											<div class="col-sm-3 p-0">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon1"><i class="feather icon-calendar createi"></i></span>
													<input type="hidden" value="" id="from-date-value" name="">
													<input type="text" class="form-control" id="datepicker" name="startdate" aria-describedby="basic-addon2" value="">
												</div>
											</div>
											<div class="col-sm-1 p-0">
												<span style="text-align: center;"><h5><b>to</b></h5></span>
											</div>
											<div class="col-sm-3 p-0">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon1"><i class="feather icon-calendar createi"></i></span>
													<input type="hidden" value="" id="to-date-value" name="">
													<input type="text" class="form-control" id="datepicker1" name="enddate"  aria-describedby="basic-addon2">

												</div>
											</div> -->

															<div class="col-sm-1 p-0">
												<h6><b>Expiry Date</b></h6>
											</div>
											<div class="col-sm-3 p-0">
												<div class="input-group">
													<span class="input-group-addon" id="basic-addon1"><i class="feather icon-calendar createi"></i></span>
													<input type="text" class="form-control" id="datepickerdataedit" aria-describedby="basic-addon2" name="datepickerdataedit" value="" placeholder="Enter Expiry Date Here">
												</div>
											</div>

															<div class=" form-group">
												<div class="checkbox-zoom zoom-primary col-sm-12" style="padding-top: 20px;">
													<label>
														<input type="checkbox" value="private" id="private" class="private" <?php if($selcreatetodo['private']=='private'){ echo "checked"; }?>>
														<span class="cr">
															<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
														</span>
														<span>Mark As Private</span>
													</label>
												</div>
											</div>
										</div>
							<!-- 			<div class="col-sm-6" style="float: left;">
											<div class="form-group">
												<h6> Please set your auto reminders </h6>
											</div>
											<div class="form-group" id="reminder_clickhere">
												<b onclick="reminder()" style="cursor: pointer;"> Click here </b>&nbsp; to add auto reminders
											</div>
											<div id="reminder_input" style="display: none;">
												<div class="p-0 col-sm-12" id="input" style="float: left;">
												</div>
												<p style="float: left;"><button class="btn btn-info btn-mini" onclick="add_field();">
													<i class="feather icon-plus"></i></button> Add an additional reminder</p>
												</div>
											</div> -->

										
							
											<div class="row col-sm-12">
												<div class="col-sm-4 p-0">
													<div class="form-group">
														<label for="usr" class="col-form-label"><b>Relate To</b></label><br>
														<select id="relate" name="framework[]" multiple="multiple"  class="form-control" placeholder="Please Select">
															<option value="">Please Select</option>
														</select>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group framework-div">
														<label for="usr" class="col-form-label"><b>Assign To</b></label><br>
														<select id="framework" name="framework[]" multiple class="form-control" >

														</select> 
													</div>
												</div>
								<!-- 				<div class="col-sm-4 p-0">
													<div class="form-group framework-div">
														<label for="usr" class="col-form-label"><b>Assign To Advocates</b></label><br>
														<select id="advocate" name="framework[]" multiple class="form-control" >

														</select> 
													</div>
												</div> -->
												<div class="todo-btn" style="padding-top: 30px;"> 
													<button class="btn button_submit btn-sm btn-primary" type="button" onclick="updatetodo()">Update</button>
												</div> 
												<!-- <div class="todo-btn" style="padding-top: 30px;"> 
													<a class="btn button_submit btn-sm btn-primary" href="create-todo.php">Cancel</a>
												</div>  -->
											</div>

											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php include 'footer.php'; ?>

		<script type="text/javascript" src="scripts/jquery.datetimepicker.full.min.js"></script>
		<script src='scripts/todo_list.js' type='text/javascript'></script>
		<script src='scripts/edit-todo.js' type='text/javascript'></script>
						<script>
									$( function() {
										$( "#datepickerdataedit" ).datepicker({
											minDate: 0,
											dateFormat: 'yy-mm-dd' 
										});

									} );
								</script>
