<link rel="stylesheet" type="text/css" href="styles/caselaw-search.css">
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<?php session_start(); ?> 
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <?php $case = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
          $selcase = mysqli_fetch_array($case);
           if($selcase['supreme_court'] == 'Diary Number')
                                     $case =  $selcase['diary_no'].'/ '.$selcase['diary_year'];
                                    else
                                     $case = $selcase['case_type'].' '.$selcase['case_no'].'/ '.$selcase['case_no_year']; ?>
          <div class="page-wrapper">
            <div class="page-body">
             <div class="card">
              <div class="card-block">
                <div class="row">

                  <h3 class="card-header form-group title_h3 col-sm-12" style="padding-top: 1%" id="district_court_title"><?php echo $case;?> </h3>
                  <div class="input-group col-sm-8 mb-3" style="float: left;">
                    <input type="text" class="form-control" placeholder="Please enter here yours search query" name="search" id="search">
                    <input type="hidden" class="form-control inputBox" id="case_id" name="text" value="<?php echo base64_decode($_GET['caseid']);?>" >
                    <div class="input-group-append">
                      <span class="input-group-text btn" style="background: blue;color: white" onclick="getquery()"><i class="fa fa-search"></i></span>
                    </div>
                  </div>
                 <!--  <div class="col-sm-4 mb-3" style="float: left;">
                   <button class="btn btn-info btn-sm" type="button" onclick="viewCase('<?php echo $_GET['caseid']; ?>')">View Case</button> 
                 </div> -->
                 <div id="show_data" class="col-sm-12"></div>
                 <div class="pagination" class="col-sm-12"></div>
               </div>
             </div>
           </div>
         </div>
<!-- <div id="styleSelector">
</div> -->
</div>
</div>
</div>
</div>
</div>
<!-- </div>
</div> -->
<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/caselaw-search.js"></script>

