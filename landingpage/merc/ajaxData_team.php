<?php 
// Include the database config file 
include_once 'includes/dbconnect.php'; 
 
if(!empty($_POST["state_id"])){ 
    // Fetch state data based on the specific country 
     $query = "SELECT * FROM geo_locations WHERE parent_id = ".$_POST['state_id']."  ORDER BY name ASC"; 
    $result = $connection->query($query); 
     
    // Generate HTML of state options list 
    if($result->num_rows > 0){ 
        echo '<option value="">Select State</option>'; 
        while($row = $result->fetch_assoc()){  
            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>'; 
        } 
    }else{ 
        echo '<option value="">City not available</option>'; 
    } 
}elseif(!empty($_POST["city_id"])){ 
    // Fetch city data based on the specific state 
    $query = "SELECT * FROM geo_locations WHERE parent_id = ".$_POST['city_id']."  ORDER BY name ASC"; 
    $result = $connection->query($query); 
     
    // Generate HTML of city options list 
    if($result->num_rows > 0){ 
        echo '<option value="">Select city</option>'; 
        while($row = $result->fetch_assoc()){  
            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>'; 
        } 
    }else{ 
        echo '<option value="">Town not available</option>'; 
    } 
} 
?>