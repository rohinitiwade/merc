<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> <!-- <link rel="stylesheet" href="css/vertical-layout-light/add_cases.css"> -->
<?php include("phpfile/sql_home.php"); ?>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>
			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title">My Expenses</h5>
							</div>
							<div class="page-body">
								<div class="form-group">
									<div class="card col-sm-12">
										<div class="card-block form-group">
											<?php if(isset($_POST['submit'])){
												extract($_POST);
												$sqlcase = mysqli_query($connection, "SELECT `case_id` FROM `reg_cases` WHERE `case_no`='".$caseno."'");
												$sqlrows = mysqli_fetch_array($sqlcase);
												$timesheetquery.=" AND `case_id`='".$sqlrows['case_id']."'";
											}?>
											<form name="Myform" action="" method="POST" class="col-sm-12 row">

												<div class="col-sm-2 form-group">
													<label><b>Select Case Number</b></label>
												</div>
												<div class="col-sm-4 form-group">
													<input type="text" name="caseno" id="case_no" class="form-control" placeholder="Select Case Number" required="" value="<?php echo $_POST['caseno'];?>">
												</div>
												<div class="col-sm-2 form-group">
													<button type="submit" name="submit" class="btn-primary btn btn-sm">Submit</button>
												</div>
												<div class="col-sm-4 form-group text-right">
													<a href="add-expenses.php" class="btn btn-info btn-sm form-group"><i class="fa fa-plus"></i>Add Expenses</a>
													<button class="btn btn-warning btn-sm form-group" type="button" id="myBtn" data-target="#filter" data-toggle="modal">Filter</button>
													<button class="btn btn-primary btn-sm form-group" type="button" id="myBtn" data-target="#export" data-toggle="modal">Export</button>
												</div>											
											</form>
											<div class="col-sm-12 form-group table-responsive">
												<table id="order-listing" class="table table-bordered">
													<thead class="bg-primary">
														<tr>
															<th>Id</th>
															<th>Case</th>
															<th>Date</th>
															<th>Particulars</th>
															<th>Money Spent (INR)</th>
															<th>Payment Method</th>
															<!-- <th>Member</th> -->
															<th>Image</th>
															<th>Actions</th>
														</tr>
													</thead>
													<tbody>
														<?php 
														$add_expenses = mysqli_query($connection, "SELECT * FROM `add_expenses` WHERE ".$timesheetquery." ORDER BY exp_id DESC");
														$counts = mysqli_num_rows($add_expenses);
														if($counts!=0){
															$i=1;
															while($row=mysqli_fetch_array($add_expenses)){
																$casedocuments =mysqli_query($connection,"SELECT * FROM `reg_cases` WHERE  `case_id`='".$row['case_id']."'");
																$selcses = mysqli_fetch_array($casedocuments);
																?>
																<tr>
																	<td><?php echo $i; ?></td>
																	<td style="word-break:break-all;" width="200"> <a href="view-case.php.php?caseid=<?php echo base64_encode($selcses['case_id']);?>"><?php echo $selcses['case_type'];?> / <?php echo $selcses['case_no'];?> / <?php echo $selcses['case_no_year'];?> / <?php echo $selcses['case_title'];?></a></td>
																	<td><?php echo $newDate = date("d-m-Y", strtotime($row['datetime']));?></td>
																	<td><?php echo $row['particulars'];?></td>
																	<td><?php echo $row['moneyspent'];?></td>
																	<td><?php echo $row['method'];?></td>
																<!-- <td><?php
																 $member = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='".$row['userid']."'"); $selteam = mysqli_fetch_array($member); echo $selteam['name']; echo "&nbsp;";echo $selteam['last_name'];?>
																</td> -->
																<td>
																	<?php $addimage = mysqli_query($connection, "SELECT `image` FROM `addexpdoc` WHERE `exp_id`='".$row['exp_id']."'");
																	while($selimages = mysqli_fetch_array($addimage)){
																		$ext = pathinfo($selimages['image'], PATHINFO_EXTENSION); ?>

																		<?php if($selimages['image']!=""){?><a href="upload_expimage/<?php echo $selimages['image'];?>" download><?php } ?>
																		<i class="feather icon-file-pdf-o pdfcolor"></i><?php  $ext = pathinfo($selimages['image'], PATHINFO_EXTENSION); 
																		if($ext=='pdf'){ 
																			echo "<img src='images/pdf.png' width='30' title='".$selimages['image']."'>"; }elseif($ext=='docx'){  
																				echo "<img src='images/word.png' width='30' title='".$selimages['image']."'>"; }elseif($ext=='xlsx'){  
																					echo "<img src='images/excel.png' width='30' title='".$selimages['image']."'>"; }elseif($ext=='csv'){  
																						echo "<img src='images/csv.png' width='30' title='".$selimages['image']."'>"; }elseif($ext=='png'){  
																							echo "<img src='images/png.png' width='30' title='".$selimages['image']."'>"; }elseif($ext=='jpg' || $ext=='jpeg'){  
																								echo "<img src='images/jpg.png' width='30' title='".$selimages['image']."'>"; } ?></a>
																							<?php } ?>
																						</td>
																						<td>
																							<a href="update_expenses.php?id=<?php echo base64_encode($row['exp_id']);?>">
																								<i class="feather icon-edit action-css" style="color: orange;" title="Edit Expenses"></i></a>
																								<a href="delete_exp.php?id=<?php echo $row['exp_id'];?>" onclick="return confirm('Are you sure you want to delete?');"><i class="feather icon-trash action-css" style="color: red;" title="Delete"></i></a>
																							</td>
																						</tr> 
																						<?php $i++;  }?>

																					<?php }else{ ?>
																						<tr>
																							<td colspan="8">
																								<b>Data Not Found</b>
																							</td>
																						<?php } ?>
																					</tr>
																				</tbody>
																			</table>


																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div id="myModal" class="modal">
									<span class="close">&times;</span>
									<img class="modal-content" id="img01">
									<div id="caption"></div>
								</div>

								<div class="modal" id="filter">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<form method="POST">
												<div class="modal-header">
													<h4>Filter</h4>
													<button type="button" class="close" data-dismiss="modal">&times;</button>
												</div>
												<div class="modal-body">

													<div class="col-sm-12 row">
														<div class="col-sm-4 form-group">
															<label>Cases</label>
															<select class="form-control" name="caseno" id="state" required>
																<option value="">Please Case No</option>
																<?php $caseno = mysqli_query($connection, "SELECT * FROM `add_expenses` WHERE `case_id`!=''"); 
																while($selcases = mysqli_fetch_array($caseno)){?>
																	<option value="<?php echo $selcases['case_id'];?>"><?php echo $selcases['case_id'];?></option>
																<?php } ?>

															</select>
														</div>
														<div class="col-sm-4">
															<label>Payment Method</label>
															<select class="form-control" name="payment" id="state" required>
																<option value="">Please select</option>
																<option value="Bank Transfer">Bank Transfer</option>
																<option value="Cash">Cash</option>
																<option value="Cheque">Cheque</option>
																<option value="Online Payment">Online Payment</option>
																<option value="Other">Other</option>
															</select> 
														</div>

														<div class="col-sm-4">
															<label>Team Member</label>
															<select name="team_member" class="form-control">
																<option value="">--Select Team Member--</option>
																<?php $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `parent_id`='".$_SESSION['user_id']."'");
																while($selteam = mysqli_fetch_array($team)){?>
																	<option value="<?php echo $selteam['reg_id'];?>"><?php echo $selteam['name'];?> <?php echo $selteam['last_name'];?></option>
																<?php } ?>
															</select>
														</div>
														<div class="col-sm-4">
															<label>From</label>
															<input type="text" id="datepicker" class="form-control" name="from_date" placeholder="Enter From Date">
														</div>
														<div class="col-sm-4">
															<label>To</label>
															<input type="text" id="datepicker1" class="form-control" name="to_date" placeholder="Enter to Date">
														</div>


													</div>

												</div>
												<div class="modal-footer">
													<button type="reset" class="btn btn-danger btn-sm">Reset</button>
													<button type="submit" class="btn btn-info btn-sm">Submit</button>
													<button type="button" data-dismiss="modal" class="btn btn-info btn-sm">Close</button>        
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="modal" id="export">
									<form name="myform" method="POST" action="downloadExpenses.php">
										<div class="modal-dialog modal-lg">
											<div class="modal-content">
												<div class="modal-header">
													<h4>Export</h4>
													<button type="button" class="close" data-dismiss="modal">&times;</button>
												</div>
												<div class="modal-body">
																<!-- <div class="col-sm-12 row form-group">

																	<div class="checkbox-zoom zoom-primary">
																		<label>
																			<input type="checkbox" name='checkboxvar[]'  value="moneyspent" />
																			<span class="cr">
																				<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																			</span>
																			<span>Money Spent</span>
																		</label>
																	</div>
																	<div class="checkbox-zoom zoom-primary">
																		<label>
																			<input type="checkbox" name='checkboxvar[]'  value="caseno" />
																			<span class="cr">
																				<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																			</span>
																			<span>Case</span>
																		</label>
																	</div>
																	<div class="checkbox-zoom zoom-primary">
																		<label>
																			<input type="checkbox" name='checkboxvar[]'  value="method" />
																			<span class="cr">
																				<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																			</span>
																			<span>Payment Mode</span>
																		</label>
																	</div>
																	<div class="checkbox-zoom zoom-primary">
																		<label>
																			<input type="checkbox" name='checkboxvar[]'  value="particulars" />
																			<span class="cr">
																				<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																			</span>
																			<span>Particulars</span>
																		</label>
																	</div>
																	<div class="checkbox-zoom zoom-primary">
																		<label>
																			<input type="checkbox" name='checkboxvar[]' value="notes" />
																			<span class="cr">
																				<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																			</span>
																			<span>Notes</span>
																		</label>
																	</div>
																	<div class="checkbox-zoom zoom-primary">
																		<label>
																			<input type="checkbox" name='checkboxvar[]'  value="date" />
																			<span class="cr">
																				<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																			</span>
																			<span>Date</span>
																		</label>
																	</div>
																	<div class="checkbox-zoom zoom-primary">
																		<label>
																			<input type="checkbox" name='checkboxvar[]' value="datetime" />
																			<span class="cr">
																				<i class="cr-icon icofont icofont-ui-check txt-primary"></i>
																			</span>
																			<span>Entered Date</span>
																		</label>
																	</div>
																	
																</div> -->
																<div class="col-sm-12 row">
																	<label class="col-sm-2 p-0">Download as:</label>
																	<div class="form-radio col-sm-3">
																		<div class="radio radio-primary radio-inline">
																			<label>
																				<input type="radio" name="downloadas" value="pdf" checked>

																				<i class="helper"></i>PDF
																			</label>
																		</div>
																		<div class="radio radio-primary radio-inline">
																			<label>
																				<input type="radio" name="downloadas" value="excel">

																				<i class="helper"></i>Excel
																			</label>
																		</div>
																		<div id="document-table-div-export"></div>
																	</div>

																	<div class="col-sm-4">
																		<button type="button" type="button" class="btn btn-info btn-sm" onclick="download_doc()">Download</button>
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
															</div>
														</div>
													</div>
												</form>
											</div>
											<div id="abcd" class="modal fade" role="dialog">
												<div class="modal-dialog">

													<!-- Modal content-->
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>

														</div>
														<div class="modal-body">
															<img src="images/female2.jpg">
														</div>
														<div class="modal-footer">
															<button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
														</div>
													</div>

												</div>
											</div>

											<?php include 'footer.php'; ?>
											<script type="text/javascript">
												$(".add-expenses-li").addClass('active pcoded-trigger');
											</script>
											<script type='text/javascript' src='scripts/jspdf.min.js'></script>
											<script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
											<script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
											<script type="text/javascript" src="scripts/all_export.js"></script>
											<script type="text/javascript" src="scripts/my-expenses.js"></script>









