<?php session_start(); ?> 
<?php include("header.php") ?> 
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <style type="text/css">
      .row{
        margin-left: 0;
        margin-right: 0;
      }
      .form-group {
        margin-bottom: 10px;
      }
      .action-th{
        width: 1%;
      }
      .particulars-th{
        width: 40%;
      }
      .tax-th{
        width: 20%;
      }
      .total-th{
        width: 10%;
      }
    </style>
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header m-b-10">
              <h5 class="card-title">Create Invoice </h5>
            </div>
            <div class="page-body">
              <div class="card form-group">
                <div class="card-block">
                  <div class="col-sm-6 p-0 row f-left">
                    <label class="col-sm-2 d-flex align-items-center">Client: <span style="color: red;">*</span></label>
                    <div class="col-sm-9 p-0" id="client-list">
                     <!--  <select class="form-control">

                     </select> -->
                     <select id="getclient" name="get-client"  class="form-control" placeholder="Please Select">
                      <option value="">Please Select</option>
                    </select>
                  </div>
                  <div class="col-sm-1 d-flex align-items-center">
                    <button class="btn btn-info btn-mini" type="button" title="Create New Client" data-target="#newClientModal" data-toggle="modal"><b><i class="feather icon-plus"></i></b></button>
                  </div>
                </div>
                <div class="col-sm-6 row p-0 f-left form-group">
                  <div class="col-sm-12 row form-group">
                    <label class="col-sm-4 text-right">Title: <span style="color: red;">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="" id="title-invoice" value="Invoice">
                    </div>
                  </div>
                  <!-- <div class="col-sm-12 row form-group">
                    <label class="col-sm-4 text-right">Invoice Number: <span style="color: red;">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="" id="number-invoice" disabled>
                    </div>
                  </div> -->
                  <div class="col-sm-12 row form-group">
                    <label class="col-sm-4 text-right">Date of Invoice: <span style="color: red;">*</span></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="" id="date-invoice">
                    </div>
                  </div>
                  <div class="col-sm-12 row form-group">
                    <label class="col-sm-4 text-right">Billing Address: <span style="color: red;">*</span></label>
                    <div class="col-sm-8">
                      <select class="form-control" id="billing-invoice">
                        <option value="">Please select</option>
                        <option>Nagpur, India</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12 form-group f-left">
                  <label style="width: auto;">Import from: </label>
                  <!-- <button class="btn btn-primary btn-sm" type="button">Timesheet</button> -->
                  <button class="btn btn-success btn-sm" type="button" onclick="hearingfetch('expenses')">Expenses</button>
                  <button class="btn btn-warning btn-sm" type="button" id="fetch_hearing" onclick="hearingfetch('hearing')">Hearings</button>
                  <button class="btn btn-danger btn-sm" type="button">Fee Received</button>
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered" id="invoice-table">
                    <thead>
                      <tr>
                        <th class="action-th"><button class="btn btn-success btn-mini" type="button" onclick="addMoreInvoice()"><i class="feather icon-plus"></i></button></th>
                        <th class="particulars-th">Particulars</th>
                        <th>Quantity</th>
                        <th>Rate/ Unit Cost(INR)</th>
                        <th class="tax-th">Tax(%)</th>
                        <th class="total-th">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td></td>
                        <td contenteditable="true" id="particulars_0">
                        </td>
                        <td><p contenteditable="true" id="quantity_0" onkeyup="getQuantity('0')"></p>
                        </td>
                        <td><p contenteditable="true" id="cost_0" onkeyup="getQuantity('0')"></p>
                        </td>
                        <td>
                          <div class="col-sm-12 row p-0">
                            <div class="col-sm-12 p-0" id="tax_val_0">
                              <select class="form-control taxValue calculation" id="taxValue_0">
                                <option>Please select</option>
                                <option value="IGST_0">IGST</option>
                                <option value="GST_0">GST</option>
                                <option value="Service Tax_0">Service Tax</option>
                                <option value="VAT_0">VAT</option>
                              </select>
                            </div>
                            <div class="col-sm-5" id="per-tax_0" style="display: none;">
                              <input type="text" class="form-control" placeholder="%" name="" id="tax-value-in-per_0" name="" onkeyup="getTaxCalculation('0')">
                            </div>
                          </div>
                        </td>
                        <td id="total_0">
                          0.00
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th colspan="4" rowspan="6"></th>                          
                        <td>Subtotal</td>
                        <th id="subtotal"></th>
                      </tr>
                      <tr style="display: none;" id="igst">
                        <td>IGST</td>
                        <th id="igst-total">0.00</th>
                      </tr>
                      <tr style="display: none;" id="gst">
                        <td>GST</td>
                        <th id="gst-total">0.00</th>
                      </tr>
                      <tr style="display: none;" id="service-tax">
                        <td>Service Tax</td>
                        <th id="service-total">0.00</th>
                      </tr>
                      <tr style="display: none;" id="vat">
                        <td>VAT</td>
                        <th id="vat-total">0.00</th>
                      </tr>
                      <tr>
                        <th>Total</th>
                        <th><b id="total_val">0.00</b></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="col-sm-12 p-0">
                  <div class="col-sm-4 p-0">
                    <label>Notes Visible to Client</label>
                    <textarea class="form-control form-group" rows="3" id="notes_for_client"></textarea>
                    <button type="button" class="btn btn-info btn-mini">Save as Draft</button>
                    <button type="button" class="btn btn-warning btn-mini" onclick="sendbyEmail()">Send by Email</button>
                    <button type="button" class="btn btn-danger btn-mini" onclick="history.back();">Cancel</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="modal" id="hearing-data">
	<div class="modal-dialog modal-lg fetchdata-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><b id="invoice-export-heading">Insert Hearings</b></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" style="padding: 0px;" id="modal-data">

			</div>
      <div class="modal-footer">       
        <input type="button" class="btn btn-info newsubmit btn-sm" value="Insert"  name="submit" onclick="import_hearing()"> 
        <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>
      </div>

    </div>

  </div>
</div>
<div class="modal" id="newClientModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><b>Add Client</b></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="container">
          <div class="col-sm-12">
            <label>Full Name <span style="color:red">*</span></label>
            <input type="text" class="form-control" id="new-client-name" name="">
          </div>
          <div class="col-sm-12">
            <label>Email </label>
            <input type="text" class="form-control" id="new-client-email" name="">
          </div>
          <div class="col-sm-12">
            <label>Mobile </label>
            <input type="text" class="form-control" id="new-client-mobile" name="">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-info btn-mini" type="button">Submit</button>
        <button class="btn btn-danger btn-mini" type="button" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php include ('footer.php'); ?>
<script type="text/javascript" src="scripts/create-invoice.js"></script>