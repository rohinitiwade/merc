<?php
include('includes/dbconnect.php');
session_start();
error_reporting(0);
$arr               = array();
$date              = date('Y-m-d H:i:s');
// $update_autostatus = mysqli_query($connection, "UPDATE `reg_cases` SET `auto_update`='" . $_POST['autoupdate'] . "' WHERE `case_id`='" . $_POST['case_id'] . "' ");
$view_case         = mysqli_query($connection, "SELECT `client_name`,`type_of_work`,`case_description`,`case_id`,`user_id` FROM reg_cases WHERE `remove_status`=1 AND `case_id`='" . $_POST['case_id'] . "'");

$count_data        = mysqli_num_rows($view_case);
if ($count_data == 0) {
    $arr = array(
        'status' => 'error'
    );
}
while ($view_cases = mysqli_fetch_assoc($view_case)) {
	 $view_data_object                                  = new stdClass();
    $view_data_object->case_id                         = TRIM($view_cases['case_id']);
    $view_data_object->user_id                         = TRIM($view_cases['user_id']);
    $view_data_object->client_name                        = TRIM($view_cases['client_name']);
    $view_data_object->type_of_work                      = TRIM($view_cases['type_of_work']);
    $view_data_object->case_title                = TRIM($view_cases['case_description']);
     $case_details[]                                    = $view_data_object;

     $reg             = mysqli_query($connection, "SELECT CONCAT(name, ' ', last_name) AS user_name,gender,image FROM `law_registration` WHERE `reg_id`='" . $view_cases['user_id'] . "'");
    $regs            = mysqli_fetch_array($reg);
    
    if ($view_cases['date_time'] != '') {
        $created_date = date('d-m-Y', strtotime($view_cases['date_time']));
        $created_time = date('H:i', strtotime($view_cases['date_time']));
    }
$user_image = $regs['image'];

    $team = mysqli_query($connection, "SELECT tt.user_id,tt.case_id,tt.assign_to,tt.case_no FROM todo_team tt  WHERE  tt.case_id='" . $_POST['case_id'] . "' ORDER BY toteam_id DESC");
    $teamcount = mysqli_num_rows($team);
while ($teams = mysqli_fetch_array($team)) {
    $law          = mysqli_query($connection, "SELECT `name`,`last_name`,`email`,`mobile`,`designation`,`state`,`district`,`city`,`gender`,`address` FROM `law_registration` WHERE `reg_id`='" . $teams['assign_to'] . "'");
    $laws         = mysqli_fetch_array($law);
    // $full_name  = $teams['name'].' '.$teams['last_name'];
    $teams_data[] = array(
        'case_id' => $teams['case_id'],
        'user_id' => $teams['user_id'],
        'first_name' => $laws['name'],
        'last_name' => $laws['last_name'],
        'email' => $laws['email'],
        'mobile' => $laws['mobile'],
        'address' => $laws['address'],
        'designation' => $laws['designation'],
        'state' => $laws['state'],
        'district' => $laws['district'],
        'city' => $laws['city'],
        'gender' => $laws['gender'],
        'case_no' => $teams['case_no'],
        'team_id' => $teams['assign_to']
    );
}

$document  = mysqli_query($connection, "SELECT count(case_id) AS total_count FROM `add_document` WHERE `case_id`='" . $_POST['case_id'] . "' AND `remove_status`=1");
$count_doc = mysqli_fetch_array($document);
}

$arr        = array(
    'status' => 'success',
    'data' => $case_details,
    'user_name' => $regs['user_name'],
    'created_date' => $created_date,
    'created_time' => $created_time,  
    'team_data' => $teams_data,
    'document_count' => $count_doc['total_count'],
    'team_count' => $teamcount,
    'user_image' => $user_image,
);
echo json_encode($arr, true);
?>