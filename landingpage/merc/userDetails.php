<?php include("header.php");?>
<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<?php include("menu.php");?>
<style>
* {
  box-sizing: border-box;
}

.heading {
  font-size: 25px;
  margin-right: 25px;
}

.fa {
  font-size: 25px;
}

.checked {
  color: orange;
}

/* Three column layout */
.side {
  float: left;
  width: 15%;
  margin-top:10px;
}

.middle {
  margin-top:10px;
  float: left;
  width: 70%;
}

/* Place text to the right */
.right {
  text-align: right;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* The bar container */
.bar-container {
  width: 100%;
  background-color: #f1f1f1;
  text-align: center;
  color: white;
}

/* Individual bars */
.bar-5 {width: 60%; height: 18px; background-color: #4CAF50;}
.bar-4 {width: 30%; height: 18px; background-color: #2196F3;}
.bar-3 {width: 10%; height: 18px; background-color: #00bcd4;}
.bar-2 {width: 4%; height: 18px; background-color: #ff9800;}
.bar-1 {width: 15%; height: 18px; background-color: #f44336;}

/* Responsive layout - make the columns stack on top of each other instead of next to each other */
@media (max-width: 400px) {
  .side, .middle {
    width: 100%;
  }
  .right {
    display: none;
  }
}
</style>
<style type="text/css">
  *{
    margin: 0;
    padding: 0;
}
.rate {
    float: left;
    height: 46px;
    padding: 0 10px;
}
.rate:not(:checked) > input {
    display: none;
}
.rate:not(:checked) > label {
    float:right;
    width:1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:30px;
    color:#ccc;
}
.rate:not(:checked) > label:before {
    content: '★ ';
}
.rate > input:checked ~ label {
    color: #ffc700;    
}
.rate:not(:checked) > label:hover,
.rate:not(:checked) > label:hover ~ label {
    color: #deb217;  
}
.rate > input:checked + label:hover,
.rate > input:checked + label:hover ~ label,
.rate > input:checked ~ label:hover,
.rate > input:checked ~ label:hover ~ label,
.rate > label:hover ~ input:checked ~ label {
    color: #c59b08;
}
</style>
<div class="main-panel">
  	<div class="content-wrapper">
    <?php include("slider.php");?>
    <?php $userprofile = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='".base64_decode($_GET['id'])."'");
          $selprofile = mysqli_fetch_array($userprofile);?>
    	<div class="card">
      	<div class="card-body">
        	<h4 class="card-header" style="padding-top: 0px">User Profile</h4>
          <div class="row form-group">
            <div class=" col-sm-6 row" style="padding: 10px;align-content: center;">
              <div class="form-group col-sm-3">
                <span><b>NAME:</b></span><br>
                <span><b>ADDRESS:</b></span><br>
                <span><b>CITY:</b></span><br>
                <span><b>MOBILE NO:</b></span><br>
                <span><b>EMAIL ID:</b></span><br>
                <span><b>DESIGNATION:</b></span><br>
              </div>
              <div class="form-group col-sm-4">
                <span><?php echo  $selprofile['name'];?> <?php echo  $selprofile['last_name'];?></span><br>
                <span><?php echo  $selprofile['address'];?></span><br>
                <span><?php echo  $selprofile['city'];?></span><br>
                <span><?php echo  $selprofile['mobile'];?></span><br>
                <span><?php echo  $selprofile['email'];?></span><br>
                <span><?php echo  $selprofile['designation'];?></span><br>
              </div>
            </div>
            <div class="col-sm-6" style="padding: 10px;text-align: center;">
              <span style="font-size: 17px;"><b>PROFILE PICTURE</b></span><br>
              <?php if($selprofile['gender']=="Male" AND $selprofile['image']==""){?>
              <img src="images/male.png" class="img-responsive" alt="" style="width: 200px;height: 200px;border-radius: 100px;filter: drop-shadow(1px 2px 8px black);">
              <?php }elseif($selprofile['gender']=="Female" AND $selprofile['image']==""){ ?>
              <img src="images/female.jpg" class="img-responsive" alt="" style="width: 200px;height: 200px;border-radius: 100px;filter: drop-shadow(1px 2px 8px black);">
              <?php }else{ ?>
               <img src="uploads_profile/<?php echo $selprofile['image'];?>" class="img-responsive" alt="" style="width: 200px;height: 200px;border-radius: 100px;filter: drop-shadow(1px 2px 8px black);">
              <?php } ?>
             
            </div>
          </div>
      	</div>
        <h4 class="card-header" style="padding-top: 0px"><b>Last Login Details</b></h4>
        <div class="row" style="margin: 20px;">
          <div class="col-12">
              <div class="table-responsive">
                  <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th style="background-color: #353eb3; color: white;">Last Login</th>
                            <th style="background-color: #353eb3; color: white;">Last Time</th>
                            <th style="background-color: #353eb3; color: white;">Login Details</th>
                            <th style="background-color: #353eb3; color: white;">Case Details</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php  $login= mysqli_query($connection, "SELECT `login_date`,`login_time`,`log_id`,`user_id` FROM `login_history` WHERE `user_id`='".base64_decode($_GET['id'])."' ORDER BY log_id DESC LIMIT 1");
                              while($sellogin = mysqli_fetch_array($login)){?>
                        <tr>
                            <td style="padding: 10px;"><?php  echo $sellogin['login_date'];?></td>
                            <td style="padding: 10px;"><?php  echo $sellogin['login_time'];?></td>
                            <td style="padding: 10px;text-align: center;"><button style="background-color: #2d4866;color: white;padding: 4px 11px;border: 0px;" data-target="#login" data-toggle="modal" onclick="viewlogin(<?php echo base64_decode($_GET['id']);?>)">View All</button></td>
                            <td style="padding: 10px;text-align: center;"><button style="background-color: #2d4866;color: white;padding: 4px 11px;border: 0px;" data-target="#case" data-toggle="modal">View All</button></td>
                        </tr>
                        <?php  } ?> 
                      </tbody>
                  </table>
                  <tr>
                      <td colspan="10">
                      </td>
                  </tr>
              </div>
          </div>
        </div>
        <form name="Myform" method="POST">
        <div class="card-body">
          <h4 class="card-header" style="padding-top: 0px">User Comment</h4>
          <div class="row form-group">
            <div class=" col-sm-6" style="padding: 10px;">
              <div class="row form-group">
                <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Comment</b></label>
                <div class="col-sm-8">
                  <textarea rows="4" cols="50" placeholder="Enter Comment for <?php echo  $selprofile['name'];?> <?php echo  $selprofile['last_name'];?>"></textarea>
                </div>
              </div>
            </div>
            <div class="col-sm-6" style="padding: 10px;text-align: center;">
              <label for="exampleInputUsername1" class="col-sm-3 col-form-label" style="float: left;"><b>Rate</b></label>
              <div class="rate" style="padding: 10px;">
                <input type="radio" id="star5" name="rate" value="5" />
                <label for="star5" title="text">5 stars</label>
                <input type="radio" id="star4" name="rate" value="4" />
                <label for="star4" title="text">4 stars</label>
                <input type="radio" id="star3" name="rate" value="3" />
                <label for="star3" title="text">3 stars</label>
                <input type="radio" id="star2" name="rate" value="2" />
                <label for="star2" title="text">2 stars</label>
                <input type="radio" id="star1" name="rate" value="1" />
                <label for="star1" title="text">1 star</label>
              </div>
            </div>
            <div style="text-align: center;width: 100%;">
              <input type="submit" style="background-color: #2d4866;color: white;padding: 4px 11px;border: 0px;" value="Submit">
            </div>
          </div>
        </div>
      </form>
        <div class="card-body">
          <span class="heading">User Rating</span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star"></span>
          <p>4.1 average based on 254 reviews.</p>
          <hr style="border:3px solid #f1f1f1">
          
          <div class="row">
            <div class="side">
              <div>5 star</div>
            </div>
            <div class="middle">
              <div class="bar-container">
                <div class="bar-5"></div>
              </div>
            </div>
            <div class="side right">
              <div>150</div>
            </div>
            <div class="side">
              <div>4 star</div>
            </div>
            <div class="middle">
              <div class="bar-container">
                <div class="bar-4"></div>
              </div>
            </div>
            <div class="side right">
              <div>63</div>
            </div>
            <div class="side">
              <div>3 star</div>
            </div>
            <div class="middle">
              <div class="bar-container">
                <div class="bar-3"></div>
              </div>
            </div>
            <div class="side right">
              <div>15</div>
            </div>
            <div class="side">
              <div>2 star</div>
            </div>
            <div class="middle">
              <div class="bar-container">
                <div class="bar-2"></div>
              </div>
            </div>
            <div class="side right">
              <div>6</div>
            </div>
            <div class="side">
              <div>1 star</div>
            </div>
            <div class="middle">
              <div class="bar-container">
                <div class="bar-1"></div>
              </div>
            </div>
            <div class="side right">
              <div>20</div>
            </div>
          </div>
        </div>
        <h4 class="card-header" style="padding-top: 0px">User Comment List</h4>
        <div class="row" style="margin: 20px;">
          <div class="col-12">
              <div class="table-responsive">
                  <table id="example" class="table">
                      <thead>
                        <tr>
                            <th style="background-color: #353eb3; color: white;">Sr.No.</th>
                            <th style="background-color: #353eb3; color: white;">Comment</th>
                            <th style="background-color: #353eb3; color: white;">Comment Date</th>
                            <th style="background-color: #353eb3; color: white;">Rating</th>
                            <th style="background-color: #353eb3; color: white;">Comment & Rating By</th>
                            <th style="background-color: #353eb3; color: white;">Edit Comment</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php  $team_comment= mysqli_query($connection, "SELECT * FROM `team_comment` WHERE `user_id`='".base64_decode($_GET['id'])."' ORDER BY id DESC");
                        $i=1;
                              while($sellogin = mysqli_fetch_array($team_comment)){?>
                        <tr>
                            <td style="padding: 10px;"><?php  echo $i;?></td>
                            <td style="padding: 10px;"><?php  echo $sellogin['comment'];?></td>
                            <td style="padding: 10px;"><?php  echo $sellogin['date_time'];?></td>
                            <td style="padding: 10px;"><?php  echo $sellogin['rating'];?></td>
                            <td style="padding: 10px;"><?php  $commentby = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='".$sellogin['commented_by']."'"); $comsel = mysqli_fetch_array($commentby); echo $comsel['name']; echo "&nbsp"; echo $comsel['last_name'];?></td>
                            <td style="padding: 10px;text-align: center;"><button style="background-color: #2d4866;color: white;padding: 4px 11px;border: 0px;" data-target="#edicomment" data-toggle="modal" onclick="editcomment(<?php echo $sellogin['id'];?>)">Edit Comment and Rating</button></td>
                        </tr>
                        <?php $i++; } ?> 
                      </tbody>
                  </table>
                  <tr>
                      <td colspan="10">
                      </td>
                  </tr>
              </div>
          </div>
        </div>
    	</div>
  	</div>
    <div class="modal" id="login">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-left: -185px;width: 180%;">
            <div class="modal-body">
              <div class="row" style="margin: 20px;">
                <div class="col-12">
                  <div class="table-responsive">
                     
                       <div class="box-content"></div>
                      <tr>
                          <td colspan="10">
                          </td>
                      </tr>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>
  <div class="modal" id="case">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-left: -185px;width: 180%;">
            <div class="modal-header" style="background-color: #2d4866;color: white;">
              <h4>Cases</h4>
              <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row" style="margin: 20px;">
                <div class="col-12">
                  <div class="table-responsive">
                      <table id="order-listing" class="table">
                          <thead>
                            <tr>
                                <th style="background-color: #353eb3; color: white;">#</th>
                                <th style="background-color: #353eb3; color: white;">Court</th>
                                <th style="background-color: #353eb3; color: white;">Case</th>
                                <th style="background-color: #353eb3; color: white;">Title</th>
                                <th style="background-color: #353eb3; color: white;">Team Member(s)</th>
                                <th style="background-color: #353eb3; color: white;">Hearing Date</th>
                                <th style="background-color: #353eb3; color: white;">Priority</th>
                                <th style="background-color: #353eb3; color: white;">Case Discription</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 10px;"></td>
                                <td style="padding: 10px;"></td>
                            </tr> 
                          </tbody>
                      </table>
                      <tr>
                          <td colspan="10">
                          </td>
                      </tr>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
  </div>

  <script>

    function viewlogin (id) {
        var id = +id;
          $.ajax({
              type:'POST',
              url:'ajaxdata/loginhistory.php',
              data:'ids='+id,
              success:function(html){
                $('.box-content').html(html);
              }
            }); 
      }
       function viewcases (id) {
        var id = +id;
          $.ajax({
              type:'POST',
              url:'ajaxdata/teamcases.php',
              data:'ids='+id,
              success:function(html){
                $('#casedetails').html(html);
              }
            }); 
      }

       function editcase (id) {
        var id = +id;
          $.ajax({
              type:'POST',
              url:'ajaxdata/editcase.php',
              data:'ids='+id,
              success:function(html){
                $('#casedetailss').html(html);
              }
            }); 
      }

       function editcomment (id) {
        var id = +id;
        alert(id);
          $.ajax({
              type:'POST',
              url:'ajaxdata/editcomment.php',
              data:'ids='+id,
              success:function(html){
                $('#editcom').html(html);
              }
            }); 
      }
    </script>

   
    <script>
      $(document).ready(function() {
    $('#example').DataTable();
} );
    </script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php include("footer.php");?>

 <div class="modal" id="edicomment">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

    <div id="editcom"></div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>