<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->

<style type="text/css">
   .chat-img{
      padding: 5px 0px 0px 5px;
   }
   .replyheading{
      color: green; font-size: 23px;
      text-align: center;
      font-weight: bold;
      padding-top: 33px;
   }
   .replybody{
      padding-bottom: 41px;
   }
   .query{
      font-size: 15px;
      font-weight: bold;
      color: teal;
      float: left;
      width: auto;
      padding-left: 0;
   }
   .chat_div .col-sm-12 span{
      font-size: 15px;
    color: #4e4949;
    padding: 0;
    float: left;
   }
   .chat_div{
      border:1px solid lightgray;
   }
   .view_reply{
      /*margin-bottom: 4%;*/
      font-size: 14px;
      color: green; 
      /*padding-left: 19px;*/
   }
   .view_reply:hover{
      color: green; 
   }
   .reply_show{
      float: left;
      /*border: 1px solid #ced4da;*/
      padding: 0px 1%;
      border-radius: .25rem;
      color: #495057;
      font-size: 14px;
      /*padding-bottom: 4%;*/
      display: none;
      /*border: 1px solid #ced4da;*/
      /*margin-bottom: 4%;*/
      padding-left: 0;
      margin-top: 1%;
      text-align: justify;
   }
   #reply{
      border: 1px solid #ced4da;
      
   }
   .view::before{
      content:'View Answer';
   }
   .hide::before{
      content: "Hide Answer";
   }
   #pagen{
      width: 20%;
    margin: 0 auto;
   }
</style>

<script>
   $(document).ready(function(){
      getallquery(1);


   });

   

   function getallquery(pageno){
      debugger;
      var user = $("#email").val();
      // var user= 'rameshwar.air@gmail.com';
      /*var pageno=1;*/
      var QueVo=new Object();
      QueVo.user=user;
      QueVo.pageNo=pageno;

      $.ajax({ 
         url: legalResearch+"/alluserquery",
         type:'POST',
         data: JSON.stringify(QueVo),
         dataType: 'json',
         async:false,
         headers: {
            "Content-Type": 'application/json'
         },
         success:function(data){            
            console.log(data);
            
                     // if(data){
                     //    $("#queryshow").val(data.caseTitleid);
                     // }
                     var pagination = '<div class="panel-footer "><ul class="pagination pagination-sm pull-right pagination-separate pagination-round pagination-flat pageNo"><li id="page_link_prev" class="page-item"><a class="page-link" href="#">Prev</a></li>';
                     for(var i=1; i<=data.numPages;i++){
                        pagination += '<li id="page_link_'+i+'" class="page-item activepageno"><a class="page-link" onclick="getallquery('+i+')">'+i+'</a></li>';
                     }

                     pagination += '<li id="page_link_next" class="page-item"><a class="page-link" href="#">Next</a></li></ul></div>';
                     $("#pagen").html(pagination);

                     var getquey= data.userOutBoxVos;
                     var queyd = '';
                     $.each(getquey, function(key,val) {
                        queyd += '<div class="chat_div form-group"><div class="col-sm-12" style="text-align: justify;"><div class="col-sm-1" style="text-align: justify;"><label class=" query">Que '+val.qid+'. </label></div><div class="col-sm-11"><span>'+val.query+'</span></div></div><div class="reply_show col-sm-12" id="reply'+val.qid+'"></div><div class="col-sm-12" style="width: 89%;margin: 0 auto;"><div class="col-sm-12 chat_img" >&nbsp; &nbsp;<img src="http://crm.airinfotech.in/law/images/explanation.png" class="bookimg"><span><a href="javascript:void(0)" id="showreply'+val.qid+'" class="view_reply view" style="float: left;" onclick="getans('+val.qid+')"></a></span></div></div></div>';
                        
                     }); 
                     $("#queryshow").html(queyd);

                     assign_active_pagination(pageno);
                  },
                  error:function(e){
            //console.log(e);
         }
      });
   }

   function assign_active_pagination(pageNo){
      $("#page_link_"+pageNo).addClass('active');
   }

   $(window).ready(function(){
      $("#page_link_prev").on("click",function(){
         var pageNo = $(".activepageno.active a").text();
         var prev_page = parseInt(pageNo)-1;
         getallquery(prev_page);
      });

      $("#page_link_next").on("click",function(){
         var pageNo = $(".activepageno.active a").text();
         var next_page = parseInt(pageNo)+1;
         getallquery(next_page);
      });
   });


   var oldqid='';
   function getans(qid){
       // debugger;
   // alert(qid);
   
   var AnsVo=new Object();
   AnsVo.qid=qid;
   var htmlData="";

   $.ajax({ 
      url: legalResearch+"/answers",
      type:'POST',
      data: JSON.stringify(AnsVo),
      dataType: 'json',
      async:false,
      headers: {
         "Content-Type": 'application/json'
      },
      success:function(data){ 
         if(qid == oldqid){
            if ($("#showreply"+qid).hasClass("view")) 
               $("#showreply"+qid).removeClass('view').addClass('hide');
            else
               $("#showreply"+qid).removeClass('hide').addClass('view');
            $("#reply"+qid).toggle();
         }
         else{
            if(data.questionAnswer!=null){
               htmlData='<div class="col-sm-12"><label class="col-sm-2 query">Ans: </label>';
               var researcherDocVos =data.researcherDocVos;
            //$.each(researcherDocVos, function(i,v){
               htmlData+='<span style="font-weight: bold;padding-left: 15px;" class="col-sm-10">'+data.questionAnswer+'</span></div>';//+"<ul><li><a href="+v.path+">"+v.docs+"</a></li></ul>
               
            //});
            $("#reply"+qid).html(htmlData);

            $("#reply"+qid).show();
            // $("#showreply"+qid).text('Hide Answer');
            $("#showreply"+qid).removeClass('view').addClass('hide');
            oldqid = qid;

         }else{
            if(qid == oldqid){
               if ($("#showreply"+qid).hasClass("view")) 
                  $("#showreply"+qid).removeClass('view').addClass('hide');
               else
                  $("#showreply"+qid).removeClass('hide').addClass('view');
               $("#reply"+qid).toggle();
            }else{
               var ans = '<div class="col-sm-12"><label class="col-sm-2 query">Ans: </label><span style="font-weight: bold;padding-left: 15px;">Waiting For Response</span></span>';
               $("#reply"+qid).html(ans);
               $("#reply"+qid).show();
               $("#showreply"+qid).removeClass('view').addClass('hide');
               oldqid = qid;
            }
         }
      }
   },
   error:function(e){
            //console.log(e);
         }
      });
}
</script>





 <body > 
      <?php include("menu.php");?>
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="">
            <div class="card-body">
                          <h4>Case Legal Research Response</h4>
              <hr/>
               <div class="bread_crumbs" style="text-align: center;"> 
 <!--      <span class="replyheading">Case Legal Research Response</span>  -->
   </div>

   <div class="container" style="margin-bottom: 4%;">
      <div class="row">
         <div class="col-sm-12">
            <div class="panel panel-primary">
               <div class="">
                  <div class="panel-body">
                     <ul class="chat" style="font-size: 14px;">
                        <li class="left "><span class="chat-img pull-left" style="float: left;">
                           <!-- <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" /> -->
                        </span>
                        <div class="chat-body ">
                          <?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                                $row = mysqli_fetch_array($fetch);?>
                           <input type="hidden" id="email" value="<?php echo base64_decode($_GET['caseid']);?>">
                           <div class="header">
                              <small class="pull-right text-muted" style="width: 100%; float: left;"> </small>
                              <!-- <span class="glyphicon glyphicon-time"></span>12 mins ago</small> -->
                              <!-- <strong class="primary-font" style="color: green;">Name Of Matter:</strong> <span>Contract Labour</span><br> -->
                              <!-- <b> Document Type :</b><span>Notice</span><br> -->


                              <div id="queryshow"></div>


                           </div>
                        </div>
                     </li>
                  </ul>
               </div>

            </div>

         </div>
         <div id="pagen"></div>

      </div>


   </div>
</div>
              </div>
            </div>
            <?php include("footer.php");?>
        </div>

