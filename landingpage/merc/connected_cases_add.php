    <div class="media">
      <div class="media-body col-sm-12">
        <h4>Related to the existing cases</h4><br>
        <div class="col-sm-12 row" style="display: flex;align-items: center;">
          <div class="col-sm-5">
            <label class="col-form-label"><b>Case Title</b><span style="color: red;">*</span></label>
            <select name="case_title" id="case_title" class="form-control"></select>   
          </div>
          <div class="col-sm-1" style="text-align: center;">
            <p style="font-size: 0.85rem"><b>is:</b></p>
          </div>
          <div class="col-sm-4">
            <label class="col-form-label"><b>Relate</b></label>            
            <select name="connect" id="connect" class="form-control"></select>
          </div>
          <div class="col-sm-2">
            <br><button class="btn btn-info btn-sm" type="button" onclick="uploadConnectedCases('click')"><b>Connect</b></button>
          </div>
        </div>
        <hr>
        <div class="col-sm-12">
          <div class="table-responsive" id="connect_cases_table">
          </div>
        </div>
      </div>
    </div>