<?php session_start();
include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<link rel="stylesheet" type="text/css" href="styles/tree_style.min.css">
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			 
			<?php include("menu.php") ?>
			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title">Consolidated Legal Audit Report</h5>
							</div>
							<div class="page-body">
								<div class="form-group">
									<div class="card col-sm-12 ">
										<div class="card-block row">
											 <input type="hidden" name="oragnisation_id" id="oragnisation_id" value="<?php  echo  $_SESSION['organisation_id']?>">
											<div class="col-sm-12">
												<ul class="nav nav-tabs form-group" role="tablist">
													<li class="nav-item">
														<a class="nav-link active" id="case-details-tab" data-toggle="tab" href="#activity-case-details" role="tab" aria-controls="home-1" aria-selected="true" onclick="getAllCategory('all',<?php echo  $_SESSION['organisation_id']?>,5);" >All</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="case-details-tab" data-toggle="tab" href="#court-wise" role="tab" aria-controls="home-1" aria-selected="true" onclick="byRiskCategory(<?php echo  $_SESSION['organisation_id']?>,2);" >By Court</a>
													</li>
													<!-- <li class="nav-item">
														<a class="nav-link" id="case-details-tab" data-toggle="tab" href="#topic-wise" role="tab" aria-controls="home-1" aria-selected="true" onclick="byRiskCategory(<?php echo  $_SESSION['organisation_id']?>,3);" > By Topic</a>
													</li> -->
													<li class="nav-item">
														<a class="nav-link" id="case-details-tab" data-toggle="tab" href="#riskcategory-wise" role="tab" aria-controls="home-1" aria-selected="true"  onclick="byRiskCategory(<?php echo  $_SESSION['organisation_id']?>,1);">  By Risk Category</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="case-details-tab" data-toggle="tab" href="#adr-wise" role="tab" aria-controls="home-1" aria-selected="true"  onclick="byRiskCategory(<?php echo  $_SESSION['organisation_id']?>,3);">By Recommended for ADR</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" id="case-details-tab" data-toggle="tab" href="#contempt-wise" role="tab" aria-controls="home-1" aria-selected="true" onclick="byRiskCategory(<?php echo  $_SESSION['organisation_id']?>,4);" > Cases in Contempt</a>
													</li>

												</ul>
												<div class="tab-content">

													<div class="tab-pane fade active show" id="activity-case-details">


														<div class="table-responsive judicialtable">
															<div class="col-sm-12">
																<div id="alltree-description" class="col-sm-12 table-reponsive">
														<div id="pagination_all" class="pagination"></div>
													
															<!-- <table class="table table-bordered">
																<thead class="bg-primary">
																	<tr>

																		<th class="court-report">AIR Legal No.</th>
																		<th class="court-report">Case No. </th>
																		<th class="case-report">Legal Report</th> 

																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td  ></td>
																		<td  ></td>
																		<td  ></td>  
																		
													                              
																	</tr> 
																</tbody>
															</table> -->
																<!-- <div id="lazy1" class="demo col-sm-5 p-0"></div> -->
														</div>
														<!-- <div class="row">
														<div id="lazy" class="demo col-sm-5 p-0"></div>
														<div id="tree-description" class="col-sm-7 table-reponsive"></div> 
														</div> -->
													</div>
												</div>
</div>
													<div class="tab-pane fade" id="court-wise">
														<div class="table-responsive judicialtable">
															 <div class="col-sm-12 row">
														<div id="court-category" class="demo col-sm-5 p-0"></div>

														<div id="courttree-description" class="col-sm-7 table-reponsive">
															 
														</div> 
														<div class="col-sm-12">
														<div id="pagination_court" class="pagination"></div></div>
														</div>
														</div>
													</div>

													<div class="tab-pane fade" id="topic-wise">
														<div class="table-responsive judicialtable">
															 <div class="col-sm-12 row">
														<div id="topic-category" class="demo col-sm-5 p-0"></div>

														<div id="topictree-description" class="col-sm-7 table-reponsive">
															 
														</div> 
														<div class="col-sm-12">
														<div id="pagination_topic" class="pagination"></div></div>
														</div>
														</div>
														<!-- <div class="table-responsive judicialtable">
															 <div class="col-sm-12 row">
														<div id="lazy" class="demo col-sm-5 p-0"></div>
														<div id="tree-description" class="col-sm-7 table-reponsive"></div> 
														</div>
														</div> -->
													</div>

													<div class="tab-pane fade" id="riskcategory-wise">
														<div class="table-responsive judicialtable">
															
															 <div class="col-sm-12 row">
														<div id="risk-category" class="demo col-sm-5 p-0"></div>

														<div id="risktree-description" class="col-sm-7 table-reponsive">
															 
														</div> 
														<div class="col-sm-12">
														<div id="pagination_risk" class="pagination"></div></div>
														</div>
														</div>
													</div>

													<div class="tab-pane fade" id="adr-wise">
														<div class="table-responsive judicialtable">
															
															 <div class="col-sm-12 row">
														<div id="adr-category" class="demo col-sm-5 p-0"></div>

														<div id="adrtree-description" class="col-sm-7 table-reponsive">
															 
														</div> 
														<div class="col-sm-12">
														<div id="pagination_adr" class="pagination"></div></div>
														</div>
														</div>
														<!-- <div class="table-responsive judicialtable">
															<table class="table table-bordered">
																<thead class="bg-primary">
																	<tr>
																		
																		<th class="court-report"  >AIR Legal No.</th>
																		<th class="court-report">Case No. </th>
																		<th class="case-report">Legal Report</th> 
																		
																	</tr>
																</thead>
																<tbody>
																	<tr  >
																		<td  ></td>
																		<td  ></td>
																		<td  ></td>                                 
																	</tr> 
																</tbody>
															</table>
														</div> -->
													</div>

													<div class="tab-pane fade" id="contempt-wise">
														<div class="table-responsive judicialtable">
															
															 <div class="col-sm-12 row">
														<div id="contempt-category" class="demo col-sm-5 p-0"></div>

														<div id="contempttree-description" class="col-sm-7 table-reponsive">
															 
														</div> 
														<div class="col-sm-12">
														<div id="pagination_contempt" class="pagination"></div></div>
														</div>
														</div>
														<!-- <div class="table-responsive judicialtable">
															<table class="table table-bordered">
																<thead class="bg-primary">
																	<tr>
																		
																		<th class="court-report"  >AIR Legal No.</th>
																		<th class="court-report">Case No. </th>
																		<th class="case-report">Legal Report</th> 
																		
																	</tr>
																</thead>
																<tbody>
																	<tr  >
																		<td  ></td>
																		<td  ></td>
																		<td  ></td>                                 
																	</tr> 
																</tbody>
															</table>
														</div> -->
													</div>
													
												</div>
											</div>
											<div id="tree-modal" class="modal fade" role="dialog">
												<div class="modal-dialog" style="max-width: 928px;">
													<div class="modal-content">
														<div class="" style="background-color: white;">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title" style="color: #f10038;
															text-align: center;">Legal Audit Report</h4>

														</div>
														<hr>
														<div class="modal-body">
              <!-- <div class="col-sm-12">
                <button class="btn btn-primary btn-sm" type="button" onclick="printLegalAudit()">Print</button>
                <button class="btn btn-warning btn-sm" type="button" onclick="exportLegalAudit()">Export</button>
            </div> -->
            <div id="legal_data"></div>
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>


</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php include 'footer.php'; ?>
<script type="text/javascript">
	$(".add-client-li").addClass('active pcoded-trigger');
</script>

<script src="scripts/jstree.min.js"></script>
<script type="text/javascript" src="scripts/tree-dashboard.js"></script>







