 <?php session_start();
 include("includes/dbconnect.php");?>
<form name="Myform" method="POST" action="edit_submit.php?ids=169">
  <!-- Modal Header -->
  <div class="modal-header">
    <h4 class="modal-title">Profile Details</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>

  <!-- Modal body -->
  <div class="modal-body">



   <div class="col-sm-12 form-group">
    <div class="card">
      <!-- <div class="card-header"><h6>Edit Data of   </h6></div> -->
      <div class="row">
       <div class="petitioner col-sm-6" style="">
        <table class="table table-bordered table-striped  case-sidebar-section export_table">
          <thead>
            <tr>
              <th class="" colspan="2">
                <span class="section-header full-width">Petitioner</span>
                <div class="clear"></div>
              </th>
            </tr>
          </thead>
          <tbody>
            <?php $pet = mysqli_query($connection, "SELECT * FROM `petitioner_respondent` WHERE `case_id`='".$_POST['caseid']."' AND `designation`='Petitioner' AND `organisation_id`='".$_SESSION['organisation_id']."'");
                    while($petitioner = mysqli_fetch_array($pet)){ ?>
            <tr>
              <td class=""><p><?php echo $petitioner['full_name'];?></p></td>
            </tr>
           <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="petitioner col-sm-6" style="">
        <table class="table table-bordered table-striped  case-sidebar-section export_table">
          <thead>
            <tr>
              <th class="" colspan="2">
                <span class="section-header full-width"> Respondent</span>
                <div class="clear"></div>
              </th>
            </tr>
          </thead>
          <tbody>
           <?php $res = mysqli_query($connection, "SELECT * FROM `petitioner_respondent` WHERE `case_id`='".$_POST['caseid']."' AND `designation`='Respondent' AND `organisation_id`='".$_SESSION['organisation_id']."'");
                    while($respondent = mysqli_fetch_array($res)){ ?>
            <tr>
              <td class=""><p><?php echo $respondent['full_name'];?></p></td>
            </tr>
           <?php } ?>
          </tbody>
        </table>
      </div>
     </div>

   </div>
 </div>
</div>

<!-- Modal footer -->
<!-- <div class="modal-footer">
  <input type="submit" value="Submit" name="submit" class="btn-sm btn-primary btn pull-right">
  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
</div> -->
</form>
<style>
  .col-form-label {
    font-size: 13px;
  }
</style>