<?php 
include('includes/dbconnect.php');
$arr = array();
if($_POST['flag'] == 'case_law'){
	// $productlist = file_get_contents($qtool.'opi/users/getProductResources');
	$productlist = '';
	
	$productapi = $qtool.'opi/users/getProductResources';
	productList($productapi);
	$method= 'GET';
	$url = $qtool.'api/qtool/webservices/v2/searchAll';
	 $data = $_POST['data'];
	 
	$crl = addslashes($_POST['token']);
	$result = '';
	CallApiSearchall($method, $url, $data, $crl);
	$arr = array('product' => json_decode($productlist),
		'searchAll' => json_decode($result));
	// $arr =json_decode($legal_support);
}

if($_POST['flag'] == 'login'){
	// $productlist = file_get_contents($qtool.'opi/users/getProductResources');
	

	$method= 'POST';
	$url = $qtool.'opi/users/login';
	// $data = JSON_decode($_POST['qtooldata']);
	// $data = json_decode(file_get_contents('php://input'), true);
	// $param = $_POST['qtooldata']; 	
	$data = $_POST['params'];
	$login = '';
	CallApiLogin($method, $url, $data);
	$arr = json_decode($login);
}


if($_POST['flag'] == 'view_response'){
	$method= 'GET';
	$url = $legalResearch.'showLegalAudit';
	$data = $_POST['params'];	
	$view_response = '';
	viewResponseCLR($method, $url, $data);
	$arr = json_decode($view_response);
}
if($_POST['flag'] == 'legal_support'){
	$method= 'POST';
	  $url = $legalResearch.'addprocessdata';
	$data = $_POST['params'];	
	// print_r($data);
	$legal_support = '';
	legalSupportToCLR($method, $url, $data);
	$arr =json_decode($legal_support);
}

if($_POST['flag'] == 'risk_tree'){
	$method= 'GET';
	  $url = $legalResearch.'get_Cases_List';
	$data = $_POST['params'];	
	// print_r($data);
	$risk_tree_get = '';
	riskCategoryList($method, $url, $data);
	$arr =json_decode($risk_tree_get);
}

if($_GET['flag'] == 'topic'){
	$method= 'GET';
	  $url = $legalResearch.'get_riskCategory_List';
	$datas = $_GET['organizationId'];
// $data= "organizationId:".$datas."";
$arrs = array();
$arrs = array('organizationId' => $datas,'task' => $_GET['task']);
	// print_r($_POST['organizationId']);
	$getriskcategory = '';
	riskcategory($method, $url, $arrs);
	$arr =json_decode($getriskcategory);
}


function productList($productapi){
	global $productlist;
	$productlist = file_get_contents($productapi);
	return $productlist;
}

function CallApiLogin($method, $url, $data){
	// print_r($data);
	// global $login;
	// $curl = curl_init();

	// switch ($method)
	// {
	// 	case "POST":
	// 	curl_setopt($curl, CURLOPT_POST, 1);

	// 	if ($data)
	// 		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	// 	break;
	// 	case "PUT":
	// 	curl_setopt($curl, CURLOPT_PUT, 1);
	// 	break;
	// 	default:
	// 	if ($data)
	// 		$url = sprintf("%s?%s", $url, http_build_query($data));
	// }

 //    // Optional Authentication:
	// curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	// // curl_setopt($curl, CURLOPT_HTTPHEADER,
	// // 	array(
	// // 		// 'Authorization: Bearer '. $crl,
	// // 		'Content-type: application/json'
 // //         // 'Access-Control-Allow-Origin: *'
	// // 	));

	// curl_setopt($curl, CURLOPT_URL, $url);
	// curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	// print_r(curl_setopt($curl));
	// $login = curl_exec($curl); 
	// echo $login;
	// curl_close($curl);

	// return $login;

// 	$ch = curl_init();

// 	curl_setopt($ch, CURLOPT_URL,$url);
// 	curl_setopt($ch, CURLOPT_POST, 1);
// 	curl_setopt($ch, CURLOPT_POSTFIELDS,  http_build_query($data));

// // In real life you should use something like:
// // curl_setopt($ch, CURLOPT_POSTFIELDS, 
// //          http_build_query(array('postvar1' => 'value1')));

// // Receive server response ...
// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// 	$server_output = curl_exec($ch);
// 	echo $server_output;
// 	curl_close ($ch);

	global $login;
	$curl = curl_init();
	switch ($method)
	{
		case "POST":
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		break;
		case "PUT":
		curl_setopt($curl, CURLOPT_PUT, 1);
		break;
		default:
		if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
	}
    // Optional Authentication:
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_HTTPHEADER,
		array(
			// 'Authorization: Bearer '. $crl,
			'Content-type: application/json'
         // 'Access-Control-Allow-Origin: *'
		));
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$login = curl_exec($curl); 
	// echo $login;
	curl_close($curl);
	return $login;
}

function CallApiSearchall($method, $url, $data , $crl)
{
	global $result;
	$curl = curl_init();
	switch ($method)
	{
		case "POST":
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		break;
		case "PUT":
		curl_setopt($curl, CURLOPT_PUT, 1);
		break;
		default:
		if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
	}
    // Optional Authentication:
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	curl_setopt($curl, CURLOPT_HTTPHEADER,
		array(
			'Authorization: Bearer '. $crl,
			'Content-type: application/json'
         // 'Access-Control-Allow-Origin: *'
		));
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl); 
	curl_close($curl);
	return $result;
}

function viewResponseCLR($method, $url, $data )
{
	global $view_response;
	$curl = curl_init();
	switch ($method)
	{
		case "POST":
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		break;
		case "PUT":
		curl_setopt($curl, CURLOPT_PUT, 1);
		break;
		default:
		if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
	}
    // Optional Authentication:
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$view_response = curl_exec($curl); 
	// echo $view_response;
	curl_close($curl);
	return $view_response;
}

function legalSupportToCLR($method, $url, $data )
{
	global $legal_support;
	$curl = curl_init();
	switch ($method)
	{
		case "POST":
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		break;
		case "PUT":
		curl_setopt($curl, CURLOPT_PUT, 1);
		break;
		default:
		if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
	}
    // Optional Authentication:
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_HTTPHEADER,
		array(
			// 'Authorization: Bearer '. $crl,
			'Content-type: application/json'
         // 'Access-Control-Allow-Origin: *'
		));
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$legal_support = curl_exec($curl); 
	// echo $url;
	curl_close($curl);
	return $legal_support;
}


function riskCategoryList($method, $url, $data )
{
	// print_r($data);
	global $risk_tree_get;
	$curl = curl_init();
	switch ($method)
	{
		case "POST":
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		break;
		case "PUT":
		curl_setopt($curl, CURLOPT_PUT, 1);
		break;
		default:
		if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
	}
    // Optional Authentication:
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_HTTPHEADER,
		array(
			// 'Authorization: Bearer '. $crl,
			'Content-type: application/json'
         // 'Access-Control-Allow-Origin: *'
		));
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$risk_tree_get = curl_exec($curl); 
	// echo $url;
	curl_close($curl);
	return $risk_tree_get;
}

function riskcategory($method, $url, $data )
{
	// print_r($data);
	global $getriskcategory;
	$curl = curl_init();
	switch ($method)
	{
		case "POST":
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		break;
		case "PUT":
		curl_setopt($curl, CURLOPT_PUT, 1);
		break;
		default:
		if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
	}
    // Optional Authentication:
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_HTTPHEADER,
		array(
			// 'Authorization: Bearer '. $crl,
			'Content-type: application/json'
         // 'Access-Control-Allow-Origin: *'
		));
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$getriskcategory = curl_exec($curl); 
	// echo $getriskcategory;
	curl_close($curl);
	return $getriskcategory;
}

echo json_encode($arr);
?>