<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?>

<link rel="stylesheet" href="css/vertical-layout-light/add-client.css"> 

<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">                    
                  <b>Process and Upload Matter</b>  
                </h5>
              </div>
              <div class="page-body">
                <div class="card form-group">
                  <div class="card-block">
                    <div class="row">
                      <?php $caseno = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
                      $selcaseno = mysqli_fetch_array($caseno);
                      $getinfo = mysqli_query($connection,"SELECT `email`,`name`,`last_name`,`mobile` FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                      $fetch_getinfo = mysqli_fetch_array($getinfo);?>
                      <h4 class="head_div col-12 form-group" style="text-align: center;"><b><?php echo $selcaseno['case_type']; echo " "; echo $selcaseno['case_no']; echo " "; echo $selcaseno['case_no_year'];?></b></h4>
                      <div class="col-12 form-group">
                        <form class="forms-sample col-sm-12 row" method="POST" action="" enctype="multipart/form-data" autocomplete="off">
                          <div class="form-group col-sm-6 row">
                           <input type="hidden" name="case_id" id="case_id" value="<?php echo base64_decode($_GET['caseid'])?>">
                           <input type="hidden" name="email" id="email" value="<?php echo $fetch_getinfo['email'];?>">
                           <input type="hidden" id="fname" value="<?php echo $fetch_getinfo['name'];?>">
                           <input type="hidden" id="lname" value="<?php echo $fetch_getinfo['last_name'];?>">
                           <input type="hidden" id="mobile" value="<?php echo $fetch_getinfo['mobile'];?>">
                           <input type="hidden" name="enc_caseid" id="enc_caseid" value="<?php echo $_GET['caseid']?>">
                           <label for="exampleInputUsername2" class="col-sm-4 col-form-label"><b>Subject<span class="asterisk"></span>&nbsp;: </b></label>
                           <div class="col-sm-8">
                            <select name="matter_type" id="matter"  class="form-control" style="font-size: 14px; color: gray; border: 1px solid #babfc7;">
                              <option value="">---- Select Subject ---</option>
                              <?php $nameofmatter = mysqli_query($connection, "SELECT `nameofmatter` FROM `name_of_matter` WHERE `status`=1 ORDER BY nameofmatter ASC");
                              while($nameofmatterss = mysqli_fetch_array($nameofmatter)){?>
                                <option value="<?php echo $nameofmatterss['nameofmatter'];?>"><?php echo $nameofmatterss['nameofmatter'];?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-6 form-group row" style="display: none;" id="other_label">
                          <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Other:</b></label>
                          <div class="col-sm-8" id="other_div">
                            <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                            <input type="text" name="subject_other" id="subject_other" class="form-control" placeholder="Enter Case Nmber Here"  >
                          </div>
                        </div>
                        <div class="col-sm-6 form-group row">
                          <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Court Name:</b></label>
                          <div class="col-sm-8">
                            <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                            <input type="text" name="court_name" id="court_name" value="<?php echo $selcaseno['court_name'];?>" class="form-control" placeholder="Enter Case Nmber Here"  >
                          </div>

                        </div>
                        <?php if($selcaseno['court_id'] == 1){?>
                          <div class="form-group col-sm-6 row">
                           <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Diary Number:</b></label>
                           <div class="col-sm-8">
                            <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                            <input type="text" name="diary_no" id="diary_no" value="<?php echo $selcaseno['diary_no'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                          </div>
                        </div>
                        <div class="col-sm-6 form-group row">
                          <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Diary Year:</b></label>
                          <div class="col-sm-8">
                            <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                            <input type="text" name="diary_year" id="diary_year" value="<?php echo $selcaseno['diary_year'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                          </div>

                        </div>
                      <?php } else{?>
                        <div class="form-group row col-sm-6">
                         <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Case Number:</b></label>
                         <div class="col-sm-8">
                          <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                          <input type="text" name="case_no" id="case_no" value="<?php echo $selcaseno['case_no'];?>" class="form-control" placeholder="Enter Case Nmber Here"  >
                        </div>
                      </div>
                      <div class="col-sm-6 form-group row">
                        <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Case Year:</b></label>
                        <div class="col-sm-8">
                          <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                          <input type="text" name="case_no_year" id="case_no_year" value="<?php echo $selcaseno['case_no_year'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                        </div>
                      </div>
                    <?php } ?>
                    <div class="col-sm-6 form-group row">
                     <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Case Type:</b></label>
                     <div class="col-sm-8">
                      <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                      <input type="text" name="case_type" id="case_type" value="<?php echo $selcaseno['case_type'];?>" class="form-control" placeholder="Enter Case Nmber Here"  >
                    </div>
                  </div>
                  <div class="col-sm-6 form-group row">
                    <label for="exampleInputConfirmPassword2" class="col-sm-4 col-form-label"><b>Department:</b></label>
                    <div class="col-sm-8">

                      <select name="department" id="dept" class="form-control">
                        <option value="<?php echo $selcaseno['name_of_matter']; ?>"><?php if($selcaseno['name_of_matter']!=''){echo $selcaseno['name_of_matter'];}else{?>----- Select Department ----- <?php } ?></option>
                        <?php $document = mysqli_query($connection, "SELECT `department_name` FROM `department` WHERE `status`=0 ORDER BY department_name ASC");
                        while($seldocment = mysqli_fetch_array($document)){?>
                          <option value="<?php echo $seldocment['department_name'];?>"><?php echo $seldocment['department_name'];?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <?php if($selcaseno['court_id'] == 2){?>
                    <div class="col-sm-6 form-group row">
                      <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>High Court:</b></label>
                      <div class="col-sm-8">
                        <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                        <input type="text" name="high_court" id="high_court" value="<?php echo $selcaseno['high_court'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                      </div>
                    </div>
                    <div class="col-sm-6 form-group row">
                      <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Bench:</b></label>
                      <div class="col-sm-8">
                        <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                        <input type="text" name="bench" id="bench" value="<?php echo $selcaseno['bench'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                      </div>
                    </div>
                    <div class="col-sm-6 form-group row">
                      <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Side:</b></label>
                      <div class="col-sm-8">
                        <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                        <input type="text" name="side" id="side" value="<?php echo $selcaseno['side'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                      </div>
                    </div>
                    <div class="col-sm-6 form-group row">
                      <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Stamp/ Register:</b></label>
                      <div class="col-sm-4">
                        <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                        <input type="text" name="hc_stamp_register" id="hc_stamp_register" value="<?php echo $selcaseno['hc_stamp_register'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                      </div>
                    </div>
                  <?php } ?>
                  <?php if($selcaseno['court_id'] == 3){?>
                    <div class="form-group col-sm-6 row">
                      <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>State:</b></label>
                      <div class="col-sm-8">
                        <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                        <input type="text" name="state" id="state" value="<?php echo $selcaseno['state'];?>" class="form-control" placeholder="Enter Case Nmber Here"  >
                      </div>
                    </div>
                    <div class="col-sm-6 form-group row">
                      <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>District:</b></label>
                      <div class="col-sm-8">
                        <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                        <input type="text" name="district" id="district" value="<?php echo $selcaseno['district'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                      </div>
                    </div>
                    <div class="col-sm-6 form-group row">
                      <label for="exampleInputEmail2" class="col-sm-4 col-form-label"><b>Court Establishment:</b></label>
                      <div class="col-sm-8">
                        <!-- <input type="email" class="form-control" id="email" placeholder="Email Address" name="email"> -->
                        <input type="text" name="court_establishment" id="court_establishment" value="<?php echo $selcaseno['court_establishment'];?>" class="form-control" placeholder="Enter Case Nmber Here" >
                      </div>
                    </div>
                  <?php } ?>

                  <div class="form-group col-sm-6 row">
                    <div class="checkbox-zoom zoom-primary col-sm-4">
                      <label>
                        <input type="checkbox" id="litigation" name="litigation" class="cb" value="Litigation" onclick="litigationfunction('Yes')" id="view"><span class="cr">
                          <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                        </span>
                        <span>Litigation:</span>
                      </label>
                    </div>
                    <div class="checkbox-zoom zoom-primary col-sm-4">
                      <label>
                        <input type="checkbox" value="Non Litigation" name="non_litigation" id="non_litigation" class="cb" onclick="litigationfunction('No')" id="nl"><span class="cr">
                          <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                        </span>
                        <span>Non Litigation:</span>
                      </label>
                    </div>
                  </div>

                  <div class="form-group col-sm-6" id="litigationid">
                    <select id="demo" multiple="multiple" name="Litigationsd[]" class="col-sm-8 LCheckboxr">
                      <option value="Filing Of Suit/Plaint">Filing Of Suit/Plaint</option>
                      <option value="Vakalatnama">Vakalatnama</option>
                      <option value="Court Fees">Court Fees</option>
                      <option value="How Proceedings Are">How Proceedings Are Conducted</option>
                      <option value="Written Statement">Written Statement</option>
                      <option value="Replication By Plaintiff">Replication By Plaintiff</option>
                      <option value="Filing Of Other Documents">Filing Of Other Documents</option>
                      <option value="Framing Of Issues/List Of Witness">Framing Of Issues/List Of Witness</option>
                      <option value="Final Hearing">Final Hearing</option>
                      <option value="Appeal, Reference And Review">Appeal, Reference And Review</option>
                      <option value="Limitation Filing of Suit/Plaint">Limitation</option>
                    </select>
                  </div>
                  <div class="form-group col-sm-6 row">
                    <label for="exampleInputConfirmPassword2" class="col-sm-4 col-form-label"><b>Document Type:</b></label>
                    <div class="col-sm-8">
                      <select name="cas_type" class="form-control"  id="document_typeapi">

                      <!-- <option value="Writ petition">Writ petition</option>
                      <option value="SLP">SLP</option>
                      <option value="Other Petitions">Other Petitions</option>
                      <option value="Application">Application</option>
                      <option value="Complaint">Complaint</option>
                      <option value="Civil Suit">Civil Suit</option>
                      <option value="Appeal">Appeal</option>
                      <option value="Revision Application">Revision Application</option> -->
                    </select>
                  </div>
                </div>
                <div class="col-sm-12 ">
                  <div id="attach_document_div">

                  </div>
                </div>                 
                <div class="form-group col-12 row">
                  <h5>Instructions/Queries <span class="asterisk">*</span></h5>
                  <div class="form-radio col">
                    <div class="radio radio-primary radio-inline">
                      <label>
                        <input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('English')" value="English" checked>
                        <i class="helper"></i>English
                      </label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      <label>
                        <input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('Marathi')" value="Marathi">     
                        <i class="helper"></i>Marathi 
                      </label>
                    </div>
                  </div>
                  <textarea name="details" id="query" ><?php echo $_POST['details'];?></textarea>

                </div>
                <div class="col-12 div_buttons">
                  <input type="button" onclick="submitRdd()" id="load_button" class="btn btn-info btn-sm dashboardsubmit" name="savedata" value="Submit">
                  <input type="Reset" class="btn btn-danger btn-sm" value="Reset" id="res" onclick="ckr()" >
                </div>



              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</body>
<?php include("footer.php");?>
<script type="text/javascript" src="scripts/rdd_avail.js"></script>

<!-- <script type="text/javascript">
  $(document).ready(function(){
    $(this).next().next().prop('disabled', !this.checked)
    $('.LCheckboxr').not(':checked').prop('disabled', $('.LCheckboxr:checked').length == 2);
    $('.LCheckboxr').on("click",function () {
      $(this).next().next().prop('disabled', !this.checked)
      $('.LCheckboxr').not(':checked').prop('disabled', $('.LCheckboxr:checked').length == 2);
    });
  });
</script>  -->

<div class="fifty"></div>

