$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  var actions = $("table td:last-child").html();
  // Append table with add row form on add new button click
  $(".add-new").click(function(){
    $(this).attr("disabled", "disabled");
    var index = $("table tbody tr:last-child").index();
    var row = '<tr>' +
    '<td><input type="text" class="form-control" name="name" id="name"></td>' +
    '<td><input type="text" class="form-control" name="department" id="department"></td>' +
    '<td><input type="text" class="form-control" name="phone" id="phone"></td>' +
    '<td><input type="text" class="form-control" name="designation" id="designation"></td>' +
    '<td>' + actions + '</td>' +
    '</tr>';
    $("table").append(row);   
    $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
    $('[data-toggle="tooltip"]').tooltip();
  });
  // Add row on add button click
  $(document).on("click", ".add", function(){
    var empty = false;
    var input = $(this).parents("tr").find('input[type="text"]');
    input.each(function(){
      if(!$(this).val()){
        $(this).addClass("error");
        empty = true;
      } else{
        $(this).removeClass("error");
      }
    });
    $(this).parents("tr").find(".error").first().focus();
    if(!empty){
      input.each(function(){
        $(this).parent("td").html($(this).val());
      });     
      $(this).parents("tr").find(".add, .edit").toggle();
      $(".add-new").removeAttr("disabled");
    }   
  });
  // Edit row on edit button click
  $(document).on("click", ".edit", function(){    
    $(this).parents("tr").find("td:not(:last-child)").each(function(){
      $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
    });   
    $(this).parents("tr").find(".add, .edit").toggle();
    $(".add-new").attr("disabled", "disabled");
  });
  // Delete row on delete button click
  $(document).on("click", ".delete", function(){
    $(this).parents("tr").remove();
    $(".add-new").removeAttr("disabled");
  });
});
$(document).ready(function () {
  var counter = 1;

  $("#addrow").on("click", function () {
    var newRow = $("<tr>");
    var cols = "";

    cols += '<td><input type="text" class="form-control" name="fullname' + counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" name="email_id' + counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" name="contact_no' + counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" name="designation' + counter + '"/></td>';

    cols += '<td class="advdeletemiddle"><button type="button" class=" ibtnDel btn btn-sm btn-danger deleteadv"><i class="icofont icofont-trash"></i></button></td>';
    newRow.append(cols);
    $("table.order-list").append(newRow);
    counter++;
    $("#h").val(counter);
        //alert(counter);
      });



  $("table.order-list").on("click", ".ibtnDel", function (event) {
    $(this).closest("tr").remove();       
    counter -= 1
  });


});



function calculateRow1(row) {
  var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal1() {
  var grandTotal = 0;
  $("table.order-list").find('input[name^="price"]').each(function () {
    grandTotal += +$(this).val();
  });
  $("#grandtotal").text(grandTotal.toFixed(2));
}
$('#state').on('change',function(){
 var countryID = $(this).val();
 if(countryID){
   $.ajax({
     type:'POST',
     url:'ajaxData.php',
     data:'country_id='+countryID,
     success:function(html){
       $('#states').html(html);
       $('#city').html('<option value="">Select District first</option>');
     }
   });
 }else{
   $('#states').html('<option value="">Select State first</option>');
   $('#city').html('<option value="">Select District first</option>');
 }
});

$('#states').on('change',function(){
 var stateID = $(this).val();

 if(stateID){
   $.ajax({
     type:'POST',
     url:'ajaxData.php',
     data:'state_id='+stateID,
     success:function(html){
       $('#city').html(html);
     }
   });
 }else{
   $('#city').html('<option value="">Select District first</option>');
 }
});
$('#home_state').on('change',function(){
 var countryID = $(this).val();
 if(countryID){
   $.ajax({
     type:'POST',
     url:'ajaxData.php',
     data:'country_id='+countryID,
     success:function(html){

       $('#home_states').html(html);
       $('#city').html('<option value="">Select District first</option>');
     }
   });
 }else{
   $('#states').html('<option value="">Select State first</option>');
   $('#city').html('<option value="">Select District first</option>');
 }

});


function  onlyNumberAllowed(event) {
      if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
           event.preventDefault(); //stop character from entering input
           toastr.error("","Only Numbers are allowed",{timeout:5000});
         }
       }

      
       
       $("#home_state").select2();
       $("#home_states").select2();
       $("#home_city").select2();



       function  myValidate(){
        var fullname =$("#full_name").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var age = $("#age").val();
        var fathername = $("#father_name").val();
        var Companyname =$("#company_name").val();
        var website =$("#website").val();
        var tin =$("#tin").val();
        var gstinVal=$("#gst").val();
        var panno =$("#pan_no").val();
        var hourlyrate =$("#hourly_rate").val();
        var officeline_1= $("#office_line_1").val();
        var officeline_2 =$("#office_line_2").val();
        var officezip = $("#office_zip").val();
        var homeline_1= $("#home_line_1").val();
        var homeline_2= $("#home_line_2").val();
        var homezip = $("#home_zip").val();

        var regfullname=/^[a-zA-Z][a-zA-Z\\s]+$/;
        var regphone= /^([+]\d{2}[ ])?\d{10}$/;
        var reggst = /^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/;
        var regpancard=/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        var regpostcode=/^[1-9]{1}[0-9]{2}\s{0,1}[0-9]{3}$/;
        var regwebsite=/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
        var regemail=/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        var reginr=/(?:Rs\.?|INR)\s*(\d+(?:[.,]\d+)*)|(\d+(?:[.,]\d+)*)\s*(?:Rs\.?|INR)/


        if(!fullname.match(regfullname) && fullname ==''){

         toastr.warning("Please Enter Full name",{timeout:5000});
         return false;
       }

       if(!reggst.test(gstinVal) && gstinVal!='' ){

        toastr.warning("GST Identification Number is not valid. It should be in this '11AAAAA1111Z1A1' format",{timeout:5000});
        return false;
      }
      if(!mobile.match(regphone) && mobile!=''){

       toastr.warning("Please put 10  digit mobile number",{timeout:5000});
       return false;
     }
     if ( !( age > 1 && age<100) && age!=''){

      toastr.warning("Age Not Valid",{timeout:5000});
      return false;
    }
    if(!regpancard.test(panno) && panno!=''){

      toastr.warning("Please Enter Valid Pan Card Number",{timeout:5000});
      return false;

    }
    if(!regpancard.test(tin) && tin!=''){

     toastr.warning("Please Enter Valid TIN Number",{timeout:5000});
     return false;
   }
   if(!regpostcode.test(officezip) && officezip!='' || !regpostcode.test(homezip) && homezip!=''){

     toastr.warning("Please Enter Valid Postal Code",{timeout:5000});
     return false;
   }
   if(!regwebsite.test(website) && website!=''){

    toastr.warning("Please Enter Valid Website link",{timeout:5000});
    return false;
  }

 /*if(!reginr.test(hourlyrate) && hourlyrate!=''){
  
   toastr.warning("Please Enter Valid INR",{timeout:5000});
      return false;
    }*/
    else{

     return true;
   }
 }