$(document).ready(function(){
  getDepartmentList();
})
function getclr(){
  var user= $("#email").val();
  var firstname= $("#fname").val();
  var lastname= $("#lname").val();
  var contact= $("#mobile").val();
  var state= $("#state").val();
  var district= $("#district").val();
  // var query = CKEDITOR.instances.question_id.getData();
  // var query = $("#question_id").val();
  var query = tinymce.get("question_id").getContent();
  var act= $("#actlist").val();
  /*var documents= $("#docfiles").val();*/
  var section= $("#section").val();
  
  var subject = [];
  subject.push(act);
  var QueryVo=new Object();
  QueryVo.user=user;
  QueryVo.firstname=firstname;
  QueryVo.lastname=lastname;
  QueryVo.contact=contact;
  QueryVo.state=state;
  QueryVo.city=district;
  QueryVo.query=query;
  QueryVo.subject = subject;

  // var ActVO=new Object();
  // ActVO.act= act;
  // ActVO.section=section;
  

    // var file = document.getElementById("docfiles").files[0];     
    // var formdata=new FormData();
    // formdata.append("documents",file);
    // var fileArray=[];
    // fileArray.push(formdata);
    // QueryVo.documents=fileArray;
    if(query==""){
      toastr.warning("Please provide some query",{timeOut: 5000});
      return false;
    }
    


    $.ajax({
      url: legalResearch+"/restquery",
      type:'POST',
      data: JSON.stringify(QueryVo),
      dataType: 'json',
      async:false,
      headers: {
        "Content-Type": 'application/json'
      },
      success:function(data){            
        console.log(data);
        if(data.error=='SUCCESS'){

          toastr.success('Query Sent Successfully', {timeOut: 5000}) ;
          $('#processtheMatter').modal('hide');
                 // $('#processtheMatter').hide().removeClass("modal-backdrop show");


               }       
             },

             complete: function(){ 
              $('#flip-square-loader').hide();

            },
            error:function(e){
              toastr.error('Unable to Send Query', 'Sorry For Inconvenience!', {timeOut: 5000})
              //console.log(e);
            }
          });
  }

  function getDepartmentList(){
    $.ajax({
      type:'POST',
      url:'department_api.php',
      success:function(response){
        // console.log(response);
        var response = JSON.parse(response);
        var option = '<option value="">Select Department</option>';
        $.each(response,function(i,obj){
          option +='<option value='+obj.dep_id+'>'+obj.department_name+'</option>';
        });
        $("#department_view").html(option);
        $("#department_view").select2({
          placeholder: 'Select...'
        });
      },
      error:function(){
       toastr.error("","Error fetching department",{timeout:5000});
     }
   }); 
  }