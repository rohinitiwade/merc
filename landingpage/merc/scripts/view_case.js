 var cnr='',case_no='',color='',court_id_val='',getcase='',autoupdate,caseid='',endate=0,hearing_date,complete_data_report='';
$("#inform-to-client-tbl").hide();
$(".case-activity-info").hide();
$("#who_attended_others_div").hide();
var teamListArray = '';
$(document).ready(function(){
  function deletecase (id) {
   // var yes = confirm('All the documents, To-does, Notes, Timesheet Etc. associated to this case will be deleted Are you sure? this action cant be undone.');
   if (window.confirm("All the documents, To-does, Notes, Timesheet Etc. associated to this case will be deleted Are you sure? this action cant be undone.")) { 
      // window.open("exit.html", "Thanks for Visiting!"); 
      $.ajax({
        type:'POST',
        url:'delete_cases.php',
        data:'case_id='+caseid,
        success:function(html){
          alert("Case Removed Successfully");
          window.location.href='cases-report.php';
       // $('#delcase').html(html);
     }
   }); 
    }
  }
  tinymce.init({
   selector : '#query',
   plugins : [
   "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
   "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });
 /* debugger;
  $(".tab-pane").removeClass("active show");
  $(".nav-link").removeClass("active");
  if(localStorage.getItem("findcaselaw") == 'FCL'){
    $("#connected-tab").addClass("active");
    $("#cases-referred").addClass("active show");
    localStorage.setItem("findcaselaw","");
  }
  else{
    $("#activity-tab").addClass("active");
    $("#activity-history").addClass("active show");
  }*/
  $("#caseactivityinfo-next_hearing").datepicker({
   dateFormat: 'yy/mm/dd',
   changeYear: true,
   changeMonth: true
 });
 /* $(".next_hearing_datepicker").datepicker({
   dateFormat: 'yy/mm/dd',
   changeYear: true,
   changeMonth: true
 });*/
  // if()
  getAutoUpdateStatus();
   view_case_details();
  
  // view_case_details();
  /*getallquery(1);  */
  $("input[name='informAdvocateradio']").on('change',function(){
    var val = $(this).val();
    if(val == 'Yes')
      $("#inform-to-client-tbl").show();

    else
      $("#inform-to-client-tbl").hide();
  });
  $("input[name='recordHearingradio']").on('change',function(){
    var val = $(this).val();
    if(val == 'Yes')
      $(".case-activity-info").show();

    else
      $(".case-activity-info").hide();
  });

  $("input[name='who_attended']").on('click',function(){
    var val = $(this).val();
    if(val == 'Others' && $(this).is(":checked"))
      $("#who_attended_others_div").show();
    else
      $("#who_attended_others_div").hide();
  });
  $("#record_status").on('change',function(){
    var val = $(this).val();
    if(val != 'Order'){
      $("#hearing-date-label").text(val+' Date:');
      $("input[name='who_attended'][value='Others']").prop('checked',false);
      $("#who_attended_others_div").hide();
    }
    else if(val == 'Order'){
      $("input[name='who_attended'][value='Others']").prop('checked',true);
      $("#who_attended_others_div").show();
      $("#hearing-date-label").text('Next Hearing Date:');
    }
  });
});
$( "#datepickertimesheet" ).datepicker();
$('#addtimesheet').click(function(){
  $('#addtimesheetform').slideToggle(400);
});

function encryptData(data){
         var key = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef');
          var iv  = CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210');
//        var key = CryptoJS.enc.Hex.parse('5678943210fdecba5678943210fdecba');
//        var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');
        var encrypted = CryptoJS.AES.encrypt((data), key, { iv: iv });
        var encrypted_data = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
            return encrypted_data;
}

function getAutoUpdateStatus(){
  var caseid=$("#caseid").val();
  var court_id = $("#court-id").val();
  $.ajax({ 
    url: host+"/auto_update.php",
    type:"POST",
    data: {
      case_id : caseid
    },
    cache: false,
    async:false,

    success: function(response){
      /*var d = new Date(); */
      endate = response.expiry_time;
      var response = JSON.parse(response);
      if(response.auto_update == 'off'){
        // document.getElementById("auto_update").checked = false;
        autoupdate = 'off';
      }
      else{
        // document.getElementById("auto_update").checked = true;
        autoupdate = 'on';
        var date = new Date();
        var today_date = date.getFullYear() + "-"+ ((date.getMonth())+1) +"-"+ date.getDate();
        // alert("Today Date-"+today_date);
        view_case_details();
        /*if(court_id == '3' || court_id == '2' || court_id == '1' && hearing_date < today_date)      
        getAutoUpdate();*/

      }
    },
    error :function(){
     toastr.error("","Error in auto update for case",{timeout:5000});
   }
 });
}

// function getAutoUpdate(){
//  var court_id = $("#court-id").val();
//  if(court_id == '1'){
//    var url = legalResearch+'/search_by_sc';
//    var data = {
//      caseNo : "",
//      caseType : "",
//      caseYear : "",
//      party : "", 
//      dairyNo : $("#case_no").val(),
//      dairyYear : $("#case_no_year").val(),
//      searchType : "DAIRY"
//    }

//  }
//  else if(court_id =='2'){
//    courtType = 'HC';
//    var url = legalResearch+'/search_by_bom';
//    var data = {
//      stamp : $("#stamp").val(),
//      party : "",
//      side : $("#side").val(),
//      bench : $("#bench").val(),
//      cType : $("#case_type").val(),
//      cYear :  $("#case_no_year").val(),
//      cNo : $("#case_no").val(),
//      sType : "FETCH",
//      id : ''
//    }
//  }
//  else if(court_id == '3'){
//    var url = legalResearch+"/search_by_cnr";
//    var cnrVo = new Object();
//    cnrVo.cino = encryptData(cnr);
//    cnrVo.courtType = 'DC';
//    data = cnrVo;
//  }

//  if(document.getElementById("auto_update").checked === true){
//    autoupdate = 'on';
//    $.ajax({ 
//      url: url,
//      data: data,
//      cache: false,
//      async:false,
//      headers: {
//        "Content-Type": 'application/json'
//      },
//      success: function(get){
//        getcase = JSON.stringify(get);
//        sendtoPhp(getcase);
//      },
//      error :function(){
//        toastr.error("Error in auto updating case","Site is slow,please try after some time",{timeout:5000});
//        document.getElementById("auto_update").checked = false;
//        // view_case_details();
//      }
//    });
//  } 
//  else {
//    autoupdate='off';
//    view_case_details();
//    var yes = confirm("Are you sure that you want to disable the auto pulling? It means system will not record any hearing dates automatically.");
//  }

// }

// function sendtoPhp(data){
//  var caseid=$("#caseid").val();
//  var court_id = $("#court-id").val();
//   /*var data = {
//     hearing : data,
//     case_id : caseid,
//     court_id : court_id,
//     CNR : cnr,
//     case_no : case_no
//   }*/
//   $.ajax({
//    type:"POST",
//    url:host+'/updated_hearing_date.php',
//    data: {
//     hearing : data,
//     case_id : caseid,
//     court_id : court_id,
//     CNR : cnr,
//     case_no : case_no
//   },    
//   cache: false,
//   async:false,
//   beforeSend : function(){      
//   },
//   success: function(response){
//     console.log("RESPONSE",response);
//     // view_case_details();
//   },
//   error :function(){

//   }
// });
// }


/*
Dropzone.autoDiscover = false;

var myDropzone = new Dropzone(".dropzone", { 
   autoProcessQueue: false,
   parallelUploads: 10 // Number of files process at a time (default 2)
});

$('#uploadfiles').click(function(){
   myDropzone.processQueue();
 });*/

/*function getValues() {
    var formData = new FormData();
    // these image appends are getting dropzones instances
    formData.append('image', $('#foobar_image')[0].dropzone.getAcceptedFiles()[0]); // attach dropzone image element
    formData.append('image_2', $('#barfoo')[0].dropzone.getAcceptedFiles()[0]);
    formData.append("id", $("#id").val()); // regular text form attachment
    formData.append("_method", 'PUT'); // required to spoof a PUT request for a FormData object (not needed for POST request)

    return formData;
}

$(document).on('submit','#document_form', function(e) {
    e.preventDefault();
    e.stopPropagation();    

    $.ajax({
        method: 'POST',
        url: host+"case_document_upload.php",
        data: getValues(),
        processData: false, // required for FormData with jQuery
        contentType: false, // required for FormData with jQuery
        success: function(response) {
            // do something
        }
    });
});
*/



function getallquery(pageno){    
  var user = $("#email").val();
      // var user= 'rameshwar.air@gmail.com';
      /*var pageno=1;*/
      var QueVo=new Object();
      QueVo.user=user;
      QueVo.pageNo=pageno;

      $.ajax({ 
        url: legalResearch+"/alluserquery",
        type:'POST',
        data: JSON.stringify(QueVo),
        dataType: 'json',
        async:false,
        headers: {
          "Content-Type": 'application/json'
        },
        success:function(data){            
          // console.log(data);

          $("#clr_count").text(data.total);
                     // if(data){
                     //    $("#queryshow").val(data.caseTitleid);
                     // }
                     var pagination = '<div class="panel-footer "><ul class="pagination pagination-sm pull-right pagination-separate pagination-round pagination-flat pageNo"><li id="page_link_prev" class="page-item"><a class="page-link" href="#">Prev</a></li>';
                     for(var i=1; i<=data.numPages;i++){
                      pagination += '<li id="page_link_'+i+'" class="page-item activepageno"><a class="page-link" onclick="getallquery('+i+')">'+i+'</a></li>';
                    }

                    pagination += '<li id="page_link_next" class="page-item"><a class="page-link" href="#">Next</a></li></ul></div>';
                    $("#pagen").html(pagination);

                    var getquey= data.userOutBoxVos;
                    var queyd = '';

                    $.each(getquey, function(key,val) {
                      var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
                      var d = new Date(data.userOutBoxVos[0].displayDate);
                      var seekdate = month[d.getMonth()];
                      var t = new Date(data.userOutBoxVos[0].displayDate);
                // var seektime = getTime();
        // var seektime = time[t.getTime()];
        queyd += '<div class="chat_div form-group"><div class="col-sm-12 row" style="padding: 0px; text-align: justify; background:none;"><label class="col-sm-12 row query"><div class="col-sm-10" style="font-weight: normal; color: black; text-align: justify;"> <span style="color:teal; font-weight:bold;">Question '+val.qid+'. </span>'+val.query+'</div> <div class="col-sm-2"><i class="feather icon-calendar"></i>&nbsp;'+seekdate +' '+ d.getDate() + ','+ d.getFullYear()+' <br><i class="icofont icofont-time" aria-hidden="true"></i> '+ t.getHours() + ':'+ t.getMinutes() +'</div></label></div><div class="reply_show col-sm-12" id="reply'+val.qid+'"></div><div class="col-sm-12"><div class="col-sm-12 chat_img" style="padding:0px;"><img src="'+host+'/images/explanation.png" class="bookimg"><span>&nbsp; &nbsp;<a href="javascript:void(0)" id="showreply'+val.qid+'" class="view_reply view" onclick="getans('+val.qid+')"></a></span></div></div></div>';

      }); 
                    $("#queryshow").html(queyd);
                    tinymce.init({
                      selector : '#question_id',
                      plugins : [
                      "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                      "  directionality textcolor paste fullpage textcolor colorpicker " ],
                      toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
                      menubar : false,
                      contextmenu_never_use_native : true,
                      toolbar_items_size : 'small',
                      branding : false,
                      height : 200
                    });
                    assign_active_pagination(pageno);
                  },
                  error:function(e){
            //console.log(e);
          }
        });
    }

    function assign_active_pagination(pageNo){
     $("#page_link_"+pageNo).addClass('active');
   }

   $(window).ready(function(){
     $("#page_link_prev").on("click",function(){
      var pageNo = $(".activepageno.active a").text();
      var prev_page = parseInt(pageNo)-1;
      getallquery(prev_page);
    });

     $("#page_link_next").on("click",function(){
      var pageNo = $(".activepageno.active a").text();
      var next_page = parseInt(pageNo)+1;
      getallquery(next_page);
    });
   });


   var oldqid='';
   function getans(qid){
       // debugger;
   // alert(qid);
   
   var AnsVo=new Object();
   AnsVo.qid=qid;
   var htmlData="";

   $.ajax({ 
    url: legalResearch+"/answers",
    type:'POST',
    data: JSON.stringify(AnsVo),
    dataType: 'json',
    async:false,
    headers: {
      "Content-Type": 'application/json'
    },
    success:function(data){ 
      if(qid == oldqid){
        if ($("#showreply"+qid).hasClass("view")) 
          $("#showreply"+qid).removeClass('view').addClass('hide');
        else
          $("#showreply"+qid).removeClass('hide').addClass('view');
        $("#reply"+qid).toggle();
      }
      else{
        if(data.questionAnswer!=null){
          htmlData='<div class="col-sm-12 row" style="background:none;"><label class="col-sm-1 query" style="padding:0px; text-align: center;">Ans: </label>';
          var researcherDocVos =data.researcherDocVos;
            //$.each(researcherDocVos, function(i,v){
               htmlData+='<span style="font-weight: bold;" class="col-sm-11">'+data.questionAnswer+'</span></div>';//+"<ul><li><a href="+v.path+">"+v.docs+"</a></li></ul>
               
            //});
            $("#reply"+qid).html(htmlData);

            $("#reply"+qid).show();
            // $("#showreply"+qid).text('Hide Answer');
            $("#showreply"+qid).removeClass('view').addClass('hide');
            oldqid = qid;

          }else{
           if(qid == oldqid){
            if ($("#showreply"+qid).hasClass("view")) 
             $("#showreply"+qid).removeClass('view').addClass('hide');
           else
             $("#showreply"+qid).removeClass('hide').addClass('view');
           $("#reply"+qid).toggle();
         }else{
          var ans = '<div class="col-sm-12"><label class="col-sm-1 query" style="padding:0px;">Ans: </label><span  class="col-sm-11" style="font-weight: bold;">Waiting For Response</span></span>';
          $("#reply"+qid).html(ans);
          $("#reply"+qid).show();
          $("#showreply"+qid).removeClass('view').addClass('hide');
          oldqid = qid;
        }
      }
    }
  },
  error:function(e){
            //console.log(e);
          }
        });
 }

 function view_case_details(getFlag){
  // debugger;
  caseid = $("#caseid").val();
  court_id = $("#court-id").val();
  $.ajax({
    url: host+'/view_caseurl.php',
    type:'POST',
    data :"case_id="+caseid+"&autoupdate="+autoupdate+"&court_id="+court_id,
    dataType: 'json',
    async:false,
    cache : false,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(response){
      if(response.status == 'success'){
        // console.log(response);
        $(".tab-pane").removeClass('active show');
        $(".nav-link").removeClass('active');
        // $("#activity-tab").removeClass("active");
          // $("#activity-history").removeClass("active show");
        // if(getFlag == 'c'){
        //   $("#activity-tab").addClass("active");
        //   $("#activity-history").addClass("active show");
        //    $("#case-details-tab").removeClass("active");
        //     $("#activity-case-details").removeClass("active show");
        // }
        if(response.service == 'QA'){
          $("#seek-legal-help").addClass('active show');
          $("#seeklegaltab").addClass('active');
        }
        else if(localStorage.getItem("findcaselaw") == 'FCL'){
          $("#cases-referred-tab").addClass("active");
          $("#cases-referred").addClass("active show");
          localStorage.setItem("findcaselaw","");
        }
        else{
          $("#activity-case-details").addClass('active show');
          $("#case-details-tab").addClass('active');
        }
        // debugger;
        if(response.case_status == 'Disposed'){

          $("#closed-status").text("DISPOSED OFF / CLOSED");

          $('#auto-update-div').hide();
          $("#case_disposed").show();
          $("#right-side-div-view").addClass('right-side-div');
        }
        else{
          $("#running-status").text(response.case_status);
          $("#auto-update-div").show();
          $("#right-side-div-view").removeClass('right-side-div');
        }
    // $("#district_court_title").text(data.sc_case_type+' '+data.case_no+' / '+data.case_no_year);
    // $("#sub_case_title").text(data.case_title);
    $("#sub_case_nominal").text(response.nominal);
    // complete_data_report = response.nominal;
    $(".flip-square-loader").hide();
    $("#connected-case").text(response.connected_count);
    $("#view_case_document").text(response.document_count);
    hearing_date = response.data[0].hearing_date;
    // alert(hearing_date);
    buildCaseDetails(response);
    var account_type = $("#account_type").val();
    if(!isEmpty(response.appearing_modal)){
      var app_model = response.appearing_modal.split("-");

      var appmodelval = [];

      for(var i=0;i<app_model.length;i++){
        appmodelval.push(app_model[i].trim());
      }
      var appear_as = appmodelval.indexOf(response.data[0].are_you_appearing_as);

      if(appear_as == 0 || response.data[0].are_you_appearing_as == 'Petitioner'){ 
        // if(account_type == 'Individual') 
        buildPetitioner(response.petitioners_data,response.data[0].are_you_appearing_as,response.appearing_modal);       

        /*else
        buiildPetitionerAdv(response.pet_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);*/
        buildRespondent(response.respondent_data,response.data[0].are_you_appearing_as,response.appearing_modal);
        buildRespondentAdv(response.res_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);

      // else{
      //   buildRespondentAdv(response.res_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
      // }
    }
    else{
     // if(account_type == 'Individual') 
     buildRespondent(response.respondent_data,response.data[0].are_you_appearing_as,response.appearing_modal);  

    // else
    //   buildRespondentAdv(response.res_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    buildPetitioner(response.petitioners_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    buiildPetitionerAdv(response.pet_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);

  }

}

else{
  if(response.data[0].are_you_appearing_as == "Petitioner"){
    // if(account_type == 'Individual') 
    buildPetitioner(response.petitioners_data,response.data[0].are_you_appearing_as,response.appearing_modal);       

    // else
    //   buiildPetitionerAdv(response.pet_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    buildRespondent(response.respondent_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    buildRespondentAdv(response.res_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
  }
  else{
    // if(account_type == 'Individual') 
    buildRespondent(response.respondent_data,response.data[0].are_you_appearing_as,response.appearing_modal);  

    // else
    //   buildRespondentAdv(response.res_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    buildPetitioner(response.petitioners_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    buiildPetitionerAdv(response.pet_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);

  }
}

    // buiildPetitionerAdv(response.pet_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    // buildRespondentAdv(response.res_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
    // buildHearingData(response.hearing_data);
    buildHearingDataList(response);
    teamList();


    buildTeamMember(response.team_data);    

    
    buildStage(response.stage,response.data[0].flag);

    // buildActivityLog(response);
  }
  else if(response.status == 'error'){
   $("#view_card").hide();
   $("#not_found").show();

 }
},
complete:function(){
  $(".flip-square-loader").hide();
},
error:function(e){
  $(".flip-square-loader").hide();
  toastr.error("","Error while fetching data for entered CNR no.","Sorry for inconvinence",{timeout:5000});
}
});
}


function getUploadedDoc(type){
  /*$(".tab-pane fade").removeClass("active show");
  $("#documents").addClass('active show');*/
  var file = '',doc_type='';
  if(type == 'u'){
   


    file = document.getElementById("upload_doc").files[0];
    doc_type = $("#doc_type option:selected").val();
     var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','doc','docx'];
  // var case_id = caseid;
  if(isEmpty(file)){
    toastr.error("","Please upload atleast one document",{timeout:5000});
    return false;
  }else if ($.inArray($("#upload_doc").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls' formats are allowed.",{timeout:5000});
      return false;
    }
  else if(isEmpty(doc_type)){
     toastr.error("","Please Select Document Type",{timeout:5000});
    $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
    return false;
  }
  

}
 var doc_value = $( "#doc_type option:selected" ).text();
var formData = new FormData();
formData.append('file', file);
formData.append('case_no', case_no);
formData.append('case_id',caseid);
formData.append('doc_type',doc_type);
formData.append('doc_value',doc_value);
var ajaxReq = $.ajax({
  url : host+"/case_document_upload.php",
  type : 'POST',
  data : formData,
  cache : false,
  contentType : false,
  processData : false,
  success:function(response){
    $("#documentList").html('');
    $('input[type=file]').val('');

    var response_val = JSON.parse(response);
    var tr ='';
    if(type == 'u'){
      if(response_val.status == 'success')
        toastr.success("",'File Uploaded Successfully',{timeout:3000});
      else
        toastr.error("","Error in uploading files",{timeout:5000});
    }
    $.each(response_val.data,function(i,obj){
      $("#view_case_document").text(obj.doc_count);
      var filesize = bytesToSize(obj.size);
      tr +='<tr>';
      tr +='<td class="case-title"><span id="'+obj.doc_guid+'" onclick=viewcases('+obj.doc_id+',this.id) class="viewDoc"><i class="feather icon-file-pdf-o "></i> '+obj.file_name+'</span></td>';
      tr +='<td class="case-title">'+filesize +'</td>';
      tr +='<td class="case-title">'+obj.type+'</td>';
      tr +='<td class="case-title">'+obj.uploaded_by+'</td>';
      tr +='<td class="case-title">'+obj.date_time+'</td>';
      tr+='<td><a class="btn btn-outline-warning doc_action btn-mini form-group" href="'+host+'/editMercDocument.php?id='+obj.encrypted_case_id+'&&flag=view" title="Edit"><i class="feather icon-edit"></i></a>';
      if(obj.title == 'doc'){
       tr+= '<a class="btn btn-outline-warning doc_action btn-mini form-group" href="'+obj.doc_path+'" title="Download" download><i class="feather icon-eye"></i></a>'; 
     }else{
      tr+= ' <button title="Download" type="button" class="doc_action viewBtn btn btn-outline-info btn-mini form-group" ><a href="downloadDocument.php?getid='+obj.doc_guid+'&&flag=alldoc" class="viewDoc" target="_blank" download><i class="feather icon-download"></i></a></button>';
    }
    tr+= ' <button type="button" onclick="deleteDoc('+obj.doc_id+')" title="Delete" class="doc_action btn btn-outline-danger btn-mini form-group"><i class="feather icon-trash"></i></button></td>';
  });

    if(tr !=''){
      var table = '<table class="table table-bordered" id="documentList-table">';
      table +='<thead class="bg-primary"><tr><th>Title</th><th>Size</th><th>Type</th><th>Uploaded By</th><th>Uploaded Date</th><th class="action_th">Action</th></tr></thead><tbody>';
      table+= tr +'</tbody></table>';
    }
    $("#documentList").html(table);
    $("#documentList-table").dataTable();
  },error:function(){
    toastr.error("",'Error in uploading document',{timeout:5000});
  }
});
};

$("#search-doc-filter").on('change',function(){
  var val = $(this).val();
  if(!isEmpty(val)){
    $("#search-document-div").show();
    $("#search-in-document").attr('placeholder','Enter '+val.toUpperCase());
  }
  else{
    $("#search-document-div").hide();
    $("#search-in-document").val('');
  }

});

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
function viewcases (docid,fileguid) {
  var fileExtension = ['doc','docx'];
  if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 0 || $.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 1) {
    // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
    window.location.href = host +"/viewDocument.php?getid="+fileguid+"&&flag=alldoc";
    $("#view_case_document_modal").modal('hide');
     // window.open(filename);
   }
   else{
    $.ajax({
     type:'POST',
     url:'ajaxdata/viewdoc.php',
     data:'docid='+docid,
     beforeSend: function()
     { 
      $('#flip-square-loader').show();
    },
    success:function(html){
      $('#casedocument').html(html);
      $("#view_case_document_modal").modal('show');
    },
    complete: function(){ 
      $('#flip-square-loader').hide();
    },
    error:function(e){
      $(".flip-square-loader").hide();
      toastr.error('Unable to Load ', 'Sorry For Inconvenience!', {timeOut: 5000})
              //console.log(e);
            }
          }); 
  }
}
function deleteDoc(doc_id){
  var yes = confirm("Are you sure you want to delete document?");
  if(yes){
    $.ajax({
      url : host+"/delete_view_document.php",
      type:'POST',
      data : {
        doc_id : doc_id
      },
      dataType: 'json',
      cache : false,
      success:function(response){
        // console.log(response);
        if(response.status == 'success'){
          getUploadedDoc('d');
          toastr.success("","Successfully deleted the document",{timeout:5000});
        }
        else
          toastr.error("","Error in deleting the document",{timeout:5000});
      },
      error:function(){
        toastr.error("","Error in deleting the document",{timeout:5000});
      }
    });
  }else
  return false
}
function buildStage(response,flag){
  var next_hearing_date = isEmpty(response.next_hearing_date) ? "" : response.next_hearing_date;
  var stage = isEmpty(response.stage) ? "" : response.stage;
  var posted_for = isEmpty(response.posted_for) ? "" : response.posted_for;
  var action_taken = isEmpty(response.action_taken) ? "" : response.action_taken;
  var session_phase = isEmpty(response.session_phase) ? "" : response.session_phase;

  courtdata = '<table class="case-sidebar-section table-striped table table-bordered">';
  courtdata += '<tbody><tr>';
  courtdata += '<td class="c-type">Stage:</td>';
  courtdata += '<td class="c-desc">'+stage+'</td>';
  courtdata += '</tr>';
  if(flag != 'judicial'){
    courtdata += '<tr>';
    courtdata += '<td class="c-type">Posted For:</td>';
    courtdata += '<td class="c-desc">'+posted_for+'</td>';
    courtdata += '</tr>';
    courtdata += '<tr>';
    courtdata += '<td class="c-type">Last Action Taken:</td>';
    courtdata += '<td class="c-desc">'+action_taken+'</td>';
    courtdata += '</tr>';
  }
  courtdata += '<tr>';
  courtdata += '<td class="c-type">Hearing Date:</td>';
  courtdata += '<td class="c-desc">'+next_hearing_date+'</td>';
  courtdata += '</tr>';
  // courtdata += '<tr>';
  // courtdata += '<td class="c-type">Session:</td>';
  // if(session_phase == 1)
  //   courtdata += '<td class="c-desc">Morning</td>';
  // else if(session_phase == 2)
  //   courtdata += '<td class="c-desc">Evening</td>';
  // else
  //   courtdata += '<td class="c-desc"></td>';
  // courtdata += '</tr>';
  courtdata += '</tbody></table>';
  $(".stage").html(courtdata);
}

function buildCaseDetails(response){
  // debugger;
  var id = $("#id").val();
  $.each(response.data,function(i,obj){
    cnr = obj.CNR;
    // if(obj.auto_update == 'off')
    //   document.getElementById("auto_update").checked === false;
    // else
    //   document.getElementById("auto_update").checked === true;
        // case_no = obj.case_no;
        court_id_val = obj.court_id;
        // if(court_id_val == 0){
        //   $("#auto-update-div").hide();
        //   $("#set_priority_select").hide();
        //   $("#timesheet-li").hide();
        //   $("#connected-cases-li").hide();
        //   $("#relevant-case-li").hide();
        // }
        // else{
        //   //$("#auto-update-div").show();
        //   $("#set_priority_select").show();
        //   $("#timesheet-li").show();
        //   $("#connected-cases-li").show();
        //   $("#relevant-case-li").show();
        // }
        var case_type = isEmpty(obj.case_type) ? "" : obj.case_type;
        $("#district_court_title").text(case_type+' '+obj.case_no+' / '+obj.case_no_year);
        $("#sub_case_nominal").show();
        $("#sub_case_nominal").text(obj.nominal);
        $("#sub_case_desc").html(obj.case_description);

        var appearing_as = isEmpty(obj.are_you_appearing_as) ? "" : obj.are_you_appearing_as;
        var court_name = isEmpty(obj.court_name) ? "" : obj.court_name;
        var date_of_filling = isEmpty(obj.date_of_filling) ? "": obj.date_of_filling;
        var is_affidavit_filed = isEmpty(obj.is_the_affidavit_vakalath_filed) ? "" : obj.is_the_affidavit_vakalath_filed;
        var is_vakalat_filed = isEmpty(obj.is_vakalat_filed) ? "" : obj.is_vakalat_filed;
        var high_court = isEmpty(obj.high_court) ? "" : obj.high_court;
        var supreme_court = isEmpty(obj.supreme_court) ? "" : obj.supreme_court;
        var bench = isEmpty(obj.bench) ? "" : obj.bench;
        var side = isEmpty(obj.side) ? "" : obj.side;
        var stamp = isEmpty(obj.hc_stamp_register) ? "" : obj.hc_stamp_register;
        if(court_id_val == '2'){
          $("#stamp").val(stamp);
          $("#side").val(side);
          $("#bench").val(bench);
          $("#stamp").val(stamp);
          $("#case_type").val(obj.case_type_initials);
          $("#case_no").val(obj.case_no);
          $("#case_no_year").val(obj.case_no_year);
        }

        var state_name = isEmpty(obj.state) ? "" : obj.state;

        var court_establishment= isEmpty(obj.court_establishment) ? "" : obj.court_establishment;
        var district_name= isEmpty(obj.district) ? "" : obj.district;
        var ministry = isEmpty(obj.ministry) ? "" : obj.ministry;
        var div_comm_level = isEmpty(obj.div_comm_level) ? "" : obj.div_comm_level;
        var division = isEmpty(obj.division) ? "" : obj.division;
        var classification = isEmpty(obj.classification) ? "" :obj.classification;
        var zp_name = isEmpty(obj.zp_name) ? "" : obj.zp_name;
        var ministry_desk_no = isEmpty(obj.ministry_desk_no) ? "" : obj.ministry_desk_no;
        var name_of_matter = isEmpty(obj.name_of_matter) ? "" : obj.name_of_matter;
        var appearing_modal= isEmpty(response.appearing_modal) ? "" : response.appearing_modal;
        var petitioner= isEmpty(obj.petitioner) ? "" : obj.petitioner;
        var judge= isEmpty(obj.judge) ? "" : obj.judge;
        var reffered_by= isEmpty(obj.reffered_by) ? "" : obj.reffered_by;
        // var is_affidavit_filed= isEmpty(obj.is_affidavit_filed) ? "" : obj.is_affidavit_filed;
        var affidavite_filling_date = isEmpty(obj.affidavite_filling_date) ? "" : obj.affidavite_filling_date;
        var vakalat_filling_date = isEmpty(obj.vakalat_filling_date) ? "" : obj.vakalat_filling_date;
        var sc_case_type= isEmpty(obj.sc_case_type) ? "" : obj.sc_case_type;
        var classification= isEmpty(obj.classification) ? "" : obj.classification;
        case_no = isEmpty(obj.case_no) ? "" : obj.case_no;
        var case_no_year= isEmpty(obj.case_no_year) ? "" : obj.case_no_year;
        var court_hall= isEmpty(obj.court_hall) ? "" : obj.court_hall;
        var floor= isEmpty(obj.floor) ? "" : obj.floor;
        var priority= isEmpty(obj.priority) ? "" : obj.priority;
        var section_category= isEmpty(obj.section_category) ? "" : obj.section_category;
        var cnr_case_details = isEmpty(obj.CNR) ? "" : obj.CNR;
        /*var judges = isEmpty(obj.judge) ? "" : obj.judge;
        var referred_by = isEmpty(obj.reffered_by) ? "" : obj.reffered_by;
        var section  = isEmpty(obj.section_category) ? "" : obj.section_category;*/
        var account_type = $("#account-type-input").val();
        var courtdata = '';
        courtdata +='<table class="table table-bordered table-striped case-sidebar-section court-details-section-1 export_table"><thead style="display:none"><tr>';
        courtdata +='<th>Title</th><th>'+obj.case_title+'</th></tr></thead><tbody>';

courtdata += '<tr><td class="c-type">Case </th>';
        courtdata += '<td class="c-desc">'+ obj.autoid +'</td></tr>';
        courtdata += '<tr><td class="c-type">Court </th>';
        courtdata += '<td class="c-desc">'+ court_name +'</td></tr>';

        // if(court_id_val == '0'){

        //   courtdata += '<tr><td class="c-type">Competant Authority </th>';
        //   courtdata += '<td class="c-desc">'+ court_name +'</td></tr>';
        //   courtdata += '<tr><td class="c-type">Ministry </th>';
        //   courtdata += '<td class="c-desc">'+ ministry +'</td></tr>';
        //   if(account_type !='Individual'){
        //     courtdata += '<tr><td class="c-type">Ministry Desk No. </th>';
        //     courtdata += '<td class="c-desc">'+ ministry_desk_no +'</td></tr>';
        //     courtdata += '<tr><td class="c-type">Division </th>';
        //     courtdata += '<td class="c-desc">'+ division +'</td></tr>';
        //     courtdata += '<tr><td class="c-type">ZP Name </th>';
        //     courtdata += '<td class="c-desc">'+ zp_name +'</td></tr>';
        //     courtdata += '<tr><td class="c-type">Divisional Com Level </th>';
        //     courtdata += '<td class="c-desc">'+ div_comm_level +'</td></tr>';
        //     // courtdata += '<tr><td class="c-type">Department </th>';
        //     // courtdata += '<td class="c-desc">'+ name_of_matter +'</td></tr>';          
        //     // courtdata += '<tr><td class="c-type">Subject </th>';
        //     // courtdata += '<td class="c-desc">'+ classification +'</td></tr>';             
        //   }

        //   var petitioner  =' <div class="table-responsive"><table class="table table-bordered table-striped case-sidebar-section court-details-section-1 export_table">';
        //   petitioner += '<thead><tr><th colspan="2">Petitioner</th></tr></thead>';
        //   petitioner += '<tbody><tr><th> Petitioner Name</td><td>'+obj.pet_name+'</td></tr>';
        //   petitioner += '<tr><td>Petitioner Postal Address</td><td>'+obj.pet_address+'</td></tr>';
        //   petitioner += '<tr><td> Petitioner Email</td><td>'+obj.pet_email_id+'</td></tr>';
        //   petitioner += '<tr><td> Petitioner Mobile</td><td>'+obj.pet_mobile+'</td></tr></tbody></table>';
        //   $(".petitioner").show();
        //   $(".petitioner").html(petitioner); 

        //   var petitioner  = '<div class="table-responsive"><table class="table table-bordered table-striped case-sidebar-section court-details-section-1 export_table">';
        //   petitioner += '<thead><tr><th colspan="2"> Respondent</th></tr></thead>';
        //   petitioner += '<tbody><tr><td>Respondent Name</td><td>'+obj.res_name+'</td></tr>';
        //   petitioner += '<tr><td> Respondent Postal Address</td><td>'+obj.res_address+'</td></tr>';
        //   petitioner += '<tr><td> Respondent Email</td><td>'+obj.res_email_id+'</td></tr>';
        //   petitioner += '<tr><td> Respondent Mobile</td><td>'+obj.res_mobile+'</td></tr></tbody></table>';
        //   $(".respondent").show();
        //   $(".respondent").html(petitioner);
        // }

        // if(court_id_val == 2 || court_id_val == 5){
        //   courtdata += '<tr><td class="c-type">'+court_name+'</td>';
        //   courtdata += '<td class="c-desc">'+high_court+'</td></tr>';
        // }
        // if(court_id_val == 1){
        //   courtdata += '<tr><td class="c-type">'+court_name+'</td>';
        //   courtdata += '<td class="c-desc">'+supreme_court+'</td></tr>';
        // }
        // if(obj.court_details !=''){
        //   courtdata += '<tr><td class="c-type">'+court_name+' Details </td>';
        //   courtdata += '<td class="c-desc">'+obj.court_details+'</td></tr>';
        // }
        // if(court_id_val == 2){
          courtdata += '<tr><td class="c-type">Type of Litigation</td>';
          courtdata += '<td class="c-desc">'+obj.type_of_litigation+'</td></tr>';
          courtdata += '<tr><td class="c-type">Case Numver</td>';
          courtdata += '<td class="c-desc">'+case_no+'</td></tr>';
          courtdata += '<tr><td class="c-type">Case Year</td>';
          courtdata += '<td class="c-desc">'+case_no_year+'</td></tr>';
          courtdata += '<tr><td class="c-type">Risk Category</td>';
          courtdata += '<td class="c-desc">'+obj.risk_category+'</td></tr>';
          courtdata += '<tr><td class="c-type">Subject/ Matter</td>';
          courtdata += '<td class="c-desc">'+obj.name_of_matter+'</td></tr>';
          courtdata += '<tr><td class="c-type">Title</td>';
          courtdata += '<td class="c-desc">'+obj.case_title+'</td></tr>';
          courtdata += '<tr><td class="c-type">Case Description</td>';
          courtdata += '<td class="c-desc">'+obj.case_description+'</td></tr>';
          courtdata += '<tr><td class="c-type">Date of Filling</td>';
          courtdata += '<td class="c-desc">'+date_of_filling+'</td></tr>';
        // }

        // if(court_id_val == 5){
        //   courtdata += '<tr><td class="c-type">State</td>';
        //   courtdata += '<td class="c-desc">'+state_name+'</td></tr>';
        //   courtdata += '<tr><td class="c-type"> Bench</td>';
        //   courtdata += '<td class="c-desc">'+bench+'</td></tr>';
        // }

        // if(court_id_val == 3){
        //   courtdata += '<tr><td class="c-type">State</td>';
        //   courtdata += '<td class="c-desc">'+state_name+'</td></tr>';
        //   courtdata += '<tr><td class="c-type">District</td>';
        //   courtdata += '<td class="c-desc">'+district_name+'</td></tr>';
        //   courtdata += '<tr><td class="c-type"> Court Establishment</td>';
        //   courtdata += '<td class="c-desc">'+court_establishment+'</td></tr>';
        //   courtdata += '<tr><td class="c-type">CNR</td>';
        //   courtdata += '<td class="c-desc">'+cnr_case_details+'</td></tr>';
        // }
        // if(court_id_val == 1 || court_id_val == 3 || court_id_val == 5){
        //   courtdata += '<tr><td class="c-type">Appearing Model</td>';
        //   courtdata += '<td class="c-desc">'+appearing_modal+'</td></tr>';
        // }
        // if(court_id_val !='0'){
        //   courtdata += '<tr><td class="c-type">You are appearing for</td>';
        //   courtdata += '<td class="c-desc">'+ appearing_as +'</td></tr>';

        // // if(court_id_val == 5 && court_id_val == 1){
        //   courtdata += '<tr><td class="c-type">'+appearing_as+'</td>';
        //   courtdata += '<td class="c-desc">'+petitioner+'</td></tr>';
        // }
        /*if(){
          courtdata += '<tr><td class="c-type">'+appearing_as+'</td>';
          courtdata += '<td class="c-desc">'+petitioner+'</td></tr>';
        }*/
        // courtdata += '<tr><td class="c-type"> Judges</td>';
        // courtdata += '<td class="c-desc">'+judge+'</td></tr>';
        // courtdata += '<tr><td class="c-type">Referred By/ यांच्या संदर्भाने</td>';
        // courtdata += '<td class="c-desc">'+reffered_by+'</td></tr>';


        // courtdata += '<tr><td class="c-type">Is the affidavit filed? :</th>';
        // courtdata += '<td class="c-desc">'+is_affidavit_filed+'</td></tr>';
      // }
      // if(court_id_val == 1 || court_id_val == 2){
       // courtdata += '<tr><td class="c-type"> Affidavit Filling Date :</td>';
       // courtdata += '<td class="c-desc">'+affidavite_filling_date+'</td>';
     // }
   //   if(court_id_val!='0'){
   //     // courtdata += '<tr><td class="c-type">Is the vakalatnama filed?:</th>';
   //     // courtdata += '<td class="c-desc">'+is_vakalat_filed+'</td></tr>';

   //     // courtdata += '<tr><td class="c-type"> Vakalatnama Filling Date:</th>';
   //     // courtdata += '<td class="c-desc">'+vakalat_filling_date+'</td></tr>';
   //     if(account_type !='Individual'){
   //      // courtdata += '<tr><td class="c-type">Department:</th>';
   //      // courtdata += '<td class="c-desc">'+obj.name_of_matter+'</td>';

   //      // courtdata += '<tr><td class="c-type">विषय / Subject:</th>';
   //      // courtdata += '<td class="c-desc">'+obj.classification+'</td>';

   //      // courtdata += '<tr><td class="c-type"> Ministry Desk Number:</th>';
   //      // if(!isEmpty(obj.ministry_desk_no))
   //      //   courtdata += '<td class="c-desc">'+obj.ministry_desk_no+'</td>';
   //      // else
   //      //   courtdata += '<td class="c-desc"></td>';

   //      // courtdata += '<tr><td class="c-type"> Divisional Commissioner Level:</th>';
   //      // courtdata += '<td class="c-desc">'+obj.div_comm_level+'</td>';
   //    }
   //    courtdata += '<tr><td class="c-type"> Priority:</th>';
   //    courtdata += '<td class="c-desc">'+obj.priority+'</td></tr></tbody></table>';
   //    if(!isEmpty(obj.priority))
   //     prioritySelected(obj.priority);
   // }

   $(".court_data").html(courtdata);      

   // courtdata = '<table class="table table-bordered table-striped case-sidebar-section court-details-section-1"><thead style="display:none"><tr><th>Title</th><th>'+obj.case_title+'</th></tr></thead><tbody><tr>';
   // if(supreme_court == 'Case Number'){
   //   courtdata += '<tr><td class="c-type">Case Type</td>';
   //   courtdata += '<td class="c-desc">'+case_type+'</td></tr>';
   // }
   // // if(court_id_val == 1 || court_id_val == 2){
   // //   courtdata += '<tr><td class="c-type">Subject</td>';
   // //   courtdata += '<td class="c-desc">'+obj.classification+'</td></tr>';
   // // }
   // if(supreme_court == 'Case Number'){
   //   courtdata += '<tr><td class="c-type">Case Number</td>';
   //   courtdata += '<td class="c-desc">'+obj.case_no+'</td></tr>';
   // }
   // else if(supreme_court == 'Diary Number'){
   //   courtdata += '<tr><td class="c-type"> Dairy Number</td>';
   //   courtdata += '<td class="c-desc">'+obj.case_no+'</td></tr>';      
   // }
   // else{
   //   courtdata += '<tr><td class="c-type"> Case Type</td>';
   //   courtdata += '<td class="c-desc">'+case_type+'</td></tr>';
   //   courtdata += '<tr><td class="c-type"> Case Number</td>';
   //   courtdata += '<td class="c-desc">'+obj.case_no+'</td></tr>';

   // }
   // courtdata += '<tr><td class="c-type">Year</td>';
   // courtdata += '<td class="c-desc">'+obj.case_no_year+'</td></tr>';
   // courtdata += '<tr><td class="c-type"> Date of filing</td>';
   // courtdata += '<td class="c-desc">'+date_of_filling+'</td></tr>';
   // if(court_id_val == 1){
   //   courtdata += '<tr><td class="c-type">Court Hall #</td>';
   //   courtdata += '<td class="c-desc">'+obj.court_hall+'</td></tr>';
   //   courtdata += '<tr><td class="c-type">Floor  #</td>';
   //   courtdata += '<td class="c-desc">'+obj.floor+'</td></tr>';
   //      // courtdata += '<tr><td class="c-type">Priority</td>';
   //      // courtdata += '<td class="c-desc">'+obj.priority+'</td></tr>';

   //    }
      // courtdata += '<tr><td class="c-type">Section/Category </td>';
      // courtdata += '<td class="c-desc">'+obj.section_category+'</td>';
      // courtdata += '</tr></tbody></table><div class="stage"></div>';
      // courtdata += buildHearingData('load');
      // $(".casetype").html(courtdata);
      // complete_data_report = courtdata;
    });
}

function buildPetitioner(response,appearing_as,appeaing_model){
  if(!isEmpty(appeaing_model)){
    var app_model = appeaing_model.split("-");
    var appmodelval = [];
    for(var i=0;i<app_model.length;i++){
      appmodelval.push(app_model[i].trim());
    }
    var appear_as = appmodelval.indexOf(appearing_as);

    courtdata = '<table class="table table-bordered table-striped  case-sidebar-section export_table">';
    courtdata += '<thead>';
    courtdata += '<tr>';
    if(appear_as == 0)
      courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Client Details <span class="client-type-txt">('+app_model[0]+')</span></span>';
    else
      courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Opposite Party <span class="client-type-txt">('+app_model[0]+')</span></span>';
  }
  else{
    courtdata = '<table class="table table-bordered table-striped  case-sidebar-section export_table">';
    courtdata += '<thead>';
    courtdata += '<tr>';
    if(appearing_as == 'Petitioner')
      courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Client Details <span class="client-type-txt">(Petitioner)</span></span>';
    else
      courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Opposite Party  <span class="client-type-txt">(Petitioner)</span></span>';
  }
  courtdata += '<div class="clear"></div></th></tr></thead><tbody>';
  if(!isEmpty(response)){
    $.each(response,function(index,object){
      courtdata += '<tr>';
      courtdata += '<td class="">';
      /*if(object.respondent_name)
        courtdata += '<p>'+object.respondent_name+'</p>';
        else*/
          courtdata += '<p>'+object.petitioner_name+'</p>';
        courtdata += '</td></tr>';
      });
  }
  else{
    courtdata += '<tr>';
    courtdata += '<td class="">';
    courtdata += '<p>No Clients were added.</p>';
    courtdata += '</td></tr>';
  }

  courtdata += '</tbody></table>';
  $(".petitioner").show();
  $(".petitioner").html(courtdata);


  courtdata = '<table class="table table-bordered"><thead><tr>';
  courtdata += '<th class="client_inform">Client Name</th>';
  courtdata += '<th class="email_inform">Email Address</th>';
  courtdata += '<th class="phone_inform">Phone Number';
  courtdata += '<span class="sms-helper-info">(Maximum character limit is 1,500 for SMS)</span>';
  courtdata += '</th></tr></thead><tbody>';
  if(!isEmpty(response)){
    $.each(response,function(index,object){
      courtdata += '<tr>';
      if(object.respondent_name){
        courtdata += '<td class="">'+object.respondent_name+'</td>';
        courtdata += '<td class="">'+object.respondent_email+'</td>';
        courtdata += '<td class="">'+object.respondent_mobile+'</td>';
      }
      else{
        courtdata += '<td class="">'+object.petitioner_name+'</td>';
        courtdata += '<td class="">'+object.petitioner_email+'</td>';
        courtdata += '<td class="">'+object.petitioner_mobile+'</td>';
      }
      courtdata += '</tr>';
    });
  }
  courtdata += '</tbody></table>';
  $("#inform-to-client-tbl tbody").html(courtdata);

}

// function buiildPetitionerAdv(response,appearing_as,appeaing_model){
//   if(!isEmpty(appeaing_model)){
//     var app_model = appeaing_model.split("-");

//     var appmodelval = [];
//     for(var i=0;i<app_model.length;i++){
//       appmodelval.push(app_model[i].trim());
//     }
//     var appear_as = appmodelval.indexOf(appearing_as);
//     courtdata = '<table class="table table-bordered table-striped case-sidebar-section">';
//     courtdata += '<thead>';
//     courtdata += '<tr>';
//     if(appear_as == 0)
//       courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Your Advocates <span class="client-type-txt">('+app_model[0]+')</span></span>';
//     else
//       courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Opposing Counsels <span class="client-type-txt">('+app_model[0]+' )</span></span>';


//   }
//   else{
//     courtdata = '<table class="table table-bordered table-striped ase-sidebar-section">';
//     courtdata += '<thead>';
//     courtdata += '<tr>';
//     if(appearing_as == 'Petitioner')
//       courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Your Advocates <span class="client-type-txt">(Petitioner)</span></span>';
//     else
//       courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Opposing Counsels <span class="client-type-txt">(Petitioner )</span></span>';
//   }
//   courtdata += '<div class="clear"></div></th></tr></thead><tbody>';
//   if(!isEmpty(response)){
//     $.each(response,function(index,object){
//       courtdata += '<tr>';
//       courtdata += '<td class="">';
//       courtdata += '<p>'+object.advocate_name+'</p>';
//       courtdata += '</td></tr>';
//     });
//   }
//   else{
//     courtdata += '<tr>';
//     courtdata += '<td class="">';
//     courtdata += '<p>No advocates were added.</p>';
//     courtdata += '</td></tr>';
//   }

//   courtdata += '</tbody></table>';
//   $(".petitionerAdv").show();
//   $(".petitionerAdv").html(courtdata);

//   courtdata = '<table class="table table-bordered"><thead><tr>';
//   courtdata += '<th class="client_inform">Client Name</th>';
//   courtdata += '<th class="email_inform">Email Address</th>';
//   courtdata += '<th class="phone_inform">Phone Number';
//   courtdata += '<span class="sms-helper-info">(Maximum character limit is 1,500 for SMS)</span>';
//   courtdata += '</th></tr></thead><tbody>';
//   if(!isEmpty(response)){
//     $.each(response,function(index,object){
//       courtdata += '<tr>';
//       courtdata += '<td class="">'+object.advocate_name+'</td>';
//       courtdata += '<td class=""><input type="checkbox"> '+object.advocate_email+'</td>';
//       courtdata += '<td class=""><input type="checkbox"> '+object.advocate_mobile+'</td>';
//       courtdata += '</tr>';
//     });
//   }
//   courtdata += '</tbody></table>';
//   $("#inform-to-client-tbl tbody").html(courtdata);
// }

function buildRespondent(response,appearing_as,appeaing_model){
//   if(!isEmpty(appeaing_model)){
//     var app_model = appeaing_model.split("-");

//     var appmodelval = [];
//     for(var i=0;i<app_model.length;i++){
//       appmodelval.push(app_model[i].trim());
//     }
//     var appear_as = appmodelval.indexOf(appearing_as);

//     courtdata = '<table class="table table-bordered case-sidebar-section export_table">';
//     courtdata += '<thead>';
//     courtdata += '<tr>';
//     if(appear_as == 0)
//       courtdata += '<th class="" colspan="2"><span class="section-header ">Opposite Party ('+app_model[1]+') </span>';
//     else 
//       courtdata += '<th class="" colspan="2"><span class="section-header ">Client Details ('+app_model[1]+') </span>';
//   // courtdata += '<span class="action-add-update-link"><a href="">Update</a></span>';

// }
// else{
  courtdata = '<table class="table table-bordered case-sidebar-section export_table">';
  courtdata += '<thead>';
  courtdata += '<tr>';
  // if(appearing_as == 'Petitioner')
    courtdata += '<th class="" colspan="2"><span class="section-header ">Opposite Party (Respondent) </span>';
  // else 
  //   courtdata += '<th class="" colspan="2"><span class="section-header ">Opponent (Respondent) </span>';
// }
courtdata += '<div class="clear"></div>';
courtdata += ' </th></tr>';
courtdata += '</thead><tbody>';
if(!isEmpty(response)){
  $.each(response,function(index,object){
    courtdata += '<tr>';
    courtdata += ' <td class="" data-url="">';
    if(object.petitioner_name)
      courtdata += '<p class="case-petopp-info">'+object.petitioner_name+'</p>';
    else
      courtdata += '<p class="case-petopp-info">'+object.respondent_name+'</p>';
    courtdata += '</td>';
    courtdata += '</tr>';
  });
}
else{
  courtdata += '<tr>';
  courtdata += ' <td class="" data-url="">';
  courtdata += '<p class="case-petopp-info">No Opposite Party were added</p>';
  courtdata += '</td>';
  courtdata += '</tr>';
}

courtdata +='</tbody></table>';
$(".respondent").show();
$(".respondent").html(courtdata);
}

// function buildRespondentAdv(response,appearing_as,appeaing_model){
//   if(!isEmpty(appeaing_model)){
//     var app_model = appeaing_model.split("-");

//     var appmodelval = [];
//     for(var i=0;i<app_model.length;i++){
//       appmodelval.push(app_model[i].trim());
//     }
//     var appear_as = appmodelval.indexOf(appearing_as);
//     courtdata = '<table class="table table-bordered case-sidebar-section">';
//     courtdata +='<thead><tr><th colspan="2">';
//     if(appear_as == 0 || appearing_as == 'Petitioner')
//       courtdata +=' <span class="section-header ">Opposing Counsels ('+app_model[1]+') </span>';
//     else
//       courtdata +=' <span class="section-header ">Your Advocates ('+app_model[1]+') </span>';
//   }
//   else{

//     courtdata = '<table class="table table-bordered case-sidebar-section">';
//     courtdata +='<thead><tr><th colspan="2">';
//     if(appearing_as == 'Petitioner')
//       courtdata +=' <span class="section-header ">Opposing Counsels (Respondent) </span>';
//     else
//       courtdata +=' <span class="section-header ">Your Advocates (Respondent) </span>';
//   }

//   courtdata +='</th></tr></thead><tbody>';
//   if(!isEmpty(response)){
//     $.each(response,function(index,object){
//       courtdata +=' <tr>';
//       courtdata +='<td class="">';
//       courtdata +='<p class="case-petopp-info">'+object.resadvocate_name+'</p></td>';
//       courtdata +='</tr>';
//     });
//   }
//   else{
//     courtdata +=' <tr>';
//     courtdata +='<td class="">';
//     courtdata +='<p class="case-petopp-info">No Opposing Counsels were added</p></td>';
//     courtdata +='</tr>';
//   }

//   courtdata +='</tbody></table>';         
//   $(".respondent_adv").show();   
//   $(".respondent_adv").html(courtdata);
// }
var limit = 0;

function buildHearingData(type){
  limit = limit + 2;
  var courtdata='';
  var caseid=$("#caseid").val();
  $.ajax({
    url : host+"/loading_hearing_history.php",
    type:'POST',
    data : {
      "CNR": cnr,
      "limit" : limit,
      case_id : caseid
    },
    dataType: 'json',
    async:false,
    cache : false,
    success:function(response){
      // console.log("loading_hearing_history",response);
      var hearingdata='';
      $.each(response.hear_data,function(i,obj){
        hearingdata +='<tr>';
        hearingdata +='<td class="">';
        hearingdata +='<p class="case-past-hearing"><span>'+obj.hearing_date+'</span> <span class="view-details view-details_'+i+'" onclick="viewDetails('+i+')"><a href="javascript:void(0)">view details</a></span></p>';
        hearingdata +='<div class="case-past-hearing-details_'+i+'" style="display: none;">';
        hearingdata +='<div class="case-past-hearing-loggedby">';
        hearingdata +='<div class="left_1">';
        hearingdata +=' <p class="m-b-0"><b>Recorded by </b>';
        hearingdata +='<span class="activity-name"><a href="javascript:void(0)">'+response.user_name+'</a></span> on <span class="activity-at">'+obj.date+' '+obj.time+'</span></p>';
        hearingdata +='</div>';
        hearingdata +='</div><ul>';    
        if(!isEmpty(obj.stage)){  
          hearingdata +='<li><b>Stage: </b>';
          hearingdata +=' <span class="attended-by-chunk">'+obj.stage+'</span>';
          hearingdata +='</li>'; 
        }
        if(!isEmpty(obj.posted_for) && account_type !='Individual'){ 
          hearingdata +='<li><b>Posted For: </b>';
          hearingdata +=' <span class="attended-by-chunk">'+obj.posted_for+'</span>';
          hearingdata +='</li>'; 
        }

        if(!isEmpty(obj.action_taken) && account_type !='Individual'){ 
          hearingdata +='<li><b>Action Taken: </b>';
          hearingdata +=' <span class="attended-by-chunk">'+obj.action_taken+'</span>';
          hearingdata +='</li>';   
        }
        if(!isEmpty(obj.stage)){           
          hearingdata +='<li><b>Attended by: </b>';
          hearingdata +=' <span class="attended-by-chunk">'+obj.stage+'</span>';
          hearingdata +='</li>';  
        }        
        hearingdata +='<li><b>Next Hearing Date: </b><span>'+obj.hearing_date+'</span></li>';
        if(!isEmpty(obj.session_phase)){ 
          if(obj.session_phase == '1')   
            hearingdata +='<li><b>Session </b><span>Morning</span></li>';
          else
            hearingdata +='<li><b>Session </b><span>Evening</span></li>';
        }
        hearingdata +='</ul>';
        hearingdata +='</div>';
        hearingdata +='</td>';
        hearingdata +='</tr>';
      });
      courtdata = '<div class="history_date"><table class="table table-bordered table-striped case-sidebar-section case-sidebar-hearing-tbl" id="hearing-history-table">';
      courtdata += '<thead><tr>';
      courtdata +=' <th>Hearings Date History</th>';
      courtdata +='</tr>';
      courtdata +='</thead>';
      courtdata +='<tbody class="case-hearing-list">';
      if(hearingdata !=''){       
        courtdata += hearingdata;
        if(response.total_count != response.hear_data.length)
          courtdata +='<tr><td><p onclick=buildHearingData("click") style="text-align:right;cursor:pointer">view more</p></td></tr>';
      }
      else{
        courtdata += '<tr>';
        courtdata += '<td>No hearing date available.</td></tr>';
      }
      courtdata +='</tbody></table></div>';
      if(type=='click')
        $(".history_date").html(courtdata);

      // $("#hearing-history-table").DataTable();
    },
    error : function(){
     toastr.error("","Error in getting hearing data list",{timeout:5000});

   }
 });
  return courtdata;
}

function viewDetails(i){
  $(".case-past-hearing-details_"+i).toggle();
  $(".view-details_"+i+" a").text($(".view-details_"+i+" a").text() == 'view details' ? 'hide details' : 'view details');
}

function buildHearingDataList(response){
  courtdata = '<table class="table table-bordered hearing_date_table export_table" id="hearing-data-table"><thead style="display:none"><tr><th colspan="3">Activity/History </th></tr></thead><tbody>';
  courtdata +='<tr>';
  courtdata +='<td style="width:5%;">';
  courtdata +='<img src="'+getActivityUserProfile(response.user_image)+'" alt="" title="Adv. '+response.user_name+'"></td>';
  courtdata +='<td class="user_name">by '+response.user_name+'</td>';
  courtdata +='<td style="text-align:right;" class="desktop-view-icons-td" style="text-align:right"> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+response.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+response.created_time+'</span></td>';
  courtdata +='</tr>';
  courtdata +='<tr class="mobile-view-tr">';
  courtdata+='<td></td>';
  courtdata +='<td colspan="2" class="mobile-view-icons-td desktop-view-icons-td" style="text-align:right;"><span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+response.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+response.created_time+'</span></td>';
  courtdata +='</tr>';
  courtdata +='<tr>';
  courtdata +='<td></td>';
  var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  var d = new Date(response.data[0].date_time);
  var fildate = month[d.getMonth()];
  courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Created on </b>  '+fildate +' '+ d.getDate() + ','+ d.getFullYear()+'.</p></td>';
  courtdata +='</tr>';

  courtdata += buildActivityLog(response.data[0].case_id,response.data[0].case_no,response.user_image);

  if(!isEmpty(response.hearing_data)){

    $.each(response.hearing_data,function(i,obj){
      if(isEmpty(obj.activity_log_type)){ 
        courtdata +='<tr>';
        courtdata +='<td style="width:5%;">';
        courtdata +='<img src="'+getActivityUserProfile(response.user_image)+'" alt="" title="Adv. '+response.user_name+'"></td>';
        courtdata +='<td class="user_name">by '+response.user_name+'</td>';
        courtdata +='<td class="desktop-view-icons-td" style="text-align:right"><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case form-group"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case form-group"></i> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.time+'</span></td>';
        courtdata +='</tr>';
        courtdata +='<tr class="mobile-view-tr">';
        courtdata+='<td></td>';
        courtdata +='<td colspan="2" class="mobile-view-icons-td desktop-view-icons-td" style="text-align:right"><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case form-group" onclick="editHearingDate('+obj.activity_log_id+')"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case form-group" onclick="deleteActivity('+obj.activity_log_id+')"></i> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.time+'</span></td>';
        courtdata +='</tr>';
        if(!isEmpty(obj.stage)){
          courtdata +='<tr>';
          courtdata +='<td></td>';
          courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Stage </b> '+obj.stage+'</p></td>';
          courtdata +='</tr>';
        }
        courtdata +='<tr>';
        courtdata +='<td></td>';
        courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Next Hearing Date </b> '+obj.next_hearing_date+'</p></td>';
        courtdata +='</tr>';
      }

      

    });

  }
        // courtdata +='</table>';

        
      // courtdata += buildAttachment(response);
      courtdata +='<tbody></table>';
      $("#show_hearing_data_list").html(courtdata);
// $("#hearing-data-table").dataTable();

}

function buildActivityLog(caseid,case_no,userimge){
  var data_td='';
  $.ajax({
    url : host+"/fetch_user_activity.php",
    data : {
      case_id : caseid,
      case_no : case_no
    },
    type:"POST",
    async:false,
    success : function(response){
      var response = JSON.parse(response);
      $.each(response,function(i,obj){
        $(".case_title_div").css('background',obj.code);
        if(obj.activity_log_type == 'p'){        
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="'+getActivityUserProfile(userimge)+'" alt="" title="Adv. Vidhi Sharma"></td>';
          data_td +='<td class="user_name">by '+obj.user_name+'</td>';
          data_td +='<td style="text-align:right"><span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i>  '+obj.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr class="mobile-view-tr">';
          data_td+='<td></td>';
          data_td +='<td colspan="2" class="mobile-view-icons-td desktop-view-icons-td" style="text-align:right"><span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i>  '+obj.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr>';
          data_td +='<td></td>';
          if(isEmpty(obj.last_priority))
            data_td +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Priority </b> added <b>'+obj.priority+'</b></p></td>';
          else
            data_td +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Priority </b> changed from <b>'+obj.last_priority+'</b> to <b>'+obj.priority+'</b></p></td>';
          data_td +='</tr>';
          if(!isEmpty(obj.priority))
            prioritySelected(obj.priority);
        }
        if(obj.activity_log_type == 'e'){
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="'+getActivityUserProfile(userimge)+'" alt="" title="Adv. Vidhi Sharma"></td>';
          data_td +='<td class="user_name">by '+obj.user_name+'</td>';
          data_td +='<td style="text-align:right"><span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i>  '+obj.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr class="mobile-view-tr">';
          data_td+='<td></td>';
          data_td +='<td colspan="2" class="mobile-view-icons-td desktop-view-icons-td" style="text-align:right"><span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i>  '+obj.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr>';
          data_td +='<td></td>';
          var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
          var d = new Date(obj.date_time);
          var fildate = month[d.getMonth()];
          data_td +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Case Edited on </b>  '+fildate +' '+ d.getDate() + ','+ d.getFullYear()+'.</p></td>';

          data_td +='</tr>';
        }
        if(obj.activity_log_type == 'c') {
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="'+getActivityUserProfile(userimge)+'" alt="" title="Adv. Vidhi Sharma"></td>';
          data_td +='<td class="user_name">by '+obj.user_name+'</td>';
          data_td +='<td class="desktop-view-icons-td" style="text-align:right"><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case form-group" onclick="editActivityComment('+obj.activity_log_id+')"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case form-group" onclick="deleteActivity('+obj.activity_log_id+')"></i> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr class="mobile-view-tr">';
          data_td+='<td></td>';
          data_td +='<td colspan="2" class="mobile-view-icons-td desktop-view-icons-td" style="text-align:right"><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case form-group" onclick="editActivityComment('+obj.activity_log_id+')"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case form-group" onclick="deleteActivity('+obj.activity_log_id+')"></i> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr>';
          data_td +='<td></td>';
          data_td +='<td colspan="2"><div class="speech-bubble">';
          data_td +='<div id="user-comment_'+obj.activity_log_id+'">'+obj.user_comment+'</div>';
          data_td +='<div id="activity-info_'+obj.activity_log_id+'" class="row activity-info" style="display:none;">';
          // data_td +='<label class="col-sm-5 p-0 d-flex"><b>Would you like to inform client(s)?</b></label>';
          // data_td +='<div class="col-sm-6 row"><div class="form-check col-sm-3">';
          // data_td +='<label class="form-check-label checkboxlabel">';
          // data_td +='<input type="radio" class="chkPassport" name="inform_client" value="Yes">Yes';
          // data_td +='<i class="input-helper"></i><i class="input-helper"></i></label>';
          // data_td +=' </div>';
          // data_td +='<div class="form-check col-sm-3">';
          // data_td +='<label class="form-check-label checkboxlabel">';
          // data_td +='<input type="radio" class="chkPassport" name="inform_client" value="No">No';
          // data_td +='<i class="input-helper"></i><i class="input-helper"></i></label>';
          // data_td +=' </div>';
          // data_td +='</div>';
          data_td +='<div class="col-sm-12" style="text-align:right"><button class="btn btn-info btn-sm" type="button" onclick=updateCaseComment('+obj.activity_log_id+','+obj.court_id+','+obj.case_id+','+obj.case_no+',"'+obj.CNR+'")>Update</button>';
          data_td +=' <button class="btn btn-danger btn-sm" type="button" onclick="cancelActivityEdit('+obj.activity_log_id+')">Cancel</button></div></div></div></td>';
          data_td +='</tr>';
        }
        if(obj.activity_log_type == 'a') {
         var fileExtension = ['jpeg', 'jpg', 'png'];
         data_td +='<tr>';
         data_td +='<td style="width:5%;"><img src="'+getActivityUserProfile(userimge)+'" alt="" title="'+obj.user_name+'"></td>';
         data_td +='<td class="user_name">by '+obj.user_name+'</td>';
         data_td +='<td class="desktop-view-icons-td" style="text-align:right;"> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.created_time+'</span></td>';
         data_td +='</tr>';
         data_td +='<tr class="mobile-view-tr">';
         data_td+='<td></td>';
         data_td +='<td colspan="2" class="mobile-view-icons-td desktop-view-icons-td" style="text-align:right"><span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.created_date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.created_time+'</span></td>';
         data_td +='</tr>';
         data_td +='<tr>';
         data_td +='<td></td>';

         if ($.inArray(obj.document.split('.').pop().toLowerCase(), fileExtension) == -1) 
          data_td +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Added Attachment</b> <a href="'+getDocUrl(obj.document)+'" download>'+obj.document+'</a></p></td>';
        else
          data_td +='<td colspan="1" class="activity-chunks"><p class="m-b-0"><b>Added Attachment</b> <a href="'+getDocUrl(obj.document)+'" target="_blank" download>'+obj.document+'</a><div class="speech-bubble"><img src="'+getDocUrl(obj.document)+'" class="doc_img"></div></p></td>';

        data_td +='</tr>';
      }
      if(obj.activity_log_type == 'h'){        
        courtdata +='<tr>';
        courtdata +='<td style="width:5%;"><img src="'+getActivityUserProfile(response.user_image)+'" alt="" title="Adv. Vidhi Sharma"></td>';
        courtdata +='<td class="user_name">by '+response.user_name+'</td>';
        courtdata +='<td class="desktop-view-icons-td" style="text-align:right"><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case form-group" onclick="editHearingDate('+obj.activity_log_id+')"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case form-group" onclick="deleteActivity('+obj.activity_log_id+')"></i> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.time+'</span></td>';
        courtdata +='</tr>';
        courtdata +='<tr class="mobile-view-tr">';
        courtdata+='<td></td>';
        courtdata +='<td colspan="2" class="mobile-view-icons-td desktop-view-icons-td" style="text-align:right"><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case form-group" onclick="editHearingDate('+obj.activity_log_id+')"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case form-group" onclick="deleteActivity('+obj.activity_log_id+')"></i> <span class="btn btn-outline-primary btn-mini form-group"><i class="feather icon-calendar icons-view-case"></i>  '+obj.date+'</span> <span class="btn btn-outline-warning btn-mini form-group"> <i class="icofont icofont-time icons-view-case"></i> '+obj.time+'</span></td>';
        courtdata +='</tr>';
        courtdata +='<tr>';
        courtdata +='<td></td>';
        courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Stage </b> '+obj.stage+'</p></td></tr>';
        if(!isEmpty(obj.posted_for)){
         courtdata +='<tr>';
         courtdata +='<td></td>';
         courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Posted For </b> '+obj.posted_for+'</p></td></tr>';
         courtdata +='<tr>';
       }
       if(!isEmpty(obj.action_taken)){
         courtdata +='<td></td>';
         courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Action taken </b> '+obj.action_taken+'</p></td></tr>';
       }
       if(!isEmpty(obj.next_hearing_date)){
         courtdata +='<tr>';
         courtdata +='<td></td>';
         courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Next Hearing Date </b>'+obj.next_hearing_date+'</p></td>';
         courtdata +='</tr>';

       }
       if(!isEmpty(obj.session_phase)){
         courtdata +='<tr>';
         courtdata +='<td></td>';
         if(obj.session_phase == '1')
          courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Session </b> Morning</p></td></tr>';
        else
          courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Session </b> Evening</p></td></tr>';        
      }
      if(!isEmpty(obj.who_attended)){
       courtdata +='<tr>';
       courtdata +='<td></td>';        
       var who_attended_namearray=[];
       $.each(obj.who_attended,function(index,object){
        who_attended_namearray.push(object.who_attended_name);
      });
       courtdata +='<td colspan="2" class="activity-chunks"><p class="m-b-0"><b>Attended by </b> '+who_attended_namearray.join(",")+'</p></td></tr>';
     }
     courtdata +='<tr>';
     courtdata +='<td></td>';
     courtdata +='<td colspan="2" class="activity-chunks">';
     courtdata +='<div class="speech-bubble" id="speech-bubble-id-'+obj.activity_log_id+'" style="display:none"><div id="next_hearing_'+obj.activity_log_id+'">'+obj.user_comment+'</div>';
     courtdata +='<div id="next-hearing-info_'+obj.activity_log_id+'" class="row activity-info col-sm-12">';
     courtdata +='<div class="col-sm-12 row"><label class="col-sm-4 d-flex"><b>Would you like to inform client(s)?</b></label>';
     courtdata +='<div class="col-sm-6 row">';

     courtdata +='<div class="form-radio">';
     courtdata +='<div class="radio radio-inline">';
     courtdata +='<label>';
     courtdata +='<input type="radio" class="chkPassport" name="inform_client" value="Yes">';
     courtdata +='<i class="helper"></i> Yes';
     courtdata +='</label>';
     courtdata +='</div>';
     courtdata +='<div class="radio radio-inline">';
     courtdata +='<label>';
     courtdata +='<input type="radio" class="chkPassport" name="inform_client" value="No">';
     courtdata +='<i class="helper"></i> No';
     courtdata +='</label>';
     courtdata +='</div>';
     courtdata +=' </div>';

     courtdata +='</div>';
     courtdata +='<div class="col-sm-4"><label>Status</label><select class="form-control" id="record_status_'+obj.activity_log_id+'"><option>Running</option></select></div>';

     var whoAttendedList = whoAttended(obj.activity_log_id,obj.who_attended);
     courtdata +='<div class="col-sm-12 row"><label class="col-sm-3 d-flex"><b>Who Attended</b></label>';
     courtdata +='<div class="row col-sm-9">'+whoAttendedList+'</div>';
     courtdata +='</div>';



     courtdata +='<div class="col-sm-12 row">';
     courtdata+='<div class="col-sm-4"><label>Stage</label> <input class="form-control" id="caseactivityinfo-stage_'+obj.activity_log_id+'" value="'+obj.stage+'"></div>';
     courtdata +='<div class="col-sm-4"><label>Posted For:</label> <input class="form-control" id="caseactivityinfo-posted_for_'+obj.activity_log_id+'" value="'+obj.posted_for+'"></div>'
     courtdata+='<div class="col-sm-4"><label>Action Taken</label> <input class="form-control" id="caseactivityinfo-action_taken_'+obj.activity_log_id+'" value="'+obj.action_taken+'"></div>';
     courtdata+='<div class="col-sm-4"><label>Next Hearing Date</label> <input class="form-control next_hearing_datepicker" id="caseactivityinfo-next_hearing_'+obj.activity_log_id+'" value="'+obj.next_hearing_date+'"></div>';
     courtdata+='<div class="col-sm-4"><label>Session</label> <select class="form-control" id="caseactivityinfo-sessions_'+obj.activity_log_id+'"><option value="">Please Select</option>';
     if(obj.session_phase == '1')
       courtdata +='<option value="1" selected>Morning</option><option value="2">Evening</option></select></div>';
     else
       courtdata +='<option value="1" >Morning</option><option value="2" selected>Evening</option></select></div>';
     courtdata +='</div>';

     courtdata +='<div class="col-sm-12" style="text-align:right"><button class="btn btn-info btn-sm" type="button" onclick=updateHearingDate('+obj.activity_log_id+','+obj.court_id+','+obj.case_id+','+obj.case_no+',"'+obj.CNR+'")>Update</button>';
     courtdata +=' <button class="btn btn-danger btn-sm" type="button" onclick="cancelActivityEdit('+obj.activity_log_id+')">Cancel</button></div></div>';
     courtdata +='</div>';

     courtdata+='</td>';         
     courtdata +='</tr>';

     if(isEmpty(obj.user_comment))
       $("#speech-bubble-id-"+obj.activity_log_id).hide();
     else
       $("#speech-bubble-id-"+obj.activity_log_id).show();
   }

 });

}
});
return data_td;
}

function updateCaseComment(id,courtid,caseid,caseno,cnr){
  var priorityVo = new Object();
  var caseid = $("#caseid").val();
  priorityVo.case_id = caseid;
  priorityVo.CNR = cnr;
  priorityVo.court_id = court_id_val;
  priorityVo.case_no = caseno;
  priorityVo.color = "";
  priorityVo.priority = "";
  priorityVo.activity_log_id = id;
  priorityVo.user_comment = tinymce.get('user-comment_'+id).getContent(); // query is the id of CKEDITOR
  priorityVo.activity_log_type = 'c';
  priorityVo.activity_flag = 'edit';
  if(priorityVo.user_comment=='' && priorityVo.activity_log_type == 'c'){
    toastr.warning("","Please insert some comment",{timeout:5000});
    return false;
  }
  var data_string = JSON.stringify(priorityVo);
  updateHearingDateList(data_string);
}

function deleteActivity(id){
  var yes = confirm("Are you sure you wanna delete?");
  if(yes){
    $.ajax({
      url : 'delete_activity_case.php',
      type : "POST",
      data : {
        activity_log_id : id
      },
      datatype : "json",
      beforeSend:function(){
        $(".flip-square-loader").show();
      },
      success:function(response){
        // console.log("delete activity",response);
        var response = JSON.parse(response);
        if(response.status == 'success')
          // view_case_details();
        toastr.success("","Successfully deleted the activity",{timeout:5000});
        document.getElementById("activity-tab").click();
      },
      complete:function(){
        $(".flip-square-loader").hide();
      },
      error:function(){
        toastr.error("","Error in deleting activity",{timeout:5000});
      }
    });
  }
  else
    return false;
}

function editHearingDate(id){
  tinymce.init({
    selector : "#next_hearing_"+id,
    plugins : [
    "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });
  $("#speech-bubble-id-"+id).show();
  $("#next-hearing-info_"+id).show();
  $("#caseactivityinfo-next_hearing_"+id).datepicker({
    dateFormat: 'yy/mm/dd',
    changeYear: true,
    changeMonth: true
  });
}

function updateHearingDate(id,courtid,caseid,caseno,cnr){
  var priorityVo = new Object();
  // var caseid = $("#caseid").val();
  priorityVo.case_id = caseid;


    // var courtid = $("#court-id").val();

    priorityVo.CNR = cnr;
    priorityVo.court_id = courtid;
    priorityVo.case_no = caseno;
    priorityVo.color = "";
    priorityVo.priority = "";
        // priorityVo.user_comment = CKEDITOR.instances.query.getData(); // query is the id of CKEDITOR
        priorityVo.user_comment = tinymce.get("next_hearing_"+id).getContent();
        priorityVo.status = $("#record_status_"+id+" option:selected").text();
        priorityVo.who_attended = [];


        $.each($("input[name='who_attended']:checked"),function(i,obj){
          priorityVo.who_attended.push(this.value);
          if($("input[name='who_attended']:checked").val() == 'Others')
            priorityVo.who_attended.push($("#who_attended_others").val());
        });


        priorityVo.stage = $("#caseactivityinfo-stage_"+id).val();
        priorityVo.posted_for = $("#caseactivityinfo-posted_for_"+id).val();
        priorityVo.action_taken = $("#caseactivityinfo-action_taken_"+id).val();
        priorityVo.next_hearing = $("#caseactivityinfo-next_hearing_"+id).val();
        priorityVo.session = $("#caseactivityinfo-sessions_"+id).val();
         // query is the id of CKEDITOR
         priorityVo.activity_log_type = 'h';
         priorityVo.activity_flag = 'edit';
         priorityVo.activity_log_id = id;

         // console.log("priorityVo",priorityVo);
         if(priorityVo.user_comment=='' && priorityVo.activity_log_type == 'c'){
          toastr.warning("","Please insert some comment",{timeout:5000});
          return false;
        }
        var data_string = JSON.stringify(priorityVo);
        buildHearingData('load');
        updateHearingDateList(data_string);
      }

      function cancelActivityEdit(id){
        tinymce.remove('#user-comment_'+id);
        $("#activity-info_"+id).hide();
      }

      function editActivityComment(id){
        tinymce.init({
         selector : "#user-comment_"+id,
         plugins : [
         "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });
        $("#activity-info_"+id).css('display','flex');
      }

      function getDocUrl(doc_name){
        if(!isEmpty(doc_name)){
         // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
      // $('#pdfUrl').attr('href',host+"/casepdf"+response.pdfFile); 
      return host+'/upload_document/'+doc_name;
    }
    else
     return 'images/business+costume+male+man+office+user+icon-1320196264882354682.png';
 }
 function getActivityUserProfile(doc_name){
   if(!isEmpty(doc_name)){
    // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
      // $('#pdfUrl').attr('href',host+"/casepdf"+response.pdfFile); 
      return host+'/uploads_profile/'+doc_name;
    }
    else
     return 'uploads_profile/femaleicon.png';
     // return 'images/business+costume+male+man+office+user+icon-1320196264882354682.png';
   }
   /* function buildAttachment(response){
      var data_td='';
      $.each(response.acitivity_log,function(i,obj){
        $(".case_title_div").css('background',obj.color_code);
        if(obj.activity_log_type == 'p'){        
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="images/business+costume+male+man+office+user+icon-1320196264882354682.png" alt="" title="Adv. Vidhi Sharma"></td>';
          data_td +='<td class="user_name">by '+response.user_name+'</td>';
          data_td +='<td><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case"></i> <span><i class="feather icon-calendar icons-view-case"></i>  '+response.created_date+'</span> <span> <i class="icofont icofont-time icons-view-case"></i> '+response.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr>';
          data_td +='<td></td>';
          if(isEmpty(obj.last_priority))
            data_td +='<td colspan="2" class="activity-chunks"><p><b>Priority </b> added <b>'+obj.priority+'</b></p></td>';
          else
            data_td +='<td colspan="2" class="activity-chunks"><p><b>Priority </b> changed from <b>'+obj.last_priority+'</b> to <b>'+obj.priority+'</b></p></td>';
          data_td +='</tr>';
          prioritySelected(obj.priority);
        }

        else if(obj.activity_log_type == 'c') {
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="images/business+costume+male+man+office+user+icon-1320196264882354682.png" alt="" title="Adv. Vidhi Sharma"></td>';
          data_td +='<td class="user_name">by '+response.user_name+'</td>';
          data_td +='<td><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case"></i> <span><i class="feather icon-calendar icons-view-case"></i>  '+response.created_date+'</span> <span> <i class="icofont icofont-time icons-view-case"></i> '+response.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr>';
          data_td +='<td></td>';
          data_td +='<td colspan="2"><div class="speech-bubble">'+obj.user_comment+'</div></td>';
          data_td +='</tr>';
        }
      });
      return data_td;
    }*/
    var teamMemberArray ='',teamId=[];

    function teamList(){
    /*var data = {
      teamArr : teamId
    }*/
    $.ajax({ 
     url: host+"/team_member_ajax.php",
     type:'POST',
     dataType: 'json',
     data : {
      case_id : caseid,
    },
    async:false,
    success:function(data){            
        // console.log(data);
        var options = '';
        $.each(data,function(i,obj){
          options +='<option value="'+obj.reg_id+'">'+obj.name+' '+obj.last_name+'</option>';
        });
        // console.log("option",options);
        teamListArray = options;
      },
      error:function(){
       toastr.error("","Error in loading team list",{timeout:5000});
     }
   });
  }


  function buildTeamMember(response){
   teamMemberArray = response;
   var table = '';
   if(!isEmpty(response)){
    $("#team_member_count").text(response.length);
    var tr = '';
    $.each(response,function(i,obj){
     teamId.push(obj.team_id);
     var email = isEmpty(obj.email) ? "" : obj.email;
     var mobile = isEmpty(obj.mobile) ? "" : obj.mobile;
     if(!isEmpty(obj.first_name) || !isEmpty(obj.last_name)){
      tr+='<tr>';
      tr+='<td>'+obj.first_name+'</td>';
      tr+='<td>'+obj.last_name+'</td>';
      tr+='<td>'+email+'</td>';
      tr+='<td>'+mobile+'</td>';
      tr+='<td>'+obj.address+'</td>';
    }
    // tr+='<td><button class="btn btn-danger btn-sm" type="button" onclick="deleteteamMember()"><i class="feather icon-trash btn btn-outline-danger btn-mini"></i></button><tr>';
  });
    if(tr!=''){
     table = '<table class="table table-bordered export_table"><thead>';
     table +='<tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Contact</th><th>Address</th></tr></thead><tbody>';
     table += tr +'</tbody></table>';
   }
    // table +='<div class="col-sm-12 row p-0">';
    // table+='<div class="col-sm-5"></div>';
    // table+='<div class="col-sm-5 p-0" style="text-align:right" id="muliselect-team-div">';
    // table+='<select class="form-control select2" id="team_multiselect" multiple>'+teamListArray+'</select></div>';
    // table+='<div class="col-sm-2 p-0" style="text-align:right">';
    // table+='<button class="btn btn-info btn-sm" type="button" onclick="assignTeamMeambers()">Assign</button></div></div>';
    $("#team-member-table-div").html(table);

    $('#team_multiselect').select2({
      placeholder: 'Select...',
      closeOnSelect: false,
      multiple: true,
    });
    $("#team_multiselect").val(teamId);
    $("#team_multiselect").select2().trigger('change');
    /*  $("#team_multiselect").multiselect('refresh');*/

  }
  var who_attended='';

  $.each(response,function(i,obj){

    who_attended += '<div class="checkbox-zoom zoom-primary">';
    who_attended += '<label>';
    who_attended += '<input type="checkbox" class="chkPassport" value="'+obj.team_id+'" name="who_attended">';
    who_attended += '<span class="cr">';
    who_attended += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
    who_attended += '</span>';
    who_attended += '<span> '+obj.first_name+' '+obj.last_name+'</span>';
    who_attended += '</label>';
    who_attended += ' </div>';    
  });
  who_attended += '<div class="checkbox-zoom zoom-primary">';
  who_attended += '<label>';
  who_attended += '<input type="checkbox" class="chkPassport" value="Others" name="who_attended">';
  who_attended += '<span class="cr">';
  who_attended += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
  who_attended += '</span>';
  who_attended += '<span> Others</span>';
  who_attended += '</label>';
  who_attended += ' </div>';  

  $("#who_attended_div").html(who_attended);

}


function assignTeamMeambers(){
  var team_member_array = [];
  var team_multiselect = $("#team_multiselect").val();
  $.each(team_multiselect,function(i,obj){
    team_member_array.push(obj);
  });
  var teamVo = new Object();
  teamVo.team_member_array = team_member_array;
  teamVo.case_id = caseid;
  teamVo.relateto = $("#sub_case_title").text()
  $.ajax({ 
    url: host+"/submit_view_team.php",
    type:'POST',
    data: "viewtododetail="+JSON.stringify(teamVo),
    dataType: 'json',
    async:false,
    success:function(data){            
      // console.log(data);
      if(data.status == 'success'){
        toastr.success("","Successfully changed case priority value",{timeout:5000});
          // CKEDITOR.instances.query.setData('');
          tinymce.get('query').setContent('');
          view_case_details('t');
        }
        else
         toastr.success("","Error in changing case priority value",{timeout:5000});  
     },
     error:function(){

     }
   });
}

function whoAttended(id,whoAttendedArray){
  var response = teamMemberArray;
  var who_attended_div_list='';
  var whoAttendedId = [];
  $.each(whoAttendedArray,function(i,obj){
    whoAttendedId.push(obj.who_attended);
  });
  $.each(response,function(i,obj){
    who_attended_div_list += '<div class="form-check col-sm-4">';
    who_attended_div_list += '<label class="form-check-label checkboxlabel">';
    if(whoAttendedId.includes(obj.team_id)){
      who_attended_div_list +='<div class="checkbox-zoom zoom-primary">';
      who_attended_div_list +='<label>';
      who_attended_div_list +='<input type="checkbox" class="chkPassport" value="'+obj.team_id+'" checked name="who_attended"><span class="cr">';
      who_attended_div_list +='<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
      who_attended_div_list +='</span> <span>'+obj.first_name+' '+obj.last_name+'</span>';
      who_attended_div_list +='</label>';
      who_attended_div_list +='</div>';

    }
    else{
      who_attended_div_list +='<div class="checkbox-zoom zoom-primary">';
      who_attended_div_list +='<label>';
      who_attended_div_list +='<input type="checkbox" class="chkPassport" value="'+obj.team_id+'" name="who_attended"><span class="cr">';
      who_attended_div_list +='<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
      who_attended_div_list +='</span> <span>'+obj.first_name+' '+obj.last_name+'</span>';
      who_attended_div_list +='</label>';
      who_attended_div_list +='</div>';
    }
    who_attended_div_list += '</label>';
    who_attended_div_list += '</div>';

  });
  who_attended_div_list +='<div class="form-check col-sm-3">';
  who_attended_div_list +='<div class="checkbox-zoom zoom-primary">';
  who_attended_div_list +='<label>';
  who_attended_div_list +='<input type="checkbox" class="chkPassport" name="who_attended" value="Others"><span class="cr">';
  who_attended_div_list +='<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
  who_attended_div_list +='</span> <span>Others</span>';
  who_attended_div_list +='</label>';
  who_attended_div_list +='</div>';
  who_attended_div_list +=' </div>'
      // $("#who_attended_div_"+id).html(who_attended);
      return who_attended_div_list;
    }

    function prioritySelected(data){
     var objSelect = document.getElementById("set_priority_select");
     setSelectedValue(objSelect,data);  

   }
   function setSelectedValue(selectObj, valueToSet) {
     for (var i = 0; i < selectObj.options.length; i++) {
      if (selectObj.options[i].value== valueToSet) {
       selectObj.options[i].selected = true;
       return;
     }
   }
 }

 $("#set_priority_select").on('change',function(){
   var priority = $(this).val();
   var caseid = $("#caseid").val();
    // var courtid = $("#court-id").val();
    if(priority == 'Super Critical'){
      $(".case_title_div").css('background','red');
      color = 'red';
    }
    else if(priority == 'Critical'){
      $(".case_title_div").css('background','#ffeedd');
      color = '#ffeedd';
    }
    else if(priority == 'Important'){
      $(".case_title_div").css('background','#ffffdd');
      color = '#ffffdd';
    }
    else if(priority == 'Routine'){
      $(".case_title_div").css('background','#ceffff');
      color = '#ceffff';
    }
    else if(priority == 'Others'){
      $(".case_title_div").css('background','#eef8fb');
      color = '#eef8fb';
    }
    else if(priority == 'Normal'){
      $(".case_title_div").css('background','#ffffff');
      color = '#ffffff';
    }

    var priorityVo = new Object();
    priorityVo.case_id = caseid;
    priorityVo.CNR = cnr;
    priorityVo.court_id = court_id_val;
    priorityVo.case_no = case_no;
    priorityVo.color = color;
    priorityVo.case_id = caseid;
    priorityVo.priority = priority;
    priorityVo.user_comment = '';
    priorityVo.activity_log_type = 'p';
    priorityVo.activity_flag = 'submit';

    var data_string = JSON.stringify(priorityVo);

    updateHearingDateList(data_string);

  });

 function updateHearingDateList(data){
   $.ajax({ 
    url: host+"/user_activity_log.php",
    type:'POST',
    data: {
     activity_log : data
   },
   dataType: 'json',
   async:false,
   success:function(data){            
     // console.log(data);
     if(data.status == 'success'){
      toastr.success("You have successfully update the case activity",{timeout:5000});
      // CKEDITOR.instances.query.setData('');
      tinymce.get('query').setContent('');
      $("input[name='recordHearingradio'][value='No']").prop("checked",true);
      $(".case-activity-info").hide();
      $(".case-activity-info input").val('');
      // $("#activity-history").load(window.location.href + "#activity-history");
      // $( "#activity-history" ).load(window.location.href + " #activity-history" );
      view_case_details('c');
    }
    else
     toastr.error("","Error in updating activity",{timeout:5000});  
 },
 error:function(){
   toastr.error("","Error in updating activity",{timeout:5000});
 }
});
 }


 function submitCaseComment(activity_type){
   var priorityVo = new Object();
   var caseid = $("#caseid").val();
   priorityVo.case_id = caseid;
    // var courtid = $("#court-id").val();

    priorityVo.CNR = cnr;
    priorityVo.court_id = court_id_val;
    priorityVo.case_no = case_no;
    priorityVo.color = "";
    priorityVo.priority = "";
    priorityVo.case_id = caseid;
    priorityVo.user_comment = tinymce.get('query').getContent();
        // priorityVo.user_comment = CKEDITOR.instances.query.getData(); // query is the id of CKEDITOR
        priorityVo.activity_log_type = 'c';

        if($("input[name='recordHearingradio']:checked").val() == 'Yes'){
          priorityVo.status = $("#record_status option:selected").text();
          priorityVo.who_attended = [];


          $.each($("input[name='who_attended']:checked"),function(i,obj){
            priorityVo.who_attended.push(this.value);
            if($("input[name='who_attended']:checked").val() == 'Others')
              priorityVo.who_attended.push($("#who_attended_others").val());
          });


          priorityVo.stage = $("#caseactivityinfo-stage").val();
          priorityVo.posted_for = $("#caseactivityinfo-posted_for").val();
          priorityVo.action_taken = $("#caseactivityinfo-action_taken").val();
          priorityVo.next_hearing = $("#caseactivityinfo-next_hearing").val();
          // priorityVo.session = $("#caseactivityinfo-sessions").val();
         // query is the id of CKEDITOR
         priorityVo.activity_log_type = 'h';

       }
       priorityVo.activity_flag = activity_type;
       // console.log("priorityVo",priorityVo);
       if(priorityVo.user_comment=='' && priorityVo.activity_log_type == 'c'){
        toastr.warning("","Please insert some comment",{timeout:5000});
        return false;
      }
      var data_string = JSON.stringify(priorityVo);
      updateHearingDateList(data_string);
    }

  // .......................Added note tab js..........................

  load_nodecount();

  function load_nodecount() {
    var caseid = $("#caseid").val();
    $.ajax({
      url: "ajaxdata/getNumberCount.php",
      type: 'POST',
      data: {caseid: caseid},
      cache: false,
      success: function (html) {
        $("#notes-counter").html(" ("+html+")");
      }
    });
  }

  function deleteNote(id,case_id) {
    var yes = confirm("Are you sure you want to delete?");
    if (yes)
      $.ajax({
        url: "ajaxdata/deletenote.php",
        type: 'POST',
        data: {
          noteid: id,
          case_id: case_id
        },
        success: function (data) {
          // console.log(data);
          buildNotes(data,case_id);
        }
      });

  }

    //NOTE 
    $("#btnNoteSave").click(function () {
      var notes = $("#notes").val();
      var caseid = $("#caseid").val();

      var radioValue = $("input[name='np']:checked").val();
      var notes_file = document.getElementById("notes-file").files[0]

      if (notes == '') {
        alert("Please enter notes");
        return false;
      }

      if (radioValue == undefined) {
        alert("Select Mark As");
        return false;
      }
      var formData = new FormData();

      formData.append("note",notes);
      formData.append("private",radioValue);
      formData.append("caseid",caseid);
      formData.append("flag",'insert');
      formData.append("notes_file",notes_file);

      var ajaxResult = $.ajax({
       url : "ajaxdata/savenote.php",
       type : 'POST',
       data : formData,
       cache : false,
       contentType : false,
       processData : false,
       success:function(response){
        // console.log(response);

        $("#notes").val('');
        $("#notes-file").val('');
        buildNotes(response,caseid);
      },
      error:function(){
        toastr.error("","Error in adding case",{timeout:5000});
      }
    });

    });
    $("#next-hearing-date").datepicker();
    $("#btnNoteClear").click(function () {
      $("#notes").val('');
    });

    //END NOTE

    function getNotes(case_id){
      $.ajax({
        url : "ajaxdata/savenote.php",
        type : 'POST',
        data: {
          case_id: case_id
        },
        cache: false,
        dataType: 'json',
        success : function(data){
          // console.log(data);
          buildNotes(data,case_id);
      // if (data.status) {
                  // var    tr = '<table class="table table-bordered export_table" id="timesheettable">';
                  
                  // $("#notes").val('');
                // }
              }
            });
    }

    function btnNoteCancel(id){
     $("#note_div_"+id).show();
     $("#notetxt_"+id).hide();
   }


   function editNotes(id){
     $("#note_div_"+id).hide();
     $("#notetxt_"+id).show();

     
      // $("#activity-info_"+id).css('display','flex');
    }

    function buildNotes(data,case_id){
      $("#note_block").html('');
      var tr ='';
      $.each(data,function(i,obj){
       //tr +='<div class="row">';
       tr +='<div class="col" id="note_'+obj.lastid+'" style="border:1px solid #ccc;background: #fff;margin-top:10px;">';
       tr +='<div class="row" style="background: #fff;">';
       tr +='<div class="col col-md-50">';
       tr +='<div class="row justify-content-end" style="background: #fff;">';
       tr +='<div class="col case-note-item-actions" style="text-align:right;padding:5px;">';
       if(obj.private == 'yes'){
        tr +='<span id="note-creater" style="border-right: 1px solid;padding-right: 6px;font-weight:bold;">Private</span>';
      }
      tr +='<b id="note-creater" style="border-right: 1px solid;padding-right: 6px;">';
      tr += obj.name+' '+obj.last_name+'</b>';
      tr +='<b id="note-created-date" style="border-right: 1px solid;padding-right: 6px;">';
      tr += obj.datetime+'</b>';
      tr +='<span style="padding:5px" id="'+obj.file_name+'" onclick="getNotesFile(this.id)"><i class="fa fa-file-image-o"></i></span>';
      tr +='<span class="download-item" style="padding:5px"> ';
      tr +='<a onclick="downloadNotePdf('+obj.lastid+')" class="sprite download_icon" title="Download" data-pjax="">';
      tr +=' <i class="fa fa-download" aria-hidden="true"></i></a></span>';
      tr +='<span class="edit-item" style="padding:5px">';
      tr +='<a onclick="editNotes('+obj.lastid+')" class="case-notes-edit sprite edit-icon" title="Edit" data-ntype-checked="false">';
      tr +=' <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>';
      tr +='<span class="delete-item" style="padding:5px">';
      tr +='<a onclick="deleteNote('+obj.lastid+','+case_id+')" class="case-notes-delete sprite delete_icon" title="Delete" data-requestrunning="false">';
      tr +='<i class="fa fa-trash-o" aria-hidden="true"></i></a></span></div></div></div></div>';
      tr +='<div class="col-sm-12 row editNoteBlock" style="display: none;" id="notetxt_'+obj.lastid+'">';
      tr +='<textarea id="notetxtarea_'+obj.lastid+'" rows="5" class="form-control form-group">'+obj.note+'</textarea>';
      tr +='<div class="row col-sm-12"><div class="col-sm-10 form-group row p-0"><b class="col-sm-3">Mark this note as private </b>';
                      // tr +='<div class="row"><div class="col row"><b>Mark this note as private </b>';
                      tr +='<div class="form-radio">';
                      if(obj.private == 'yes'){
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="yes" checked><i class="helper"></i> Yes</label></div>';
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="no"><i class="helper"></i> No</label></div>';
                      }
                      else if(obj.private == 'no'){
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="yes"><i class="helper"></i> Yes</label></div>';
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="no" checked><i class="helper"></i> No</label></div>';
                      } else{
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="yes"><i class="helper"></i> Yes</label></div>';
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="no"><i class="helper"></i> No</label></div>';
                      }
                      tr +='</div>';
                      tr +=' <div class="col row form-group"><input type="file" name="notes-file" id="notes-files"><p id="existingfilename" style="width:100%">File name: '+obj.file_name+'</p></div>';
                      tr +='</div>';
                      tr +='<div class="col-sm-2"><button class="btn btn-primary btn-sm" type="button" onclick=btnUpdateNote('+obj.lastid+',"'+case_id+'")>Update</button>';
                      tr +=' <button class="btn btn-danger btn-sm" type="button" onclick="btnNoteCancel('+obj.lastid+')">Cancel</button></div>';
                      tr +='</div>';
                      tr +='</div>';
                      tr +='<div class="col" style="padding:5px;" id="note_div_'+obj.lastid+'">'+obj.note+'</div></div>';
                      //tr+='</div>';
                    });
                  // $(data.data).insertBefore("#note_block .content:first-child");

                  $("#note_block").html(tr);
                }

                $(document).on('change','#notes-files',function(e){
                  $("#existingfilename").text('File name: '+e.target.files[0].name);
                })

                function getNotesFile(filename){


                  if(!isEmpty(filename)){
                    var fileExtension = ['doc','docx'];
                    if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 0 || $.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 1) {
                      // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
                      $("#notes-file-iframe").attr('src',host +"/upload_document/"+filename);
                      $("#notes_image_modal").modal('Hide');

                    }
                    $("#notes-file-iframe").attr('src',host +"/upload_document/"+filename);
                    $("#notes_image_modal").modal('show');
                  }
                }
                function btnUpdateNote(id,caseid){
                  var content = $("#notetxtarea_"+id).val();
                  //var radioValue = type;
                  var radioValue = $("input[name='enp']:checked").val();
                  var notes_files = document.getElementById("notes-files").files[0];
                  //$('input[name=enp][value='+radioValue+']').attr('checked', true);
                  var formData = new FormData();

                  formData.append("notes",content);
                  formData.append("type",radioValue);
                  formData.append("noteid",id);
                  formData.append("notes_files",notes_files);


                  var updatenotes = $.ajax({
                    url: "ajaxdata/updatednote.php",
                    type: 'POST',
                    data: formData,
                    cache : false,
                    contentType : false,
                    processData : false,
                    success: function (data) {
                      // console.log(data);
                      if (data.status) {
                        $("#notetxt_"+id).hide();
                        $("#note_div_"+id).show();
                        $("#note_div_"+id).html(content);
                        getNotes(caseid);
                      }
                    }
                  });

                }



                function getCasesReferredList(){
                 var caseid = $("#caseid").val();
                 $.ajax({
                  url: "show_supporting_cases.php",
                  type: 'POST',
                  data: {
                   caseid: caseid
                 },
                 cache: false,
                 dataType: 'json',
                 success: function (data) {
                //alert(data);
                // console.log(data);
                $.each(data,function(i,obj){
                  tr+='<tr>';
                  tr+='<td></td>';
                  tr+='<td><i class="feather icon-external-link"></i></td>';
                  tr+='</tr>';
                });
                if(tr!=''){
                  var table = '<table class="table table-bordered"><thead><tr><th>Citation</th><th>Action</th></tr></thead><tbody>';
                  table+=tr+'</tbody></table>';
                }
                $("#show_lawdata").html(table);
              },
              error:function(){
               toastr.error("","Error in fetching cases referred list",{timeout:5000});
             }
           });


               }

               function getCasesReffered(){
                 var tr='';
                 var caseid = $("#caseid").val();
                 $.ajax({
                  url: "show_supporting_cases.php",
                  type: 'POST',
                  data: {
                   caseid: caseid
                 },
                 cache: false,
                 dataType: 'json',
                 success: function (response) {
                //alert(data);
                // $("#connected-case").text(response.count);
                var table = '<table class="table table-bordered" id="law_table"><thead class="bg-primary"><tr><th>Sr No</th><th>Citation</th><th>Searched Word</th><th>Product</th><th>View Judgement</th></tr></thead><tbody>';
                
                $.each(response.data,function(i,obj){
                  i++;

                  var online = obj.product_type;
                  tr+='<tr>';
                  tr+='<td>'+i+'</td>';
                  tr+='<td>'+obj.citation+'' +obj.dod+'</td>';
                  tr+='<td>'+obj.search+'</td>';
                   tr+='<td>'+obj.product_type+'</td>';
                   if(obj.product_type == 'AIR Online')
                  tr+='<td onclick=getcitation(this.id) id="'+online+'_'+obj.citation+'" style="text-align:center;">';
                else
                   tr+='<td onclick=viewfulljudgmentforIndiankanoon(this.id) id="'+obj.judgement_href+'" style="text-align:center;">';
                   tr+='<i class="feather icon-external-link"></i></td>';
                  tr+='</tr>';
                });
                table += tr+'</tbody></table>';
                // if(tr!=''){

                // }
                // $("#law_table").dataTable();
                $("#show_lawdata").html(table);
                $("#law_table").DataTable();
              },
              error:function(){
               toastr.error("","Error in fetching cases referred list",{timeout:5000});
             }
           });
               }

               function viewfulljudgmentforIndiankanoon(citname){
  window.open(citname);
}


               function getcitation(onlineget){
                 var citvar = onlineget.split("_");
                 var online = citvar[0];
                 var citation = citvar[1];
                 var search = $("#search").val();
                 $.ajax({ 
      // url: "http://192.168.0.121:8080/qtool/api/qtool/judgement",
      url: qtoolurl+"api/qtool/judgement",
      type:'get',
      data: {
        citation:citation,
        product: online
      },
      dataType: 'json',
      async:false,
      headers: {
        "Content-Type": 'application/json',
        "Authorization":"Bearer "+localStorage.getItem('auth_token')
      },
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(response){  
     // console.log(response);
     // var content = replace(search,response.content);
     var printWin = window.open(''); 
     printWin.document.write('<html><head><title>'+citation+'</title><link rel="stylesheet" type="text/css" href="styles/showjudgment.css"><link rel="stylesheet" href="css/bootstrap.min.css" /></head><body>'); 
     printWin.document.write(response.content); 
     printWin.document.write("</body></html>");
   },
   complete: function(){
    $('.flip-square-loader').hide();  

  },
  error: function(e){

    $('.flip-square-loader').hide();  
    toastr.error('Unable to load Citation', 'Sorry For Inconvenience!', {timeOut: 5000})
  }
});
               }

               function deletecase (id) {
                 var yes = confirm('All the documents, notes Etc. associated to this case will be deleted Are you sure? this action cant be undone.');
                 if(yes){
                  var caseid = id;
                  $.ajax({
                   type:'POST',
                   url:'delete_cases.php',
                   data:'case_id='+caseid,
                   beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
    // alert("Case Data Removed Successfully");
    toastr.success('', 'Case Data Removed Successfully', {timeOut: 5000})
    window.location.href='cases-report.php';
       // $('#delcase').html(html);
     },
     complete: function(){
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Case', {timeOut: 5000})
    }
  }); 
                }
                else{
                  return false;
                }
              }
              /*function replace(query,content) { var s = query.split(" "); var keys = keywordarray(); s.forEach(function(i,e){ if(!keys.includes(i)) content= i && content ? content.replace(new RegExp(escapeRegexp(i), 'gi'), "<span style='background:yellow'>$&</span>") : content; }); return content; }*/


