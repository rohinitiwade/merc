function exportPdf(type,page){
	if(type == 'pdf'){
		var doc = new jsPDF('landscape','pt', 'a4');

		var elem = document.getElementsByClassName('main-table');
		$.each(elem,function(i,obj){
			var data = doc.autoTableHtmlToJson(obj);
			doc.autoTable(data.columns, data.rows, {
				startY: doc.autoTableEndPosY()+20,
				pageBreak: 'auto',
				"styles": { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "halign": "justify", "fontSize": "10", "lineColor": "100", "lineWidth": ".25" },
				drawHeaderRow: function(row, data) {
					if (data.pageCount > 1) {
						return false;
					}

				}
			});
		});
		setTimeout(function() {
			doc.save(page + new Date().toISOString().replace(/[\-\:\.]/g, "") +'.pdf');
		}, 1000);
	}
	else if(type == 'excel'){
		var table = document.getElementsByClassName('main-table');
		if(table && table.length){
						// var preserveColors = (table.hasClass('table2excel_with_colors') ? true : false);
						$(table).table2excel({
							exclude: ".noExl",
							name: "Excel Document Name",
							filename: page + new Date().toISOString().replace(/[\-\:\.]/g, "") + ".xls",
							fileext: ".xls",
							exclude_img: true,
							exclude_links: true,
							exclude_inputs: true,
							// preserveColors: preserveColors
						});
					}
				}
			}