$(document).ready(function(){
	/*var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	var time = today.getHours() + ":" + today.getMinutes();
	var dateTime = date+' '+time;*/
	/*$('#datepicker').datetimepicker({
		uiLibrary: 'bootstrap4',
		footer: true,
		format: 'yyyy-mm-dd HH:MM',
		value: dateTime,
		autoclose: true
	});
	$('#datepicker1').datetimepicker({
		uiLibrary: 'bootstrap4',
		footer: true,
		format: 'yyyy-mm-dd HH:MM',
		value: dateTime,
		autoclose: true
	});
	Date.prototype.toDateInputValue = (function() { 
		var local = new Date(this);
		local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
		return local.toJSON().slice(0,10);
	});*/
	$("#datepicker_todo").datepicker();
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	var time = today.getHours() + ":" + today.getMinutes();
	var dateTime = date+' '+time;
	jQuery(function(){
		jQuery('#datepicker').datetimepicker({
			format:'d-m-Y H:i',
			timepicker:true,
			mask:true,
			step: 5,
			value : dateTime,
			onShow:function( ct ){
				this.setOptions({
					minDate:jQuery('#datepicker1').val()?jQuery('#datepicker1').val():false
				})
			},
		});
		jQuery('#datepicker1').datetimepicker({
			format:'d-m-Y H:i',
			timepicker:true,
			mask:true,
			step: 5,
			value : dateTime,
			onShow:function( ct ){
				this.setOptions({
					minDate:jQuery('#datepicker').val()?jQuery('#datepicker').val():false
				})
			},			
		});
		// jQuery('#datepicker_todo').datetimepicker({
		// 	format:'d-m-Y H:i',
		// 	timepicker:true,
		// 	mask:true,
		// 	step: 5,
		// 	value : dateTime,
		// 	onShow:function( ct ){
		// 		this.setOptions({
		// 			minDate:jQuery('#datepicker1_todo').val()?jQuery('#datepicker1_todo').val():false
		// 		})
		// 	},

		// });
		// jQuery('#datepicker1_todo').datetimepicker({
		// 	format:'d-m-Y H:i',
		// 	timepicker:true,
		// 	mask:true,
		// 	step: 5,
		// 	value : dateTime,
		// 	onShow:function( ct ){
		// 		this.setOptions({
		// 			minDate:jQuery('#datepicker_todo').val()?jQuery('#datepicker_todo').val():false
		// 		})
		// 	},

		// });
		
	});


	/*jQuery('#datepicker').datetimepicker({
		timepicker:true,
 		mask:true, // '9999/19/39 29:59' - digit is the maximum possible for a cell
 		format:'d.m.Y H:i',
 	});
	jQuery('#datepicker1').datetimepicker({
		timepicker:true,
 		mask:true, // '9999/19/39 29:59' - digit is the maximum possible for a cell
 		format:'d.m.Y H:i',
 	});*/
	/*$('#framework').multiselect({
		nonSelectedText: 'Select Team Members',
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		buttonWidth:'400px'
	});*/

	$('#framework_form').on('submit', function(event){
		event.preventDefault();
		var form_data = $(this).serialize();
		$.ajax({
			url:"insert.php",
			method:"POST",
			data:form_data,
			success:function(data)
			{
				$('#framework option:selected').each(function(){
					$(this).prop('selected', false);
				});
				$('#framework').select2();
				alert(data);
			}
		});
	});
	$('#relate').select2({
		ajax: {
			url: './ajaxdata/relatedto.php',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}
		      // Query parameters will be ?search=[term]&type=public
		      return query;
		  },
		  processResults: function(data) {
		  	// console.log(data);
		  	if (data.length)
		  		return {
		  			results: data
		  		};
		  	}
		  }
		});
	getTeamMemberListDropdown();
	getAdvocates();
});

function getTeamMemberListDropdown(){
// var hidValue = $("#hidSelectedOptions").val();
var selectedOptions = [];

$.ajax({
	type:'POST',
	url:host+'/your_team.php',
	dataType:"json",
	success:function(response){
		// debugger;
		// console.log(response);
// var response = JSON.parse(response);
if(response.team.length > 0){
	var option = '<option value="">Select</option>';
	$.each(response.team,function(i,obj){
		option += '<option value='+obj.reg_id+'>'+obj.name+' '+obj.last_name+'</option>';
	});
	$('#framework').html(option);
	$('#framework').select2({
		placeholder: 'Select...',
		closeOnSelect: false,
		multiple: true,
		allowClear: false
	});
}
$.each(response.show_team,function(i,obj){
	selectedOptions.push(obj.reg_id);
});
$('#framework').val(selectedOptions).trigger('change');
},
error:function(){
	toastr.error("","Error fetching team list",{timeout:5000});
}
});
}

function getAdvocates(){
// var hidValue = $("#hidSelectedOptions").val();
var selectedOptionsa = [];

$.ajax({
	type:'POST',
	url:host+'/your_advocates.php',
	dataType:"json",
	success:function(response){
		// console.log(response);
// var response = JSON.parse(response);
if(response.team.length > 0){
	var option = '<option value="">Select</option>';
	$.each(response.team,function(i,obj){
		option += '<option value='+obj.reg_id+'>'+obj.name+'</option>';
	});
	$('#advocate').html(option);
	$('#advocate').select2({
		placeholder: 'Select...',
		closeOnSelect: false,
		multiple: true,
		allowClear: false
	});
}
$.each(response.show_team,function(i,obj){
	selectedOptions.push(obj.reg_id);
});
$('#advocate').val(selectedOptionsa).trigger('change');
},
error:function(){
	toastr.error("","Error fetching team list",{timeout:5000});
}
});
}