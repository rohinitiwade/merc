
jQuery(document).bind("keyup keydown", function (e) {
//The key "P" on chrome is seen as e.keyCode == 80, on opera it is e.charCode == 16, while on firefox it is e.charCode == 112.
if (e.ctrlKey && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) {  
    e.preventDefault();
    printCase();
}
});

function printCase(){
	buildExportReport();
    $("#complete_view_data").show();
	var printWin = window.open("", "", "left=0,top=0,width=700,height=600,toolbar=0,scrollbars=0,status=0");
    $("#complete_view_data img").hide();
    printWin.document.write('<html><head><title></title><link rel="stylesheet" type="text/css" href="styles/bootstrap.min.css"></head><body>');
    printWin.document.write($("#complete_view_data").html());
    printWin.document.write("</body></html>");
    printWin.document.close();
    printWin.print();
    $("#complete_view_data").hide();

                     /*   if (printWin.attachEvent)
                        	printWin.attachEvent('onload', _print);
                        else
                        	printWin.addEventListener('load', _print);*/
                    }
                    /*function _print() {
                    	printWin.focus();

                    	printWin.close();
                    	if (printWin.detachEvent)
                    		printWin.detachEvent('onload', _print);
                    	else
                    		printWin.removeEventListener('load', _print);
                    }*/