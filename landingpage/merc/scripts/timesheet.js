function getTimesheet(){
  $('#updatetimesheet').hide();
  var timesheetcase = $("#case").val();
  var userid = $("#userid").val();
  $.ajax({
    url : "ajaxdata/gettimesheet.php",
    type : 'POST',
    data: {
      case_id: timesheetcase,
      user_id: userid
    },
    cache: false,
    dataType: 'json',
    success : function(response){
      console.log(response);
      // $.each(response,function(i,obj){
     /*if(obj == "No data found" )
     {
      $("#error_message").html('No data Available in Timesheet');
    }*/
    // if(obj.status == 'Success'){  
     var    tr = '<table class="table table-bordered export_table" id="timesheettable">';
     tr +='<thead><tr><th>#</th><th>Date</th><th>Particulars</th><th>Timespent(in Hours)</th><th>Type</th><th>Action</th></tr></thead><tbody>';  
     $.each(response,function(i,obj){
      tr +='<tr>';
      tr +='<td style="text-align:center;">';
      tr+='<div class="checkbox-zoom zoom-primary">';
      tr+='<label>';
      tr+=' <input type="checkbox" value="" name="" class="checked_checkbox">';
      tr+='<span class="cr">';
      tr+=' <i class="cr-icon icofont icofont-ui-check txt-primary">';
      tr+='</i>';
      tr+=' </span>';
      tr+=' </label>';
      tr+=' </div>';
      tr+='</td>';
      tr +='<td>'+obj.edate+'</td>';
      tr +='<td>'+obj.particulars+'</td>';
      tr +='<td>'+obj.timespent+':'+obj.timeinmin+'</td>';
      tr +='<td>'+obj.types+'</td>';
      tr+='<td><i class="fa fa-edit action-css" id="'+obj.particulars+'_'+obj.types+'" onclick=editTimesheet(this.id,'+obj.timespent+','+obj.timeinmin+',"'+obj.edate+'","'+obj.id1+'")></i>&nbsp<i class="fa fa-trash" onclick=deleteTimesheet('+obj.id1+')></i></td></tr>';
    });
     tr+= '</tbody></table>';
     $("#timesheetlist").html(tr);
     $("#timesheettable").DataTable();
   // }
      // });
    },error:function(){
      toastr.error("Timesheet ",{timeout:5000});
    }
  });
}



function submitTimesheet(){
  $("#error_message").hide();
  $('#updatetimesheet').hide();
  var timesheetcase = $("#case").val();
  var userid = $("#userid").val();
  var particular = $("#particulars").val();
  var datepickertimesheet = $("#datepickertimesheet").val();
  var timespent = $("#timespent").val();
  var timeinmin = $("#timeinmin").val();
    // var types = $("#type").val();
    var type= $("input[type='radio'][name='type']:checked").val();

    var formData = new FormData();
    formData.append('case_id', timesheetcase);
    formData.append('user_id', userid);
    formData.append('particulars', particular);
    formData.append('date1', datepickertimesheet);
    formData.append('timespent', timespent);
    formData.append('timeinmin', timeinmin);
    formData.append('type', type);
    // $("input[type='type']").val(type).prop("checked",true);
    formData.append('submit1', 'submit');
  // timeVo.particulars=particular;
  // timeVo.stdate=datepickertimesheet;
  // timeVo.timespent=timespent;
  // timeVo.timeinmin=timeinmin;
  // timeVo.types =types ;

  if(particular == ''){
    toastr.warning("Please enter Particular ",{timeOut: 5000});
    return false;
  }
  else if(datepickertimesheet == ''){
    toastr.warning("Please select Date ",{timeOut: 5000});
    return false;
  }
  else if(timespent == '' || timeinmin == ''){
    toastr.warning("Please select Time spent ",{timeOut: 5000});
    return false;
  }
  var ajaxReq = $.ajax({
    url : "ajaxdata/posttimesheet.php",
    type : 'POST',
    data : formData,
        // data: JSON.stringify(timeVo),
        cache : false,
        contentType : false,
        processData : false,
        beforeSend : function(){
          $(".flip-square-loader").show();
        }
      });


    // Called on success of file upload
    ajaxReq.done(function(response) {
     $(".flip-square-loader").hide();
     $("#timesheetlist").html('');


     console.log(response);
     if(response.status == 'Success')
      toastr.success("Timesheet submitted successfully",{timeOut: 5000});
    var response_val = JSON.parse(response);
    var tr ='';
      // if(response.status == 'FAILED')

      tr += '<table class="table table-bordered export_table" id="timesheettable">';
      tr +='<thead><tr><th>#</th><th>Date</th><th>Particulars</th><th>Timespent(in Hours)</th><th>Type</th><th>Action</th></tr></thead>';
      $.each(response_val,function(i,obj){
        // $("#view_case_document").text(response.doc_count);
        // var filesize = bytesToSize(obj.size);
        tr +='<tbody><tr>';
        tr +='<td style="text-align:center;"><input type="checkbox" value="" name="" class="checked_checkbox"></td>';
        tr +='<td>'+obj.edate+'</td>';
        tr +='<td>'+obj.particulars+'</td>';
        tr +='<td>'+obj.timespent+':'+obj.timeinmin+'</td>';
        tr +='<td>'+obj.types+'</td>';
        // tr +='<td>'+obj.datetime+'</td>';
        var time = isEmpty(obj.timespent) ? "" : obj.timespent;
        var timeinmin = isEmpty(obj.timeinmin) ? "" : obj.timeinmin;
        tr+='<td><i class="fa fa-edit action-css" id="'+obj.particulars+'_'+obj.types+'" onclick=editTimesheet(this.id,'+timespent+','+timeinmin+',"'+obj.edate+'",'+obj.id1+')></i>&nbsp<i class="fa fa-trash" onclick=deleteTimesheet('+obj.id1+')></i></td></tr>';
      });
      tr+= '</tbody></table>';
      // }
      $("#timesheetlist").html(tr);
      $("#timesheettable").DataTable();
      $('#addtimesheetform').slideUp(400);

      $("#particulars").val('');
      $("#datepickertimesheet").val('');
      $("#timespent").val('');
      $("#timeinmin").val('');
      $("input[type='radio'][name='type']:checked").val('');
    });


     // Called on failure of file upload
     // ajaxReq.fail(function(jqXHR) {
     //         toastr.error('Error in uploading document',{timeout:5000});
     //  });
   };
// if(response_val.status == 'Success'){

// debugger;

//    $("#particulars").val('');
//    $("#datepickertimesheet").val('');
//    $("#timespent").val('');
//    $("#timeinmin").val('');
//    $("#type").val('');
// }

function editTimesheet(particulars,timespent,timeinmin,date,id1){
  debugger;
  $("#error_message").hide();
  var parti_type = particulars.split("_");
  $('#addtimesheetform').slideDown(400);
  $("#particulars").val(parti_type[0]);
  $("#timeinmin").val(timeinmin);
  $("#timespent").val(timespent);

  var objSelect = document.getElementById("timeinmin");
  setSelectedValue(objSelect,timeinmin); 

  var objSelect2 = document.getElementById("timespent");
  setSelectedValue(objSelect2,timespent); 


  $("#id1").val(id1);
  var timesheetcase = $("#case").val();
  var userid = $("#userid").val();
  $("#datepickertimesheet").val(date);
  $("input[name='type']").val(parti_type[1]).prop("checked",true);
    // var id = $("#id1").val();
    
    // var particular = $("#particulars").val();
    // var datepickertimesheet = $("#datepickertimesheet").val();
    // var timespenttime = $("#timespent").val();
    // var timeinmin = $("#timeinmin").val();
    // var typetime= $("input[type='radio'][name='type']:checked").val();
    $('#timesheetsubmit').hide();
    $('#updatetimesheet').show();

// var particular = $("#particulars").val();
// var particular = $("#particulars").val();
// var particular = $("#particulars").val();


}

function updateTimesheet(){
  debugger;
  $("#error_message").hide();
  var id1 = $("#id1").val();
  var timesheetcase = $("#case").val();
  var userid = $("#userid").val();
  var particular = $("#particulars").val();
  var datepickertimesheet = $("#datepickertimesheet").val();
  var timespent = $("#timespent").val();
  var timeinmin = $("#timeinmin").val();
  var type= $("input[type='radio'][name='type']:checked").val();
  var formData = new FormData();
  formData.append('id1', id1);
  formData.append('case', timesheetcase);
  formData.append('user_id', userid);
  formData.append('particulars', particular);
  formData.append('date1', datepickertimesheet);
  formData.append('timespent', timespent);
  formData.append('timeinmin', timeinmin);
  formData.append('type', type);
  formData.append('submit2', 'submit');
  var ajaxReq = $.ajax({
    url : "ajaxdata/updatetimesheet.php",
    type : 'POST',
    data : formData,
        // data: JSON.stringify(timeVo),
        cache : false,
        contentType : false,
        processData : false,
      });

  ajaxReq.done(function(response) {
    console.log(response);

    var response_val = JSON.parse(response);
    var tr ='';
      // if(response.status == 'FAILED')

      tr += '<table class="table table-bordered export_table" id="timesheettable">';
      tr +='<thead><tr><th>#</th><th>Date</th><th>Particulars</th><th>Timespent(in Hours)</th><th>Type</th><th>Action</th></tr></thead>';
      $.each(response_val,function(i,obj){
        // $("#view_case_document").text(response.doc_count);
        // var filesize = bytesToSize(obj.size);
        tr +='<tbody><tr>';
        tr +='<td style="text-align:center;"><input type="checkbox" value="" name="" class="checked_checkbox"></td>';
        tr +='<td>'+obj.edate+'</td>';
        tr +='<td>'+obj.particulars+'</td>';
        tr +='<td>'+obj.timespent+':'+obj.timeinmin+'</td>';
        tr +='<td>'+obj.types+'</td>';
        // tr +='<td>'+obj.datetime+'</td>';
        tr+='<td><i class="fa fa-edit action-css" id="'+obj.particulars+'_'+obj.types+'" onclick=editTimesheet(this.id,'+obj.timespent+','+obj.timeinmin+',"'+obj.edate+'","'+obj.id1+'")></i>&nbsp<i class="fa fa-trash" onclick=deleteTimesheet('+obj.id1+')></i></td></tr>';
      });
      tr+= '</tbody></table>';
      // }
      $("#timesheetlist").html(tr);
      $("#timesheettable").DataTable();



    });
  toastr.success("Timesheet Updated successfully",{timeOut: 5000});
  $('#addtimesheetform').slideUp(400);
  $("#particulars").val('');
  $("#datepickertimesheet").val('');
  $("#timespent").val('');
  $("#timeinmin").val('');
  $("input[type='radio'][name='type']:checked").val('');
  
}
function deleteTimesheet(id){
  debugger;
  // $("#error_message").show();
    // $("#timesheetlist").html('');
    var id1 = $("#id1").val();
    var timesheetcase = $("#case").val();
    var userid = $("#userid").val();
    //   if(obj == "No data found" )
    // {
    //   $("#error_message").html('No data Available in Timesheet');
    // }
    var x = confirm("Are you sure you want to delete?");
    if (x)
    {
      $("#timesheetlist").html('');
      var ajaxReq = $.ajax({

        url : "ajaxdata/deletetimesheet.php",
        type : 'POST',
        data: {
          id1: id,
          case: timesheetcase,
          user_id: userid
        },
        cache: false,
        dataType: 'json',
      });

      ajaxReq.done(function(response) {
        console.log(response);
        $.each(response,function(i,obj){
         console.log(response.status);

         if(obj.status == 'Success'){
           var tr ='';
           tr += '<table class="table table-bordered export_table" id="timesheettable">';
           tr +='<thead><tr><th>#</th><th>Date</th><th>Particulars</th><th>Timespent(in Hours)</th><th>Type</th><th>Action</th></tr></thead>';
           $.each(response,function(i,obj){
            tr +='<tbody><tr>';
            tr +='<td style="text-align:center;"><input type="checkbox" value="" name="" class="checked_checkbox"></td>';
            tr +='<td>'+obj.edate+'</td>';
            tr +='<td>'+obj.particulars+'</td>';
            tr +='<td>'+obj.timespent+':'+obj.timeinmin+'</td>';
            tr +='<td>'+obj.types+'</td>';
            tr+='<td><i class="fa fa-edit action-css" id="'+obj.particulars+'_'+obj.types+'" onclick=editTimesheet(this.id,'+obj.timespent+','+obj.timeinmin+',"'+obj.edate+'","'+obj.id1+'")></i>&nbsp<i class="fa fa-trash" onclick=deleteTimesheet('+obj.id1+')></i></td></tr>';
          });
           tr+= '</tbody></table>';
      // }
      $("#timesheetlist").html(tr);
      $("#timesheettable").DataTable();
    }
  });

        toastr.success("Successfully deleted timesheet ",{timeout:5000});
      });
    }
    else{
      return false;
    }


  }


// function getActivitySession(session,id){
//      var objSelect = document.getElementById("caseactivityinfo-sessions_"+id);
//      setSelectedValue(objSelect,data);  

//    }
   // function setSelectedValue(selectObj, valueToSet) {
   //   for (var i = 0; i < selectObj.options.length; i++) {
   //     if (selectObj.options[i].value== valueToSet) {
   //       selectObj.options[i].selected = true;
   //       return;
   //     }
   //   }
   // }


   function CancelTimesheet(){
     $("#particulars").val('');
     $("#datepickertimesheet").val('');
     $("#timespent").val('');
     $("#timeinmin").val('');
     $("input[type='radio'][name='type']:checked").val('');
     $('#addtimesheetform').slideUp(400);
   }