  $("#loader-div").hide();
  $("#petitioner_div").hide();
  $(document).ready(function(){
    $(".add-case-li").addClass("active pcoded-trigger");
    $("#court_hall_div").hide();
    $("#floor_div").hide();
    
  });

  var getcase='';
  var pet_count=0,petadv_count = 0,res_count=0,resadv_count=0,petitioner_arr=[],tribunal_appleant='',tribunal_respondent='';
// Initialize select2
$("#diary_year").select2({
  placeholder: 'Select...'
});
$("#case_no_year").select2();

$("#cases-court_id").select2();
$("#cases-case_type").select2({
  placeholder: 'Select...'

});
$("#district_court_city").select2({
  placeholder: 'Select...'

});
$("#district_court_establishment").select2({
  placeholder: 'Select...'

});
$("#cases-priority").select2({
  placeholder: 'Select...'

});
$("#cases-high_court_ids").select2({
  placeholder: 'Select...'

});
$("#state").select2({
  placeholder: 'Select...'

});
$("#high-court-list").select2({
  placeholder: 'Select...'

});
$("#districtcourt_state").select2({
  placeholder: 'Select...'

});
$("#high_court_bench_list").select2({
  placeholder: 'Select...'

});
$("#high_court_side_list").select2({
  placeholder: 'Select...'
});
$("#cases-high_court_id").select2({
  placeholder: 'Select...'
});
$("#high_court_casetype").select2({
  placeholder: 'Select...'
});
$("#district_court_casetype").select2({
  placeholder: 'Select...'
});
$("#department").select2({
  placeholder: 'Select...'
});
$("#commission").select2({
  placeholder: 'Select...'
});
$("#appearing_modal").select2({
  placeholder: 'Select...'
});
$("#supreme_court").select2({
  placeholder: 'Select...'
});
$("#tribunal_select").select2({
  placeholder: 'Select...'
});
$("#tribunal_state").select2({
  placeholder: 'Select...'
});
$("#tribunal_case").select2({
  placeholder: 'Select...'
});
$("#revenue_states").select2({
  placeholder: 'Select...'
});
$("#commissionerate_select").select2({
  placeholder: 'Select...'
});
$(document).on('click', 'input[type="checkbox"][name="commissioner_level"]', function() {      
  $('input[type="checkbox"][name="commissioner_level"]').not(this).prop('checked', false);      
});
$("#cases_case_type").select2();
$("#sc_casetype_other").hide();
$("#hc_casetype_other").hide();
$("#district_court_casetype_other").hide();
$("#dc_establishment_other").hide();
$("#district_court_casetype_other").hide();

$("#sc_info").hide();
getDepartment();
$("#anycondition").on('hidden.bs.modal', function(){
  $(this).find("input,textarea").val('').end();
});

$( "#datepicker" ).datepicker({
  dateFormat: 'yy/mm/dd',
  changeYear: true,
  changeMonth: true
});

$( "#dateoffilling" ).datepicker({
  dateFormat: 'yy/mm/dd',
  changeYear: true,
  changeMonth: true,
  yearRange: "-100:+0"
});
$( "#affidavit_filling_date" ).datepicker({
  dateFormat: 'yy/mm/dd',
  changeYear: true,
  changeMonth: true,
  yearRange: "-100:+0"
});
$( "#vakalath_filling_date" ).datepicker({
  dateFormat: 'yy/mm/dd',
  changeYear: true,
  changeMonth: true,
  yearRange: "-100:+0"
});
$("#cases_case_type").on('change',function(){
  var val = $(this).val();
  if(!isEmpty(val) && val == "0")
    $("#sc_casetype_other").show();
  else
    $("#sc_casetype_other").hide();
});
/*$("#sel_pet").on('change',function(){
  var val = $(this).val();
  if(val == 'Petitioner'){

  }
})*/

changeLanguage('English');
function changeLanguage(language){
  tinymce.init({
    selector : "#query",
    plugins : [
    "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });

  if(language == 'Marathi'){
    google.setOnLoadCallback(OnLoad);
    OnLoad();
  }
}
google.load("elements", "1", {packages: "transliteration"});

function OnLoad() { 
  var options = {
    sourceLanguage:
    google.elements.transliteration.LanguageCode.ENGLISH,
    destinationLanguage:
    [google.elements.transliteration.LanguageCode.MARATHI],
    shortcutKey: 'ctrl+g',
    transliterationEnabled: true
  };  
  var control = new google.elements.transliteration.TransliterationControl(options);
  control.makeTransliteratable(["query_ifr"]);
  
} //end onLoad function


var appearing_top_array = [];
function getAppearingInput(){
  appearing_top_array = [];
  $.each($("input[name='appearing_radio']"),function(i,obj){
    appearing_top_array.push($(this).val());
  });

  if ($("input[name='appearing_radio']").is(":checked")) {
    var val = $("input[name='appearing_radio']:checked").val();
    var val_not_checked = $("input[name='appearing_radio']:not(:checked)").val();
    var appear_radio = '<label for="exampleInputUsername1" class="col-sm-4 col-form-label">';
    appear_radio +=val+'</label>';
    appear_radio +='<div class="col-sm-8 form-group"><input type="text" id="petitioner" onkeypress="return onlyCharAllowed(event)" class="form-control" value="" /></div>';
    appear_radio +='<label for="exampleInputUsername1" class="col-sm-4 col-form-label">';
    appear_radio +='Attach Document </label>';
    appear_radio +='<div class="col-sm-6">';
    appear_radio +='<input type="file" id="pet_file" aria-label="File browser example" style="margin-top: 4px;">';
    appear_radio +='<p id="sc_info">Kindly wait as it will take upto 1 minute to fetch the data</p></div>';
    appear_radio +='<div class="col-sm-2" id="otin">';
    appear_radio +='<div class="fetchLoaderDiv"></div><button type="button" class=" btn btn-info btn-sm fetchData_outer" style="float: right;" id="fetchData_outer" onclick=datafetch()>Fetch Data </button>';
    appear_radio +=' </div>';    
    $("#appearingas_input").html(appear_radio);
    $("#appearingas_input").show();
    $(".appearing_as_first").text(val);

    $(".appearing_as_second").text(val_not_checked);
    submit_caseapi();
/*if(val != appearing_top_array[0]){
}*/

if(($("#cnr_div").css('display') == 'block' && $("input[name='membershipRadios']:checked").val() == 'Yes'))
  $("#otin").hide();
else if(($("#cases-court_id").val() == 2 || $("#cases-court_id").val() == 3) && $("input[name='membershipRadios']:checked").val() == 'Yes')
  $("#otin").hide();
else
  $("#otin").show();
}
}

$("#case_no").keypress(function(event){
  if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
           event.preventDefault(); //stop character from entering input
           toastr.error("","Only Numbers are allowed",{timeout:5000});
         }
       });
getTeamMemberListDropdown();
function getTeamMemberListDropdown(){
// var hidValue = $("#hidSelectedOptions").val();
var selectedOptions = [];

$.ajax({
  type:'POST',
  url:host+'/your_team.php',
  dataType:"json",
  success:function(response){
    console.log(response);
// var response = JSON.parse(response);
if(response.team.length > 0){
  var option = '<option value="">Select</option>';
  $.each(response.team,function(i,obj){
    option += '<option value='+obj.reg_id+'>'+obj.name+' '+obj.last_name+'</option>';
  });
  $('#framework').html(option);
  $('#framework').select2({
    placeholder: 'Select...',
    closeOnSelect: false,
    multiple: true,
    allowClear: false
  });
}
$.each(response.show_team,function(i,obj){
  selectedOptions.push(obj.reg_id);
});
$('#framework').val(selectedOptions).trigger('change');
},
error:function(){
  toastr.error("","Error fetching team list",{timeout:5000});
}
});
}

$(document).ready(function(){
  $("#tribunal_bench_div").hide();

// $('#framework option:selected').prop('disabled', true);

$.each($("#framework option"), function (i, item) {
  if (item.selected) {
    /*$(item).prop("disabled", true); */
    $(".select2-selection__choice__remove").hide();
  }else {
    /*$(item).prop("disabled", false);*/
    $(".select2-selection__choice__remove").show();
  }
});

$("#nominal-div").hide();
$("#any-condition-search-div").hide();
$("#any-condition-search-div-district").hide();
$("#any-condition-search-div-high").hide();
$("#commision_casetype_other").hide();
$("#cases-court_id").on('change',function(){
  $("#sc_info").hide();
  var val = $(this).val();
  cnr_allow('No');
  $("input[type='text']").val('');
  $(".common-select-div").hide();


  $("#diary_year").val(" ");
    // $("#case_no_year").val(" ");
    // $("#cases-court_id").select2("val", "");
    // $("#cases-case_type").val("").trigger('change');
    // $("#district_court_city").val("").trigger('change');
    // $("#district_court_establishment").val("").trigger('change');
    // $("#cases-priority").val("").trigger('change');
    // $("#cases-high_court_ids").val("").trigger('change');
    // $("#case_no_year").val(" ").trigger('change');
    // $("#state").val("").trigger('change');
    // $("#high-court-list").val("").trigger('change');
    // $("#districtcourt_state").val("").trigger('change');
    // $("#high_court_bench_list").val("").trigger('change');
    // $("#high_court_side_list").val("").trigger('change');
    // $("#cases-high_court_id").val("").trigger('change');
    // $("#high_court_casetype").val("").trigger('change');
    // $("#district_court_casetype").val("").trigger('change');
    // $("#department").val("").trigger('change');

    $("#appearing_radio_one_label").html('<input type="radio" class="chkPassport" name="appearing_radio" onclick="getAppearingInput()" checked id="appearing_radio_one" value="Petitioner"> <i class="input-helper"></i> Petitioner');
    $("#appearing_radio_two_label").html('<input type="radio" class="chkPassport" name="appearing_radio" onclick="getAppearingInput()" id="appearing_radio_one" value="Respondent"> <i class="input-helper"></i> Respondent');
    // $("#appearing_radio_two_label").text('Respondent');
    $("#case_no").val('');
    if(val == 3)
      $("#nominal-div").show();
    if(val == 1){
      $("#court_hall_div").show();
      $("#floor_div").show();
    }
    else{
      $("#court_hall_div").hide();
      $("#floor_div").hide();
    }

    /*$("#case_no_year option:selected").text('Select Year');
    $("#case_no_year option:selected").val('');*/
    getAppearingInput();
    if(val == 1)// Supreme Court
    {
      // $('.select').prop('selectedIndex',0);
      $("#supreme_court").val("").trigger('change');
      $("#cnr_div").hide();
      $("#tribunal_authority").hide();
      $("#supremecourt").show();
      $("#appearing_model").show();
      $("#highcourt").hide();
      $("#supreme_court_casetype").hide();
      // $("#cnr_div").hide();
      $("#supreme_court_diary").hide();
      $("#district_court").hide();
      $("#commission_forum").hide();
      $("#state_commission_div").hide();
      $("#NCDRC").hide();
      $("#circuit").hide();
      $("#commission_state_div").hide();
      getAppearingModel();
      $("#revenue_court").hide();
      $("#court_other").hide();
      $("#any-condition-search-div").show();
      $("#commissionerate_div").hide();
      $("#any-condition-search-div-district").hide();
      $("#any-condition-search-div-high").hide();
      $("#revenue_casetype_div").hide();
      $("#commissionerate_authority").hide();
      $("#courtsidecasetype").hide();
    }
    if(val == 2){
      // $("#court_code").val(val);
      /*$("#state_code").val($("#high-court-list").val());
      $("#dist_code").val($("#high_court_bench_list").val());*/
      $("#high-court-list").val("").trigger('change');
      $("#high_court_bench_list").val("").trigger('change');
      $("#high_court_side_list").val("").trigger('change');
      $("#cases-high_court_id").val("").trigger('change');
      $("#high_court_casetype").val("").trigger('change');
      $("#any-condition-search-div").hide();
      $("#court_type").val('HC');
      $("#sc_info").hide();
      $("#tribunal_authority").hide();
      $("#cnr_div").show();
      $("#supreme_court_casetype").hide();
      $("#supreme_court_diary").hide();
      $("#highcourt").show();
      $("#appearing_model").hide();
      $("#commission_forum").hide();
      $("#state_commission_div").hide();
      $("#NCDRC").hide();
      $("#bench").hide();
      $("#highcourt_side").hide();
      $("#stamp_register").hide();
      $("#highcourt_casetype").hide();
      $("#circuit").hide();
      $("#commission_state_div").hide();      
      $("#supremecourt").hide();
      $("#revenue_court").hide();
      cnr_allow('No');
      $("#district_court").hide();
      $("#court_other").hide();
      $("#commissionerate_div").hide();
      $("#otin").show();
      $("#cnr_div").hide();
      $("#revenue_casetype_div").hide();
      $("#commissionerate_authority").hide();
      $("#courtsidecasetype").hide();

    }
    if(val == 3){
      // $("#court_code").val(val);
      // $("#districtcourt_state").select2("val", "");
      $("#district_court_city").val("").trigger('change');
      $("#district_court_establishment").val("").trigger('change');
      $("#districtcourt_state").val("").trigger('change');
      $("#district_court_casetype").val("").trigger('change');
      $("#court_type").val('DC');
      $("#sc_info").hide();
      $("#tribunal_authority").hide();
      $("#cnr_div").show();
      $("#highcourt").hide();
      $("#district_court").show();
      $("#supreme_court_casetype").hide();
      $("#supreme_court_diary").hide();
      $("#appearing_model").hide();
      $("#commission_forum").hide();
      $("#state_commission_div").hide();
      $("#NCDRC").hide();
      $("#circuit").hide();
      $("#commission_state_div").hide();
      cnr_allow('No');
      $("#revenue_court").hide();
      $("#supremecourt").hide();
      $("#appearing_model").show();
      getAppearingModel();
      $("#court_other").hide();
      $("#commissionerate_div").hide();
      $("#revenue_casetype_div").hide();
      $("#commissionerate_authority").hide();
      $("#courtsidecasetype").hide();

      // $("#otin").hide();
    }
    if(val == 4 || val == 5 || val == 6){
     if(val == 4){
      $("#commission").val(" ").trigger('change');
      $("#commission_forum").show();
      $("#commission_state_div").show();
      $("#revenue_court").hide();
      $("#tribunal_authority").hide();
      $("#any-condition-search-div").hide();
      $("#any-condition-search-div-high").hide();
      $("#any-condition-search-div-district").hide();
      $("#otin").show();
      $("#revenue_casetype_div").hide();
    }
    if(val == 5){
      $("#tribunal_select").val("").trigger('change');
      $("#tribunal_case").val("").trigger('change');
      $("#tribunal_authority").show();
      $("#other-case-type").hide();
      $("#commission_forum").hide();
      $("#state_commission_div").hide();
      $("#NCDRC").hide();
      $("#circuit").hide();
      $("#commission_state_div").hide();
      $("#revenue_court").hide();
      $("#any-condition-search-div-district").show();
      $("#any-condition-search-div").hide();
      $("#any-condition-search-div-high").hide();
      $("#otin").hide();
      $("#revenue_casetype_div").hide();
    }
    if(val == 6){
      $("#revenue_court").show();
      $("#tribunal_authority").hide();
      $("#commission_forum").hide();
      $("#state_commission_div").hide();
      $("#NCDRC").hide();
      $("#circuit").hide();
      $("#commission_state_div").hide();
      $("#commissionerate_div").hide();
      $("#any-condition-search-div-high").hide();
      $("#any-condition-search-div-district").hide();
      $("#any-condition-search-div").hide();
      $("#otin").hide();
    }
    $("#sc_info").hide();
    $("#cnr_div").hide();
    $("#highcourt").hide();
    $("#district_court").hide();
    $("#supreme_court_casetype").hide();
    $("#supreme_court_diary").hide();
    $("#appearing_model").hide();
    $("#supremecourt").hide();
    $("#appearing_model").show();
    $("#commissionerate_authority").hide();
    // $("#caseandyear").show();
    $("#case-div").show();
    $("#year-div").show();
    getAppearingModel();
      // $("#otin").hide();
      $("#court_other").hide();
      $("#commissionerate_div").hide();
      $("#courtsidecasetype").hide();
    }
    if(val == 7){
     $("#sc_info").hide();
     $("#cnr_div").hide();
     $("#highcourt").hide();
     $("#district_court").hide();
     $("#supreme_court_casetype").hide();
     $("#supreme_court_diary").hide();
     $("#appearing_model").hide();
     $("#supremecourt").hide();
     $("#appearing_model").hide();
     // $("#caseandyear").show();
     $("#case-div").show();
     $("#year-div").show();
     $("#tribunal_authority").hide();
     $("#commission_forum").hide();
     $("#state_commission_div").hide();
     $("#NCDRC").hide();
     $("#circuit").hide();
     $("#commission_state_div").hide();
     $("#otin").hide();
     $("#commissionerate_div").show();
     $("#court_other").hide();
     $("#any-condition-search-div").hide();
     $("#any-condition-search-div-district").hide();
     $("#any-condition-search-div-high").hide();
     $("#revenue_court").hide();
     $("#revenue_casetype_div").hide();
   }
   if(val == 9){
    $("#sc_info").hide();
    $("#court_other").show();
    $("#cnr_div").hide();
    $("#tribunal_authority").hide();
    $("#highcourt").hide();
    $("#district_court").hide();
    $("#supreme_court_casetype").hide();
    $("#supreme_court_diary").hide();
    $("#appearing_model").show();
    $("#supremecourt").hide();
    $("#any-condition-search-div").hide();
    $("#any-condition-search-div-district").hide();
    $("#any-condition-search-div-high").hide();
    $("#appearing_model").show();
     // $("#caseandyear").show();
     $("#case-div").show();
     $("#year-div").show();
     $("#commission_forum").hide();
     $("#state_commission_div").hide();
     $("#NCDRC").hide();
     $("#revenue_court").hide();
     $("#circuit").hide();
     $("#commission_state_div").hide();      
     $("#commissionerate_div").hide();
     $("#revenue_casetype_div").hide();
     getAppearingModel();
     $("#commissionerate_authority").hide();
     $("#otin").hide();
     $("#courtsidecasetype").hide();
   }
   $("#areyouappearingas").show();

 });
  $("#revenue_states").on('change',function(){
    $("#revenue_other").show();
    $("#revenue_casetype_div").show();
  });
  $("#commissionerate_state").on('change',function(){
    $("#courtside").show();
  });
  $("#courtside").on('change',function(){
    $("#courtsidecasetype").show();
  });
  $("#cases-high_court_id").on('change',function(){
    $("#any-condition-search-div-high").show();
  });

  $("#supreme_court").on('change',function(){
  // $("#any-condition-search-div").show();
  var val = $(this).val();
    if(val == "Diary Number")// Supreme Court diary
    {
      $("#supreme_court_diary").show();
      $("#supreme_court_casetype").hide();
      // $("#caseandyear").hide();
      $("#case-div").hide();
      $("#year-div").hide();
    }
    else if(val == 'Case Number'){
      $("#supreme_court_casetype").show();
      $("#supreme_court_diary").hide();
      // $("#caseandyear").show();
      $("#case-div").show();
      $("#year-div").show();
    }
  });
  $("#commissionerate_select").on('change',function(){
    var val = $(this).val();
    if(val == 'Charity Commissionerate'){
      $("#commissionerate_state_div").show();
      $("#commissionerate_authority").hide();
      $("#courtside").show();
    }
    else{
      $("#commissionerate_authority").show();
      $("#commissionerate_state_div").hide();
      $("#courtsidecasetype").hide();
      $("#courtside").hide();
    }
  });
  $("#authority_select").on('change',function(){
    $("#courtsidecasetype").show();
    var casetype = '<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;">*</span>&nbsp;&nbsp;Case Type</label>';
    casetype+='<div class="col-sm-8">';
    casetype+='<input type="text" class="form-control" id="commissionerate_casetype"></div>';
    $("#courtsidecasetype").html(casetype);
  });

  $("#high-court-list").on('change',function(){
    var val = $(this).val().split(",");
    var val_to_php = val[0];
    var val_to_api = val[1];
    $("#state_code").val(val_to_api);
    if(val_to_php != ''){
      $.ajax({
        type:'POST',
        url:'highcourtlist.php',
        data:'high_court='+val_to_api,
        success:function(response){
          console.log(response);
          $('#high_court_bench_list').html('');
          var response = JSON.parse(response);
          if(response.length > 0){
            var option = '<option value="">Select</option>';
            $.each(response,function(i,obj){
              option += '<option value='+obj.bench_id+','+obj.master_id+'>'+obj.bench_name+'</option>';
            });
            $('#high_court_bench_list').html(option);
            cnr_allow('No');
            $("#bench").show();
            $("#highcourt_side").hide();
          }
        },
        error:function(){
          toastr.error("","Error fetching bench list",{timeout:5000});
        }
      });

    }
  });

  $("#commission_state_casetype").on('change',function(){
    var val = $(this).val();
    if(val == '0')
      $("#commision_casetype_other").show();
    else
      $("#commision_casetype_other").hide();
  });
  $("#high_court_bench_list").on('change',function(){
    var val = $(this).val().split(",");
    var val_to_php = val[0];
    var val_to_api = val[1];

    $("#court_code").val(val_to_api);
    $("#dist_code").val(val_to_api);

    if(val_to_php !=''){
      $.ajax({
        type:'POST',
        url:'highcourtlist.php',
        data:'highcourt_bench='+val_to_php,
        success:function(response){
          debugger;
          var response = JSON.parse(response);
          if(response !=''){
           var option = '<option value="">Select</option>';
           if(response.flag == 'case_type'){
            $.each(response.data,function(i,obj){
              option += '<option value='+obj.case_typeid+'>'+obj.case_type+'</option>';
            });
            $("#high_court_casetype").html(option);
            $("#highcourt_casetype").show();
          }else{
           $.each(response.data,function(i,obj){
            option += '<option value='+obj.side_id+'>'+obj.side_name+'</option>';
          });
           $("#highcourt_side").show();
           $("#high_court_side_list").html(option);
         }
       }
     },
     error:function(){
      toastr.error("","Error fetching side",{timeout:5000});
    }
  });

    }
  });

  $("#high_court_side_list").on('change',function(){
    // function high_court_side_list(){
     var val = $(this).val();
     if(val !=''){
      $.ajax({
       type:'POST',
       url:'highcourtlist.php',
       data:'highcourt_side='+val,
       success:function(response){
        console.log(response); 
        if(response !='')         
          var response = JSON.parse(response);

        if(response.length > 0){
         var option = '<option value="">Select</option>';
         $.each(response,function(i,obj){
          option += '<option value='+obj.case_typeid+'>'+obj.case_type+'</option>';
        });  
         option +='<option value="0">Other</option>';  
         var highcourt = $("#high-court-list").val().split(",");
         var val_bench = val[0];
         if(val_bench == '1')
           $("#stamp_register").show();
         else
          $("#stamp_register").hide();                  
        $("#high_court_casetype").html(option);
        $("#highcourt_casetype").show();
      }
    },
    error:function(){
      toastr.error("","Error fetching casetype",{timeout:5000});
    }
  });
    }
  });

  $("#high_court_casetype").on('change',function(){
    var val = $(this).val();
    if(val == "0")
      $("#hc_casetype_other").show();
    else
      $("#hc_casetype_other").hide();
  })

/*$("#tribunal_select").on('change',function(){
  var val = $(this).val();
  $.ajax({
    type:'POST',
    url:'ajaxData.php',
    data:'countryids='+val,
    success:function(html){
      $('#tribunal_state').html(html);
          // $('#city').html('<option value="">Select District first</option>');
        },
        error:function(){

        }
      });
    })*/
    $('#tribunal_select').on('change', function(){
     var tribunal = $(this).val();
     $.ajax({
      type:'POST',
      url:'ajax_tribunals.php',
      data:'tribunals='+tribunal,
      success:function(response){
       console.log(response);
       var response = JSON.parse(response);
       if(response.length > 0){
        $("#tribunal_state_div").show();
        var option ='<option value="">Please Select </option>';
        $.each(response,function(i,obj){
         option +='<option value="'+obj.t_state_id+'">'+obj.t_state_name+'</option>';
       });
        $('#tribunal_state').html(option);
      }else{
        $("#tribunal_state_div").hide();
        $("#tribunal_bench_div").hide();
        $("#tribunal_location_div").hide();
      }
      tribunalState();    
      getTribunalLocation();      
    },
    error:function(){
     toastr.error("","Error fetching tribunal state",{timeout:5000});
   }
 });      
   });
    $("#tribunal_state").on('change',function(){
     // tribunalState();  
     getBenchTribunal();
     getTribunalSection();
     
   });

    function getTribunalLocation(){
     var tribunal = $("#tribunal_select").val();
     $.ajax({
      type:'POST',
      url:'ajaxdata/tribunal_location.php',
      data:'tribunals_id='+tribunal,
      success:function(response){
       console.log(response);
       var response = JSON.parse(response);
       if(response.length > 0){
        var option ='<option value="">Please Select </option>';
        $.each(response,function(i,obj){
         option +='<option value="'+obj.location_id+'">'+obj.location_name+'</option>';
       });
        $("#tribunal_location_div").show();
        $('#tribunal_location').html(option);
        $("#tribunal_location").select2();
      }
      else
        $("#tribunal_location_div").hide();
    }
  });
   }

   function tribunalState(){
     $('#tribunal_case').html('');
     $("#other-case-type").hide();
     var tribunal = $("#tribunal_select").val();
     $.ajax({
      type:'POST',
      url:'ajaxdata/tribunal_case_type.php',
      data:'tribunals_id='+tribunal,
      success:function(response){
       console.log(response);
       var response = JSON.parse(response);
       if(response.length > 0){
        var option ='<option value="">Please Select </option>';
        $.each(response,function(i,obj){
         option +='<option value="'+obj.case_type_id+'">'+obj.tribunals_name+'</option>';
       });
        $('#tribunal_case').html(option);
        $("#case-type-tribunal").show();

      }
      else
        $("#case-type-tribunal").hide();
    }
  });    
   }
   function getTribunalSection(){
     var tribunal = $("#tribunal_state").val();
     $.ajax({
      type:'POST',
      url:'ajaxdata/tribunal_section.php',
      data:'state_id='+tribunal,
      success:function(response){
       console.log(response);
       var response = JSON.parse(response);
       if(response.length > 0){
        var option ='<option value="">Please Select </option>';
        $.each(response,function(i,obj){
         option +='<option value="'+obj.section_id+'">'+obj.tribunal_section+'</option>';
       });
        $("#tribunal_section_div").show();
        $('#tribunal_section').html(option);
        $('#tribunal_section').select2();
      }
      else{
        $("#tribunal_section_div").hide();
      }      
    }
  });
   }
   function getBenchTribunal(){
     var tribunal = $("#tribunal_state").val();
     $.ajax({
      type:'POST',
      url:'ajaxdata/tribunal_bench.php',
      data:'state_id='+tribunal,
      success:function(response){
       console.log(response);
       var response = JSON.parse(response);
       if(response.length > 0){
        var option ='<option value="">Please Select </option>';
        $.each(response,function(i,obj){
         option +='<option value="'+obj.bench_id+'">'+obj.bench_name+'</option>';
       });
        $("#tribunal_bench_div").show();
        $('#tribunal_bench').html(option);
        $('#tribunal_bench').select2();
      }
      else{
        $("#tribunal_bench_div").hide();
      }      
    }
  });
   }

   $("#tribunal_case").on('change',function(){
     var val = $("#tribunal_case option:selected").text();
     if(val == 'Other')
      $("#tribunal_other").show();

    else
      $("#tribunal_other").hide();
  })

    /*$(".toggle-switch.toggle-switch-danger input:checked + .toggle-slider").on("click",function(){
      $.ajax({
        type:'POST',
        url:'ajax_tribunals.php',
        data:'tribunalss='+tribunal,
        success:function(response){
          console.log(response);
          var response = JSON.parse(response);
          if(response.length > 0){
            var option ='<option value="">Please Select </option>';
            $.each(response,function(i,obj){
              option +='<option value="'+obj.t_state_id+'">'+obj.t_state_name+'</option>';
            });
            $('#tribunal_case').html(option);
          }        
        }
      });
    });
    */
    $('.appearing_modals').on('change',function(){
      var stateID = $(this).val();
      var selected_appearing = $("#appearing_modal option:selected").text().split("-");
      $(".appearing_as_first").text(selected_appearing[0]);
      $(".appearing_as_second").text(selected_appearing[1]);
      if(stateID){
        $.ajax({
          type:'POST',
          url:'appearing_modal_fetch.php',
          data:'state_id='+stateID,
          success:function(response){
            $('#areyouappearingas').show();
            $('#areyouappearingas').html(response);  
            getAppearingInput();
          },
          error:function(){
            toastr.error("","Error fetching appearing",{timeout:5000});
          }
        });
      }else{
        $('#areyouappearingas').html('<option value="">Select Appearing Modal first</option>');
      }
    });
    $("#commission_state").on('change',function(){
      $("#commision_casetype_other").hide();
      var val= $(this).val();
      if(val == "4")
        $("#ms").show();
      
      else
        $("#ms").hide();;

      $('#circuit').show();

      /*if(val<'3')
      else document.getElementById('circuit').style.display="none";
      if(val=='3')document.getElementById('gs').style.display="flex";
      else document.getElementById('gs').style.display="none";
      if(val=='4')document.getElementById('ms').style.display="flex";
      else document.getElementById('ms').style.display="none";*/
    });
    $("#commission_bench").on('change',function(){
      $("#commision_casetype_other").hide();
      var val= $(this).val();
      $("#circuit").show();
    });

    $("#commission").on('change',function(){
      var commision_name = $(this).val();
      if(commision_name == 'National Commission - NCDRC'){
        $("#NCDRC").show();
        $("#state_commission_div").hide();
        $("#commission_state_div").hide();
        $("#circuit").hide();
      }
      else{
        $("#NCDRC").hide();
        // $("#state_commission_div").show();
      }
    });

  });



  function getDepartment(){
    $.ajax({
      type:'POST',
      url:'department_api.php',
      success:function(response){
        // console.log(response);
        var response = JSON.parse(response);
        var option = '<option value="">Select Department</option>';
        $.each(response,function(i,obj){
          option +='<option value='+obj.dep_id+'>'+obj.department_name+'</option>';
        });
        $("#department").html(option);
      },
      error:function(){
       toastr.error("","Error fetching department",{timeout:5000});
     }
   });
  }
  function mahastate(name){
    document.getElementById('circuit').style.display="flex";
  }

  function getAppearingModel(){
    $.ajax({
      type:'POST',
      url:'select_appearing_model.php',
      success:function(response){      
        var response = JSON.parse(response);        
        // console.log(response);
        var option = '';
        $.each(response,function(i,obj){
          option +='<option value='+obj.appmodid+'>'+obj.app_model+'</option>';
        });
        $("#appearing_modal").html(option);
      },
      error:function(){
       toastr.error("","Error fetching appearing model",{timeout:5000});
     }
   });
  }

  $("#districtcourt_state").on('change',function(){
    var val = $(this).val().split("_");
    var val_to_php = val[0];
    var val_to_api = val[1];
  // $("#dist_code").val(val);
  $.ajax({
    type:'POST',
    url:'ajaxData.php',
    data:'country_id='+val_to_php,
    success:function(response){
      $("#state_code").val(val_to_api);
      $('#district-court-district').show();
      $('#district-court-establishment').show();
      $('#district_court_city').html(response);
      $('#district_court_establishment').html('<option value="">Select District first</option>');
    },
    error:function(){
      toastr.error("","Error fetching city",{timeout:5000});
    }
  });
});

  $("#district_court_city").on('change',function(){
    var val = $(this).val().split("_");
    var val_to_php = val[0];
    var val_to_api = val[1];
    $("#dist_code").val(val_to_api);
    $.ajax({
      type:'POST',
      url:'establisment.php',
      data:'estais_id='+val_to_php,
      success:function(response){
        $('#district_court_establishment').html(response);

      // $('#district_court_district').html('<option value="">Select District first</option>');
    },
    error:function(){
     toastr.error("","Error fetching establisment",{timeout:5000});
   }
 });
  });

  $("#district_court_establishment").on('change',function(){
    var val = $(this).val().split("_");
    var val_to_php = val[0];
    var val_to_api = val[1];
    $("#court_code").val(val_to_api);
    if(val_to_php == 0)
      $("#district_court_casetype_other").show();
    else
      $("#district_court_casetype_other").hide();
    $.ajax({
      type:'POST',
      url:'district_case_type.php',
      data:'est_id='+val_to_php,
      success:function(response){
        $("#district-court-casetype-div").show();
        $('#district_court_casetype').html('');
        var response = JSON.parse(response);
        var option = '<option>Please select case type</option>';
        $.each(response,function(i,obj){
          option+='<option value="'+obj.master_id+'">'+obj.case_type+'</option>';
        })
        $('#district_court_casetype').html(option);

      // $('#district_court_district').html('<option value="">Select District first</option>');
    },
    error:function(){
     toastr.error("","Error fetching case type",{timeout:5000});
   }
 });

    $("#any-condition-search-div-district").show();
  });

  $("#district_court_casetype").on('change',function(){
    var val = $(this).val();
    if(val == 0)
      $("#district_court_casetype_other").show();
    else
      $("#district_court_casetype_other").hide();
  })



  function cnr_allow(val){
    $("input[name='membershipRadios'][value="+val+"]").prop("checked",true);
    var court_val = $("#cases-court_id").val();

    if(val == 'Yes' && court_val == 2){
      $("#highcourt").hide();
      $("#otin").hide();
      // $("#caseandyear").hide();
      $("#case-div").hide();
      $("#year-div").hide();
      $("#cnr_yes").show();
    } 
    else if(val == 'No' && court_val == 2) {
      $("#highcourt").show();
      $("#otin").show();
      // $("#caseandyear").show();
      $("#case-div").show();
      $("#year-div").show();
      $("#cnr_yes").hide();
      $("#supremecourt").hide();
    }
    else if(val == 'Yes' && court_val == 3){
      $("#district_court").hide();
      $("#district-court-district").hide();
      $("#district-court-establishment").hide();
      $("#district-court-casetype-div").hide();
      $("#supremecourt").hide();
      // $("#caseandyear").hide();
      $("#case-div").hide();
      $("#year-div").hide();
      $("#otin").hide();
      $("#cnr_yes").show();
      $("#supremecourt").hide();
      $("#nominal_div").show();
    }
    else if(val == 'No' && court_val == 3){
      $("#district_court").show();
      $("#supremecourt").show();
      $("#highcourt").hide();
      // $("#caseandyear").show();
      $("#case-div").show();
      $("#year-div").show();
      $("#cnr_yes").hide();
      $("#otin").show();
      $("#supremecourt").hide();
      $("#nominal_div").show();
    }

    else{
      $("#otin").show();
      // $("#caseandyear").show();
      $("#case-div").show();
      $("#year-div").show();
      $("#cnr_yes").hide();


    }
    /*$("#areyouappearingas").show();*/
  }

  function affidavitShow(val){
    if(val == 'Yes'){
      $("#affidavitBrowse").show();
      $("#affidavitDate").show();
    }
    else{
      $("#affidavitBrowse").hide();
      $("#affidavitDate").hide();
    }
  }
  function vakalathShow(val){
    if(val == 'Yes'){
      $("#vakalathDate").show();
      $("#vakalathBrowse").show();
    }
    else{
      $("#vakalathDate").hide();
      $("#vakalathBrowse").hide();
    }
  }

  function submitCase(){
    var petArr= [];
    var resArr=[];
    var resadvArr =[];
    var petAdvArr = [];
    var court_name = $("#cases-court_id").val();
    var supreme_court = $("#supreme_court").val();
    var cases_case_type = $("#cases_case_type option:selected").val();
    var divisional_commisioner_level = $("input[name='commissioner_level']:checked").val();
    // var ministry_desk_no = $("#mantralay_no option:selected").val();
    var ministry_desk_no = $("#mantralay_no").val();
    var diary_number = $("#diary_number").val();
    var diary_year= $("#diary_year").val();
    var case_no = $("#case_no").val();
    var case_no_year = $("#case_no_year").val();
    var appearing_modal = $("#appearing_modal").val();
    var are_you_appearing_as = $("input[name='appearing_radio']:checked").val(); //----//are you appearing as
    var petitioner = $("#petitioner").val();
    var account_type_input = $("#account-type-input").val();
      // var respondent = $("#respondent").val();
      var dateoffilling = $("#dateoffilling").val();
      var court_hall = $("#court_hall").val();
      var floor = $("#floor").val();
      var classification = $("#classification").val();
      var title = $("#title").val();
      var nominal = $("#nominal").val();
      var nastikramank = $("#nastikramank").val();
      /*var query = CKEDITOR.instances.query.getData();*/
      // var query = CKEDITOR.instances.query.document.getBody().getText();
      var query = tinymce.get("query").getContent({ format: 'text' });
      var judge = $("#judge").val();
      var reffered_by = $("#reffered_by").val();
      var section_category = $("#section_category").val();
      var cases_priority = $("#cases-priority").val();
      var commissions = $("#commissions").val();
      var doyou_havecnr = $("input[name='membershipRadios']:checked").val();
      // var passport_radio = $("#passport_radio").val();
      var cnr = $("#name").val();
      var high_court = $("#high-court-list").val();

      var stamp_register = $("#cases-high_court_id option:selected").text();

      // var affidavite_document = document.getElementById("attach_file").files[0].name;      
      var affidavite_filling_date = $("#affidavit_filling_date").val();
      var vakalath_filling_date = $("#vakalath_filling_date").val();
      if($("#appearing_modal").is(":visible")){
        var petetitionerArr=[],respondentArr=[];
        var appearing = $("#appearing_modal option:selected").text().split("-");
        if(appearing[0].trim() == are_you_appearing_as){
          for(var i = 0;i<=petadv_count;i++){
            var pet_adv_name = $("#your_adv_name"+i).val();
            var pet_adv_email  = $("#your_adv_email"+i).val();
            /*var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
            if(!emailReg.test(pet_adv_email))
            {
             alert('Please enter a valid email address.');
             return false;
           }*/

           var pet_adv_mobile = $("#your_adv_mobile"+i).val();
           var petVo = {
            pet_adv_name : pet_adv_name,
            pet_adv_email : pet_adv_email,
            pet_adv_mobile : pet_adv_mobile
          }
          petAdvArr.push(petVo);
        }
        for(var i = 0;i <= pet_count;i++){
         var pet_name = $("#your_name"+i).val();
         var pet_email  = $("#your_email"+i).val();
         /*var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
         if(!emailReg.test(pet_email))
         {
           alert('Please enter a valid respondent email address.');
           return false;
         }*/

         var pet_mobile = $("#your_mobile"+i).val();
         var petVo = {
          pet_name : pet_name,
          pet_email : pet_email,
          pet_mobile : pet_mobile
        }
        petArr.push(petVo);
      }
  // var res_count = $("#res_count").val();
  for(var i = 0;i<=res_count;i++){
    var res_name = $("#opponent_name"+i).val();
    var res_email  = $("#opponent_email"+i).val();
    //var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
   /*if(!emailReg.test(res_email))
   {
     alert('Please enter a valid email address.');
     return false;
   }*/

    var res_mobile = $("#opponent_mobile"+i).val();
    var resVo = {
      res_name : res_name,
      res_email : res_email,
      res_mobile : res_mobile
    }
    resArr.push(resVo);
  }

  // var resadv_count = $("#resadv_count").val();
  for(var i = 0;i<=resadv_count;i++){
    var res_adv_name = $("#opponent_adv_name"+i).val();
    var res_adv_email  = $("#opponent_adv_email"+i).val();
   // var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
   /*if(!emailReg.test(res_adv_email))
   {
     alert('Please enter a valid email address.');
     return false;
   }*/

    var res_adv_mobile = $("#opponent_adv_mobile"+i).val();
    var resadvVo = {
      res_adv_name : res_adv_name,
      res_adv_email : res_adv_email,
      res_adv_mobile : res_adv_mobile
    }
    resadvArr.push(resadvVo);
  }
}
else{
  for(var i = 0;i<=petadv_count;i++){
    var pet_adv_name = $("#opponent_adv_name"+i).val();
    var pet_adv_email  = $("#opponent_adv_email"+i).val();
   //  var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
   //  if(!emailReg.test(pet_adv_email))
   //  {
   //   alert('Please enter a valid email address.');
   //   return false;
   // }

   var pet_adv_mobile = $("#opponent_adv_mobile"+i).val();
   var petVo = {
     pet_adv_name : pet_adv_name,
     pet_adv_email : pet_adv_email,
     pet_adv_mobile : pet_adv_mobile
   }
   petAdvArr.push(petVo);
 }
 for(var i = 0;i <= pet_count;i++){
  var pet_name = $("#opponent_name"+i).val();
  var pet_email  = $("#opponent_email"+i).val();
  /*var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
  if(!emailReg.test(pet_email))
  {
   alert('Please enter a valid email address.');
   return false;
 }*/

 var pet_mobile = $("#opponent_mobile"+i).val();
 var petVo = {
   pet_name : pet_name,
   pet_email : pet_email,
   pet_mobile : pet_mobile
 }
 petArr.push(petVo);
}
  // var res_count = $("#res_count").val();
  for(var i = 0;i<=res_count;i++){
    var res_name = $("#your_name"+i).val();
    var res_email  = $("#your_email"+i).val();
    /*var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
    if(!emailReg.test(res_email))
    {
     alert('Please enter a valid respondent email address.');
     return false;
   }*/

   var res_mobile = $("#your_mobile"+i).val();
   /*var intRegex = /[0-9 -()+]+$/;
   if((res_mobile.length < 10) || (!intRegex.test(res_mobile)))
   {
     alert('Please enter a valid phone number.');
     return false;
   }*/
   var resVo = {
    res_name : res_name,
    res_email : res_email,
    res_mobile : res_mobile
  }
  resArr.push(resVo);
}

  // var resadv_count = $("#resadv_count").val();
  for(var i = 0;i<=resadv_count;i++){
    var res_adv_name = $("#your_adv_name"+i).val();
    var res_adv_email  = $("#your_adv_email"+i).val();
   /*var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
   if(!emailReg.test(res_adv_email))
   {
     alert('Please enter a valid email address.');
     return false;
   }*/

   var res_adv_mobile = $("#your_adv_mobile"+i).val();
   var intRegex = /[0-9 -()+]+$/;
   // if((res_adv_mobile.length < 10) || (!intRegex.test(res_adv_mobile)))
   // {
   //   alert('Please enter a valid respondent phone number.');
   //   return false;
   // }
   var resadvVo = {
    res_adv_name : res_adv_name,
    res_adv_email : res_adv_email,
    res_adv_mobile : res_adv_mobile
  }
  resadvArr.push(resadvVo);
}
}
}
else{
  if(are_you_appearing_as == 'Petitioner'){
    for(var i = 0;i<=petadv_count;i++){
      var pet_adv_name = $("#your_adv_name"+i).val();
      var pet_adv_email  = $("#your_adv_email"+i).val();
      var pet_adv_mobile = $("#your_adv_mobile"+i).val();
      var petVo = {
        pet_adv_name : pet_adv_name,
        pet_adv_email : pet_adv_email,
        pet_adv_mobile : pet_adv_mobile
      }
      petAdvArr.push(petVo);
    }
    for(var i = 0;i <= pet_count;i++){
      var pet_name = $("#your_name"+i).val();
      var pet_email  = $("#your_email"+i).val();
      var pet_mobile = $("#your_mobile"+i).val();
      /*var intRegex = /[0-9 -()+]+$/;
   if((pet_mobile.length < 10) || (!intRegex.test(pet_mobile)))
   {
     alert('Please enter a valid phone number.');
     return false;
   }*/
      var petVo = {
        pet_name : pet_name,
        pet_email : pet_email,
        pet_mobile : pet_mobile
      }
      petArr.push(petVo);
    }
  // var res_count = $("#res_count").val();
  for(var i = 0;i<=res_count;i++){
    var res_name = $("#opponent_name"+i).val();
    var res_email  = $("#opponent_email"+i).val();
   /* if(res_email!=''){
      var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
      if(!emailReg.test(res_email))
      {
       alert('Please enter a valid email address.');
       return false;
     }
   }*/
   var res_mobile = $("#opponent_mobile"+i).val();
   // if(res_mobile!=''){
   //   var intRegex = /[0-9 -()+]+$/;
   //   if((res_mobile.length < 10) || (!intRegex.test(res_mobile)))
   //   {
   //     alert('Please enter a valid phone number.');
   //     return false;
   //   }
   // }
   var resVo = {
    res_name : res_name,
    res_email : res_email,
    res_mobile : res_mobile
  }
  resArr.push(resVo);
}

  // var resadv_count = $("#resadv_count").val();
  for(var i = 0;i<=resadv_count;i++){
    var res_adv_name = $("#opponent_adv_name"+i).val();
    var res_adv_email  = $("#opponent_adv_email"+i).val();
    var res_adv_mobile = $("#opponent_adv_mobile"+i).val();
   //  if(res_mobile!=''){
   //   var intRegex = /[0-9 -()+]+$/;
   //   if((res_adv_mobile.length < 10) || (!intRegex.test(res_adv_mobile)))
   //   {
   //     alert('Please enter a valid phone number.');
   //     return false;
   //   }
   // }
    var resadvVo = {
      res_adv_name : res_adv_name,
      res_adv_email : res_adv_email,
      res_adv_mobile : res_adv_mobile
    }
    resadvArr.push(resadvVo);
  }
}
else{
  for(var i = 0;i<=petadv_count;i++){
    var pet_adv_name = $("#opponent_adv_name"+i).val();
    var pet_adv_email  = $("#opponent_adv_email"+i).val();
    var pet_adv_mobile = $("#opponent_adv_mobile"+i).val();
    var petVo = {
      pet_adv_name : pet_adv_name,
      pet_adv_email : pet_adv_email,
      pet_adv_mobile : pet_adv_mobile
    }
    petAdvArr.push(petVo);
  }
  for(var i = 0;i <= pet_count;i++){
    var pet_name = $("#opponent_name"+i).val();
    var pet_email  = $("#opponent_email"+i).val();
    var pet_mobile = $("#opponent_mobile"+i).val();
    var petVo = {
      pet_name : pet_name,
      pet_email : pet_email,
      pet_mobile : pet_mobile
    }
    petArr.push(petVo);
  }
  // var res_count = $("#res_count").val();
  for(var i = 0;i<=res_count;i++){
    var res_name = $("#your_name"+i).val();
    var res_email  = $("#your_email"+i).val();
    var res_mobile = $("#your_mobile"+i).val();
    var resVo = {
      res_name : res_name,
      res_email : res_email,
      res_mobile : res_mobile
    }
    resArr.push(resVo);
  }

  // var resadv_count = $("#resadv_count").val();
  for(var i = 0;i<=resadv_count;i++){
    var res_adv_name = $("#your_adv_name"+i).val();
    var res_adv_email  = $("#your_adv_email"+i).val();
    var res_adv_mobile = $("#your_adv_mobile"+i).val();
    var resadvVo = {
      res_adv_name : res_adv_name,
      res_adv_email : res_adv_email,
      res_adv_mobile : res_adv_mobile
    }
    resadvArr.push(resadvVo);
  }
}
}
      // var respAdv = resadvArr;
      // var petAdv = petAdvArr;
      /*$.each(petAdvArr,function(i,obj){
        var resObj = {
          res_adv_name : obj.pet_adv_name,
          res_adv_email : obj.pet_adv_email,
          res_adv_mobile : obj.pet_adv_mobile
        }
        respondentArr.push(resObj);
      });
      $.each(resadvArr,function(i,obj){
        var petObj = {
          pet_adv_name : obj.res_adv_name,
          pet_adv_email : obj.res_adv_email,
          pet_adv_mobile : obj.res_adv_mobile
        }
        petetitionerArr.push(petObj);
      });
      resadvArr = respondentArr;
      petAdvArr = petetitionerArr
    }
    else{
     resadvArr = resadvArr;
     petAdvArr  = petAdvArr;
   }
 }*/
  /*else{
    var petetitionerArr=[],respondentArr=[];
    if(are_you_appearing_as == 'Respondent'){
      $.each(petAdvArr,function(i,obj){
        var resObj = {
          res_adv_name : obj.pet_adv_name,
          res_adv_email : obj.pet_adv_email,
          res_adv_mobile : obj.pet_adv_mobile
        }
        respondentArr.push(resObj);
      });
      $.each(resadvArr,function(i,obj){
        var petObj = {
          pet_adv_name : obj.res_adv_name,
          pet_adv_email : obj.res_adv_email,
          pet_adv_mobile : obj.res_adv_mobile
        }
        petetitionerArr.push(petObj);
      });
      resadvArr = respondentArr;
      petAdvArr = petetitionerArr;
    }  
    else{
      resadvArr = resadvArr;
      petAdvArr = petAdvArr;
    }
  }*/

  var assign_to = $("#framework").val();
  var assign_to_array = [];
  $.each(assign_to,function(a,obj){
   assign_to_array.push(obj);
 });
// }
if(court_name == ''){
  toastr.error("","Please select Court",{timeout:5000});
  return false;
}

var caseVo = new Object();
caseVo.court_name = court_name;
caseVo.divisional_commisioner_level =divisional_commisioner_level;
caseVo.ministry_desk_no = ministry_desk_no;
caseVo.supreme_court = supreme_court;
caseVo.cases_sc_type = cases_case_type !='' ? $("#cases_case_type option:selected").text() : (cases_case_type == '0' ? $("#sc_casetype_other").val(): "");
caseVo.cases_sc_typeId = cases_case_type !='' ? $("#cases_case_type").val() : "" ;
caseVo.diary_number = diary_number;
caseVo.diary_year = diary_year;
caseVo.case_no = case_no;
caseVo.case_no_year = case_no_year;
caseVo.appearing_modal = appearing_modal;
caseVo.are_you_appearing_as = are_you_appearing_as;
caseVo.petitioner = petitioner;
// caseVo.respondent = respondent;
caseVo.dateoffilling = dateoffilling;
caseVo.court_hall = court_hall;
caseVo.floor = floor;
caseVo.classification = classification;nominal
caseVo.title = title;
caseVo.nominal = nominal;

caseVo.query = query;
caseVo.judge = judge;
caseVo.reffered_by = reffered_by;
caseVo.section_category = section_category;
caseVo.cases_priority = cases_priority;
if(court_name == '1'){
  if(caseVo.supreme_court == ''){
    $("#supreme_court").focus();
    toastr.error("","Please Select case number / diary number",{timeout:5000});
    return false;
  }
  if(caseVo.supreme_court=='Case Number' && caseVo.cases_sc_type == ''){
    $("#cases_case_type").focus();
    toastr.error("","Please Select case type",{timeout:5000});
    return false;
  }
  if(caseVo.case_no == '' && $("#case-div").css('display') == 'block'){
    toastr.error("","Please enter case number",{timeout:5000});
    return false;
  }
}

// caseVo.passport_radio = passport_radio;
if(court_name == '2'){
  caseVo.high_court = $("#high-court-list").val() != '' ? $("#high-court-list option:selected").text() :"";
  caseVo.bench = $("#high_court_bench_list").val() != '' ? $("#high_court_bench_list option:selected").text() : "";
  caseVo.side = $("#high_court_side_list").val() != '' ? $("#high_court_side_list option:selected").text() : "";  
  caseVo.stamp_register = stamp_register;
  caseVo.highcourt_casetype = $("#high_court_casetype").val() != '' ? $("#high_court_casetype option:selected").text() : ($("#high_court_casetype").val() == "0" ? $("#hc_casetype_other").val() : "");//
  caseVo.highcourt_casetypeId = $("#high_court_casetype").val() != '' ? $("#high_court_casetype").val() : "";
  if(doyou_havecnr == 'No'){
    if(caseVo.high_court == ''){
      toastr.error("","Please Select High Court",{timeout:5000});
      return false;
    }
    if(caseVo.bench == ''){
      toastr.error("","Please Select Bench",{timeout:5000});
      return false;
    }
    if(caseVo.side == ''){
      toastr.error("","Please Select Side",{timeout:5000});
      return false;
    }
    if(caseVo.stamp_register == ''){
      toastr.error("","Please Select Stamp Register",{timeout:5000});
      return false;
    }
    if(caseVo.highcourt_casetype == ''){
      toastr.error("","Please Select Case Type",{timeout:5000});
      return false;
    }
    if(caseVo.case_no == ''){
      toastr.error("","Please enter case number",{timeout:5000});
      return false;
    }

  }
}
else{
  caseVo.high_court = "";
  caseVo.bench = "";
  caseVo.side =  "";
  caseVo.highcourt_casetype = "";
  caseVo.stamp_register = "";
  caseVo.highcourt_casetypeId = '';
}

if(court_name == '3'){
  var masterIdState ='',masterIdCity='',masterIdEst = '',masterIdCaseType;
  caseVo.doyou_havecnr = doyou_havecnr;
  caseVo.cnr = cnr;

  caseVo.dc_state = $("#districtcourt_state").val() != '' ? $("#districtcourt_state option:selected").text() : "";
  caseVo.dc_city = $("#district_court_city").val() != '' ? $("#district_court_city option:selected").text() : "";
 caseVo.Establisment = $("#district_court_establishment").val() != '' ? $("#district_court_establishment option:selected").text() : "";//
 caseVo.district_court_casetype = $("#district_court_casetype").val() != '' ? $("#district_court_casetype option:selected").text() : ($("#district_court_casetype").val() == '0' ? $("#district_court_casetype_other").val() : "");//
 caseVo.district_court_casetypeId = $("#district_court_casetype").val() != '' ? $("#district_court_casetype").val() : "";

 if(caseVo.dc_state != '')
  masterIdState = $("#districtcourt_state").val().split("_");

 if(caseVo.dc_city!= '')
  masterIdCity = $("#district_court_city").val().split("_");

 if(caseVo.Establisment != '')
  masterIdEst = $("#district_court_establishment").val().split("_");

 caseVo.EstablismentId = masterIdEst[1];//
 caseVo.dc_stateId =  masterIdState[1];
 caseVo.dc_cityId = masterIdCity[1];

 if(doyou_havecnr == 'No'){
  // if($("#district_court_casetype").val() != ''){
  //   masterIdCaseType = $("#district_court_casetype").val().split("_");
  //   caseVo.district_court_casetypeId = masterIdCaseType[1] ;
  // }
  if(caseVo.dc_state == ''){
    toastr.error("","Please Select State",{timeout:5000});
    return false;
  }
  if(caseVo.dc_city == ''){
    toastr.error("","Please Select District",{timeout:5000});
    return false;
  }
  if(caseVo.Establisment == ''){
    toastr.error("","Please Select Court Establisment",{timeout:5000});
    return false;
  }
  if(caseVo.district_court_casetype == ''){
    toastr.error("","Please Select Case Type",{timeout:5000});
    return false;
  }
  if(caseVo.case_no == ''){
    toastr.error("","Please enter case number",{timeout:5000});
    return false;
  }

}
if(doyou_havecnr == 'Yes' && cnr == ''){
  $("#name").focus();
  toastr.error("","Please Enter CNR no.",{timeout:5000});
  return false;
}
else if(doyou_havecnr == 'Yes' && cnr != '' && (cnr.length < 16 || cnr.length > 16)){
  $("#name").focus();
  toastr.error(""," Please enter valid CNR",{timeout:5000});
  return false;
}
}
else{
  caseVo.dc_state = '';
  caseVo.dc_city = '';
  caseVo.Establisment = '';
  caseVo.doyou_havecnr = "";
  caseVo.cnr = "";
  caseVo.district_court_casetype =  '';
  caseVo.district_court_casetypeId = '';
  caseVo.dc_stateId = '';
  caseVo.dc_cityId = '';
  caseVo.EstablismentId = '';
}

caseVo.petArr = petArr;
caseVo.resArr = resArr;
caseVo.resadvArr = resadvArr;
caseVo.petAdvArr = petAdvArr;

caseVo.affidavite_filling_date = $("#affidavitBrowse").is(":visible") ? affidavite_filling_date : '';

caseVo.passport_radio = $("input[name='affidaviteRadios']:checked").val();

caseVo.vakalath_filling_date = $("#vakalathBrowse").is(":visible") ? vakalath_filling_date : '';

caseVo.vakalat_radio = $("input[name='vakalathRadios']:checked").val();

if(court_name == '7'){
  caseVo.commissionerate_state = $("#commissionerate_state").val() != '' ? $("#commissionerate_state option:selected").text() : "";
  caseVo.commisionrate_courtside = $("#commisionrate_courtside").val() != '' ? $("#commisionrate_courtside option:selected").text() : "";
  caseVo.commissionerate = $("#commissionerate_select").val() !='' ? $("#commissionerate_select").val() : '';
  caseVo.commissionerate_casetype = $("#commissionerate_casetype").val() != '' ? $("#commissionerate_casetype option:selected").text() : "";
  caseVo.commissionerate_authority = $("#authority_select").val() != '' ? $("#authority_select option:selected").text() : "";

  if(caseVo.commissionerate == ''){
    toastr.error("","Please select Commissionarate",{timeout:5000});
    return false;
  }
  if(caseVo.commissionerate == 'Charity Commissionerate' && caseVo.commissionerate_state == ''){
    toastr.error("","Please select State",{timeout:5000});
    return false;
  }
  if(caseVo.commissionerate == 'Charity Commissionerate' && caseVo.commisionrate_courtside == ''){
    toastr.error("","Please  select Court Side",{timeout:5000});
    return false;
  }

  if($("#courtsidecasetype").is(":visible") && caseVo.commissionerate_casetype == ''){
    toastr.error("","Please select Case Type",{timeout:5000});
    return false;
  }
  if(caseVo.commissionerate == 'Commissionerate of Commercial Taxes' && caseVo.commissionerate_authority == ''){
    toastr.error("","Please Select Authority",{timeout:5000});
    return false;
  }
  if(caseVo.case_no == ''){
    toastr.error("","Please enter case number",{timeout:5000});
    return false;
  }

}
else{
  caseVo.commissionerate_state = '';
  caseVo.commisionrate_courtside = '';
  caseVo.commissionerate = '';
  caseVo.commissionerate_casetype = '';
  caseVo.commissionerate_authority = '';
}

if(court_name == '4'){
  caseVo.commission = $("#commission").val() != '' ? $("#commission option:selected").text() : "";
  caseVo.commission_state = $("#commission_state").val() != '' ? $("#commission_state option:selected").text() : "";
  caseVo.commission_state_casetype = $("#commission_state_casetype").val() != '' ? $("#commission_state_casetype option:selected").text() : "";
  caseVo.commission_bench = $("#commission_bench").val() != '' ? $("#commission_bench option:selected").text() : "";

  if(caseVo.commission == ''){
    toastr.error("Please Select Commissions",{timeout:5000});
    return false;
  }
  if((caseVo.commission =='District Forum' || caseVo.commission =='State Commission')&& caseVo.commission_state == ''){
    toastr.error("Please Select Commissions State",{timeout:5000});
    return false;
  }
  if(caseVo.commission_state == 'Maharashtra' && caseVo.commission_state_casetype == ''){
    toastr.error("Please Select Commissions Case Type",{timeout:5000});
    return false;
  }
  if(caseVo.commission =='National Commission - NCDRC' && caseVo.commission_bench == ''){
    toastr.error("Please Select Commissions Bench",{timeout:5000});
    return false;
  }
  if(caseVo.case_no == ''){
    toastr.error("","Please enter case number",{timeout:5000});
    return false;
  }


}
else{
  caseVo.commission = '';
  caseVo.commission_state = '';
  caseVo.commission_state_casetype = '';
  caseVo.commission_bench = '';
}
// caseVo.commission_district = commission_district;
// caseVo.state_commission = state_commission;

if(court_name == '5'){
  caseVo.tribunal_authority = $("#tribunal_select").val() !='' ? $("#tribunal_select option:selected").text() : "";
  caseVo.tribunal_state_district = $("#tribunal_state").val() !='' ? $("#tribunal_state option:selected").text() : "" ;
  caseVo.tribunal_casetype = $("#tribunal_case").val() !='' ? $("#tribunal_case option:selected").text() : "";
  caseVo.tribunal_other = $("#tribunal_other").val() != '' ? $("#tribunal_other").val() : "";
  caseVo.tribunal_bench = $("#tribunal_bench").val() != '' ? $("#tribunal_bench option:selected").text() : "";
  if(caseVo.tribunal_authority == ''){
    toastr.error("","Please Select Tribunal Authority",{timeout:5000});
    return false;
  }
  if($("#tribunal_state").is(":visible") && caseVo.tribunal_state_district == ''){
    toastr.error("","Please Select Tribunal State",{timeout:5000});
    return false;
  }
  if(caseVo.tribunal_casetype == ''){
    toastr.error("","Please Select Case Type",{timeout:5000});
    return false;
  }
  if(caseVo.case_no == ''){
    toastr.error("","Please enter case number",{timeout:5000});
    return false;
  }
  /*if($("#other-case-type").is(":visible") && caseVo.tribunal_other == ''){
    toastr.error("","Please Enter other Case Type",{timeout:5000});
    return false;
  }*/

}
else{
  caseVo.tribunal_authority = '';
  caseVo.tribunal_state_district = '';
  caseVo.tribunal_casetype = '';
  caseVo.tribunal_other = '';
  caseVo.tribunal_bench='';
}
if(court_name == '6'){
  caseVo.revenue_casetype = $("#revenue_other").val() !='' ? $("#revenue_other").val() : "";
  caseVo.revenue_states = $("#revenue_states").val() !='' ? $("#revenue_states").val() : "";

  if(caseVo.revenue_states == ''){
    toastr.error("","Please Select revenue court",{timeout:5000});
    return false;
  }
  if(caseVo.revenue_casetype == ''){
    toastr.error("","Please Select revenue Case Type",{timeout:5000});
    return false;
  }
  if(caseVo.case_no == ''){
    toastr.error("","Please enter case number",{timeout:5000});
    return false;
  }
}
else{
  caseVo.revenue_casetype = '';
  caseVo.revenue_states = '';
}
if(court_name == '9'){
  caseVo.other_casetype = $("#court_other_input").val()!='' ? $("#court_other_input").val() : "";
  if(caseVo.other_casetype == ''){
    toastr.error("","Please Enter Case Type",{timeout:5000});
  }
  if(caseVo.case_no == ''){
    toastr.error("","Please enter case number",{timeout:5000});
    return false;
  }
}
// if(caseVo.title == ''){
// toastr.error("","Please Select Title",{timeout:5000});
// return false;
// }

caseVo.complete_details = getcase;
caseVo.assign_to_array = assign_to_array;
caseVo.petitioner_arr = petitioner_arr;

caseVo.department_name = $("#department").val() != '' ? $("#department option:selected").text() : "";
if(nastikramank != '')
caseVo.nastikramank = nastikramank;//
else if(account_type_input != 'Individual'){
  toastr.error("","Please enter nastikramank",{timeout:5000});
  return false;
}
// caseVo.globalOrderArrVar = globalOrderArrVar;
  // var regs = JSON.stringify($('#forms-sample').serialize());

/*  formData.append("pet_filename",pet_filename);
formData.append("affidavite_document",affidavite_document);*/

// caseVo.files = formData;
var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','xlsx','xls','doc','docx'];
if(!isEmpty($("#pet_file").val())){
  if ($.inArray($("#pet_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
    return false;
  }
}
if(!isEmpty($("#attach_file").val())){
  if ($.inArray($("#attach_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
    return false;
  }
}
else if($("#affidavitBrowse").is(":visible") && isEmpty($("#attach_file").val())){
  toastr.error("","Please Select affidavite document",{timeout:5000});
  return false;
}
if(!isEmpty($("#vakalat_file").val())){
  if ($.inArray($("#vakalat_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
    return false;
  }
}
else if($("#vakalathBrowse").is(":visible") && isEmpty($("#vakalat_file").val())){
  toastr.error("","Please Select vakalat document",{timeout:5000});
  return false;
}
$.ajax({
  url: host+'/submit_demo.php',
  type:'POST',
  data : caseVo,
    // contentType : 'application/json;utf-8',
    dataType: 'json',
    async:false,
    cache : false,
    success:function(data){
      console.log(data.caseid);
      if(data.status == 'success'){
        // alert("Data Submitted Successfully");
        // window.location.href='view_case.php?caseid='+ data.caseid;
        getFiles(data.case_id,data.caseid);
      }
      else
       toastr.error("",data.status,{timeout:5000});
   },
   error:function(e){
            // console.log(e);
          }
        });
}

function getFiles(caseid,urlId){
  var affidavite_file = '',pet_file='',vakalat_file='';
  var formData = new FormData();
  var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','xlsx','xls','doc','docx'];
  if(!isEmpty($("#pet_file").val())){
    // if ($.inArray($("#pet_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    //  toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
    //  return false;
    // }
    pet_file =  $("#appearingas_input").is(":visible") ? document.getElementById("pet_file").files[0] : "";
  }
  else
    pet_file = '';

  if(!isEmpty($("#attach_file").val())){

    /*if ($.inArray($("#attach_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
      return false;
    }*/
    affidavite_file = $("#affidavitBrowse").is(":visible") ? document.getElementById("attach_file").files[0] : "";//
  }
  else if($("#affidavitBrowse").is(":visible") && isEmpty($("#attach_file").val())){
   toastr.error("","Please Select affidavite document",{timeout:5000});
   return false;
 }

 if(!isEmpty($("#vakalat_file").val())){

  /*if ($.inArray($("#vakalat_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
    return false;
  }*/
    vakalat_file = $("#vakalathBrowse").is(":visible") ? document.getElementById("vakalat_file").files[0] : "";//
  }
  else if($("#vakalathBrowse").is(":visible") && isEmpty($("#vakalat_file").val())){
   toastr.error("","Please Select vakalat document",{timeout:5000});
   return false;
 }




 formData.append("pet_file",pet_file);
 formData.append("affidavite_document",affidavite_file);
 formData.append("vakalat_document",vakalat_file);
 formData.append("case_id",caseid);

 var ajaxResult = $.ajax({
   url : host+"/submit_files.php",
   type : 'POST',
   data : formData,
   cache : false,
   contentType : false,
   processData : false,
   success:function(response){
    var response = JSON.parse(response);
    if(response.status == 'success')
     window.location.href='view-case.php?caseid='+ urlId;
 },
 error:function(){
  toastr.error("","Error in adding case",{timeout:5000});
}
});
}

function datafetch(){
  /*var party1 = $("input[name='appearing_radio']:checked").val();
  var party2 = $("input[name='appearing_radio']:not(:checked)").val();*/
  var party1 = appearing_top_array[0];
  var party2 = appearing_top_array[1];
  var name = $("#name").val();
  var court = $("#cases-court_id").val();
  var cinos = encryptData(name);
  var cino =new Object();
  cino.cino=cinos;
  cino.courtType=$("#court_type").val();
  var districtc = name.substring(0,4);
  /*$(".fetchData_outer").addClass("fetchdataloader");
  $(".fetchData_outer").prop("disabled",true);*/
  if(court == 1){
    $("#sc_info").show();
    getSCCases(party1,party2);
  }
  else if(court == 5){
    getTribunalCourtCasesFetchData(party1,party2);
  }
  else if(court == 2){
    getCasesHighCourt(party1,party2);
  }
  else if(court == 3){

    if($("input[name='membershipRadios']:checked").val() == 'Yes'){
      if(court == 3 && name == ''){
        alert("Please enter CNR no.");
        $("#data").css('display', 'none');
      }
      else if(name !=''){
      // $("#fetch_cnr_data_id").addClass("fetchdataloader");
      $(".fetchLoaderDiv").addClass("fetchdataloader");
      $("#fetch_cnr_data_id").hide();
      // $("#fetch_cnr_data_id").prop("disabled",true);  
      getDistrictCourtCases(cino,party1,party2);
    }
  }

  else{
   getCaseNumberFetchDataCases(party1,party2);
 }
}

else{
  $("#sc_info").hide();
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetchData_outer").show();
    /*$(".fetchData_outer").html("<i class='fa fa-plus'></i> Fetch Data");
    $(".fetchData_outer").prop("disabled",false);*/
    toastr.info("","Unable to fetch data through provided CNR",{timeout:5000});
    $("#data").modal("hide");
  }
}

function onlyCharAllowed(e){
  var k;
  document.all ? k = e.keyCode : k = e.which;
  return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}

function getCaseNumberFetchDataCases(party1,party2){
  var state = $("#districtcourt_state").val().split("_");
  var state_code = encryptData(state[1]);
  var district = $("#district_court_city").val().split("_");
  var dist_code = encryptData(district[1]);
  var court = $("#district_court_establishment").val().split("_");  
  var courtCode = encryptData(court[1]);
  var courtType = $("#court_type").val();
  var appeal_type = $("#district_court_casetype").val();
  var appeal_no = $("#case_no").val();
  var appeal_year = $("#case_no_year").val();

  var caseTypeVal = encryptData(appeal_type);
  var caseNumber = encryptData(appeal_no);
  var year = encryptData(appeal_year);

/*  $(".fetchData_outer").text("Loading...");
$(".fetchData_outer").prop("disabled",true);*/
$(".fetchLoaderDiv").addClass("fetchdataloader");
$("#fetchData_outer").hide();
$.ajax({
  type: "GET",
  url: legalResearch+'/search_by_casenumber',
  data: {
    state_code : state_code,
    dist_code : dist_code,
    court_code : courtCode,
    courtType : courtType,
    caseTypeVal : caseTypeVal,
    caseNumber : caseNumber,
    year : year
  },
  cache: false,
  async:false,
  headers: {
    "Content-Type": 'application/json'
  },

  success: function(response){
    if(isEmpty(response)){
        /*$(".fetchData_outer").html("<i class='fa fa-plus'></i> Fetch Data");
        $(".fetchData_outer").prop("disabled",false);*/
        $(".fetchLoaderDiv").removeClass("fetchdataloader");
        $("#fetchData_outer").show();
        toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
      }
      else if(response.caseNos.length > 0){
       getcase = response.caseNos;
       var showdata ='';
       $.each(response.caseNos,function(i,obj){    
        getCaseByCNRFetchCase(obj.cino,party1,party2);
      });
     }
     else{
        /*$(".fetchData_outer").html("<i class='fa fa-plus'></i> Fetch Data");
        $(".fetchData_outer").prop("disabled",false);*/
        $(".fetchLoaderDiv").removeClass("fetchdataloader");
        $("#fetchData_outer").show();
        toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
      }
    },
    error:function(e){
     $(".fetchLoaderDiv").removeClass("fetchdataloader");
     $("#fetchData_outer").show();
     toastr.error("","Site is slow please try after some time",{timeout:5000});
   }

 });
}

function getTribunalCourtCasesFetchData(party1,party2){
  var caseNumber = isEmpty($("#case_no").val()) ? "0" : $("#case_no").val();
  var year = isEmpty($("#case_no_year").val()) ? "0" : $("#case_no_year").val();
  var party_name_search = "";
  var data = {
    party : party_name_search,
    case_number : caseNumber,
    case_year : year
  }
  var url = legalResearch+'/mat_search_by_party';
  $.ajax({
    type: "GET",
    url: url,
    data: data,
    cache: false,
    async:false,
    headers: {
      "Content-Type": 'application/json'
    },
    // contentType: 'application/json;utf-8',  
    success: function(response){
      document.getElementById("fetch_case_modal").reset();
      console.log(response);
      var tr_party = '';
      if($("#cases-court_id").val() == 5){
        tribunal_search = response;
        var showdata ='';
        showdata += '<form id="reg-form" name="reg-form" class="table-responsive"><table class="pull-data-popup table table-bordered" style=""><tbody style="">';
        showdata += '<tr id="row1" style=""><td class="no-padding">'+'<table class="no-bordered"><tbody>';
        showdata += '<tr class="row-inner">';
        showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
        showdata += '<td><input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+tribunal_appleant+'"></td>';
        showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
        showdata += '<input type="text" class="form-control rowVal fullname" value="'+tribunal_appleant+'" name="petitionerorRespondentName" id="sel_pet_name_0"></td>';
        showdata += '<td data-th="Email Address"><label>Email Address:</label>';
        showdata += '<input type="email" class="form-control rowVal email" value="" id="sel_pet_email_0" name="petitionerorRespondentNameemail" ></td>';
        showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
        showdata += '<input type="text" class="form-control rowVal phonenumber" id="sel_pet_mobile_0" value="" name="petitionerorRespondentphone"></td></tr>';

        showdata +='</tbody></table></td>'+
        '<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
        '<select id="sel_pet" class="form-control" style="" name="Petitionerrespom">'+
    // '<option value="">Please select</option>'+
    '<option value="Petitioner">Petitioner</option>'+
    '<option value="Respondent">Respondent</option>'+
    '</select></td></tr>';
    showdata +='<tr id="row1" style=""><td class="no-padding">'+
    '<table class="no-bordered"><tbody>';
    showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
    showdata += '<input type="hidden" value="" name="cnr_no">';
    showdata += '<td><input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+tribunal_respondent+'"></td>';
    showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
    showdata += '<input type="text" class="form-control rowVal fullname" value="'+tribunal_respondent+'" name="petitionerorRespondentName" id="sel_res_name_0"></td>';
    showdata += '<td data-th="Email Address"><label>Email Address:</label>';
    showdata += '<input type="email" class="form-control rowVal email" value="" id="sel_res_email_0" name="petitionerorRespondentNameemail" > </td>';
    showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
    showdata += '<input type="text" class="form-control rowVal phonenumber" value="" id="sel_res_mobile_0" name="petitionerorRespondentphone"></td></tr>';

    showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
    '<td class="td_dd" style="">'+
    '<select id="sel_res" class="form-control" style="" name="Petitionerrespomr">'+
    // '<option value="">Please select</option>'+
    '<option value="Respondent">Respondent  </option>'+
    '<option value="Petitioner">Petitioner  </option>'+
    
    '</select></td></tr>';

    showdata +='</tbody></table><div class="submit_data col-sm-12" id="submit_data">'+
    '<input type="button" class="btn btn-info newsubmit btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
    '</div></form>';

    $("#modal-data").html(showdata);
    $("#data").modal('show');
  }
},
error:function(){

}
});

}
// var globalOrderArrVar='';
function getCasesHighCourt(party1,party2){
  $(".fetchLoaderDiv").addClass("fetchdataloader");
  $("#fetchData_outer").hide();
  $.ajax({
    type:'GET',
    url:legalResearch+'/search_by_bom',
    data:{
      stamp : $("#cases-high_court_id option:selected").text(),
      party : "",
      side : $("#high_court_side_list option:selected").text(),
      bench : $("#high_court_bench_list option:selected").text(),
      cType : $("#high_court_casetype").val(),
      cYear :  $("#case_no_year").val(),
      cNo : $("#case_no").val(),
      sType : "FETCH",
      id : ""
    },
    success:function(response){
      getcase = response;
      $("#anycondition").modal('hide');
      var showdata ='';
      if(response.length > 0){
        $.each(response,function(i,obj){
          var orderArr = [];
          /*$.each(obj.orderVOs,function(index,object){
            if(!isEmpty(object.pdfArray)){
              // The Base64 string of a simple PDF file
              // var b64 = object.pdfArray;

              // Decode Base64 to binary and show some information about the PDF file (note that I skipped all checks)
              // var bin = atob(b64);

              // Embed the PDF into the HTML page and show it to the user
              // var obj = document.createElement('object');
              // obj.style.width = '100%';
              // obj.style.height = '842pt';
              // obj.type = 'application/pdf';
              // obj.data = 'data:application/pdf;base64,' + b64;
              // document.body.appendChild(obj);

              // Insert a link that allows the user to download the PDF file
              // var link = document.createElement('a');
              // link.innerHTML = 'Download PDF file';
              // link.download = 'file.pdf';
              // link.href = 'data:application/octet-stream;base64,' + b64;
              // // document.body.appendChild(link);

              // orderArr.push(link.href);

              }
            });*/

          // globalOrderArrVar = orderArr;
          const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
          ];
          if(!isEmpty(obj.fillingDate)){
            // const d = new Date(obj.fillingDate);
            // var fildate = monthNames[d.getMonth()];
            // $("#dateoffilling").val(fildate +" "+ d.getDate() + ","+ d.getFullYear());
            $("#dateoffilling").val(obj.fillingDate);
          }

          showdata += '<form id="reg-form" name="reg-form" class="table-responsive"><table class="pull-data-popup table table-bordered" style=""><tbody style="">';

          if(!isEmpty(obj.petioner)){
           showdata += '<tr id="row1" style=""><td class="no-padding">'+'<table class="no-bordered"><tbody>';
           var counter =0;
           $.each(obj.petioner,function(index,object){
            counter++;
            showdata += '<tr class="row-inner">';
            showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
            showdata += '<td>';

            showdata += '<div class="checkbox-zoom zoom-primary">';
            showdata += '<label>';
            showdata += '<input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+object+'"> ';
            showdata += '<span class="cr">';
            showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
            showdata += '</span>';
            showdata += '<span>'+counter+'</span>';
            showdata += ' </label>';
            showdata += '</div>'; 
            showdata += '</td>';
            showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
            showdata += '<textarea rows="2" class="form-control rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_pet_name_'+index+'">'+object+'</textarea></td>';
            showdata += '<td data-th="Email Address"><label>Email Address:</label>';
            showdata += '<input type="email" class="form-control rowVal email" value="" id="sel_pet_email_'+index+'" name="petitionerorRespondentNameemail" ></td>';
            showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
            showdata += '<input type="text" class="form-control rowVal phonenumber" id="sel_pet_mobile_'+index+'" value="" name="petitionerorRespondentphone"></td></tr>';
          });
           showdata +='</tbody></table></td>'+
           '<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
           '<select id="sel_pet" class="form-control party1_drop" style="" name="Petitionerrespom">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'" class="party1_drop">'+party1+'</option>'+
        '<option value="'+party2+'" class="party2_drop">'+party2+'</option>'+
        '</select></td></tr>';
      }
      if(!isEmpty(obj.respondent)){
       var counter =0;
       showdata +='<tr id="row1" style=""><td class="no-padding">'+
       '<table class="no-bordered"><tbody>';
       $.each(obj.respondent,function(index,object){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
        showdata += '<input type="hidden" value="" name="cnr_no">';
        showdata += '<td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+object+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';
        showdata += '</td>';
        showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
        showdata += '<textarea rows="2" class="form-control rowVal fullname" value="" name="petitionerorRespondentName" id="sel_res_name_'+index+'">'+object+'</textarea></td>';
        showdata += '<td data-th="Email Address"><label>Email Address:</label>';
        showdata += '<input type="email" class="form-control rowVal email" value="" id="sel_res_email_'+index+'" name="petitionerorRespondentNameemail" > </td>';
        showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
        showdata += '<input type="text" class="form-control rowVal phonenumber" value="" id="sel_res_mobile_'+index+'" name="petitionerorRespondentphone"></td></tr>';
      });
       showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
       '<td class="td_dd" style="">'+
       '<select id="sel_res" class="form-control party2_drop" style="" name="Petitionerrespomr">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'" class="party2_drop">'+party2+'  </option>'+
        '<option value="'+party1+'" class="party1_drop">'+party1+'  </option>'+

        '</select></td></tr>';
      }
      if(!isEmpty(obj.petionerAdv)){
       showdata += '<tr id="row1" style="">';
       showdata += '<td style="border-right: 1px solid #ccc;">';
       showdata += '<table class="no-bordered"><tbody>';

       var counter =0;
       $.each(obj.petionerAdv,function(index,object){
        counter++;
        showdata += '<tr class="row-inner">';
        showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
        showdata += '<td>';

        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+object+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';
        showdata += '</td>';
        showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
        showdata += '<textarea rows="2" class="form-control rowVal fullname" value="" name="petitionerorRespondentName" id="sel_petadv_name_'+index+'">'+object+'</textarea></td>';
        showdata += '<td data-th="Email Address"><label>Email Address:</label>';
        showdata +='<input type="email" class="form-control rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+index+'" > </td>';
        showdata +='<td data-th="Phone Number"><label>Phone Number:</label>';
        showdata +='<input type="text" class="form-control rowVal phonenumber" value="" id="sel_petadv_mobile_'+index+'" name="petitionerorRespondentphone"></td></tr>';
      });
       showdata +='</tbody></table></td>';
       showdata +='<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
       showdata +='<select id="sel_petadv" class="form-control party1_drop" style="" name="Petitionerrespompa">'+
        // showdata +='<option value="">Please select</option>'+
        '<option value="'+party1+'Adv" class="party1_drop">'+party1+' Advocate</option>'+
        '<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option>'+
        '</select></td></tr>';
      }
      if(!isEmpty(obj.respondentAdv)){
       var counter =0;
       showdata +='<tr id="row1" style="">'+
       '<td style="border-right: 1px solid #ccc;">'+
       '<table class="no-bordered"><tbody>';
       $.each(obj.respondentAdv,function(index,object){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
        showdata += '<input type="hidden" value="" name="cnr_no"><td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+object+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';
        showdata += '</td>';

        showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
        showdata += '<textarea rows="2" class="form-control rowVal fullname" value="" name="petitionerorRespondentName" id="sel_resadv_name_'+index+'">'+object+'</textarea></td>';
        showdata += '<td data-th="Email Address"><label>Email Address:</label>';
        showdata += '<input type="email" class="form-control rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_resadv_email_'+index+'" > </td>';
        showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
        showdata += '<input type="text" class="form-control rowVal phonenumber" value="" id="sel_resadv_mobile_'+index+'" name="petitionerorRespondentphone"></td></tr>';        
      });
       showdata += '</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
       showdata += '<select id="sel_resadv" class="form-control party2_drop" style="" name="Petitionerrespompa">'+
        // showdata +='<option value="">Please select</option>'+
        '<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option>'+
        '<option value="'+party1+'Adv" class="party1_drop">'+party1+' Advocate</option>'+
        '</select></td></tr>';
      }
      showdata +='</tbody></table><div class="submit_data col-sm-12" id="submit_data">'+
      '<input type="button" class="btn btn-info newsubmit btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
      '</div></form>';
    });
  $("#modal-data").html(showdata);
  $("#data").modal('show');
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetchData_outer").show();
  /*$(".fetchData_outer").html("<i class='fa fa-plus'></i> Fetch Data");
  $(".fetchData_outer").prop("disabled",false);*/
}
else{
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetchData_outer").show();
  /*$(".fetchData_outer").html("<i class='fa fa-plus'></i> Fetch Data");
  $(".fetchData_outer").prop("disabled",false);*/
  toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});
}
},
error:function(){
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetchData_outer").show();
  /*$(".fetchData_outer").html("<i class='fa fa-plus'></i> Fetch Data");
  $(".fetchData_outer").prop("disabled",false);*/
  toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});
}
});
}

function getDistrictCourtCases(cino,party1,party2){
  $.ajax({
    type: "GET",
    url: legalResearch+"/search_by_cnr",
    data: cino,
    cache: false,
    async:false,
    headers: {
      "Content-Type": 'application/json'
    },    
    success: function(get){
      if(get!='' || get.length > 0){
        getcase = get;
        var showdata ='';
        var casedetail = '<div class="col-sm-12"><b>State: </b><span>'+get.state_name+'</span></div>'+
        '<div class="col-sm-12"><b>District: </b> <span> '+get.district_name+'</span></div>'+
        '<div class="col-sm-12"><b> Court Establishment: </b> <span> Other</span></div>'+
        '<div class="col-sm-12"><b> Case Type: </b> <span> '+get.type_name+'</span></div>'+
        '<div class="col-sm-12"><b> Case Number: </b> <span> '+get.reg_no+'</span></div>'+
        '<div class="col-sm-12"><b> Case Year: </b> <span> '+get.reg_year+'</span></div>';
        $("#casedetails").html(casedetail);
        $("#casedetails-div").show();
        $("#case_insert_show").show();

        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
        ];
        if(!isEmpty(get.date_of_filing)){
          /*const d = new Date(get.date_of_filing);
          var fildate = monthNames[d.getMonth()];
          $("#dateoffilling").val(fildate +" "+ d.getDate() + ","+ d.getFullYear());*/
          $("#dateoffilling").val(get.date_of_filing);
        }
        $("#nominal").val(get.pet_name+' vs '+ get.res_name);
        $("#case_no").val(get.reg_no);

        $('#case_no_year').val(get.reg_year);
        $('#case_no_year').select2().trigger('change');

        $("#district_court_casetype").val($("#district_court_casetype option:contains("+get.type_name+")").val());
        $("#district_court_casetype").select2().trigger('change');

        /*$("#").val();*/
        // $('#district_court_casetype').select2().trigger('change');

        showdata += '<form id="reg-form" name="reg-form" class="table-responsive"><table class="pull-data-popup table table-bordered table-bordered table" style=""><tbody style=""><tr id="row1" style="">'+
        '<td class="no-padding">'+'<table class="no-bordered"><tbody>';
        if(get.petitioner_array.length > 0){
          var counter =0;
          $.each(get.petitioner_array,function(i,obj){
            counter++;
            showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
            showdata += '<div class="checkbox-zoom zoom-primary">';
            showdata += '<label>';
            showdata += '<input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+obj+'">';
            showdata += '<span class="cr">';
            showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
            showdata += '</span>';
            showdata += '<span>'+counter+'</span>';
            showdata += ' </label>';
            showdata += '</div>';



            showdata += '</td><td data-th="Full Name"><label style="width:100%">Full Name:</label>';
            showdata +='<textarea rows="2" class="form-control rowVal fullname" name="petitionerorRespondentName" id="sel_pet_name_'+i+'">'+obj+'</textarea></td>'+
            '<td data-th="Email Address"><label>Email Address:</label><input type="email" class="form-control rowVal email" value="" id="sel_pet_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" id="sel_pet_mobile_'+i+'" value="" name="petitionerorRespondentphone"></td></tr>';
          });

          showdata +='</tbody></table></td>'+
          '<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
          '<select id="sel_pet" class="form-control party1_drop" style="" name="Petitionerrespom">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'" class="party1_drop">'+party1+'</option>'+
        '<option value="'+party2+'" class="party2_drop">'+party2+'</option>'+
        '</select></td></tr>';
      }
      if(get.respondent_array.length > 0){
       var counter =0;
       showdata +='<tr id="row1" style=""><td class="no-padding">'+
       '<table class="no-bordered"><tbody>';
       $.each(get.respondent_array,function(i,res){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+res+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';

        showdata+='</td><td data-th="Full Name"><label style="width:100%">Full Name:</label>';
        showdata+='<textarea rows="2" class="form-control rowVal fullname" name="petitionerorRespondentName" id="sel_res_name_'+i+'">'+res+'</textarea></td>'+
        '<td data-th="Email Address"><label>Email Address:</label><input type="email" class="form-control rowVal email" value="" id="sel_res_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" value="" id="sel_res_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';
      }) ;
       showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
       '<td class="td_dd" style="">'+
       '<select id="sel_res" class="form-control party2_drop" style="" name="Petitionerrespomr">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'" class="party2_drop">'+party2+'</option>'+
        '<option value="'+party1+'" class="party1_drop">'+party1+'</option>'+
        
        '</select></td></tr>';
      }

      if(get.petitioner_adv_array.length > 0){
       var counter =0;
       showdata +='<tr id="row1" style="">'+
       '<td class="no-padding">'+
       '<table class="no-bordered"><tbody>';

       $.each(get.petitioner_adv_array,function(i,peta){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+peta+'"> ';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';

        showdata+='</td><td data-th="Full Name"><label style="width:100%">Full Name:</label>';
        showdata+='<textarea rows="2" class="form-control rowVal fullname" name="petitionerorRespondentName" id="sel_petadv_name_'+i+'">'+peta+'</textarea></td>'+
        '<td data-th="Email Address"><label>Email Address:</label><input type="email" class="form-control rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+i+'" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" value="" id="sel_petadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';      
      }) ;
       showdata +='</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
       '<select id="sel_petadv" class="form-control party1_drop" style="" name="Petitionerrespompa">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'Adv" class="party1_drop">'+party1+' Advocate</option>'+
        '<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option>'+
        '</select></td></tr>';
      }
      if(get.respondent_adv_array.length > 0){
       var counter =0;
       showdata +='<tr id="row1" style="">'+
       '<td class="no-padding">'+
       '<table class="no-bordered"><tbody>';

       $.each(get.respondent_adv_array,function(i,resa){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+resa+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';

        showdata+='</td>';
        showdata+='<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
        showdata+='<textarea class="form-control rowVal fullname" rows="2" name="petitionerorRespondentName" id="sel_resadv_name_'+i+'">'+resa+'</textarea></td>';
        showdata+='<td data-th="Email Address"><label>Email Address:</label>';
        showdata+='<input type="text" class="form-control rowVal email" rows="2" id="sel_resadv_email_'+i+'" name="petitionerorRespondentNameemail"> </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" id="sel_resadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';      
      });

       showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+'<td class="td_dd" style="">'+
       '<select id="sel_resadv" class="form-control party2_drop" style="" name="Petitionerrespomra">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option><option value="'+party1+'Adv" class="party1_drop">'+party1+' Advocate</option>'+
        '</select></td></tr>';
      }
      showdata +='</tbody></table><div class="submit_data col-sm-12" id="submit_data" style="text-align:right">'+
      '<input type="button" class="btn btn-info newsubmit btn-sm btn-info" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
      '</div></form>';
      $("#modal-data").html(showdata);
      $("#data").modal('show');
  /*$("#fetch_cnr_data_id").html("<i class='fa fa-plus'></i> Fetch Data");
  $("#fetch_cnr_data_id").prop("disabled",false); */
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetch_cnr_data_id").show();
}
else{
  /*$("#fetch_cnr_data_id").html("<i class='fa fa-plus'></i> Fetch Data");  
  $("#fetch_cnr_data_id").prop("disabled",false); */  
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetch_cnr_data_id").show();
  toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
}
},

error:function(e){
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetch_cnr_data_id").show();
  /*$("#fetch_cnr_data_id").prop('disabled',false);
    // $(".flip-square-loader").hide();
    $("#fetch_cnr_data_id").html("<i class='fa fa-plus'></i> Fetch Data");*/
    toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
  }
});
}

function getCaseByCNRFetchCase(cnr_no,party1,party2){
  var cino =new Object();
  cino.cino=encryptData(cnr_no);
  cino.courtType=$("#court_type").val();
  $(".fetchLoaderDiv").addClass("fetchdataloader");
  $("#fetchData_outer").hide();
  $.ajax({
    type: "GET",
    url: legalResearch+"/search_by_cnr",
    // data: "cino="+cino+"&&courtType=SC",
    data: cino,
    cache: false,
    async:false,
    headers: {
      "Content-Type": 'application/json'
    },
    success: function(get){
      if(get!='' || get.length > 0){
        getcase = get;
        var showdata ='';
        var casedetail = '<div class="col-sm-12"><b>State: </b><span>'+get.state_name+'</span></div><br>'+
        '<div class="col-sm-12"><b>District: </b> <span> '+get.district_name+'</span></div><br>'+
        '<div class="col-sm-12"><b> Court Establishment: </b> <span> '+get.court_name+'</span></div><br>'+
        '<div class="col-sm-12"><b> Case Type: </b> <span> '+get.type_name+'</span></div><br>'+
        '<div class="col-sm-12"><b> Case Number: </b> <span> '+get.reg_no+'</span></div><br>'+
        '<div class="col-sm-12"><b> Case Year: </b> <span> '+get.reg_year+'</span></div><br>';
        $("#casedetails").html(casedetail);
        $("#casedetails-div").show();
        $("#case_insert_show").show();

        const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
        ];
        if(!isEmpty(get.date_of_filing)){
         /* const d = new Date(get.date_of_filing);
          var fildate = monthNames[d.getMonth()];
          $("#dateoffilling").val(fildate +" "+ d.getDate() + ","+ d.getFullYear());*/
          $("#dateoffilling").val(get.date_of_filing);
        }
        $("#nominal").val(get.pet_name+' vs '+ get.res_name);
        $("#case_no").val(get.reg_no);

        $('#case_no_year').val(get.reg_year);
        $('#case_no_year').select2().trigger('change');

        $("#district_court_casetype").val($("#district_court_casetype option:contains("+get.type_name+")").val());
        $("#district_court_casetype").select2().trigger('change');

        /*$("#").val();*/
        // $('#district_court_casetype').select2().trigger('change');

        showdata += '<form id="reg-form" name="reg-form" class="table-responsive"><table class="pull-data-popup table table-bordered" style=""><tbody style=""><tr id="row1" style="">'+
        '<td class="no-padding">'+'<table class="no-bordered"><tbody>';
        if(get.petitioner_array.length > 0){
          var counter =0;
          $.each(get.petitioner_array,function(i,obj){
            counter++;
            showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
            showdata += '<div class="checkbox-zoom zoom-primary">';
            showdata += '<label>';
            showdata += '<input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+obj+'">';
            showdata += '<span class="cr">';
            showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
            showdata += '</span>';
            showdata += '<span>'+counter+'</span>';
            showdata += ' </label>';
            showdata += '</div>';

            showdata+='</td><td data-th="Full Name"><label style="width:100%">Full Name:</label><textarea rows="2" class="form-control rowVal fullname" value="" name="petitionerorRespondentName" id="sel_pet_name_'+i+'">'+obj+'</textarea></td>'+
            '<td data-th="Email Address"><label>Email Address:</label><input type="email" class="form-control rowVal email" value="" id="sel_pet_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" id="sel_pet_mobile_'+i+'" value="" name="petitionerorRespondentphone"></td></tr>';
          });

          showdata +='</tbody></table></td>'+
          '<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
          '<select id="sel_pet" class="form-control party1_drop" style="" name="Petitionerrespom">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'" class="party1_drop">'+party1+'</option>'+
        '<option value="'+party2+'" class="party2_drop">'+party2+'</option>'+
        '</select></td></tr>';
      }
      if(get.respondent_array.length > 0){
       var counter =0;
       showdata +='<tr id="row1" style=""><td class="no-padding">'+
       '<table class="no-bordered"><tbody>';
       $.each(get.respondent_array,function(i,res){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+res+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';

        showdata+='</td><td data-th="Full Name"><label style="width:100%">Full Name:</label><textarea rows="2" class="form-control rowVal fullname" value="" name="petitionerorRespondentName" id="sel_res_name_'+i+'">'+res+'</textarea></td>'+
        '<td data-th="Email Address"><label>Email Address:</label><input type="email" class="form-control rowVal email" value="" id="sel_res_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" value="" id="sel_res_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';
      }) ;
       showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
       '<td class="td_dd" style="">'+
       '<select id="sel_res" class="form-control party2_drop" style="" name="Petitionerrespomr">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'" class="party2_drop">'+party2+'</option>'+
        '<option value="'+party1+'" class="party1_drop">'+party1+'</option>'+

        '</select></td></tr>';
      }

      if(get.petitioner_adv_array.length > 0){
       var counter =0;
       showdata +='<tr id="row1" style="">'+
       '<td class="no-padding">'+
       '<table class="no-bordered"><tbody>';

       $.each(get.petitioner_adv_array,function(i,peta){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+peta+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>';

        showdata+=' </td><td data-th="Full Name"><label style="width:100%">Full Name:</label><textarea rows="2" class="form-control rowVal fullname" value="" name="petitionerorRespondentName" id="sel_petadv_name_'+i+'">'+peta+'</textarea></td>'+
        '<td data-th="Email Address"><label>Email Address:</label><input type="email" class="form-control rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+i+'" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" value="" id="sel_petadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';      
      }) ;
       showdata +='</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
       '<select id="sel_petadv" class="form-control party1_drop" style="" name="Petitionerrespompa">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'Adv" class="party1_drop">'+party1+' Advocate</option>'+
        '<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option>'+
        '</select></td></tr>';
      }
      if(get.respondent_adv_array.length > 0){
       var counter =0;
       showdata +='<tr id="row1" style="">'+
       '<td class="no-padding">'+
       '<table class="no-bordered"><tbody>';

       $.each(get.respondent_adv_array,function(i,resa){
        counter++;
        showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no"><td>';
        showdata += '<div class="checkbox-zoom zoom-primary">';
        showdata += '<label>';
        showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+resa+'">';
        showdata += '<span class="cr">';
        showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
        showdata += '</span>';
        showdata += '<span>'+counter+'</span>';
        showdata += ' </label>';
        showdata += '</div>'; 
        showdata+='</td><td data-th="Full Name"><label style="width:100%">Full Name:</label><textarea rows="2" class="form-control rowVal fullname" value="" name="petitionerorRespondentName" id="sel_resadv_name_'+i+'">'+resa+'</textarea></td>'+
        '<td data-th="Email Address"><label>Email Address:</label><input type="email" class="form-control rowVal email" value="" id="sel_resadv_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="form-control rowVal phonenumber" value="" id="sel_resadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>'      
      });

       showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+'<td class="td_dd" style="">'+
       '<select id="sel_resadv" class="form-control party2_drop" style="" name="Petitionerrespomra">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option><option class="party1_drop" value="'+party1+'Adv">'+party1+' Advocate</option>'+
        '</select></td></tr>';
      }
      showdata +='</tbody></table><div class="submit_data col-sm-12" id="submit_data">'+
      '<input type="button" class="btn btn-info newsubmit btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
      '</div></form>';
      $("#modal-data").html(showdata);
      $("#data").modal('show');
      $(".fetchLoaderDiv").removeClass("fetchdataloader");
      $("#fetchData_outer").show();
    }
    else{
     $(".fetchLoaderDiv").removeClass("fetchdataloader");
     $("#fetchData_outer").show();
     toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
   }
 },

 error:function(e){
   $(".fetchLoaderDiv").removeClass("fetchdataloader");
   $("#fetchData_outer").show();
   toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
 }
});
}

function getSCCases(party1,party2){
  $(".fetchLoaderDiv").addClass("fetchdataloader");
  $("#fetchData_outer").hide();
  var supreme_court = $("#supreme_court").val();
  if(supreme_court == 'Case Number'){
    var appeal_no = $("#case_no").val();
    var appeal_type = $("#cases_case_type").val();
    var appeal_year = $("#case_no_year").val();
    var party_name = "";
    var dairyNo = "";
    var diaryYear = "";
    var searchType = "PARTY_SEARCH";
  }
  else{
    var appeal_no = "";
    var appeal_type ="";
    var appeal_year ="";
    var party_name = "";
    var dairyNo = $("#diary_number").val().trim();
    var diaryYear = $("#diary_year").val().trim();
    var searchType = "DAIRY";
  }
  $.ajax({
    type:'GET',
    url:legalResearch+'/search_by_sc',
    data:{
      caseNo : appeal_no,
      caseType : appeal_type,
      caseYear : appeal_year,
      party : party_name,
      dairyNo : dairyNo,
      dairyYear : diaryYear,
      searchType : searchType
    },
    success:function(response){
      var showdata ='';
      getcase = response;
      $.each(response,function(i,obj){    
        if((!isEmpty(obj.diaryNo) && !isEmpty(obj.diaryYear)) || !isEmpty(obj.caseNo)){
          const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
          "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
          ];
          if(!isEmpty(obj.filedDate)){
                  /*const d = new Date(obj.filedDate);
                  var fildate = monthNames[d.getMonth()];
                  $("#dateoffilling").val(fildate +" "+ d.getDate() + ","+ d.getFullYear());*/
                  $("#dateoffilling").val(obj.filedDate);
                }
                $("#classification").val(obj.category);  
                showdata += '<form id="reg-form" name="reg-form" class="table-responsive"><table class="pull-data-popup table table-bordered" style=""><tbody style="">';
                if(obj.petitioner.length > 0 || !isEmpty(obj.petitioner)){
                 showdata += '<tr id="row1" style=""><td class="no-padding">'+'<table class="no-bordered"><tbody>';
                 var counter =0;
                 $.each(obj.petitioner,function(index,object){
                  counter++;
                  showdata += '<tr class="row-inner">';
                  showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
                  showdata += '<td>';

                  showdata += '<div class="checkbox-zoom zoom-primary">';
                  showdata += '<label>';
                  showdata += '<input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+object+'"> ';
                  showdata += '<span class="cr">';
                  showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
                  showdata += '</span>';
                  showdata += '<span>'+counter+'</span>';
                  showdata += ' </label>';
                  showdata += '</div>'; 
                  showdata += '</td>';
                  showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
                  showdata += '<textarea rows="2" class="form-control rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_pet_name_'+index+'">'+object+'</textarea></td>';
                  showdata += '<td data-th="Email Address"><label>Email Address:</label>';
                  showdata += '<input type="email" class="form-control rowVal email" value="" id="sel_pet_email_'+index+'" name="petitionerorRespondentNameemail" ></td>';
                  showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
                  showdata += '<input type="text" class="form-control rowVal phonenumber" id="sel_pet_mobile_'+index+'" value="" name="petitionerorRespondentphone"></td></tr>';
                });
                 showdata +='</tbody></table></td>'+
                 '<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
                 '<select id="sel_pet" class="form-control party1_drop" style="" name="Petitionerrespom">'+
              // '<option value="">Please select</option>'+
              '<option value="'+party1+'" class="party1_drop">'+party1+' </option>'+
              '<option value="'+party2+'" class="party2_drop">'+party2+'  </option>'+
              '</select></td></tr>';
            }
            if(obj.respondent.length > 0 || !isEmpty(obj.respondent)){
             showdata +='<tr id="row1" style=""><td class="no-padding">'+
             '<table class="no-bordered"><tbody>';
             var counter =0;
             $.each(obj.respondent,function(index,object){
              counter++;
              showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
              showdata += '<input type="hidden" value="" name="cnr_no">';
              showdata += '<td>';
              showdata += '<div class="checkbox-zoom zoom-primary">';
              showdata += '<label>';
              showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+object+'"> ';
              showdata += '<span class="cr">';
              showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
              showdata += '</span>';
              showdata += '<span>'+counter+'</span>';
              showdata += ' </label>';
              showdata += '</div>'; 
              showdata += '</td>';
              showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
              showdata += '<textarea rows="2" class="form-control rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_res_name_'+index+'">'+object+'</textarea></td>';
              showdata += '<td data-th="Email Address"><label>Email Address:</label>';
              showdata += '<input type="email" class="form-control rowVal email" value="" id="sel_res_email_'+index+'" name="petitionerorRespondentNameemail" > </td>';
              showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
              showdata += '<input type="text" class="form-control rowVal phonenumber" value="" id="sel_res_mobile_'+index+'" name="petitionerorRespondentphone"></td></tr>';
            });
             showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
             '<td class="td_dd" style="">'+
             '<select id="sel_res" class="form-control party2_drop" style="" name="Petitionerrespomr">'+
              // '<option value="">Please select</option>'+
              '<option value="'+party2+'" class="party2_drop">'+party2+'  </option>'+
              '<option value="'+party1+'" class="party1_drop">'+party1+' </option>'+
              
              '</select></td></tr>';
            }
            if(obj.petAdvocate.length > 0 || !isEmpty(obj.petAdvocate)){
             var counter =0;
             showdata += '<tr id="row1" style="">';
             showdata += '<td style="border-right: 1px solid #ccc;">';
             showdata += '<table class="no-bordered"><tbody>';
             $.each(obj.petAdvocate,function(index,object){
              counter++;
              showdata += '<tr class="row-inner">';
              showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
              showdata += '<td>';
              showdata += '<div class="checkbox-zoom zoom-primary">';
              showdata += '<label>';
              showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+object+'"> ';
              showdata += '<span class="cr">';
              showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
              showdata += '</span>';
              showdata += '<span>'+counter+'</span>';
              showdata += ' </label>';
              showdata += '</div>'; 

              showdata+='</td>';
              showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
              showdata += '<textarea rows="2" class="form-control rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_petadv_name_'+index+'">'+object+'</textarea></td>';
              showdata += '<td data-th="Email Address"><label>Email Address:</label>';
              showdata +='<input type="email" class="form-control rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+index+'" > </td>';
              showdata +='<td data-th="Phone Number"><label>Phone Number:</label>';
              showdata +='<input type="text" class="form-control rowVal phonenumber" value="" id="sel_petadv_mobile_'+index+'" name="petitionerorRespondentphone"></td></tr>';
            });
             showdata +='</tbody></table></td>';
             showdata +='<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
             showdata +='<select id="sel_petadv" class="form-control party1_drop" style="" name="Petitionerrespompa">';
              // showdata +='<option value="">Please select</option>'+
              showdata +='<option value="'+party1+'Adv" class="party1_drop">'+party1+' Advocate</option>'+
              '<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option>'+
              '</select></td></tr>';
            }
            if(obj.respAdvocate.length > 0 || !isEmpty(obj.respAdvocate)){
             showdata +='<tr id="row1" style="">'+
             '<td style="border-right: 1px solid #ccc;">'+
             '<table class="no-bordered"><tbody>';
             var counter = 0;
             $.each(obj.respAdvocate,function(index,object){
              counter++;
              showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
              showdata += '<input type="hidden" value="" name="cnr_no"><td>';
              showdata += '<div class="checkbox-zoom zoom-primary">';
              showdata += '<label>';
              showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+object+'"> ';
              showdata += '<span class="cr">';
              showdata += '<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
              showdata += '</span>';
              showdata += '<span>'+counter+'</span>';
              showdata += ' </label>';
              showdata += '</div>'; 
              showdata += '</td>';
              showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
              showdata += '<input type="text" class="form-control rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_resadv_name_'+index+'"></td>';
              showdata += '<td data-th="Email Address"><label>Email Address:</label>';
              showdata += '<input type="email" class="form-control rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_resadv_email_'+index+'" > </td>';
              showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
              showdata += '<input type="text" class="form-control rowVal phonenumber" value="" id="sel_resadv_mobile_'+index+'" name="petitionerorRespondentphone"></td></tr>';        
            });
             showdata += '</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
             showdata += '<select id="sel_resadv" class="form-control party2_drop" style="" name="Petitionerrespompa">';

              // showdata += '<option value="">Please select</option>'+
              showdata +='<option value="'+party2+'Adv" class="party2_drop">'+party2+' Advocate</option><option class="party1_drop" value="'+party1+'Adv">'+party1+' Advocate</option>'+
              '</select></td></tr>';
            }
            showdata +='</tbody></table><div class="submit_data col-sm-12" id="submit_data">'+
            '<input type="button" class="btn btn-info newsubmit btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
            '</div></form>';
            $("#modal-data").html(showdata);
            $("#data").modal('show');
            $(".fetchLoaderDiv").removeClass("fetchdataloader");
            $("#fetchData_outer").show();
          }
          else{
           $(".fetchLoaderDiv").removeClass("fetchdataloader");
           $("#fetchData_outer").show();
           toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});      
         }
       });
},
error:function(){
  $(".fetchLoaderDiv").removeClass("fetchdataloader");
  $("#fetchData_outer").show();
  toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});
}
});
}

function encryptData(data){
  var key = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef');
  var iv  = CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210');
//        var key = CryptoJS.enc.Hex.parse('5678943210fdecba5678943210fdecba');
//        var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');
var encrypted = CryptoJS.AES.encrypt((data), key, { iv: iv });
var encrypted_data = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
return encrypted_data;
}

function submit_caseapi(){
/*  var party1 = $("input[name='appearing_radio']:checked").val();
var party2 = $("input[name='appearing_radio']:not(:checked)").val();*/
var party1 = appearing_top_array[0];
var party2 = appearing_top_array[1];
var data=getcase;
var petnameArr=[];
var resnameArr=[];
var petadvnameArr=[];

var resadvnameArr=[];

var select_pet = $("#sel_pet").val();
$("input:checkbox[class='petcheck']:checked").each(function(i,obj){
  var petemail = isEmpty($("#sel_pet_email_"+i).val()) ? '' : $("#sel_pet_email_"+i).val();
  var petmobile = isEmpty($("#sel_pet_mobile_"+i).val()) ? '' : $("#sel_pet_mobile_"+i).val();
  var petname = isEmpty($("#sel_pet").val()) ? '' : $("#sel_pet").val();
  var full_pet = isEmpty($("#sel_pet_name_"+i).val()) ? '' : $("#sel_pet_name_"+i).val();
  if(select_pet == party1){

    var petVo = {
      petemail : petemail,
      petmobile : petmobile,
      petname : petname,
      full_pet : full_pet
    }
    petnameArr.push(petVo);
    petitioner_arr = petnameArr;
  }
  else{
    var petVo = {
      resemail : petemail,
      resmobile : petmobile,
      resname : petname,
      full_res : full_pet
    }
    resnameArr.push(petVo);
  }
});
var select_res = $("#sel_res").val();
$("input:checkbox[class='rescheck']:checked").each(function(i,obj){
  var resemail = isEmpty($("#sel_res_email_"+i).val()) ? '' : $("#sel_res_email_"+i).val();
  var resmobile = isEmpty($("#sel_res_mobile_"+i).val()) ? '' : $("#sel_res_mobile_"+i).val();
  var resname = isEmpty($("#sel_res").val()) ? '' : $("#sel_res").val();
  var full_res = isEmpty($("#sel_res_name_"+i).val()) ? '' : $("#sel_res_name_"+i).val();

  if(select_res == party1){
    var resVo = {
      petemail : resemail,
      petmobile : resmobile,
      petname : resname,
      full_pet : full_res
    }
    petnameArr.push(resVo);
    petitioner_arr = petnameArr;
  }
  else{
    var resVo = {
      resemail : resemail,
      resmobile : resmobile,
      resname : resname,
      full_res : full_res
    }
    resnameArr.push(resVo);
  }
    // resnameArr.push(resVo);
    console.log(resnameArr);
  });
var sel_petadv = $("#sel_petadv").val();
$("input:checkbox[class='petadvcheck']:checked").each(function(i,obj){
  var petadvemail = isEmpty($("#sel_petadv_email_"+i).val()) ? '' : $("#sel_petadv_email_"+i).val();
  var petadvmobile = isEmpty($("#sel_petadv_mobile_"+i).val()) ? '' : $("#sel_petadv_mobile_"+i).val();
  var petadvname = isEmpty($("#sel_petadv").val()) ? '' : $("#sel_petadv").val();
  var full_petadv = isEmpty($("#sel_petadv_name_"+i).val()) ? '' : $("#sel_petadv_name_"+i).val();

  if(sel_petadv == party1+'Adv'){
    var petadvVo = {
      petadvemail : petadvemail,
      petadvmobile : petadvmobile,
      petadvname : petadvname,
      full_petadv : full_petadv
    }
    petadvnameArr.push(petadvVo);
  }    
  else{
    var petadvVo = {
      resadvemail : petadvemail,
      resadvmobile : petadvmobile,
      resadvname : petadvname,
      full_resadv : full_petadv
    }
    resadvnameArr.push(petadvVo);
  }

    // petadvnameArr.push(petadvVo);
    // console.log(petadvnameArr);

  });
var sel_resadv = $("#sel_resadv").val();
$("input:checkbox[class='resadvcheck']:checked").each(function(i,obj){
  var resadvemail = isEmpty($("#sel_resadv_email_"+i).val()) ? '' : $("#sel_resadv_email_"+i).val();
  var resadvmobile = isEmpty($("#sel_resadv_mobile_"+i).val()) ? '' :  $("#sel_resadv_mobile_"+i).val();
  var resadvname = isEmpty($("#sel_resadv").val()) ? '' : $("#sel_resadv").val();
  var full_resadv = isEmpty($("#sel_resadv_name_"+i).val()) ? '' : $("#sel_resadv_name_"+i).val();

  if(sel_resadv == party1+'Adv'){
    var resadvVo = {
      petadvemail : resadvemail,
      petadvmobile : resadvmobile,
      petadvname : resadvname,
      full_petadv : full_resadv
    }
    petadvnameArr.push(resadvVo);
  }
  else{
    var resadvVo = {
      resadvemail : resadvemail,
      resadvmobile : resadvmobile,
      resadvname : resadvname,
      full_resadv : full_resadv
    }
    resadvnameArr.push(resadvVo);
  }
    // resadvnameArr.push(resadvVo);
  });

var casedetailVo = new Object();
casedetailVo.petnameArr = petnameArr;
casedetailVo.resnameArr = resnameArr;
casedetailVo.petadvnameArr = petadvnameArr;
casedetailVo.resadvnameArr = resadvnameArr;

var val = $("input[name='appearing_radio']:checked").val();
if(appearing_top_array[0] == val){
  var getdata = '<div class="card-body"><div class="container"><table id="repondant_table" class="table-bordered  table-bordered table order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+
  '<td>Action</td>'+'</tr></thead><tbody>';
  if(resnameArr.length > 0){
    $.each(resnameArr,function(i,obj){
      if(i==0){
        getdata+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
        '<td><input type="text" name="mail" class="form-control" id="opponent_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
        '<td></td></tr>';
      }
      else{
        getdata+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
        '<td><input type="text" name="mail" class="form-control" id="opponent_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_count+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      res_count++;
    }) ;
  }
  else{
    getdata+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value=""/></td>'+

    '<td><input type="text" name="mail" class="form-control" id="opponent_email'+res_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value=""/></td>'+
    '<td></td></tr>';
    res_count++;
  }
  getdata+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" id="petitioner_advocate_add" onclick="addRespondant()"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-11").html(getdata);



  var getpetadv = '<div class="card-body"><div class="container"><table id="petitioneradv_table" class="table table-bordered order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+

  '<td>Action</td>'+'</tr></thead><tbody>';

  if(petadvnameArr.length > 0){
    $.each(petadvnameArr,function(i,obj){
      if(i==0){
        getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+

        '<td><input type="text" name="mail" class="form-control" id="your_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
        '<td></td></tr>';
      }

      else{
        getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+

        '<td><input type="text" name="mail" class="form-control" id="your_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+petadv_count+',"p-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      petadv_count++;
    });
  }
  else {
    getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+petadv_count+'" value=""/></td>'+

    '<td><input type="text" name="mail" class="form-control" id="your_adv_email'+petadv_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+petadv_count+'" value=""/></td>'+
    '<td></td></tr>';
    petadv_count++;
  }

  getpetadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" onclick="addPetitionerAdv()"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-10").html(getpetadv);



  var getresadv = '<div class="card-body"><div class="container"><table id="respondant_adv_table" class="table table-bordered order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+
  '<td>Action</td>'+'</tr></thead><tbody>';
  if(resadvnameArr.length > 0){
    $.each(resadvnameArr,function(i,obj){
      if(i==0){
        getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+          
        '<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
        '<td></td></tr>';
      }else{
        getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+      
        '<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+resadv_count+',"res-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      resadv_count++;
    });
  }
  else{
    getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+resadv_count+'" value=""/></td>'+      
    '<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+resadv_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+resadv_count+'" value=""/></td>'+
    '<td></td></tr>';
    resadv_count++;
  }
  getresadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" onclick="respondantAdvAdd()"  id="respondant_advocate_add"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-12").html(getresadv);

  var getpet = '<div class="card-body"><div class="container"><table id="petitioner_table" class=" table-bordered table order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+

  '<td>Action</td>'+'</tr></thead><tbody>';
  if(petnameArr.length > 0){
    $.each(petnameArr,function(i,obj){
      if(i==0){
        getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

        '<td><input type="text" name="mail" class="form-control" id="your_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="your_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
        '<td></td></tr>';
      }

      else{
        getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

        '<td><input type="text" name="mail" class="form-control" id="your_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="your_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pet_count+',"pet-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      pet_count++;
    });
  }
  else {
    getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value=""/></td>'+

    '<td><input type="text" name="mail" class="form-control" id="your_email'+pet_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="your_mobile'+pet_count+'" value=""/></td>'+
    '<td></td></tr>';
    pet_count++;
  }
  getpet+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" onclick="addPetitioner()"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-13").html(getpet);

}
else{
  var getpet = '<div class="card-body"><div class="container"><table id="petitioner_table" class=" table-bordered table order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+
  '<td>Action</td>'+'</tr></thead><tbody>';
  if(petnameArr.length > 0){
    $.each(petnameArr,function(i,obj){
      if(i==0){
        getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

        '<td><input type="text" name="mail" class="form-control" id="opponent_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
        '<td></td></tr>';
      }

      else{
        getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

        '<td><input type="text" name="mail" class="form-control" id="opponent_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pet_count+',"pet-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      pet_count++;
    });
  }
  else {
    getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+pet_count+'" value=""/></td>'+

    '<td><input type="text" name="mail" class="form-control" id="opponent_email'+pet_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+pet_count+'" value=""/></td>'+
    '<td></td></tr>';
    pet_count++;
  }

  getpet+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" onclick="addPetitioner()"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-11").html(getpet);

  var getresadv = '<div class="card-body"><div class="container"><table id="respondant_adv_table" class=" table-bordered table order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+
  '<td>Action</td>'+'</tr></thead><tbody>';
  if(resadvnameArr.length > 0){
    $.each(resadvnameArr,function(i,obj){
      if(i==0){
        getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+          
        '<td><input type="text" name="mail" class="form-control" id="your_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
        '<td></td></tr>';
      }else{
        getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+      
        '<td><input type="text" name="mail" class="form-control" id="your_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+resadv_count+',"res-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      resadv_count++;
    });
  }
  else{
    getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+resadv_count+'" value=""/></td>'+      
    '<td><input type="text" name="mail" class="form-control" id="your_adv_email'+resadv_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+resadv_count+'" value=""/></td>'+
    '<td></td></tr>';
    resadv_count++;
  }
  getresadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" onclick="respondantAdvAdd()"  id="respondant_advocate_add"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-10").html(getresadv);



  var getpetadv = '<div class="card-body"><div class="container"><table id="petitioneradv_table" class=" table-bordered table order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+

  '<td>Action</td>'+'</tr></thead><tbody>';

  if(petadvnameArr.length > 0){
    $.each(petadvnameArr,function(i,obj){
      if(i==0){
        getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+
        '<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
        '<td></td></tr>';
      }

      else{
        getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+

        '<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+petadv_count+',"p-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      petadv_count++;
    });
  }
  else {
    getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+petadv_count+'" value=""/></td>'+

    '<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+petadv_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+petadv_count+'" value=""/></td>'+
    '<td></td></tr>';
    petadv_count++;
  }

  getpetadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" onclick="addPetitionerAdv()"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-12").html(getpetadv);



  var getresp = '<div class="card-body"><div class="container"><table id="repondant_table" class=" table-bordered table order-list" width="100"><thead><tr><th class="full-name">Full Name</th>'+
  '<td>Email</td>'+
  '<td>Contact</td>'+

  '<td>Action</td>'+'</tr></thead><tbody>';
  if(resnameArr.length > 0){
    $.each(resnameArr,function(i,obj){
      if(i==0){
        getresp+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
        '<td><input type="text" name="mail" class="form-control" id="your_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
        '<td></td></tr>';
      }
      else{
        getresp+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
        '<td><input type="text" name="mail" class="form-control" id="your_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
        '<td><input type="text" name="phone" class="form-control" id="your_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
        '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_count+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
      }
      res_count++;
    });
  }
  else {
    getresp+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+res_count+'" value=""/></td>'+

    '<td><input type="text" name="mail" class="form-control" id="your_email'+res_count+'" value=""/></td>'+
    '<td><input type="text" name="phone" class="form-control" id="your_mobile'+res_count+'" value=""/></td>'+
    '<td></td></tr>';
    petadv_count++;
  }

  getresp+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="btn btn-info btn-sm add-opponent add-more-button" onclick="addRespondant()"> आणखी जोडा / Add More </button></td></tr></tfoot></table></div></div>';
  $("#collapse-13").html(getresp);

}

$(".collapse").addClass("show");
$("#data").modal("hide");
  // casedetailVo.getcase = getcase;
  // $.ajax({
  //  url: 'submitcase.php',
  //  type:'POST',    
  //  data : 'casedetail='+JSON.stringify(casedetailVo),
  //  dataType: 'json',
  //  async:false,
  //  cache:false,
  //  success:function(data){
  //    console.log(data.caseid);
  //    // if(data.status == 'success'){
  //    //  // alert("Data Submitted Successfully");
  //    //  window.location.href='view_case.php?caseid='+ data.caseid;
  //    // }
  //  },
  //  error:function(e){
  //          // console.log(e);
  //        }
  //      });
}
var pa_counter = 0,res_counter = 0,res_adv_counter = 0,pet_counter = 0;
// $("#petitioner_advocate_add").on("click", function () {
  function addPetitionerAdv(){
    pa_counter = petadv_count;

    var val = $("input[name='appearing_radio']:checked").val(),cols='';
    if(appearing_top_array[0] == val){

      cols = '<tr id="p-adv-row-'+pa_counter+'"><td><input type="text" class="form-control" id="your_adv_name'+pa_counter+'" name="name' + pa_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="your_adv_email'+pa_counter+'" name="mail' + pa_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="your_adv_mobile'+pa_counter+'" name="phone' + pa_counter + '"/></td>';

      cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pa_counter+',"p-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
    }
    else{
      cols = '<tr id="p-adv-row-'+pa_counter+'"><td><input type="text" class="form-control" id="opponent_adv_name'+pa_counter+'" /></td>';
      cols += '<td><input type="text" class="form-control" id="opponent_adv_email'+pa_counter+'" /></td>';
      cols += '<td><input type="text" class="form-control" id="opponent_adv_mobile'+pa_counter+'" /></td>';

      cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pa_counter+',"p-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
    }
    pa_counter++;
    petadv_count = pa_counter;
    $("#petitioneradv_table tbody").append(cols);

  };
  function deleteRow(count,id){
    document.getElementById(id+"-"+count).outerHTML = ''

  }
/*$("#petitioner_table tbody").on("click", ".btn btn-danger btn-sm ibtnDel", function (event) {
  $(this).closest("tr").remove();      
  pa_counter -= 1
});*/

$("#commission").on('change',function(){
  var val = $(this).val();
  $.ajax({
    url: 'commission_state.php',
    type:'POST',    
    data : 'commission='+val,
    dataType: 'json',
    async:false,
    cache:false,
    success:function(data){
      var option = '<option value="">कृपया निवडा / Please select</option>';
      $.each(data,function(i,obj){
        option +='<option value='+obj.commision_id+'>'+obj.state_name+'</option>';
      });
      $("#commission_state").html(option);
      $("#state_commission_div").show();
    },
    error:function(e){
      toastr.error("","Error in fetching state",{timeout:5000});
    }
  });
});

function addPetitioner(){
  pet_counter = pet_count;
  var val = $("input[name='appearing_radio']:checked").val(),cols='';
  if(appearing_top_array[0] == val){
    cols += '<tr id="pet-row-'+pet_counter+'"><td><input type="text" class="form-control" id="your_name'+pet_counter+'" name="name' + pet_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="your_email'+pet_counter+'" name="mail' + pet_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="your_mobile'+pet_counter+'" name="phone' + pet_counter + '"/></td>';
    cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pet_counter+',"pet-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
  }
  else{
    cols += '<tr id="pet-row-'+pet_counter+'"><td><input type="text" class="form-control" id="opponent_name'+pet_counter+'" name="name' + pet_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="opponent_email'+pet_counter+'" name="mail' + pet_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="opponent_mobile'+pet_counter+'" name="phone' + pet_counter + '"/></td>';
    cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pet_counter+',"pet-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
  }
  pet_counter++;
  pet_count = pet_counter;
  $("#petitioner_table tbody").append(cols);
}

// $("#respondant_add").on("click", function () {
  function addRespondant(){
    res_counter = res_count ;

    var val = $("input[name='appearing_radio']:checked").val(),cols='';
    if(appearing_top_array[0] == val){
      cols += '<tr id="res-row-'+res_counter+'"><td><input type="text" class="form-control" id="opponent_name'+res_counter+'" name="name' + res_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="opponent_email'+res_counter+'" name="mail' + res_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="opponent_mobile'+res_counter+'" name="phone' + res_counter + '"/></td>';
      cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_counter+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
    }
    else{
      cols += '<tr id="res-row-'+res_counter+'"><td><input type="text" class="form-control" id="your_name'+res_counter+'" name="name' + res_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="your_email'+res_counter+'" name="mail' + res_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="your_mobile'+res_counter+'" name="phone' + res_counter + '"/></td>';
      cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_counter+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
    }
    res_counter++;
    res_count = res_counter;
    $("#repondant_table tbody").append(cols);

  };
/*$("#repondant_table").on("click", ".btn btn-danger btn-sm ibtnDel", function (event) {
  $(this).closest("tr").remove();      
  res_counter -= 1
});*/

// $("#respondant_advocate_add").on("click", function () {
  function respondantAdvAdd(){
    res_adv_counter = resadv_count;

  // var newRow = $("<tr>");
  var val = $("input[name='appearing_radio']:checked").val(),cols='';
  if(appearing_top_array[0] == val){
    cols += '<tr id="res-adv-row-'+res_adv_counter+'"><td><input type="text" class="form-control" id="opponent_adv_name'+res_adv_counter+'" name="name' + res_adv_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="opponent_adv_email'+res_adv_counter+'" name="mail' + res_adv_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="opponent_adv_mobile'+res_adv_counter+'" name="phone' + res_adv_counter + '"/></td>';
    cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_adv_counter+',"res-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
  }
  else{
    cols += '<tr id="res-adv-row-'+res_adv_counter+'"><td><input type="text" class="form-control" id="your_adv_name'+res_adv_counter+'" name="name' + res_adv_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="your_adv_email'+res_adv_counter+'" name="mail' + res_adv_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="your_adv_mobile'+res_adv_counter+'" name="phone' + res_adv_counter + '"/></td>';
    cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_adv_counter+',"res-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
  }
  res_adv_counter++;
  resadv_count = res_adv_counter;
  $("#respondant_adv_table tbody").append(cols);    
};

/*$("#respondant_adv_table").on("click", ".btn btn-danger btn-sm ibtnDel", function (event) {
  $(this).closest("tr").remove();      
  res_adv_counter -= 1
});*/

function replace(text){
  text = text.replace(/<br\s*\/?>/gi,' ');
  return text;
}

function getAnyCondition(){
  // $("#flip-square-loader").show();
  $("#fetch_result_table").html('');
  /*$("#flip-square-loader").show();*/

  $("#loader-div").show();
  $("#getSearch").hide();
  if($("#cases-court_id").val() == 5 || $("#cases-court_id").val() == 1 || $("#cases-court_id").val() == 2){
    $("#appeal-type-div").hide();
    $("#registration-year").hide();
    $("#anycondition").modal('show');
    $("#loader-div").hide();
    $("#getSearch").show();
  }
  else{
    var state_code = encryptData($("#state_code").val());
    var dist_code = encryptData($("#dist_code").val());
    var courtCode = encryptData($("#court_code").val());
    var courtType = $("#court_type").val();

    $("#appeal-type-div").show();
    $("#registration-year").show();
  /*var state_code = encryptData("2");
  var dist_code = encryptData("1");
  var courtCode = encryptData("1");
  var courtType = "HC";*/


  $.ajax({
    type: "GET",
    url: legalResearch+"/get_case_type",
    // data: "cino="+cino+"&&courtType=SC",
    data: {
      state_code : state_code,
      dist_code : dist_code,
      courtCode : courtCode,
      courtType : courtType
    },
    cache: false,
    async:false,
    contentType: 'application/json;utf-8',    
    beforeSend : function(){
      $("#loader-div").show();
      $("#getSearch").hide();
    },
    success: function(response){
      console.log(response);

      var option = '<option value=""> Select Appeal type</option>';
      $.each(response,function(i,obj){
        option +='<option value='+obj.key+'>'+obj.value+'</option>';
      });
      $("#appeal_type").html(option);
      $("#appeal_type").select2();
      $("#loader-div").hide();
      $("#getSearch").show();
      $("#anycondition").modal('show');
      // $("#flip-square-loader").hide();
    },
    complete:function(){

    },
    error:function(){
     $("#flip-square-loader").hide();
   }
 });
}
/*$("#flip-square-loader").hide();*/
}
/*function isEmpty(val) {
  return (val === undefined || val == null || val == "null" || val.length <= 0) ? true : false;
}*/

function fetchDatafromApi(){
  // $("#loader-div").show();
  var state_code = encryptData($("#state_code").val());
  var dist_code = encryptData($("#dist_code").val());
  var courtCode = encryptData($("#court_code").val());
  var courtType = $("#court_type").val();
  var dairyNo = $("#diary_number").val();
  var searchType = 'PARTY_SEARCH';

  var appeal_type = $("#appeal_type").val();
  var appeal_no = $("#appealno").val();
  var appeal_year = $("#appeal_year").val();
  var party_name = $("#party_name_search").val();
  var party_year = $("#party_year").val();

  var caseTypeVal = encryptData(appeal_type);
  var caseNumber = encryptData(appeal_no);
  var year = encryptData(appeal_year);
  var party_name_search = encryptData(party_name);
  var pendingDisposed = encryptData("Both");
  var party_year = encryptData(party_year);

  if(isEmpty(appeal_type) && isEmpty(appeal_no) && isEmpty(appeal_year) && isEmpty(party_name)){
    toastr.info("","Select party name or appeal number",{timeout:5000});
    return false;
  }
  if(!isEmpty(appeal_no)){
    if($("#cases-court_id").val() == 5){
      var caseNumber = isEmpty($("#appealno").val()) ? "0" : $("#appealno").val();
      var year = isEmpty($("#appeal_year").val()) ? "0" : $("#appeal_year").val();
      var party_name_search = $("#party_name_search").val();
      var data = {
        party : party_name_search,
        case_number : caseNumber,
        case_year : year
      }
      var url = legalResearch+'/mat_search_by_party';
    }
    else if($("#cases-court_id").val() == 1){
      var data = {
        caseNo : appeal_no,
        caseType : appeal_type,
        caseYear : appeal_year,
        party : party_name,
        dairyNo : dairyNo,
        /*diaryYear : diaryYear,*/
        searchType : searchType
      }
      var url = legalResearch+'/search_by_sc';
    }
    else if($("#cases-court_id").val() == 2){
      var data = {
        party : party_name,
        side : '',
        bench : '',
        stamp : '',
        cType : '',
        cYear : appeal_year,
        cNo : appeal_no,
        sType : 'SEARCH',
        id : ""
      }
      var url = legalResearch+'/search_by_bom';
    }
    else{
      var data = {
        state_code : state_code,
        dist_code : dist_code,
        court_code : courtCode,
        courtType : courtType,
        caseTypeVal : caseTypeVal,
        caseNumber : caseNumber,
        year : year
      }
      var url = legalResearch+'/search_by_casenumber';
    }
  }

  else{
    if($("#cases-court_id").val() == 5){
      var caseNumber = isEmpty($("#appealno").val()) ? "0" : $("#appealno").val();
      var year = isEmpty($("#appeal_year").val()) ? "0" : $("#appeal_year").val();
      var party_name_search = $("#party_name_search").val();
      var data = {
        party : party_name_search,
        case_number : caseNumber,
        case_year : year
      }
      var url = legalResearch+'/mat_search_by_party';
    }
    else if($("#cases-court_id").val() == 1){
      var data = {
        caseNo : appeal_no,
        caseType : appeal_type,
        caseYear : appeal_year,
        party : party_name,
        dairyNo : dairyNo,
        // diaryYear: diaryYear,
        searchType : searchType
      }
      var url = legalResearch+'/search_by_sc';
    }
    else if($("#cases-court_id").val() == 2){
     var data = {
      party : party_name,
      side : '',
      bench : '',
      stamp : '',
      cType : '',
      cYear : appeal_year,
      cNo : appeal_no,
      sType : 'PARTY',
      id : ""
    }
    var url = legalResearch+'/search_by_bom';
  }
  else{
   var data = {
    state_code : state_code,
    dist_code : dist_code,
    court_code : courtCode,
    courtType : courtType,
    pendingDisposed : pendingDisposed,
    pet_name : party_name_search,
    year : party_year
  }
  var url = legalResearch+'/party_search';
}
}
$("#fetch_data_button").text('Loading...');
$.ajax({
  type: "GET",
  url: url,
  data: data,
  cache: false,
  async:false,
  headers: {
    "Content-Type": 'application/json'
  },
    // contentType: 'application/json;utf-8',  
    beforeSend : function(){
      $(".loader-div").show();
    },
    success: function(response){
      document.getElementById("fetch_case_modal").reset();
      console.log(response);
      var tr_party = '';
      if($("#cases-court_id").val() == 5){
        tribunal_search = response;
        $.each(response,function(i,obj){
          tr_party +='<tr>';
          tr_party +='<td><a href="javascript:void(0);" class="cnr_no_td" id="'+obj.appleantName+'_'+obj.dep+'" onclick=getCaseDetails(this.id)>'+obj.caseNo+'</a></td>';
          tr_party +='<td>'+obj.appleantName+'</td>';
          tr_party +='<td>'+obj.dep+'</td>';
          tr_party +='<td>'+obj.hearingDate+'</td>';        
          tr_party +='<td>'+obj.subject+'</td>';
          tr_party +='<td>'+obj.designationAndAddress+'</td></tr>';
        });
        if(tr_party !=''){
          var party_table = '<table class="table table-bordered table-xs" id="party_search_table"><thead>';
          party_table+= '<tr><th class="tribunal_caseno">Case No.</th>';
          party_table+= '<th class="tribunal_appleant">Appleant</th>';
          party_table+= '<th class="tribunal_respondent">Respondent</th>';        
          party_table+= '<th class="reg_th">Hearing Date</th>';
          party_table+= '<th class="reg_th">Subject</th>';
          party_table+= '<th class="reg_th">Destination and Address</th>';
          party_table+= '</tr>';
          party_table+= '</thead>';
          party_table+= '<tbody>';
          party_table+= tr_party;
          party_table+= '</body>';
          party_table+= '</table>';
        }
      }
      else if($("#cases-court_id").val() == 2){
        var highcourt_response = JSON.stringify(response);
        $.each(response,function(i,obj){
          tr_party +='<tr>';
          tr_party +="<td><a href='javascript:void(0);' class='cnr_no_td' onclick=getBomDetails("+obj.id+")>"+obj.stampNo+"</a></td>";
          tr_party +='<td>'+obj.regNo+'</td>';
          tr_party +='<td>'+obj.petioner.join("/")+'</td>';
          tr_party +='<td>'+obj.respondent.join("/")+'</td></tr>';
        });
        if(tr_party !=''){
          var party_table = '<table class="table table-bordered table-xs" id="party_search_table"><thead>';
          party_table+= '<tr><th class="stamp_no_th">Stamp No.</th>';
          party_table+= '<th class="reg_no_tr">Reg No</th>';
          // party_table+= '<th class="party_name_th">Party Name</th>';
          party_table+= '<th class="reg_th">Petitioner</th>';
          party_table+= '<th class="reg_th">Respondent</th>';
          party_table+= '</tr>';
          party_table+= '</thead>';
          party_table+= '<tbody>';
          party_table+= tr_party;
          party_table+= '</body>';
          party_table+= '</table>';
        }
      }
      else if($("#cases-court_id").val() == 1){
       var highcourt_response = JSON.stringify(response);
       $.each(response,function(i,obj){
        tr_party +='<tr>';
        tr_party +="<td><a href='javascript:void(0);' class='cnr_no_td' id='"+obj.diaryNo+"_"+obj.caseNo+"' onclick=getSCDetails(this.id)>"+obj.diaryNo+"</a></td>";
        tr_party +='<td>'+obj.caseNo+'</td>';
        tr_party +='<td>'+obj.petitioner[0]+'</td>';
        tr_party +='<td>'+obj.respondent[0]+'</td></tr>';
      });
       if(tr_party !=''){
        var party_table = '<table class="table table-bordered table-xs" id="party_search_table"><thead>';
        party_table+= '<tr><th class="stamp_no_th">Diary No.</th>';
        party_table+= '<th class="reg_no_tr">Case No</th>';
          // party_table+= '<th class="party_name_th">Party Name</th>';
          party_table+= '<th class="reg_th">Petitioner</th>';
          party_table+= '<th class="reg_th">Respondent</th>';
          party_table+= '</tr>';
          party_table+= '</thead>';
          party_table+= '<tbody>';
          party_table+= tr_party;
          party_table+= '</body>';
          party_table+= '</table>';
        }
      }
      else{
       $.each(response.caseNos,function(i,obj){
        tr_party +='<tr>';
        tr_party +='<td><a href="javascript:void(0);" class="cnr_no_td" id='+obj.cino+' onclick=getCNRDetails("'+obj.cino+'")>'+obj.cino+'</a></td>';
        tr_party +='<td>'+obj.type_name+'/'+obj.case_no2+'/'+obj.reg_year+'</td>';
        tr_party +='<td>'+replace(obj.petnameadArr)+'</td>';
        tr_party +='<td>'+obj.reg_year+'</td></tr>';
      });
       if(tr_party !=''){
        var party_table = '<table class="table table-bordered table-xs" id="party_search_table"><thead>';
        party_table+= '<tr><th>CNR No.</th>';
        party_table+= '<th>Case Type</th>';
        party_table+= '<th class="party_name_th">Party Name</th>';
        party_table+= '<th class="reg_th">Registration Year</th>';
        party_table+= '</tr>';
        party_table+= '</thead>';
        party_table+= '<tbody>';
        party_table+= tr_party;
        party_table+= '</body>';
        party_table+= '</table>';
      }
    }
    $("#fetch_result_table").html(party_table);
    $("#party_search_table").dataTable();
    $(".loader-div").hide();
    $("#fetch_data_button").text('Fetch data');
  },
  complete:function(){
   $(".loader-div").hide();
 },
 error:function(){
   $(".loader-div").hide();
 }
});
}

function getSCDetails(response){
  $("#anycondition").modal('hide');
  var diaryno_caseno = response.split("_");
  var diaryno = diaryno_caseno[0].split("/");
  $("#diary_number").val(diaryno[0]);
  var diary_yr = diaryno[1].trim();
  $("#diary_year").val(diary_yr);
  $('#diary_year').select2().trigger('change');

  var caseno = diaryno_caseno[0].split("/");
  var casetype = caseno[0];
  var case_no = casetype[1];
  datafetch();

}

function getBomDetails(response){
  var stamp = $("#stamp_id").val(response);
  datafetch();

}

function getCaseDetails(appresp){
  var appleant_respondant = appresp.split("_");
  tribunal_appleant = appleant_respondant[0];
  tribunal_respondent = appleant_respondant[1];
  $("#anycondition").modal('hide');
  datafetch();
}

function getCNRDetails(cnrno){

  $("#name").val(cnrno);
  $("#anycondition").modal('hide');
  datafetch();

}

