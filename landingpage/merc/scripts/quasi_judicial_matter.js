$(document).ready(function(){	
	$("#hearing_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeYear: true,
		changeMonth: true,
		minDate : '+0d'
	});

	$("#filing_date").datepicker({
		dateFormat: 'dd-mm-yy',
		changeYear: true,
		changeMonth: true,
		minDate : '+0d'
	});

	$("#other_competant_authority").hide();
	$("#other_district").hide();
	$("#legal-help-div").hide();

	tinymce.init({
		selector : "#legal-help",
		plugins : [
		"wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
    });

	getCompetantAuthority();
	// getDeskNo();
	getMinistry();
	$("#division").select2();
	$("#district").select2();

	$('#division').on('change', function(){
		var division = $(this).val();

		if(division){
			$.ajax({
				type:'POST',
				url:'quasi_district_list.php',
				data:'division_id='+division,
				success:function(html){
					$('#district').html(html);
				}
			}); 
		}
	}); 
});

var legalHelp = false;
function getMinistry(){
	$.ajax({
		type:'POST',
		url:host+'/ajaxdata/judicial_matter.php',
		dataType:"json",
		async:false,
		success:function(response){
			console.log("judicial_matter",response);
			var option = '<option value="">Select</option>';
			$.each(response,function(i,obj){
				option +='<option value="'+obj.id+'">'+obj.ministry_name+'</option>';
			});
			$("#ministry_name").html(option);
			$("#ministry_name").select2();
		},
		error:function(){
			toastr.error("","Error in fetching competant authority",{timeout:5000});
		}
	});
}
/*function getDeskNo(){
	$.ajax({
		type:'POST',
		url:host+'/ajaxdata/competant_authority.php',
		dataType:"json",
		success:function(response){
			console.log(response);
			var option = '<option value="">Select</option>';
			$.each(response,function(i,obj){
				option +='<option value="'+obj.comps_auto_id+'">'+obj.competant_authority+'</option>';
			});
			$("#zp_name").html(option);
			$("#zp_name").select2();
		},
		error:function(){
			toastr.error("","Error in fetching competant authority",{timeout:5000});
		}
	});
}*/

function getCompetantAuthority(){	
	$.ajax({
		type:'POST',
		url:host+'/ajaxdata/competant_authority.php',
		dataType:"json",
		async:false,
		success:function(response){
			console.log(response);
			var option = '<option value="">Select</option>';
			$.each(response,function(i,obj){
				option +='<option value="'+obj.comps_auto_id+'">'+obj.competant_authority+'</option>';
			});
			$("#competant_authority").html(option);
			$("#competant_authority").select2();
		},
		error:function(){
			toastr.error("","Error in fetching competant authority",{timeout:5000});
		}
	});
}

var order_counter = 0,doc_counter=0;
function addOrderJudgement(){
	order_counter++;
	var order = '<div id="order_new_div_'+order_counter+'" class="form-group">';
	order+='<input type="file" name="order_judgment" id="order_judgement_'+order_counter+'">';
	order +='<button class="btn btn-danger btn-sm pull-right" type="button" onclick="deleteOrderJudgement('+order_counter+')">Delete</button>';
	order +='</div>';
	$(".mulitple_orders").append(order);
}

function deleteOrderJudgement(id){
	document.getElementById('order_new_div_'+id).outerHTML = '';
}

function addDocuments(){
	doc_counter++;
	var order = '<div id="document_new_div_'+doc_counter+'" class="form-group">';
	order+='<input type="file" name="document" id="document_'+doc_counter+'">';
	order +='<button class="btn btn-danger btn-sm pull-right" type="button" onclick="deleteDocument('+doc_counter+')">Delete</button>';
	order +='</div>';
	$(".mulitple_docs").append(order);
}

function deleteDocument(id){
	document.getElementById('document_new_div_'+id).outerHTML = '';
}

saveAddress('pet');
function saveAddress(party){
	if(party == 'pet'){
		$("#party-mobile").text('Petitioner');
		$("#party-email").text('Petitioner');
		$("#party-address").text('Petitioner');
	}
	else{
		$("#party-mobile").text('Respondent');
		$("#party-email").text('Respondent');
		$("#party-address").text('Respondent');
	}
}

$("#competant_authority").on('change',function(){
	var val = $(this).val();
	if(val == '0')
		$("#other_competant_authority").show();
	else
		$("#other_competant_authority").hide();
});
$("#district").on('change',function(){
	var val = $(this).val();
	if(val == '0')
		$("#other_district").show();
	else
		$("#other_district").hide();
});

function seekLegalHelp(){
	legalHelp = true;
	$("#legal-help-div").show();
}

function submitQuasi(){
	var formData = new FormData();
	var	competant_authority_other = '';
	if($("#competant_authority").val() == '0')
		competant_authority_other = $("#other_competant_authority").val()!=''?$("#other_competant_authority").val():'';
	else if($("#competant_authority").val() == ''){
		toastr.error("","Please select competant authority",{timeout:5000});
		return false;
	}
	else
		var competant_authority = $("#competant_authority option:selected").text();
	if($("#division").val() == ""){
		toastr.error("","Please Select Division",{timeout:5000});
		return false;
	}
	else
		var division = $("#division option:selected").text();
	if($("#district").val() == ""){
		toastr.error("","Please Select Zilla Parishad",{timeout:5000});
		return false;
	}
	else
		var district = $("#district option:selected").text();
	if($("#ministry_name").val() != '')
		var ministry = $("#ministry_name option:selected").text();
	else{
		toastr.error("","Please select ministry",{timeout:5000});
		return false;
	}
	if($("#zp_name").val() != '')
		var zp_name = $("#zp_name").val();
	else{
		toastr.error("","Please provide Desk Number",{timeout:5000});
		return false;
	}
	var commisioner_level = $("input[name='commissioner_level']:checked").val();

	if($("#department").val() != '')
		var department = $("#department").val();
	else{
		toastr.error("","Please provide department",{timeout:5000});
		return false;
	}
	if($("#subject").val() !='')
		var subject = $("#subject").val();
	else{
		toastr.error("","Please provide Subject",{timeout:5000});
		return false
	}

	if($("#filing_date").val() !='')
		var filing_date = $("#filing_date").val();
	else{
		toastr.error("","Please provide filling date",{timeout:5000});
		return false;
	}
	if($("#case_type").val() !='')
		var case_type = $("#case_type").val();
	else{
		toastr.error("","Please provide case type",{timeout:5000});
		return false;
	}
	if($("#case_no").val() !='')
		var case_no = $("#case_no").val();
	else{
		toastr.error("","Please provide case no",{timeout:5000});
		return false;
	}
	if($("#case_year").val() !='')
		var case_year = $("#case_year").val();
	else{
		toastr.error("","Please provide case year",{timeout:5000});
		return false;
	}
	
	if($("#case_title").val() !='')
		var case_title = $("#case_title").val();
	else{
		toastr.error("","Please provide case title",{timout:5000});
		return false;
	}
	if($("#stage").val() !='')
		var stage = $("#stage").val();
	else{
		toastr.error("","Please provide stage",{timeout:5000});
		return false;
	}
	if($("#hearing_date").val() !='')
		var hearing_date = $("#hearing_date").val();
	else{
		toastr.error("","Please provide hearing date",{timeout:5000});
		return false;
	}
	if($("#next-stage").val()!='')
		var nextstage = $("#next-stage").val();
	else{
		toastr.error("","Please provide next stage",{timeout:5000});
		return false;
	}

	// var peti_respondant = $("input[name='peti_respondant']:checked").val();
	if($("#pet_mobile").val() !='')
		var pet_mobile = $("#pet_mobile").val();
	else{
		toastr.error("","Please provide petitioner mobile",{timeout:5000});
		return false;
	}
	if($("#pet_email").val() !='')
		var pet_email = $("#pet_email").val();
	else{
		toastr.error("","Please provide petitioner email",{timeout:5000});
		return false;
	}
	if($("#pet_address").val() !='')
		var pet_address = $("#pet_address").val();
	else{
		toastr.error("","Please provide petitioner address",{timeout:5000});
		return false;
	}
	if($("#resp_mobile").val() !='')
		var resp_mobile = $("#resp_mobile").val();
	else{
		toastr.error("","Please provide respondent mobile",{timeout:5000});
		return false;
	}
	if($("#resp_name").val() !='')
		var resp_name = $("#resp_name").val();
	else{
		toastr.error("","Please provide respondent email",{timeout:5000});
		return false;
	}
	if($("#pet_name").val() !='')
		var pet_name = $("#pet_name").val();
	else{
		toastr.error("","Please provide respondent email",{timeout:5000});
		return false;
	}
	if($("#resp_email").val() !='')
		var resp_email = $("#resp_email").val();
	else{
		toastr.error("","Please provide respondent email",{timeout:5000});
		return false;
	}
	if($("#resp_address").val() !='')
		var resp_address = $("#resp_address").val();
	else{
		toastr.error("","Please provide respondent address",{timeout:5000});
		return false;
	}
	// var legal_help = tinymce.get('legal-help').getContent({ format: 'text' });
	if(legalHelp == true)
		var emp_no = $("#emp_no").val();
	else if(legalHelp == true && emp_no==''){
		toastr.error("","Please provide employee number",{timeout:5000});
		return false;
	}
	
	// var orderArr = [],documentArr=[];
	for(var ord=0;ord<=order_counter;ord++){
		// for(var i=0;i<=doc_counter;i++){
			$.each($("input[name='order_judgment']")[ord].files, function(i, file) {
		// orderArr.push();
		formData.append("order_"+ord,file);
	});
		}
		for(var doc=0;doc<=doc_counter;doc++){
			if($("input[name='document']")[doc].files !=''){
				$.each($("input[name='document']")[doc].files, function(i, file) {
			// console.log("documents",file);
			formData.append('document_'+doc, file);
		});
			}
			else{
				toastr.error("","Please attach document",{timeout:5000});
				return false;
			}
		}

	/*for(var i=0;i<=doc_counter;i++){
		// documentArr.push();
		formData.append("document",document.getElementById("document_"+i).files[i]);
	}*/
	
	
	// var quasiVo = new Object();
	formData.append("competant_authority",competant_authority);
	formData.append("competant_authority_other", competant_authority_other);
	formData.append("division",division);
	formData.append("district",district);
	formData.append("ministry",ministry);
	formData.append("zp_name",zp_name);
	formData.append("department",department);
	formData.append("subject",subject);
	formData.append("filing_date",filing_date);
	formData.append("case_no",case_no);
	formData.append("case_type",case_type);
	formData.append("case_year",case_year);
	formData.append("case_title",case_title);
	formData.append("stage",stage);
	formData.append("hearing_date",hearing_date);
	formData.append("nextstage", nextstage);
	// formData.append("peti_respondant",peti_respondant);
	formData.append("pet_mobile", pet_mobile);
	formData.append("pet_email", pet_email);
	formData.append("pet_address",pet_address);
	formData.append("resp_mobile", resp_mobile);
	formData.append("resp_email", resp_email);
	formData.append("resp_name", resp_name);
	formData.append("pet_name", pet_name);
	formData.append("resp_address",resp_address);
	formData.append("Order_counter",order_counter);
	formData.append("Document_counter",doc_counter);
	formData.append("div_comm_level",commisioner_level);
	// formData.append("legal_help",legal_help);
	formData.append("emp_no",emp_no);
	// formData.append(formData = formData;
	var ajaxResult = $.ajax({
		url : host+"/judical_submit.php",
		type : 'POST',
		data : formData,
		cache : false,
		contentType : false,
		processData : false,
		success:function(response){
			var response = JSON.parse(response);
			if(response.status == 'success' && legalHelp == true)
				sendClrQuery();
			else if(response.status == 'success' && legalHelp == false)
				window.location.href="cases-report";
		},
		error:function(){
			toastr.error("","Error submitting form",{timeout:5000});
		}
	});
	
}

function sendClrQuery(){
	var user= $("#email").val();
	var firstname= $("#fname").val();
	var lastname= $("#lname").val();
	var contact= $("#mobile").val();
	var query = tinymce.get('legal-help').getContent();
	var QueryVo = new Object();
	QueryVo.user=user;
	QueryVo.firstname=firstname;
	QueryVo.lastname=lastname;
	QueryVo.contact=contact;	
	QueryVo.query=query;

	$.ajax({
		url: legalResearch+"/restquery",
		type:'POST',
		data: JSON.stringify(QueryVo),
		dataType: 'json',
		async:false,
		headers: {
			"Content-Type": 'application/json'
		},
		success:function(data){            
			console.log(data);
			if(data.error=='SUCCESS'){
				toastr.success('Query Sent Successfully', {timeOut: 5000}) ;
				// window.location.href="cases-report";
			}       
		},

		complete: function(){ 
			$('#flip-square-loader').hide();

		},
		error:function(e){
			toastr.error('Unable to Send Query', 'Sorry For Inconvenience!', {timeOut: 5000})
              //console.log(e);
          }
      });
}