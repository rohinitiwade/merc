
$( document ).ready(function() {
    filterCase(1,'all');
    $("#uploaded_date").datepicker({ dateFormat: 'yy-mm-dd' });
    $("#court_forum").select2();
    // $("#court_name").select2();

     $("#case_no").keyup(function(){ 
    $(".party-name-div").hide();
    $.ajax({
      type: "POST",
      url: "search_casedata.php",
      data:'keyword='+$(this).val()+'&flag=search',
      beforeSend: function(){
        $("#case_no").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
      },
      success: function(response){
        var res_data = JSON.parse(response);
        var dataShow = '<ul id="case-list" style="width: 268px;">';
        // dataShow += '<a style="margin-bottom: -10px;background: #2d4866;width: 100%;float: left;text-decoration: underline;font-weight: bold;font-size: 16px;">Did You mean</a>';
        $.each(res_data.data,function(key,val){
        //   if(val.flag == 'party')
        //   dataShow += '<li id="'+val.full_name+'" onclick="partNameWise(this.id)">'+val.full_name+'</li>';
        // else
        dataShow += ' <li onClick="selectCase('+val.case_no+'); " style="font-size: 13px;cursor:pointer;">'+val.court_name+' - '+val.case_no+' / '+val.case_no_year+'</li> ';
       // dataShow += '<li style="font-size: 13px;" id="'+val.full_name+'" onclick=partNameWise("'+val.full_name+'")>'+val.full_name+'</li>';
     });

        dataShow += '</ul>';

        $(".managecase-box").show();
        $(".managecase-box").html(dataShow);
        $("#case_no").css("background","#FFF");
      }
    });
  });
});

function selectCase(val) {
  $("#case_no").val(val);
  $("#managecase-box").hide();
}
$("#myBtn-filter").on('click',function(){
  $("#showfilter").show();
});

function closeFilter(){
  $("#showfilter").hide();
}
function download_doc(){
	var downloadtype = $("input[name='downloadas']:checked").val();
	$.ajax({
		type:'GET',
		url:'ajaxdata/export_document.php',
		dataType:"json",
		success:function(response){
			// console.log(response);
			var table = '<table class="table table-bordered main-table" id="main-table"><thead>';
			table +='<tr>';
			table += '<th>Sr No.</th>';
			table += '<th>Title</th>';
			table += '<th style="width:10%;">Size</th>';
			table += '<th>Cases</th>';
			table += '<th>Uploaded By</th>';
			table += '<th>Type</th></tr></thead><tbody>';
			$.each(response.document_array,function(i,obj){
				i++;
				table += '<tr>';
				table += '<td>'+i+'</td>';
				table += '<td>'+obj.document+'</td>';
				table += '<td>'+getFileSize(obj.size)+'</td>';
				if(!isEmpty(obj.cases))
					table += '<td>'+obj.cases+'</td>';
				else
					table +='<td></td>';
				table += '<td>'+obj.uploaded_by+'</td>';				
				table += '<td>'+obj.type+'</td></tr>';
			});
			table +='</tbody></table>';
			$("#document-table-div-export").html(table);

// var pdfTitle = $("#case_type_export_report").text();
if(!isEmpty(response.document_array))
	exportPdf(downloadtype,'Document');
else{
	$("#export").modal('hide');
	toastr.warning("","No record found to export",{timeout:5000});
}
},
error:function(e){

}
});
}

function getFileSize(bytes){
	var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Byte';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

$(document).ready(function(){
	$("#search_document").on('change',function(){
		var val = $(this).val();
		$(".seacrhoption input").val('');
		
		if(isEmpty(val)){
			$(".seacrhoption").hide();
			$(".seacrhoption input").val('');
		}
		else{
			$(".seacrhoption").not("."+val).hide();
			$(".seacrhoption."+val).show();
		}
	});
})

$(document).ready(function(){


  // add multiple select / deselect functionality

$(".selectall").change(function() { //"select all" change 
      $(".check").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change 
    $('.check').change(function() {
      //uncheck "select all", if one of the listed checkbox item is unchecked
      if (false == $(this).prop("checked")) { //if this item is unchecked
        $(".selectall").prop('checked', false); //change "select all" checked status to false
      }
      //check "select all" if all checkbox items are checked
      if ($('.check:checked').length == $('.check').length) {
        $(".selectall").prop('checked', true);
      }
    });
  });

  function deletecaseMultiple(){
  // debugger;
  var checkArr = [];
  if($('.check:checked').length <= 0)
  {
    toastr.warning("",'Please select atleast one case',{timeout:5000});
    return false;
  }else{
    var yes = confirm('Are You Sure Want To Delete The Documents?');
    if(yes){
    $.each($("input[name='select_case_single']:checked"),function(){
      checkArr.push($(this).val());
    });
    var deleteVo = new Object();
    deleteVo.flag= 'delete';
    deleteVo.doc_id = checkArr
    $.ajax({
      type:'POST',
      url:'delete_alldoc.php',
      data:JSON.stringify(deleteVo),
      dataType: 'json',
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
    // alert("Case Data Removed Successfully");
    if(html.status == 'success')
      toastr.success('', 'Documents Removed Successfully', {timeOut: 5000})
    window.location.href='manage-document.php';
    // window.location.href='cases-report.php';
       // $('#delcase').html(html);
     },
     complete: function(){
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Documents', {timeOut: 5000})
    }
  }); 
  }
  else{
    return false;
  }
}

}

function filterCase(currentpage,flag){
  // debugger;

  var case_no = $("#case_no").val();
  var uploaded_date = $("#uploaded_date").val();
  var court_name = $("#court_name").val();
   var limit = '10';
  var offsets =   offset(currentpage,limit);
  // var date   =uploaded_date.toString('yy-MM-dd'); 
    // yr      = date.getFullYear(),
    // month   = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
    // day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate(),
    // newDate = yr + '-' + month + '-' + day;
// console.log(date);
  $.ajax({
      type:'GET',
      url:'ajaxdata/filterCase.php',
      data:"ad.case_no="+case_no+"&ad.date_time="+uploaded_date+"&rc.court_name="+court_name+'&limit='+limit+"&offset="+offsets+"&flag="+flag,
      dataType: 'json',
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
    // console.log(html);
    var getdata = '';
     // $.each(data,function(key,val){
       $("#showfilter").hide();
     getdata += '<input type="hidden" id="countn" value='+html.total_count+'> <table id="order-listing" class="table table-bordered">';
                  getdata += ' <thead class="bg-primary">';
                   getdata += ' <tr> <th>Sr.No. </th> <th style="width:129px;">Type of attachment</th> <th style="width:349px;">Title</th><th>Case No/ Year</th> <th>Document Type</th> <th>Uploded Date Time</th> <th>Actions</th></tr>';
     getdata += '       </thead> <tbody>';
     var i=1;
     $.each(html.data,function(is,get){
       getdata += '<tr> <td>'+i+'</td>';
        getdata += '  <td style="text-align:center;">';
        if(get.img_extension=='pdf') 
               getdata += '<img src="images/pdf.png" width="30" title='+get.img_name+'> ';
               else if(get.img_extension=='docx')   
               getdata += '<img src="images/word.png" width="30" title='+get.img_name+'>';  
              else if(get.img_extension=='xlsx')  
                 getdata += '<img src="images/excel.png" width="30" title='+get.img_name+'>'; 
               else if(get.img_extension=='csv')  
                  getdata += '<img src="images/csv.png" width="30" title='+get.img_name+'>'; 
                else if(get.img_extension=='png')   
                   getdata += '<img src="images/png.png" width="30" title='+get.img_name+'>';
                 else if(get.img_extension=='jpg' || get.img_extension=='jpeg')  
                     getdata += '<img src="images/jpg.png" width="30" title='+get.img_name+'>'; 
                  else if(get.img_extension=='doc' || get.img_extension=='docx')  
                     getdata += '<img src="images/docimg.png" width="30" title='+get.img_name+'>';  
        getdata += '</td>';
         getdata += '  <td>'+get.case_title+'</td>';
          getdata += '  <td>'+get.case+'</td>';
           getdata += '  <td>'+get.document_type+'</td>';
            getdata += ' <td>'+get.uploaded_date+'</td>';
             getdata += '  <td>';
            if(get.user_id == get.session_id) 
              getdata += '<a href="editMercDocument.php?id='+get.enc_doc_id+'"><i class="feather icon-edit" title="Update" style="color: orange;"></i></a>';
              else if(get.user_id != get.session_id) 
                getdata += ' <a><i class="feather icon-edit noaccess" onclick="return confirm("You Cant edit this document because this is not uploaded by you");" title="Update" ></i></a>';
             if(get.img_name!="") 
              getdata += ' <a href="downloadDocument.php?getid='+get.doc_guid+'&&flag=alldoc"> <i class="feather icon-download" title="Download" style="color: blue;"></i> </a>';
               if(get.user_id  == get.session_id) {
            getdata += '<a href="delete_document.php?id='+get.doc_id+'"  onclick="return confirm("Are you sure want to remove this Document?");"> ';
             getdata += '<i class="feather icon-trash"  title="Delete" style="color: red;" ></i> </a>';
                    }else if(get.user_id != get.session_id) {
                  getdata += ' <a href="javascript:void()" onclick="return confirm("You Cant edit this document because this is not uploaded by you");">';
                     getdata += '<i class="feather icon-trash noaccess" title="Delete"></i></a>';
                   }
             getdata +='</td></tr>';
        i++;
     });
     getdata += '</tbody></table?';
   // });
  
     $("#show_data").html(getdata);
      $("#order-listing").DataTable();
     $("#documentCount").text(html.total_count);
     },
     complete: function(){
      $("#case_no").val('');
      $("#uploaded_date").val('');
      $("#court_name").val('');
      var counts = $("#countn").val();
      showPagination(counts,10,flag);
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Documents', {timeOut: 5000})
    }
  }); 
}

function offset(page,limit){
  if(page <= 1)
    return 0;
  else
    return ((page-1)*limit);
}

var currentPage;
      function showPagination(counts,limit,flag) {
    // Number of items and limits the number of items per page
    var numberOfItems = counts;
    var limitPerPage =limit;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    var paginationSize = 7; 
    function showPage(whichPage) {
      if (whichPage < 1 || whichPage > totalPages) return false;
      currentPage = whichPage;                   
      $(".pagination li").slice(1, -1).remove();
      getPageList(totalPages, currentPage, paginationSize).forEach( item => {
        $("<li>").addClass("page-item")
        .addClass(item ? "current-page" : "disabled")
        .toggleClass("active", item === currentPage).append(
          $("<a>").addClass("page-link").attr({
            href: "javascript:void(0)"}).text(item || "...")
          ).insertBefore("#next-page");
      });
        // Disable prev/next when at first/last page:
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }
    $(".pagination").append(
      $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Prev")
        ),
      $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Next")
        )
      );
    showPage(1);

    // Use event delegation, as these items are recreated later    
    $(document).on("click", ".pagination li.current-page:not(.active)", function () {
      var s=showPage(+$(this).text());
      getpagination(+$(this).text(),flag);
      return s;
    });
    $("#next-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage+1);
        getpagination(currentPage+1,flag);
        return s;
      }
    });

    $("#previous-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage-1);
        getpagination(currentPage-1,flag);
        return s ;
      }
    });

}

function getPageList(totalPages, page, maxLength) {
  if (maxLength < 5) throw "maxLength must be at least 5";

  function range(start, end) {
    return Array.from(Array(end - start + 1), (_, i) => i + start); 
  }

  var sideWidth = maxLength < 9 ? 1 : 2;
  var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
  var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
  if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength-sideWidth-1)
        .concat([0])
        .concat(range(totalPages-sideWidth+1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
        .concat([0])
        .concat(range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }
    // Breaks on both sides
    return range(1, sideWidth)
    .concat([0])
    .concat(range(page - leftWidth, page + rightWidth)) 
    .concat([0])
    .concat(range(totalPages-sideWidth+1, totalPages));
}

function getpagination(currentpage,flag){
   var case_no = $("#case_no").val();
  var uploaded_date = $("#uploaded_date").val();
  var court_name = $("#court_name").val();
   var limit = '10';
  var offsets =   offset(currentpage,limit);
  $.ajax({
      type:'GET',
      url:'ajaxdata/filterCase.php',
      data:"case_no="+case_no+"&uploaded_date="+uploaded_date+"&court_name="+court_name+"&limit="+limit+"&offset="+offsets+"&flag="+flag,
      dataType: 'json',
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
    // console.log(html);
    var getdata = '';
     // $.each(data,function(key,val){
       
     getdata += '<input type="hidden" id="countn" value='+html.total_count+'> <table id="order-listing" class="table table-bordered">';
                  getdata += ' <thead class="bg-primary">';
                   getdata += ' <tr> <th>Sr.No. </th> <th style="width:129px;">Type of attachment</th> <th style="width:349px;">Title</th><th>Case No/ Year</th> <th>Document Type</th> <th>Uploded Date Time</th> <th>Actions</th></tr>';
     getdata += '       </thead> <tbody>';
       var i=1;
     $.each(html.data,function(is,get){
       getdata += '<tr> <td>'+i+'</td>';
        getdata += '  <td>';
        if(get.img_extension=='pdf') 
               getdata += '<img src="images/pdf.png" width="30" title='+get.img_name+'> ';
               else if(get.img_extension=='docx')   
               getdata += '<img src="images/word.png" width="30" title='+get.img_name+'>';  
              else if(get.img_extension=='xlsx')  
                 getdata += '<img src="images/excel.png" width="30" title='+get.img_name+'>'; 
               else if(get.img_extension=='csv')  
                  getdata += '<img src="images/csv.png" width="30" title='+get.img_name+'>'; 
                else if(get.img_extension=='png')   
                   getdata += '<img src="images/png.png" width="30" title='+get.img_name+'>';
                 else if(get.img_extension=='jpg' || get.img_extension=='jpeg')  
                     getdata += '<img src="images/jpg.png" width="30" title='+get.img_name+'>'; 
                  else if(get.img_extension=='doc' || get.img_extension=='docx')  
                     getdata += '<img src="images/docimg.png" width="30" title='+get.img_name+'>';  
        getdata += '</td>';
         getdata += '  <td>'+get.case_title+'</td>';
          getdata += '  <td>'+get.case+'</td>';
           getdata += '  <td>'+get.document_type+'</td>';
            getdata += ' <td>'+get.uploaded_date+'</td>';
             getdata += '  <td>';
            if(get.user_id == get.session_id) 
              getdata += '<a href="editMercDocument.php?id='+get.enc_doc_id+'"><i class="feather icon-edit" title="Update" style="color: orange;"></i></a>';
              else if(get.user_id != get.session_id) 
                getdata += ' <a><i class="feather icon-edit noaccess" onclick="return confirm("You Cant edit this document because this is not uploaded by you");" title="Update" ></i></a>';
             if(get.img_name!="") 
              getdata += ' <a href="upload_document/'+get.img_url+'" download> <i class="feather icon-download" title="Download" style="color: blue;"></i> </a>';
               if(get.user_id  == get.session_id) {
            getdata += '<a href="delete_document.php?id='+get.doc_id+'"  onclick="return confirm("Are you sure want to remove this Document?");"> ';
             getdata += '<i class="feather icon-trash"  title="Delete" style="color: red;" ></i> </a>';
                    }else if(get.user_id != get.session_id) {
                  getdata += ' <a href="javascript:void()" onclick="return confirm("You Cant edit this document because this is not uploaded by you");">';
                     getdata += '<i class="feather icon-trash noaccess" title="Delete"></i></a>';
                   }
             getdata +='</td></tr>';
        i++;
     });
     getdata += '</tbody></table?';
   // });
    
     $("#show_data").html(getdata);
      $("#order-listing").DataTable();
     },
     complete: function(){
      // var counts = $("#countn").val();
      // showPagination(counts,10);
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Documents', {timeOut: 5000})
    }
  }); 
}

// $( "#case_no" ).autocomplete({
//   source: function( request, response ) {
//    // Fetch data
//    $.ajax({
//     url: "search_casedata.php",
//     type: 'post',
//     dataType: "json",
//     data: {
//      search: request.term,
//      flag: 'search'
//          },
//     success: function( data ) {
//      response( data.data.case_no );
//     }
//    });
//   },
//   select: function (event, ui) {
//    // Set selection
//    $('#case_no').val(ui.item.label); // display the selected text
//    // $('#selectuser_id').val(ui.item.value); // save selected id to input
//    return false;
//   }
//  });

function downloadDoc(doc){
$.ajax({
      type:'POST',
      url:'ajaxdata/downloadDocument.php',
      data:"doc="+doc,
      dataType: 'json',
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
     var blob = html;
    console.log(blob.size);
    var link=document.createElement('a');
    link.href=window.URL.createObjectURL(blob);
    link.download="Dossier_" + new Date() + ".pdf";
    link.click();
     },
     complete: function(){
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Documents', {timeOut: 5000})
    }
  }); 
}