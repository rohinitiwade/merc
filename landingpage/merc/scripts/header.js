$(document).ready(function() {

	$("#display").click(function() {                

      $.ajax({    //create an ajax request to display.php
      	type: "GET",
      	url: "switch_division.php",             
        dataType: "html",   //expect html to be returned                
        success: function(response){  
        	alert(response);                  
        	$("#responsecontainer").html(response); 
            //alert(response);
        }

    });
  });
	$(document).click(function(){
		$("#country-list").hide();
	});

	$(".caseno_search").keyup(function(){ 
		$(".party-name-div").hide();
		$.ajax({
			type: "POST",
			url: "search_casedata.php",
			data:'keyword='+$(this).val()+'&flag=search',
			beforeSend: function(){
				$("#caseno_search").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
			},
			success: function(response){
				var res_data = JSON.parse(response);
				var dataShow = '<ul id="country-list" style="width: 268px;">';
				dataShow += '<a style="margin-bottom: -10px;background: #2d4866;width: 100%;float: left;text-decoration: underline;font-weight: bold;font-size: 16px;">Did You mean</a>';
				$.each(res_data.data,function(key,val){
					if(val.flag == 'party')
					dataShow += '<li id="'+val.full_name+'" onclick="partNameWise(this.id)">'+val.full_name+'</li>';
				else
					dataShow += '<a href="view-case.php?caseid='+val.case_id+'" onClick="this.form.submit()" target="_BLANK" style="padding: 0px;"><li style="font-size: 13px;cursor:pointer;">'+val.court_name+' - '+val.case_no+' / '+val.case_no_year+'</li></a>';
       // dataShow += '<li style="font-size: 13px;" id="'+val.full_name+'" onclick=partNameWise("'+val.full_name+'")>'+val.full_name+'</li>';
   });

				dataShow += '</ul>';
				$(".suggesstion-box").show();
				$(".suggesstion-box").html(dataShow);
				$("#caseno_search").css("background","#FFF");
			}
		});
	});
});
function partNameWise(partName){
	debugger;
	$.ajax({
		type: "POST",
		url: "search_casedata.php",
		data:'keyword='+partName+'&flag=partysearch',
		beforeSend: function(){
			$("#caseno_search").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(response){
			console.log(response);
			$("#caseno_search").val(partName);
			var partydata = JSON.parse(response);
			var partyShow = '<ul id="country-list" style="width: 268px;">';
			partyShow += '<a style="margin-bottom: -10px;background: #2d4866;width: 100%;float: left;text-decoration: underline;font-weight: bold;font-size: 16px;">Cases</a>';
			$.each(partydata.data,function(key,val){
				partyShow += '<a href="view-case.php?caseid='+val.case_id+'" target="_BLANK" style="padding: 0px;">';
				partyShow += '<li style="font-size: 13px;">'+val.court_name+' - '+val.case_no+' / '+val.case_no_year+'</li></a>';
       // dataShow += '<li style="font-size: 13px;" id="'+val.full_name+'" onclick=partNameWise("'+val.full_name+'")>'+val.full_name+'</li>';
   });

			partyShow += '</ul>';
			$(".suggesstion-box").hide();
			$(".party-name-div").show();
			$(".party-name-div").html(partyShow);
			$("#caseno_search").css("background","#FFF");
		}
	});
}

function filterTeamList() {
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("search-team");
	filter = input.value.toUpperCase();
	table = document.getElementById("team-list-filter");
	tr = table.getElementsByTagName("li");
	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByClassName("team-user-name")[0];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}       
	}
} 
