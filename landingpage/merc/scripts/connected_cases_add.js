getCaseTitle();

function getCaseTitle(){
	var val = $("#case_title").val();
	$.ajax({
		url: 'connected_case_title.php',
		type:'POST',		
		data : 'codestatus='+val,
		dataType: 'json',
		async:false,
		cache:false,
		success:function(response){
			console.log(response);
			var option = '<option value="">Select</option>';
			$.each(response.data,function(i,obj){
				option +='<option value='+obj.case_id+'>'+obj.case_type+' '+obj.case_no+'/ '+obj.case_no_year+' '+obj.case_title+'</option>';
			});
			$("#case_title").html(option);
			$("#case_title").select2({
				minimumResultsForSearch: 2
			});
		},
		error:function(e){
			// toastr.error("Error in fetching state",{timeout:5000});
		}
	});
}

/*$('#test').select2({
    data: mockData(),
    placeholder: 'search',
    multiple: true,
    // query with pagination
    query: function(q) {
      var pageSize,
        results,
        that = this;
      pageSize = 2; // or whatever pagesize
      results = [];
      if (q.term && q.term !== '') {
        // HEADS UP; for the _.filter function i use underscore (actually lo-dash) here
        results = _.filter(that.data, function(e) {
          return e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0;
        });
      } else if (q.term === '') {
        results = that.data;
      }
      q.callback({
        results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
        more: results.length >= q.page * pageSize,
      });
    },
  });

// Function to shuffle the demo data
function shuffle(str) {
  return str
    .split('')
    .sort(function() {
      return 0.5 - Math.random();
    })
    .join('');
}

// For demonstration purposes we first make
// a huge array of demo data (20 000 items)
// HEADS UP; for the _.map function i use underscore (actually lo-dash) here
function mockData() {
  return _.map(_.range(1, 20000), function(i) {
    return {
      id: i,
      text: shuffle('te ststr ing to shuffle') + ' ' + i,
    };
  });
}




*/



getRelateTo();

function getRelateTo(){
	$.ajax({
		url: 'connected_type.php',
		type:'POST',
		dataType: 'json',
		cache:false,
		success:function(response){
			console.log(response);
			var option = '<option value="">Select</option>';
			$.each(response,function(i,obj){
				option +='<option value='+obj.case_type_id+'>'+obj.connected_type+'</option>';
			});
			$("#connect").html(option);
		},
		error:function(e){
			toastr.error("Error in fetching state",{timeout:5000});
		}
	});
}
// uploadConnectedCases();
function uploadConnectedCases(type){


	var connected_id = $("#case_title").val();
	var case_id = $("#caseid").val();
	var status = $("#connect").val();
	var priority = $("#set_priority_select").val();
	var status_data = $("#connect option:selected").text();
	
	var uploadVo = new Object();
	uploadVo.connected_id = connected_id;
	uploadVo.case_id = case_id;
	uploadVo.status = status;
	uploadVo.priority = priority;
	uploadVo.status_data = status_data;
	if(isEmpty(connected_id) && type== 'click'){
		toastr.danger("Please select case title",{timeout:5000});
	}else{
		$.ajax({
			url: 'submit_connected_case.php',
			type:'POST',
			dataType: 'json',
			data : {
				connect_data : JSON.stringify(uploadVo)
			},
			cache:false,
			success:function(response){
				console.log(response);
				$("#connect_cases_table").html('');
				$("#connected-case").text(response.length);
			// if(response.status == 'success'){
				buildConnectedCaseTable(response);
		// }
		// else
		// 	toastr.error("Error in uploading connected case",{timeout:5000});
	},
	error:function(e){
		toastr.error("Error in fetching state",{timeout:5000});
	}
});
	}
}

function deleteConnectedCase(connect_id,caseid){
	var yes = confirm("Are you sure you want to delete connected case?");
	if(yes){
		$.ajax({
			url: 'delete_connect_case.php',
			type:'POST',
			dataType: 'json',
			data : {
				connected_main_id : connect_id,
				case_id : caseid
			},
			cache:false,
			success:function(response){
				console.log(response);
				if(response.status == 'success')
					toastr.success("Successfully deleted connected case",{timeout:5000});
			// uploadConnectedCases();
			buildConnectedCaseTable(response);
		},
		error:function(e){
			toastr.error("Error in fetching state",{timeout:5000});
		}
	});
	}
	else
		return false;
}

function buildConnectedCaseTable(response){
	debugger;
	$("#connect_cases_table").html('');
	var tr ='';
	$.each(response,function(i,obj){
		var casetype = isEmpty(obj.case_type) ? "" : obj.case_type;
		tr +='<tr>';
		tr +='<td class="case-title">'+obj.case_title+'</td>';
		tr +='<td class="case-title"><a style="text-decoration:none;cursor:pointer" href="'+host+'/view-case.php?caseid='+obj.encrypt_caseid+'" target="_blank">'+casetype+ ' ' +obj.case_no+'/ '+obj.case_no_year+'</a></td>';
		tr +='<td class="case-title">'+obj.status_data+'</td>';
		tr +='<td class="case-title"><button class="btn btn-danger btn-sm" type="button" onclick="deleteConnectedCase('+obj.connected_main_id+','+obj.case_id+')"><i class="fa fa-trash"></i></button></td></tr>';
	});
	if(tr!=''){
		var table = '<table class="table table-bordered"><thead><tr><th class="title_class">Title</th><th class="case_class">Case Reference No.</th><th>Relation</th><th>Action</th></tr></thead><tbody>';
		table += tr +'</tbody></table>';
	}
	$("#connect_cases_table").html(table);
}

function editNote(id){
	var getCurrentNote  = $("#noteText_"+id).text();
     //alert(getCurrentNote.trim());
     $(".editNoteBlock").remove();
     $(".noteTextDetail").show();
     var editElement     = '<div class="col editNoteBlock" style="padding:5px;" id="notetxt_'+id+'">'
     +'<textarea id="notetxtarea_'+id+'" class="noteText">'+getCurrentNote.trim()+'</textarea>'
     +'<div class="col"><b>Mark this note as private'
     +'<input type="radio" id="private_yes" name="enp" value="yes">Yes'
     +'<input type="radio" id="private_no" name="enp" value="no">No</b></div>'
     +'<div class="col" style="padding-right:6px;">'
     +'<button class="btn btn-primary" style="color: white;float: right;" id="btnEditNoteClear"><b>Cancel</b></button>'
     +'<button class="btn btn-primary" style="color: white;float: right;margin-right:3px;" id="btnUpdateNote"><b>Update</b></button></div></div>';
     $(editElement).insertBefore("#noteText_"+id);
     $("#noteText_"+id).hide();
     var radioValue = $("#type_"+id).val();
     //alert(radioValue);
     $('input[name=enp][value='+radioValue+']').attr('checked', true);
     
     $("#btnEditNoteClear").click(function(){
     	$(".editNoteBlock").remove();
     	$(".noteTextDetail").show();
     });

     $("#btnUpdateNote").click(function(){

     	var content = $("#notetxtarea_"+id).val();

     	$.ajax({
     		url: "/ajaxdata/updatednote.php",
     		type: 'POST',
     		data: {
     			noteid : id,
     			notes : content,
     			type : radioValue
     		},
     		success: function (data) {
     			console.log(data);
     			if (data.status) {
     				$(".editNoteBlock").remove();
     				$(".noteTextDetail").show();
     				$("#noteText_"+id).html("<p>"+content+"</p>");
     			}
     		}
     	});

     });

 }
 var todocounter=0;
 $(document).ready(function(){

$(".selectall_checkbox").change(function() { //"select all" change 
      $(".private").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
  });

    //".checkbox" change 
    $('.private').change(function() {
      //uncheck "select all", if one of the listed checkbox item is unchecked
      if (false == $(this).prop("checked")) { //if this item is unchecked
        $(".selectall_checkbox").prop('checked', false); //change "select all" checked status to false
    }
      //check "select all" if all checkbox items are checked
      if ($('.private:checked').length == $('.private').length) {
      	$(".selectall_checkbox").prop('checked', true);
      }
  });
});

 function markCompleted(){
 	if($('.private:checked').length <= 0)
 	{
 		toastr.warning("",'Please select atleast one case',{timeout:5000});
 		return false;
 	}
 }

 function reminder(){
 	document.getElementById('reminder_input').style.display="block";
 	document.getElementById('reminder_clickhere').style.display="none";
 }
 function remove_reminder(){
 	document.getElementById('reminder_input').style.display="none";
 	document.getElementById('reminder_clickhere').style.display="block";
 }
 function add_field()
 {
 	todocounter++;
 	var total_text=document.getElementsByClassName("input_text");
    // total_text=total_text.length+1;
    var append_reminder ="<div class='row' id='input_text"+todocounter+"_wrapper' style='margin-left: 0px!important;width: 100%'><i class='fa fa-minus-circle' style='color: red;cursor: pointer;margin-top: 9px;font-size: 20px;' onclick=remove_field('input_text"+todocounter+"')></i><div class='col-sm-4'><select class='input_text form-control' id='email_option_"+todocounter+"'> <option>Email</option><option>SMS</option></select></div><div class='col-sm-4'><select class='input_text form-control reminder_format' id='minute_"+todocounter+"'> <option value='minutes'>minute(s)</option><option value='hours'>hour(s)</option value='days'><option value='days'>day(s)</option><option value='week'>week(s)</option></select></div><div class='col-sm-3'><select class='input_text form-control' id='reminder_format_value_"+todocounter+"'> <option>5</option><option>10</option><option>15</option><option>20</option><option>25</option><option>30</option><option>35</option><option>40</option><option>45</option><option>50</option><option>55</option></select></div><span style='margin-top: 8px;'>before the due date & time</span></div>";
    
    $("#input").append(append_reminder);
    rcounter = todocounter;
}
function remove_field(id)
{
	document.getElementById(id+"_wrapper").innerHTML="";
}

function todoViewClick(all){
	var case_id = $("#case_id").val();
	var todoVo = new Object();
	todoVo.case_id = case_id;
	todoVo.flag = 'view';
	todoVo.list = all;
	$.ajax({
		url: './submit_todo.php',
		type:'POST',
		data : "tododetail="+JSON.stringify(todoVo),
		dataType: 'json',
		async:false,
		beforeSend: function(){
			$('.flip-square-loader').show(); 
			// toastr.success('Submit Data Successfully', 'Success Alert', {timeOut: 5000})             
		},
		success:function(response){
			// console.log(data);
			// var datas = JSON.parse(data);
			if(response.status == 'success'){
				// location.reload();
				getTodoDetails(response);
			}
		},
		complete: function(){
			$('.flip-square-loader').hide();  

		},
		error: function(e){
			$('.flip-square-loader').hide();  
			toastr.error('Unable to Load Data', 'Unable to Load Data!', {timeOut: 5000})
		}
	});
}

function todoView(all){
	var select_receipent_array = [];
	var assign_to_array = [];
	var reminder_array = [];
	var assign_adv = [];
	var privated = "public";
	var desc = $("#desc").val();
	var case_id = $("#case_id").val();
	if(desc == ''){
		toastr.warning('Please Enter To Do Description', {timeOut: 5000})  
		return false;
	}
	var datepicker_todo = $("#datepicker_todo").val();
	// var replace_date1 =  datepicker_todo.replace('/','');
	// var datepicker1_todo = $("#datepicker1_todo").val();
	// var replace_date1_todo =  datepicker1_todo.replace('/','');
	// var email_option = $("#email_option").val();
	// var time = $("#time").val();
	// var minute = $("#minute").val();
	//var private = $("#private").val();
	if(document.getElementById('private').checked){
		privated = "private";
	}
	var full_name = $("#full_name").val();
	var assign_to = $("#framework").val();
	$.each(assign_to,function(a,obj){
		assign_to_array.push(obj);
	});
	// var adv = $("#advocate").val();
	// $.each(adv,function(a,ad){
	// 	assign_adv.push(ad);
	// });
	// $.each(email_option,function(a,email){
	// 	email_option_array.push(email);
	// });
	// $.each(time,function(a,time){
	// 	time_array.push(time);
	// });
	// $.each(minute,function(a,min){
	// 	minute_array.push(min);
	// });
	for(var i = 0;i<=rcounter;i++){
		var email_option = $("#email_option_"+i).val();
		var time  = $("#reminder_format_value_"+i).val();
		var minute = $("#minute_"+i).val();
		var reminderVo = {
			email_option : email_option,
			time : time,
			minute : minute
		}
		reminder_array.push(reminderVo);
	}

	
	// var relate_to = $("#relate").select2('data');
	// $.each(relate_to,function(i,obj){
	// 	select_receipent_array.push(obj);
	// });

	var todoVo = new Object();
	todoVo.desc = desc;
	todoVo.datepicker = datepicker_todo;
	// todoVo.datepicker1 = replace_date1_todo;
	// todoVo.option = option;
	// todoVo.time = time;
	// todoVo.minute = minute;
	todoVo.private = privated;
	todoVo.full_name = full_name;
	todoVo.assign_to_array = assign_to_array;
	// todoVo.relateto = select_receipent_array;
	todoVo.reminder_array = reminder_array;
	todoVo.case_id = case_id;
	// todoVo.advocates = assign_adv;
	todoVo.flag = 'view';
	todoVo.list = all;
	$.ajax({
		url: './submit_todo.php',
		type:'POST',
		data : "tododetail="+JSON.stringify(todoVo),
		dataType: 'json',
		async:false,
		beforeSend: function(){
			$('.flip-square-loader').show(); 
			            
		},
		success:function(data){
			// console.log(data);
			// var datas = JSON.parse(data);
			if(data.status == 'success'){
				// location.reload();
				$("#todocount").text(data.count);
				getTodoDetails(data);
			}
			toastr.success('Submit Data Successfully', 'Success Alert', {timeOut: 5000}) 
		},
		complete: function(){
			$('.flip-square-loader').hide();  

		},
		error: function(e){
			$('.flip-square-loader').hide();  
			toastr.error('Unable to Submit Data', 'Sorry For Inconvenience!', {timeOut: 5000})
		}
	});

}

function getTodoDetails(response){
var tr ='';
			$.each(response.data,function(i,obj){
				i++;
				tr +='<tr id="todo_tr'+i+'">';
				tr +=' <td><p>'+i+'</p></td>';
				tr +='<td><div class="checkbox-zoom zoom-primary"><label>';
                 tr +='<input type="checkbox" value="'+obj.encrypted_todo_id+'" id="private" class="todo-list-checkbox" name="checked_id[]" onclick=markComplete('+i+','+obj.todo_id+')>';
                tr +='<span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>';
                tr +='<span><b>'+obj.content+'</b></span></label></div>'; 
                tr +='<p class="m-b-0"><i><b>Visibility:</b>'+obj.private+'</i> </p></td>';
				tr +=' <td>'+obj.case+'</td>';
				tr +='<td>'+obj.expiry_date+'</td>';
				tr +='<td><p class="assign m-b-0"><i>';
				$.each(obj.team,function(i,tem){
					tr +=tem;
				});
				tr+='</i></p></td>';
				tr +='<td><a href="edit_todo?id='+obj.encrypted_todo_id+'" class="btn btn-info btn-mini"><i class="feather icon-edit createtodolist" cursor="pointer" title="Edit" ></i></a>';
				tr +='<a href="delete_todo?id='+obj.encrypted_todo_id+'" class="btn btn-danger btn-mini"><i class="feather icon-trash createtodolist" cursor="pointer" title="Delete" ></i></a>';
				tr +='<a href="delete_todo?id='+obj.encrypted_todo_id+'" class="btn btn-primary btn-mini"><i class="fa fa-refresh createtodolist" cursor="pointer" title="Undo" ></i></a></td>';
			});

			var table='<div class="todo_nav"><nav class="navbar navbar-expand-lg">';
              table += '<div class="collapse navbar-collapse" id="navb">';
              table += '<ul class="navbar-nav mr-auto" id="action-span">';
              table += '<li class="nav-item"><div class="nav-link"><a onclick=todoViewClick("all") style="cursor:pointer"><b>To Dos</b>'; 
              table += '<span class="main_back "><b style="color: black !important;">('+response.all+')</b></span></a></div></li>';
               table += '<li class="nav-item"><div class="nav-link"><a onclick=todoViewClick("Completed") style="cursor:pointer"><b>Completed</b>';
              table += '<span class="main_completed list"><b style="color: black !important;">('+response.complete+')</b></span></a></div></li>';
              table += '</ul>';
              table += '<form class="form-inline my-2 my-lg-0" method="POST">';
              table += '<select  class="form-control" id="" name="toAll" >';
              table += '<option value="">--Select To-Do--</option>';
              table += '<option value="" selected> My to-dos  </option>';
              table += '</select>&nbsp;&nbsp;';
              table += '<div class="input-group"><input class="form-control search_input" type="text" placeholder="Search" name="">';
              table += '<span class="input-group-addon" id="basic-addon1" onclick="this.form.submit()"><i class="feather icon-search createi"></i>Search</span>';
              table += '</div>';
              table += '</form>';
              table += '</div>';
              table += '</nav></div>';
                                   
			table += ' <div class="table-responsive"><table class="table table-striped table-bordered" border="2"><thead><tr> <th style="background-color: #5AD9E9;">Sr.No.</th>';
               table += '<th style="background-color: #5AD9E9;width: 25%;">Description</th>';
                table += '<th style="background-color: #5AD9E9;">Case</th>';
                 table += '<th style="background-color: #5AD9E9;">Expiry Date</th>';
                 table += '<th style="background-color: #5AD9E9;">Assigned To</th>';
                 table += '<th style="background-color: #5AD9E9;">Action</th></tr></thead><tbody>';
			table += tr +'</tbody></table></div>';
			$("#todo_show").html(table);
}


function markComplete(id,todoid){
$.ajax({
		url: 'complete_todo.php',
		type:'POST',
		data : "to_id="+todoid,
		dataType: 'json',
		async:false,
		beforeSend: function(){
			$('.flip-square-loader').show();            
		},
		success:function(data){
			// console.log(data);
			if(data.status == 'success'){
				//location.reload();
				toastr.success('Successfully marked as completed', 'Success Alert', {timeOut: 5000}); 
				document.getElementById("todo_tr"+id).outerHTML = '';
				// todoViewClick('Completed');
			}
		},
		complete: function(){
			$('.flip-square-loader').hide();  

		},
		error: function(e){
			$('.flip-square-loader').hide();  
			toastr.error('Unable to mark as complete', 'Sorry For Inconvenience!', {timeOut: 5000})
		}
	});

}
var rcounter =0;

// $(document).on('change','.reminder_format',function(){
// 	var reminder_format_value='';
// 	var val = $(this).val();

// 	if(val == 'minutes'){
// 		for(var i =1 ; i <= 11;i++){
// 			reminder_format_value += '<option>'+i*5+'</option>';
// 		}
// 	}
// 	if(val == 'hours'){
// 		for(var i =1 ; i <= 23;i++){
// 			reminder_format_value += '<option>'+i+'</option>';
// 		}
// 	}
// 	if(val == 'days'){
// 		for(var i =1 ; i <= 31;i++){
// 			reminder_format_value += '<option>'+i+'</option>';
// 		}
// 	}
// 	if(val == 'week'){
// 		for(var i =1 ; i <= 4;i++){
// 			reminder_format_value += '<option>'+i+'</option>';
// 		}
// 	}
// 	$("#reminder_format_value_"+rcounter).html(reminder_format_value);
// });



function order(){
	var case_id = $("#caseid").val();
	var uploadVo = new Object();
	uploadVo.case_id = case_id;
	$.ajax({
		url: 'fetch_order_judgement.php',
		type:'POST',
		dataType: 'json',
		data : {
			connect_data : JSON.stringify(uploadVo)
		},
		cache:false,
		success:function(response){
			console.log(response);
			$("#order_cases_table").html('');
			// $("#order-jud").text(response.length);
			var tr ='';
			$.each(response,function(i,obj){
				tr +='<tr>';
				tr +='<td class="case-title">'+obj.count+'</td>';
				tr +='<td class="case-title"><a style="text-decoration:none;cursor:pointer;color: #1c273c;" href="'+host+'/upload_document/'+obj.pdf_url+'" target="_blank">'+obj.ordername+''+obj.coram+'</a></td>';
				tr +='<td class="case-title">'+obj.order_date+'</td>';
				tr +='<td class="case-title"><a style="text-decoration:none;cursor:pointer" href="'+host+'/upload_document/'+obj.pdf_url+'" target="_blank">';
				if(!isEmpty(obj.pdf_url)){
					tr +='View';
				}
				tr +='</a></td>';
			});

			var table = '<table class="table table-bordered" id="order_table_id"><thead><tr><th class="title_class">#</th><th class="case_class">Orders/Judgment</th><th>Date of Orders/Judgment</th><th> </th></tr></thead><tbody>';
			table += tr +'</tbody></table>';

			$("#order_cases_table").html(table);
			$("#order_table_id").DataTable();

		},
		error:function(e){
			toastr.error("Error in fetching state",{timeout:5000});
		}
	});
}

// $('#advocate').select2({
// 		placeholder: 'Select...',
// 		closeOnSelect: false,
// 		multiple: true,
// 		allowClear: false
// 	});


