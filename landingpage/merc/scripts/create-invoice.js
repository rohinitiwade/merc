$(document).ready(function(){
	$("#hearing-listing").dataTable();
	$("#date-invoice").datepicker({
		maxDate :'+0d'
	}).datepicker("setDate", new Date());
	$(document).on('change',".taxValue",function(){
		var igstcount = 0,gstcount=0,vatcount=0,servicecount=0;
		var val=$(this).val().split("_");
		$("#tax_val_"+val[1]).addClass('col-sm-7').removeClass('col-sm-12');
		$("#per-tax_"+val[1]).show();
		if(val[0] == 'IGST'){
			$("#igst").show();
			igstcount++;
		}
		else if(val[0] == 'GST'){
			$("#gst").show();
			gstcount++;
		}
		else if(val[0] == 'Service Tax'){
			vatcount++;
			$("#service-tax").show();
		}
		else if(val[0] == 'VAT'){
			servicecount++;
			$("#vat").show();
		}
		if(igstcount == 0){
			$("#igst-total").text("0.00");
		}
		if(gstcount == 0){
			$("#gst-total").text("0.00");
		}
		if(vatcount == 0){
			$("#vat-total").text("0.00");
		}
		if(servicecount == 0){
			$("#service-total").text("0.00");
		}			
		getTaxCalculation(val[1]);
		// var tax_val_perc = $("#tax-value-in-per_"+val[1]).val();
	});
	
	$('#getclient').select2({
		ajax: {
			url: 'ajaxdata/getClientName.php',
			data: function (params) {
				var query = {
					search: params.term
				}
				return query;
			},
			processResults: function(data) {
				if (data.length)        	
					return {
						results: data
					};

				}
			}
		});
});
var counter = 0,stateChange = false;
function addMoreInvoice(){
	//stateChange = true;
	counter++;
	var newEntry = '<tr id="added-invoice_'+counter+'">';
	newEntry +='<td><button class="btn btn-danger btn-mini" type="button" onclick="deleteInvoice('+counter+')">';
	newEntry +='<i class="feather icon-trash"></i></button></td>';
	newEntry +='<td contenteditable="true">';
	newEntry +='</td>';
	newEntry +='<td>';
	newEntry +='<p contenteditable="true" id="quantity_'+counter+'" onkeyup="getQuantity('+counter+')"></p></td>';
	newEntry +='<td>';
	newEntry +='<p contenteditable="true" id="cost_'+counter+'" onkeyup="getQuantity('+counter+')"></p></td>';
	newEntry +='<td>';
	newEntry +='<div class="row col-sm-12 p-0"><div class="col-sm-12 p-0" id="tax_val_'+counter+'">';
	newEntry +='<select class="form-control taxValue calculation" id="taxValue_'+counter+'">';
	newEntry +=' <option>Please select</option>';
	newEntry +='<option value="IGST_'+counter+'">IGST</option>';
	newEntry +='<option value="GST_'+counter+'">GST</option>';
	newEntry +='<option value="Service Tax_'+counter+'">Service Tax</option>';
	newEntry +='<option value="VAT_'+counter+'">VAT</option>';
	newEntry +='</select>';
	newEntry +='</div>';
	newEntry +='<div class="col-sm-5" id="per-tax_'+counter+'" style="display: none;">';
	newEntry +='<input type="text" class="form-control" id="tax-value-in-per_'+counter+'" onkeyup="getTaxCalculation('+counter+')">';
	newEntry +='</div></div>';
	newEntry +='</td>';
	newEntry +='<td id="total_'+counter+'"> 0.00 </td>';
	newEntry +='</tr>';
	
	$("#invoice-table").append(newEntry);
}

function getTaxCalculation(counter) {
	var tax_val=0,total_igst=0,total_gst=0,total_vat=0,total_service=0,total=0;
	$(".calculation").each(function(i,obj){
		if($("#tax-value-in-per_"+counter).val() > 0){
			total = $("#total_"+i).text();
			tax_val = parseFloat($("#tax-value-in-per_"+i).val());
			tax_calculated_val = parseFloat(total).toFixed(2) * parseFloat((tax_val / 100)).toFixed(2);

			var tax_type = $("#taxValue_"+i).val().split("_");
			if(tax_type[0] == 'IGST'){	

				total_igst = total_igst + tax_calculated_val;
				$("#igst-total").text(total_igst.toFixed(2));

			}
			if(tax_type[0] == 'GST'){

				/*total = $("#total_"+counter).text();
				tax_val = parseFloat($("#tax-value-in-per_"+counter).val());*/
				total_gst = parseFloat(total_gst) + tax_calculated_val;
			// if(tax_type[0] == 'IGST')
			$("#gst-total").text(total_gst.toFixed(2));
			
		}
		if(tax_type[0] == 'VAT'){

			/*total = $("#total_"+counter).text();
			tax_val = parseFloat($("#tax-value-in-per_"+counter).val());*/
			total_vat = total_vat + tax_calculated_val;
			// if(tax_type[0] == 'IGST')
			$("#vat-total").text(total_vat.toFixed(2));
			
		}
		if(tax_type[0] == 'Service Tax'){

			/*total = $("#total_"+counter).text();
			tax_val = parseFloat($("#tax-value-in-per_"+counter).val());*/
			total_service = total_service + tax_calculated_val;
			
			$("#service-total").text(total_service.toFixed(2));
			
		}
	}

	subTotal = $("#subtotal").text();
	var final_total = parseFloat(subTotal) + 
	parseFloat($("#igst-total").text()) + 
	parseFloat($("#gst-total").text()) + 
	parseFloat($("#service-total").text()) + 
	parseFloat($("#vat-total").text()); 

	$("#total_val").text(final_total.toFixed(2));
});
}

function getQuantity(counter){
	var quantity = $("#quantity_"+counter).text() == '' ? 0 : $("#quantity_"+counter).text();
	var cost = $("#cost_"+counter).text() == '' ? 0 : $("#cost_"+counter).text();
	var total = parseFloat(quantity).toFixed(2) * parseFloat(cost).toFixed(2);
	$("#total_"+counter).text(total.toFixed(2));
	var subTotal = getSubTotal(counter);
	$("#subtotal").text(subTotal);
	/*var final_total = parseFloat(subTotal) +  parseFloat($("#gst-total").text()) + parseFloat($("#service-total").text()) + parseFloat($("#vat-total").text()); 
	$("#total_val").text(final_total.toFixed(2));*/
	getTaxCalculation(counter);
}

function getSubTotal(count){
	var subtotal = 0;
	for(var i=0;i<=count;i++){
		subtotal = parseFloat(subtotal) + parseFloat($("#total_"+i).text());
	}
	return subtotal.toFixed(2);
}
function hearingfetch(type){
	$.ajax({
		type:'POST',
		url:'ajaxdata/invoicedata.php',
		data:{
			exporttype: type
		},
		dataType:"json",
		success:function(response){
			console.log(response);
			var showdata ='';
			if(type == 'expenses'){
				$("#invoice-export-heading").text('Insert Expenses');
				showdata += '<div class="col-12 table-responsive"><table id="hearing-listing" class="table table-bordered "><thead class="bg-primary">';
				showdata += '<tr><th>#</th><th>Case</th><th>Particulars</th>';
				showdata +='<th>Date</th><th>Money Spent (INR)</th><th>Payment Method</th></tr></thead><tbody>';
				$.each(response,function(i,obj){
					showdata += '<tr><td style="padding: 10px;" class="rowclickable"><div class="checkbox-zoom zoom-primary"><label><input type="checkbox" name="selectUser" class="invoicecheck" value="'+obj.case_type+' '+obj.case_no+'/ '+obj.case_no_year+'_'+obj.moneyspent+'" id="" >';
					showdata += ' <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span></label></div></td>';
					showdata += ' <td style="padding: 10px;" class="rowclickable" >'+obj.case_type+' '+obj.case_no+'/ '+obj.case_no_year+'</td>';
					showdata += ' <td style="padding: 10px;" class="rowclickable" >'+obj.particulars+' </td>';
					showdata += ' <td style="padding: 10px;" class="rowclickable" >'+obj.date+' </td>';
					showdata += ' <td style="padding: 10px;" class="rowclickable" >'+obj.moneyspent+' </td>';
					showdata += ' <td style="padding: 10px;" class="rowclickable" >'+obj.method+' </td>';
					showdata += '</tr>';
				});
				showdata += '</tbody></table></div>';
			}
			else if(type == 'hearing'){
				$("#invoice-export-heading").text('Insert Hearings');
				showdata += '<div class="col-12 table-responsive"><table id="hearing-listing" class="table table-bordered "><thead class="bg-primary">';
				showdata += '<tr><th>#</th><th>Case</th><th>Hearing Date</th></tr></thead><tbody>';
				$.each(response,function(i,obj){
					showdata += '<tr><td style="padding: 10px;" class="rowclickable"><div class="checkbox-zoom zoom-primary"><label><input type="checkbox" name="selectUser" class="invoicecheck" value="'+obj.case_type+' '+obj.case_no+'/ '+obj.case_no_year+'_" id="" >';
					showdata += ' <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span></label></div></td>';
					showdata += '<td style="padding: 10px;" class="rowclickable" >'+obj.case_type+' '+obj.case_no+'/ '+obj.case_no_year+'</td>';
					showdata += ' <td style="padding: 10px;" class="rowclickable" >'+obj.hearing_date+' </td></tr>';
				});
				showdata += '</tbody></table></div>';
			}
			$("#modal-data").html(showdata);
			$("#hearing-data").modal('show');

		},
		error:function(){
			toastr.error("","Error fetching team list",{timeout:5000});
		}
	});
}


function import_hearing(){
	var caseArr=[];
	var particularsval='';
	var getdata ='';

	$("input:checkbox[class='invoicecheck']:checked").each(function(i,obj){
		var val = $(this).val().split("_");
		var casetype = val[0];
		var amt = val[1];
		/*caseArr.push(val);
	});*/
	
	// if(caseArr.length > 0){
		/*	$.each(caseArr,function(i,obj){*/
			particularsval = $("#particulars_0").text().trim();
			if(!isEmpty(particularsval)){
				i = counter;
				i++;
			}
			getdata+='<tr id="added-invoice_'+i+'"><td><button class="btn btn-danger btn-mini" type="button" onclick="deleteInvoice('+i+')">';
			getdata +='<i class="feather icon-trash"></i></button></td>';
			getdata+='<td contenteditable="true" id="particulars_'+i+'">'+casetype+'</td><td><p contenteditable="true" id="quantity_'+i+'" onkeyup="getQuantity('+i+')">1</p></td>';
			getdata+='<td><p contenteditable="true" id="cost_'+i+'" onkeyup="getQuantity('+i+')">'+amt+'</p></td>';
			getdata+='<td><div class="col-sm-12 row p-0"><div class="col-sm-12 p-0" id="tax_val_'+i+'">';
			getdata+='<select class="form-control taxValue calculation" id="taxValue_'+i+'">';
			getdata+='  <option>Please select</option>';
			getdata+='  <option value="IGST_'+i+'">IGST</option>';
			getdata+=' <option value="GST_'+i+'">GST</option>';
			getdata+='<option value="Service Tax_'+i+'">Service Tax</option>';
			getdata+='<option value="VAT_'+i+'">VAT</option></select></div>';
			getdata+='<div class="col-sm-5" id="per-tax_'+i+'" style="display: none;">';
			getdata+='<input type="text" class="form-control" placeholder="%" name="" id="tax-value-in-per_'+i+'" onkeyup="getTaxCalculation('+i+')">';
			getdata+='</div></div></td>';
			getdata+='<td id="total_'+i+'">0.00</td></tr>';			
			counter = i;
		});
	// }
	if(isEmpty(particularsval))
		$("#invoice-table tbody").html(getdata);
	else
		$("#invoice-table tbody").append(getdata);
	$("#hearing-data").modal("hide");
}

function deleteInvoice(count){
	document.getElementById("added-invoice_"+count).outerHTML = '';
	counter--;
	//getQuantity(counter);
}

function sendbyEmail(){
	var billing_invoice = $("#billing-invoice").val();
	if(isEmpty(billing_invoice)){
		alert("Please select billing address");
		return false;
	}
	var totalArr= [];
	var sendInvoice = new Object();
	sendInvoice.getclient = $("#getclient").val();
	sendInvoice.title = $("#title-invoice").val();
	sendInvoice.invoice_number = $("#number-invoice").val();
	sendInvoice.date = $("#date-invoice").val();
	sendInvoice.billing = $("#billing-invoice").val();
	for(var i=0;i<=counter;i++){
		var totalObj = new Object();
		totalObj.particulars = $("#particulars_"+i).text();
		totalObj.quantity = $("#quantity_"+i).text();
		totalObj.cost = $("#cost_"+i).text();
		totalObj.taxValue = $("#taxValue_"+i+" option:selected").text();
		totalObj.tax_value_in_per = $("#tax-value-in-per_"+i).val();
		totalObj.total = $("#total_"+i).text();
		
		if(totalObj.taxValue == 'IGST')
			totalObj.tax_total = $("#igst-total").text();
		if(totalObj.taxValue == 'GST')
			totalObj.tax_total = $("#gst-total").text();
		if(totalObj.taxValue == 'Service Tax')
			totalObj.tax_total = $("#service-total").text();
		if(totalObj.taxValue == 'VAT')
			totalObj.tax_total = $("#vat-total").text();
		totalArr.push(totalObj);
	}
	sendInvoice.totalArr = totalArr;
	sendInvoice.subtotal = $("#subtotal").text();
	sendInvoice.total_val = $("#total_val").text();
	sendInvoice.notes_for_client =$("#notes_for_client").val();
	$.ajax({
		type:'POST',
		url:'submit_invoice.php',
		dataType:"json",
		data: JSON.stringify(sendInvoice),
		contentType : 'application/json;charset=utf-8',
		success:function(response){
			console.log(response);
			
		},
		error:function(e){

		}
	});
}