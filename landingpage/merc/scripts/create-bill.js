$(document).ready(function(){
	$("#other-client-details").hide();
	$("input[name='bill-from']").on('change',function(){
		var val = $(this).val();
		if(val == 'Client'){
			$("#other-client-details").hide();
			$("#client-details").show();
		}
		else if(val == 'Other'){
			$("#other-client-details").show();
			$("#client-details").hide();
		}
	});
	$(document).on('change',".taxValue",function(){
		var igstcount = 0,gstcount=0,vatcount=0,servicecount=0;
		var val=$(this).val().split("_");
		$("#tax_val_"+val[1]).addClass('col-sm-7').removeClass('col-sm-12');
		$("#per-tax_"+val[1]).show();
		if(val[0] == 'IGST'){
			$("#igst").show();
			igstcount++;
		}
		else if(val[0] == 'GST'){
			$("#gst").show();
			gstcount++;
		}
		else if(val[0] == 'Service Tax'){
			vatcount++;
			$("#service-tax").show();
		}
		else if(val[0] == 'VAT'){
			servicecount++;
			$("#vat").show();
		}
		if(igstcount == 0){
			$("#igst-total").text("0.00");
		}
		if(gstcount == 0){
			$("#gst-total").text("0.00");
		}
		if(vatcount == 0){
			$("#vat-total").text("0.00");
		}
		if(servicecount == 0){
			$("#service-total").text("0.00");
		}			
		getTaxCalculation(val[1]);
		// var tax_val_perc = $("#tax-value-in-per_"+val[1]).val();
	});
	$("#data-of-receipt").datepicker();
});
var counter = 0,stateChange = false;
function addMoreInvoice(){
	stateChange = true;
	counter++;
	var newEntry = '<tr>';
	newEntry +='<td><button class="btn btn-danger btn-mini" type="button" onclick="deleteInvoice('+counter+')">';
	newEntry +='<i class="feather icon-trash"></i></button></td>';
	newEntry +='<td contenteditable="true">';
	newEntry +='</td>';
	newEntry +='<td>';
	newEntry +='<p contenteditable="true" id="quantity_'+counter+'" onkeyup="getQuantity('+counter+')"></p></td>';
	newEntry +='<td>';
	newEntry +='<p contenteditable="true" id="cost_'+counter+'" onkeyup="getQuantity('+counter+')"></p></td>';
	newEntry +='<td>';
	newEntry +='<div class="row col-sm-12 p-0"><div class="col-sm-12 p-0" id="tax_val_'+counter+'">';
	newEntry +='<select class="form-control taxValue calculation" id="taxValue_'+counter+'">';
	newEntry +=' <option>Please select</option>';
	newEntry +='<option value="IGST_'+counter+'">IGST</option>';
	newEntry +='<option value="GST_'+counter+'">GST</option>';
	newEntry +='<option value="Service Tax_'+counter+'">Service Tax</option>';
	newEntry +='<option value="VAT_'+counter+'">VAT</option>';
	newEntry +='</select>';
	newEntry +='</div>';
	newEntry +='<div class="col-sm-5" id="per-tax_'+counter+'" style="display: none;">';
	newEntry +='<input type="text" class="form-control" id="tax-value-in-per_'+counter+'" onkeyup="getTaxCalculation('+counter+')">';
	newEntry +='</div></div>';
	newEntry +='</td>';
	newEntry +='<td id="total_'+counter+'"> 0.00 </td>';
	newEntry +='</tr>';

	$("#invoice-table").append(newEntry);
}

function getTaxCalculation(counter) {
	var tax_val=0,total_igst=0,total_gst=0,total_vat=0,total_service=0,total=0;
	$(".calculation").each(function(i,obj){
		if($("#tax-value-in-per_"+counter).val() > 0){
			total = $("#total_"+i).text();
			tax_val = parseFloat($("#tax-value-in-per_"+i).val());
			tax_calculated_val = parseFloat(total).toFixed(2) * parseFloat((tax_val / 100)).toFixed(2);

			var tax_type = $("#taxValue_"+i).val().split("_");
			if(tax_type[0] == 'IGST'){	

				total_igst = total_igst + tax_calculated_val;
				$("#igst-total").text(total_igst.toFixed(2));

			}
			if(tax_type[0] == 'GST'){

				/*total = $("#total_"+counter).text();
				tax_val = parseFloat($("#tax-value-in-per_"+counter).val());*/
				total_gst = parseFloat(total_gst) + tax_calculated_val;
			// if(tax_type[0] == 'IGST')
			$("#gst-total").text(total_gst.toFixed(2));
			
		}
		if(tax_type[0] == 'VAT'){

			/*total = $("#total_"+counter).text();
			tax_val = parseFloat($("#tax-value-in-per_"+counter).val());*/
			total_vat = total_vat + tax_calculated_val;
			// if(tax_type[0] == 'IGST')
			$("#vat-total").text(total_vat.toFixed(2));
			
		}
		if(tax_type[0] == 'Service Tax'){

			/*total = $("#total_"+counter).text();
			tax_val = parseFloat($("#tax-value-in-per_"+counter).val());*/
			total_service = total_service + tax_calculated_val;
			
			$("#service-total").text(total_service.toFixed(2));
			
		}
	}

	subTotal = $("#subtotal").text();
	var final_total = parseFloat(subTotal) + 
	parseFloat($("#igst-total").text()) + 
	parseFloat($("#gst-total").text()) + 
	parseFloat($("#service-total").text()) + 
	parseFloat($("#vat-total").text()); 

	$("#total_val").text(final_total.toFixed(2));
});
}

function getQuantity(counter){
	var quantity = $("#quantity_"+counter).text() == '' ? 0 : $("#quantity_"+counter).text();
	var cost = $("#cost_"+counter).text() == '' ? 0 : $("#cost_"+counter).text();
	var total = parseFloat(quantity).toFixed(2) * parseFloat(cost).toFixed(2);
	$("#total_"+counter).text(total.toFixed(2));
	var subTotal = getSubTotal(counter);
	$("#subtotal").text(subTotal);
	/*var final_total = parseFloat(subTotal) +  parseFloat($("#gst-total").text()) + parseFloat($("#service-total").text()) + parseFloat($("#vat-total").text()); 
	$("#total_val").text(final_total.toFixed(2));*/
	getTaxCalculation(counter);
}

function getSubTotal(count){
	var subtotal = 0;
	for(var i=0;i<=count;i++){
		subtotal = parseFloat(subtotal) + parseFloat($("#total_"+i).text());
	}
	return subtotal.toFixed(2);
}