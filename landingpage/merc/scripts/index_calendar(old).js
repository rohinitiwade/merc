/*
today = new Date();
currentMonth = today.getMonth();
currentYear = today.getFullYear();
     /* selectYear = document.getElementById("year");
     selectMonth = document.getElementById("month");/

     months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

     monthAndYear = document.getElementById("monthAndYear");
     showCalendar(currentMonth, currentYear);


     function next() {
      currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
      currentMonth = (currentMonth + 1) % 12;
      showCalendar(currentMonth, currentYear);
    }

    function previous() {
      currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
      currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
      showCalendar(currentMonth, currentYear);
    }

    function jump() {
      currentMonth = today.getMonth();
      currentYear = today.getFullYear();
      showCalendar(currentMonth, currentYear);
    }

    function showCalendar(month, year) {
      $.ajax({
        url:host+"/hearing_calendar/hearing_api.php",
        type:"POST",
        dataType : 'json',
        contentType : 'application/json;charset=utf-8',
        beforeSend : function(){
          $(".flip-square-loader").show();
        },
        success: function(response){
         firstDay = (new Date(year, month)).getDay();

    tbl = document.getElementById("calendar-body"); // body of the calendar

    // clearing all previous cells
    tbl.innerHTML = "";

    // filing data about month and in the page via DOM.
    monthAndYear.innerHTML = months[month] + " " + year;
    // selectYear.value = year;
    // selectMonth.value = month;

    // creating all cells
    var date = 1;
    for (var i = 0; i < 5; i++) {
        // creates a table row
        var row = document.createElement("tr");

        //creating individual cells, filing them up with data.
        for (var j = 0; j < 7; j++) {
          if (i === 0 && j < firstDay) {
            cell = document.createElement("td");
            cellText = document.createTextNode("");
            cell.appendChild(cellText);
            row.appendChild(cell);
          }
          else if (date > daysInMonth(month, year)) {
            cell = document.createElement("td");
            cellText = document.createTextNode("");
            cell.appendChild(cellText);
            row.appendChild(cell);
          }

          else {
            cell = document.createElement("td");
            cell_p = document.createElement("p");
            cellText = document.createTextNode(date);
            if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
              cell.classList.add("today_day");
            } // color today's date
            else
              cell.classList.add("addTodo_hearing");
            cell.setAttribute('title','Click to add Todo or Hearing Date');
            cell_p.appendChild(cellText);
            cell.appendChild(cell_p);
            $.each(response,function(i,obj){
				
				var caseno_array = obj.caseno;
				$.each(caseno_array,function(j,new_obj){
					var hearing_date = new Date(obj.hearing_date).getDate();
					var hearing_month = new Date(obj.hearing_date).getMonth();
					var hearing_year = new Date(obj.hearing_date).getFullYear();
					if(date == hearing_date && year === hearing_year && month === hearing_month){
						cell_hearing = document.createElement("p");
						cell_hearing.classList.add("calendaer_event");
						cellText_hearing = document.createTextNode(new_obj);
						cell_hearing.appendChild(cellText_hearing);
						cell.appendChild(cell_hearing);
						row.appendChild(cell);
					}
				});
           });

            row.appendChild(cell);
            date++;
          }
        }

        tbl.appendChild(row); // appending each row into calendar body.
      }
    },
    complete:function(){
      $(".flip-square-loader").hide();
    },
    error :function(e){
      $(".flip-square-loader").hide();
      toastr.error("Unable to load calendar","Sorry for inconvinience",{timeout:5000});
    }
  });
    }


// check how many days in a month code from https://dzone.com/articles/determining-number-days-month
function daysInMonth(iMonth, iYear) {
  return 32 - new Date(iYear, iMonth, 32).getDate();
}
*/

$(document).ready(function(){
  $(".reminder_div").hide();
  $("#to-do-list").hide();
});
$("input[name='new_event']").on('change',function(){
  var val = $(this).val();
  if(val == 'hearing_radio'){
    $("#hearing").show();
    $("#to-do-list").hide();
  }
  else{
    $("#hearing").hide();
    $("#to-do-list").show();
  }
});

var rcounter = 0;
var reminder_format_value ;
function auto_reminder(){
  rcounter = 0;
  $(".reminder_div").show();
  for(var i =1 ; i <= 11;i++){
    reminder_format_value += '<option>'+i*5+'</option>';
  }
  $("#reminder_format_value_"+rcounter).html(reminder_format_value);
}
function addExtraReminder(){
  rcounter++;
  var extra_reminder='<div id="reminder_new_div_'+rcounter+'" class="row" style="width:100%"><div class="col-sm-1">';
  extra_reminder += '<button class="btn btn-link btn-sm" type="button" onclick="delete_reminder()">Delete<i class="fas fa-minus"></i></button></div>';
  extra_reminder += '<div class="col-sm-3">';
  extra_reminder+='<select class="form-control" id="reminder_type_'+rcounter+'"><option value="email">Email</option><option value="sms">SMS</option></select></div>';
  extra_reminder+='<div class="col-sm-3">';
  extra_reminder+='<select class="form-control" id="reminder_format_value_'+rcounter+'"></select></div>';
  extra_reminder+='<div class="col-sm-3">';
  extra_reminder+='<select class="form-control reminder_format" id="reminder_format_'+rcounter+'" ><option value="minutes">minute(s)</option><option value="hours">hour(s)</option><option value="days">day(s)</option><option value="week">week(s)</option></select></div>';
  extra_reminder+='<div class="col-sm-2" style="padding:0;"><small>before due date & time</small></div><div>';

  $("#reminder-list").append(extra_reminder);
}
function delete_reminder(){
  if(rcounter <= 0 )
    $(".reminder_div").hide();    
  else
    $("#reminder_new_div_"+rcounter).hide();
  rcounter--;
}

$(document).bind('change','.reminder_format',function(){
  var val = $(this).val();
  
  if(val == 'minutes'){
    for(var i =1 ; i <= 11;i++){
      reminder_format_value += '<option>'+i*5+'</option>';
    }
  }
  if(val == 'hours'){
    for(var i =1 ; i <= 23;i++){
      reminder_format_value += '<option>'+i+'</option>';
    }
  }
  if(val == 'days'){
    for(var i =1 ; i <= 31;i++){
      reminder_format_value += '<option>'+i+'</option>';
    }
  }
  if(val == 'week'){
    for(var i =1 ; i <= 4;i++){
      reminder_format_value += '<option>'+i+'</option>';
    }
  }
  $("#reminder_format_value_"+rcounter).html(reminder_format_value);
});
$(document).on('click',".addTodo_hearing",function(){
  $("#myModal").modal('show');
});