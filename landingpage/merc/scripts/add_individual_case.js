function submitIndividualCase(){
	var petArr= [];
	var resArr=[];
	var resadvArr =[];
	var petAdvArr = [];
	var court_name = $("#cases-court_id").val();
	var supreme_court = $("#supreme_court").val();
	var cases_case_type = $("#cases_case_type option:selected").val();
    // var divisional_commisioner_level = $("input[name='commissioner_level']:checked").val();
    // var ministry_desk_no = $("#mantralay_no option:selected").val();
    // var ministry_desk_no = $("#mantralay_no").val();
    var diary_number = $("#diary_number").val();
    var diary_year= $("#diary_year").val();
    var case_no = $("#case_no").val();
    var case_no_year = $("#case_no_year").val();
    var appearing_modal = $("#appearing_modal").val();
    var are_you_appearing_as = $("input[name='appearing_radio']:checked").val(); //----//are you appearing as
    var petitioner = $("#petitioner").val();
      // var respondent = $("#respondent").val();
      var dateoffilling = $("#dateoffilling").val();
      var court_hall = $("#court_hall").val();
      var floor = $("#floor").val();
      var classification = $("#classification").val();
      var title = $("#title").val();
      var nominal = $("#nominal").val();
      var nastikramank = $("#nastikramank").val();
      /*var query = CKEDITOR.instances.query.getData();*/
      // var query = CKEDITOR.instances.query.document.getBody().getText();
      var query = tinymce.get("query").getContent({ format: 'text' });
      var judge = $("#judge").val();
      var reffered_by = $("#reffered_by").val();
      var section_category = $("#section_category").val();
      var cases_priority = $("#cases-priority").val();
      var commissions = $("#commissions").val();
      var doyou_havecnr = $("input[name='membershipRadios']:checked").val();
      // var passport_radio = $("#passport_radio").val();
      var cnr = $("#name").val();
      var high_court = $("#high-court-list").val();

      var stamp_register = $("#cases-high_court_id option:selected").text();

      // var affidavite_document = document.getElementById("attach_file").files[0].name;      
      var affidavite_filling_date = $("#affidavit_filling_date").val();
      var vakalath_filling_date = $("#vakalath_filling_date").val();
      if($("#appearing_modal").is(":visible")){
      	var petetitionerArr=[],respondentArr=[];
      	var appearing = $("#appearing_modal option:selected").text().split("-");
      	if(appearing[0].trim() == are_you_appearing_as){
      		for(var i = 0;i<=petadv_count;i++){
      			var pet_adv_name = $("#your_adv_name"+i).val();
      			var pet_adv_email  = $("#your_adv_email"+i).val();
      			var pet_adv_mobile = $("#your_adv_mobile"+i).val();
      			var petVo = {
      				pet_adv_name : pet_adv_name,
      				pet_adv_email : pet_adv_email,
      				pet_adv_mobile : pet_adv_mobile
      			}
      			petAdvArr.push(petVo);
      		}
      		for(var i = 0;i <= pet_count;i++){
      			var pet_name = $("#your_name"+i).val();
      			var pet_email  = $("#your_email"+i).val();
      			var pet_mobile = $("#your_mobile"+i).val();
      			var petVo = {
      				pet_name : pet_name,
      				pet_email : pet_email,
      				pet_mobile : pet_mobile
      			}
      			petArr.push(petVo);
      		}
      		for(var i = 0;i<=res_count;i++){
      			var res_name = $("#opponent_name"+i).val();
      			var res_email  = $("#opponent_email"+i).val();
      			var res_mobile = $("#opponent_mobile"+i).val();
      			var resVo = {
      				res_name : res_name,
      				res_email : res_email,
      				res_mobile : res_mobile
      			}
      			resArr.push(resVo);
      		}

      		for(var i = 0;i<=resadv_count;i++){
      			var res_adv_name = $("#opponent_adv_name"+i).val();
      			var res_adv_email  = $("#opponent_adv_email"+i).val();
      			var res_adv_mobile = $("#opponent_adv_mobile"+i).val();
      			var resadvVo = {
      				res_adv_name : res_adv_name,
      				res_adv_email : res_adv_email,
      				res_adv_mobile : res_adv_mobile
      			}
      			resadvArr.push(resadvVo);
      		}
      	}
      	else{
      		for(var i = 0;i<=petadv_count;i++){
      			var pet_adv_name = $("#opponent_adv_name"+i).val();
      			var pet_adv_email  = $("#opponent_adv_email"+i).val();
      			var pet_adv_mobile = $("#opponent_adv_mobile"+i).val();
      			var petVo = {
      				pet_adv_name : pet_adv_name,
      				pet_adv_email : pet_adv_email,
      				pet_adv_mobile : pet_adv_mobile
      			}
      			petAdvArr.push(petVo);
      		}
      		for(var i = 0;i <= pet_count;i++){
      			var pet_name = $("#opponent_name"+i).val();
      			var pet_email  = $("#opponent_email"+i).val();
      			var pet_mobile = $("#opponent_mobile"+i).val();
      			var petVo = {
      				pet_name : pet_name,
      				pet_email : pet_email,
      				pet_mobile : pet_mobile
      			}
      			petArr.push(petVo);
      		}
      		for(var i = 0;i<=res_count;i++){
      			var res_name = $("#your_name"+i).val();
      			var res_email  = $("#your_email"+i).val();
      			var res_mobile = $("#your_mobile"+i).val();
      			var resVo = {
      				res_name : res_name,
      				res_email : res_email,
      				res_mobile : res_mobile
      			}
      			resArr.push(resVo);
      		}

      		for(var i = 0;i<=resadv_count;i++){
      			var res_adv_name = $("#your_adv_name"+i).val();
      			var res_adv_email  = $("#your_adv_email"+i).val();
      			var res_adv_mobile = $("#your_adv_mobile"+i).val();
      			var resadvVo = {
      				res_adv_name : res_adv_name,
      				res_adv_email : res_adv_email,
      				res_adv_mobile : res_adv_mobile
      			}
      			resadvArr.push(resadvVo);
      		}
      	}
      }
      else{
      	if(are_you_appearing_as == 'Petitioner'){
      		for(var i = 0;i<=petadv_count;i++){
      			var pet_adv_name = $("#your_adv_name"+i).val();
      			var pet_adv_email  = $("#your_adv_email"+i).val();
      			var pet_adv_mobile = $("#your_adv_mobile"+i).val();
      			var petVo = {
      				pet_adv_name : pet_adv_name,
      				pet_adv_email : pet_adv_email,
      				pet_adv_mobile : pet_adv_mobile
      			}
      			petAdvArr.push(petVo);
      		}
      		for(var i = 0;i <= pet_count;i++){
      			var pet_name = $("#your_name"+i).val();
      			var pet_email  = $("#your_email"+i).val();
      			var pet_mobile = $("#your_mobile"+i).val();
      			var petVo = {
      				pet_name : pet_name,
      				pet_email : pet_email,
      				pet_mobile : pet_mobile
      			}
      			petArr.push(petVo);
      		}
      		for(var i = 0;i<=res_count;i++){
      			var res_name = $("#opponent_name"+i).val();
      			var res_email  = $("#opponent_email"+i).val();
      			var res_mobile = $("#opponent_mobile"+i).val();
      			var resVo = {
      				res_name : res_name,
      				res_email : res_email,
      				res_mobile : res_mobile
      			}
      			resArr.push(resVo);
      		}

      		for(var i = 0;i<=resadv_count;i++){
      			var res_adv_name = $("#opponent_adv_name"+i).val();
      			var res_adv_email  = $("#opponent_adv_email"+i).val();
      			var res_adv_mobile = $("#opponent_adv_mobile"+i).val();
      			var resadvVo = {
      				res_adv_name : res_adv_name,
      				res_adv_email : res_adv_email,
      				res_adv_mobile : res_adv_mobile
      			}
      			resadvArr.push(resadvVo);
      		}
      	}
      	else{
      		for(var i = 0;i<=petadv_count;i++){
      			var pet_adv_name = $("#opponent_adv_name"+i).val();
      			var pet_adv_email  = $("#opponent_adv_email"+i).val();
      			var pet_adv_mobile = $("#opponent_adv_mobile"+i).val();
      			var petVo = {
      				pet_adv_name : pet_adv_name,
      				pet_adv_email : pet_adv_email,
      				pet_adv_mobile : pet_adv_mobile
      			}
      			petAdvArr.push(petVo);
      		}
      		for(var i = 0;i <= pet_count;i++){
      			var pet_name = $("#opponent_name"+i).val();
      			var pet_email  = $("#opponent_email"+i).val();
      			var pet_mobile = $("#opponent_mobile"+i).val();
      			var petVo = {
      				pet_name : pet_name,
      				pet_email : pet_email,
      				pet_mobile : pet_mobile
      			}
      			petArr.push(petVo);
      		}
      		for(var i = 0;i<=res_count;i++){
      			var res_name = $("#your_name"+i).val();
      			var res_email  = $("#your_email"+i).val();
      			var res_mobile = $("#your_mobile"+i).val();
      			var resVo = {
      				res_name : res_name,
      				res_email : res_email,
      				res_mobile : res_mobile
      			}
      			resArr.push(resVo);
      		}

      		for(var i = 0;i<=resadv_count;i++){
      			var res_adv_name = $("#your_adv_name"+i).val();
      			var res_adv_email  = $("#your_adv_email"+i).val();
      			var res_adv_mobile = $("#your_adv_mobile"+i).val();
      			var resadvVo = {
      				res_adv_name : res_adv_name,
      				res_adv_email : res_adv_email,
      				res_adv_mobile : res_adv_mobile
      			}
      			resadvArr.push(resadvVo);
      		}
      	}
      }


      var assign_to = $("#framework").val();
      var assign_to_array = [];
      $.each(assign_to,function(a,obj){
      	assign_to_array.push(obj);
      });
if(court_name == ''){
	toastr.error("","Please select court name",{timeout:5000});
	return false;
}

var caseVo = new Object();
caseVo.court_name = court_name;
// caseVo.divisional_commisioner_level =divisional_commisioner_level;
// caseVo.ministry_desk_no = ministry_desk_no;
caseVo.supreme_court = supreme_court;
caseVo.cases_sc_type = cases_case_type !='' ? $("#cases_case_type option:selected").text() : (cases_case_type == '0' ? $("#sc_casetype_other").val(): "");
caseVo.cases_sc_typeId = cases_case_type !='' ? $("#cases_case_type").val() : "" ;
caseVo.diary_number = diary_number;
caseVo.diary_year = diary_year;
caseVo.case_no = case_no;
caseVo.case_no_year = case_no_year;
caseVo.appearing_modal = appearing_modal;
caseVo.are_you_appearing_as = are_you_appearing_as;
caseVo.petitioner = petitioner;
// caseVo.respondent = respondent;
caseVo.dateoffilling = dateoffilling;
caseVo.court_hall = court_hall;
caseVo.floor = floor;
caseVo.classification = classification;nominal
caseVo.title = title;
caseVo.nominal = nominal;

caseVo.query = query;
caseVo.judge = judge;
caseVo.reffered_by = reffered_by;
caseVo.section_category = section_category;
caseVo.cases_priority = cases_priority;
if(court_name == '1'){
	if(caseVo.supreme_court == ''){
		$("#supreme_court").focus();
		toastr.error("","Please Select case number / diary number",{timeout:5000});
		return false;
	}
	if(caseVo.supreme_court=='Case Number' && caseVo.cases_sc_type == ''){
		$("#cases_case_type").focus();
		toastr.error("","Please Select case type",{timeout:5000});
		return false;
	}
	if(caseVo.case_no == '' && $("#case-div").css('display') == 'block'){
		toastr.error("","Please enter case number",{timeout:5000});
		return false;
	}
}

// caseVo.passport_radio = passport_radio;
if(court_name == '2'){
	caseVo.high_court = $("#high-court-list").val() != '' ? $("#high-court-list option:selected").text() :"";
	caseVo.bench = $("#high_court_bench_list").val() != '' ? $("#high_court_bench_list option:selected").text() : "";
	caseVo.side = $("#high_court_side_list").val() != '' ? $("#high_court_side_list option:selected").text() : "";  
	caseVo.stamp_register = stamp_register;
  caseVo.highcourt_casetype = $("#high_court_casetype").val() != '' ? $("#high_court_casetype option:selected").text() : ($("#high_court_casetype").val() == "0" ? $("#hc_casetype_other").val() : "");//
  caseVo.highcourt_casetypeId = $("#high_court_casetype").val() != '' ? $("#high_court_casetype").val() : "";
  if(doyou_havecnr == 'No'){
  	if(caseVo.high_court == ''){
  		toastr.error("","Please Select High Court",{timeout:5000});
  		return false;
  	}
  	if(caseVo.bench == ''){
  		toastr.error("","Please Select Bench",{timeout:5000});
  		return false;
  	}
  	if(caseVo.side == ''){
  		toastr.error("","Please Select Side",{timeout:5000});
  		return false;
  	}
  	if(caseVo.stamp_register == ''){
  		toastr.error("","Please Select Stamp Register",{timeout:5000});
  		return false;
  	}
  	if(caseVo.highcourt_casetype == ''){
  		toastr.error("","Please Select Case Type",{timeout:5000});
  		return false;
  	}
  	if(caseVo.case_no == ''){
  		toastr.error("","Please enter case number",{timeout:5000});
  		return false;
  	}

  }
}
else{
	caseVo.high_court = "";
	caseVo.bench = "";
	caseVo.side =  "";
	caseVo.highcourt_casetype = "";
	caseVo.stamp_register = "";
	caseVo.highcourt_casetypeId = '';
}

if(court_name == '3'){
	var masterIdState ='',masterIdCity='',masterIdEst = '',masterIdCaseType;
	caseVo.doyou_havecnr = doyou_havecnr;
	caseVo.cnr = cnr;

	caseVo.dc_state = $("#districtcourt_state").val() != '' ? $("#districtcourt_state option:selected").text() : "";
	caseVo.dc_city = $("#district_court_city").val() != '' ? $("#district_court_city option:selected").text() : "";
 caseVo.Establisment = $("#district_court_establishment").val() != '' ? $("#district_court_establishment option:selected").text() : "";//
 caseVo.district_court_casetype = $("#district_court_casetype").val() != '' ? $("#district_court_casetype option:selected").text() : ($("#district_court_casetype").val() == '0' ? $("#district_court_casetype_other").val() : "");//
 caseVo.district_court_casetypeId = $("#district_court_casetype").val() != '' ? $("#district_court_casetype").val() : "";

 if($("#districtcourt_state").val() != '')
 	masterIdState = $("#districtcourt_state").val().split("_");

 if($("#district_court_city").val() != '')
 	masterIdCity = $("#district_court_city").val().split("_");

 if($("#district_court_establishment").val() != '')
 	masterIdEst = $("#district_court_establishment").val().split("_");

 caseVo.EstablismentId = masterIdEst[1];//
 caseVo.dc_stateId =  masterIdState[1];
 caseVo.dc_cityId = masterIdCity[1];

 if(doyou_havecnr == 'No'){
  // if($("#district_court_casetype").val() != ''){
  //   masterIdCaseType = $("#district_court_casetype").val().split("_");
  //   caseVo.district_court_casetypeId = masterIdCaseType[1] ;
  // }
  if(caseVo.dc_state == ''){
  	toastr.error("","Please Select State",{timeout:5000});
  	return false;
  }
  if(caseVo.dc_city == ''){
  	toastr.error("","Please Select District",{timeout:5000});
  	return false;
  }
  if(caseVo.Establisment == ''){
  	toastr.error("","Please Select Court Establisment",{timeout:5000});
  	return false;
  }
  if(caseVo.district_court_casetype == ''){
  	toastr.error("","Please Select Case Type",{timeout:5000});
  	return false;
  }
  if(caseVo.case_no == ''){
  	toastr.error("","Please enter case number",{timeout:5000});
  	return false;
  }

}
if(doyou_havecnr == 'Yes' && cnr == ''){
	$("#name").focus();
	toastr.error("","Please Enter CNR no.",{timeout:5000});
	return false;
}
else if(doyou_havecnr == 'Yes' && cnr != '' && (cnr.length < 16 || cnr.length > 16)){
	$("#name").focus();
	toastr.error("","Please enter valid CNR no.",{timeout:5000});
	return false;
}
}
else{
	caseVo.dc_state = '';
	caseVo.dc_city = '';
	caseVo.Establisment = '';
	caseVo.doyou_havecnr = "";
	caseVo.cnr = "";
	caseVo.district_court_casetype =  '';
	caseVo.district_court_casetypeId = '';
	caseVo.dc_stateId = '';
	caseVo.dc_cityId = '';
	caseVo.EstablismentId = '';
}

caseVo.petArr = petArr;
caseVo.resArr = resArr;
caseVo.resadvArr = resadvArr;
caseVo.petAdvArr = petAdvArr;

caseVo.affidavite_filling_date = $("#affidavitBrowse").is(":visible") ? affidavite_filling_date : '';

caseVo.passport_radio = $("input[name='affidaviteRadios']:checked").val();

caseVo.vakalath_filling_date = $("#vakalathBrowse").is(":visible") ? vakalath_filling_date : '';

caseVo.vakalat_radio = $("input[name='vakalathRadios']:checked").val();

if(court_name == '7'){
	caseVo.commissionerate_state = $("#commissionerate_state").val() != '' ? $("#commissionerate_state option:selected").text() : "";
	caseVo.commisionrate_courtside = $("#commisionrate_courtside").val() != '' ? $("#commisionrate_courtside option:selected").text() : "";
	caseVo.commissionerate = $("#commissionerate_select").val() !='' ? $("#commissionerate_select").val() : '';
	caseVo.commissionerate_casetype = $("#commissionerate_casetype").val() != '' ? $("#commissionerate_casetype option:selected").text() : "";
	caseVo.commissionerate_authority = $("#authority_select").val() != '' ? $("#authority_select option:selected").text() : "";

	if(caseVo.commissionerate == ''){
		toastr.error("","Please select Commissionarate",{timeout:5000});
		return false;
	}
	if(caseVo.commissionerate == 'Charity Commissionerate' && caseVo.commissionerate_state == ''){
		toastr.error("","Please select State",{timeout:5000});
		return false;
	}
	if(caseVo.commissionerate == 'Charity Commissionerate' && caseVo.commisionrate_courtside == ''){
		toastr.error("","Please  select Court Side",{timeout:5000});
		return false;
	}

	if($("#courtsidecasetype").is(":visible") && caseVo.commissionerate_casetype == ''){
		toastr.error("","Please select Case Type",{timeout:5000});
		return false;
	}
	if(caseVo.commissionerate == 'Commissionerate of Commercial Taxes' && caseVo.commissionerate_authority == ''){
		toastr.error("","Please Select Authority",{timeout:5000});
		return false;
	}
	if(caseVo.case_no == ''){
		toastr.error("","Please enter case number",{timeout:5000});
		return false;
	}

}
else{
	caseVo.commissionerate_state = '';
	caseVo.commisionrate_courtside = '';
	caseVo.commissionerate = '';
	caseVo.commissionerate_casetype = '';
	caseVo.commissionerate_authority = '';
}

if(court_name == '4'){
	caseVo.commission = $("#commission").val() != '' ? $("#commission option:selected").text() : "";
	caseVo.commission_state = $("#commission_state").val() != '' ? $("#commission_state option:selected").text() : "";
	caseVo.commission_state_casetype = $("#commission_state_casetype").val() != '' ? $("#commission_state_casetype option:selected").text() : "";
	caseVo.commission_bench = $("#commission_bench").val() != '' ? $("#commission_bench option:selected").text() : "";

	if(caseVo.commission == ''){
		toastr.error("Please Select Commissions",{timeout:5000});
		return false;
	}
	if((caseVo.commission =='District Forum' || caseVo.commission =='State Commission')&& caseVo.commission_state == ''){
		toastr.error("Please Select Commissions State",{timeout:5000});
		return false;
	}
	if(caseVo.commission_state == 'Maharashtra' && caseVo.commission_state_casetype == ''){
		toastr.error("Please Select Commissions Case Type",{timeout:5000});
		return false;
	}
	if(caseVo.commission =='National Commission - NCDRC' && caseVo.commission_bench == ''){
		toastr.error("Please Select Commissions Bench",{timeout:5000});
		return false;
	}
	if(caseVo.case_no == ''){
		toastr.error("","Please enter case number",{timeout:5000});
		return false;
	}


}
else{
	caseVo.commission = '';
	caseVo.commission_state = '';
	caseVo.commission_state_casetype = '';
	caseVo.commission_bench = '';
}
// caseVo.commission_district = commission_district;
// caseVo.state_commission = state_commission;

if(court_name == '5'){
	caseVo.tribunal_authority = $("#tribunal_select").val() !='' ? $("#tribunal_select option:selected").text() : "";
	caseVo.tribunal_state_district = $("#tribunal_state").val() !='' ? $("#tribunal_state option:selected").text() : "" ;
	caseVo.tribunal_casetype = $("#tribunal_case").val() !='' ? $("#tribunal_case option:selected").text() : "";
	caseVo.tribunal_other = $("#tribunal_other").val() != '' ? $("#tribunal_other").val() : "";
	caseVo.tribunal_bench = $("#tribunal_bench").val() != '' ? $("#tribunal_bench option:selected").text() : "";
	if(caseVo.tribunal_authority == ''){
		toastr.error("","Please Select Tribunal Authority",{timeout:5000});
		return false;
	}
	if($("#tribunal_state").is(":visible") && caseVo.tribunal_state_district == ''){
		toastr.error("","Please Select Tribunal State",{timeout:5000});
		return false;
	}
	if(caseVo.tribunal_casetype == ''){
		toastr.error("","Please Select Case Type",{timeout:5000});
		return false;
	}
	if(caseVo.case_no == ''){
		toastr.error("","Please enter case number",{timeout:5000});
		return false;
	}
  /*if($("#other-case-type").is(":visible") && caseVo.tribunal_other == ''){
    toastr.error("","Please Enter other Case Type",{timeout:5000});
    return false;
}*/

}
else{
	caseVo.tribunal_authority = '';
	caseVo.tribunal_state_district = '';
	caseVo.tribunal_casetype = '';
	caseVo.tribunal_other = '';
	caseVo.tribunal_bench='';
}
if(court_name == '6'){
	caseVo.revenue_casetype = $("#revenue_other").val() !='' ? $("#revenue_other").val() : "";
	caseVo.revenue_states = $("#revenue_states").val() !='' ? $("#revenue_states").val() : "";

	if(caseVo.revenue_states == ''){
		toastr.error("","Please Select revenue court",{timeout:5000});
		return false;
	}
	if(caseVo.revenue_casetype == ''){
		toastr.error("","Please Select revenue Case Type",{timeout:5000});
		return false;
	}
	if(caseVo.case_no == ''){
		toastr.error("","Please enter case number",{timeout:5000});
		return false;
	}
}
else{
	caseVo.revenue_casetype = '';
	caseVo.revenue_states = '';
}
if(court_name == '9'){
	caseVo.other_casetype = $("#court_other_input").val()!='' ? $("#court_other_input").val() : "";
	if(caseVo.other_casetype == ''){
		toastr.error("","Please Enter Case Type",{timeout:5000});
	}
	if(caseVo.case_no == ''){
		toastr.error("","Please enter case number",{timeout:5000});
		return false;
	}
}

caseVo.complete_details = getcase;
caseVo.assign_to_array = assign_to_array;
caseVo.petitioner_arr = petitioner_arr;

caseVo.department_name = $("#department").val() != '' ? $("#department option:selected").text() : "";
if(nastikramank != '')
caseVo.nastikramank = nastikramank;//
else{
	toastr.error("","Please enter nastikramank",{timeout:5000});
	return false;
}


$.ajax({
	url: host+'/submit_demo.php',
	type:'POST',
	data : caseVo,
    dataType: 'json',
    async:false,
    cache : false,
    success:function(data){
    	console.log(data.caseid);
    	if(data.status == 'success'){
        getFiles(data.case_id,data.caseid);
    }
    else
    	toastr.error("",data.status,{timeout:5000});
},
error:function(e){
            // console.log(e);
        }
    });
}

function getFiles(caseid,urlId){
	var affidavite_file = '',pet_file='',vakalat_file='';
	var formData = new FormData();
	var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','xlsx','xls','doc','docx'];
	if(!isEmpty($("#pet_file").val())){
		if ($.inArray($("#pet_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
			return false;
		}
		pet_file =  $("#appearingas_input").is(":visible") ? document.getElementById("pet_file").files[0] : "";
	}
	else
		pet_file = '';

	if(!isEmpty($("#attach_file").val())){

		if ($.inArray($("#attach_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
			return false;
		}
    affidavite_file = $("#affidavitBrowse").is(":visible") ? document.getElementById("attach_file").files[0] : "";//
}
else if($("#affidavitBrowse").is(":visible") && isEmpty($("#attach_file").val())){
	toastr.error("","Please Select affidavite document",{timeout:5000});
	return false;
}

if(!isEmpty($("#vakalat_file").val())){

	if ($.inArray($("#vakalat_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
		return false;
	}
    vakalat_file = $("#vakalathBrowse").is(":visible") ? document.getElementById("vakalat_file").files[0] : "";//
}
else if($("#vakalathBrowse").is(":visible") && isEmpty($("#vakalat_file").val())){
	toastr.error("","Please Select vakalat document",{timeout:5000});
	return false;
}




formData.append("pet_file",pet_file);
formData.append("affidavite_document",affidavite_file);
formData.append("vakalat_document",vakalat_file);
formData.append("case_id",caseid);

var ajaxResult = $.ajax({
	url : host+"/submit_files.php",
	type : 'POST',
	data : formData,
	cache : false,
	contentType : false,
	processData : false,
	success:function(response){
		var response = JSON.parse(response);
		if(response.status == 'success')
			window.location.href='view-case.php?caseid='+ urlId;
	},
	error:function(){
		toastr.error("","Error in adding case",{timeout:5000});
	}
});
}
