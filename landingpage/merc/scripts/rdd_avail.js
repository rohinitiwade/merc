$(document).ready(function(){
	$("#matter").select2();
  $("#demo").select2();
  tinymce.init({
    selector : "#query",
    plugins : [
    "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });
  // $("#state").select2();
  // $("#district").select2();
  // $("#court").select2();
  $("#dept").select2();
  $("#litigationid").hide();
  changeLanguage('English');
  getDocumentType();
  function getDocumentType(){  
  	$.ajax({
  		type:'POST',
  		url:host+'/ajaxdata/document_typeapi.php',
  		success:function(response){
  			console.log(response);
  			var response = JSON.parse(response);
  			var option='<option value="">---- Select Case Type ----</option>';
  			$.each(response,function(i,obj){
  				option +='<option value="'+obj.doc_id+'">'+obj.doc_type+'</option>';
  			});
  			$("#document_typeapi").html(option);
  		},
  		error:function(){
  			toastr.error("","Error fetching city",{timeout:5000});
  		}
  	}); 
  }
  function ckr(){
    tinymce.get('query').setContent('');
        // CKEDITOR.instances.query.setData('');
      } 
      $("#state").on('change',function(){ 
       var val = $(this).val().split("_");
       var val_to_php = val[0];
       var val_to_api = val[1];
  // $("#dist_code").val(val);
  $.ajax({
  	type:'POST',
  	url:host+'/ajaxData.php',
  	data:'country_id='+val_to_php,
  	success:function(response){

  		$('#district').html(response);
  		$('#district_court_district').html('<option value="">Select District first</option>'); 
  	},
  	error:function(){
  		toastr.error("","Error fetching city",{timeout:5000});
  	}
  }); 
});

      $("#district").on('change',function(){
       var val = $(this).val().split("_");
       var val_to_php = val[0];
       var val_to_api = val[1];
       $("#dist_code").val(val_to_api);
       $.ajax({
        type:'POST',
        url:host+'/establisment.php',
        data:'estais_id='+val_to_php,
        success:function(response){
         $('#court').html(response);

      // $('#district_court_district').html('<option value="">Select District first</option>'); 
    },
    error:function(){
     toastr.error("","Error fetching establisment",{timeout:5000});
   }
 }); 
     });
      $("#document_typeapi").on('change',function(){
       var doc_list='';
       var val = $(this).val();
       $.ajax({
        type:'POST',
        url:host+'/ajaxdata/subdocuemnt_api.php',
        data:'doc_id='+val,
        success:function(response){
         var response = JSON.parse(response);
         $.each(response,function(i,obj){
          i++;
          doc_list +='<div class="input-group form-group" id="copy_of_petition">';
          doc_list+='<div class="col-sm-4"> <label><b>'+i+') </b>'+obj.sub_doc_type+'</label></div>';
          doc_list+='<input type="file" id="pet-file-'+i+'" class="document_upload" aria-label="File browser example" onchange=submitDoc('+i+',"'+obj.sub_doc_id+'",this.id)>';
          doc_list+='<button class="btn btn-sm btn-primary" type="button" id="'+obj.sub_doc_type+'" onclick=writePetition("'+obj.sub_doc_id+'",this.id)> <i class="fa fa-plus" aria-hidden="true"></i></button>';
          doc_list+='</div><div id="'+obj.sub_doc_id+'"></div>';
        });
         $("#attach_document_div").html(doc_list);
       },
       error:function(){
         toastr.error("","Error fetching document list",{timeout:5000});
       }
     }); 
     });
    });
var con = 1;
function writePetition(sub_doc_id,sub_doc_type){        
	con++;
	var multi_doc ='<div id="passportappendDiv_'+con+'">';
	multi_doc+='<div class="input-group form-group" id="">';
	multi_doc+='<div class="col-sm-4"><label></label></div>';
	multi_doc+='<input type="file" id="pet-file-'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange=submitDoc('+con+','+sub_doc_id+',this.id)>';
	multi_doc+='<button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> ';
	multi_doc+='<i class="fa fa-minus" aria-hidden="true"></i></button></div></div>';
	$('#'+sub_doc_id).append(multi_doc);
      /*if(appendchange == "copy_petition"){
        var e = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.passdiv').append(e);    

      }
      else if(appendchange == "Annexure"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.Annexurediv').append(copy_petition);    
      }

      else if(appendchange == "para-wise"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.para-wisediv').append(copy_petition);    
      }
      else if(appendchange == "written"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.writtendiv').append(copy_petition);    
      }
      else if(appendchange == "annexure_attached"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.annexure_attacheddiv').append(copy_petition);    
      }
      else if(appendchange == "affidatits"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.affidatitsdiv').append(copy_petition);    
      }
      else if(appendchange == "lower"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.lower_courtdiv').append(copy_petition);    
      }
      else if(appendchange == "interim"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.interimdiv').append(copy_petition);    
      }
      else if(appendchange == "vakalathnama"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.vakalathnamadiv').append(copy_petition);    
      }
      else if(appendchange == "other"){
        var copy_petition = $('<div id="passportappendDiv_'+con+'"><div class="input-group form-group" id=""><div class="col-sm-3"><label></label></div><input type="file" id="pet_file_'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange="submitDoc('+con+')"><button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> <i class="fa fa-minus" aria-hidden="true"></i></button></div></div>');
        $('.otherdiv').append(copy_petition);    
      }*/

    }

    function removePassport(con){
     document.getElementById("passportappendDiv_"+con).outerHTML="";
   }
   function submitDoc(cnt,sub_id,sub_type){
     debugger;
      // var subType = sub_type.split("_");
      var caseid = $("#case_id").val();
      var doc_type_id = $("#document_type").val();
      var doc_type = $("#document_type option:selected").text();
      var getdoc = '';
      // getdoc = $(this).val();
      var getdoc = document.getElementById(sub_type).files[0];
      var formData = new FormData();
      formData.append("document",getdoc);
      formData.append("case_id",caseid);
      formData.append("doc_type",doc_type);
      formData.append("doc_id",doc_type_id);
      // formData.append("sub_doc_type",subType[1])
      formData.append("sub_doc_id",sub_id);
      var ajaxResult = $.ajax({
      	url : host+"/submit_rdddocument.php",
      	type : 'POST',
      	data : formData,
      	dataType: 'json',
      	cache : false,
      	contentType : false,
      	processData : false,
        // beforeSend : function(){
        //   $(".flip-square-loader").show();
        // },
        success:function(response){
         // var json_status = JSON.parse(response.status);
         if(response.status == 'success'){
         	toastr.success("","File Uploaded Successfully",{timeout:5000});
         }
        // $("#"+sub_type).val('');
      },
      // complete:function(){
      //   $(".flip-square-loader").hide();
      // },
      error:function(){
      	toastr.error("","Error in Uploading File",{timeout:5000});
      }
    });
    }
    function litigationfunction(checkBox){

     // var checkBox = $("input[name='litigation']:checked");
     if(checkBox == 'Yes'){
        // dropdn.style.display="block";
        $("#litigationid").show();
      }
      else
        $("#litigationid").hide();
     
    }
    $(document).ready(function(){
     $('input[class="cb"]').on('change', function() {
      $('input[class="cb"]').not(this).prop('checked', false);
    });
   });
/*    function nonlitigationfunction(){
     var checkBox= document.getElementById("nl");
     var dropdn= document.getElementById("drp");
     if (checkBox.checked == true) {

           // $('#demo').multiselect('deselectAll', true);
           // $('#demo').multiselect('destroy');
           dropdn.style.display="none";
           
         }
       }*/

       function submitRdd(){
        var matter = $("#matter").val();
        var case_id = $("#case_id").val();
        var enc_caseid = $("#enc_caseid").val();
        var user = $("#email").val();
        var firstname = $("#fname").val();
        var lastname = $("#lname").val();
        var contact = $("#mobile").val();
        var subject_other = $("#subject_other").val();
        var state = $("#state").val();
        // var splitstate = state.split('_')[0];
        var district = $("#district").val();
        // var splitdistrict = district.split('_')[0];
        var case_no = $("#case_no").val();
        var court = $("#court").val();
        var dept = $("#dept").val();
        var litigation = $("#litigation").val();
        var demo = $("#demo").val();
        var non_litigation = $("#non_litigation").val();
        var query = $('#query_ifr').contents().find('body').text();
        if(isEmpty(query)){
        	toastr.error("Please Enter Instruction","Required",{timeout:5000});
          return false;
        }
        
        var rddVo = new Object();
        rddVo.matter = matter;
        rddVo.case_id = case_id;
        rddVo.email = email;
        rddVo.subject_other = subject_other;
        // rddVo.state = splitstate;
        // rddVo.district = splitdistrict;
        rddVo.case_no = case_no;
        rddVo.court = court;
        rddVo.dept = dept;
        rddVo.litigation = litigation;
        rddVo.demo = demo;
        rddVo.non_litigation = non_litigation;
        rddVo.query = query;
        var documentTypeArray= [];
        var QueryVo=new Object();
        QueryVo.user=user;
        QueryVo.firstname=firstname;
        QueryVo.lastname=lastname;
        QueryVo.contact=contact;
        var CaseRegisterVO=new Object();  
        CaseRegisterVO.casetitleId=case_id;
        CaseRegisterVO.queryVo=QueryVo;
        // CaseRegisterVO.email=email;
        CaseRegisterVO.user=user;
        CaseRegisterVO.caseTitle=matter;
        // CaseRegisterVO.user=user;
        // CaseRegisterVO.firstname=firstname;
        // CaseRegisterVO.lastname=lastname;
        // CaseRegisterVO.contact=contact;
        var ajaxResult = $.ajax({
        	url : host+"/submit_rdd.php",
        	type : 'POST',
        	data : JSON.stringify(rddVo),      
        	dataType:"json",
        	cache : false,
        	contentType : false,
        	processData : false,
        	success:function(response){
           // console.log(response);
           if(response.status == 'success'){
           	$.ajax({
           		type: "POST",
           		url: "ajaxdata/gettitle.php",
           		data: "caseid="+case_id,
           		cache: false,
           		async:false,
           		success: function(data){
           			var datatitle = JSON.parse(data);
           			var RddCaseDetailVO= new Object();
                RddCaseDetailVO.caseType= datatitle.case_type;
                RddCaseDetailVO.caseNumber= datatitle.case_no;
                RddCaseDetailVO.caseYear= datatitle.case_no_year;
                RddCaseDetailVO.caseTitleId= datatitle.case_id;
                RddCaseDetailVO.subject= datatitle.case_name;
                RddCaseDetailVO.dairyNo= datatitle.diary_no;
                RddCaseDetailVO.dairyYear= datatitle.diary_year;
                RddCaseDetailVO.court= datatitle.court_name;
                RddCaseDetailVO.state= datatitle.state;
                RddCaseDetailVO.district= datatitle.district;
                RddCaseDetailVO.courtEstablishment= datatitle.court_establishment;
                RddCaseDetailVO.bench= datatitle.bench;
                RddCaseDetailVO.side= datatitle.side;
                RddCaseDetailVO.register= datatitle.hc_stamp_register;
                RddCaseDetailVO.commissionerate= datatitle.commissionerate;
                RddCaseDetailVO.authority= datatitle.tribunals;
                RddCaseDetailVO.consumerForum= datatitle.commissions;
                RddCaseDetailVO.createBy= datatitle.email;
                RddCaseDetailVO.petioner= datatitle.petitioner;
                RddCaseDetailVO.respondent= datatitle.respondent;
                CaseRegisterVO.rddCaseDetailVO= RddCaseDetailVO;

                $.ajax({
                 type: "POST",
                 url: "ajaxdata/getmatter.php",
                 data: 'caseid='+case_id+'',
                 async:false,
                 success: function(result){
                 // console.log(result);
                 var dataobject=JSON.parse(result);
                 $.each(dataobject,function(i,obj){
                 	var DocumentTypeVO=new Object();
                 	DocumentTypeVO.documentTypeId=obj.documentTypeId;
                 	DocumentTypeVO.documentTypeCode=obj.documentTypeCode;
                 	DocumentTypeVO.instruction=obj.instruction;
                 	$.ajax({
                 		type: "POST",
                 		url: "ajaxdata/getdocuments.php",
                 		data: 'caseid='+case_id+'&&doctype='+obj.documentTypeCode,
                 		async:false,
                 		success: function(results){
                 			var datadoc=JSON.parse(results);
                 			var fileTypeArray= [];                                  
                 			$.each(datadoc,function(i,objs){
                 				var FileTypeVO=new Object();
                 				FileTypeVO.fileid=objs.fileid;
                 				FileTypeVO.filename=objs.filename;
                 				FileTypeVO.fileUrl=objs.document;
                 				fileTypeArray.push(FileTypeVO);
                        // console.log(fileTypeArray); 
                      });
                 			DocumentTypeVO.fileTypeVOs=fileTypeArray;                       
                 		}
                 	});
                 	documentTypeArray.push(DocumentTypeVO); 
                 });
               }
             });
                CaseRegisterVO.documentTypeVOs=documentTypeArray;
                console.log(CaseRegisterVO);
                $.ajax({
                  url: legalResearch+"/addprocessdata",
            // url:"http://192.168.0.56:8080/LegalResearch/addprocessdata",
            // url:legalResearch+"/addprocessdata",
            type:'POST',
            data: JSON.stringify(CaseRegisterVO),
            dataType: 'json',
            async:false,
            headers: {
            	"Content-Type": 'application/json'
            },
            beforeSend : function(){
            	$(".flip-square-loader").show();
            },
            success:function(data){ 
              // console.log(data);
              // var strerror = data.error;
            // var nerror = strerror.includes("Duplicate");
              // var error = "CallableStatementCallback; SQL [{call proc_rdd_add_casetitle_data(?, ?, ?, ?, ?)}Duplicate entry '12' for key 'PRIMARY'; nested exception is com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry '12' for key 'PRIMARY'";
              if(data.error == "SUCCESS")
              {
              	toastr.success("","Matter Proceed Successfully", {timeOut: 5000})
                // alert("Matter Proceed Successfully");
                // var casedata = data.caseid;
                window.location.href='my_cases.php?caseid='+ enc_caseid;
              // document.getElementById("process_btn").disabled = true;
              // $("#process_btn").attr('disabled',true);
            }
            else if(data.error.includes('Duplicate entry'))
              toastr.error("","Already Processed this case",{timeout:5000});
            else
              toastr.error("","Error in matter process",{timeout:5000});
          // else if(nerror == true){
          // 	toastr.error("","Duplicate entry",{timeout:5000});
          // }
          //   // 
            // if(nerror == true){
            //   toastr.error("","Duplicate entry",{timeout:5000});
            // }
            
          },
          complete:function(){
           $(".flip-square-loader").hide();
         },
         error:function(){
           toastr.error("","Error in Uploading File",{timeout:5000});
         }
       });
              }
            });
}
},
error:function(){
	toastr.error("","Error in adding case",{timeout:5000});
}
});
}

function changeLanguage(language){
	tinymce.init({
		selector : "#query",
		plugins : [
		"wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });

	if(language == 'Marathi'){
		google.setOnLoadCallback(OnLoad);
		OnLoad();
	}
}

google.load("elements", "1", {packages: "transliteration"});

function OnLoad() { 
	var options = {
		sourceLanguage:
		google.elements.transliteration.LanguageCode.ENGLISH,
		destinationLanguage:
		[google.elements.transliteration.LanguageCode.MARATHI],
		shortcutKey: 'ctrl+g',
		transliterationEnabled: true
	};  
	var control = new google.elements.transliteration.TransliterationControl(options);
	control.makeTransliteratable(["query_ifr"]);

} //end onLoad function

$("#matter").on('change',function(){
  var mat = $("#matter").val();
  if(mat == 'Other'){
   $("#other_label").show();
   // $("#other_div").show();
 }
});