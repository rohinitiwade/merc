// var urlParams = new URLSearchParams(window.location.search);
// var urlflag = urlParams.get('flag'); 
// console.log($.urlParam('flag'));

var storedCaseId = JSON.parse(localStorage.getItem("caseidArr"));
// console.log(storedCaseId);
 var urlflag = $("#flag").val();
if(!isEmpty(urlflag)){
  showDocument(storedCaseId);
}

// $('#merc_by').select2({
//     placeholder: 'Select...',
//     closeOnSelect: false,
//     multiple: true,
//     allowClear: false
//   });


// $('#merc_against').select2({
//     placeholder: 'Select...',
//     closeOnSelect: false,
//     multiple: true,
//     allowClear: false
//   });
$("input[name='appearing_radio']").change(function(){
  if($("#appearing_radio_one").is(":checked") ){
    $("#respondent_div").show();
    $("#petitioner_div").hide();
  }else if($("#appearing_radio_two").is(":checked") ){
    $("#petitioner_div").show();
    $("#respondent_div").hide();
  }
});

$( "#dateoffilling" ).datepicker({
  dateFormat: 'yy/mm/dd',
  changeYear: true,
  changeMonth: true,
  yearRange: "-100:+0"
});

$(document).ready(function(){
  $("#petitioner_div").hide();
  $("#merc_by").select2();
  $("#case_no_year").select2();
  $("#subject_matter").select2();
 // $("#merc_by").hide();
       // $("#merc_against").hide();
       $("#merc_by").change(function(){
        var courtVal = $(this).val();
        if(courtVal == 'Others'){
          $("#merc_by").next(".select2-container").hide();
          $("#court_other").show();
        }
       });
     });
function litigationfunction(checkBox){ 
  if(checkBox == 'Yes'){

    $("#merc_against_id").hide();
    $("#merc_by_id").show();
  }
}


var getcase='';
var pet_count=1,res_count=1;
var pet_counter = 1,res_counter = 1;

function addPetitioner(){
  pet_counter = pet_count;
  var val = $("input[name='appearing_radio']:checked").val(),cols='';
  // if(appearing_top_array[0] == val){
    cols += '<tr id="pet-row-'+pet_counter+'"><td><input type="text" class="form-control" id="your_name'+pet_counter+'" name="name' + pet_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="your_email'+pet_counter+'" name="mail' + pet_counter + '"/></td>';
    cols += '<td><input type="text" class="form-control" id="your_mobile'+pet_counter+'" name="phone' + pet_counter + '"/></td>';
    cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pet_counter+',"pet-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
  // }
  // else{

    // cols += '<tr id="pet-row-'+pet_counter+'"><td><input type="text" class="form-control" id="opponent_name'+pet_counter+'" name="name' + pet_counter + '"/></td>';
    // cols += '<td><input type="text" class="form-control" id="opponent_email'+pet_counter+'" name="mail' + pet_counter + '"/></td>';
    // cols += '<td><input type="text" class="form-control" id="opponent_mobile'+pet_counter+'" name="phone' + pet_counter + '"/></td>';
    // cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+pet_counter+',"pet-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
  // }
  pet_counter++;
  pet_count = pet_counter;
  $("#petitioner_table tbody").append(cols);
}


function addRespondant(){
  res_counter = res_count ;

  var val = $("input[name='appearing_radio']:checked").val(),cols='';
    // if(appearing_top_array[0] == val){
      cols += '<tr id="res-row-'+res_counter+'"><td><input type="text" class="form-control" id="opponent_name'+res_counter+'" name="name' + res_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="opponent_email'+res_counter+'" name="mail' + res_counter + '"/></td>';
      cols += '<td><input type="text" class="form-control" id="opponent_mobile'+res_counter+'" name="phone' + res_counter + '"/></td>';
      cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_counter+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
    // }
    // else{
      // cols += '<tr id="res-row-'+res_counter+'"><td><input type="text" class="form-control" id="your_name'+res_counter+'" name="name' + res_counter + '"/></td>';
      // cols += '<td><input type="text" class="form-control" id="your_email'+res_counter+'" name="mail' + res_counter + '"/></td>';
      // cols += '<td><input type="text" class="form-control" id="your_mobile'+res_counter+'" name="phone' + res_counter + '"/></td>';
      // cols += '<td><a class="btn btn-danger btn-sm ibtnDel" onclick=deleteRow('+res_counter+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></a></td></tr>';
    // }
    res_counter++;
    res_count = res_counter;
    $("#repondant_table tbody").append(cols);

  };
  function deleteRow(count,id){
   document.getElementById(id+"-"+count).outerHTML = ''

 }




 function submitMerc(){
   var petArr= [];
   var resArr=[];
   var tempidArr = [];
   $(".add_casesubmitdiv").addClass("fetchdataloader");
   $(".add_casesubmit").hide();


   if ($("input[name='appearing_radio']").is(":checked")) {
    var mercradio_val = $("input[name='appearing_radio']:checked").val();
  }

  var merc_by = $("#merc_by").val();
  if(merc_by == 'Others'){
    var court_forum = $("#court_other").val();
  }
  else{
    var court_forum = $("#merc_by").val();
  }
   var temp_caseid = $("#temp_case_id").val();
  var merc_against = $("#merc_against").val();

  var case_number = $("#case_number").val();
  var case_no_year = $("#case_no_year").val();
  var dateoffilling = $("#dateoffilling").val();
  var risk_category = $("#risk_category").val();
  var subject_matter= $("#subject_matter").val();
  var title = $("#title").val();
  var desc = tinyMCE.get('query').getContent();



//  formData.append("upload",upload);
//   if(client_name != '')
//  formData.append("client_name",client_name);
// else{
//   toastr.error("","Please Enter Client Name",{timeout:5000});
//   return false;
// }
// if(merc_by !='')
//  var court_name = merc_by;
// else if(merc_against !='')
//  var court_name = merc_against;

// if(court_name == ''){
//   toastr.error("","Please select Court/ Forum",{timeout:5000});
//    $(".add_casesubmitdiv").removeClass("fetchdataloader");
//     $(".add_casesubmit").show();
//   return false;
// }

// if(mercradio_val == ''){
//   toastr.error("","Please select Type of Litigation",{timeout:5000});
//    $(".add_casesubmitdiv").removeClass("fetchdataloader");
//     $(".add_casesubmit").show();
//   return false;
// }

// if(title == ''){
//   toastr.error("","Please enter title",{timeout:5000});
//    $(".add_casesubmitdiv").removeClass("fetchdataloader");
//     $(".add_casesubmit").show();
//   return false;
// }
for(var i = 0;i <= pet_count;i++){
  var pet_name = $("#your_name"+i).val();
  var pet_email  = $("#your_email"+i).val();
         /*var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
         if(!emailReg.test(pet_email))
         {
           alert('Please enter a valid respondent email address.');
           return false;
         }*/

         var pet_mobile = $("#your_mobile"+i).val();
         var petVo = {
          pet_name : pet_name,
          pet_email : pet_email,
          pet_mobile : pet_mobile
        }
        petArr.push(petVo);
      }
  // var res_count = $("#res_count").val();
  for(var i = 0;i<=res_count;i++){
    var res_name = $("#opponent_name"+i).val();
    var res_email  = $("#opponent_email"+i).val();
    //var emailReg = /^([w-.]+@([w-]+.)+[w-]{2,4})?$/;
   /*if(!emailReg.test(res_email))
   {
     alert('Please enter a valid email address.');
     return false;
   }*/

   var res_mobile = $("#opponent_mobile"+i).val();
   var resVo = {
    res_name : res_name,
    res_email : res_email,
    res_mobile : res_mobile
  }
  resArr.push(resVo);

  
}
 var tempcaseisArr = [];
  tempcaseisArr.push(storedCaseId);
  var data = {
    tempcaseisArr : storedCaseId
  }

var formData = new FormData();
formData.append("mercradio_val",mercradio_val);
formData.append("court_name",court_forum);
 // formData.append("merc_against",merc_against);
 formData.append("case_number",case_number);
 formData.append("case_no_year",case_no_year);
 formData.append("dateoffilling",dateoffilling);
 formData.append("title",title);
 formData.append("risk_category",risk_category);
 formData.append("desc",desc);

 formData.append("petArr",JSON.stringify(petArr));
 formData.append("subject_matter",subject_matter);

 formData.append("resArr",JSON.stringify(resArr));
 formData.append("urlflag",urlflag);
 formData.append("storedCaseId",JSON.stringify(storedCaseId));

 var ajaxResult = $.ajax({
  url : host+"/submit_merc.php",
  type : 'POST',
  data : formData,
  cache : false,
  contentType : false,
  processData : false,
  success:function(response){
    console.log(response);
    $(".add_casesubmitdiv").removeClass("fetchdataloader");
    $(".add_casesubmit").show();
    var responseGet = JSON.parse(response);
    if(responseGet.status == 'success'){
      toastr.success("","Case Added Successfully",{timeout:5000});
      if(urlflag == 'tempdoc'){
      window.location.href='cases-report.php';
    }else{
      window.location.href='merc_document.php?caseid='+ responseGet.caseid;
    }
    } 
    if(responseGet.status == 'error'){
      toastr.error("","Case not Added ",{timeout:5000});
    // window.location.href='merc_document.php?caseid='+ responseGet.caseid;
  } 

},
error:function(){
  $(".add_casesubmitdiv").removeClass("fetchdataloader");
  $(".add_casesubmit").show();
  toastr.error("","Error in adding case",{timeout:5000});
}
});
}


function showDocument(storedCaseId){
  debugger;
  var tempcaseisArr = [];
  tempcaseisArr.push(storedCaseId);
  var data = {
    tempcaseisArr : storedCaseId,
    flag : 'mul_addcase',
  }
  var ajaxResult = $.ajax({
    url : host+"/multipleDocumentSubmit.php",
    type : 'POST',
    data :data,
    dataType: 'json',
    success:function(response){
      console.log(response);
      var table ='';
      table +='<h5 class="col-sm-12">Document List</h5>';
      table += '<table class="table table-bordered main-table" id="main-table"><thead>';
      table +='<tr>';
      table += '<th>Temporary Case Id.</th>';
      table += '<th>Document Name</th>';
      table += '<th>Document Type</th>';
      table += '<th>Upload date</th>';

      table += '</tr></thead><tbody>';
      $.each(response.data,function(i,obj){
        i++;
        table += '<input type="hidden" name="temp_case_id" id="temp_case_id" value="'+obj.case_id+'"><tr>';
        table += '<td>'+obj.case_id+'</td>';
        table += '<td>'+obj.doc_path+'</td>';
        table += '<td>'+obj.type+'</td>'; 
        table += '<td>'+obj.date_time+'</td>'; 
        
      });
      table +='</tbody></table>';
      $("#document-table").html(table);
    },
    error:function(){
      $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
      toastr.error("","Error in adding case",{timeout:5000});
    }
  });
}

