
getCompetantAuthority();
	// getDeskNo();
	getMinistry();
	$(document).ready(function(){
		getEditQuasi();
		$("#other_competant_authority").hide();
		$("#other_district").hide();
	});

	$('#division').on('change', function(){
		var division = $(this).val();

		if(division){
			$.ajax({
				type:'POST',
				async:false,
				cache:false,
				url:'quasi_district_list.php',
				data:'division_id='+division,
				success:function(html){
					$('#district').html(html);
				},
				error:function(e){
					toastr.error("Error","Unable to load district list",{timeout:5000});
				}
			}); 
		}
	});

	function getEditQuasi(){
		var caseid = $("#caseid").val();
		$.ajax({
			type:'POST',
			url:'edit_quasiurl.php',
			data:'case_id='+caseid,
			async:false,
			cache:false,
			success:function(response){
				console.log(response);
				var response = JSON.parse(response);
				$.each(response.data,function(i,obj){
					getDropDownSelected(obj.competant_authority,"competant_authority");
					getDropDownSelected(obj.ministry,"ministry_name");
					$("#zp_name").val(obj.desk_number);
					getDropDownSelected(obj.division,"division");
					getDropDownSelected(obj.zp_name,"district");
					$("input[name='commissioner_level'][value='"+obj.div_comm_level+"']").prop("checked",true);
					$("#department").val(obj.department);
					$("#subject").val(obj.subject);
					$("#filing_date").val(obj.date_of_filling);
					$("#case_type").val(obj.case_type);
					$("#case_no").val(obj.case_no);
					$("#case_year").val(obj.case_no_year);
					$("#case_title").val(obj.case_title);
					$("#stage").val(obj.Stage);
					$("#hearing_date").val(obj.hearing_date);
					$("#next-stage").val(obj.hearing_nextstage);
					$("#pet_name").val(obj.pet_name);
					$("#pet_address").val(obj.pet_address);
					$("#pet_email").val(obj.pet_email_id);
					$("#pet_mobile").val(obj.pet_mobile);
					$("#resp_name").val(obj.res_name);
					$("#resp_address").val(obj.res_address);
					$("#resp_email").val(obj.res_email_id);
					$("#resp_mobile").val(obj.res_mobile);
				})

			},
			error:function(){
				toastr.error("","Error fetching appearing",{timeout:5000});
			}
		}); 
	}
	function getMinistry(){
		$.ajax({
			type:'POST',
			url:host+'/ajaxdata/judicial_matter.php',
			dataType:"json",
			async:false,
			success:function(response){
				console.log("judicial_matter",response);
				var option = '<option value="">Select</option>';
				$.each(response,function(i,obj){
					option +='<option value="'+obj.id+'">'+obj.ministry_name+'</option>';
				});
				$("#ministry_name").html(option);
				$("#ministry_name").select2();
			},
			error:function(){
				toastr.error("","Error in fetching competant authority",{timeout:5000});
			}
		});
	}
	function getDropDownSelected(courtname,courtId){
	/*var court = document.getElementById('cases-court_id');
	setSelectedValue(court,courtname); */
	var abcd = $("#"+courtId).val($("#"+courtId+" option:contains("+courtname+")").val());
	// $("#"+courtId).select2("val", $("#"+courtId+" option:contains("+courtname+")").val());
	$("#"+courtId).select2().trigger('change');
}


function getCompetantAuthority(){	
	$.ajax({
		type:'POST',
		url:host+'/ajaxdata/competant_authority.php',
		dataType:"json",
		async:false,
		success:function(response){
			console.log(response);
			var option = '<option value="">Select</option>';
			$.each(response,function(i,obj){
				option +='<option value="'+obj.comps_auto_id+'">'+obj.competant_authority+'</option>';
			});
			$("#competant_authority").html(option);
			$("#competant_authority").select2();
		},
		error:function(){
			toastr.error("","Error in fetching competant authority",{timeout:5000});
		}
	});
}