var cnr='',case_no='',color='',court_id_val='',getcase='',autoupdate,caseid='',endate=0,hearing_date,complete_data_report='';
$("#inform-to-client-tbl").hide();
$(".case-activity-info").hide();
$("#who_attended_others_div").hide();
$("#team-member-table-div").show();
$("#documentList").show();

var teamListArray = '';
$(document).ready(function(){
  function deletecase (id) {
    // debugger;
   // var yes = confirm('All the documents, To-does, Notes, Timesheet Etc. associated to this case will be deleted Are you sure? this action cant be undone.');
   if (window.confirm("All the documents, To-does, Notes, Timesheet Etc. associated to this case will be deleted Are you sure? this action cant be undone.")) { 
      // window.open("exit.html", "Thanks for Visiting!"); 
      $.ajax({
        type:'POST',
        url:'delete_cases.php',
        data:'case_id='+caseid,
        success:function(html){
          alert("Case Removed Successfully");
          window.location.href='cases-report.php';
       // $('#delcase').html(html);
     }
   }); 
    }
  }view_case_details();
   getUploadedDoc('d');
 /* debugger;
  $(".tab-pane").removeClass("active show");
  $(".nav-link").removeClass("active");
  if(localStorage.getItem("findcaselaw") == 'FCL'){
    $("#connected-tab").addClass("active");
    $("#cases-referred").addClass("active show");
    localStorage.setItem("findcaselaw","");
  }
  else{
    $("#activity-tab").addClass("active");
    $("#activity-history").addClass("active show");
  }*/
  $("#caseactivityinfo-next_hearing").datepicker({
   dateFormat: 'yy/mm/dd',
   changeYear: true,
   changeMonth: true
 });
});

 function view_case_details(getFlag){
  // debugger;
  caseid = $("#caseid").val();
  // court_id = $("#court-id").val();
  $.ajax({
    url: host+'/view_noncaseurl.php',
    type:'POST',
    data :"case_id="+caseid,
    dataType: 'json',
    async:false,
    cache : false,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(response){
      if(response.status == 'success'){
        // console.log(response);
        $(".tab-pane").removeClass('active show');
        $(".nav-link").removeClass('active');
        // $("#activity-tab").removeClass("active");
          // $("#activity-history").removeClass("active show");
        // if(getFlag == 'c'){
        //   $("#activity-tab").addClass("active");
        //   $("#activity-history").addClass("active show");
        //    $("#case-details-tab").removeClass("active");
        //     $("#activity-case-details").removeClass("active show");
        // }
        
          $("#activity-case-details").addClass('active show');
          $("#case-details-tab").addClass('active');
       
    // $("#district_court_title").text(data.sc_case_type+' '+data.case_no+' / '+data.case_no_year);
    // $("#sub_case_title").text(data.case_title);
    // $("#sub_case_nominal").text(response.nominal);
    // complete_data_report = response.nominal;
    $(".flip-square-loader").hide();
     $("#team_member_count").text(response.team_count);
    // $("#connected-case").text(response.connected_count);
    $("#view_case_document").text(response.document_count);
    // hearing_date = response.data[0].hearing_date;
    // alert(hearing_date);
    buildCaseDetails(response);
    // buildTeamMember(response);

    
  }
  else if(response.status == 'error'){
   $("#view_card").hide();
   $("#not_found").show();

 }
},
complete:function(){
  $(".flip-square-loader").hide();
},
error:function(e){
  $(".flip-square-loader").hide();
  toastr.error("","Error while fetching data for entered CNR no.","Sorry for inconvinence",{timeout:5000});
}
});
}


function getUploadedDoc(type,judicial){
   $(".add_casesubmitdiv").addClass("fetchdataloader");
    $(".newsubmit").hide();
  /*$(".tab-pane fade").removeClass("active show");
  $("#documents").addClass('active show');*/
  var file = '',doc_type='';
  if(type == 'u'){
    var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','doc','docx'];
    if ($.inArray($("#upload_doc").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls' formats are allowed.",{timeout:5000});
      return false;
    }


    file = document.getElementById("upload_doc").files[0];
  // var case_id = caseid;
  if(isEmpty(file)){
    toastr.error("","Please upload atleast on document",{timeout:5000});
    return false;
  }
  doc_type = $("#doc_type option:selected").val();
}
var formData = new FormData();
formData.append('file', file);
formData.append('case_no', case_no);
formData.append('case_id',caseid);
formData.append('doc_type',doc_type);
formData.append('judicial',judicial);
var ajaxReq = $.ajax({
  url : host+"/case_document_upload.php",
  type : 'POST',
  data : formData,
  cache : false,
  contentType : false,
  processData : false,
  success:function(response){
    $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".newsubmit").show();
    $("#documentList").html('');
    $('input[type=file]').val('');

    var response_val = JSON.parse(response);
    var tr ='';
    if(type == 'u'){
      if(response_val.status == 'success')
        toastr.success("",'File Uploaded Successfully',{timeout:3000});
      else
        toastr.error("","Error in uploading files",{timeout:5000});
    }
    $.each(response_val.data,function(i,obj){
      $("#view_case_document").text(obj.doc_count);
      var filesize = bytesToSize(obj.size);
      tr +='<tr>';
      tr +='<td class="case-title"><span id="'+obj.doc_path+'" onclick=viewcases('+obj.doc_id+',this.id) class="viewDoc"><i class="feather icon-file-pdf-o "></i> '+obj.file_name+'</span></td>';
      tr +='<td class="case-title">'+filesize +'</td>';
      tr +='<td class="case-title">'+obj.type+'</td>';
      tr +='<td class="case-title">'+obj.uploaded_by+'</td>';
      tr +='<td class="case-title">'+obj.date_time+'</td>';
      tr+='<td><a class="btn btn-outline-warning doc_action btn-mini" href="'+host+'/edit_document.php?id='+obj.encrypted_case_id+'" title="Edit"><i class="feather icon-edit"></i></a>';
      if(obj.title == 'doc'){
       tr+= '<a class="btn btn-outline-warning doc_action btn-mini" href="'+obj.doc_path+'" title="Download" download><i class="feather icon-eye"></i></a>'; 
     }else{
      tr+= ' <button title="Download" type="button" class="doc_action viewBtn btn btn-outline-info btn-mini" ><a href="'+getDocUrl(obj.doc_path)+'" class="viewDoc" target="_blank" download><i class="feather icon-download"></i></a></button>';
    }
    tr+= ' <button type="button" onclick="deleteDoc('+obj.doc_id+')" title="Delete" class="doc_action btn btn-outline-danger btn-mini"><i class="feather icon-trash"></i></button></td>';
  });

    if(tr !=''){
      var table = '<table class="table table-bordered" id="documentList-table">';
      table +='<thead><tr><th>Title</th><th>Size</th><th>Type</th><th>Uploaded By</th><th>Uploaded Date</th><th class="action_th">Action</th></tr></thead><tbody>';
      table+= tr +'</tbody></table>';
    }
    $("#documentList").html(table);
    $("#documentList-table").dataTable();
  },error:function(){
    $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".newsubmit").show();
    toastr.error("",'Error in uploading document',{timeout:5000});
  }
});
};

$("#search-doc-filter").on('change',function(){
  var val = $(this).val();
  if(!isEmpty(val)){
    $("#search-document-div").show();
    $("#search-in-document").attr('placeholder','Enter '+val.toUpperCase());
  }
  else{
    $("#search-document-div").hide();
    $("#search-in-document").val('');
  }

});

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
function viewcases (docid,filename) {
  var fileExtension = ['doc','docx'];
  if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 0 || $.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 1) {
    // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
    window.location.href = host +"/upload_document/"+filename;
    $("#view_case_document_modal").modal('hide');
     // window.open(filename);
   }
   else{
    $.ajax({
     type:'POST',
     url:'ajaxdata/viewdoc.php',
     data:'docid='+docid,
     beforeSend: function()
     { 
      $('#flip-square-loader').show();
    },
    success:function(html){
      $('#casedocument').html(html);
      $("#view_case_document_modal").modal('show');
    },
    complete: function(){ 
      $('#flip-square-loader').hide();
    },
    error:function(e){
      $(".flip-square-loader").hide();
      toastr.error('Unable to Load ', 'Sorry For Inconvenience!', {timeOut: 5000})
              //console.log(e);
            }
          }); 
  }
}
function deleteDoc(doc_id){
  var yes = confirm("Are you sure you want to delete document?");
  if(yes){
    $.ajax({
      url : host+"/delete_view_document.php",
      type:'POST',
      data : {
        doc_id : doc_id
      },
      dataType: 'json',
      cache : false,
      success:function(response){
        // console.log(response);
        if(response.status == 'success'){
          getUploadedDoc('d');
          toastr.success("","Successfully deleted the document",{timeout:5000});
        }
        else
          toastr.error("","Error in deleting the document",{timeout:5000});
      },
      error:function(){
        toastr.error("","Error in deleting the document",{timeout:5000});
      }
    });
  }else
  return false
}
// function buildStage(response,flag){
//   var next_hearing_date = isEmpty(response.next_hearing_date) ? "" : response.next_hearing_date;
//   var stage = isEmpty(response.stage) ? "" : response.stage;
//   var posted_for = isEmpty(response.posted_for) ? "" : response.posted_for;
//   var action_taken = isEmpty(response.action_taken) ? "" : response.action_taken;
//   var session_phase = isEmpty(response.session_phase) ? "" : response.session_phase;

//   courtdata = '<table class="case-sidebar-section table-striped table table-bordered">';
//   courtdata += '<tbody><tr>';
//   courtdata += '<td class="c-type">Stage:</td>';
//   courtdata += '<td class="c-desc">'+stage+'</td>';
//   courtdata += '</tr>';
//   if(flag != 'judicial'){
//     courtdata += '<tr>';
//     courtdata += '<td class="c-type">Posted For:</td>';
//     courtdata += '<td class="c-desc">'+posted_for+'</td>';
//     courtdata += '</tr>';
//     courtdata += '<tr>';
//     courtdata += '<td class="c-type">Last Action Taken:</td>';
//     courtdata += '<td class="c-desc">'+action_taken+'</td>';
//     courtdata += '</tr>';
//   }
//   courtdata += '<tr>';
//   courtdata += '<td class="c-type">Hearing Date:</td>';
//   courtdata += '<td class="c-desc">'+next_hearing_date+'</td>';
//   courtdata += '</tr>';
//   // courtdata += '<tr>';
//   // courtdata += '<td class="c-type">Session:</td>';
//   // if(session_phase == 1)
//   //   courtdata += '<td class="c-desc">Morning</td>';
//   // else if(session_phase == 2)
//   //   courtdata += '<td class="c-desc">Evening</td>';
//   // else
//   //   courtdata += '<td class="c-desc"></td>';
//   // courtdata += '</tr>';
//   courtdata += '</tbody></table>';
//   $(".stage").html(courtdata);
// }

function buildCaseDetails(response){
  // debugger;
  var id = $("#id").val();
  $.each(response.data,function(i,obj){    
        var courtdata = '';
        courtdata +='<table class="table table-bordered table-striped case-sidebar-section court-details-section-1"><thead><tr>';
        // courtdata +='<th>Title</th><th>'+obj.case_title+'</th></tr></thead><tbody>';

courtdata += '<tr><td class="c-type">Type of Work </th>';
        courtdata += '<td class="c-desc">'+ obj.type_of_work +'</td></tr>';
        courtdata += '<tr><td class="c-type">Description of Work </th>';
        courtdata += '<td class="c-desc">'+ obj.case_title +'</td></tr>';       
   $(".court_data").html(courtdata);      

    });

     teamMemberArray = response.team_data;
   var table = '';
   if(!isEmpty(response.team_data)){
    $("#team_member_count").text(response.length);
    var tr = '';
    $.each(response.team_data,function(i,obj){
     // teamId.push(obj.team_id);
     var email = isEmpty(obj.email) ? "" : obj.email;
     var mobile = isEmpty(obj.mobile) ? "" : obj.mobile;
     if(!isEmpty(obj.first_name) || !isEmpty(obj.last_name)){
      tr+='<tr>';
      tr+='<td>'+obj.first_name+'</td>';
      tr+='<td>'+obj.last_name+'</td>';
      tr+='<td>'+email+'</td>';
      tr+='<td>'+mobile+'</td>';
      tr+='<td>'+obj.address+'</td>';
    }
    // tr+='<td><button class="btn btn-danger btn-sm" type="button" onclick="deleteteamMember()"><i class="feather icon-trash btn btn-outline-danger btn-mini"></i></button><tr>';
  });
    if(tr!='')
    {
     table = '<table class="table table-bordered export_table"><thead>';
     table +='<tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Contact</th><th>Address</th></tr></thead><tbody>';
     table += tr +'</tbody></table>';
   }
    $("#team-member-table-div").html(table);
  }
}


function viewDetails(i){
  $(".case-past-hearing-details_"+i).toggle();
  $(".view-details_"+i+" a").text($(".view-details_"+i+" a").text() == 'view details' ? 'hide details' : 'view details');
}




function deleteActivity(id){
  var yes = confirm("Are you sure you wanna delete?");
  if(yes){
    $.ajax({
      url : 'delete_activity_case.php',
      type : "POST",
      data : {
        activity_log_id : id
      },
      datatype : "json",
      beforeSend:function(){
        $(".flip-square-loader").show();
      },
      success:function(response){
        // console.log("delete activity",response);
        var response = JSON.parse(response);
        if(response.status == 'success')
          // view_case_details();
        toastr.success("","Successfully deleted the activity",{timeout:5000});
        document.getElementById("activity-tab").click();
      },
      complete:function(){
        $(".flip-square-loader").hide();
      },
      error:function(){
        toastr.error("","Error in deleting activity",{timeout:5000});
      }
    });
  }
  else
    return false;
}



      function cancelActivityEdit(id){
        tinymce.remove('#user-comment_'+id);
        $("#activity-info_"+id).hide();
      }


      function getDocUrl(doc_name){
        if(!isEmpty(doc_name)){
         // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
      // $('#pdfUrl').attr('href',host+"/casepdf"+response.pdfFile); 
      return host+'/upload_document/'+doc_name;
    }
    else
     return 'images/business+costume+male+man+office+user+icon-1320196264882354682.png';
 }
 function getActivityUserProfile(doc_name){
   if(!isEmpty(doc_name)){
    // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
      // $('#pdfUrl').attr('href',host+"/casepdf"+response.pdfFile); 
      return host+'/uploads_profile/'+doc_name;
    }
    else
     return 'uploads_profile/femaleicon.png';
     // return 'images/business+costume+male+man+office+user+icon-1320196264882354682.png';
   }
   /* function buildAttachment(response){
      var data_td='';
      $.each(response.acitivity_log,function(i,obj){
        $(".case_title_div").css('background',obj.color_code);
        if(obj.activity_log_type == 'p'){        
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="images/business+costume+male+man+office+user+icon-1320196264882354682.png" alt="" title="Adv. Vidhi Sharma"></td>';
          data_td +='<td class="user_name">by '+response.user_name+'</td>';
          data_td +='<td><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case"></i> <span><i class="feather icon-calendar icons-view-case"></i>  '+response.created_date+'</span> <span> <i class="icofont icofont-time icons-view-case"></i> '+response.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr>';
          data_td +='<td></td>';
          if(isEmpty(obj.last_priority))
            data_td +='<td colspan="2" class="activity-chunks"><p><b>Priority </b> added <b>'+obj.priority+'</b></p></td>';
          else
            data_td +='<td colspan="2" class="activity-chunks"><p><b>Priority </b> changed from <b>'+obj.last_priority+'</b> to <b>'+obj.priority+'</b></p></td>';
          data_td +='</tr>';
          prioritySelected(obj.priority);
        }

        else if(obj.activity_log_type == 'c') {
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="images/business+costume+male+man+office+user+icon-1320196264882354682.png" alt="" title="Adv. Vidhi Sharma"></td>';
          data_td +='<td class="user_name">by '+response.user_name+'</td>';
          data_td +='<td><i class="feather icon-edit-2 btn btn-outline-info btn-mini icons-view-case"></i> <i class="feather icon-trash btn btn-outline-danger btn-mini icons-view-case"></i> <span><i class="feather icon-calendar icons-view-case"></i>  '+response.created_date+'</span> <span> <i class="icofont icofont-time icons-view-case"></i> '+response.created_time+'</span></td>';
          data_td +='</tr>';
          data_td +='<tr>';
          data_td +='<td></td>';
          data_td +='<td colspan="2"><div class="speech-bubble">'+obj.user_comment+'</div></td>';
          data_td +='</tr>';
        }
      });
      return data_td;
    }*/
  //   var teamMemberArray ='',teamId=[];

  //   function teamList(){
  //     debugger;
  //   /*var data = {
  //     teamArr : teamId
  //   }*/
  //   $.ajax({ 
  //    url: host+"/team_member_ajax.php",
  //    type:'POST',
  //    dataType: 'json',
  //    data : {
  //     case_id : caseid,
  //   },
  //   async:false,
  //   success:function(data){            
  //       // console.log(data);
  //       var options = '';
  //       $.each(data,function(i,obj){
  //         options +='<option value="'+obj.reg_id+'">'+obj.name+' '+obj.last_name+'</option>';
  //       });
  //       // console.log("option",options);
  //       teamListArray = options;
  //     },
  //     error:function(){
  //      toastr.error("","Error in loading team list",{timeout:5000});
  //    }
  //  });
  // }


//   function buildTeamMember(response){
//     debugger;
//    teamMemberArray = response.team_data;
//    var table = '';
//    if(!isEmpty(response.team_data)){
//     $("#team_member_count").text(response.length);
//     var tr = '';
//     $.each(response.team_data,function(i,obj){
//      // teamId.push(obj.team_id);
//      var email = isEmpty(obj.email) ? "" : obj.email;
//      var mobile = isEmpty(obj.mobile) ? "" : obj.mobile;
//      if(!isEmpty(obj.first_name) || !isEmpty(obj.last_name)){
//       tr+='<tr>';
//       tr+='<td>'+obj.first_name+'</td>';
//       tr+='<td>'+obj.last_name+'</td>';
//       tr+='<td>'+email+'</td>';
//       tr+='<td>'+mobile+'</td>';
//       tr+='<td>'+obj.address+'</td>';
//     }
//     // tr+='<td><button class="btn btn-danger btn-sm" type="button" onclick="deleteteamMember()"><i class="feather icon-trash btn btn-outline-danger btn-mini"></i></button><tr>';
//   });
//     if(tr!='')
//     {
//      table = '<table class="table table-bordered export_table"><thead>';
//      table +='<tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Contact</th><th>Address</th></tr></thead><tbody>';
//      table += tr +'</tbody></table>';
//    }
//     $("#team-member-table-div").html(table);
//   }

// }


function assignTeamMeambers(){
  // debugger;
  var team_member_array = [];
  var team_multiselect = $("#team_multiselect").val();
  $.each(team_multiselect,function(i,obj){
    team_member_array.push(obj);
  });
  var teamVo = new Object();
  teamVo.team_member_array = team_member_array;
  teamVo.case_id = caseid;
  teamVo.relateto = $("#sub_case_title").text()
  $.ajax({ 
    url: host+"/submit_view_team.php",
    type:'POST',
    data: "viewtododetail="+JSON.stringify(teamVo),
    dataType: 'json',
    async:false,
    success:function(data){            
      // console.log(data);
      if(data.status == 'success'){
        toastr.success("","Successfully changed case priority value",{timeout:5000});
          // CKEDITOR.instances.query.setData('');
          tinymce.get('query').setContent('');
          view_case_details('t');
        }
        else
         toastr.success("","Error in changing case priority value",{timeout:5000});  
     },
     error:function(){

     }
   });
}

function whoAttended(id,whoAttendedArray){
  var response = teamMemberArray;
  var who_attended_div_list='';
  var whoAttendedId = [];
  $.each(whoAttendedArray,function(i,obj){
    whoAttendedId.push(obj.who_attended);
  });
  $.each(response,function(i,obj){
    who_attended_div_list += '<div class="form-check col-sm-4">';
    who_attended_div_list += '<label class="form-check-label checkboxlabel">';
    if(whoAttendedId.includes(obj.team_id)){
      who_attended_div_list +='<div class="checkbox-zoom zoom-primary">';
      who_attended_div_list +='<label>';
      who_attended_div_list +='<input type="checkbox" class="chkPassport" value="'+obj.team_id+'" checked name="who_attended"><span class="cr">';
      who_attended_div_list +='<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
      who_attended_div_list +='</span> <span>'+obj.first_name+' '+obj.last_name+'</span>';
      who_attended_div_list +='</label>';
      who_attended_div_list +='</div>';

    }
    else{
      who_attended_div_list +='<div class="checkbox-zoom zoom-primary">';
      who_attended_div_list +='<label>';
      who_attended_div_list +='<input type="checkbox" class="chkPassport" value="'+obj.team_id+'" name="who_attended"><span class="cr">';
      who_attended_div_list +='<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
      who_attended_div_list +='</span> <span>'+obj.first_name+' '+obj.last_name+'</span>';
      who_attended_div_list +='</label>';
      who_attended_div_list +='</div>';
    }
    who_attended_div_list += '</label>';
    who_attended_div_list += '</div>';

  });
  who_attended_div_list +='<div class="form-check col-sm-3">';
  who_attended_div_list +='<div class="checkbox-zoom zoom-primary">';
  who_attended_div_list +='<label>';
  who_attended_div_list +='<input type="checkbox" class="chkPassport" name="who_attended" value="Others"><span class="cr">';
  who_attended_div_list +='<i class="cr-icon icofont icofont-ui-check txt-primary"></i>';
  who_attended_div_list +='</span> <span>Others</span>';
  who_attended_div_list +='</label>';
  who_attended_div_list +='</div>';
  who_attended_div_list +=' </div>'
      // $("#who_attended_div_"+id).html(who_attended);
      return who_attended_div_list;
    }

    function prioritySelected(data){
     var objSelect = document.getElementById("set_priority_select");
     setSelectedValue(objSelect,data);  

   }
   function setSelectedValue(selectObj, valueToSet) {
     for (var i = 0; i < selectObj.options.length; i++) {
      if (selectObj.options[i].value== valueToSet) {
       selectObj.options[i].selected = true;
       return;
     }
   }
 }

 $("#set_priority_select").on('change',function(){
   var priority = $(this).val();
   var caseid = $("#caseid").val();
    // var courtid = $("#court-id").val();
    if(priority == 'Super Critical'){
      $(".case_title_div").css('background','red');
      color = 'red';
    }
    else if(priority == 'Critical'){
      $(".case_title_div").css('background','#ffeedd');
      color = '#ffeedd';
    }
    else if(priority == 'Important'){
      $(".case_title_div").css('background','#ffffdd');
      color = '#ffffdd';
    }
    else if(priority == 'Routine'){
      $(".case_title_div").css('background','#ceffff');
      color = '#ceffff';
    }
    else if(priority == 'Others'){
      $(".case_title_div").css('background','#eef8fb');
      color = '#eef8fb';
    }
    else if(priority == 'Normal'){
      $(".case_title_div").css('background','#ffffff');
      color = '#ffffff';
    }

    var priorityVo = new Object();
    priorityVo.case_id = caseid;
    priorityVo.CNR = cnr;
    priorityVo.court_id = court_id_val;
    priorityVo.case_no = case_no;
    priorityVo.color = color;
    priorityVo.case_id = caseid;
    priorityVo.priority = priority;
    priorityVo.user_comment = '';
    priorityVo.activity_log_type = 'p';
    priorityVo.activity_flag = 'submit';

    var data_string = JSON.stringify(priorityVo);

    updateHearingDateList(data_string);

  });

 function updateHearingDateList(data){
   $.ajax({ 
    url: host+"/user_activity_log.php",
    type:'POST',
    data: {
     activity_log : data
   },
   dataType: 'json',
   async:false,
   success:function(data){            
     // console.log(data);
     if(data.status == 'success'){
      toastr.success("You have successfully update the case activity",{timeout:5000});
      // CKEDITOR.instances.query.setData('');
      tinymce.get('query').setContent('');
      $("input[name='recordHearingradio'][value='No']").prop("checked",true);
      $(".case-activity-info").hide();
      $(".case-activity-info input").val('');
      // $("#activity-history").load(window.location.href + "#activity-history");
      // $( "#activity-history" ).load(window.location.href + " #activity-history" );
      view_case_details('c');
    }
    else
     toastr.error("","Error in updating activity",{timeout:5000});  
 },
 error:function(){
   toastr.error("","Error in updating activity",{timeout:5000});
 }
});
 }


 function submitCaseComment(activity_type){
   var priorityVo = new Object();
   var caseid = $("#caseid").val();
   priorityVo.case_id = caseid;
    // var courtid = $("#court-id").val();

    priorityVo.CNR = cnr;
    priorityVo.court_id = court_id_val;
    priorityVo.case_no = case_no;
    priorityVo.color = "";
    priorityVo.priority = "";
    priorityVo.case_id = caseid;
    priorityVo.user_comment = tinymce.get('query').getContent();
        // priorityVo.user_comment = CKEDITOR.instances.query.getData(); // query is the id of CKEDITOR
        priorityVo.activity_log_type = 'c';

        if($("input[name='recordHearingradio']:checked").val() == 'Yes'){
          priorityVo.status = $("#record_status option:selected").text();
          priorityVo.who_attended = [];


          $.each($("input[name='who_attended']:checked"),function(i,obj){
            priorityVo.who_attended.push(this.value);
            if($("input[name='who_attended']:checked").val() == 'Others')
              priorityVo.who_attended.push($("#who_attended_others").val());
          });


          priorityVo.stage = $("#caseactivityinfo-stage").val();
          priorityVo.posted_for = $("#caseactivityinfo-posted_for").val();
          priorityVo.action_taken = $("#caseactivityinfo-action_taken").val();
          priorityVo.next_hearing = $("#caseactivityinfo-next_hearing").val();
          // priorityVo.session = $("#caseactivityinfo-sessions").val();
         // query is the id of CKEDITOR
         priorityVo.activity_log_type = 'h';

       }
       priorityVo.activity_flag = activity_type;
       // console.log("priorityVo",priorityVo);
       if(priorityVo.user_comment=='' && priorityVo.activity_log_type == 'c'){
        toastr.warning("","Please insert some comment",{timeout:5000});
        return false;
      }
      var data_string = JSON.stringify(priorityVo);
      updateHearingDateList(data_string);
    }

  // .......................Added note tab js..........................

  load_nodecount();

  function load_nodecount() {
    var caseid = $("#caseid").val();
    $.ajax({
      url: "ajaxdata/getNumberCount.php",
      type: 'POST',
      data: {caseid: caseid},
      cache: false,
      success: function (html) {
        $("#notes-counter").html(" ("+html+")");
      }
    });
  }

  function deleteNote(id,case_id) {
    var yes = confirm("Are you sure you want to delete?");
    if (yes)
      $.ajax({
        url: "ajaxdata/deletenote.php",
        type: 'POST',
        data: {
          noteid: id,
          case_id: case_id
        },
        success: function (data) {
          // console.log(data);
          buildNotes(data,case_id);
        }
      });

  }

    //NOTE 
    $("#btnNoteSave").click(function () {
      var notes = $("#notes").val();
      var caseid = $("#caseid").val();

      var radioValue = $("input[name='np']:checked").val();
      var notes_file = document.getElementById("notes-file").files[0]

      if (notes == '') {
        alert("Please enter notes");
        return false;
      }

      if (radioValue == undefined) {
        alert("Select Mark As");
        return false;
      }
      var formData = new FormData();

      formData.append("note",notes);
      formData.append("private",radioValue);
      formData.append("caseid",caseid);
      formData.append("flag",'insert');
      formData.append("notes_file",notes_file);

      var ajaxResult = $.ajax({
       url : "ajaxdata/savenote.php",
       type : 'POST',
       data : formData,
       cache : false,
       contentType : false,
       processData : false,
       success:function(response){
        // console.log(response);

        $("#notes").val('');
        $("#notes-file").val('');
        buildNotes(response,caseid);
      },
      error:function(){
        toastr.error("","Error in adding case",{timeout:5000});
      }
    });

    });
    $("#next-hearing-date").datepicker();
    $("#btnNoteClear").click(function () {
      $("#notes").val('');
    });

    //END NOTE

    function getNotes(case_id){
      $.ajax({
        url : "ajaxdata/savenote.php",
        type : 'POST',
        data: {
          case_id: case_id
        },
        cache: false,
        dataType: 'json',
        success : function(data){
          // console.log(data);
          buildNotes(data,case_id);
      // if (data.status) {
                  // var    tr = '<table class="table table-bordered export_table" id="timesheettable">';
                  
                  // $("#notes").val('');
                // }
              }
            });
    }

    function btnNoteCancel(id){
     $("#note_div_"+id).show();
     $("#notetxt_"+id).hide();
   }


   function editNotes(id){
     $("#note_div_"+id).hide();
     $("#notetxt_"+id).show();

     
      // $("#activity-info_"+id).css('display','flex');
    }

    function buildNotes(data,case_id){
      $("#note_block").html('');
      var tr ='';
      $.each(data,function(i,obj){
       //tr +='<div class="row">';
       tr +='<div class="col" id="note_'+obj.lastid+'" style="border:1px solid #ccc;background: #fff;margin-top:10px;">';
       tr +='<div class="row" style="background: #fff;">';
       tr +='<div class="col col-md-50">';
       tr +='<div class="row justify-content-end" style="background: #fff;">';
       tr +='<div class="col case-note-item-actions" style="text-align:right;padding:5px;">';
       if(obj.private == 'yes'){
        tr +='<span id="note-creater" style="border-right: 1px solid;padding-right: 6px;font-weight:bold;">Private</span>';
      }
      tr +='<b id="note-creater" style="border-right: 1px solid;padding-right: 6px;">';
      tr += obj.name+' '+obj.last_name+'</b>';
      tr +='<b id="note-created-date" style="border-right: 1px solid;padding-right: 6px;">';
      tr += obj.datetime+'</b>';
      tr +='<span style="padding:5px" id="'+obj.file_name+'" onclick="getNotesFile(this.id)"><i class="fa fa-file-image-o"></i></span>';
      tr +='<span class="download-item" style="padding:5px"> ';
      tr +='<a onclick="downloadNotePdf('+obj.lastid+')" class="sprite download_icon" title="Download" data-pjax="">';
      tr +=' <i class="fa fa-download" aria-hidden="true"></i></a></span>';
      tr +='<span class="edit-item" style="padding:5px">';
      tr +='<a onclick="editNotes('+obj.lastid+')" class="case-notes-edit sprite edit-icon" title="Edit" data-ntype-checked="false">';
      tr +=' <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></span>';
      tr +='<span class="delete-item" style="padding:5px">';
      tr +='<a onclick="deleteNote('+obj.lastid+','+case_id+')" class="case-notes-delete sprite delete_icon" title="Delete" data-requestrunning="false">';
      tr +='<i class="fa fa-trash-o" aria-hidden="true"></i></a></span></div></div></div></div>';
      tr +='<div class="col-sm-12 row editNoteBlock" style="display: none;" id="notetxt_'+obj.lastid+'">';
      tr +='<textarea id="notetxtarea_'+obj.lastid+'" rows="5" class="form-control form-group">'+obj.note+'</textarea>';
      tr +='<div class="row col-sm-12"><div class="col-sm-10 form-group row p-0"><b class="col-sm-3">Mark this note as private </b>';
                      // tr +='<div class="row"><div class="col row"><b>Mark this note as private </b>';
                      tr +='<div class="form-radio">';
                      if(obj.private == 'yes'){
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="yes" checked><i class="helper"></i> Yes</label></div>';
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="no"><i class="helper"></i> No</label></div>';
                      }
                      else if(obj.private == 'no'){
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="yes"><i class="helper"></i> Yes</label></div>';
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="no" checked><i class="helper"></i> No</label></div>';
                      } else{
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="yes"><i class="helper"></i> Yes</label></div>';
                        tr +='<div class="radio radio-inline"><label><input type="radio" name="enp" value="no"><i class="helper"></i> No</label></div>';
                      }
                      tr +='</div>';
                      tr +=' <div class="col row form-group"><input type="file" name="notes-file" id="notes-files"><p id="existingfilename" style="width:100%">File name: '+obj.file_name+'</p></div>';
                      tr +='</div>';
                      tr +='<div class="col-sm-2"><button class="btn btn-primary btn-sm" type="button" onclick=btnUpdateNote('+obj.lastid+',"'+case_id+'")>Update</button>';
                      tr +=' <button class="btn btn-danger btn-sm" type="button" onclick="btnNoteCancel('+obj.lastid+')">Cancel</button></div>';
                      tr +='</div>';
                      tr +='</div>';
                      tr +='<div class="col" style="padding:5px;" id="note_div_'+obj.lastid+'">'+obj.note+'</div></div>';
                      //tr+='</div>';
                    });
                  // $(data.data).insertBefore("#note_block .content:first-child");

                  $("#note_block").html(tr);
                }

                $(document).on('change','#notes-files',function(e){
                  $("#existingfilename").text('File name: '+e.target.files[0].name);
                })

                function getNotesFile(filename){


                  if(!isEmpty(filename)){
                    var fileExtension = ['doc','docx'];
                    if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 0 || $.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == 1) {
                      // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
                      $("#notes-file-iframe").attr('src',host +"/upload_document/"+filename);
                      $("#notes_image_modal").modal('Hide');

                    }
                    $("#notes-file-iframe").attr('src',host +"/upload_document/"+filename);
                    $("#notes_image_modal").modal('show');
                  }
                }
                function btnUpdateNote(id,caseid){
                  var content = $("#notetxtarea_"+id).val();
                  //var radioValue = type;
                  var radioValue = $("input[name='enp']:checked").val();
                  var notes_files = document.getElementById("notes-files").files[0];
                  //$('input[name=enp][value='+radioValue+']').attr('checked', true);
                  var formData = new FormData();

                  formData.append("notes",content);
                  formData.append("type",radioValue);
                  formData.append("noteid",id);
                  formData.append("notes_files",notes_files);


                  var updatenotes = $.ajax({
                    url: "ajaxdata/updatednote.php",
                    type: 'POST',
                    data: formData,
                    cache : false,
                    contentType : false,
                    processData : false,
                    success: function (data) {
                      // console.log(data);
                      if (data.status) {
                        $("#notetxt_"+id).hide();
                        $("#note_div_"+id).show();
                        $("#note_div_"+id).html(content);
                        getNotes(caseid);
                      }
                    }
                  });

                }



                function getCasesReferredList(){
                 var caseid = $("#caseid").val();
                 $.ajax({
                  url: "show_supporting_cases.php",
                  type: 'POST',
                  data: {
                   caseid: caseid
                 },
                 cache: false,
                 dataType: 'json',
                 success: function (data) {
                //alert(data);
                // console.log(data);
                $.each(data,function(i,obj){
                  tr+='<tr>';
                  tr+='<td></td>';
                  tr+='<td><i class="feather icon-external-link"></i></td>';
                  tr+='</tr>';
                });
                if(tr!=''){
                  var table = '<table class="table table-bordered"><thead><tr><th>Citation</th><th>Action</th></tr></thead><tbody>';
                  table+=tr+'</tbody></table>';
                }
                $("#show_lawdata").html(table);
              },
              error:function(){
               toastr.error("","Error in fetching cases referred list",{timeout:5000});
             }
           });


               }

               function getCasesReffered(){
                 var tr='';
                 var caseid = $("#caseid").val();
                 $.ajax({
                  url: "show_supporting_cases.php",
                  type: 'POST',
                  data: {
                   caseid: caseid
                 },
                 cache: false,
                 dataType: 'json',
                 success: function (response) {
                //alert(data);
                // $("#connected-case").text(response.count);
                var table = '<table class="table table-bordered" id="law_table"><thead><tr><th>Sr No</th><th>Citation</th><th>View Judgement</th></tr></thead><tbody>';
                
                $.each(response.data,function(i,obj){
                  i++;

                  var online = obj.product_type;
                  tr+='<tr>';
                  tr+='<td>'+i+'</td>';
                  tr+='<td>'+obj.citation+'</td>';
                  tr+='<td onclick=getcitation(this.id) id="'+online+'_'+obj.citation+'" style="text-align:center;"><i class="feather icon-external-link"></i></td>';
                  tr+='</tr>';
                });
                table += tr+'</tbody></table>';
                // if(tr!=''){

                // }
                // $("#law_table").dataTable();
                $("#show_lawdata").html(table);
                $("#law_table").DataTable();
              },
              error:function(){
               toastr.error("","Error in fetching cases referred list",{timeout:5000});
             }
           });
               }
               function getcitation(onlineget){
                 var citvar = onlineget.split("_");
                 var online = citvar[0];
                 var citation = citvar[1];
                 var search = $("#search").val();
                 $.ajax({ 
      // url: "http://192.168.0.121:8080/qtool/api/qtool/judgement",
      url: qtoolurl+"api/qtool/judgement",
      type:'get',
      data: {
        citation:citation,
        product: online
      },
      dataType: 'json',
      async:false,
      headers: {
        "Content-Type": 'application/json',
        "Authorization":"Bearer "+localStorage.getItem('auth_token')
      },
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(response){  
     // console.log(response);
     // var content = replace(search,response.content);
     var printWin = window.open(''); 
     printWin.document.write('<html><head><title>'+citation+'</title><link rel="stylesheet" type="text/css" href="css/showjudgment.css"><link rel="stylesheet" href="css/bootstrap.min.css" /></head><body>'); 
     printWin.document.write(response.content); 
     printWin.document.write("</body></html>");
   },
   complete: function(){
    $('.flip-square-loader').hide();  

  },
  error: function(e){

    $('.flip-square-loader').hide();  
    toastr.error('Unable to load Citation', 'Sorry For Inconvenience!', {timeOut: 5000})
  }
});
               }

               function deletecase (id) {
                 var yes = confirm('All the documents, Reminders, Notes, Timesheet Etc. associated to this case will be deleted Are you sure? this action cant be undone.');
                 if(yes){
                  var caseid = id;
                  $.ajax({
                   type:'POST',
                   url:'delete_cases.php',
                   data:'case_id='+caseid,
                   beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
    // alert("Case Data Removed Successfully");
    toastr.success('', 'Case Data Removed Successfully', {timeOut: 5000})
    window.location.href='cases-report.php';
       // $('#delcase').html(html);
     },
     complete: function(){
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Case', {timeOut: 5000})
    }
  }); 
                }
                else{
                  return false;
                }
              }
              /*function replace(query,content) { var s = query.split(" "); var keys = keywordarray(); s.forEach(function(i,e){ if(!keys.includes(i)) content= i && content ? content.replace(new RegExp(escapeRegexp(i), 'gi'), "<span style='background:yellow'>$&</span>") : content; }); return content; }*/


