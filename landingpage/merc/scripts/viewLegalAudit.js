$(document).ready(function(){
   tinymce.init({
    selector : "#askquery",
    plugins : [
    "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });
  var caseid = $("#case_id").val();
   var org_id = $("#org_id").val();
viewAudit(caseid,org_id);
});


function viewAudit(caseid,org_id){
var caseids = localStorage.getItem("legal_audit_caseid");
  var i = localStorage.getItem("legal_audit_counter");
  var dataSend = {
    caseTitleId:caseid,
    organizationId:org_id
  }
$.ajax({
    // url: "http://192.168.0.56:8080/LegalResearch/legal_audit_get",
    // url: legalResearch+"/legal_audit_get",
    // type:'GET',
    // data: "caseTitleId="+caseid,
    // dataType: 'json',
    // async:false,
    // headers: {
    //   "Content-Type": 'application/json'
    // },
    url: "allapi.php",
    type:'POST',
    data: {params :dataSend,
flag: 'view_response'
    },
    dataType: 'json',
    async:false,
    // headers: {
    //   "Content-Type": 'application/json'
    // },
    success: function(result){
      // console.log(result.caseStatus);
      if(result.caseStatus == 'Assigned'){
        alert('Case is Assigned... Please Wait for the Legal Audit');
        window.location.href='viewLegalSupport.php';
      }
      else if(result.caseStatus == 'In Review'){
        alert('Case is In Review... Please Wait for the Legal Audit');
        window.location.href='viewLegalSupport.php';
      }else if(result.caseStatus == 'Publish'){
      var datas = result.workBook;
      // if(result.error == "Data not yet to Proceed. Please wait some time"){
      //   toastr.warning('Data not yet to Proceed. Please wait some time', 'Sorry For Inconvenience!', {timeOut: 5000});
      //   /*$("#researcher").modal('hide');*/
      // }else{
        if(!isEmpty(datas)){
          var report = '';
          report += '<div> <h4 class="modal-title" style="color: #f10038;text-align: center;">Legal Audit Report</h4></div>';
           report += '<div class="col-sm-12" style="text-align: right;"><button class="btn btn-sm btn-info" type="button" onclick="askQuery('+caseid+')">Ask a query</button>&nbsp;&nbsp;';
          report += ' </div><div class="heading_legal">';
          report += '<b>Case No: <span style="text-decoration:underline;">'+datas.case_no+' of '+datas.case_year+' <span></b></div>';
          report += '<table class="table table-bordered legal_table" id="LegalResponse"><tbody>';
          report +='<tr>';
          // report +='<td class="srno">1</td>';
          report +='<td class="subhead_legal">AIR Legal No:</td>';
          // report +='<td>'+datas.casetitle_id+'</td></tr>';
            report +='<td>';
          report +=  (datas.casetitle_id != null) ? datas.casetitle_id : '';
          report += ' </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Department:</td>';
          // report +='<td>'+datas.department+'</td></tr>';
            report +='<td>';
          report +=  (datas.department != null) ? datas.department : '';
          report += ' </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Researcher Name:</td>';
          report +='<td>';
          report +=  (datas.researcher_name != null) ? datas.researcher_name : '';
          report += ' </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Court/ Forum:</td>';
          // report +='<td> '+datas.court_forum+'</td></tr>';
            report +='<td>';
          report +=  (datas.court_forum != null) ? datas.court_forum : '';
          report += ' </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Case Status:</td>';
          // report +='<td>'+datas.case_status_name+' </td></tr>';
            report +='<td>';
          report +=  (datas.case_status_name != null) ? datas.case_status_name : '';
          report += ' </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Orders Challenged:</td>';
          report +='<td>';

           report += (datas.orders_challenged == "Y") ? "Yes" : "No";
           report += (!isEmpty(datas.orders_challenged_text)) ? ' ( '+datas.orders_challenged_text+' )' : '';
          report +='</td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Subject matter:</td>';
          // report +='<td>'+datas.subject_matter_name+' </td></tr>';
            report +='<td>';
          report +=  (datas.subject_matter_name != null) ? datas.subject_matter_name : '';
          report += ' </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Category of Risk involved From Client:</td>';
          // report +='<td>'+datas.risk_category_client_text+'</td></tr>';
            report +='<td>';
          report +=  (datas.risk_category_client_text != null) ? datas.risk_category_client_text : '';
          report += ' </td></tr>';

            report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Category of Risk involved From Auditor:</td>';
          // report +='<td>'+datas.risk_category_Auditor_text+'</td></tr>';
            report +='<td>';
          report +=  (datas.risk_category_Auditor_text != null) ? datas.risk_category_Auditor_text : '';
          report += ' </td></tr>';

          

          report +='<tr>';
          // report +='<td class="srno">5</td>';
          report +='<td class="subhead_legal">Missing Documents:(if any):</td>';
           report +='<td>';
            report += (datas.missing_document == "Y") ? "Yes" : "No";
           report += (!isEmpty(datas.missing_document_text)) ? ' ( '+datas.missing_document_text+' )' : '';
          report +='</td></tr>';

          report +='<tr>';
          // report +='<td class="srno">6</td>';
          report +='<td class="subhead_legal">Whether documents submitted to the Court?:</td>';
          
            report +='<td> ';
             report += (datas.document_submited_to_cout == "Y") ? "Yes" : "No";
           report +='</td></tr>';
         
         
          // report +='<tr>';
          // // report +='<td class="srno">9</td>';
          // report +='<td class="subhead_legal">Petitioner Facts:</td>';
          // report +='<td>';
          // if(!isEmpty(result.petionerFactVos)){
          //   $.each(result.petionerFactVos,function(c,pf){
          //     report +='<li>'+pf.facts+'</li>';
          //   });
          // }
          // report +='</td></tr>';

          report +='<tr>';
          // report +='<td class="srno">10</td>';
          report +='<td class="subhead_legal">  Facts:</td>';
          // report +='<td>'+datas.facts+' </td></tr>';
            report +='<td>';
          report +=  (datas.facts != null) ? datas.facts : '';
          report += ' </td></tr>';

            report +='<tr>';
          // report +='<td class="srno">13</td>';
          report +='<td class="subhead_legal">Issues:</td>';
          // report +='<td> '+datas.issues+'</td></tr>';
            report +='<td>';
          report +=  (datas.issues != null) ? datas.issues : '';
          report += ' </td></tr>';
          // report +='<td class="srno">11</td>';
          report +='<td class="subhead_legal">Plea:</td>';
          // report +='<td> '+datas.plea+'</td></tr>';
            report +='<td>';
          report +=  (datas.plea != null) ? datas.plea : '';
          report += ' </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">12</td>';
          report +='<td class="subhead_legal">Counter Plea:</td>';
          // report +='<td>'+datas.counter_plea+'</td></tr>';
            report +='<td>';
          report +=  (datas.counter_plea != null) ? datas.counter_plea : '';
          report += ' </td></tr>';

         

          report +='<tr>';
          // report +='<td class="srno">14</td>';
          report +='<td class="subhead_legal">Opinion:</td>';
          // var opinion_text = '';
          // if(!isEmpty(result.legalReportVO.opinionVOs)){
          //   $.each(result.legalReportVO.opinionVOs,function(i,obj){
          //     opinion_text+=obj.opinion;
          //   });
          // }
          // report +='<td>'+datas.opinion+'</td></tr>';
            report +='<td>';
          report +=  (datas.opinion != null) ? datas.opinion : '';
          report += ' </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">15</td>';
          report +='<td class="subhead_legal">Case referred</td>';
           report +='<td>';
          if(!isEmpty(datas.caserefer)){
            $.each(datas.caserefer,function(i,obj){
              report +='<li>'+obj.link_name+' <a href='+obj.link+' style="text-decoration: underline;color: #01a9ac;"> '+obj.link+'</a>'+'</li>';
            });
          }


          //   var opinion_text = '';
          // if(!isEmpty(datas.caserefer)){
          //   $.each(datas.caserefer,function(i,obj){
          //      report +=datas.link_name+' '+datas.link
          //   });
          // }
         report +=' </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">16</td>';
          report +='<td class="subhead_legal">Last word of land:</td>';
          // report +='<td>'+datas.last_word_of_land+' </td></tr>';
            report +='<td>';
          report +=  (datas.last_word_of_land != null) ? datas.last_word_of_land : '';
          report += ' </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">17</td>';
          report +='<td class="subhead_legal">Act and Rules:</td>';
          report +='<td> </td></tr>';

          // report +='<tr>';
          // // report +='<td class="srno">18</td>';
          // report +='<td class="subhead_legal">Advocate:</td>';
          // report +='<td> </td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Strength of the case:</td>';
          // report +='<td>'+datas.strength_of_the_case+'</td></tr>';
            report +='<td>';
          report +=  (datas.strength_of_the_case != null) ? datas.strength_of_the_case : '';
          report += ' </td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Special opinion for ADR or Disposed case:</td>';
          report +='<td>';
          report += (datas.special_opinion == "Y") ? "Yes" : "No";
          report +='</td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Unnecessary Delay:</td>';
          report +='<td>';
          report += (datas.unnecessary_delay == "Y") ? "Yes" : "No";
            report +='</td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Contempt (if any):</td>';
          report +='<td>';
           report += (datas.contempt == "Y") ? "Yes" : "No";
           report += (!isEmpty(datas.contempt_text)) ? ' ( '+datas.contempt_text+' )' : '';
           report +='</td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Financial Repercussion:</td>';
          // report +='<td>'+datas.financial_repercussion+'</td></tr>';
            report +='<td>';
          report +=  (datas.financial_repercussion != null) ? datas.financial_repercussion : '';
          report += ' </td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">First Verification:</td>';
          // report +='<td>'+datas.first_verification+'</td></tr>';
            report +='<td>';
          report +=  (datas.first_verification != null) ? datas.first_verification : '';
          report += ' </td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Second Verification:</td>';
          // report +='<td>'+datas.second_verification+'</td></tr>';
            report +='<td>';
          report +=  (datas.second_verification != null) ? datas.second_verification : '';
          report += ' </td></tr>';
          report +='<tr>';
          // report +='<td class="srno">19</td>';
          report +='<td class="subhead_legal">Final opinion:</td>';
          // if(!isEmpty(result.legalReportVO.finalApproval))
          //   report +='<td>'+result.legalReportVO.finalApproval+'</td>';
          // else
            report +='<td>'+datas.final_opinion+'</td>';
          report +='</tr></tbody></table>';
          $("#legal_data").html(report);
          $("#tree-modal").modal('show');
          $(".fetchLoaderDiv_"+i).removeClass("fetchdataloader");
          $("#research_reportid_"+i).show();
        // }else{
        //   $(".fetchLoaderDiv_"+i).removeClass("fetchdataloader");
        //   $("#research_reportid_"+i).show();
        // }
      } 
    } else{
        alert('Case is not Assigned Yet');
        window.location.href='viewLegalSupport.php';
      }
    },
    error:function(){
      $(".fetchLoaderDiv_"+i).removeClass("fetchdataloader");
      $("#research_reportid_"+i).show();
      toastr.error("","Error in getting audit report",{timeout:5000});
    }
});
}(1);


function askQuery(){
  $("#ask-query").modal('show');
}

function askQueToCLR(){
  var askquery = tinyMCE.get('askquery').getContent();
  var caseid = $("#case_id").val();
  $.ajax({
    // url: "allapi.php",
    url:'',
    type:'POST',
    data: {params :dataSend,
flag: 'view_response'
    },
    dataType: 'json',
    async:false,
    success: function(result){
      // console.log(result);
    }
  });
}