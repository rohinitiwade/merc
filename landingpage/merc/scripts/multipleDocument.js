$("#doc_type").select2();
$("#caseid").select2();
$(document).ready(function(){
  getUploadedDoc('all');

});
function getUploadedDoc(type){
  debugger;
  /*$(".tab-pane fade").removeClass("active show");
  $("#documents").addClass('active show');*/
  $(".add_casesubmitdiv").addClass("fetchdataloader");
  $(".add_casesubmit").hide();
  var file = '',doc_type='';

  file = document.getElementById("upload_doc").files[0];
  doc_type = $("#doc_type").val();
  var doc_value = $( "#doc_type option:selected" ).text();
  if(type != 'all'){
    if(isEmpty(file)){
      toastr.error("","Please select atleast one document",{timeout:5000});
      $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
      return false;
    }else if(isEmpty(doc_type)){
      toastr.error("","Please select Document Type",{timeout:5000});
      $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
      return false;
    }
    var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','doc','docx'];
    if ($.inArray($("#upload_doc").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls' formats are allowed.",{timeout:5000});
      $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
      return false;
    }



  // var case_id = caseid;

  
}

  // var caseid = $("#caseid").val();
  // var case_no = $("#case_no").val();

  // if ($("input[name='complete_papers']").is(":checked")) {
  //   var uploaded_papers = $("input[name='complete_papers']:checked").val();
  // }
  var formData = new FormData();
  formData.append('file', file);
  // formData.append('case_no', case_no);
  // formData.append('case_id',caseid);
  formData.append('doc_type',doc_type);
  formData.append('doc_value',doc_value);
  formData.append('type',type);
  // formData.append('uploaded_papers',uploaded_papers);
  formData.append('flag',type);
  var ajaxReq = $.ajax({
    url : host+"/multipleDocumentSubmit.php",
    type : 'POST',
    data : formData,
    cache : false,
    contentType : false,
    processData : false,
    success:function(response){
     $(".add_casesubmitdiv").removeClass("fetchdataloader");
     $(".add_casesubmit").show();
     $("#documentList").html('');
     $('input[type=file]').val('');

     var response_val = JSON.parse(response);
     var tr ='';

      // if(response_val.status == 'success')
      //   toastr.success("",'File Uploaded Successfully',{timeout:3000}); 
      // else
      //   toastr.error("",+response_val.status+,{timeout:5000});
      tr = '<table class="table table-bordered" id="documentList-table">';
      tr +='<thead><tr><th>Title</th><th>Size</th><th>Type</th><th>Uploaded By</th><th>Uploaded Date</th><th class="action_th">Action</th></tr></thead><tbody>';
      $.each(response_val.data,function(i,obj){
        $("#view_case_document").text(obj.doc_count);
        var filesize = bytesToSize(obj.size);

        tr +='<tr>';
        tr +='<td class="case-title"><span id="'+obj.doc_path+'" onclick=viewcases('+obj.doc_id+',this.id) class="viewDoc"><i class="feather icon-file-pdf-o "></i> '+obj.file_name+'</span></td>';
        tr +='<td class="case-title">'+filesize +'</td>';
        tr +='<td class="case-title">'+obj.type+'</td>';
        tr +='<td class="case-title">'+obj.uploaded_by+'</td>';
        tr +='<td class="case-title">'+obj.date_time+'</td>';
        tr+='<td>';
       // tr+='<a class="btn btn-outline-warning doc_action btn-mini form-group" style="margin-bottom:0px;"  href="'+host+'/editMercDocument.php?id='+obj.encrypted_case_id+'" title="Edit"><i class="feather icon-edit"></i></a>';
       if(obj.title == 'doc'){
         tr+= '<a class="btn btn-outline-warning doc_action btn-mini form-group" href="'+obj.doc_path+'" title="Download" style="margin-bottom:0px;" download><i class="feather icon-eye"></i></a>'; 
       }else{
        tr+= ' <button title="Download" type="button" class="doc_action viewBtn btn btn-outline-info btn-mini form-group" ><a href="'+getDocUrl(obj.doc_guid)+'" class="viewDoc" target="_blank" download><i class="feather icon-download"></i></a></button>';
      }
      tr+= ' <button type="button" onclick="deleteDoc('+obj.doc_id+')" title="Delete" class="doc_action btn btn-outline-danger btn-mini form-group"><i class="feather icon-trash"></i></button></td>';
    });

      tr+= '</tbody></table>';
        tr+= '<div class="col-sm-12" style="text-align:right;"><button class="btn btn-sm btn-primary newsubmit add_casesubmit"  type="button" id="temporary_id" name="Upload" onclick="addTemporaryCaseid( )">Add for Temporary Case Ids</button></div>';

      $("#documentList").html(tr);
      $("#documentList-table").dataTable();

    },
     complete: function(){
            $("#doc_type").val('');
          },
          error:function(){
      toastr.error("",'Error in uploading document',{timeout:5000});
      $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
    }
  });
}

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function getDocUrl(doc_name){
  if(!isEmpty(doc_name)){ 
         // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
      // $('#pdfUrl').attr('href',host+"/casepdf"+response.pdfFile); 
      return host+'/viewDocument.php?getid='+doc_name+'&&flag=alldoc';
    }
    else
     return 'images/business+costume+male+man+office+user+icon-1320196264882354682.png';
 }



 function deleteDoc(doc_id){
  var yes = confirm("Are you sure you want to delete document?");
  if(yes){
    $.ajax({
      url : host+"/delete_view_document.php",
      type:'POST',
      data : {
        doc_id : doc_id
      },
      dataType: 'json',
      cache : false,
      success:function(response){
        // console.log(response);
        if(response.status == 'success'){
          getUploadedDoc('m');
          toastr.success("","Successfully deleted the document",{timeout:5000});
        }
        else
          toastr.error("","Error in deleting the document",{timeout:5000});
      },
      error:function(){
        toastr.error("","Error in deleting the document",{timeout:5000});
      }
    });
  }else
  return false
}

function addTemporaryCaseid(){
   $.ajax({
      url : host+"/add_temporary_caseid.php",
      type:'POST',
      dataType: 'json',
      cache : false,
      success:function(response){
        // console.log(response);
        if(response.status == 'success'){
          
          toastr.success("","Successfully Added Case Id",{timeout:5000});
          getUploadedDoc('all');
          window.location.href='multipleCaseidReport.php';
        }
        else
          toastr.error("","Error in Adding Case Id",{timeout:5000});
      },
      error:function(){
        toastr.error("","Error in deleting the document",{timeout:5000});
      }
    });
}