var case_desc='',getcase='',petitioner_arr=[];


$(document).ready(function(){
	$("#merc_court").select2();
	getEditCaseDetails();
	$("input[name='appearing_radio']").change(function(){
   if($("#appearing_radio_one").is(":checked") ){
$("#respondent_div").show();
$("#petitioner_div").hide();
}else if($("#appearing_radio_two").is(":checked") ){
  $("#petitioner_div").show();
  $("#respondent_div").hide();
}
});
	if($("#litigationValue").val() == 'Litigations Filed by MSEDCL'){
		$("#respondent_div").show();
$("#petitioner_div").hide();
	}
	else if($("#litigationValue").val() == 'Litigations Filed against MSEDCL' ){
  $("#petitioner_div").show();
  $("#respondent_div").hide();
}
});
	 

var pet_count=0,petadv_count = 0,res_count=0,resadv_count=0;
var pa_counter = 0,res_counter = 0,res_adv_counter = 0,pet_counter = 0;
getDepartment();

function getEditCaseDetails(){
	debugger;
	var caseid = $("#caseid").val();
	var court_id = $("#court-id").val();
	$.ajax({
		url:host+"/edit_caseapi.php",
		dataType:'json',
		type : 'POST',
		async : false,
		data : {
			case_id : caseid
		},
		success : function(response){
			debugger;
			 
				 $.each(response.data,function(i,obj){
				 
				buildPetitioner(response.petitioners_data );
				// buildPetitionerAdv(response.pet_advocate_data,obj.are_you_appearing_as);
				buildRespondent(response.respondent_data );
				// buildRespondentAdv(response.res_advocate_data,obj.are_you_appearing_as);
				$("#litigationValue").val(obj.type_of_litigation);
				$("#case_no").val(obj.case_no);
				$("#title").val(obj.case_title);
				$("#dateoffilling").val(obj.date_of_filling);
				 
				// $("#case_no_year").val(obj.court_name);
				// $("#case_no_year").val(obj.case_no_year);
				// $("#merc_court").val(obj.case_no_year);
				getDropDownSelected(obj.court_name,'merc_court');
				// $("#merc_court").val(obj.court_name);
				// $('#merc_court').select2('data', {  text: obj.court_name});
				// var abcd = $("#merc_court").val($("#merc_court"+" option:contains("+obj.court_name+")").val());
				$("#risk_category").val(obj.risk_category);
				// case_desc = obj.case_description;
				$("#query").html(obj.case_description);
				// tinymce.get('query').setContent(obj.case_description);
				$("input[name='appearing_radio'][value='"+obj.type_of_litigation+"']").prop("checked",true);
				 
				getDropDownSelected(obj.case_no_year,'case_no_year');
				// $("div:not(#"+obj.court_id+")").hide();
			});
			},
error:function(e){
	toastr.error('Error in Updating Data',{timeout:5000});
}
});
}

 

function buildPetitioner(petitioners_data,app_as){
	var getpet = '<table id="petitioner_table" class="table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+

		'<td>Action</td>'+'</tr></thead><tbody>';
		if(!isEmpty(petitioners_data)){
			$.each(petitioners_data,function(i,obj){
				if(i==0){
					getpet +='<tr id="p-row-'+pet_count+'"><td>';
					getpet +='<input type="hidden" value="'+obj.pet_add_flag+'" id="your_name_addmore_'+pet_count+'">';
					getpet +='<input type="hidden" value="'+obj.pet_id+'" id="your_id'+pet_count+'">';
					getpet +='<input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value="'+obj.petitioner_name+'"/></td>'+

					'<td><input type="email" name="mail" class="form-control" id="your_email'+pet_count+'" value="'+obj.petitioner_email+'"/></td>'+
					'<td><input type="text" name="phone" minlength="10" maxlength="10" class="form-control" id="your_mobile'+pet_count+'" value="'+obj.petitioner_mobile+'"/></td>'+
					'<td></td></tr>';
				}

				else{
					getpet+='<tr id="p-row-'+pet_count+'"><td>';
					getpet +='<input type="hidden" value="'+obj.pet_add_flag+'" id="your_name_addmore_'+pet_count+'">';
					getpet +='<input type="hidden" value="'+obj.pet_id+'" id="your_id'+pet_count+'">';
					getpet+='<input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value="'+obj.petitioner_name+'"/></td>'+
					'<td><input type="text" name="mail" class="form-control" id="your_email'+pet_count+'" value="'+obj.petitioner_email+'"/></td>'+
					'<td><input type="text" name="phone" minlength="10" maxlength="10" class="form-control" id="your_mobile'+pet_count+'" value="'+obj.petitioner_mobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow("pet_id",'+obj.pet_id+','+pet_count+',"p-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				pet_count++;
			});
		}
		else {
			getpet+='<tr id="p-row-'+pet_count+'"><td>';
			getpet +='<input type="hidden" value="" id="your_name_addmore_'+pet_count+'">';
			getpet +='<input type="hidden" value="" id="your_id'+pet_count+'"><input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value=""/></td>'+

			'<td><input type="email" name="mail" class="form-control" id="your_email'+pet_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" minlength="10" maxlength="10" class="form-control" id="your_mobile'+pet_count+'" value=""/></td>'+
			'<td></td></tr>';
			pet_count++;
		}
		getpet+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=addPetitioner("your_name","your_email","your_mobile")> Add More </button></td></tr></tfoot></table>';
		$("#collapse-13").html(getpet);
	
}



function buildRespondent(respondent_data,app_as){
	 
		var getdata = '<table id="repondant_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+
		'<td>Action</td>'+'</tr></thead><tbody>';
		if(!isEmpty(respondent_data)){
			$.each(respondent_data,function(i,obj){
				if(i==0){
					getdata+='<tr id="res-row-'+res_count+'"><td>';
					getdata +='<input type="hidden" value="'+obj.res_add_flag+'" id="opponent_name_addmore_'+res_count+'">';
					getdata+='<input type="hidden" value="'+obj.res_id+'" id="opponent_id'+res_count+'"><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.respondent_name+'"/></td>'+
					'<td><input type="email" name="mail" class="form-control" id="opponent_email'+res_count+'" value="'+obj.respondent_email+'"/></td>'+
					'<td><input type="text" minlength="10" maxlength="10" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.respondent_mobile+'"/></td>'+
					'<td></td></tr>';
				}
				else{
					getdata+='<tr id="res-row-'+res_count+'"><td>';
					getdata +='<input type="hidden" value="'+obj.res_add_flag+'" id="opponent_name_addmore_'+res_count+'">';
					getdata+='<input type="hidden" value="'+obj.res_id+'" id="opponent_id'+res_count+'"><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.respondent_name+'"/></td>'+
					'<td><input type="email" name="mail" class="form-control" id="opponent_email'+res_count+'" value="'+obj.respondent_email+'"/></td>'+
					'<td><input type="text" minlength="10" maxlength="10" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.respondent_mobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow("res_id",'+obj.res_id+','+res_count+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				res_count++;
			}) ;
		}
		else{
			getdata+='<tr id="res-row-'+res_count+'"><td>';
			getdata +='<input type="hidden" value="" id="opponent_name_addmore_'+res_count+'">';
			getdata +='<input type="hidden" value="" id="opponent_id'+res_count+'"><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value=""/></td>'+

			'<td><input type="email" name="mail" class="form-control" id="opponent_email'+res_count+'" value=""/></td>'+
			'<td><input type="text" minlength="10" maxlength="10" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value=""/></td>'+
			'<td></td></tr>';
			res_count++;
		}
		getdata+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=addRespondant("opponent_name","opponent_email","opponent_mobile")> Add More </button></td></tr></tfoot></table>';
		$("#collapse-11").html(getdata);
	 

	 
}

 
function addPetitioner(nameid,emailid,mobileid){
	pet_count;
	var cols = "";

	cols += '<tr id="p-row-'+pet_count+'"><td>';
	cols += '<input type="hidden" class="form-control" id="'+nameid+'_addmore_'+pet_count+'" value="add"/>';
	cols += '<input type="text" class="form-control" id="'+nameid+pet_count+'"/></td>';
	cols += '<td><input type="email" class="form-control" id="'+emailid+pet_count+'"/></td>';
	cols += '<td><input type="text" minlength="10" maxlength="10" class="form-control" id="'+mobileid+pet_count+'"/></td>';

	cols += '<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow("pet_id","0",'+pet_count+',"p-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
	// pet_count = pet_counter;
	$("#petitioner_table tbody").append(cols);
}

function addRespondant(nameid,emailid,mobileid){
	res_count++;
	var cols = "";

	cols += '<tr id="res-row-'+res_count+'"><td>';
	cols += '<input type="hidden" class="form-control" id="'+nameid+'_addmore_'+res_count+'" value="add"/>';
	cols += '<input type="text" class="form-control" id="'+nameid+res_count+'"/></td>';
	cols += '<td><input type="email" class="form-control" id="'+emailid+res_count+'"/></td>';
	cols += '<td><input type="text" minlength="10" maxlength="10" class="form-control" id="'+mobileid+res_count+'"/></td>';

	cols += '<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow("res_id","0",'+res_count+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
	// res_count = res_counter;
	$("#repondant_table tbody").append(cols);

};

function respondantAdvAdd(nameid,emailid,mobileid){
	resadv_count++;
	var cols = "";

	cols += '<tr id="res-adv-row-'+resadv_count+'"><td>';
	cols += '<input type="hidden" class="form-control" id="'+nameid+'_addmore_'+resadv_count+'" value="add"/>';
	cols += '<input type="text" class="form-control" id="'+nameid+resadv_count+'"/></td>';
	cols += '<td><input type="email" class="form-control" id="'+emailid+resadv_count+'"/></td>';
	cols += '<td><input type="text" minlength="10" maxlength="10" class="form-control" id="'+mobileid+resadv_count+'"/></td>';

	cols += '<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow("resadv_id","0",'+resadv_count+',"res-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
	// resadv_count = res_adv_counter;
	$("#respondant_adv_table tbody").append(cols);    
};

function addPetitionerAdv(nameid,emailid,mobileid){
	petadv_count++;

	var cols = '<tr id="p-adv-row-'+petadv_count+'"><td>';
	cols += '<input type="hidden" class="form-control" id="'+nameid+'_addmore_'+petadv_count+'" value="add"/>';
	cols += '<input type="text" class="form-control" id="'+nameid+petadv_count+'"/></td>';
	cols += '<td><input type="email" class="form-control" id="'+emailid+petadv_count+'"/></td>';
	cols += '<td><input type="text" minlength="10" maxlength="10" class="form-control" id="'+mobileid+petadv_count+'"/></td>';

	cols += '<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow("petadv_id","0",'+petadv_count+',"p-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
	// petadv_count = pa_counter;
	$("#petitioneradv_table tbody").append(cols);

};

function deleteRow(unique_name,unique_id,count,id){
	$.ajax({
		type:'POST',
		url:'delete_editadvocate.php',
		data:{
			unique_name : unique_name,
			unique_id : unique_id
		},
		async:false,
		success:function(response){
			var delete_res = JSON.parse(response);
			if(delete_res.status == 'SUCCESS'){
				document.getElementById(id+"-"+count).outerHTML = '';
				toastr.success("","Successfully Deleted",{timeout:5000});
			}
			console.log("Delete",response);
		},
		error:function(){
			toastr.error("","Error fetching bench list",{timeout:5000});
		}
	});
}


function getBenchList(){
	var val = $("#high-court-list").val().split(",");
	var val_to_php = val[0];
	var val_to_api = val[1];
	$("#state_code").val(val_to_api);
	if(val_to_php != ''){
		$.ajax({
			type:'POST',
			url:'highcourtlist.php',
			data:'high_court='+val_to_php,
			async:false,
			success:function(response){
				var response = JSON.parse(response);
				console.log("Bench",response);
				if(response.length > 0){
					var option = '<option value="">Select</option>';
					$.each(response,function(i,obj){
						option += '<option value='+obj.bench_id+','+obj.master_id+'>'+obj.bench_name+'</option>';
					});
					$('#high_court_bench_list').html(option);
					// cnr_allow('No');
					// $("#bench").show();
					// $("#highcourt_side").hide();
				}
			},
			error:function(){
				toastr.error("","Error fetching bench list",{timeout:5000});
			}
		});
	}
}

/*function getSideList(){
	var val = $("#high_court_bench_list").val().split(",");
	var val_to_php = val[0];
	var val_to_api = val[1];

	$("#court_code").val(val_to_api);
	$("#dist_code").val(val_to_api);

	if(val_to_php !=''){
		$.ajax({
			type:'POST',
			url:'highcourtlist.php',
			data:'highcourt_bench='+val_to_php,
			async:false,
			success:function(response){
				var response = JSON.parse(response);
				if(response.length > 0){
					var option = '<option value="">Select</option>';
					$.each(response,function(i,obj){
						option += '<option value='+obj.side_id+'>'+obj.side_name+'</option>';
					});
					$("#highcourt_side").show();
					$("#high_court_side_list").html(option);

				}
			},
			error:function(){
				toastr.error("","Error fetching side",{timeout:5000});
			}
		});

	}
}*/

function validateEmail(mail,name) 
{
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
	{
		return true
	}
	else{
		alert("You have entered an invalid email address of "+name)
		return false
	}
}

var cnr_yes='';
function cnr_allow(val){	
	$("input[name='membershipRadios'][value='"+val+"']").prop("checked",true);
	var court_val = $("#cases-court_id").val();
	$(".court-wise-div").hide();
	if(val == 'Yes' && court_val == 2){
		$("."+court_val).hide(); 
		$("#otin").hide();
		$("#caseandyear").hide();
		$("#cnr_yes").show();
	}
	else if(val == 'No' && court_val == 2) {
		$("."+court_val).show(); 
		$("#otin").show();
		$("#caseandyear").show();
		$("#cnr_yes").hide();
		/*$("#supremecourt").hide();*/
	}
	else if(val == 'Yes' && court_val == 3){
		cnr_yes = 'Yes';
		$("."+court_val).hide();
		/*$("#supremecourt").hide();*/
		$("#caseandyear").hide();
		$("#otin").hide();
		$("#cnr_yes").show();

	}
	else if(val == 'No' && court_val == 3){
		$("."+court_val).show();
		if(cnr_yes == 'Yes'){
			$("#districtcourt_state").prop('disabled',false);
			$("#district_court_city").prop('disabled',false);
			$("#district_court_establishment").prop('disabled',false);
			$("#case_no_year").prop('disabled',false);
			$("#case_no").prop('disabled',false);
			cnr_yes == 'No';
		}
		else{
			$("#districtcourt_state").prop('disabled',true);
			$("#district_court_city").prop('disabled',true);
			$("#district_court_establishment").prop('disabled',true);
			$("#case_no_year").prop('disabled',true);
			$("#case_no").prop('disabled',true);
		}
		/*$("#name").val('');*/
		/*$("#supremecourt").show();
		$("#highcourt").hide(); */
		$("#caseandyear").show();
		$("#cnr_yes").hide();
		$("#otin").show();
		getDistrictWithoutCNR(cnr_allow_obj);
	}
	else{
		$("#otin").show();
		$("#caseandyear").show();
		$("#cnr_yes").hide();
	}
	/*$("#areyouappearingas").show();*/
}
var appearing_top_array = [];
function getAppearingInput(){
	appearing_top_array = [];
	$.each($("input[name='appearing_radio']"),function(i,obj){
		appearing_top_array.push($(this).val());
	});

	if ($("input[name='appearing_radio']").is(":checked")) {
		var val = $("input[name='appearing_radio']:checked").val();
		var val_not_checked = $("input[name='appearing_radio']:not(:checked)").val();
		var appear_radio = '<label for="exampleInputUsername1" class="col-sm-4 col-form-label">';
		appear_radio += val+'</label>';
		appear_radio +='<div class="col-sm-6 form-group">';
		appear_radio +='<input type="text" id="petitioner" class="form-control" value="" /><p id="sc_info">Kindly wait as it will take upto 1 minute to fetch the data</p></div>';
		// appear_radio +='<label for="exampleInputUsername1" class="col-sm-4 col-form-label">';
		// appear_radio +='Attach Document</label>';
		// appear_radio +='<div class="col-sm-6 ">';
		// appear_radio +='<input type="file" id="pet_file" aria-label="File browser example" style="margin-top: 4px;"></div>';
		appear_radio +='<div class="col-sm-2" id="otin">';
		appear_radio +='<div class="fetchLoaderDiv"></div>';
		appear_radio +='<button type="button" class=" btn btn-info btn-sm fetchData_outer" style="float: right;" id="fetchData_outer" onclick=datafetch()>Fetch Data </button>';
		appear_radio +=' </div>';    
		$("#appearingas_input").html(appear_radio);
		$("#appearingas_input").show();
		$(".appearing_as_first").text(val);

		$(".appearing_as_second").text(val_not_checked);
		// submit_caseapi();

		if(($("#cnr_div").css('display') == 'block' && $("input[name='membershipRadios']:checked").val() == 'Yes'))
			$("#otin").hide();
		else if(($("#cases-court_id").val() == 2 || $("#cases-court_id").val() == 3 || $("#cases-court_id").val() == 5) && $("input[name='membershipRadios']:checked").val() == 'Yes')
			$("#otin").hide();
		else
			$("#otin").show();
	}
}

function getAppearingModel(){
	$.ajax({
		type:'POST',
		url:'select_appearing_model.php',
		async:false,
		success:function(response){       
			var response = JSON.parse(response);        
        // console.log(response);
        var option = '';
        $.each(response,function(i,obj){
        	option +='<option value='+obj.appmodid+'>'+obj.app_model+'</option>';
        });
        $("#appearing_modal").html(option);
    },
    error:function(){
    	toastr.error("","Error fetching appearing model",{timeout:5000});
    }
});
	
	/*var appearingasmodel = document.getElementById('appearing_modal');
	setSelectedValue(appearingasmodel,appearingmodel);*/
}

/*function getCaseTypeList(){
	var val = $("#high_court_side_list").val();
	if(val !=''){
		$.ajax({
			type:'POST',
			url:'highcourtlist.php',
			data:'highcourt_side='+val,
			async:false,
			success:function(response){     
				var response = JSON.parse(response);
				if(response.length > 0){
					var option = '<option value="">Select</option>';
					$.each(response,function(i,obj){
						option += '<option value='+obj.case_typeid+'>'+obj.case_type+'</option>';
					});   
					option +='<option value="0">Other</option>';  

					$("#high_court_casetype").html(option);
					$("#highcourt_casetype").show();
				}
			},
			error:function(){
				toastr.error("","Error fetching casetype",{timeout:5000});
			}
		});
	}
}*/

// function getCaseYear(caseyear){
// 	var year = document.getElementById('case_no_year');
// 	setSelectedValue(year,caseyear); 
// }

/*function getHighCourt(highcourt){
	var highcourt_select = document.getElementById('high-court-list');
	setSelectedValue(highcourt_select,highcourt); 
	
}*/

/*function getBench(benchname){
	var bench = document.getElementById('high_court_bench_list');
	setSelectedValue(bench,benchname); 
}*/

/*function getSide(side){
	var highcourt_side = document.getElementById('high_court_side_list');
	setSelectedValue(highcourt_side,side); 
	
}*/

/*function getStampRegister(stampname){
	var stamp = document.getElementById('cases-high_court_id');
	setSelectedValue(stamp,stampname); 
}

function getCaseTypeHC(caseTypeName){
	var caseType = document.getElementById('');
	setSelectedValue(caseType,caseTypeName); 
}*/

function getDropDownSelected(courtname,courtId){
	/*var court = document.getElementById('cases-court_id');
	setSelectedValue(court,courtname); */
	var abcd = $("#"+courtId).val($("#"+courtId+" option:contains("+courtname+")").val());
	// $("#"+courtId).select2("val", $("#"+courtId+" option:contains("+courtname+")").val());
	$("#"+courtId).select2().trigger('change');
}

function getCaseTypeSelected(courtname,courtId){
	$("#"+courtId+" option:contains("+courtname+")").filter(function(){
		/*var val = $(this).text() === courtname ? true : false;*/
    /*if(val){
      $("#"+courtId).val($("#"+courtId+" option:contains("+$(this).text()+")").val());
      $("#"+courtId).select2().trigger('change');
  }*/

  return $(this).text() === courtname;
}).attr("selected", "selected");
}

/*function getSideSelected(courtname,courtId){

}*/
/*function getState(statename){
	var state = document.getElementById('districtcourt_state');
	setSelectedValue(state,statename); 
	
}*/

function buildDistrict(){
	var val = $("#districtcourt_state").val().split("_");
	var val_to_php = val[0];
	var val_to_api = val[1];
  // $("#dist_code").val(val);
  $.ajax({
  	type:'POST',
  	url:'ajaxData.php',
  	data:'country_id='+val_to_php,
  	async:false,
  	success:function(response){
  		$("#state_code").val(val_to_api);
  		$('#district_court_city').html(response);
  		// $('#district_court_district').html('<option value="">Select District first</option>'); 
  	},
  	error:function(){
  		toastr.error("","Error fetching city",{timeout:5000});
  	}
  }); 
}

/*function getDistrict(districtname){
	var district = document.getElementById('district_court_city');
	setSelectedValue(district,districtname);
	
}*/

function getDepartment(){
	$.ajax({
		type:'POST',
		url:'department_api.php',
		async:false,
		success:function(response){
        // console.log(response);
        var response = JSON.parse(response);
        var option = '<option value="">Select Department</option>';
        $.each(response,function(i,obj){
        	option +='<option value='+obj.dep_id+'>'+obj.department_name+'</option>';
        });
        $("#department").html(option);
    },
    error:function(){
    	toastr.error("","Error fetching department",{timeout:5000});
    }
}); 
}

function mahastate(name){
	document.getElementById('circuit').style.display="flex";
}

/*function buildEstablishment(){
	var val = $("#district_court_city").val().split("_");
	var val_to_php = val[0];
	var val_to_api = val[1];
	$("#dist_code").val(val_to_api);
	$.ajax({
		type:'POST',
		url:'establisment.php',
		data:'estais_id='+val_to_php,
		async:false,
		success:function(response){
			$('#district_court_establishment').html(response); 
		},
		error:function(){
			toastr.error("","Error fetching establisment",{timeout:5000});
		}
	}); 
}*/

/*function getCourtEstablishment(establishmentname){
	var establishment = document.getElementById('district_court_establishment');
	setSelectedValue(establishment,establishmentname);
	
}*/

/*function buildCaseType(){
  debugger;
	var val = $("#district_court_establishment").val().split("_");
	var val_to_php = val[0];
	var val_to_api = val[1];
	$("#court_code").val(val_to_api);
	if(val_to_php == 0)
		$("#district_court_casetype_other").show();
	else
		$("#district_court_casetype_other").hide();
	$.ajax({
		type:'POST',
		url:'district_case_type.php',
		data:'est_id='+val_to_php,
    async:false,
		success:function(response){
			console.log(response);
			var response = JSON.parse(response);
			var option = '<option>Please select case type</option>';
			$.each(response,function(i,obj){
				option+='<option value="'+obj.master_id+'">'+obj.case_type+'</option>';
			})
			$('#district_court_casetype').html(option);

      // $('#district_court_district').html('<option value="">Select District first</option>'); 
    },
    error:function(){
     toastr.error("","Error fetching case type",{timeout:5000});
   }
 });
}*/

/*function getCaseType(establishmentname){
	var establishment = document.getElementById('district_court_casetype');
	setSelectedValue(establishment,establishmentname);
}

function getTribunals(tribunalName){
	var tribunal = document.getElementById('tribunal_select');
	setSelectedValue(tribunal,tribunalName);
	
}

function getTribunalState(tribunalStateName){
	var tribunalState = document.getElementById('tribunal_state');
	setSelectedValue(tribunalState,tribunalStateName);
}

function getTribunalCaseType(tribunalCaseTypeName){
	var tribunalcasetype = document.getElementById('tribunal_case');
	setSelectedValue(tribunalcasetype,tribunalCaseTypeName);
}*/

function buildTribunalStateList(){
	var tribunal = $("#tribunal_select").val();
	$.ajax({
		type:'POST',
		url:'ajax_tribunals.php',
		data:'tribunals='+tribunal,
		async:false,
		success:function(response){
			// console.log(response);
			var response = JSON.parse(response);
			if(response.length > 0){
				$("#tribunal_state_div").show();
				var option ='<option value="">Please Select </option>';
				$.each(response,function(i,obj){
					/*option +='<option value="'+obj.t_state_id+'">'+obj.t_state_name+'</option>';*/
					option +='<option value="'+obj.t_state_id+'_'+obj.state_id+'">'+obj.t_state_name+'</option>';
				});
				$('#tribunal_state').html(option); 
			}
			else
				$("#tribunal_state_div").hide();
			tribunalState();          
		},
		error:function(){
			toastr.error("","Error fetching tribunal state",{timeout:5000});
		}
	});       
}

/*function tribunalCasetype(){
	debugger;
	var tribunal = $("#tribunal_select").val();
	$.ajax({
		type:'POST',
		url:'ajaxdata/tribunal_case_type.php',
		data:'tribunals_id='+tribunal,
		async:false,
		success:function(response){
			// console.log(response);
			var response = JSON.parse(response);
			if(response.length > 0){
				var option ='<option value="">Please Select </option>';
				$.each(response,function(i,obj){
					option +='<option value="'+obj.case_type_id+'">'+obj.tribunals_name+'</option>';
				});
				$('#tribunal_case').html(option);
				$("#case-type-tribunal").show();
			}
			else
				$("#case-type-tribunal").hide();
		}
	});     
}*/

function getSCType(typeName){
	/*var scType = document.getElementById('supreme_court');
	setSelectedValue(scType,typeName);*/
	$("#supreme_court").val($("#supreme_court option:contains("+typeName+")").val());
	$("#supreme_court").select2().trigger('change');
}

function setSelectedValue(selectObj, valueToSet) {
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].text== valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}

function datafetch(){
	var party1 = appearing_top_array[0];
	var party2 = appearing_top_array[1];
	var name = $("#name").val();
	var court = $("#cases-court_id").val();
	var cinos = encryptData(name);
	var cino =new Object();
	cino.cino=cinos;
	cino.courtType=$("#court_type").val();
	var districtc = name.substring(0,4);
	
	if(court == 1){
		$("#sc_info").show();
		getSCCases(party1,party2);
	}
	else if(court == 5){
		getTribunalCourtCasesFetchData(party1,party2);
	}
	else if(court == 2){
		getCasesHighCourt(party1,party2);
	}
	else if(court == 3){

		if($("input[name='membershipRadios']:checked").val() == 'Yes'){
			if(court == 3 && name == ''){
				alert("Please enter CNR no.");
				$("#data").css('display', 'none');

			}
			if(name !=''){
				$(".fetchLoaderDiv").addClass("fetchdataloader");
				$("#fetch_cnr_data_id").hide(); 
				getDistrictCourtCases(cino,party1,party2);
			}
		} 

		else{
			getCaseNumberFetchDataCases(party1,party2);
		}
	}

	else{
		$("#sc_info").hide();
		$(".fetchLoaderDiv").removeClass("fetchdataloader");
		$("#fetchData_outer").show();
		toastr.info("","Unable to fetch data through provided CNR",{timeout:5000});
		$("#data").modal("hide");
	}
}

function getCaseNumberFetchDataCases(party1,party2){
	var state = $("#districtcourt_state").val().split("_");
	var state_code = encryptData(state[1]);
	var district = $("#district_court_city").val().split("_");
	var dist_code = encryptData(district[1]);
	var court = $("#district_court_establishment").val().split("_");  
	var courtCode = encryptData(court[1]);
	var courtType = $("#court_type").val();
	var appeal_type = $("#district_court_casetype").val();
	var appeal_no = $("#case_no").val();
	var appeal_year = $("#case_no_year").val();

	var caseTypeVal = encryptData(appeal_type);
	var caseNumber = encryptData(appeal_no);
	var year = encryptData(appeal_year);

	$(".fetchLoaderDiv").addClass("fetchdataloader");
	$("#fetchData_outer").hide();
	$.ajax({
		type: "GET",
		url: legalResearch+'/search_by_casenumber',
		data: {
			state_code : state_code,
			dist_code : dist_code,
			court_code : courtCode,
			courtType : courtType,
			caseTypeVal : caseTypeVal,
			caseNumber : caseNumber,
			year : year
		},
		cache: false,
		async:false,
		headers: {
			"Content-Type": 'application/json'
		},

		success: function(response){
			if(isEmpty(response)){
				$(".fetchLoaderDiv").removeClass("fetchdataloader");
				$("#fetchData_outer").show();
				toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
			}
			if(response.caseNos.length > 0){
				getcase = JSON.stringify(response.caseNos);
				var showdata ='';
				$.each(response.caseNos,function(i,obj){    
					getCaseByCNRFetchCase(obj.cino,party1,party2);
				});
			}
			else{
				$(".fetchLoaderDiv").removeClass("fetchdataloader");
				$("#fetchData_outer").show();
				toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
			}
		},
		error:function(e){
			$(".fetchLoaderDiv").removeClass("fetchdataloader");
			$("#fetchData_outer").show();
			toastr.error("","Site is slow please try after some time",{timeout:5000});
		}

	});
}

function getTribunalCourtCasesFetchData(party1,party2){
	var caseNumber = isEmpty($("#case_no").val()) ? "0" : $("#case_no").val();
	var year = isEmpty($("#case_no_year").val()) ? "0" : $("#case_no_year").val();
	var party_name_search = "";
	var data = {
		party : party_name_search,
		case_number : caseNumber,
		case_year : year
	}
	var url = legalResearch+'/mat_search_by_party';
	$.ajax({
		type: "GET",
		url: url,
		data: data,
		cache: false,
		async:false,
		headers: {
			"Content-Type": 'application/json'
		},
    // contentType: 'application/json;utf-8',   
    beforeSend : function(){
    	$(".loader-div").show();
    },
    success: function(response){
    	document.getElementById("fetch_case_modal").reset();
    	console.log(response);
    	var tr_party = '';
    	if($("#cases-court_id").val() == 5){
    		tribunal_search = response;
    		var showdata ='';
    		showdata += '<form id="reg-form" name="reg-form"><table class="table table-bordered pull-data-popup" style=""><tbody style="">';
    		showdata += '<tr id="row1" style=""><td class="no-padding">'+'<table class="no-bordered"><tbody>';
    		showdata += '<tr class="row-inner">';
    		showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td><input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+tribunal_appleant+'"></td>';
    		showdata += '<td data-th="Full Name"><label>Full Name:</label>';
    		showdata += '<input type="text" class="rowVal fullname" value="'+tribunal_appleant+'" name="petitionerorRespondentName" id="sel_pet_name_0"></td>';
    		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
    		showdata += '<input type="email" class="rowVal email" value="" id="sel_pet_email_0" name="petitionerorRespondentNameemail" ></td>';
    		showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
    		showdata += '<input type="text" class="rowVal phonenumber" id="sel_pet_mobile_0" minlength="10" maxlength="10" value="" name="petitionerorRespondentphone"></td></tr>';

    		showdata +='</tbody></table></td>'+
    		'<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
    		'<select id="sel_pet" class="form-control" style="" name="Petitionerrespom">'+
    		'<option value="">Please select</option>'+
    		'<option value="Petitioner">Petitioner</option>'+
    		'<option value="Respondent">Respondent</option>'+
    		'</select></td></tr>';
    		showdata +='<tr id="row1" style=""><td class="no-padding">'+
    		'<table class="no-bordered"><tbody>';
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
    		showdata += '<input type="hidden" value="" name="cnr_no">';
    		showdata += '<td><input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+tribunal_respondent+'"></td>';
    		showdata += '<td data-th="Full Name"><label>Full Name:</label>';
    		showdata += '<input type="text" class="rowVal fullname" value="'+tribunal_respondent+'" name="petitionerorRespondentName" id="sel_res_name_0"></td>';
    		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
    		showdata += '<input type="email" class="rowVal email" value="" id="sel_res_email_0" name="petitionerorRespondentNameemail" > </td>';
    		showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
    		showdata += '<input type="text" class="rowVal phonenumber" value="" id="sel_res_mobile_0" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';

    		showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
    		'<td class="td_dd" style="">'+
    		'<select id="sel_res" class="form-control" style="" name="Petitionerrespomr">'+
    		'<option value="">Please select</option>'+
    		'<option value="Petitioner">Petitioner  </option>'+
    		'<option value="Respondent">Respondent  </option>'+
    		'</select></td></tr>';

    		showdata +='</tbody></table><div class="submit_data form-group" id="submit_data">'+
    		'<input type="button" class="btn newsubmit btn-info btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
    		'</div></form>';

    		$("#modal-data").html(showdata);
    		$("#data").modal('show');
    	}
    },
    error:function(){

    }
});

}

function getCasesHighCourt(party1,party2){
	$(".fetchLoaderDiv").addClass("fetchdataloader");
	$("#fetchData_outer").hide();
	$.ajax({
		type:'GET',
		url:legalResearch+'/search_by_bom',
		data:{
			stamp : $("#cases-high_court_id option:selected").text(),
			party : "",
			side : $("#high_court_side_list option:selected").text(),
			bench : $("#high_court_bench_list option:selected").text(),
			cType : $("#high_court_casetype").val(),
			cYear :  $("#case_no_year").val(),
			cNo : $("#case_no").val(),
			sType : "FETCH",
			id : ""
		},
		success:function(response){
			getcase = response;
			$("#anycondition").modal('hide');
			var showdata ='';
			if(response.length > 0){
				$.each(response,function(i,obj){
					const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
					"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
					];
					if(!isEmpty(obj.fillingDate)){
						$("#dateoffilling").val(obj.fillingDate);
					}

					showdata += '<form id="reg-form" name="reg-form"><table class="table table-bordered pull-data-popup" style=""><tbody style="">';

					if(!isEmpty(obj.petioner)){
						showdata += '<tr id="row1" style=""><td class="no-padding">'+'<table class="no-bordered"><tbody>';

						$.each(obj.petioner,function(index,object){
							showdata += '<tr class="row-inner">';
							showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
							showdata += '<td><input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+object+'"></td>';
							showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
							showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_pet_name_'+index+'"></td>';
							showdata += '<td data-th="Email Address"><label>Email Address:</label>';
							showdata += '<input type="email" class="rowVal email" value="" id="sel_pet_email_'+index+'" name="petitionerorRespondentNameemail" ></td>';
							showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
							showdata += '<input type="text" class="rowVal phonenumber" id="sel_pet_mobile_'+index+'" minlength="10" maxlength="10" value="" name="petitionerorRespondentphone"></td></tr>';
						});
						showdata +='</tbody></table></td>'+
						'<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
						'<select id="sel_pet" class="form-control" style="" name="Petitionerrespom">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'">'+party1+'</option>'+
        '<option value="'+party2+'">'+party2+'</option>'+
        '</select></td></tr>';
    }
    if(!isEmpty(obj.respondent)){
    	showdata +='<tr id="row1" style=""><td class="no-padding">'+
    	'<table class="no-bordered"><tbody>';
    	$.each(obj.respondent,function(index,object){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
    		showdata += '<input type="hidden" value="" name="cnr_no">';
    		showdata += '<td><input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+object+'"></td>';
    		showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
    		showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_res_name_'+index+'"></td>';
    		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
    		showdata += '<input type="email" class="rowVal email" value="" id="sel_res_email_'+index+'" name="petitionerorRespondentNameemail" > </td>';
    		showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
    		showdata += '<input type="text" class="rowVal phonenumber" value="" id="sel_res_mobile_'+index+'" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';
    	});
    	showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
    	'<td class="td_dd" style="">'+
    	'<select id="sel_res" class="form-control" style="" name="Petitionerrespomr">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'">'+party2+'  </option>'+
        '<option value="'+party1+'">'+party1+'  </option>'+

        '</select></td></tr>';
    }
    if(!isEmpty(obj.petionerAdv)){
    	showdata += '<tr id="row1" style="">';
    	showdata += '<td style="border-right: 1px solid #ccc;">';
    	showdata += '<table class="no-bordered"><tbody>';


    	$.each(obj.petionerAdv,function(index,object){

    		showdata += '<tr class="row-inner">';
    		showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td><input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+object+'"></td>';
    		showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
    		showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_petadv_name_'+index+'"></td>';
    		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
    		showdata +='<input type="email" class="rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+index+'" > </td>';
    		showdata +='<td data-th="Phone Number"><label>Phone Number:</label>';
    		showdata +='<input type="text" class="rowVal phonenumber" value="" id="sel_petadv_mobile_'+index+'" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';
    	});
    	showdata +='</tbody></table></td>';
    	showdata +='<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
    	showdata +='<select id="sel_petadv" class="form-control" style="" name="Petitionerrespompa">'+
        // showdata +='<option value="">Please select</option>'+
        '<option value="'+party1+'Adv">'+party1+' Advocate</option>'+
        '<option value="'+party2+'Adv">'+party2+' Advocate</option>'+
        '</select></td></tr>';
    }
    if(!isEmpty(obj.respondentAdv)){
    	showdata +='<tr id="row1" style="">'+
    	'<td style="border-right: 1px solid #ccc;">'+
    	'<table class="no-bordered"><tbody>';
    	$.each(obj.respondentAdv,function(index,object){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
    		showdata += '<input type="hidden" value="" name="cnr_no"><td>';
    		showdata += '<input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+object+'"></td>';
    		showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
    		showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_resadv_name_'+index+'"></td>';
    		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
    		showdata += '<input type="email" class="rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_resadv_email_'+index+'" > </td>';
    		showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
    		showdata += '<input type="text" class="rowVal phonenumber" value="" id="sel_resadv_mobile_'+index+'" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';        
    	});
    	showdata += '</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
    	showdata += '<select id="sel_resadv" class="form-control" style="" name="Petitionerrespompa">'+
        // showdata +='<option value="">Please select</option>'+
        '<option value="'+party2+'Adv">'+party2+' Advocate</option>'+
        '<option value="'+party1+'Adv">'+party1+' Advocate</option>'+
        '</select></td></tr>';
    }
    showdata +='</tbody></table><div class="submit_data form-group" id="submit_data">'+
    '<input type="button" class="btn newsubmit btn-info btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
    '</div></form>';
});
$("#modal-data").html(showdata);
$("#data").modal('show');
$(".fetchLoaderDiv").removeClass("fetchdataloader");
$("#fetchData_outer").show();
}
else{
	$(".fetchLoaderDiv").removeClass("fetchdataloader");
	$("#fetchData_outer").show();
	toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});
}
},
error:function(){
	$(".fetchLoaderDiv").removeClass("fetchdataloader");
	$("#fetchData_outer").show();
	toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});
}
});
}

function getDistrictCourtCases(cino,party1,party2){
	$.ajax({
		type: "GET",
		url: legalResearch+"/search_by_cnr",
		data: cino,
		cache: false,
		async:false,
		headers: {
			"Content-Type": 'application/json'
		},    
		success: function(get){
			if(get!='' || get.length > 0){
				getcase = get;
				var showdata ='';
				var casedetail = '<div class="col-sm-12"><b>State: </b><span>'+get.state_name+'</span></div><br>'+
				'<div class="col-sm-12"><b>District: </b> <span> '+get.district_name+'</span></div><br>'+
				'<div class="col-sm-12"><b> Court Establishment: </b> <span> Other</span></div><br>'+
				'<div class="col-sm-12"><b> Case Type: </b> <span> '+get.type_name+'</span></div><br>'+
				'<div class="col-sm-12"><b> Case Number: </b> <span> '+get.reg_no+'</span></div><br>'+
				'<div class="col-sm-12"><b> Case Year: </b> <span> '+get.reg_year+'</span></div><br>';
				$("#casedetails").html(casedetail);
				$("#casedetails-div").show();
				$("#case_insert_show").show();

				const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
				];
				if(!isEmpty(get.date_of_filing)){
					$("#dateoffilling").val(get.date_of_filing);
				}
				$("#nominal").val(get.pet_name+' vs '+ get.res_name);
				$("#case_no").val(get.reg_no);

				$('#case_no_year').val(get.reg_year);
				$('#case_no_year').select2().trigger('change');

				/*$("#district_court_casetype").val($("#district_court_casetype option:contains("+get.type_name+")").val());
				$("#district_court_casetype").select2().trigger('change');*/

				showdata += '<form id="reg-form" name="reg-form"><table class="table table-bordered pull-data-popup" style=""><tbody style=""><tr id="row1" style="">'+
				'<td class="no-padding">'+'<table class="no-bordered"><tbody>';
				if(get.petitioner_array.length > 0){
					$.each(get.petitioner_array,function(i,obj){
						showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
						showdata += '<td>';
						showdata +='<div class="checkbox-zoom zoom-primary">';
						showdata +='<label>';
						showdata +='<input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+obj+'">';
						showdata +='<span class="cr">';
						showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
						showdata +=' </i>';
						showdata +=' </span>';
						showdata +=' </label>';
						showdata +='</div></td>';

						showdata+='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+obj+'" name="petitionerorRespondentName" id="sel_pet_name_'+i+'"></td>'+
						'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" id="sel_pet_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" id="sel_pet_mobile_'+i+'" value="" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';
					});

					showdata +='</tbody></table></td>'+
					'<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
					'<select id="sel_pet" class="form-control" style="" name="Petitionerrespom">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'">'+party1+'</option>'+
        '<option value="'+party2+'">'+party2+'</option>'+
        '</select></td></tr>';
    }
    if(get.respondent_array.length > 0){
    	showdata +='<tr id="row1" style=""><td class="no-padding">'+
    	'<table class="no-bordered"><tbody>';
    	$.each(get.respondent_array,function(i,res){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td>';
    		showdata +='<div class="checkbox-zoom zoom-primary">';
    		showdata +='<label>';
    		showdata +='<input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+res+'">';
    		showdata +='<span class="cr">';
    		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
    		showdata +=' </i>';
    		showdata +=' </span>';
    		showdata +=' </label>';
    		showdata +='</div></td>';
    		showdata+='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+res+'" name="petitionerorRespondentName" id="sel_res_name_'+i+'"></td>'+
    		'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" id="sel_res_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" value="" minlength="10" maxlength="10" id="sel_res_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';
    	}) ;
    	showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
    	'<td class="td_dd" style="">'+
    	'<select id="sel_res" class="form-control" style="" name="Petitionerrespomr">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'">'+party2+'</option>'+
        '<option value="'+party1+'">'+party1+'</option>'+
        
        '</select></td></tr>';
    }

    if(get.petitioner_adv_array.length > 0){
    	showdata +='<tr id="row1" style="">'+
    	'<td class="no-padding">'+
    	'<table class="no-bordered"><tbody>';

    	$.each(get.petitioner_adv_array,function(i,peta){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td>';
    		showdata +='<div class="checkbox-zoom zoom-primary">';
    		showdata +='<label>';
    		showdata +='<input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+peta+'">';
    		showdata +='<span class="cr">';
    		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
    		showdata +=' </i>';
    		showdata +=' </span>';
    		showdata +=' </label>';
    		showdata +='</div></td>';
    		showdata+='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+peta+'" name="petitionerorRespondentName" id="sel_petadv_name_'+i+'"></td>'+
    		'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+i+'" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" value="" minlength="10" maxlength="10" id="sel_petadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';      
    	}) ;
    	showdata +='</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
    	'<select id="sel_petadv" class="form-control" style="" name="Petitionerrespompa">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'Adv">'+party1+' Advocate</option>'+
        '<option value="'+party2+'Adv">'+party2+' Advocate</option>'+
        '</select></td></tr>';
    }
    if(get.respondent_adv_array.length > 0){
    	showdata +='<tr id="row1" style="">'+
    	'<td class="no-padding">'+
    	'<table class="no-bordered"><tbody>';

    	$.each(get.respondent_adv_array,function(i,resa){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td>';
    		showdata +='<div class="checkbox-zoom zoom-primary">';
    		showdata +='<label>';
    		showdata +=' <input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+resa+'">';
    		showdata +='<span class="cr">';
    		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
    		showdata +=' </i>';
    		showdata +=' </span>';
    		showdata +=' </label>';
    		showdata +='</div></td>';
    		showdata+='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+resa+'" name="petitionerorRespondentName" id="sel_resadv_name_'+i+'"></td>'+
    		'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" id="rsel_resadv_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" minlength="10" maxlength="10" value="" id="sel_resadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>'      
    	});

    	showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+'<td class="td_dd" style="">'+
    	'<select id="sel_resadv" class="form-control" style="" name="Petitionerrespomra">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'Adv">'+party2+' Advocate</option><option value="'+party1+'Adv">'+party1+' Advocate</option>'+
        '</select></td></tr>';
    }
    showdata +='</tbody></table><div class="submit_data form-group" id="submit_data">'+
    '<input type="button" class="btn newsubmit btn-info btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
    '</div></form>';
    $("#modal-data").html(showdata);
    $("#data").modal('show');
    $(".fetchLoaderDiv").removeClass("fetchdataloader");
    $("#fetch_cnr_data_id").show();
}
else{ 
	$(".fetchLoaderDiv").removeClass("fetchdataloader");
	$("#fetch_cnr_data_id").show();
	toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
}
},

error:function(e){
	$(".fetchLoaderDiv").removeClass("fetchdataloader");
	$("#fetch_cnr_data_id").show();
	toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
}
});
}

function getCaseByCNRFetchCase(cnr_no,party1,party2){
	var cino =new Object();
	cino.cino=encryptData(cnr_no);
	cino.courtType=$("#court_type").val();
	$(".fetchLoaderDiv").addClass("fetchdataloader");
	$("#fetchData_outer").hide();
	$.ajax({
		type: "GET",
		url: legalResearch+"/search_by_cnr",
    // data: "cino="+cino+"&&courtType=SC",
    data: cino,
    cache: false,
    async:false,
    headers: {
    	"Content-Type": 'application/json'
    },
    success: function(get){
    	if(get!='' || get.length > 0){
    		getcase = get;
    		var showdata ='';
    		var casedetail = '<div class="col-sm-12"><b>State: </b><span>'+get.state_name+'</span></div><br>'+
    		'<div class="col-sm-12"><b>District: </b> <span> '+get.district_name+'</span></div><br>'+
    		'<div class="col-sm-12"><b> Court Establishment: </b> <span> '+get.court_name+'</span></div><br>'+
    		'<div class="col-sm-12"><b> Case Type: </b> <span> '+get.type_name+'</span></div><br>'+
    		'<div class="col-sm-12"><b> Case Number: </b> <span> '+get.reg_no+'</span></div><br>'+
    		'<div class="col-sm-12"><b> Case Year: </b> <span> '+get.reg_year+'</span></div><br>';
    		$("#casedetails").html(casedetail);
    		
    		$("#casedetails").show();

    		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    		"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
    		];
    		if(!isEmpty(get.date_of_filing)){
    			$("#dateoffilling").val(get.date_of_filing);
    		}
    		$("#nominal").val(get.pet_name+' vs '+ get.res_name);
    		$("#case_no").val(get.reg_no);

    		$('#case_no_year').val(get.reg_year);
    		$('#case_no_year').select2().trigger('change');

    		/*$("#district_court_casetype").val($("#district_court_casetype option:contains("+get.type_name+")").val());
    		$("#district_court_casetype").select2().trigger('change');*/

    		showdata += '<form id="reg-form" name="reg-form"><table class="table table-bordered pull-data-popup" style=""><tbody style=""><tr id="row1" style="">'+
    		'<td class="no-padding">'+'<table class="no-bordered"><tbody>';
    		if(get.petitioner_array.length > 0){
    			$.each(get.petitioner_array,function(i,obj){
    				showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    				showdata += '<td>';
    				showdata +='<div class="checkbox-zoom zoom-primary">';
    				showdata +='<label>';
    				showdata +=' <input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+obj+'">';
    				showdata +='<span class="cr">';
    				showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
    				showdata +=' </i>';
    				showdata +=' </span>';
    				showdata +=' </label>';
    				showdata +='</div></td>';
    				showdata +='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+obj+'" name="petitionerorRespondentName" id="sel_pet_name_'+i+'"></td>'+
    				'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" id="sel_pet_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" id="sel_pet_mobile_'+i+'" value="" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';
    			});

    			showdata +='</tbody></table></td>'+
    			'<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
    			'<select id="sel_pet" class="form-control" style="" name="Petitionerrespom">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'">'+party1+'</option>'+
        '<option value="'+party2+'">'+party2+'</option>'+
        '</select></td></tr>';
    }
    if(get.respondent_array.length > 0){
    	showdata +='<tr id="row1" style=""><td class="no-padding">'+
    	'<table class="no-bordered"><tbody>';
    	$.each(get.respondent_array,function(i,res){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td>';
    		showdata +='<div class="checkbox-zoom zoom-primary">';
    		showdata +='<label>';
    		showdata +=' <input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+res+'">';
    		showdata +='<span class="cr">';
    		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
    		showdata +=' </i>';
    		showdata +=' </span>';
    		showdata +=' </label>';
    		showdata +='</div></td>';
    		showdata+='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+res+'" name="petitionerorRespondentName" id="sel_res_name_'+i+'"></td>'+
    		'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" id="sel_res_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" value="" id="sel_res_mobile_'+i+'" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';
    	}) ;
    	showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
    	'<td class="td_dd" style="">'+
    	'<select id="sel_res" class="form-control" style="" name="Petitionerrespomr">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'">'+party2+'</option>'+
        '<option value="'+party1+'">'+party1+'</option>'+

        '</select></td></tr>';
    }

    if(get.petitioner_adv_array.length > 0){
    	showdata +='<tr id="row1" style="">'+
    	'<td class="no-padding">'+
    	'<table class="no-bordered"><tbody>';

    	$.each(get.petitioner_adv_array,function(i,peta){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td>';
    		showdata +='<div class="checkbox-zoom zoom-primary">';
    		showdata +='<label>';
    		showdata +=' <input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+peta+'">';
    		showdata +='<span class="cr">';
    		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
    		showdata +=' </i>';
    		showdata +=' </span>';
    		showdata +=' </label>';
    		showdata +='</div></td>';
    		showdata +='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+peta+'" name="petitionerorRespondentName" id="sel_petadv_name_'+i+'"></td>'+
    		'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+i+'" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" value="" minlength="10" maxlength="10" id="sel_petadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>';      
    	}) ;
    	showdata +='</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
    	'<select id="sel_petadv" class="form-control" style="" name="Petitionerrespompa">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party1+'Adv">'+party1+' Advocate</option>'+
        '<option value="'+party2+'Adv">'+party2+' Advocate</option>'+
        '</select></td></tr>';
    }
    if(get.respondent_adv_array.length > 0){
    	showdata +='<tr id="row1" style="">'+
    	'<td class="no-padding">'+
    	'<table class="no-bordered"><tbody>';

    	$.each(get.respondent_adv_array,function(i,resa){
    		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
    		showdata += '<td>';
    		showdata +='<div class="checkbox-zoom zoom-primary">';
    		showdata +='<label>';
    		showdata +=' <input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+resa+'">';
    		showdata +='<span class="cr">';
    		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
    		showdata +=' </i>';
    		showdata +=' </span>';
    		showdata +=' </label>';
    		showdata +='</div></td>';
    		showdata +='<td data-th="Full Name"><label style="width:100%">Full Name:</label><input type="text" class="rowVal fullname" value="'+resa+'" name="petitionerorRespondentName" id="sel_resadv_name_'+i+'"></td>'+
    		'<td data-th="Email Address"><label>Email Address:</label><input type="email" class="rowVal email" value="" id="rsel_resadv_email_'+i+'" name="petitionerorRespondentNameemail" > </td><td data-th="Phone Number"><label>Phone Number:</label><input type="text" class="rowVal phonenumber" value="" minlength="10" maxlength="10" id="sel_resadv_mobile_'+i+'" name="petitionerorRespondentphone"></td></tr>'      
    	});

    	showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+'<td class="td_dd" style="">'+
    	'<select id="sel_resadv" class="form-control" style="" name="Petitionerrespomra">'+
        // '<option value="">Please select</option>'+
        '<option value="'+party2+'Adv">'+party2+' Advocate</option><option value="'+party1+'Adv">'+party1+' Advocate</option>'+
        '</select></td></tr>';
    }
    showdata +='</tbody></table><div class="submit_data form-group" id="submit_data">'+
    '<input type="button" class="btn newsubmit btn-info btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
    '</div></form>';
    $("#modal-data").html(showdata);
    $("#data").modal('show');
    $(".fetchLoaderDiv").removeClass("fetchdataloader");
    $("#fetchData_outer").show();
}
else{
	$(".fetchLoaderDiv").removeClass("fetchdataloader");
	$("#fetchData_outer").show();
	toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
}
},

error:function(e){
	$(".fetchLoaderDiv").removeClass("fetchdataloader");
	$("#fetchData_outer").show();
	toastr.error("","Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.",{timeout:5000});
}
});
}

function getSCCases(party1,party2){
	$(".fetchLoaderDiv").addClass("fetchdataloader");
	$("#fetchData_outer").hide();
	var supreme_court = $("#supreme_court").val();
	if(supreme_court == 'Case Number'){
		var appeal_no = $("#case_no").val();
		var appeal_type = $("#cases_case_type").val();
		var appeal_year = $("#case_no_year").val();
		var party_name = "";
		var dairyNo = "";
		var diaryYear = "";
		var searchType = "PARTY_SEARCH";
	}
	else{
		var appeal_no = "";
		var appeal_type ="";
		var appeal_year ="";
		var party_name = "";
		var dairyNo = $("#diary_number").val().trim();
		var diaryYear = $("#diary_year").val().trim();
		var searchType = "DAIRY";
	}
	$.ajax({
		type:'GET',
		url:legalResearch+'/search_by_sc',
		data:{
			caseNo : appeal_no,
			caseType : appeal_type,
			caseYear : appeal_year,
			party : party_name,
			dairyNo : dairyNo,
			dairyYear : diaryYear,
			searchType : searchType
		},
		success:function(response){
			var showdata ='';
			getcase = response;
			$.each(response,function(i,obj){    
				if((!isEmpty(obj.diaryNo) && !isEmpty(obj.diaryYear)) || !isEmpty(obj.caseNo)){
					const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
					"Jul", "Aug", "Sept", "Oct", "Nov", "Dec"
					];
					if(!isEmpty(obj.filedDate)){
						$("#dateoffilling").val(obj.filedDate);
					}
					$("#classification").val(obj.category);  
					showdata += '<form id="reg-form" name="reg-form"><table class="table table-bordered pull-data-popup" style=""><tbody style="">';
					if(obj.petitioner.length > 0 || !isEmpty(obj.petitioner)){
						showdata += '<tr id="row1" style=""><td class="no-padding">'+'<table class="no-bordered"><tbody>';

						$.each(obj.petitioner,function(index,object){
							showdata += '<tr class="row-inner">';
							showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
							showdata += '<td>';
							showdata +='<div class="checkbox-zoom zoom-primary">';
							showdata +='<label>';
							showdata +=' <input type="checkbox" name="selectUser" checked="checked" class="petcheck" value="this.id" id="'+object+'">';
							showdata +='<span class="cr">';
							showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
							showdata +=' </i>';
							showdata +=' </span>';
							showdata +=' </label>';
							showdata +='</div></td>';
							showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
							showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_pet_name_'+index+'"></td>';
							showdata += '<td data-th="Email Address"><label>Email Address:</label>';
							showdata += '<input type="email" class="rowVal email" value="" id="sel_pet_email_'+index+'" name="petitionerorRespondentNameemail" ></td>';
							showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
							showdata += '<input type="text" class="rowVal phonenumber" id="sel_pet_mobile_'+index+'" minlength="10" maxlength="10" value="" name="petitionerorRespondentphone"></td></tr>';
						});
						showdata +='</tbody></table></td>'+
						'<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">'+
						'<select id="sel_pet" class="form-control" style="" name="Petitionerrespom">'+
              // '<option value="">Please select</option>'+
              '<option value="'+party1+'">'+party1+' </option>'+
              '<option value="'+party2+'">'+party2+'  </option>'+
              '</select></td></tr>';
          }
          if(obj.respondent.length > 0 || !isEmpty(obj.respondent)){
          	showdata +='<tr id="row1" style=""><td class="no-padding">'+
          	'<table class="no-bordered"><tbody>';
          	$.each(obj.respondent,function(index,object){
          		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
          		showdata += '<input type="hidden" value="" name="cnr_no">';
          		showdata += '<td>';
          		showdata +='<div class="checkbox-zoom zoom-primary">';
          		showdata +='<label>';
          		showdata +=' <input type="checkbox" name="selectUser[]" checked="checked" class="rescheck" value="this.id" id="'+object+'">';
          		showdata +='<span class="cr">';
          		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
          		showdata +=' </i>';
          		showdata +=' </span>';
          		showdata +=' </label>';
          		showdata +='</div></td>';
          		showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
          		showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_res_name_'+index+'"></td>';
          		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
          		showdata += '<input type="email" class="rowVal email" value="" id="sel_res_email_'+index+'" name="petitionerorRespondentNameemail" > </td>';
          		showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
          		showdata += '<input type="text" class="rowVal phonenumber" value="" id="sel_res_mobile_'+index+'" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';
          	});
          	showdata += '</tbody></table></td><td data-th="is" class="td_is">  <label>is</label></td>'+
          	'<td class="td_dd" style="">'+
          	'<select id="sel_res" class="form-control" style="" name="Petitionerrespomr">'+
              // '<option value="">Please select</option>'+
              '<option value="'+party2+'">'+party2+'  </option>'+
              '<option value="'+party1+'">'+party1+' </option>'+
              
              '</select></td></tr>';
          }
          if(obj.petAdvocate.length > 0 || !isEmpty(obj.petAdvocate)){
          	showdata += '<tr id="row1" style="">';
          	showdata += '<td style="border-right: 1px solid #ccc;">';
          	showdata += '<table class="no-bordered"><tbody>';
          	$.each(obj.petAdvocate,function(index,object){

          		showdata += '<tr class="row-inner">';
          		showdata += '<input type="hidden" value="" name="srnofe[]"><input type="hidden" value="" name="cnr_no">';
          		
          		showdata += '<td>';
          		showdata +='<div class="checkbox-zoom zoom-primary">';
          		showdata +='<label>';
          		showdata +=' <input type="checkbox" name="selectUser[]" checked="checked" class="petadvcheck" value="this.id" id="'+object+'">';
          		showdata +='<span class="cr">';
          		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
          		showdata +=' </i>';
          		showdata +=' </span>';
          		showdata +=' </label>';
          		showdata +='</div></td>';
          		showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
          		showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_petadv_name_'+index+'"></td>';
          		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
          		showdata +='<input type="email" class="rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_petadv_email_'+index+'" > </td>';
          		showdata +='<td data-th="Phone Number"><label>Phone Number:</label>';
          		showdata +='<input type="text" class="rowVal phonenumber" value="" id="sel_petadv_mobile_'+index+'" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';
          	});
          	showdata +='</tbody></table></td>';
          	showdata +='<td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
          	showdata +='<select id="sel_petadv" class="form-control" style="" name="Petitionerrespompa">';
              // showdata +='<option value="">Please select</option>'+
              showdata +='<option value="'+party1+'Adv">'+party1+' Advocate</option>'+
              '<option value="'+party2+'Adv">'+party2+' Advocate</option>'+
              '</select></td></tr>';
          }
          if(obj.respAdvocate.length > 0 || !isEmpty(obj.respAdvocate)){
          	showdata +='<tr id="row1" style="">'+
          	'<td style="border-right: 1px solid #ccc;">'+
          	'<table class="no-bordered"><tbody>';
          	$.each(obj.respAdvocate,function(index,object){
          		showdata += '<tr class="row-inner"><input type="hidden" value="" name="srnofe[]">';
          		showdata += '<input type="hidden" value="" name="cnr_no">';

          		showdata += '<td>';
          		showdata +='<div class="checkbox-zoom zoom-primary">';
          		showdata +='<label>';
          		showdata +=' <input type="checkbox" name="selectUser[]" checked="checked" class="resadvcheck" value="this.id" id="'+object+'">';
          		showdata +='<span class="cr">';
          		showdata +='<i class="cr-icon icofont icofont-ui-check txt-primary">';
          		showdata +=' </i>';
          		showdata +=' </span>';
          		showdata +=' </label>';
          		showdata +='</div></td>';

          		showdata += '<td data-th="Full Name"><label style="width:100%">Full Name:</label>';
          		showdata += '<input type="text" class="rowVal fullname" value="'+object+'" name="petitionerorRespondentName" id="sel_resadv_name_'+index+'"></td>';
          		showdata += '<td data-th="Email Address"><label>Email Address:</label>';
          		showdata += '<input type="email" class="rowVal email" value="" name="petitionerorRespondentNameemail" id="sel_resadv_email_'+index+'" > </td>';
          		showdata += '<td data-th="Phone Number"><label>Phone Number:</label>';
          		showdata += '<input type="text" class="rowVal phonenumber" value="" id="sel_resadv_mobile_'+index+'" minlength="10" maxlength="10" name="petitionerorRespondentphone"></td></tr>';        
          	});
          	showdata += '</tbody></table></td><td data-th="is" class="td_is"><label>is</label></td><td class="td_dd" style="">';
          	showdata += '<select id="sel_resadv" class="form-control" style="" name="Petitionerrespompa">';

              // showdata += '<option value="">Please select</option>'+
              showdata +='<option value="'+party2+'Adv">'+party2+' Advocate</option><option value="'+party1+'Adv">'+party1+' Advocate</option>'+
              '</select></td></tr>';
          }
          showdata +='</tbody></table><div class="submit_data form-group" id="submit_data">'+
          '<input type="button" class="btn newsubmit btn-info btn-sm" value="Insert"  name="submit" onclick=submit_caseapi()> <button type="button" class="btn btn-default btn-sm" id="close_btn" data-dismiss="modal">Close</button>'+
          '</div></form>';
          $("#modal-data").html(showdata);
          $("#data").modal('show');
          $(".fetchLoaderDiv").removeClass("fetchdataloader");
          $("#fetchData_outer").show();
      }
      else{
      	$(".fetchLoaderDiv").removeClass("fetchdataloader");
      	$("#fetchData_outer").show();
      	toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});      
      }
  });
},
error:function(){
	$(".fetchLoaderDiv").removeClass("fetchdataloader");
	$("#fetchData_outer").show();
	toastr.error("",'Fetching data faced a problem, please try later. Alternatively you can manually fill up the data.',{timeout:5000});
}
});
}

function encryptData(data){
	       var key = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef');
	        var iv  = CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210');
//        var key = CryptoJS.enc.Hex.parse('5678943210fdecba5678943210fdecba');
//        var iv  = CryptoJS.enc.Hex.parse('101112131415161718191a1b1c1d1e1f');
        var encrypted = CryptoJS.AES.encrypt((data), key, { iv: iv });
        var encrypted_data = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
            return encrypted_data;
}

function submit_caseapi(){
	var data=getcase;
	var petnameArr=[]; 
	var resnameArr=[]; 
	var petadvnameArr=[]; 
	var resadvnameArr=[];
	var party1 = appearing_top_array[0];
	var party2 = appearing_top_array[1]; 

	var select_pet = $("#sel_pet").val();
	$("input:checkbox[class='petcheck']:checked").each(function(i,obj){
		var petemail = isEmpty($("#sel_pet_email_"+i).val()) ? '' : $("#sel_pet_email_"+i).val();
		var petmobile = isEmpty($("#sel_pet_mobile_"+i).val()) ? '' : $("#sel_pet_mobile_"+i).val();
		var petname = isEmpty($("#sel_pet").val()) ? '' : $("#sel_pet").val();
		var full_pet = isEmpty($("#sel_pet_name_"+i).val()) ? '' : $("#sel_pet_name_"+i).val();
		if(select_pet == party1){

			var petVo = {
				petemail : petemail,
				petmobile : petmobile,
				petname : petname,
				full_pet : full_pet
			}
			petnameArr.push(petVo);
			petitioner_arr = petnameArr;
		}
		else{
			var petVo = {
				resemail : petemail,
				resmobile : petmobile,
				resname : petname,
				full_res : full_pet
			}
			resnameArr.push(petVo);
		}
	});
	var select_res = $("#sel_res").val();
	$("input:checkbox[class='rescheck']:checked").each(function(i,obj){
		var resemail = isEmpty($("#sel_res_email_"+i).val()) ? '' : $("#sel_res_email_"+i).val();
		var resmobile = isEmpty($("#sel_res_mobile_"+i).val()) ? '' : $("#sel_res_mobile_"+i).val();
		var resname = isEmpty($("#sel_res").val()) ? '' : $("#sel_res").val();
		var full_res = isEmpty($("#sel_res_name_"+i).val()) ? '' : $("#sel_res_name_"+i).val();

		if(select_res == party1){
			var resVo = {
				petemail : resemail,
				petmobile : resmobile,
				petname : resname,
				full_pet : full_res
			}
			petnameArr.push(resVo);
			petitioner_arr = petnameArr;
		}
		else{
			var resVo = {
				resemail : resemail,
				resmobile : resmobile,
				resname : resname,
				full_res : full_res
			}
			resnameArr.push(resVo);
		}
    // resnameArr.push(resVo);
    console.log(resnameArr);
});
	var sel_petadv = $("#sel_petadv").val();
	$("input:checkbox[class='petadvcheck']:checked").each(function(i,obj){
		var petadvemail = isEmpty($("#sel_petadv_email_"+i).val()) ? '' : $("#sel_petadv_email_"+i).val();
		var petadvmobile = isEmpty($("#sel_petadv_mobile_"+i).val()) ? '' : $("#sel_petadv_mobile_"+i).val();
		var petadvname = isEmpty($("#sel_petadv").val()) ? '' : $("#sel_petadv").val();
		var full_petadv = isEmpty($("#sel_petadv_name_"+i).val()) ? '' : $("#sel_petadv_name_"+i).val();

		if(sel_petadv == party1+'Adv'){
			var petadvVo = {
				petadvemail : petadvemail,
				petadvmobile : petadvmobile,
				petadvname : petadvname,
				full_petadv : full_petadv
			}
			petadvnameArr.push(petadvVo);
		}     
		else{
			var petadvVo = {
				resadvemail : petadvemail,
				resadvmobile : petadvmobile,
				resadvname : petadvname,
				full_resadv : full_petadv
			}
			resadvnameArr.push(petadvVo);
		}

    // petadvnameArr.push(petadvVo);
    // console.log(petadvnameArr);

});
	var sel_resadv = $("#sel_resadv").val();
	$("input:checkbox[class='resadvcheck']:checked").each(function(i,obj){
		var resadvemail = isEmpty($("#sel_resadv_email_"+i).val()) ? '' : $("#sel_resadv_email_"+i).val();
		var resadvmobile =isEmpty($("#sel_resadv_mobile_"+i).val()) ? '' :  $("#sel_resadv_mobile_"+i).val();
		var resadvname = isEmpty($("#sel_resadv").val()) ? '' : $("#sel_resadv").val();
		var full_resadv = isEmpty($("#sel_resadv_name_"+i).val()) ? '' : $("#sel_resadv_name_"+i).val();

		if(sel_resadv == party1+'Adv'){
			var resadvVo = {
				petadvemail : resadvemail,
				petadvmobile : resadvmobile,
				petadvname : resadvname,
				full_petadv : full_resadv
			}
			petadvnameArr.push(resadvVo);
		}
		else{
			var resadvVo = {
				resadvemail : resadvemail,
				resadvmobile : resadvmobile,
				resadvname : resadvname,
				full_resadv : full_resadv
			}
			resadvnameArr.push(resadvVo);
		}
    // resadvnameArr.push(resadvVo);
});

	var casedetailVo = new Object();
	casedetailVo.petnameArr = petnameArr;
	casedetailVo.resnameArr = resnameArr;
	casedetailVo.petadvnameArr = petadvnameArr;
	casedetailVo.resadvnameArr = resadvnameArr;

	var val = $("input[name='appearing_radio']:checked").val();
	if(appearing_top_array[0] == val){
		var getdata = '<table id="repondant_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+
		'<td>Action</td>'+'</tr></thead><tbody>';
		if(resnameArr.length > 0){
			$.each(resnameArr,function(i,obj){
				if(i==0){
					getdata+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
					'<td><input type="text" name="mail" class="form-control" id="opponent_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
					'<td></td></tr>';
				}
				else{
					getdata+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
					'<td><input type="text" name="mail" class="form-control" id="opponent_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+res_count+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				res_count++;
			}) ;
		}
		else{
			getdata+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value=""/></td>'+

			'<td><input type="text" name="mail" class="form-control" id="opponent_email'+res_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value=""/></td>'+
			'<td></td></tr>';
			res_count++;
		}
		getdata+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=addRespondant("opponent_name","opponent_email","opponent_mobile")> Add More </button></td></tr></tfoot></table>';
		$("#collapse-11").html(getdata);



		var getpetadv = '<table id="petitioneradv_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+

		'<td>Action</td>'+'</tr></thead><tbody>';

		if(petadvnameArr.length > 0){
			$.each(petadvnameArr,function(i,obj){
				if(i==0){
					getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="your_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
					'<td></td></tr>';
				}

				else{
					getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="your_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+petadv_count+',"p-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				petadv_count++;
			});
		}
		else {
			getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+petadv_count+'" value=""/></td>'+

			'<td><input type="text" name="mail" class="form-control" id="your_adv_email'+petadv_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+petadv_count+'" value=""/></td>'+
			'<td></td></tr>';
			petadv_count++;
		}

		getpetadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=addPetitionerAdv("your_adv_name","your_adv_email","your_adv_mobile")> Add More </button></td></tr></tfoot></table>';
		$("#collapse-10").html(getpetadv);



		var getresadv = '<table id="respondant_adv_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+
		'<td>Action</td>'+'</tr></thead><tbody>';
		if(resadvnameArr.length > 0){
			$.each(resadvnameArr,function(i,obj){
				if(i==0){
					getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+          
					'<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
					'<td></td></tr>';
				}else{
					getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+      
					'<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+resadv_count+',"res-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				resadv_count++;
			});
		}
		else{
			getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+resadv_count+'" value=""/></td>'+       
			'<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+resadv_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+resadv_count+'" value=""/></td>'+
			'<td></td></tr>';
			resadv_count++;
		}
		getresadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick="respondantAdvAdd()"  id="respondant_advocate_add"> Add More </button></td></tr></tfoot></table>';
		$("#collapse-12").html(getresadv);

		var getpet = '<table id="petitioner_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+

		'<td>Action</td>'+'</tr></thead><tbody>';
		if(petnameArr.length > 0){
			$.each(petnameArr,function(i,obj){
				debugger;
				if(i==0){
					getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="your_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="your_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
					'<td></td></tr>';
				}

				else{
					getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="your_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="your_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+pet_count+',"p-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				pet_count++;
			});
		}
		else {
			getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+pet_count+'" value=""/></td>'+

			'<td><input type="text" name="mail" class="form-control" id="your_email'+pet_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="your_mobile'+pet_count+'" value=""/></td>'+
			'<td></td></tr>';
			pet_count++;
		}
		getpet+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick="addPetitioner()"> Add More </button></td></tr></tfoot></table>';
		$("#collapse-13").html(getpet);

	}
	else{
		var getpet = '';
		getpet ='<table id="petitioner_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+
		'<td>Action</td>'+'</tr></thead><tbody>';
		if(!isEmpty(petnameArr.length)){
			$.each(petnameArr,function(i,obj){
				if(i==0){
					getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="opponent_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
					'<td></td></tr>';
				}

				else{
					getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+pet_count+'" value="'+obj.full_pet+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="opponent_email'+pet_count+'" value="'+obj.petemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+pet_count+'" value="'+obj.petmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+pet_count+',"p-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				pet_count++;
			});
		}
		else {
			getpet+='<tr id="p-row-'+pet_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+pet_count+'" value=""/></td>'+

			'<td><input type="text" name="mail" class="form-control" id="opponent_email'+pet_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+pet_count+'" value=""/></td>'+
			'<td></td></tr>';
			pet_count++;
		}

		getpet+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=addPetitioner("opponent_name","opponent_email","opponent_mobile")> Add More </button></td></tr></tfoot></table>';
		$("#collapse-11").html(getpet);

		var getresadv = '<table id="respondant_adv_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+
		'<td>Action</td>'+'</tr></thead><tbody>';
		if(!isEmpty(resadvnameArr.length)){
			$.each(resadvnameArr,function(i,obj){
				if(i==0){
					getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+          
					'<td><input type="text" name="mail" class="form-control" id="your_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
					'<td></td></tr>';
				}else{
					getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+resadv_count+'" value="'+obj.full_resadv+'"/></td>'+      
					'<td><input type="text" name="mail" class="form-control" id="your_adv_email'+resadv_count+'" value="'+obj.resadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+resadv_count+'" value="'+obj.resadvmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+resadv_count+',"res-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				resadv_count++;
			});
		}
		else{
			getresadv+='<tr id="res-adv-row-'+resadv_count+'"><td><input type="text" name="name" class="form-control" id="your_adv_name'+resadv_count+'" value=""/></td>'+       
			'<td><input type="text" name="mail" class="form-control" id="your_adv_email'+resadv_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="your_adv_mobile'+resadv_count+'" value=""/></td>'+
			'<td></td></tr>';
			resadv_count++;
		}
		getresadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=respondantAdvAdd("your_adv_name","your_adv_email","your_adv_mobile")  id="respondant_advocate_add"> Add More </button></td></tr></tfoot></table>';
		$("#collapse-10").html(getresadv);



		var getpetadv = '<table id="petitioneradv_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+

		'<td>Action</td>'+'</tr></thead><tbody>';

		if(!isEmpty(petadvnameArr.length)){
			$.each(petadvnameArr,function(i,obj){
				if(i==0){
					getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
					'<td></td></tr>';
				}

				else{
					getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+petadv_count+'" value="'+obj.full_petadv+'"/></td>'+

					'<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+petadv_count+'" value="'+obj.petadvemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+petadv_count+'" value="'+obj.petadvmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+petadv_count+',"p-adv-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				petadv_count++;
			});
		}
		else {
			getpetadv+='<tr id="p-adv-row-'+petadv_count+'"><td><input type="text" name="name" class="form-control" id="opponent_adv_name'+petadv_count+'" value=""/></td>'+

			'<td><input type="text" name="mail" class="form-control" id="opponent_adv_email'+petadv_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="opponent_adv_mobile'+petadv_count+'" value=""/></td>'+
			'<td></td></tr>';
			petadv_count++;
		}

		getpetadv+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=addPetitionerAdv("opponent_adv_name","opponent_adv_email","opponent_adv_mobile")> Add More </button></td></tr></tfoot></table>';
		$("#collapse-12").html(getpetadv);



		var getresp = '<table id="repondant_table" class=" table table-bordered order-list" width="100"><thead><tr><td class="full-name">Full Name</td>'+
		'<td>Email</td>'+
		'<td>Contact</td>'+

		'<td>Action</td>'+'</tr></thead><tbody>';

		if(!isEmpty(resnameArr.length)){
			$.each(resnameArr,function(i,obj){
				if(i==0){
					getresp+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
					'<td><input type="text" name="mail" class="form-control" id="your_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="opponent_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
					'<td></td></tr>';
				}
				else{
					getresp+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="opponent_name'+res_count+'" value="'+obj.full_res+'"/></td>'+
					'<td><input type="text" name="mail" class="form-control" id="your_email'+res_count+'" value="'+obj.resemail+'"/></td>'+
					'<td><input type="text" name="phone" class="form-control" id="your_mobile'+res_count+'" value="'+obj.resmobile+'"/></td>'+
					'<td><button type="button" class="ibtnDel btn btn-danger btn-sm" onclick=deleteRow('+res_count+',"res-row")><i class="feather icon-trash minus_button" aria-hidden="true"></i></button></td></tr>';
				}
				res_count++;
			});
		}
		else {
			getresp+='<tr id="res-row-'+res_count+'"><td><input type="text" name="name" class="form-control" id="your_name'+res_count+'" value=""/></td>'+

			'<td><input type="text" name="mail" class="form-control" id="your_email'+res_count+'" value=""/></td>'+
			'<td><input type="text" name="phone" class="form-control" id="your_mobile'+res_count+'" value=""/></td>'+
			'<td></td></tr>';
			petadv_count++;
		}

		getresp+='</tbody><tfoot><tr><td colspan="4" style="text-align: right;"><button type="button" class="add-opponent btn btn-info btn-sm add-more-button" onclick=addRespondant("your_name","your_email","your_mobile")> Add More </button></td></tr></tfoot></table>';
		$("#collapse-13").html(getresp);

	}

	$(".collapse").addClass("show");
	$("#data").modal("hide");

}

function submitCase(){
	var petArr= [];
	var resArr=[];
	var resadvArr =[];
	var petAdvArr = [];
	var caseid = $("#caseid").val();
	var court_name = $("#merc_court").val(); 
	 
	var case_no = $("#case_no").val(); 
	var case_no_year = $("#case_no_year").val(); 
	 
    var type_of_litigation = $("input[name='appearing_radio']:checked").val(); //----//are you appearing as
   
      var dateoffilling = $("#dateoffilling").val();
     
      var title = $("#title").val();
       var risk_category = $("#risk_category").val();
       
      var query = tinymce.get('query').getContent();
      
      
  	 
  		for(var i = 0;i <= pet_count;i++){
  			var pet_name = $("#opponent_name"+i).val();
  			var pet_email  = $("#opponent_email"+i).val();
  			var pet_mobile = $("#opponent_mobile"+i).val();
  			var pet_id = $("#opponent_id"+i).val();
  			var pet_flag = $("#opponent_name_addmore_"+i).val();
  			// if(!isEmpty(pet_email)){
  			// 	if(validateEmail(pet_email,pet_name)){
  			// 		var petVo = {
  			// 			pet_name : pet_name,
  			// 			pet_email : pet_email,
  			// 			pet_mobile : pet_mobile,
  			// 			pet_id : pet_id,
  			// 			pet_flag : pet_flag
  			// 		}
  			// 		// petArr.push(petVo);
  			// 	}
  			// }
  			// else
  				var petVo = {
  					pet_name : pet_name,
  					pet_email : pet_email,
  					pet_mobile : pet_mobile,
  					pet_id : pet_id,
  					pet_flag : pet_flag
  				}
  				petArr.push(petVo);
  			}
  // var res_count = $("#res_count").val();
  for(var i = 0;i<=res_count;i++){
  	var res_name = $("#your_name"+i).val();
  	var res_email  = $("#your_email"+i).val();
  	var res_mobile = $("#your_mobile"+i).val();
  	var res_id = $("#your_id"+i).val();
  	var res_flag = $("#your_name_addmore_"+i).val();
  	// if(!isEmpty(res_email)){
  	// 	if(validateEmail(res_email,res_name)){
  	// 		var resVo = {
  	// 			res_name : res_name,
  	// 			res_email : res_email,
  	// 			res_mobile : res_mobile,
  	// 			res_id : res_id,
  	// 			res_flag : res_flag
  	// 		}
  	// 		// resArr.push(resVo);
  	// 	}
  	// }
  	// else
  		var resVo = {
  			res_name : res_name,
  			res_email : res_email,
  			res_mobile : res_mobile,
  			res_id : res_id,
  			res_flag : res_flag
  		}
  		resArr.push(resVo);
  	}

 

      // var respAdv = resadvArr;
      // var petAdv = petAdvArr;
      /*$.each(petAdvArr,function(i,obj){
        var resObj = {
          res_adv_name : obj.pet_adv_name,
          res_adv_email : obj.pet_adv_email,
          res_adv_mobile : obj.pet_adv_mobile
        }
        respondentArr.push(resObj);
      });
      $.each(resadvArr,function(i,obj){
        var petObj = {
          pet_adv_name : obj.res_adv_name,
          pet_adv_email : obj.res_adv_email,
          pet_adv_mobile : obj.res_adv_mobile
        }
        petetitionerArr.push(petObj);
      });
      resadvArr = respondentArr;
      petAdvArr = petetitionerArr
    }
    else{
     resadvArr = resadvArr;
     petAdvArr  = petAdvArr;
   }
}*/
  /*else{
    var petetitionerArr=[],respondentArr=[];
    if(are_you_appearing_as == 'Respondent'){
      $.each(petAdvArr,function(i,obj){
        var resObj = {
          res_adv_name : obj.pet_adv_name,
          res_adv_email : obj.pet_adv_email,
          res_adv_mobile : obj.pet_adv_mobile
        }
        respondentArr.push(resObj);
      });
      $.each(resadvArr,function(i,obj){
        var petObj = {
          pet_adv_name : obj.res_adv_name,
          pet_adv_email : obj.res_adv_email,
          pet_adv_mobile : obj.res_adv_mobile
        }
        petetitionerArr.push(petObj);
      });
      resadvArr = respondentArr;
      petAdvArr = petetitionerArr;
    }   
    else{
      resadvArr = resadvArr;
      petAdvArr = petAdvArr;
    }
}*/



var caseVo = new Object();
caseVo.court_name = court_name;
caseVo.case_id = caseid;
caseVo.type_of_litigation = type_of_litigation;

caseVo.case_no = case_no;
caseVo.case_no_year = case_no_year;
 
caseVo.dateoffilling = dateoffilling;
 
caseVo.title = title;
 caseVo.risk_category= risk_category;
caseVo.query = query;
 
  

caseVo.petArr = petArr;
caseVo.resArr = resArr;
  
$.ajax({
	url: host+'/update_editcase.php',
	type:'POST',
	data : caseVo,
    // contentType : 'application/json;utf-8',
    dataType: 'json',
    async:false,
    cache : false,
    success:function(data){
    	// var editdata = JSON.parse(data);
    	if(data.status == 'success'){
    		// toastr.success("","Data Submitted Successfully",{timeout:5000});
    		window.location.href='cases-report.php';
    		// getFiles(data.case_id,data.caseid);
    	}
    	else
    		toastr.error("",data.status,{timeout:5000});
    },
    error:function(e){
    	toastr.error("","Error in updating data",{timeout:5000});
    }
});
}

