 function getclr(){
  var user= $("#email").val();
  var firstname= $("#fname").val();
  var lastname= $("#lname").val();
  var contact= $("#mobile").val();
  var state= $("#state").val();subject
  var fileno = $("#fileno").val();
  var subjects = $("#subject").val();

  // var query = CKEDITOR.instances.query.getData();
  var query = tinymce.get("query").getContent({ format: 'text' });
  if(isEmpty(query)){
    toastr.error("Please Enter The Question","Required",{timeout:5000});
  }

  /*var documents= $("#docfiles").val();*/
  var actArray= [];
  var QueryVo=new Object();
  QueryVo.user=user;
  QueryVo.firstname=firstname;
  QueryVo.lastname=lastname;
  QueryVo.contact=contact;
  QueryVo.state=state;
  QueryVo.query=query;
  // QueryVo.subject=subject;

  // ajaxData.append( 'action','uploadImages');
  var subjectArray = [];
  var fileTypeArray= [];  
  // $.each(subject, function(j,sobj){
  //     var subVO=new Object();
  //     subVO.subject = sobj;
  //   subjectArray.push(subVO);
  // });
  // $.each(subjects,function(a,objs){
   subjectArray.push(subjects);
 // });
  var ajaxData = new FormData();
  $.each($("input[type=file]"), function(i, obj) {
    $.each(obj.files,function(j, file){

      var filebj = new Object();
      filebj.fileUrl = '/upload_document/'+file['name'];
      filebj.fileNo = fileno;
      fileTypeArray.push(filebj);
      ajaxData.append("nonlit_file",file);
      var ajaxResult = $.ajax({
        url : host+"/ajaxdata/submit_nonlitfiles.php",
        type : 'POST',
        data : ajaxData,
        cache : false,
        contentType : false,
        processData : false,
        success:function(response){
           
        },
        error:function(){
          toastr.error("","Error in adding case",{timeout:5000});
        }
      });
      // ajaxData.append('fileNo', fileno);
      // ajaxData.append('fileUrl', filebj);
      
    });

  });
  QueryVo.subject=subjectArray;
  QueryVo.nonLitigationQueryVOs=fileTypeArray;
    /*var ActVO=new Object();
    ActVO.act= act;
    ActVO.section=section;
    actArray.push(ActVO);*/
    // var file = document.getElementById("docfiles").files[0];     
    // var formdata=new FormData();
    // formdata.append("documents",file);
    // var fileArray=[];
    // fileArray.push(formdata);
    // QueryVo.documents=fileArray;

    //QueryVo.actVos=actArray;
    $.ajax({
      /* url: "http://192.168.0.79:8080/LegalResearch/restquery",*/
      url: legalResearch+"/restquery",
      type:'POST',
      data: JSON.stringify(QueryVo),
      dataType: 'json',
      async:false,
      headers: {
        "Content-Type": 'application/json'
      },
      beforeSend : function(){
        $(".flip-square-loader").show();
      },
      success:function(data){            
              //console.log(data);
              if(data.error=='SUCCESS'){
                toastr.success("Query Submitted Successfully","Success",{timeout:5000});
                // alert("Query Submitted Successfully");
                // window.location.href = "non-litigation";
                $(".flip-square-loader").hide();
              }

            },
            complete:function(){
             $(".flip-square-loader").hide();
           },
           error:function(e){
             $(".flip-square-loader").hide();
             toastr.error("Sorry for inconvinence"," ","Sorry for inconvinence",{timeout:5000});
           }
           
         });
  }

  $(document).ready(function(){
    getallquery(1);
    changeLanguage('English');
$("#subject").select2();

  });



  function getallquery(pageno){
    var user = $("#email").val();
    // var user= 'rameshwar.air@gmail.com';
    /*var pageno=1;*/
    var QueVo=new Object();
    QueVo.user=user;
    QueVo.pageNo=pageno;
    $.ajax({ 
      url: legalResearch+"/alluserquery",
      type:'POST',
      data: JSON.stringify(QueVo),
      dataType: 'json',
      async:false,
      headers: {
        "Content-Type": 'application/json'
      },
      success:function(data){            
        // console.log(data);

              // if(data){
              //  $("#queryshow").val(data.caseTitleid);
              // }
              
             
              var queyd = '';
              if(data.userOutBoxVos.length>0){
              $.each(data.userOutBoxVos, function(key,val) {
                queyd += '<div class="chat_div"><div class="col-sm-12 row"><label class="col-sm-1 query">Que '+val.qid+
                '. </label><span class="col-sm-11">'+val.query+'</span></div><div class="reply_show col-sm-12" id="reply'+val.qid+
                '"></div><div class="col-sm-12 row" style="width: 86%;margin: 0 auto;"><div class=" chat_img">'+
                '<img src="images/explanation.png"></div><div class="col-sm-4 chat_reply">'+
                '<a href="javascript:void(0)" id="showreply'+val.qid+'" class="view_reply view" style="float: left;" onclick="getans('+val.qid+')">'+
                '</a></div></div></div>';

              }); 
              var pagination = '<div class="panel-footer "><ul class="pagination pagination-sm pull-right pagination-separate pagination-round pagination-flat pageNo"><li id="page_link_prev" class="page-item"><a class="page-link" href="#">Prev</a></li>';
              for(var i=1; i<=data.numPages;i++){
                pagination += '<li id="page_link_'+i+'" class="page-item activepageno"><a class="page-link" onclick="getallquery('+i+')">'+i+'</a></li>';
              }

              pagination += '<li id="page_link_next" class="page-item"><a class="page-link" href="#">Next</a></li></ul></div>';
              $("#pagen").html(pagination);
              $("#queryshow").html(queyd);
              $(".body-section").show();
            }

              assign_active_pagination(pageno);
            },
            error:function(e){
        //console.log(e);
      }
    });
  }

  function assign_active_pagination(pageNo){
    $("#page_link_"+pageNo).addClass('active');
  }

  $(window).ready(function(){
    $("#page_link_prev").on("click",function(){
      var pageNo = $(".activepageno.active a").text();
      var prev_page = parseInt(pageNo)-1;
      getallquery(prev_page);
    });

    $("#page_link_next").on("click",function(){
      var pageNo = $(".activepageno.active a").text();
      var next_page = parseInt(pageNo)+1;
      getallquery(next_page);
    });
  });


  var oldqid='';
  function getans(qid){
 
  var AnsVo=new Object();
  AnsVo.qid=qid;
  var htmlData="";

  $.ajax({ 
    url: legalResearch+"/answers",
    type:'POST',
    data: JSON.stringify(AnsVo),
    dataType: 'json',
    async:false,
    headers: {
      "Content-Type": 'application/json'
    },
    success:function(data){ 
      if(qid == oldqid){
        if ($("#showreply"+qid).hasClass("view")) 
          $("#showreply"+qid).removeClass('view').addClass('hide');
        else
          $("#showreply"+qid).removeClass('hide').addClass('view');
        $("#reply"+qid).toggle();
      }
      else{
        if(data.questionAnswer!=null){
          htmlData='<div class="col-sm-12 row"><label class="col-sm-1 query">Ans: </label>';
          var researcherDocVos =data.researcherDocVos;
        //$.each(researcherDocVos, function(i,v){
          htmlData+='<span style="font-weight: bold;padding-left: 15px;" class="col-sm-11">'+data.questionAnswer+'</span>';//+"<ul><li><a href="+v.path+">"+v.docs+"</a></li></ul>
          htmlData+='<label class="col-sm-1 query"></label>';
          htmlData+='<div class="col-sm-11 row"><table class="col-sm-11"><thead>'+
          '<tr class="nonlit_class"><td >File No</td><td>Documents Sent </td><td>Documents Received</td></tr></thead>';
          htmlData+='<tbody><tr class="non_littd">';
           $.each(data.nonLitigationQueryVOs,function(i,obj){
            var url = obj.fileUrl;
            var filename = url.substring(url.lastIndexOf('/')+1);
          htmlData+='<td>'+obj.fileNo+'</td>'+
           '<td><a href='+obj.fileUrl+'>'+filename+'</a></td>';
        });
           $.each(data.researcherDocVos,function(i,res){
            htmlData+= '<td><a href='+res.path+'>'+res.docs+'</a></td>';
           });
          '</tr></tbody></table>';
          '</div>';
        //   $.each(data.nonLitigationQueryVOs,function(i,obj){
        //     var url = obj.fileUrl;
        //     var filename = url.substring(url.lastIndexOf('/')+1);
        //   htmlData+='<div class="col-sm-5"><p style="text-align:left;"><b>Documents Sent </b></p><hr><span class="col-sm-2">File No:'+obj.fileNo+'</span>'+
        //   '<span class="col-sm-3">Documents: '+filename+'</span></div>';
          
        // });
        //     $.each(data.researcherDocVos,function(i,res){
        //   htmlData+='<div class="col-sm-6" style="text-align:right;"><p style="text-align:right;"><b>Documents Received </b></p><hr><span>Documents: <a href='+res.path+'>'+res.docs+'</a></span></div>';
        // });
            htmlData+='</div>';
        //});
        $("#reply"+qid).html(htmlData);

        $("#reply"+qid).show();
        // $("#showreply"+qid).text('Hide Answer');
        $("#showreply"+qid).removeClass('view').addClass('hide');
        oldqid = qid;

      }else{
        if(qid == oldqid){
          if ($("#showreply"+qid).hasClass("view")) 
            $("#showreply"+qid).removeClass('view').addClass('hide');
          else
            $("#showreply"+qid).removeClass('hide').addClass('view');
          $("#reply"+qid).toggle();
        }else{
          var ans = '<div class="col-sm-12"><label class="col-sm-2 query">Ans: </label><span style="font-weight: bold;padding-left: 15px;">Waiting For Response</span></span>';
          $("#reply"+qid).html(ans);
          $("#reply"+qid).show();
          $("#showreply"+qid).removeClass('view').addClass('hide');
          oldqid = qid;
        }
      }
    }
  },
  error:function(e){
        //console.log(e);
      }
    });
}

function changeLanguage(language){
  tinymce.init({
    selector : "#query",
    plugins : [
    "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });

  if(language == 'Marathi'){
    google.setOnLoadCallback(OnLoad);
    OnLoad();
  }
}
google.load("elements", "1", {packages: "transliteration"});

function OnLoad() { 
  var options = {
    sourceLanguage:
    google.elements.transliteration.LanguageCode.ENGLISH,
    destinationLanguage:
    [google.elements.transliteration.LanguageCode.MARATHI],
    shortcutKey: 'ctrl+g',
    transliterationEnabled: true
  };  
  var control = new google.elements.transliteration.TransliterationControl(options);
  control.makeTransliteratable(["query_ifr"]);
  
} //end onLoad function
var con = 0;
function nonLit(){
  con++;
  var e = '<div id="nonlitdiv'+con+'">'+
    '<input type="file" id="non_doc_'+con+'" class="same" aria-label="File browser example" style="margin-top: 4px;">'+
    '<button class="btn btn-sm btn-danger nonremove" type="button" onclick="removeDoc('+con+')">'+
    '<i class="feather icon-minus" aria-hidden="true"></i></button></div>';
  $('#nonlitShow').append(e);      
}
function removeDoc(con){
  document.getElementById("nonlitdiv"+con).outerHTML="";
}