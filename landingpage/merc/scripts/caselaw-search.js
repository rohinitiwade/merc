 
function getquery(){
  debugger;

     
      caseid = $("#case_id").val();
       var search = $("#search").val();
      $.ajax({ 
      // url: "http://192.168.0.121:8080/qtool/api/qtool/judgement",
      type:'POST',
       url: 'submit_case_lawsearch.php',
     data:'caseid='+caseid+'&search='+search+'&flag=search',
     dataType: 'json',
     async:false,
    success:function(response){  
      console.log(response);
       // toastr.success('Added Successfully', 'Added Successfully', {timeOut: 5000})
    }
  });

  var search = $("#search").val();
  var limit = '10';
  var offsets = '0';
  var searchfield = 'BOTH';
  var akajrequest = 'true';
  var totalPages;var count;
  $.ajax({
    url: qtoolurl+'api/qtool/webservices/all',
        // url: "http://192.168.0.121:8080/qtool/api/qtool/webservices/all",
        type:'GET',
        data:"q="+search+"&searchfield="+searchfield+"&limit="+limit+"&offset="+offsets+"&sortby="+"&sortorder="+"&journals="+"&courts="+"&acts="+"&benches="+"&topics="+"&judges="+"&ci_benchplace="+"&rmkdisplay="+"&akajrequest="+akajrequest,
        headers:{
          "Content-Type": 'application/json',
          "Authorization":"Bearer "+localStorage.getItem('auth_token')
        },
        beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(data){
     var getdata = '';
     $.each(data,function(key,val){
      if(key == 'AIR Online')
       online = key;
     onlineinfo = 'AIR Infotech';
     getdata += '<span class="totalcount">Total Result Found: '+val.count+'</span><input type="hidden" id="countn" value='+val.count+'><hr class="main_hr">';
     $.each(val.cases,function(i,get){
       getdata += '<div class="col-sm-12 main_div form-group" >';
       getdata += '<div class="checkbox-zoom zoom-primary">';
       getdata += '<label>';
       getdata += ' <input type="checkbox" class="checkbox_referred"  id="'+online+'_'+get.citation_orig+'" onclick=checkcaselaw(this.id)>';
       getdata += '<span class="cr">';
       getdata += '<i class="cr-icon icofont icofont-ui-check txt-primary">';
       getdata += '</i>';
       getdata += '</span><span class="srno">Relevant Case Law</span>';
       getdata += '</label>';
       getdata += '</div>';       
       getdata += '<hr class="main_hr">';
       getdata += '<div onclick=getcitation(this.id) id="'+online+'_'+get.citation_orig+'"><div class="col-sm-4" style="float: right;text-align: right;"><span class="badge badge-warning labels journal_online">'+onlineinfo+'</span></div><div class="col-sm-4 p-0"><span class="badge badge-info labels">'+get.citation_orig+'<span></span></span></div><div class="col-sm-12 nominals p-0">';
       $.each(get.appnominals,function(c,nom){
        getdata += '<b style="color:green">'+nom+'</b>,';
      });
       getdata +='<b> Vs.</b>';
       $.each(get.resnominals,function(d,res){
        getdata += '<b style="color:green">'+res+'</b>,';
      });
       getdata += '<div class="col-sm-12 appealText p-0"><div class="col-sm-12" style="padding:0px;color:maroon"><label><i>'+get.appealtext+'</i></label></div></div><div class="col-sm-12" style="padding:0px"><label><b>'+get.dod+'</b><span class="badge badge-primary journal_online">'+get.courts+'</span></label></div></div>';
       $.each(get.multiSNote,function(d,mul){
        getdata += '<div class="col-sm-12 shortnote p-0">'+mul+'</div>';
      });
       getdata+= '</div></div>';
     });
   });
     $("#show_data").html(getdata);

   },
   complete: function(){
     var counts = $("#countn").val();
     showPagination(counts,20);
     $('.flip-square-loader').hide();
   },
   error:function(e){
     $('.flip-square-loader').hide();
     toastr.error('Unable to load Cases', 'Sorry For Inconvenience!', {timeOut: 5000})
   }
 });
}

var currentPage;
      function showPagination(counts,limit) {
    // Number of items and limits the number of items per page
    var numberOfItems = counts;
    var limitPerPage =limit;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    var paginationSize = 7; 
    function showPage(whichPage) {
      if (whichPage < 1 || whichPage > totalPages) return false;
      currentPage = whichPage;                   
      $(".pagination li").slice(1, -1).remove();
      getPageList(totalPages, currentPage, paginationSize).forEach( item => {
        $("<li>").addClass("page-item")
        .addClass(item ? "current-page" : "disabled")
        .toggleClass("active", item === currentPage).append(
          $("<a>").addClass("page-link").attr({
            href: "javascript:void(0)"}).text(item || "...")
          ).insertBefore("#next-page");
      });
        // Disable prev/next when at first/last page:
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }
    $(".pagination").append(
      $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Prev")
        ),
      $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Next")
        )
      );
    showPage(1);

    // Use event delegation, as these items are recreated later    
    $(document).on("click", ".pagination li.current-page:not(.active)", function () {
      var s=showPage(+$(this).text());
      getpagination(+$(this).text());
      return s;
    });
    $("#next-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage+1);
        getpagination(currentPage+1);
        return s;
      }
    });

    $("#previous-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage-1);
        getpagination(currentPage-1);
        return s ;
      }
    });

}


function offset(page,limit){
  if(page <= 1)
    return 0;
  else
    return ((page-1)*limit);
}


function getPageList(totalPages, page, maxLength) {
  if (maxLength < 5) throw "maxLength must be at least 5";

  function range(start, end) {
    return Array.from(Array(end - start + 1), (_, i) => i + start); 
  }

  var sideWidth = maxLength < 9 ? 1 : 2;
  var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
  var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
  if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength-sideWidth-1)
        .concat([0])
        .concat(range(totalPages-sideWidth+1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
        .concat([0])
        .concat(range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }
    // Breaks on both sides
    return range(1, sideWidth)
    .concat([0])
    .concat(range(page - leftWidth, page + rightWidth)) 
    .concat([0])
    .concat(range(totalPages-sideWidth+1, totalPages));
}

function checkcaselaw(getid){
  debugger;
 var citvars = getid.split("_");
    var online = citvars[0];
    var citation = citvars[1];
      caseid = $("#case_id").val();
       var search = $("#search").val();
      $.ajax({ 
      // url: "http://192.168.0.121:8080/qtool/api/qtool/judgement",
      type:'POST',
       url: 'submit_case_lawsearch.php',
     data:'caseid='+caseid+'&productType='+online+'&citation='+citation+'&search='+search+'&flag=insert',
     dataType: 'json',
     async:false,
    
          beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(response){  
      console.log(response);
       toastr.success('Added Successfully', 'Added Successfully', {timeOut: 5000})
    },
    complete: function(){
        $('.flip-square-loader').hide();  

    },
    error: function(e){

        $('.flip-square-loader').hide();  
         toastr.error('Unable to load Citation', 'Sorry For Inconvenience!', {timeOut: 5000})
    }
  });
}
  function getcitation(onlineget){
    var citvar = onlineget.split("_");
    var online = citvar[0];
    var citation = citvar[1];
    var search = $("#search").val();
    $.ajax({ 
      // url: "http://192.168.0.121:8080/qtool/api/qtool/judgement",
       url: qtoolurl+"api/qtool/judgement",
      type:'get',
      data: {citation:citation,
       product: online
     },
     dataType: 'json',
     async:false,
     headers: {
      "Content-Type": 'application/json',
      "Authorization":"Bearer "+localStorage.getItem('auth_token')
    },
          beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(response){  
      console.log(response);
      var content = replace(search,response.content);
      var printWin = window.open(''); 
      printWin.document.write('<html><head><title>'+citation+'</title><link rel="stylesheet" type="text/css" href="styles/showjudgment.css"><link rel="stylesheet" href="css/bootstrap.min.css" /></head><body>'); 
      printWin.document.write(content); 
      printWin.document.write("</body></html>");
    },
    complete: function(){
        $('.flip-square-loader').hide();  

    },
    error: function(e){

        $('.flip-square-loader').hide();  
         toastr.error('Unable to load Citation', 'Sorry For Inconvenience!', {timeOut: 5000})
    }
  });
  }

function replace(query,content) { var s = query.split(" "); var keys = keywordarray(); s.forEach(function(i,e){ if(!keys.includes(i)) content= i && content ? content.replace(new RegExp(escapeRegexp(i), 'gi'), "<span style='background:yellow'>$&</span>") : content; }); return content; }

function escapeRegexp(queryToEscape) { return queryToEscape.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1'); } function keywordarray(){ var stopwords = ["a","about","above","after","again","against","all","am","an","and","any","are","aren't","as", "at","be","because","been","before","being","below","between","both","but","by","can't","cannot","could","couldn't", "did","didn't","do","does","doesn't","doing","don't","down","during","each","few","for","from","further","had","hadn't", "has","hasn't","have","haven't","having","he","he'd","he'll","he's","her","here","here's","hers","herself","him","himself", "his","how","how's","i","i'd","i'll","i'm","i've","if","in","into","is","isn't","it","it's","its","itself","let's", "me","more","most","mustn't","my","myself","no","nor","not","of","off","on","once","only","or","other","ought","our", "ours","ourselves","out","over","own","same","shan't","she","she'd","she'll","she's","should","shouldn't","so","some", "such","than","that","that's","the","their","theirs","them","themselves","then","there","there's","these","they", "they'd","they'll","they're","they've","this","those","through","to","too","under","until","up","very","was","wasn't", "we","we'd","we'll","we're","we've","were","weren't","what","what's","when","when's","where","where's","which","while", "who","who's","whom","why","why's","with","won't","would","wouldn't","you","you'd","you'll","you're","you've","your", "yours","yourself","yourselves"]; return stopwords; }

function getpagination(currentpage){
  var search = $("#search").val();
  var limit = '10';
  var offsets = offset(currentpage,limit);
  var searchfield = 'BOTH';
  var akajrequest = 'true';
  var totalPages;var count;
  $.ajax({
    url: qtoolurl+"api/qtool/webservices/all",
        // url: "http://192.168.0.121:8080/qtool/api/qtool/webservices/all",
        type:'GET',
        data:"q="+search+"&searchfield="+searchfield+"&limit="+limit+"&offset="+offsets+"&sortby="+"&sortorder="+"&journals="+"&courts="+"&acts="+"&benches="+"&topics="+"&judges="+"&ci_benchplace="+"&rmkdisplay="+"&akajrequest="+akajrequest,
        headers:{
          "Content-Type": 'application/json',
          "Authorization":"Bearer "+localStorage.getItem('auth_token')
        },
        beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();             
  },
  success:function(data){
          // console.log(data.AIR Online.cases);
          // var getuser = data.AIR Online.cases;
          

          var getdata = '';
          // $.each(data.cases, function(key,val){
          //     getdata += '<div class="col-sm-12 main_div"><div class="col-sm-4" style="float: right;text-align: right;"><span class="badge badge-warning labels journal_online">AIR Online</span></div></div>';

          // });
          $.each(data,function(key,val){
            if(key == 'AIR Online')
              online = key;
            getdata += '<span class="totalcount">Total Result Found:'+val.count+'</span><input type="hidden" id="countn" value='+val.count+'><hr class="main_hr">';




            $.each(val.cases,function(i,get){
              getdata += '<div class="col-sm-12 main_div" id="'+online+'_'+get.citation_orig+'" onclick=getcitation(this.id)>';
              getdata += '<div class="form-check">';
              getdata += '<label class="form-check-label checkboxlabel">';
              getdata += '<input type="checkbox" class="checkbox_referred" value="'+online+'_'+get.citation_orig+'"><i class="input-helper"></i>Relevant Case Law</label><hr></div>';
              getdata +='<div class="col-sm-4" style="float: right;text-align: right;">';       
              getdata +='<span class="badge badge-warning labels journal_online">'+online+'</span></div><div class="col-sm-4"><span class="badge badge-info labels">'+get.citation_orig+'<span></span></span></div><div class="col-sm-12 nominals">';
              $.each(get.appnominals,function(c,nom){
                getdata += '<b style="color:green">'+nom+'</b>,';
              });
              getdata +='<b> Vs.</b>';
              $.each(get.resnominals,function(d,res){
                getdata += '<b style="color:green">'+res+'</b>,';
              });
              getdata += '<div class="col-sm-12 appealText"><div class="col-sm-12" style="padding:0px;color:maroon"><label><i>'+get.appealtext+'</i></label></div></div><div class="col-sm-12" style="padding:0px"><label><b>'+get.dod+'</b><span class="badge badge-primary journal_online">'+get.courts+'</span></label></div></div>';
              $.each(get.multiSNote,function(d,mul){
                getdata += '<div class="col-sm-12 shortnote">'+mul+'</div>';
              });
              getdata+= '</div>';
            });
          });
          $("#show_data").html(getdata);
      },
      complete: function(){
          // var counts = $("#countn").val();
      // showPagination(counts,20);
      $('.flip-square-loader').hide();
  },
  error:function(e){

    $('.flip-square-loader').hide();
  }
});
}

function viewCase(caseid){
  window.location.href='view-case.php?caseid='+caseid;
}