$('#team-member').select2({
  placeholder: 'Select...',
  closeOnSelect: false,
  multiple: true
});
$('#team-member1').select2({
  placeholder: 'Select...',
  closeOnSelect: false,
  multiple: true
});
function reset() {
  var dropDown = document.getElementById("state");
  dropDown.selectedIndex = 0;
}
$(document).ready(function() {
  $('#manage_team_dataTable').DataTable( {
    dom: 'Bfrtip',
    buttons: [

    'excelHtml5',
    'csvHtml5',
    'pdfHtml5'
    ]
  } );
} );
function viewcases (id) {
  var id = +id;
  $.ajax({
    type:'POST',
    url:'ajaxdata/teamcases.php',
    data:'ids='+id,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(html){
      $('#casedetails').html(html);
    },
    complete:function(){
     $(".flip-square-loader").hide();
   },
   error:function(e){
     $(".flip-square-loader").hide();
     toastr.error("","Unable to Load","Sorry for inconvinence",{timeout:5000});
   }
 }); 
}


function frequency (id) {
  var id = + id;
  $.ajax({
    type:'POST',
    url:'ajaxdata/frequency_report.php',
    data:'regid='+id,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(html){
      $('#freq').html(html);
    },
    complete:function(){
     $(".flip-square-loader").hide();
   },
   error:function(e){
     $(".flip-square-loader").hide();
     toastr.error("","Unable to Load","Sorry for inconvinence",{timeout:5000});
   }
 }); 
}

function editteam (id) {
  var id = +id;
  $.ajax({
    type:'POST',
    url:'editteam.php',
    data:'ids='+id,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(html){
      $('#edit_team').html(html);
    },
    complete:function(){
     $(".flip-square-loader").hide();
   },
   error:function(e){
     $(".flip-square-loader").hide();
     toastr.error("","Unable to Load","Sorry for inconvinence",{timeout:5000});
   }
 }); 
}
function teamcomment (id) {
  var id = +id;
  $.ajax({
    type:'POST',
    url:'ajaxdata/team_comment.php',
    data:'ids='+id,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(html){
      $('#teamcomment').html(html);
    },
    complete:function(){
     $(".flip-square-loader").hide();
   },
   error:function(e){
     $(".flip-square-loader").hide();
     toastr.error("","Unable to Load","Sorry for inconvinence",{timeout:5000});
   }
 }); 
}
function loginhistory (id) {
  var id = +id;
  $.ajax({
    type:'POST',
    url:'ajaxdata/loginhistory.php',
    data:'ids='+id,
    beforeSend : function(){
      $(".flip-square-loader").show();
    },
    success:function(html){
      $('#loghisto').html(html);
    },
    complete:function(){
     $(".flip-square-loader").hide();
   },
   error:function(e){
     $(".flip-square-loader").hide();
     toastr.error("","Unable to Load","Sorry for inconvinence",{timeout:5000});
   }
 }); 
}

function download_doc(){
  var downloadtype = $("input[name='downloadas']:checked").val();
  $.ajax({
    type:'GET',
    url:'ajaxdata/export_team.php',
    dataType:"json",
    success:function(response){
      console.log("advocate",response);
      var table = '<table class="table table-bordered main-table" id="main-table"><thead>';
      table +='<tr>';
      table += '<th>Sr No.</th>';
      table += '<th>Full Name</th>';
      table += '<th>Designation</th>';
      table += '<th>Email</th>';
      table += '<th>Mobile</th>';
      table += '<th>GST</th></tr></thead><tbody>';
      $.each(response.team_array,function(i,obj){
        i++;
        table += '<tr>';
        table += '<td>'+i+'</td>';
        table += '<td>'+obj.name+' '+obj.last_name+'</td>';
        table += '<td>'+obj.designation+'</td>';
        table += '<td>'+obj.email+'</td>';
        table += '<td>'+obj.mobile+'</td>';             
        table += '<td>'+obj.gst+'</td></tr>';
      });
      table +='</tbody></table>';
      $("#document-table-div-export").html(table);

// var pdfTitle = $("#case_type_export_report").text();
if(!isEmpty(response.team_array))
  exportPdf(downloadtype,'Team');
else{
  $("#export").modal('hide');
  toastr.warning("","No record found to export",{timeout:5000});
}
// exportPdf(downloadtype,'Team');
},
error:function(e){

}
});
}