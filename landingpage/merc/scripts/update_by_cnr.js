$(document).ready(function() {
	var responseData = Array();
	
	function find_first_letter(str){
		var matches = str.match(/\b(\w)/g);
		var acronym = matches.join('');
		return acronym;
	}
	
	function encryptData(data) {
		var key = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef');
		var iv = CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210');
		var encrypted = CryptoJS.AES.encrypt((data), key, {
			iv : iv
		});
		var encrypted_data = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
		return encrypted_data;
	}	
	
	searchCNR();
	$("#fetch_data_cnr").click(function(){		
		searchCNR();
	});
	
	function searchCNR() {		
		$("#fetch_data_cnr").prop("disabled",true);
		var cnr_array = JSON.parse($("#cnr_array").text());
		$.each(cnr_array,function(i,v){
			var ciNumber = v.CNR;
			var courtName = v.court_name;
			$.ajax({
				type : "GET",
				//url : "http://192.168.0.79:8080/LegalResearch/search_by_cnr",
				url : "http://203.192.219.179:8081/LegalResearch/search_by_cnr",
				async : false,
				data : {
					cino : encryptData(ciNumber),
					courtType : find_first_letter(courtName)
				},
				cache : false,
				success : function(response) {			
					//console.log(response)
					responseData.push(response);				
				}
			});
		});	
		console.log(responseData)
		update_data_cnr();
		$("#update_data_cnr").show();
	}
	
	$("#update_data_cnr").click(function(){	
		update_data_cnr();
	});
	
	function update_data_cnr(){
		$("#update_data_cnr").prop("disabled",true);
		$.each(responseData,function(i,v){
			$.ajax({
				url : "update_by_cnr_ajaxData.php",
				type : "POST",
				data : v,
				success : function(data){
					var data = JSON.parse(data);
					if(data.result == 'exist'){
						var cnr_exist = parseInt($("#cnr_exist").text());
						cnr_exist = cnr_exist + 1;
						$("#cnr_exist").text(cnr_exist);
						$(".cnr_exist_btn").show();
					}
					if(data.result == 'success'){
						var cnr_updated = parseInt($("#cnr_updated").text());
						cnr_updated = cnr_updated + 1;
						$("#cnr_updated").text(cnr_updated);
						$(".cnr_updated_btn").show();
					}
					if(data.result == 'failed'){
						var cnr_skiped = parseInt($("#cnr_skiped").text());
						cnr_skiped = cnr_skiped + 1;
						$("#cnr_skiped").text(cnr_skiped);
						$(this).prop("disabled",false);
						$(".cnr_skiped_btn").show();
					}					
				},
				error : function(data){
					console.log(data);
				}
			});
		});
	}
	
	var from_datepicker = new Date();
	var to_datepicker = new Date();
	
	function formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) 
			month = '0' + month;
		if (day.length < 2) 
			day = '0' + day;

		return [year, month, day].join('-');
	}
	
	$( "#from_datepicker" ).datepicker({
		dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true,
		//maxDate: '0'
    }).datepicker('setDate', 'today');;
	
    $( "#to_datepicker" ).datepicker({
		dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true,
		//maxDate: '0'
    }).datepicker('setDate', 'today');;
	
    $( "#datepicker2" ).datepicker({
		dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
    });
	
    $( "#datepicker3" ).datepicker({
		dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
    });
	
    $( "#datepicker4" ).datepicker({
		dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
    });
	
	$( "#datepicker5" ).datepicker({
		dateFormat: 'yy/mm/dd',
        changeYear: true,
        changeMonth: true
    });
	
	$("#checkAll").click(function () {
		$(".check").prop('checked', $(this).prop('checked'));
	});
	
	$("#submit_date").click(function(){
		from_datepicker = $("#from_datepicker").val();
		to_datepicker = $("#to_datepicker").val();
		order_listing_table.ajax.url('casediary_datatable.php?from_datepicker='+formatDate(from_datepicker)+'&to_datepicker='+formatDate(to_datepicker)).load();
	});
	
	var order_listing_table = $('#order_listing').DataTable({
	  'processing': true,
	  'serverSide': true,
	  'serverMethod': 'post',
	  'ajax': {
		  'url':'casediary_datatable.php?from_datepicker='+formatDate(from_datepicker)+'&to_datepicker='+formatDate(to_datepicker),
	  },
	  'columns': [
		 { data: 'case_id' },
		 { data: 'check_box', "orderable": false},
		 { data: 'court_name' },
		 { data: 'case_no' },
		 { data: 'case_title' },
		 { data: 'team_member' },
		 { data: 'hearing_date' },
		 { data: 'last_hearing_date' },
		 { data: 'stage' },
		 { data: 'case_description' },
		 { data: 'CNR' },
	  ]
	});
});