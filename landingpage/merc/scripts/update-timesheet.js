$(document).ready(function(){
	$( "#datepickertime").datepicker({
		dateFormat: 'yy-mm-dd',
		changeYear: true,
		changeMonth: true
	});
	$("#document").select2();
	
	
var timespent = $("#timespent_hidden").val();
var timespentin = $("#timeinmin_hidden").val();
getTimeSpent();
getTimeInMin();
getDropDownSelected(timespent,"time-spent");
getDropDownSelected(timespentin,"time-in-min");
});

function getTimeSpent(){
  var option = '<option value="">HH</option>';
  for(var i=0;i<=23;i++){
    option+='<option>'+i+'</option>';
  }
  $("#time-spent").html(option);
  $("#time-spent").select2();
}

function getTimeInMin(){
  var option = '<option value="">MM</option>';
  for(var i=0;i<=11;i++){
    option+='<option>'+i*5+'</option>';
  }
  $("#time-in-min").html(option);
  $("#time-in-min").select2();
}

function getDropDownSelected(courtname,courtId){
$("#"+courtId).val($("#"+courtId+" option:contains("+courtname+")").val());
	// $("#"+courtId).select2("val", $("#"+courtId+" option:contains("+courtname+")").val());
	$("#"+courtId).select2().trigger('change');
}