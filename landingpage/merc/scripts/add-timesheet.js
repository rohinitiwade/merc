$(document).ready(function() {
  $( "#datepicker" ).datepicker({
    /*minDate: 0,*/
    dateFormat: 'yy/mm/dd',
    changeYear: true,
    changeMonth: true
  });
  getTimeSpent();
  getTimeInMin();
});

function getTimeSpent(){
  var option = '<option value="">HH</option>';
  for(var i=0;i<=23;i++){
    option+='<option>'+i+'</option>';
  }
  $("#time-spent").html(option);
}

function getTimeInMin(){
  var option = '<option value="">MM</option>';
  for(var i=0;i<=11;i++){
    option+='<option>'+i*5+'</option>';
  }
  $("#time-in-min").html(option);
}
