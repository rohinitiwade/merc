$(".rowclickable").click(function(){
	var id = $(this).attr('data-id');
	location.href = "view-case.php?"+id;

});

$(document).ready(function() {
	$( "#datepicker" ).datepicker(
	{ 
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true
	} 
	);
	$( "#datepicker1" ).datepicker(
	{
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true 
	} );
	$('#example').DataTable();
} );

function download_doc(){
	var downloadtype = $("input[name='downloadas']:checked").val();
	var advocatesearch = $("#advocatesearch-id").val();
	$.ajax({
		type:'GET',
		url:'ajaxdata/export_dailycases.php',
		dataType:"json",
		data : {
			fromdate : $("#datepicker").val(),
			todate : $("#datepicker1").val()
		},
		success:function(response){
			console.log("advocate",response);
			var table = '<table class="table table-bordered main-table" id="main-table"><thead>';
			table +='<tr>';
			table += '<th>Sr No.</th>';
			table += '<th>Court</th>';
			table += '<th>case No/ Year</th>';
			table += '<th>Title</th>';
			table += '<th>Hearing Date</th>';
			table += '</tr></thead><tbody>';
			$.each(response.dailycases_array,function(i,obj){
				i++;
				table += '<tr>';
				table += '<td>'+i+'</td>';
				table += '<td>'+obj.court+'</td>';
				table += '<td>'+obj.cases+'</td>';
				table += '<td>'+obj.case_title+'</td>';
				table += '<td>'+obj.hearing_date+'</td>';        
				    
				table += '</tr>';
			});
			table +='</tbody></table>';
			$("#document-table-div-export").html(table);

// var pdfTitle = $("#case_type_export_report").text();
if(!isEmpty(response.dailycases_array))
	exportPdf(downloadtype,'DailyCases');
else{
	$("#export").modal('hide');
	toastr.warning("","No record found to export",{timeout:5000});
}
},
error:function(e){

}
});
}
