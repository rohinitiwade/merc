jQuery(function(){
	var todate = $("#to-date-value").val();
	var fromdate = $("#from-date-value").val();

	jQuery('#datepicker').datetimepicker({
		format:'d-m-Y H:i',
		timepicker:true,
		mask:true,
		step: 5,
		value : fromdate,
		onShow:function( ct ){
			this.setOptions({
				minDate:jQuery('#datepicker1').val()?jQuery('#datepicker1').val():false
			})
		},
	});
	jQuery('#datepicker1').datetimepicker({
		format:'d-m-Y H:i',
		timepicker:true,
		mask:true,
		step: 5,
		value : todate,
		onShow:function( ct ){
			this.setOptions({
				minDate:jQuery('#datepicker').val()?jQuery('#datepicker').val():false
			})
		},			
	});
});

$(document).ready(function(){
	/*$('#relate').select2({
		ajax: {
			url: 'ajaxdata/relatedto.php',
			data: function (params) {
				var query = {
					search: params.term,
					type: 'public'
				}

		      // Query parameters will be ?search=[term]&type=public
		      return query;
		  },
		  processResults: function(data) {
		  	// console.log(data);
		  	if (data.length)
		  		return {
		  			results: data
		  		};
		  	}
		  }
		});*/
	getRelateToDrop();
	getAdvocates();
	getEditToDoDetails();
	getTeamMemberListDropdown();
	
});

function getRelateToDrop(){
	var todoids = $("#todoid").val();
	var selectedOptions = [];
	$.ajax({
		type:'GET',
		url:'ajaxdata/load_relateto.php',
		dataType:"json",
		data:{
			search: 'edit',
			todoid:todoids
		},
		async:false,
		cache:false,
		success:function(response){
			// console.log(response);
			var option='';
			var shoData = response.relate;
			$.each(shoData,function(i,obj){
				option +='<option value="'+obj.case_id+'">'+obj.case_type+'</option>';
			});
			$("#relate").html(option);
			$("#relate").select2({
		placeholder: 'Select...',
		closeOnSelect: false,
		multiple: true,
		allowClear: false
	});
			$.each(response.show_relate,function(i,obj){
	selectedOptions.push(obj.case_id);
});
$('#relate').val(selectedOptions).trigger('change');
		},
		error:function(e){

		}
	});
}

function getEditToDoDetails(){
	
	var todo_id = $("#todoid").val();
	$.ajax({
		type:'POST',
		url:host+'/getEdittodo.php',
		dataType:"json",
		data:{
			todo_id : todo_id
		},
		success:function(response){
			// console.log(response);
			getToDoDetails(response.todo_details);
			getToDoReminder(response.todo_reminder);
			// getToDoRelate(response.todo_relate);
			getToDoAdvocate(response.todo_advocate);
		},
		error:function(e){

		}
	});
}

function getToDoAdvocate(response){
	var selectedOptions = [];
	$.each(response,function(i,obj){
		selectedOptions.push(obj.adv_id);
		// $('#advocate').val($("#advocate option:contains("+selectedOptions+")").val());	
	});
	$('#advocate').val(selectedOptions).trigger('change');
// $('#advocate');
}

function getToDoRelate(response){
	var selectedOptions =[];
	$.each(response,function(i,obj){
		selectedOptions.push(obj.case_id);
		// $('#relate').val($("#relate option:contains("+obj.case_type+")").val());
		// console.log("selectedOptions",selectedOptions);
	});
	$('#relate').val(selectedOptions).trigger('change');
}

function updatetodo(){
	var todo_id = $("#todoid").val();
	var select_receipent_array = [];
	var assign_to_array = [];
	var reminder_array = [];
	var assign_adv = [];
	var privated = "public";
	var desc = $("#desc").val();
	if(desc == ''){
		toastr.warning('Please Enter To Do Description', {timeOut: 5000});  
		return false;
	}
	var datepicker = $("#datepickerdataedit").val();
	var replace_date1 =  datepicker.replace('/','');
	// var datepicker1 = $("#datepicker1").val();
	// var replace_date1_todo =  datepicker1.replace('/','');
	// var email_option = $("#email_option").val();
	// var time = $("#time").val();
	// var minute = $("#minute").val();
	//var private = $("#private").val();
	if(document.getElementById('private').checked){
		privated = "private";
	}
	var full_name = $("#full_name").val();
	var assign_to = $("#framework").val();
	$.each(assign_to,function(a,obj){
		assign_to_array.push(obj);
	});
	// var adv = $("#advocate").val();
	// $.each(adv,function(a,ad){
	// 	assign_adv.push(ad);
	// });

	for(var i = 0;i<=rcounter;i++){
		var email_option = $("#email_option_"+i).val();
		var time  = $("#reminder_format_value_"+i).val();
		var minute = $("#minute_"+i).val();
		var reminderVo = {
			email_option : email_option,
			time : time,
			minute : minute
		}
		reminder_array.push(reminderVo);
	}

	
	var relate_to = $("#relate").select2('data');
	$.each(relate_to,function(i,obj){
		select_receipent_array.push(obj);
	});
	var todoVo = new Object();
	todoVo.desc = desc;
	todoVo.datepicker = replace_date1;
	// todoVo.datepicker1 = replace_date1_todo;
	// todoVo.option = option;
	// todoVo.time = time;
	// todoVo.minute = minute;
	todoVo.private = privated;
	todoVo.full_name = full_name;
	todoVo.assign_to_array = assign_to_array;
	todoVo.relateto = select_receipent_array;
	todoVo.reminder_array = reminder_array;
	// todoVo.advocates = assign_adv;
	todoVo.todo_id = todo_id;
	$.ajax({
		url: 'update_todo.php',
		type:'POST',
		data : "tododetail="+JSON.stringify(todoVo),
		dataType: 'json',
		async:false,
		beforeSend: function(){
			$('.flip-square-loader').show(); 
			toastr.success('Submit Data Successfully', 'Success Alert', {timeOut: 5000})             
		},
		success:function(data){
			// console.log(data);
			if(data.status == 'success'){
				// location.reload();

window.location.href='create-todo?name=All';		}
		},
		complete: function(){
			$('.flip-square-loader').hide();  

		},
		error: function(e){
			$('.flip-square-loader').hide();  
			toastr.error('Unable to Submit Data', 'Sorry For Inconvenience!', {timeOut: 5000})
		}
	});
	
}

function getToDoDetails(response){
	$.each(response,function(i,obj){
		$("#desc").val(obj.content);
		$("#datepickerdataedit").val(obj.from_date);
		// $("#datepicker1").val(obj.to_date);
		if(obj.private == "private")
			$("#private").prop("checked",true);
	});
}

function getToDoReminder(response){
	var reminder='';
	$.each(response,function(i,obj){
		reminder += '<div class="col-sm-12 row">';
		if(i== 0)
			reminder +='<div class="col-sm-1 p-0" style="float: left;"></div>';
		
		else{
			reminder +='<div class="col-sm-1 p-0" style="float: left;">';
			reminder +='<button class="btn btn-danger btn-mini" type="button" onclick="remove_field()">';
			reminder +='<i class="feather icon-trash autominus"></i></button>';
			reminder +='</div>';
		}
		
		reminder +='<div class="col-sm-3 p-0 form-group" style="float: left;">';
		reminder +='<select class="form-control" id="email_option_'+i+'">';
		// reminder +='<option value=""></option>';
		if(obj.email_option == 'Email'){
			reminder +='<option value="Email" selected>Email</option>';
			reminder +='<option value="SMS">SMS</option>';
		}
		else if(obj.email_option == 'SMS'){
			reminder +='<option value="Email">Email</option>';
			reminder +='<option value="SMS" selected>SMS</option>';
		}
		reminder +='</select>';
		reminder +='</div>';
		reminder +='<div class="col-sm-3 form-group" style="float: left;">';
		reminder +='<select class="form-control reminder_format" id="minute_'+i+'">';
		reminder +=' <option value="minutes">minute(s)</option>';
		reminder +='<option value="hours">hour(s)</option>';
		reminder +='<option value="days">day(s)</option>';
		reminder +='<option value="week">week(s)</option>';
		reminder +='</select>';
		reminder +='</div>';
		reminder +='<div class="col-sm-2 form-group" style="float: left;">';
		reminder +='<select class="form-control" id="reminder_format_value_'+i+'">';
		reminder +='<option value="5">5</option>';
		reminder +='<option value="10">10</option>';
		reminder +='<option value="15">15</option>';
		reminder +='<option value="20">20</option>';
		reminder +='<option value="25">25</option>';
		reminder +='<option value="30">30</option>';
		reminder +='<option value="35">35</option>';
		reminder +='<option value="40">40</option>';
		reminder +='<option value="45">45</option>';
		reminder +='<option value="50">50</option>';
		reminder +='<option value="55">55</option>';
		reminder +='</select>';
		reminder +=' </div>';
		reminder +='<span class="col-sm-3">before the due date & time</span>';
		reminder +='</div>';
	});
	$("#reminder_clickhere").hide();
	$("#reminder_input").show();
	$("#input").html(reminder);
	$.each(response,function(i,obj){
		var minute_div = document.getElementById('minute_'+i);
		setSelectedValue(minute_div,obj.minute,i);

		var second_div = document.getElementById('reminder_format_value_'+i);
		setSelectedValue(second_div,obj.time_no,i);
	});
}

function setSelectedValue(selectObj, valueToSet, counter) {
	if(valueToSet == 'minutes' || valueToSet == 'hours' || valueToSet == 'days' || valueToSet == 'week')
		reminder_format(valueToSet,counter);
	for (var i = 0; i < selectObj.options.length; i++) {
		if (selectObj.options[i].value== valueToSet) {
			selectObj.options[i].selected = true;
			return;
		}
	}
}

function getTeamMemberListDropdown(){
	var todoid = $("#todoid").val();
// var hidValue = $("#hidSelectedOptions").val();
var selectedOptions = [];

$.ajax({
	type:'POST',
	url:host+'/your_team.php',
	dataType:"json",
	 data:"flag=edit&&todo_id="+todoid,
	success:function(response){
		// console.log(response);
// var response = JSON.parse(response);
if(response.team.length > 0){
	var option = '<option value="">Select</option>';
	$.each(response.team,function(i,obj){
		option += '<option value='+obj.reg_id+'>'+obj.name+' '+obj.last_name+'</option>';
	});
	$('#framework').html(option);
	$('#framework').select2({
		placeholder: 'Select...',
		closeOnSelect: false,
		multiple: true,
		allowClear: false
	});
}
$.each(response.show_team,function(i,obj){
	selectedOptions.push(obj.reg_id);
});
$('#framework').val(selectedOptions).trigger('change');
},
error:function(){
	toastr.error("","Error fetching team list",{timeout:5000});
}
});
}
function getAdvocates(){
	
// var hidValue = $("#hidSelectedOptions").val();
var selectedOptionsa = [];

$.ajax({
	type:'POST',
	url:host+'/your_advocates.php',
	dataType:"json",
	async:false,
	cache:false,
	success:function(response){
		// console.log(response);
// var response = JSON.parse(response);
if(response.team.length > 0){
	var option = '<option value="">Select</option>';
	$.each(response.team,function(i,obj){
		option += '<option value='+obj.reg_id+'>'+obj.name+'</option>';
	});
	$('#advocate').html(option);
	$('#advocate').select2({
		placeholder: 'Select...',
		closeOnSelect: false,
		multiple: true,
		allowClear: false
	});
}
$.each(response.show_team,function(i,obj){
	selectedOptions.push(obj.reg_id);
});
$('#advocate').val(selectedOptionsa).trigger('change');
},
error:function(){
	toastr.error("","Error fetching team list",{timeout:5000});
}
});
}

function reminder_format(val,counter){
	var reminder_format_value='';
	// var val = $("#minute_"+i).val();

	if(val == 'minutes'){
		for(var i =1 ; i <= 11;i++){
			reminder_format_value += '<option value="'+i*5+'">'+i*5+'</option>';
		}
	}
	if(val == 'hours'){
		for(var i =1 ; i <= 23;i++){
			reminder_format_value += '<option value="'+i+'">'+i+'</option>';
		}
	}
	if(val == 'days'){
		for(var i =1 ; i <= 31;i++){
			reminder_format_value += '<option value="'+i+'">'+i+'</option>';
		}
	}
	if(val == 'week'){
		for(var i =1 ; i <= 4;i++){
			reminder_format_value += '<option value="'+i+'">'+i+'</option>';
		}
	}
	$("#reminder_format_value_"+counter).html(reminder_format_value);
}