var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");;
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
$("#order-listing").dataTable();
$("#datepicker").datepicker({
dateFormat: 'yy/mm/dd',
	changeYear: true,
	changeMonth: true,
	todayHighlight : true
});
$("#datepicker1").datepicker({
	dateFormat: 'yy/mm/dd',
	changeYear: true,
	changeMonth: true,
	todayHighlight : true
});
function download_doc(){
	var downloadtype = $("input[name='downloadas']:checked").val();
	$.ajax({
		type:'GET',
		url:'ajaxdata/export_expenses.php',
		dataType:"json",
		data : {
			case_no: $("#case_no").val()
		},
		success:function(response){
			console.log("timesheet",response);
			var table = '<table class="table table-bordered main-table" id="main-table"><thead>';
			table +='<tr>';
			table += '<th>Sr No.</th>';
			table += '<th>Cases</th>';
			table += '<th>Particulars</th>';
			table += '<th>Money Spent</th>';
			table += '<th>Methohd</th>';
			table += '<th>Date</th></tr></thead><tbody>';
			$.each(response.expenses_array,function(i,obj){
				i++;
				table += '<tr>';
				table += '<td>'+i+'</td>';
				table += '<td>'+obj.cases+'</td>';
				table += '<td>'+obj.particulars+'</td>';
				table += '<td>'+obj.moneyspent+'</td>';
				table += '<td>'+obj.method+'</td>';
				table += '<td>'+obj.date+'</td></tr>';
			});
			table +='</tbody></table>';
			$("#document-table-div-export").html(table);

// var pdfTitle = $("#case_type_export_report").text();


if(!isEmpty(response.expenses_array))
 exportPdf(downloadtype,'Expenses');
else{
  $("#export").modal('hide');
  toastr.warning("","No record found to export",{timeout:5000});
};

},
error:function(e){

}
});
}