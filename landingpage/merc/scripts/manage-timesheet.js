$(document).ready(function(){
	$( "#datepicker" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeYear: true,
		changeMonth: true
	});
	$( "#datepicker1" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeYear: true,
		changeMonth: true
	});
	$( "#datepicker2" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeYear: true,
		changeMonth: true
	});
	$( "#datepicker3" ).datepicker({
		dateFormat: 'yy-mm-dd',
		changeYear: true,
		changeMonth: true
	});
	$(".rowclickable").click(function(){
		var id = $(this).attr('data-id');
		location.href = "view-case.php?"+id;

	});
});

function download_doc(){
	var downloadtype = $("input[name='downloadas']:checked").val();
	$.ajax({
		type:'GET',
		url:'ajaxdata/export_timesheet.php',
		dataType:"json",
		data : {
			sdate: $("#datepicker2").val(),
			todate: $("#datepicker3").val()
		},
		success:function(response){
			console.log("timesheet",response);
			var table = '<table class="table table-bordered main-table" id="main-table"><thead>';
			table +='<tr>';
			table += '<th>Sr No.</th>';
			table += '<th>Cases</th>';
			table += '<th>Date</th>';
			table += '<th>Particulars</th>';
			table += '<th>Time in min</th></tr></thead><tbody>';
			$.each(response.timesheet_array,function(i,obj){
				i++;
				table += '<tr>';
				table += '<td>'+i+'</td>';
				table += '<td>'+obj.cases+'</td>';
				table += '<td>'+obj.date+'</td>';
				table += '<td>'+obj.particulars+'</td>';
				table += '<td>'+obj.timeinmin+'</td></tr>';
			});
			table +='</tbody></table>';
			$("#document-table-div-export").html(table);

// var pdfTitle = $("#case_type_export_report").text();


if(!isEmpty(response.timesheet_array))
 exportPdf(downloadtype,'Timesheet');
else{
  $("#export").modal('hide');
  toastr.warning("","No record found to export",{timeout:5000});
};


},
error:function(e){

}
});
}