
$(document).ready(function(){
  $("#slide-togglecheckboxes").click(function(){
    $("#collectioncheck").slideToggle();
  });
  $("#checkAll").click(function () {
    if($(this).prop("checked")) {
      $(".check").prop("checked", true);
    } else {
      $(".check").prop("checked", false);
    }   
  });
  $('.check').click(function(){
    if($(".check").length == $(".check:checked").length) {
      $("#checkAll").prop("checked", true);
    }else {
      $("#checkAll").prop("checked", false);            
    }
  });
  $(document).on('click',"#advslide-togglecheckboxes",function(){
    $("#collectioncheckAdv").slideToggle();
  });

  $(document).on('click', ".rowclickable", function(){
    var id = $(this).attr('data-id');
    location.href = "view-case.php?"+id;

  });
});

function viewcases (id,adv_id) {
  var id = +id;
  var adv_id = adv_id;
  $.ajax({
    type:'POST',
    url:'ajaxdata/advocate_cases.php',
    data:'case_id='+id+'&&adv_id='+adv_id,
    beforeSend: function()
    { 
      $('#flip-square-loader').show();
    },
    success:function(html){
      $('#casedetails').html(html);
    },
    complete: function(){ 
      $('#flip-square-loader').hide();
    },
    error:function(e){
     $(".flip-square-loader").hide();
     toastr.error('Unable to Load ', 'Sorry For Inconvenience!', {timeOut: 5000});
   }
 }); 
}

function download_doc(){
  var downloadtype = $("input[name='downloadas']:checked").val();
  var advocatesearch = $("#advocatesearch-id").val();
  $.ajax({
    type:'GET',
    url:'ajaxdata/export_advocate.php',
    dataType:"json",
    data : {
      advocatesearch : advocatesearch
    },
    success:function(response){
      console.log("advocate",response);
      var table = '<table class="table table-bordered main-table" id="main-table"><thead>';
      table +='<tr>';
      table += '<th>Sr No.</th>';
      table += '<th>Full Name</th>';
      table += '<th>Email</th>';
      table += '<th>Mobile</th>';
      table += '<th>Website</th>';
      table += '<th>Company Name</th>';
      table += '<th>GST</th>';
      table += '<th>Tin</th>';
      table += '<th>PAN No.</th></tr></thead><tbody>';
      $.each(response.advocate_array,function(i,obj){
        i++;
        table += '<tr>';
        table += '<td>'+i+'</td>';
        table += '<td>'+obj.full_name+'</td>';
        table += '<td>'+obj.email+'</td>';
        table += '<td>'+obj.mobile+'</td>';
        table += '<td>'+obj.website+'</td>';        
        table += '<td>'+obj.company_name+'</td>';        
        table += '<td>'+obj.gst+'</td>';   
        table += '<td>'+obj.tin+'</td>';      
        table += '<td>'+obj.pan_no+'</td></tr>';
      });
      table +='</tbody></table>';
      $("#document-table-div-export").html(table);

// var pdfTitle = $("#case_type_export_report").text();

exportPdf(downloadtype,'Advocate');
},
error:function(e){

}
});
}

function viewdetail (id) {
  var id = +id;
  $.ajax({
    type:'POST',
    url:'ajaxdata/advocate_details.php',
    data:'id='+id,
    beforeSend: function()
    { 
      $('#flip-square-loader').show();
    },
    success:function(html){
      $('#advdetails').html(html);
    },
    complete: function(){ 
      $('#flip-square-loader').hide();
    },
    error:function(e){
     $(".flip-square-loader").hide();
     toastr.error('Unable to Load ', 'Sorry For Inconvenience!', {timeOut: 5000});
   }
 }); 
}

function PrintDiv() {    
 var divToPrint = document.getElementById('divToPrint');
 var popupWin = window.open('', '_blank', 'width=300,height=300');
 popupWin.document.open();
 popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
 popupWin.document.close();
}

// function downloadExcel(flag){
//   var flag = flag;
//   $.ajax({
//     type:'POST',
//     url:'downloadClient.php',
//     data:'flag='+flag,
//     success:function(html){
//       $('#casedetails').html(html);
//     },
//     error:function(e){
//      $(".flip-square-loader").hide();
//      toastr.error('Unable to Load ', 'Sorry For Inconvenience!', {timeOut: 5000});
//    }
//  }); 

// }