// $("#legal-table").DataTable();
$("#view-legalaudit").DataTable();
$(document).ready(function(){
viewLegalResponse(1);
legalReportList(1,'all');

});

function closeFilter(){
  $("#showfilter").hide();
}

$("#myBtn-filter").on('click',function(){
  $("#showfilter").show();
});


function legalReportList(currentpage,flag,page){
  debugger;
  var case_no = $("#case_no").val();
  var type_of_litigation = $("#type_of_litigation").val();
  var court_name = $("#court_name").val();
   var case_no_year = $("#case_no_year").val();
  var risk_category = $("#risk_category").val();
  var case_title = $("#case_title").val();
  var limit = '10';
  var offsets =   offset(currentpage,limit);
  
$.ajax({
  type:'GET',
  url:'ajaxdata/getLegalResponseList.php',
  data:"case_no="+case_no+"&type_of_litigation="+type_of_litigation+"&court_name="+court_name+"&case_no_year="+case_no_year+"&risk_category="+risk_category+"&case_title="+case_title+"&limit="+limit+"&offset="+offsets+"&flag="+flag,
  dataType: 'json',
  beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
         $("#showfilter").hide();
        var getdata = '';
        getdata += '<input type="hidden" id="countn" value='+html.total_count+'><table class="table table-bordered" id="main-table">';
         getdata += '<thead class="bg-primary"><tr>';
         getdata += '<th class="checkclass header_checkbox">Sr.No.</th>';
          getdata += '<th class="court-report">Type of Litigation</th>';
        getdata += ' <th class="court-report">Court/ Forum</th>';
        getdata += '<th class="case-report">Case No / Year</th> ';
        getdata += '<th class="case-report">Risk Category</th> ';                                 
       getdata += '<th class="title-report">Title/ Subject Matter</th>';
       getdata += '<th class="court-report">Case Uploaded By</th>';
        getdata += '<th class="case-details-report">Action</th>';
       
        getdata += '</thead><tbody><input type="hidden" name="" id="caseid">';
         
        $.each(html.data,function(i,get){
          i++;
         getdata += '<tr id="report_tr'+get.case_id+'" style="cursor:pointer;background-color:';
         if(get.type_of_litigation == "Litigations Filed by MSEDCL")
         getdata += "#c9e2f5";
         else
        getdata += "#f99d9d";
          getdata += '">';
         getdata += '<td class="checkboxtimesheet" data-id="caseid='+get.enc_case_id+'"><div class="checkbox-zoom zoom-primary">'; 
           getdata += '<label><span class="srno">'+i+' </span> ';
           getdata += ' <input type="checkbox" class="check case_checkbox" name="select_case_single" id="case_checkbox" value="'+get.case_id+'">';
          getdata += ' <span class="cr"> <i class="cr-icon icofont icofont-ui-check txt-primary">  </i> </span> </label></div></td>';
       // getdata += '<td class="rowclickable nastikramank-td" data-id="caseid='+get.case_id+'" style="padding: 10px;">'+get.case_id+'</td>';
     getdata += '<td class="rowclickable" data-id="caseid='+get.enc_case_id+'" style="padding: 10px;">'+get.type_of_litigation+'</td>'; 
     // getdata += '<td class="rowclickable" data-id="caseid='+get.enc_case_id+'" style="padding: 10px;">'+get.party_name+'</td> ';
     getdata += '<td class="rowclickable" data-id="caseid= '+get.enc_case_id+'" style="padding: 10px;">'+get.court_name+'</td>';                                 
     getdata += '<td class="rowclickable" data-id="caseid='+get.enc_case_id+'" style="padding: 10px;">'+get.case+'</td>';
     getdata += '<td class="rowclickable" data-id="caseid= '+get.enc_case_id+'" style="padding: 10px;">'+get.risk_category+'</td>';
       getdata += '<td class="rowclickable" data-id="caseid= '+get.enc_case_id+'" style="padding: 10px;">'+get.case_title+'</td>';
       getdata += '<td class="rowclickable" data-id="caseid='+get.enc_case_id+'" style="padding: 10px;">'+get.team_member+'</td> ';
    // getdata += ' <td> <button type="button" class="btn btn-sm btn-primary" onclick="getData('+get.case_id+')">View Case Law</button>   </td>';
    getdata += '<td style="padding: 10px;text-align:center;"><a href="view-case.php?caseid='+get.enc_case_id+'" style="cursor:pointer;color: #01a9ac !important;" class="feather icon-eye viewdetailscase report-icon" title="View" target="_BLANK" ></a></td></tr>'; 
  });
 getdata += '</tbody></table>';
// getdata += ' <tfoot><tr><td colspan="11"><button class="btn btn-sm btn-info" type="button" onclick="recordHearingModal()">Record Hearing </button></td></tr>';
getdata += '</div>';
     $("#showlegal_data").html(getdata);
      $("#main-table").DataTable();
     $("#casesCount").text(html.total_count);
    },
       complete: function(){
        $("#type_of_litigation").val('');
      $("#court_name").val('');
      $("#case_no").val('');
       $("#case_no_year").val('');
      $("#risk_category").val('');
      $("#case_title").val('');
        var counts = $("#countn").val();
       if(page !='page'){
      showPaginationList(counts,10,flag);}

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Documents', {timeOut: 5000})
    }
  }); 
 
}




var currentPage;
      function showPaginationList(counts,limit,flag) {
    // Number of items and limits the number of items per page
    var numberOfItems = counts;
    var limitPerPage =limit;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    var paginationSize = 7; 
    function showPage(whichPage) {
      if (whichPage < 1 || whichPage > totalPages) return false;
      currentPage = whichPage;                   
      $(".pagination li").slice(1, -1).remove();
      getPageListReport(totalPages, currentPage, paginationSize).forEach( item => {
        $("<li>").addClass("page-item")
        .addClass(item ? "current-page" : "disabled")
        .toggleClass("active", item === currentPage).append(
          $("<a>").addClass("page-link").attr({
            href: "javascript:void(0)"}).text(item || "...")
          ).insertBefore("#next-page");
      });
        // Disable prev/next when at first/last page:
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }
    $(".pagination").append(
      $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Prev")
        ),
      $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Next")
        )
      );
    showPage(1);

    // Use event delegation, as these items are recreated later    
    $(document).on("click", ".pagination li.current-page:not(.active)", function () {
      var s=showPage(+$(this).text());
      var page = 'page';
      legalReportList(+$(this).text(),flag,page);
      return s;
    });
    $("#next-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage+1);
        legalReportList(currentPage+1,flag,page);
        return s;
      }
    });

    $("#previous-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage-1);
        legalReportList(currentPage-1,flag,page);
        return s ;
      }
    });

}

function getPageListReport(totalPages, page, maxLength) {
  if (maxLength < 5) throw "maxLength must be at least 5";

  function range(start, end) {
    return Array.from(Array(end - start + 1), (_, i) => i + start); 
  }

  var sideWidth = maxLength < 9 ? 1 : 2;
  var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
  var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
  if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength-sideWidth-1)
        .concat([0])
        .concat(range(totalPages-sideWidth+1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
        .concat([0])
        .concat(range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }
    // Breaks on both sides
    return range(1, sideWidth)
    .concat([0])
    .concat(range(page - leftWidth, page + rightWidth)) 
    .concat([0])
    .concat(range(totalPages-sideWidth+1, totalPages));
}


function legalsupport(){
  // debugger;
  if($('.check:checked').length <= 0){
    toastr.warning("",'Please select atleast one case',{timeout:5000});
    return false;
  }

  $.each($("input[name='select_case_single']:checked"),function(i,obj){
    var caseids = $(this).val();

    var case_id = caseids;
    var user = $("#email").val();
    var firstname = $("#fname").val();
    var lastname = $("#lname").val();
    var contact = $("#mobile").val();
     var documentTypeArray= [];
      var fileTypeArray= [];   
    var ajaxResult = $.ajax({
      url : host+"/submit_rdd.php",
      type : 'POST',
          // data : 'caseid='+case_id,   
          data: "caseid="+case_id+"&flag=get",   
          // dataType:"json",
          cache : false,
          // contentType : false,
          // processData : false,
          success:function(response){
              var dataobject=JSON.parse(response);
            if(dataobject.status == 'success'){


              var QueryVo=new Object();
              QueryVo.user=user;
              QueryVo.firstname=firstname;
              QueryVo.lastname=lastname;
              QueryVo.contact=contact;
              var CaseRegisterVO=new Object();  
              CaseRegisterVO.casetitleId=case_id;
              CaseRegisterVO.queryVo=QueryVo;
        // CaseRegisterVO.email=email;
        CaseRegisterVO.user=user; 
        CaseRegisterVO.caseTitle=dataobject.case_title;
        var RddCaseDetailVO= new Object(); 
                RddCaseDetailVO.caseNumber= dataobject.case_no;
                RddCaseDetailVO.caseYear= dataobject.case_no_year;
                RddCaseDetailVO.caseTitleId= dataobject.case_id;
                RddCaseDetailVO.court= dataobject.court_name; 
                RddCaseDetailVO.createBy= user;
                RddCaseDetailVO.typeOfLitigation= dataobject.typeOfLitigation;
                RddCaseDetailVO.riskCategory= dataobject.riskCategory;
                RddCaseDetailVO.organisation_id= dataobject.organisation_id;
                
                CaseRegisterVO.rddCaseDetailVO= RddCaseDetailVO;

               //   $.ajax({
                  // type: "POST",
                  // url: "ajaxdata/getmatter.php",
                  // data: 'caseid='+case_id+'',
                  // async:false,
                  // success: function(result){
                 // console.log(result);
                 // var dataobject=JSON.parse(result);
                 $.each(dataobject.document,function(i,obj){
                  var DocumentTypeVO=new Object();
                  DocumentTypeVO.documentTypeId=obj.documentTypeId;
                  DocumentTypeVO.documentTypeCode=obj.documentTypeCode;
                  DocumentTypeVO.instruction=obj.instruction;


                  $.ajax({
                    type: "POST",
                    url: "ajaxdata/getdocuments.php",
                    data: 'caseid='+case_id+'&&doctype='+obj.documentTypeCode,
                    async:false,
                    success: function(results){
                      var datadoc=JSON.parse(results);
                      var fileTypeArray= [];                                  
                      $.each(datadoc,function(i,objs){
                        var FileTypeVO=new Object();
                        FileTypeVO.fileid=objs.fileid;
                        FileTypeVO.filename=objs.filename;
                        FileTypeVO.fileUrl=objs.document;
                        fileTypeArray.push(FileTypeVO);
                        // console.log(fileTypeArray); 
                    });
                      DocumentTypeVO.fileTypeVOs=fileTypeArray;                       
                    }
                  });
                  documentTypeArray.push(DocumentTypeVO); 
                 });

  // }
  //        });
                    CaseRegisterVO.documentTypeVOs=documentTypeArray;
                    console.log(CaseRegisterVO);
                    var dataSend = {
    CaseRegisterVO:JSON.stringify(CaseRegisterVO)
  }
        $.ajax({
           url: "allapi.php",
    type:'POST',
    data: {params :JSON.stringify(CaseRegisterVO),
flag: 'legal_support'
    },
          // url: legalResearch+"/addprocessdata",
            // url:"http://192.168.0.71:8080/LegalResearch/addprocessdata",
            // url:legalResearch+"/addprocessdata",
            // type:'POST',
            // data: JSON.stringify(CaseRegisterVO),
            // dataType: 'json',
            // async:false,
            // headers: {
            //  "Content-Type": 'application/json'
            // },
             // dataType: 'json',
    async:false,
            beforeSend : function(){
              // $(".flip-square-loader").show();
                $(".add_casesubmitdiv").addClass("fetchdataloader");
  $(".add_casesubmit").hide();
            },
            success:function(data){ 
              // console.log(data.legal_support);
               var datas = JSON.parse(data);
              var strerror = datas.error;
            // var nerror = strerror.includes("Duplicate");
              // var error = "CallableStatementCallback; SQL [{call proc_rdd_add_casetitle_data(?, ?, ?, ?, ?)}Duplicate entry '12' for key 'PRIMARY'; nested exception is com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Duplicate entry '12' for key 'PRIMARY'";
              if(strerror == "SUCCESS")
              {
                  var ajaxResult = $.ajax({
                  url : host+"/submit_rdd.php",
                  type : 'POST',
                  // data : 'caseid='+case_id,   
                  data: "caseid="+case_id,   
                  // dataType:"json",
                  cache : false,
                  // contentType : false,
                  // processData : false,
                  success:function(response){
                    if(dataobject.status == 'success'){
                        toastr.success("","Sent for Legal Support", {timeOut: 5000})
                         // document.getElementById("report_tr"+case_id).outerHTML = '';
                      } 
                    }
                  });
                }
                else if(strerror != "SUCCESS")
                  toastr.error("","Not Sent for Legal Support", {timeOut: 5000})
          },
          complete:function(){
            // $(".flip-square-loader").hide();
            $(".add_casesubmitdiv").removeClass("fetchdataloader");
  $(".add_casesubmit").show();
          },
          error:function(){
            $(".add_casesubmitdiv").removeClass("fetchdataloader");
  $(".add_casesubmit").show();
            toastr.error("","Error in Uploading File",{timeout:5000});
          }
      });
    }
}
});

  });
}
 
function viewLegalResponse(currentpage){
  
   var limit = '10';
  var offsets =   offset(currentpage,limit);
  $.ajax({
      type:'POST',
      url:'ajaxdata/viewLegalData.php',
      data:"limit="+limit+"&offset="+offsets+"&flag=all",
      dataType: 'json',
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
    // console.log(html);
    var getdata = '';
     $("#viewResponseCount").html(html.total_count);
     // $.each(data,function(key,val){
       // $("#showfilter").hide();
     getdata += ' <input type="hidden" name="" id="caseid">';
         getdata += '<input type="hidden" name="email" id="email" value="'+html.email+'">';
          getdata += '<input type="hidden" id="fname" value="'+html.name+'">';
         getdata += '<input type="hidden" id="lname" value="'+html.last_name+'">';
        getdata += '<input type="hidden" id="mobile" value="'+html.mobile+'"><input type="hidden" id="countn" value='+html.total_count+'> <table id="order-listing" class="table table-bordered">';
                  getdata += ' <thead class="bg-primary">';
                   getdata += ' <tr> <th>Sr.No. </th> <th>Type of Litigation</th> <th>Court/ Forum</th><th>Case No/ Year</th> <th>Date of Uploading</th> <th>Date of Response</th> <th>View Documents</th><th>  View Legal Audit</th></tr>';
     getdata += '       </thead> <tbody>';
     var i=1;
     $.each(html.data,function(is,get){
       getdata += '<tr> <td>'+i+'</td>';
        
               getdata += '<td>'+get.type_of_litigation+'</td> ';
                
        
         getdata += '  <td>'+get.court_name+'</td>';
          getdata += '  <td>'+get.case_no+'/ ' +get.case_no_year+'</td>';
           getdata += '  <td>'+get.case_enter_date_time+'</td>';
            getdata += ' <td> </td>';
             getdata += '  <td><a href="view-case.php?caseid='+get.enc_case_id+'" class="viewDoc">View Documents</a></td>';
             
             getdata +='<td class="" data-id="caseid='+get.enc_case_id+'" style="padding: 10px;">';
             // getdata +=' <button class="btn btn-sm btn-info" type="button" onclick="researcherModal('+get.case_id+')"> View Response </button>';
             getdata +=' <a href="viewLegalAudit.php?caseid='+get.enc_case_id+'" target="_blank" class="btn btn-sm btn-info" type="button" > View Legal Audit </a>&nbsp;&nbsp;<button class="btn btn-sm btn-success" type="button" onclick="viewAnswer()"">View Answer</button> ';
              getdata +='</td></tr>';
        i++;
     });
     getdata += '</tbody></table?';
   // });
   
     $("#show_data").html(getdata);
      $("#order-listing").DataTable();
     },
     complete: function(){
      var counts = $("#countn").val();
      showPagination(counts,10);
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Documents', {timeOut: 5000})
    }
  }); 
}

function offset(page,limit){
  if(page <= 1)
    return 0;
  else
    return ((page-1)*limit);
}

var currentPage;
      function showPagination(counts,limit) {
    // Number of items and limits the number of items per page
    var numberOfItems = counts;
    var limitPerPage =limit;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    var paginationSize = 7; 
    function showPage(whichPage) {
      if (whichPage < 1 || whichPage > totalPages) return false;
      currentPage = whichPage;                   
      $(".pagination li").slice(1, -1).remove();
      getPageList(totalPages, currentPage, paginationSize).forEach( item => {
        $("<li>").addClass("page-item")
        .addClass(item ? "current-page" : "disabled")
        .toggleClass("active", item === currentPage).append(
          $("<a>").addClass("page-link").attr({
            href: "javascript:void(0)"}).text(item || "...")
          ).insertBefore("#next-page");
      });
        // Disable prev/next when at first/last page:
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }
    $(".pagination").append(
      $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Prev")
        ),
      $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Next")
        )
      );
    showPage(1);

    // Use event delegation, as these items are recreated later    
    $(document).on("click", ".pagination li.current-page:not(.active)", function () {
      var s=showPage(+$(this).text());
      getpagination(+$(this).text());
      return s;
    });
    $("#next-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage+1);
        getpagination(currentPage+1);
        return s;
      }
    });

    $("#previous-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage-1);
        getpagination(currentPage-1);
        return s ;
      }
    });

}


function getPageList(totalPages, page, maxLength) {
  if (maxLength < 5) throw "maxLength must be at least 5";

  function range(start, end) {
    return Array.from(Array(end - start + 1), (_, i) => i + start); 
  }

  var sideWidth = maxLength < 9 ? 1 : 2;
  var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
  var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
  if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength-sideWidth-1)
        .concat([0])
        .concat(range(totalPages-sideWidth+1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
        .concat([0])
        .concat(range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }
    // Breaks on both sides
    return range(1, sideWidth)
    .concat([0])
    .concat(range(page - leftWidth, page + rightWidth)) 
    .concat([0])
    .concat(range(totalPages-sideWidth+1, totalPages));
}


function getpagination(currentpage){
  var limit = '10';
  var offsets =   offset(currentpage,limit);
  $.ajax({
      type:'POST',
      url:'ajaxdata/viewLegalData.php',
      data:"limit="+limit+"&offset="+offsets+"&flag=all",
      dataType: 'json',
      beforeSend: function(){
      // xhr.setRequestHeader("Authorization","Bearer "+getHeaderToken()); 
      $('.flip-square-loader').show();              
    },
    success:function(html){
    // console.log(html);
    var getdata = '';
     $("#viewResponseCount").html(html.total_count);
     // $.each(data,function(key,val){
       // $("#showfilter").hide();
     getdata += ' <input type="hidden" name="" id="caseid">';
         getdata += '<input type="hidden" name="email" id="email" value="'+html.email+'">';
          getdata += '<input type="hidden" id="fname" value="'+html.name+'">';
         getdata += '<input type="hidden" id="lname" value="'+html.last_name+'">';
        getdata += '<input type="hidden" id="mobile" value="'+html.mobile+'"><input type="hidden" id="countn" value='+html.total_count+'> <table id="order-listing" class="table table-bordered">';
                  getdata += ' <thead class="bg-primary">';
                   getdata += ' <tr> <th>Sr.No. </th> <th>Type of Litigation</th> <th>Court/ Forum</th><th>Case No/ Year</th> <th>Date of Uploading</th> <th>Date of Response</th> <th>View Documents</th><th>View Response</th></tr>';
     getdata += '       </thead> <tbody>';
       var i=1;
     $.each(html.data,function(is,get){
       getdata += '<tr> <td>'+i+'</td>';
        
               getdata += '<td>'+get.type_of_litigation+'</td> ';
                
        
         getdata += '  <td>'+get.court_name+'</td>';
          getdata += '  <td>'+get.case_no+'/ ' +get.case_no_year+'</td>';
           getdata += '  <td>'+get.case_enter_date_time+'</td>';
            getdata += ' <td> </td>';
             getdata += '  <td><a href="view-case.php?caseid='+get.enc_case_id+'" class="viewDoc">View Documents</a></td>';
             
             getdata +='<td class="" data-id="caseid='+get.enc_case_id+'" style="padding: 10px;"><button class="btn btn-sm btn-info" type="button" onclick="researcherModal('+get.case_id+')"> View Response </button></td></tr>';
        i++
     });
     getdata += '</tbody></table?';
   // });
  
     $("#show_data").html(getdata);
      $("#order-listing").DataTable();
     },
     complete: function(){
      var counts = $("#countn").val();
      // showPagination(counts,10);
      $('.flip-square-loader').hide();  

    },
    error: function(e){

      $('.flip-square-loader').hide();  
      toastr.error('', 'Unable To Remove Documents', {timeOut: 5000})
    }
  }); 


}



function researcherModal(caseid){
var caseids = localStorage.getItem("legal_audit_caseid");
  var i = localStorage.getItem("legal_audit_counter");
  var dataSend = {
    caseTitleId:caseid
  }
$.ajax({
    // url: "http://192.168.0.56:8080/LegalResearch/legal_audit_get",
    // url: legalResearch+"/legal_audit_get",
    // type:'GET',
    // data: "caseTitleId="+caseid,
    // dataType: 'json',
    // async:false,
    // headers: {
    //   "Content-Type": 'application/json'
    // },
    url: "allapi.php",
    type:'POST',
    data: {params :dataSend,
flag: 'view_response'
    },
    dataType: 'json',
    async:false,
    // headers: {
    //   "Content-Type": 'application/json'
    // },
    success: function(result){
      var result = JSON.parse(result.view_response);
      if(result.error == "Data not yet to Proceed. Please wait some time"){
        toastr.warning('Data not yet to Proceed. Please wait some time', 'Sorry For Inconvenience!', {timeOut: 5000});
        /*$("#researcher").modal('hide');*/
      }else{
        if(!isEmpty(result)){
          var report = '';
          report += '<div class="heading_legal"><b>Case No: <span style="text-decoration:underline;"> '+result.rddCaseDetailsVO.caseNumber+'<span></b></div>';
          report += '<table class="table table-bordered legal_table" id="LegalResponse"><tbody>';
          report +='<tr>';
          // report +='<td class="srno">1</td>';
          report +='<td class="subhead_legal">AIR Legal No:</td>';
          report +='<td>'+result.rddCaseDetailsVO.caseNumber+' </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Department:</td>';
          report +='<td></td></tr>';

          report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Researcher No:</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Court/ Forum:</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Case Status:</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Orders Challenged:</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Subject matte:</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
           report +='<tr>';
          // report +='<td class="srno">3</td>';
          report +='<td class="subhead_legal">Category of Risk involved:</td>';
          report +='<td>1. From Client<br>2. From Auditor</td></tr>';

          report +='<tr>';
          // report +='<td class="srno">4</td>';
          report +='<td class="subhead_legal">Parties Name:</td>';
          report +='<td><b>Petitioner:</b>';
          if(!isEmpty(result.rddCaseDetailsVO.petioner)){
            $.each(result.rddCaseDetailsVO.petioner,function(i,obj){
              report +='<li>'+obj.respondent+'</li>';
            });
          }
          report +='<br><b> Respondent:</b>';
          if(!isEmpty(result.rddCaseDetailsVO)){
            $.each(result.rddCaseDetailsVO.respondent,function(i,obj){
              report +='<li>'+obj.respondent+'</li>';
            });
          }
          report +='</td></tr>';

          report +='<tr>';
          // report +='<td class="srno">5</td>';
          report +='<td class="subhead_legal">Missing Documents:(if any):</td>';
          report +='<td></td></tr>';

          report +='<tr>';
          // report +='<td class="srno">6</td>';
          report +='<td class="subhead_legal">Whether documents submitted to the Court?:</td>';
          
            report +='<td> </td></tr>';
         
         
          report +='<tr>';
          // report +='<td class="srno">9</td>';
          report +='<td class="subhead_legal">Petitioner Facts:</td>';
          report +='<td>';
          if(!isEmpty(result.petionerFactVos)){
            $.each(result.petionerFactVos,function(c,pf){
              report +='<li>'+pf.facts+'</li>';
            });
          }
          report +='</td></tr>';

          report +='<tr>';
          // report +='<td class="srno">10</td>';
          report +='<td class="subhead_legal">  Facts:</td>';
          report +='<td> </td></tr>';

            report +='<tr>';
          // report +='<td class="srno">13</td>';
          report +='<td class="subhead_legal">Issues:</td>';
          report +='<td>';
          if(!isEmpty(result.issueVos)){
            $.each(result.issueVos,function(i,obj){
              report +='<li>'+obj.issue+'</li>';
            });
          }
          report+='</td></tr>';
          // report +='<td class="srno">11</td>';
          report +='<td class="subhead_legal">Plea:</td>';
          report +='<td>';
          if(!isEmpty(result.pleaVos)){
            $.each(result.pleaVos,function(c,legal_pl){
              report +='<li>'+legal_pl.plea+'</li>';
            });
          }
          report +='</td></tr>';

          report +='<tr>';
          // report +='<td class="srno">12</td>';
          report +='<td class="subhead_legal">Counter Plea:</td>';
          report +='<td>';
          if(!isEmpty(result.counterPleaVOs)){
            $.each(result.counterPleaVOs,function(i,obj){
              report += '<li>'+obj.counterPlea+'</li>';
            });
          }
          report +='</td></tr>';

         

          report +='<tr>';
          // report +='<td class="srno">14</td>';
          report +='<td class="subhead_legal">Opinion:</td>';
          var opinion_text = '';
          if(!isEmpty(result.legalReportVO.opinionVOs)){
            $.each(result.legalReportVO.opinionVOs,function(i,obj){
              opinion_text+=obj.opinion;
            });
          }
          report +='<td>'+$(opinion_text).text()+'</td></tr>';

          report +='<tr>';
          // report +='<td class="srno">15</td>';
          report +='<td class="subhead_legal">Case referred</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">16</td>';
          report +='<td class="subhead_legal">Last word of land:</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">17</td>';
          report +='<td class="subhead_legal">Act and Rules:</td>';
          report +='<td> </td></tr>';

          report +='<tr>';
          // report +='<td class="srno">18</td>';
          report +='<td class="subhead_legal">Advocate:</td>';
          report +='<td> </td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Strength of the case:</td>';
          report +='<td></td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Special opinion for ADR or Disposed case:</td>';
          report +='<td></td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Unnecessary Delay:</td>';
          report +='<td></td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Contempt (ifany):</td>';
          report +='<td></td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Financial Repercussion:</td>';
          report +='<td></td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">First Verification:</td>';
          report +='<td></td></tr>';

           report +='<tr>';
          // report +='<td class="srno">2</td>';
          report +='<td class="subhead_legal">Second Verification:</td>';
          report +='<td></td></tr>';
          report +='<tr>';
          // report +='<td class="srno">19</td>';
          report +='<td class="subhead_legal">Final opinion:</td>';
          if(!isEmpty(result.legalReportVO.finalApproval))
            report +='<td>'+result.legalReportVO.finalApproval+'</td>';
          else
            report +='<td></td>';
          report +='</tr></tbody></table>';
          $("#legal_data").html(report);
          $("#tree-modal").modal('show');
          $(".fetchLoaderDiv_"+i).removeClass("fetchdataloader");
          $("#research_reportid_"+i).show();
        }else{
          $(".fetchLoaderDiv_"+i).removeClass("fetchdataloader");
          $("#research_reportid_"+i).show();
        }
      } 
    },
    error:function(){
      $(".fetchLoaderDiv_"+i).removeClass("fetchdataloader");
      $("#research_reportid_"+i).show();
      toastr.error("","Error in getting audit report",{timeout:5000});
    }
});
}

// function researcherModal(caseid){
//   tinymce.init({
//     selector : "#showclr_data",
//     external_plugins:{
//       'saveToPdf': 'https://cdn.rawgit.com/Api2Pdf/api2pdf.tinymce/master/save-to-pdf/dist/save-to-pdf/plugin.js'
//     },
//     plugins : [
//     "print wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
//     "  directionality textcolor paste fullpage textcolor colorpicker " ],
//         //removed textpattern from plugin because of getting bullets on enter key press in judgment
//         toolbar1 : "print saveToPdf bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | fullscreen | hr removeformat",
//         menubar : false,
//         contextmenu_never_use_native : true,
//         toolbar_items_size : 'small',
//         branding : false,
//         height : 200
//       });
//   var caseid= caseid;
//   $.ajax({
//     // url: "http://59.97.236.86:7070/LegalResearch/getprocessdata",
//     url: legalResearch+"/getprocessdata",
//     type:'GET',
//     data: "casetitleid=169",
//     dataType: 'json',
//     async:false,
//     headers: {
//       "Content-Type": 'application/json'
//     },
//     success: function(result){
//       if(result.error == "Data Processing is proceed"){
//        $.each(result.documentTypeVOs,function(i,obj){
//          $.each(obj.legalAuditVO,function(j,aud){
//            // $("#showclr_data").html(aud.legalAuditReport);
//             tinymce.get("showclr_data").setContent(aud.legalAuditReport);
//           });
//        });
//        $("#researcher").modal('show');

//       }else{
//         toastr.warning('Data not yet to Proceed. Please wait some time', 'Sorry For Inconvenience!', {timeOut: 5000});
//         $("#researcher").modal('hide');
//       }
//       console.log(result);
//     }
//   });
// }

$(".rowclickable").click(function(){
  var id = $(this).attr('data-id');
  location.href = "view-case.php?"+id;
});


function viewAnswer(){
$("#view-answer").modal('show');
}



