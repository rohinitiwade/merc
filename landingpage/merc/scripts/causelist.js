 $("#cases-court_id").select2();
 $("#cases-high_court_ids").select2();
 $("#causeListDate").datepicker({
 	dateFormat : 'dd-mm-yy',
   changeYear: true,
   changeMonth: true
 });
 $("#comingsoon").hide();
// $("#datepicker-causelist").hide();
$("#cases-court_id").on('change',function(){
  var val = $(this).val();
  if(val == '1'){
    $("#highcourt").hide();
    $("#comingsoon").hide();
    $("#advocate-btn").show();
  }
  else if(val == '2'){
    $("#highcourt").show();
    $("#comingsoon").hide();
    $("#advocate-btn").show();
  }
  else{
    $("#highcourt").hide();
    $("#comingsoon").show();
    $("#advocate-btn").hide();
  }
$("#saveandcancel").show();
});

$("#cases-high_court_ids").on('change',function(){
  var val = $(this).val();

  if(val =='3'){
    $.ajax({
      type:'POST',
      url:'highcourtlist.php',
      data:'high_court='+val,
      success:function(response){
        console.log(response);          
        var response = JSON.parse(response);
        if(response.length > 0){
          var option = '<option value="">Select</option>';
          $.each(response,function(i,obj){
            option += '<option value='+obj.bench_id+'>'+obj.bench_name+'</option>';
          });                       
          $("#high_court_idslist").html(option);
          $("#highcourt_casetype").show();
          $("#high_court_idslist").select2();
        }
      },
      error:function(){
        toastr.error("","Unable to load Bench",{timeout:5000});
      }
    });
  }

});

submitCauseList('u');
function submitCauseList(type){
  var causelistVo = new Object();
  causelistVo.court = $("#cases-court_id").val();
  if(causelistVo.court == '2'){
    causelistVo.court_id = $("#cases-high_court_ids").val();
    causelistVo.court_name = $("#cases-high_court_ids option:selected").text();
    causelistVo.bench = $("#high_court_idslist option:selected").text();
  }

  causelistVo.advocate_name = $("#advocate_name").val();

  if($("#cases-court_id").val()==2 && $("#cases-high_court_ids").val() ==''){
   toastr.warning("Please Select High Court",{timeout:5000});
 }else{
   $.ajax({
    url: host+'/submit_cause_list.php',
    type:'POST',
    data : JSON.stringify(causelistVo),
    dataType : 'json',
    contentType : 'application/json;charset=utf-8',	
    async:false,
    cache : false,
    success:function(data){
     console.log("data",data);
     var tr='';
     if(type !='u'){
      toastr.success("","Successfully submitted cause list",{timeout:5000});
    }
    if(data.status == 'success'){ 					
      $.each(data.causelist,function(i,obj){
       tr+='<tr>';
       tr+='<td>'+obj.advocate_name+'</td>';
       tr+='<td>'+obj.court+'</td>';
       tr+='<td>'+obj.court_name+'</td>';
       tr+='<td>'+obj.bench+'</td>';
       tr+='<td>';
       tr+='<div id="loader-causelist_'+i+'"></div><button class="btn btn-info btn-sm loader-causelist-class_'+i+'" type="button" title="Download Cause List" id="'+obj.advocate_name+'_'+obj.bench+'" onclick=getCauseList(this.id,'+i+',"'+obj.court_id+'")>';
       tr+='<i class="feather icon-download"></i></button> <button onclick="deleteAdvocate()" class="btn btn-danger btn-sm" type="button" title="Delete">';
       tr+='<i class="feather icon-trash" ></i></button></td></tr>';
     });

      if(tr!=''){
       var table = '<table class="table table-bordered" id="cause-list-table"><thead class="bg-primary"><tr>';
       table +='<th>Advocate Name</th>';
       table +='<th>Court</th>';
       table +='<th>High Court</th>';
       table +='<th>Bench</th>';
       table +='<th>Action</th></tr></thead><tbody>';
       table += tr +'</tbody></table>';
     }
     $("#causelit-div").html(table);
     $("#cause-list-table").dataTable();
   }
 },
 error:function(e){
   toastr.success("","Error in submitting cause list",{timeout:5000});
 }
});
 }
}

function getCauseList(adv_bench,counter,court_id){
 	// $("#datepicker-causelist").show();
  var url = '';
  var data = adv_bench.split("_");
  var causelistVO = new Object();
  causelistVO.advocates = data[0];
  if(court_id == '2')    {
    causelistVO.bench = data[1];
    url = legalResearch+'/bombay_causelist';
  }
  else if(court_id == '1')
    url = legalResearch+'/sc_causelist';
 	// causelistVO.advocates = 'Dinesh';
 	// causelistVO.bench = 'Nagpur';
 /*	var date = new Date();
 	var month = date.getMonth()+1;
 	causelistVO.causeListDate = date.getDate()+'-'+ month +'-'+date.getFullYear();*/
  if(!isEmpty($("#causeListDate").val()))
    causelistVO.causeListDate = $("#causeListDate").val();
  else{
    toastr.error("","Please select date",{timeout:5000}); 
    return false;
  }
  $("#loader-causelist_"+counter).addClass('fetchdataloader');
  $(".loader-causelist-class_"+counter).hide();
 	// causelistVO.causeListDate = '18-12-2019';
 	$.ajax({
 		url: url,
 		type:'POST',
 		data :  JSON.stringify(causelistVO),
 		contentType : 'application/json;utf-8',
 		dataType: 'json',
 		async:false,
 		cache : false,
 		beforeSend : function(){
       $(".flip-square-loader").show(); 
     },
     success:function(data){
      console.log('bombay_causelist',data);
      if(court_id == '1'){
        $.ajax({
          url: 'getScCause.php',
          type:'POST',
          data :  data,
          // contentType : 'application/json;utf-8',
          dataType: 'json',
          async:false,
          cache : false,
          success:function(data){
          }
        });
        var table = data.mainCause + data.supplimentryCause;
      }
      else if(court_id == '2'){
        var table = '<table class="main-table"><thead ><tr><th>Main Cause List</th></tr></thead>'+data.mainCause+'</table>';
        table+='<table class="main-table"><thead><tr><th>Supplimentry Cause List</th></tr></thead>'+data.supplimentryCause+'</table>';
      }
      $("#causelist-download").html(table);
            // if(court_id == '1'){

            //     var main_cause_table = $(".mob tbody").length();
            //     $.each(main_cause_table,function(i,obj){
            //         console.log("Supreme court tbody",obj);
            //     });
            // }
            var doc = new jsPDF('portrait','pt', 'a4');

 			// var elem = document.getElementsByClassName('main-table');
 			var elem = $(".main-table");
 			$.each(elem,function(i,obj){
 				var data = doc.autoTableHtmlToJson(obj);
 				doc.autoTable(data.columns, data.rows, {
 					startY: doc.autoTableEndPosY()+20,
 					pageBreak: 'auto',
 					// theme:'plain',
 					"styles": { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "halign": "justify", "fontSize": "10" },
 					drawHeaderRow: function(row, data) {
 						if (data.pageCount > 1) {
 							return false;
 						}
 					}
 				});
 			});

 			setTimeout(function() {
 				doc.save(causelistVO.advocates+'-'+causelistVO.bench+'-daily-causelist-'+causelistVO.causeListDate+'.pdf');
 			}, 1000);
      $("#loader-causelist_"+counter).removeClass('fetchdataloader');
      $(".loader-causelist-class_"+counter).show();
    },
    complete : function(){
      $("#loader-causelist_"+counter).removeClass('fetchdataloader');
      $(".loader-causelist-class_"+counter).show();
    },
    error:function(e){
     $("#loader-causelist_"+counter).removeClass('fetchdataloader');
     $(".loader-causelist-class_"+counter).show();
     toastr.success("","Error in downloading cause list",{timeout:5000});
   }
 });
 }

 function deleteAdvocate(){
 	$.ajax({
 		url: host+'/ajaxdata/delete_causelist.php',
 		type:'POST',
 		data :  JSON.stringify(causelistVo),
 		contentType : 'application/json;utf-8',
 		dataType: 'json',
 		async:false,
 		cache : false,
 		success:function(data){
 			console.log(data);
 			submitCauseList('u');
 		},
 		error:function(e){
 			toastr.success("","Error in submitting cause list",{timeout:5000});
 		}
 	});
 }

 function getAdvocateName(){
 	var causelistVo = new Object();
 	causelistVo.court = $("#cases-court_id").val();
  if(causelistVo.court == '2'){
    causelistVo.court_name = $("#cases-high_court_ids").val();
    causelistVo.bench = $("#high_court_idslist option:selected").text();
  }
  causelistVo.search = $("#advocate_name").val();
  $.ajax({
   url: host+'/ajaxdata/adv_causelist.php',
   type:'POST',
   data :  JSON.stringify(causelistVo),
   contentType : 'application/json;utf-8',
   dataType: 'json',
   async:false,
   cache : false,
   success:function(data){
    console.log(data);
    var option = '';
    $.each(data,function(i,obj){
     option+='<li id="'+obj.adv_name+'" onclick=selectAdvName(this.id)>'+obj.adv_name+'</li>';
   });
    $("#suggesstion-adv").show();
    $("#suggesstion-adv").html(option);
  },
  error:function(e){
 			// toastr.success("","Error in submitting cause list",{timeout:5000});
 		}
 	});
}

function selectAdvName(advName){
  $("#advocate_name").val(advName);
  $("#suggesstion-adv").hide();
}

$(document).click(function(){
  $("#suggesstion-adv").hide();
});