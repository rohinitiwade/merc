$("#tree-description").hide();
 var dataSend = {
    organizationId:11
}
 var orgid = $("#oragnisation_id").val();
getAllCategory('all',orgid,5);

$('#lazy').on('select_node.jstree', function (e, data) {
    var loMainSelected = data;
    uiGetParents(loMainSelected);
});

function uiGetParents(loSelectedNode) {
    try {
        var lnLevel = loSelectedNode.node.parents.length;
        var lsSelectedID = loSelectedNode.node.id;
        var loParent = $("#" + lsSelectedID);
        var ls =  loSelectedNode.node.text;
        var lsParents =[];
        for (var ln = 0; ln <= lnLevel -1 ; ln++) {
            var loParent = loParent.parent().parent();
            if (loParent.children()[1] != undefined) {
                lsParents.push(loParent.children()[1].text);
            }
        }
        /*if (ls.length > 0) {
            ls = ls.substring(0, ls.length - 1);
        }*/
        // alert(lsParents);
        // alert(ls);
    }
    catch (err) {
        alert('Error in uiGetParents');
    }
    var data = {};
    if(lsParents.length == 0){
        topic = ls;
        data = {
            topic : topic,
            subTopic : '',
            subsubTopic : ''
        }
    }
    else if(lsParents.length == 1){
        topic = lsParents[0];
        data = {
            topic : topic,
            subTopic : ls,
            subsubTopic : ''
        }
    }
    else if(lsParents.length == 2){
        topic = lsParents[1];
        subTopic=lsParents[0];
        data = {
            topic : topic,
            subTopic : subTopic,
            subsubTopic : ls
        }
    }

    $.ajax({
        url : legalResearch+"/search_by_topic",
        data : data,
        success:function(response){
            $("#tree-description").show();
            var tr='<table class="table table-bordered" id="tree-report-table"><thead><tr><th>AIR Legal No.</th><th>Case No</th><th>Legal Report</th></tr></thead><tbody>';
            $.each(response,function(i,obj){
                i++;
                tr+='<tr>';
                tr+='<td>'+obj.casetitleId+'</td>';
                tr+='<td>'+obj.aType+' '+obj.aNo+' of '+obj.aYear+'</td>';
                // tr+='<td>'+obj.caseTitle+'</td>';
                tr+='<td><button class="btn btn-info btn-mini" type="button" onclick="getLegalReport('+obj.casetitleId+')">Legal Report</button></td></tr>';
            });         
            tr+='</tbody></table>';
            $("#tree-description").html(tr);
            $("#tree-report-table").DataTable();
        },
        error:function(e){

        }
    });

}


function byRiskCategory(oragnisation_id,category){

   $.ajax({
        url : "allapi.php?flag=topic&organizationId="+oragnisation_id+"&task="+category ,
        success:function(response){
           var datas = JSON.parse(response);
           getcourt(datas,category);
        },
        complete: function(){
            
            },
            error:function(e){

            }
        });

    }

function getcourt(datas,category){

    var jstree_id,jstree_table;
    if(category == 1){
         jstree_id = '#risk-category';
         jstree_table = '#risktree-description';
         jstree_datatable = 'riskcategory_datatable';
         pagination = '#pagination_risk';
    }else if(category == 2){
        jstree_id = '#court-category';
         jstree_table = '#courttree-description';
          jstree_datatable = 'courtcategory_datatable';
          pagination = '#pagination_court';
    }
    // else if(category == 3){
    //     jstree_id = '#topic-category';
    //      jstree_table = '#topictree-description';
    //       jstree_datatable = 'topiccategory_datatable';
    //       pagination = '#pagination_topic';
    // }
    else if(category == 3){
        jstree_id = '#adr-category';
         jstree_table = '#adrtree-description';
          jstree_datatable = 'adrcategory_datatable';
          pagination = '#pagination_adr';
    }
    else if(category == 4){
        jstree_id = '#contempt-category';
         jstree_table = '#contempttree-description';
          jstree_datatable = 'ccontemptcategory_datatable';
          pagination = '#pagination_contempt';
    }
    $(jstree_id).jstree({
         'core': {
       'data': datas
     }

    
});
    $(jstree_id).on('select_node.jstree', function (e, data) {
    // var datas = 'Civil Matters';
    var org_id = $("#oragnisation_id").val();
    var loMainSelected = data;
    uiGetRisk(loMainSelected,org_id,jstree_table,jstree_datatable,pagination,category);
    
});
}




function uiGetRisk(loSelectedNode,org_id,jstree_table,jstree_datatable,pagination,category) { 
    var ls =  loSelectedNode.node.text;
    var risk;
    if(category == 3 || category == 4){
          risk = ls.match(/\(([^)]+)\)/)[1] 
    }else{
      risk = ls.replace(/ *\([^)]*\) */g, ""); 
    }
    // }

    getListCategory(risk,org_id,1,jstree_table,jstree_datatable,pagination,category);


}

function getAllCategory(risk,org_id,category){
     // jstree_id = '#contempt-category';
         jstree_table = '#alltree-description';
          jstree_datatable = 'allcategory_datatable';
          pagination = '#pagination_all';

getListCategory(risk,org_id,1,jstree_table,jstree_datatable,pagination,category);
}

function getListCategory(risk,org_id,currentpage,jstree_table,jstree_datatable,pagination,category,page){
    var data = {};  
    var limit = 10; 
    data = {
        text : risk,
        organizationId : org_id,
        limit : limit,
        offset : offset(currentpage,limit),
        task : category
    }
    $.ajax({
        url : "allapi.php",
        method : 'POST',
        data : {params :data,
            flag: 'risk_tree'
        },
        success:function(response){
            var response = JSON.parse(response);
            $(jstree_table).show();
            var tr='<input type="hidden" name="total_count" id="total_counts" value="'+response.total_count+'"><table class="table table-bordered" id="'+jstree_datatable+'"><thead><tr><th>AIR Legal No.</th><th>Case No</th><th>Legal Report</th></tr></thead><tbody>';
            $.each(response.list,function(i,obj){
                // var tot_count = response.length;
                i++;
               
                tr+='<tr>';
                tr+='<td>'+obj.case_title_id+'</td>';
                tr+='<td>'+obj.case_number+' of '+obj.case_year+'</td>';
                // tr+='<td>'+obj.caseTitle+'</td>';
                tr+='<td><button class="btn btn-info btn-mini" type="button" onclick="getLegalReport('+obj.case_title_id+')">Legal Report</button></td></tr>';
            });         
            tr+='</tbody></table>';
            $(jstree_table).html(tr);
            $('#'+jstree_datatable).DataTable();
        },
        complete: function(){
            var counts = $("#total_counts").val();
            if(page !='page'){
                showPagination(counts,limit,risk,org_id,jstree_table,jstree_datatable,category,pagination);}
            },
            error:function(e){

            }
        });
}
function getLegalReport(caseid){
    $.ajax({
        url: "encryted_caseid.php",
    // url:'',
    type:'POST',
    data: "caseid="+caseid,
    dataType: 'json',
    async:false,
    success: function(result){
        window.open('viewLegalAudit.php?caseid='+result.data);
    }
});
}



function offset(page,limit){
  if(page <= 1)
    return 0;
else
    return ((page-1)*limit);
}

var currentPage;
function showPagination(counts,limit,risk,org_id,jstree_table,jstree_datatable,category,pagination) {
    // Number of items and limits the number of items per page
    var numberOfItems = counts;
    var limitPerPage =limit;
    // Total pages rounded upwards
    var totalPages = Math.ceil(numberOfItems / limitPerPage);
    var paginationSize = 7; 
    function showPage(whichPage) {
      if (whichPage < 1 || whichPage > totalPages) return false;
      currentPage = whichPage;                   
      $(pagination+" li").slice(1, -1).remove();
      getPageList(totalPages, currentPage, paginationSize).forEach( item => {
        $("<li>").addClass("page-item")
        .addClass(item ? "current-page" : "disabled")
        .toggleClass("active", item === currentPage).append(
          $("<a>").addClass("page-link").attr({
            href: "javascript:void(0)"}).text(item || "...")
          ).insertBefore("#next-page");
    });
        // Disable prev/next when at first/last page:
        $("#previous-page").toggleClass("disabled", currentPage === 1);
        $("#next-page").toggleClass("disabled", currentPage === totalPages);
        return true;
    }
    $(pagination).append(
      $("<li>").addClass("page-item").attr({ id: "previous-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Prev")
        ),
      $("<li>").addClass("page-item").attr({ id: "next-page" }).append(
        $("<a>").addClass("page-link").attr({
          href: "javascript:void(0)"}).text("Next")
        )
      );
    showPage(1); 
    // Use event delegation, as these items are recreated later    
    $(document).on("click", pagination+" li.current-page:not(.active)", function () {
      var s=showPage(+$(this).text());
      var page = 'page';
      getListCategory(risk,org_id,+$(this).text(),jstree_table,jstree_datatable,pagination,category,page);
      return s;
  });
    $("#next-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage+1);
        getListCategory(risk,org_id,currentPage+1,jstree_table,jstree_datatable,pagination,category,page);
        return s;
    }
});

    $("#previous-page").on("click", function () {
      if(!$(this).hasClass('disabled')){
        var s=showPage(currentPage-1);
        getListCategory(risk,org_id,currentPage-1,jstree_table,jstree_datatable,pagination,category,page);
        return s ;
    }
});

}


function getPageList(totalPages, page, maxLength) {
  if (maxLength < 5) throw "maxLength must be at least 5";

  function range(start, end) {
    return Array.from(Array(end - start + 1), (_, i) => i + start); 
}

var sideWidth = maxLength < 9 ? 1 : 2;
var leftWidth = (maxLength - sideWidth*2 - 3) >> 1;
var rightWidth = (maxLength - sideWidth*2 - 2) >> 1;
if (totalPages <= maxLength) {
        // no breaks in list
        return range(1, totalPages);
    }
    if (page <= maxLength - sideWidth - 1 - rightWidth) {
        // no break on left of page
        return range(1, maxLength-sideWidth-1)
        .concat([0])
        .concat(range(totalPages-sideWidth+1, totalPages));
    }
    if (page >= totalPages - sideWidth - 1 - rightWidth) {
        // no break on right of page
        return range(1, sideWidth)
        .concat([0])
        .concat(range(totalPages - sideWidth - 1 - rightWidth - leftWidth, totalPages));
    }
    // Breaks on both sides
    return range(1, sideWidth)
    .concat([0])
    .concat(range(page - leftWidth, page + rightWidth)) 
    .concat([0])
    .concat(range(totalPages-sideWidth+1, totalPages));
}
