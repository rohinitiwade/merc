changeLanguage('English');
function changeLanguage(language){
  tinymce.init({
    selector : "#query",
    plugins : [
    "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });

  if(language == 'Marathi'){
    google.setOnLoadCallback(OnLoad);
    OnLoad();
  }
}
$(document).ready(function(){
// debugger;
getTeamMemberListDropdown();
  function getTeamMemberListDropdown(){
// var hidValue = $("#hidSelectedOptions").val();
var selectedOptions = [];
var caseid = $("#caseid").val();
$.ajax({
  type:'POST',
  url:host+'/nonjud_team.php',
  data:"caseid="+caseid+"&&flag=edit",
  dataType:"json",
  success:function(response){
    // console.log(response);
// var response = JSON.parse(response);
if(response.team.length > 0){
  var option = '<option value="">Select</option>';
  $.each(response.team,function(i,obj){
    option += '<option value='+obj.reg_id+'>'+obj.name+' '+obj.last_name+'</option>';
  });
  $('#framework').html(option);
  $('#framework').select2({
    placeholder: 'Select...',
    closeOnSelect: false,
    multiple: true,
    allowClear: false
  });
}
$.each(response.show_team,function(i,obj){
  selectedOptions.push(obj.reg_id);
});
$('#framework').val(selectedOptions).trigger('change');
},
error:function(){
  toastr.error("","Error fetching team list",{timeout:5000});
}
});
}

$.each($("#framework option"), function (i, item) {
  if (item.selected) {
    /*$(item).prop("disabled", true); */
    $(".select2-selection__choice__remove").hide();
  }else {
    /*$(item).prop("disabled", false);*/
    $(".select2-selection__choice__remove").show();
  }
});
});

function updateNonjudicial(){
  // debugger;
  var client_name = $("#client_name").val();
  var caseid = $("#caseid").val();
        var type_ofwork = $("#type_ofwork").val();
          // var your_team = $("#framework").val();
          var shortdescription = tinyMCE.get('query').getContent();
var assign_to = $("#framework").val();
  var assign_to_array = [];
  $.each(assign_to,function(a,obj){
   assign_to_array.push(obj);
 });
  // var pet_file='';
  var formData = new FormData();
  // var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','xlsx','xls','doc','docx'];
  // if(!isEmpty($("#upload").val())){
  //   // if ($.inArray($("#pet_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
  //   //  toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
  //   //  return false;
  //   // }
  //   upload = document.getElementById("upload").files[0];
  // }
  // else
  //   upload = '';

    

 // formData.append("upload",upload);
 formData.append("client_name",client_name);
  formData.append("caseid",caseid);
 formData.append("type_ofwork",type_ofwork);
 formData.append("description",shortdescription);
 formData.append("assign_to_array",assign_to_array);
 // formData.append("flag",'edit');

 var ajaxResult = $.ajax({
   url : host+"/edit_nonjudicial.php",
   type : 'POST',
   data : formData,
   cache : false,
   contentType : false,
   processData : false,
   success:function(response){
    var responses = JSON.parse(response);
    // console.log(response);
    window.location.href='view-nonjudicialcase.php?caseid='+responses.caseid;
    
 },
 error:function(){
  toastr.error("","Error in adding case",{timeout:5000});
}
});
}

