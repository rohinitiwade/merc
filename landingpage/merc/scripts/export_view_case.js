 $("#export-to-pdf").on('click',function(i,obj){
  var val = $(this).val();
  if(val == 'exprt')
    buildExportReport();

   // $("#case_type_export_report").text(obj.case_type +' '+obj.case_no+' / '+obj.case_no_year)
  /*var pdfTitle = $("#case_type_export_report").text();
  html2canvas($('#complete_view_data')[0], {
  onrendered: function (canvas) {
  var data = canvas.toDataURL();
  var docDefinition = {
  content: [{
  image: data,
  width: 500,
  }]
  };
  pdfMake.createPdf(docDefinition).download(pdfTitle);
  }
});*/
var pdfTitle = $("#case_type_export_report").text();
var doc = new jsPDF('portrait','pt', 'a4');

var elem = document.getElementsByClassName('main-table');
$.each(elem,function(i,obj){
  var data = doc.autoTableHtmlToJson(obj);
  doc.autoTable(data.columns, data.rows, {
    startY: doc.autoTableEndPosY()+20,
    pageBreak: 'auto',
    "styles": { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "halign": "justify", "fontSize": "10", "lineColor": "100", "lineWidth": ".25" },
    drawHeaderRow: function(row, data) {
      if (data.pageCount > 1) {
        return false;
      }

    }
  });
  // doc.addPage();
});

/*var source = document.getElementById("activity_div_export");
    doc.fromHTML(source, 5, 5, {
        'width': 660,        
      });*/
/*var res = doc.autoTableHtmlToJson(document.getElementById('main-table'),true);
doc.autoTable(res.columns, res.data ,{startX: doc.autoTableEndPosY() + 50,
pageBreak: 'auto'});
var res1 = doc.autoTableHtmlToJson(document.getElementById('main-table1'),true);
// doc.addPage();
doc.autoTable(res1.columns, res1.data,{startY: doc.autoTableEndPosY() + 50,
  pageBreak: 'auto'});*/

  setTimeout(function() {
    doc.save(pdfTitle+'.pdf');
  }, 1000);

});


 function buildExportReport(){ 
  caseid = $("#caseid").val();
  $.ajax({
    url: host+'/view_caseurl.php',
    type:'POST',
    data :"case_id="+caseid+"&autoupdate="+autoupdate,
    dataType: 'json',
    async:false,
    cache : false,
    /*beforeSend : function(){
      $(".flip-square-loader").show();
    },*/
    success:function(response){
      if(response.status == 'success'){
        console.log("view",response);
        var export_data_format =''; 
        var case_sub = $("#sub_case_title").text();
         // var case_desc = $("#sub_case_desc").text();
        $.each(response.data,function(i,obj){

          court_id_val = obj.court_id;
          var case_desc = isEmpty(obj.case_description) ? "" : obj.case_description;
          var case_type = isEmpty(obj.case_type) ? "" : obj.case_type;
          var appearing_as = isEmpty(obj.are_you_appearing_as) ? "" : obj.are_you_appearing_as;
          var court_name = isEmpty(obj.court_name) ? "" : obj.court_name;
          var date_of_filling = isEmpty(obj.date_of_filling) ? "": obj.date_of_filling;
          // var is_affidavit_filed = isEmpty(obj.is_the_affidavit_vakalath_filed) ? "" : obj.is_the_affidavit_vakalath_filed;
          // var is_vakalat_filed = isEmpty(obj.is_vakalat_filed) ? "" : obj.is_vakalat_filed;
          var high_court = isEmpty(obj.high_court) ? "" : obj.high_court;
          var supreme_court = isEmpty(obj.supreme_court) ? "" : obj.supreme_court;
          var bench = isEmpty(obj.bench) ? "" : obj.bench;
          var side = isEmpty(obj.side) ? "" : obj.side;
          var stamp = isEmpty(obj.hc_stamp_register) ? "" : obj.hc_stamp_register;
          var state_name = isEmpty(obj.state) ? "" : obj.state;
          var dept = obj.name_of_matter.split("/");
          var department = isEmpty(obj.name_of_matter) ? "" : dept[0];
          var court_establishment= isEmpty(obj.court_establishment) ? "" : obj.court_establishment;
          var district_name= isEmpty(obj.district) ? "" : obj.district;
          var appearing_modal= isEmpty(response.appearing_modal) ? "" : response.appearing_modal;
          var petitioner= isEmpty(obj.petitioner) ? "" : obj.petitioner;
          // var judge= isEmpty(obj.judge) ? "" : obj.judge;
          var reffered_by= isEmpty(obj.reffered_by) ? "" : obj.reffered_by;
          // var affidavite_filling_date = isEmpty(obj.affidavite_filling_date) ? "" : obj.affidavite_filling_date;
          // var vakalat_filling_date = isEmpty(obj.vakalat_filling_date) ? "" : obj.vakalat_filling_date;
          var sc_case_type= isEmpty(obj.sc_case_type) ? "" : obj.sc_case_type;
          var classification= isEmpty(obj.classification) ? "" : obj.classification;
          case_no = isEmpty(obj.case_no) ? "" : obj.case_no;
          var case_no_year= isEmpty(obj.case_no_year) ? "" : obj.case_no_year;
          // var court_hall= isEmpty(obj.court_hall) ? "" : obj.court_hall;
          // var floor= isEmpty(obj.floor) ? "" : obj.floor;
          var priority= isEmpty(obj.priority) ? "" : obj.priority;
          var section_category= isEmpty(obj.section_category) ? "" : obj.section_category;
          var cnr_case_details = isEmpty(obj.CNR) ? "" : obj.CNR;

          $("#case_type_export_report").text(obj.case_type +' '+obj.case_no+' / '+obj.case_no_year)
          export_data_format += '<table class="table table-bordered main-table col-sm-12" id="main-table" style="width:100%">';
          export_data_format +='<thead><tr><th></th><th>'+obj.case_type +' '+obj.case_no+' / '+obj.case_no_year+'</th></tr></thead>';
          if(!isEmpty(obj.nominal))
            export_data_format +='<tr><td>Title</td><td>'+obj.nominal+'</td></tr>';
          else
            export_data_format +='<tr><td>Title</td><td>'+case_sub+'</td></tr>';
          export_data_format +='<tr><td>Description</td><td>'+case_desc+'</td></tr>';
          // export_data_format += '<tr><td>File</td><td>'+obj.nastikramank+'</td></tr>';
          export_data_format += '<tr><td class="c-type">Court</th>';
          export_data_format += '<td class="c-desc">'+ court_name +'</td></tr>';
          if(court_id_val == 2 || court_id_val == 5){
            export_data_format += '<tr><td class="c-type">'+court_name+'</td>';
            export_data_format += '<td class="c-desc">'+high_court+'</td></tr>';
          }
          if(court_id_val == 1){
            export_data_format += '<tr><td class="c-type">'+court_name+'</td>';
            export_data_format += '<td class="c-desc">'+supreme_court+'</td></tr>';
          }

          if(court_id_val == 2){
            export_data_format += '<tr><td class="c-type">Bench</td>';
            export_data_format += '<td class="c-desc">'+bench+'</td></tr>';
            export_data_format += '<tr><td class="c-type">Side</td>';
            export_data_format += '<td class="c-desc">'+side+'</td></tr>';
            export_data_format += '<tr><td class="c-type">Stamp/Register</td>';
            export_data_format += '<td class="c-desc">'+stamp+'</td></tr>';
          }

          if(court_id_val == 5){
            export_data_format += '<tr><td class="c-type">State</td>';
            export_data_format += '<td class="c-desc">'+state_name+'</td></tr>';
            export_data_format += '<tr><td class="c-type">Bench</td>';
            export_data_format += '<td class="c-desc">'+bench+'</td></tr>';
          }

          if(court_id_val == 3){
            export_data_format += '<tr><td class="c-type">State</td>';
            export_data_format += '<td class="c-desc">'+state_name+'</td></tr>';
            export_data_format += '<tr><td class="c-type">District</td>';
            export_data_format += '<td class="c-desc">'+district_name+'</td></tr>';
            export_data_format += '<tr><td class="c-type">Court Establishment</td>';
            export_data_format += '<td class="c-desc">'+court_establishment+'</td></tr>';
            export_data_format += '<tr><td class="c-type">CNR</td>';
            export_data_format += '<td class="c-desc">'+cnr_case_details+'</td></tr>';
          }
          if(court_id_val == 1 || court_id_val == 3 || court_id_val == 5){
            export_data_format += '<tr><td class="c-type">Appearing Model</td>';
            export_data_format += '<td class="c-desc">'+appearing_modal+'</td></tr>';
          }

          export_data_format += '<tr><td class="c-type">You are appearing as</td>';
          export_data_format += '<td class="c-desc">'+ appearing_as +'</td></tr>';

        // if(court_id_val == 5 && court_id_val == 1){
          if(!isEmpty(petitioner)){
            export_data_format += '<tr><td class="c-type">'+appearing_as+'</td>';
            export_data_format += '<td class="c-desc">'+petitioner+'</td></tr>';
          }
        // }
        /*if(){
          courtdata += '<tr><td class="c-type">'+appearing_as+'</td>';
          courtdata += '<td class="c-desc">'+petitioner+'</td></tr>';
        }*/
        // if(!isEmpty(judge)){
        //   export_data_format += '<tr><td class="c-type">Judges</td>';
        //   export_data_format += '<td class="c-desc">'+judge+'</td></tr>';
        // }
        // courtdata += '<tr><td class="c-type">Referred By/ यांच्या संदर्भाने</td>';
        // courtdata += '<td class="c-desc">'+reffered_by+'</td></tr>';


        // export_data_format += '<tr><td class="c-type">Is the affidavit filed?:</th>';
        // export_data_format += '<td class="c-desc">'+is_affidavit_filed+'</td></tr>';

       //  if(court_id_val == 1 || court_id_val == 2){
       //   export_data_format += '<tr><td class="c-type">Affidavit Filling Date:</td>';
       //   export_data_format += '<td class="c-desc">'+affidavite_filling_date+'</td>';
       // }
       // export_data_format += '<tr><td class="c-type">Is the vakalathnama filed?:</th>';
       // export_data_format += '<td class="c-desc">'+is_vakalat_filed+'</td></tr>';
       // if(!isEmpty(vakalat_filling_date)){
       //   export_data_format += '<tr><td class="c-type">Vakalathnama Filling Date:</th>';
       //   export_data_format += '<td class="c-desc">'+vakalat_filling_date+'</td></tr>';
       // }

       // export_data_format += '<tr><td class="c-type">Department:</th>';
       // export_data_format += '<td class="c-desc">'+department+'</td>';
       // if(!isEmpty(obj.ministry_desk_no)){
       //   export_data_format += '<tr><td class="c-type">Ministry Desk Number :</th>';
       //   export_data_format += '<td class="c-desc">'+obj.ministry_desk_no+'</td>';
       // }
       // if(!isEmpty(obj.div_comm_level)){
       //   export_data_format += '<tr><td class="c-type">Divisional Commissioner Level :</th>';
       //   export_data_format += '<td class="c-desc">'+obj.div_comm_level+'</td>';
       // }
       if(!isEmpty(obj.priority)){
         export_data_format += '<tr><td class="c-type">Priority:</th>';
         export_data_format += '<td class="c-desc">'+obj.priority+'</td></tr>';
       }
       /* $(".court_data").html(courtdata); */     

       // courtdata = '<table class="case-sidebar-section"><thead style="display:none"><tr><th>Title</th><th>'+obj.case_title+'</th></tr></thead><tbody><tr>';

       if(court_id_val == 1 || court_id_val == 2){
         export_data_format += '<tr><td class="c-type">Subject</td>';
         export_data_format += '<td class="c-desc">'+obj.classification+'</td></tr>';
       }
       export_data_format += '<tr><td class="c-type">Case Number</td>';
       export_data_format += '<td class="c-desc">'+obj.case_no+'</td></tr>';
       export_data_format += '<tr><td class="c-type">Year</td>';
       export_data_format += '<td class="c-desc">'+obj.case_no_year+'</td></tr>';
       export_data_format += '<tr><td class="c-type">Date of filing</td>';
       export_data_format += '<td class="c-desc">'+date_of_filling+'</td></tr>';
      //  if(court_id_val == 1){
      //    export_data_format += '<tr><td class="c-type">Court Hall #</td>';
      //    export_data_format += '<td class="c-desc">'+obj.court_hall+'</td></tr>';
      //    export_data_format += '<tr><td class="c-type">Floor #</td>';
      //    export_data_format += '<td class="c-desc">'+obj.floor+'</td></tr>';
      //   // courtdata += '<tr><td class="c-type">Priority</td>';
      //   // courtdata += '<td class="c-desc">'+obj.priority+'</td></tr>';
      //   // });
      // }
    });

 var next_hearing_date = isEmpty(response.stage.next_hearing_date) ? "" : response.stage.next_hearing_date;
 var stage = isEmpty(response.stage.stage) ? "" : response.stage.stage;
 var posted_for = isEmpty(response.stage.posted_for) ? "" : response.stage.posted_for;
 var action_taken = isEmpty(response.stage.action_taken) ? "" : response.stage.action_taken;
 var session_phase = isEmpty(response.stage.session_phase) ? "" : response.stage.session_phase;

 if(!isEmpty(stage)){
   export_data_format+='<tr>';
   export_data_format += '<td class="c-type">Stage:</td>';
   export_data_format += '<td class="c-desc">'+stage+'</td>';
   export_data_format += '</tr>';
 }
 if(!isEmpty(posted_for)){
   export_data_format += '<tr>';
   export_data_format += '<td class="c-type">Posted For:</td>';
   export_data_format += '<td class="c-desc">'+posted_for+'</td>';
   export_data_format += '</tr>';
 }
 if(!isEmpty(action_taken)){
   export_data_format += '<tr>';
   export_data_format += '<td class="c-type">Last Action Taken:</td>';
   export_data_format += '<td class="c-desc">'+action_taken+'</td>';
   export_data_format += '</tr>';
 }
 if(!isEmpty(next_hearing_date)){
   export_data_format += '<tr>';
   export_data_format += '<td class="c-type">Hearing Date:</td>';
   export_data_format += '<td class="c-desc">'+next_hearing_date+'</td>';
   export_data_format += '</tr>';
 }
 if(!isEmpty(session_phase)){
   export_data_format += '<tr>';
   export_data_format += '<td class="c-type">Session:</td>';
   if(session_phase == 1)
     export_data_format += '<td class="c-desc">Morning</td>';
   else if(session_phase == 2)
    export_data_format += '<td class="c-desc">Evening</td>';
  else
    export_data_format += '<td class="c-desc"></td>';
  export_data_format += '</tr>';
}

if(!isEmpty(response.appearing_modal)){
  var app_model = response.appearing_modal.split("-");

  var appmodelval = [];
  for(var i=0;i<app_model.length;i++){
    appmodelval.push(app_model[i].trim());
  }
  var appear_as = appmodelval.indexOf(response.data[0].are_you_appearing_as);

  if(appear_as == 0){    
    export_data_format += buildRespondentExport(response.respondent_data,response.data[0].are_you_appearing_as,response.appearing_modal);    
  }
  else{
    export_data_format += buildPetitionerExport(response.petitioners_data,response.data[0].are_you_appearing_as,response.appearing_modal);
  }
}
else{
  if(response.data[0].are_you_appearing_as == 'Petitioner'){   
    export_data_format += buildRespondentExport(response.respondent_data,response.data[0].are_you_appearing_as,response.appearing_modal);    
  }
  else{        
   export_data_format += buildPetitionerExport(response.petitioners_data,response.data[0].are_you_appearing_as,response.appearing_modal);
 }
}
export_data_format += buiildPetitionerAdvExport(response.pet_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);
export_data_format += buildRespondentAdvExport(response.res_advocate_data,response.data[0].are_you_appearing_as,response.appearing_modal);

export_data_format +='</tbody></table>';
$("#details_main_left").html(export_data_format);

var table = '';
if(!isEmpty(response.team_data)){
  $("#team_member_count").text(response.team_data.length);
  var tr = '';
  $.each(response.team_data,function(i,obj){
   teamId.push(obj.team_id);
   var email = isEmpty(obj.email) ? "" : obj.email;
   var mobile = isEmpty(obj.mobile) ? "" : obj.mobile;
   if(!isEmpty(obj.first_name) || !isEmpty(obj.last_name)){
    tr+='<tr>';
    tr+='<td>'+obj.first_name+'  '+obj.last_name+'</td>';
        // tr+='<td>'+obj.last_name+'</td>';
        tr+='<td>'+email+'</td>';
        tr+='<td>'+mobile+'</td>';
      }
    // tr+='<td><button class="btn btn-danger btn-sm" type="button" onclick="deleteteamMember()"><i class="fa fa-trash"></i></button><tr>';
  });
  if(tr!=''){

    table = ' <table class="table table-bordered main-table export_table col-sm-12" id="main-table1" style="width:100%"><thead>';
    table +='<tr><th style="text-align:center">Team Member</th><th></th><th></th></tr><tr><td>Name</td><td>Email</td><td>Contact</td></tr></thead><tbody>';
    table += tr +'</tbody></table>';
  }
  $("#team-member_export_report").html(table);
}

courtdata = '<table class="table hearing_date_table main-table" id="hearing-data-table"><thead style="display:none"><tr><th></th><th>Activity/History</th><th> </th></tr></thead>';
courtdata +='<tr>';

courtdata +='<td class="user_name"> by '+response.user_name+'</td><td></td>';
courtdata +='<td style="text-align:right;"> <span> '+response.created_date+'</span> <span>'+response.created_time+'</span></td>';
courtdata +='</tr>';  
courtdata +='<tr>';
var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
var d = new Date(response.data[0].date_time);
var fildate = month[d.getMonth()];
courtdata +='<td></td><td class="activity-chunks" style="text-align:right;"><p><b>Created on </b>  '+fildate +' '+ d.getDate() + ','+ d.getFullYear()+'.</p></td><td></td>';
courtdata +='</tr>';
courtdata +='<tr><hr></tr>';

courtdata += buildActivityLogExport(response.data[0].case_id,response.data[0].case_no,response.user_image);

if(!isEmpty(response.hearing_data)){

  $.each(response.hearing_data,function(i,obj){
    if(isEmpty(obj.activity_log_type)){ 
      courtdata +='<tr>';
      courtdata +='<td class="user_name"> by '+response.user_name+'</td><td></td>';
      courtdata +='<td class="desktop-view-icons-td"> <span>'+obj.date+'</span> <span> '+obj.time+'</span></td>';
      courtdata +='</tr>';
      if(!isEmpty(obj.stage)){
        courtdata +='<tr>';
        courtdata +='<td style="width:1px;"></td>';
        courtdata +='<td class="activity-chunks"><p><b>Stage </b> '+obj.stage+'</p></td>';
        courtdata +='</tr>';
      }
      courtdata +='<tr>';
      courtdata +='<td style="width:1px;"></td>';
      courtdata +='<td class="activity-chunks"><p><b>Next Hearing Date </b> '+obj.next_hearing_date+'</p></td>';
      courtdata +='</tr>';
    }

    if(obj.activity_log_type == 'h'){        
      courtdata +='<tr>';
      courtdata +='<td style="width:1px;"><img src="'+getActivityUserProfile(response.user_image)+'" alt="" title="Adv. Vidhi Sharma"> by '+response.user_name+'</td>';
      courtdata +='<td class="user_name"></td>';
      courtdata +='<td class="desktop-view-icons-td"><span> '+obj.date+'</span> <span> '+obj.time+'</span></td>';
      courtdata +='</tr>';

      courtdata +='<tr>';
      courtdata +='<td style="width:1px;"></td>';
      courtdata +='<td class="activity-chunks"><p><b>Stage </b> '+obj.stage+'</p></td></tr>';
      if(!isEmpty(obj.posted_for)){
        courtdata +='<tr>';
        courtdata +='<td></td>';
        courtdata +='<td class="activity-chunks"><p><b>Posted For </b> '+obj.posted_for+'</p></td></tr>';
        courtdata +='<tr>';
      }
      if(!isEmpty(obj.action_taken)){
        courtdata +='<td style="width:1px;"></td>';
        courtdata +='<td class="activity-chunks"><p><b>Action taken </b> '+obj.action_taken+'</p></td></tr>';
      }
      if(!isEmpty(obj.next_hearing_date)){
        courtdata +='<tr>';
        courtdata +='<td style="width:1px;"></td>';
        courtdata +='<td class="activity-chunks"><p><b>Next Hearing Date </b>'+obj.next_hearing_date+'</p></td>';
        courtdata +='</tr>';

      }
      if(!isEmpty(obj.session_phase)){
        courtdata +='<tr>';
        courtdata +='<td></td>';
        if(obj.session_phase == '1')
          courtdata +='<td class="activity-chunks"><p><b>Session </b> Morning</p></td></tr>';
        else
          courtdata +='<td class="activity-chunks"><p><b>Session </b> Evening</p></td></tr>';        
      }
      if(!isEmpty(obj.who_attended)){
        courtdata +='<tr>';
        courtdata +='<td></td>';        
        var who_attended_namearray=[];
        $.each(obj.who_attended,function(index,object){
          who_attended_namearray.push(object.who_attended_name);
        });
        courtdata +='<td class="activity-chunks"><p><b>Attended by </b> '+who_attended_namearray.join(",")+'</p></td></tr>';
      }

    }

  });

}
courtdata +='<tbody></table>';

$("#hearindata_export_report").html(courtdata);

courtdata = documentExport(caseid);

courtdata = connectedCaseExport(caseid);

courtdata = timesheetExport(caseid);

courtdata = getNotesExport(caseid);

// notesExport();
}
},
error:function(){

}
});
}

function getNotesExport(case_id){
  $.ajax({
    url : "ajaxdata/savenote.php",
    type : 'POST',
    data: {
      case_id: case_id
    },
    cache: false,
    async:false,
    dataType: 'json',
    success : function(data){
      var tr ='<table class="table table-bordered main-table col-sm-12"><thead><tr><th>Notes</th><th></th><th></th></tr></thead><tbody>';
      $.each(data,function(i,obj){
       tr +='<tr>';
       if(obj.private == 'yes')
        tr +='<td>Private</td>';
      else
        tr+='<td></td>';
      tr +='<td><b id="note-creater">';
      tr += obj.name+' '+obj.last_name+'</b></td>';
      tr +='<td><b id="note-created-date">';
      tr += obj.datetime+'</b></td>';
      tr +='</tr>';
    });
      tr+='</tbody></table>';
      $("#notes_export").html(tr);
    }
  });
}

function timesheetExport(caseid){
  var table='',tr='';
  var userid = $("#userid").val();
  $.ajax({
    url : "ajaxdata/gettimesheet.php",
    type : 'POST',
    data: {
      case_id: caseid,
      user_id :userid
    },
    cache: false,
    async:false,
    dataType: 'json',
    success : function(response){
      console.log(response);  
      $.each(response,function(i,obj){
        tr +='<tr>';
        // tr +='<td style="text-align:center;"><input type="checkbox" value="" name="" class="checked_checkbox"></td>';
        tr +='<td>'+obj.edate+'</td>';
        tr +='<td>'+obj.particulars+'</td>';
        tr +='<td>'+obj.timespent+':'+obj.timeinmin+'</td>';
        tr +='<td>'+obj.types+'</td></tr>';
        
      });
      if(tr!=''){
        table = '<table class="table table-bordered main-table col-sm-12" id="documentList-table">';
        table +='<thead><th>Timesheet</th><th></th><th></th><th></th></tr><tr><th>Date</th><th>Particulars</th><th>Timespent(in Hours)</th><th>Type</th></tr></thead><tbody>';
        table += tr+'</tbody></table>';
      }

      $("#timesheet_export").html(table);
    },
    error:function(){
      toastr.error("Timesheet ",{timeout:5000});
    }
  });
  return table;
}

function buiildPetitionerAdvExport(response,appearing_as,appeaing_model){
 var courtdata = '';
 if(!isEmpty(response)){
  if(!isEmpty(appeaing_model)){
    var app_model = appeaing_model.split("-");

    var appmodelval = [];
    for(var i=0;i<app_model.length;i++){
      appmodelval.push(app_model[i].trim());
    }
    var appear_as = appmodelval.indexOf(appearing_as);
    courtdata += '<tr>';
    if(appear_as == 0)
      courtdata += '<th><span class="section-header full-width"> Your Advocates <span class="client-type-txt">('+app_model[0]+')</span></span>';
    else
      courtdata += '<th><span class="section-header full-width"> Opponent Advocates <span class="client-type-txt">('+app_model[0]+' )</span></span>';


  }
  else{
    courtdata += '<tr>';
    if(appearing_as == 'Petitioner')
      courtdata += '<th class=""><span class="section-header full-width"> Your Advocates <span class="client-type-txt">(Petitioner)</span></span>';
    else
      courtdata += '<th class=""><span class="section-header full-width"> Opponent Advocates <span class="client-type-txt">(Petitioner )</span></span>';
  }
  courtdata += '</th>';
  
  $.each(response,function(index,object){
    courtdata += '<td class="">';
    courtdata += '<p>'+object.advocate_name+'</p>';
    courtdata += '</td>';
  });
  courtdata +='</tr>';
}
return courtdata;
}

function buildPetitionerExport(response,appearing_as,appeaing_model){
  if(!isEmpty(response)){
    if(!isEmpty(appeaing_model)){
      var app_model = appeaing_model.split("-");
      var appmodelval = [];
      for(var i=0;i<app_model.length;i++){
        appmodelval.push(app_model[i].trim());
      }
      var courtdata = '';
      var appear_as = appmodelval.indexOf(appearing_as);
      courtdata += '<tr>';
      if(appear_as == 0)
        courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Your Clients <span class="client-type-txt">('+app_model[0]+')</span></span>';
      else
        courtdata += '<th class="" colspan="2"><span class="section-header full-width"> Opponent <span class="client-type-txt">('+app_model[0]+')</span></span>';
    }
    else{   
      courtdata += '<tr>';
      if(appearing_as == 'Petitioner')
        courtdata += '<th class=""><span class="section-header full-width"> Your Clients <span class="client-type-txt">(Petitioner)</span></span>';
      else
        courtdata += '<th class=""><span class="section-header full-width"> Opponent Clients <span class="client-type-txt">(Petitioner)</span></span>';
    }
    courtdata += '</th>';

    courtdata += '<td class="">';
    $.each(response,function(index,object){
      index++;
      courtdata += index+') <p>'+object.petitioner_name+'</p>';        
    });
    courtdata += '</td></tr>';
  }
  return courtdata ;
}

function buildRespondentExport(response,appearing_as,appeaing_model){
  if(!isEmpty(response)){
    if(!isEmpty(appeaing_model)){
      var app_model = appeaing_model.split("-");
      var courtdata = '';
      var appmodelval = [];
      for(var i=0;i<app_model.length;i++){
        appmodelval.push(app_model[i].trim());
      }
      var appear_as = appmodelval.indexOf(appearing_as);

      courtdata += '<tr>';
      if(appear_as == 0)
        courtdata += '<th><span class="section-header ">Opponent ('+app_model[1]+') </span>';   
    }
    else{
      courtdata += '<tr>';
      if(appearing_as == 'Petitioner')
        courtdata += '<td><span class="section-header ">Opponent (Respondent) </span>';
  // else 
  //   courtdata += '<th class="" colspan="2"><span class="section-header ">Opponent (Respondent) </span>';
}
courtdata += ' </td>';
courtdata += ' <td class="" data-url="">';
$.each(response,function(index,object){
  index++;
  if(object.petitioner_name)
    courtdata += '<p class="case-petopp-info">'+object.petitioner_name+'</p>';
  else
    courtdata +='<p class="case-petopp-info">'+object.respondent_name+'</p>';

});
courtdata += '</td>';
courtdata += '</tr>';
}
return courtdata;
}

function buildRespondentAdvExport(response,appearing_as,appeaing_model){
  var courtdata ='';
  if(!isEmpty(response)){
    if(!isEmpty(appeaing_model)){
      var app_model = appeaing_model.split("-");

      var appmodelval = [];
      for(var i=0;i<app_model.length;i++){
        appmodelval.push(app_model[i].trim());
      }
      var appear_as = appmodelval.indexOf(appearing_as);
      courtdata +='<th>';
      if(appear_as == 0 || appearing_as == 'Petitioner')
        courtdata +=' <span class="section-header ">Opponent Advocates ('+app_model[1]+') </span>';
      else
        courtdata +=' <span class="section-header ">Your Advocates ('+app_model[1]+') </span>';
    }
    else{

      courtdata = '<th>';
      if(appearing_as == 'Petitioner')
        courtdata +=' <span class="section-header ">Opponent Advocates (Respondent) </span>';
      else
        courtdata +=' <span class="section-header ">Your Advocates (Respondent) </span>';
    }

    courtdata +='</th>';

    $.each(response,function(index,object){

      courtdata +='<td class="">';
      courtdata +='<p class="case-petopp-info">'+object.resadvocate_name+'</p></td>';

    });
    courtdata +='</tr>';
  }
  return courtdata;
}

function documentExport(caseid){
  var table = '';
  $.ajax({
    url : host+"/pdf_document.php",
    type : 'GET',
    cache : false,
    async:false,
    data : {
      "case_id" : caseid
    },
    contentType : 'application/json,utf-8',
    success:function(response){
      var response_val = JSON.parse(response);
      var tr ='';
      $("#documentList_export").html('');
      
      $.each(response_val.data,function(i,obj){
        var filesize = bytesToSize(obj.size);
        tr +='<tr>';
        tr +='<td class="case-title"><a href="'+getDocUrl(obj.file_name)+'" target="_blank" download><i class="fa fa-file-pdf-o"></i> '+obj.file_name+'</a></td>';
        tr +='<td class="case-title">'+filesize +'</td>';
        tr +='<td class="case-title">'+obj.uploaded_by+'</td>';
        tr +='<td class="case-title">'+obj.type+'</td>';
        tr +='<td class="case-title">'+obj.date_time+'</td></tr>';
      });
      if(tr!=''){
        table = '<table class="table table-bordered main-table col-sm-12" id="documentList-table">';
        table +='<thead><tr><th>Document List</th><th></th><th></th><th></th><th></th></tr><th>Title</th><th>Size</th><th>Uploaded By</th><th>Type</th><th>Uploaded Date</th></tr></thead><tbody>';
        table += tr+'</tbody></table>';
      }
      $("#documentList_export").html(table);
      
    }
  });
  return table;
}

function connectedCaseExport(caseid){
  var uploadVo = new Object();
  uploadVo.connected_id = '';
  uploadVo.case_id = caseid;
  uploadVo.status = '';
  uploadVo.priority = '';
  uploadVo.status_data = '';
  var table = '';
  $.ajax({
    url: 'submit_connected_case.php',
    type:'POST',
    dataType: 'json',
    data : {
      connect_data : JSON.stringify(uploadVo)
    },
    cache:false,
    async:false,
    success:function(response){
      var tr ='';
      $.each(response,function(i,obj){
        var casetype = isEmpty(obj.case_type) ? "" : obj.case_type;
        tr +='<tr>';
        tr +='<td class="case-title"><a style="text-decoration:none;cursor:pointer" href="'+host+'/view_case.php?caseid='+obj.encrypt_caseid+'" target="_blank">'+obj.case_title+'</a></td>';
        tr +='<td class="case-title">'+casetype+ ' ' +obj.case_no+'/ '+obj.case_no_year+'</td>';
        tr +='<td class="case-title">'+obj.status_data+'</td></tr>';        
      });
      if(tr!=''){
        table = '<table class="table table-bordered main-table"><thead><tr><th>Connected Cases</th><th></th><th></th></tr><tr><th class="title_class">Title</th><th class="case_class">Case Reference No.</th><th>Relation</th></tr></thead><tbody>';
        table += tr +'</tbody></table>';
      }
      $("#connected_case_export").html(table);

    },
    error:function(e){
      toastr.error("Error in fetching state",{timeout:5000});
    }
  });
  return table;
}

function buildActivityLogExport(caseid,case_no,userimge){
  var data_td='';
  $.ajax({
    url : host+"/fetch_user_activity.php",
    data : {
      case_id : caseid,
      case_no : case_no
    },
    type:"POST",
    async:false,
    success : function(response){
      var response = JSON.parse(response);
      $.each(response,function(i,obj){
        $(".case_title_div").css('background',obj.code);
        if(obj.activity_log_type == 'p'){        
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="'+getActivityUserProfile(userimge)+'" alt="" title="Adv. Vidhi Sharma"> by '+obj.user_name+'</td>';
          if(isEmpty(obj.last_priority))
            data_td +='<td></td><td class="activity-chunks"><p><b>Priority </b> added <b>'+obj.priority+'</b></p></td>';
          else
            data_td +='<td></td><td class="activity-chunks"><p><b>Priority </b> changed from <b>'+obj.last_priority+'</b> to <b>'+obj.priority+'</b></p></td>';
          data_td +='<td><span><i class="fa fa-calendar icons-view-case"></i> '+obj.created_date+'</span> <span> <i class="fa fa-clock-o icons-view-case"></i>'+obj.created_time+'</span></td>';
          data_td +='</tr>';
        }
        if(obj.activity_log_type == 'c') {
          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="'+getActivityUserProfile(userimge)+'" alt="" title="Adv. Vidhi Sharma">by '+obj.user_name+'</td>';
          data_td +='<td class="user_name">'+obj.user_comment+'</td>';
          data_td +='<td class="desktop-view-icons-td"> <span> '+obj.created_date+'</span> <span> '+obj.created_time+'</span></td>';
          data_td +='</tr>';
          
          /*data_td +='<tr>';
          data_td +='<td></td><td>'+obj.user_comment+'</td>';
          data_td +='</tr>';*/
        }
        if(obj.activity_log_type == 'a') {
          var fileExtension = ['jpeg', 'jpg', 'png'];

          data_td +='<tr>';
          data_td +='<td style="width:5%;"><img src="'+getActivityUserProfile(userimge)+'" alt="" title="'+obj.user_name+'"> by '+obj.user_name+'</td>';
          // data_td +='<td class="user_name"></td>';
          if ($.inArray(obj.document.split('.').pop().toLowerCase(), fileExtension) == -1) 
            data_td +='<td><b>Added Attachment</b> <a href="'+getDocUrl(obj.document)+'" download>'+obj.document+'</a></td>';
          else
            data_td +='<td><b>Added Attachment</b> <a href="'+getDocUrl(obj.document)+'" target="_blank" download>'+obj.document+'</a><div class="speech-bubble"><img src="'+getDocUrl(obj.document)+'" class="doc_img"></div></td>';
          data_td +='<td class="desktop-view-icons-td"> <span> '+obj.created_date+'</span> <span> '+obj.created_time+'</span></td>';
          data_td +='</tr>';
        }

      });

    }
  });
  return data_td;
}