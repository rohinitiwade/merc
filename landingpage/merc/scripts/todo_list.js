var todocounter=0;
$(document).ready(function(){

$(".selectall_checkbox").change(function() { //"select all" change 
      $(".todo-list-checkbox").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
  });

    //".checkbox" change 
    $('.todo-list-checkbox').change(function() {
      //uncheck "select all", if one of the listed checkbox item is unchecked
      if (false == $(this).prop("checked")) { //if this item is unchecked
        $(".selectall_checkbox").prop('checked', false); //change "select all" checked status to false
    }
      //check "select all" if all checkbox items are checked
      if ($('.todo-list-checkbox:checked').length == $('.todo-list-checkbox').length) {
      	$(".selectall_checkbox").prop('checked', true);
      }
  });
});

function markCompleted(id,todoid){
$.ajax({
		url: 'complete_todo.php',
		type:'POST',
		data : "to_id="+todoid,
		dataType: 'json',
		async:false,
		beforeSend: function(){
			$('.flip-square-loader').show();            
		},
		success:function(data){
			// console.log(data);
			if(data.status == 'success'){
				//location.reload();
				toastr.success('Successfully marked as complete', 'Success Alert', {timeOut: 5000}); 
				document.getElementById("todo_tr"+id).outerHTML = '';
			}
		},
		complete: function(){
			$('.flip-square-loader').hide();  

		},
		error: function(e){
			$('.flip-square-loader').hide();  
			toastr.error('Unable to mark as complete', 'Sorry For Inconvenience!', {timeOut: 5000})
		}
	});

}

/*function markToDoDone(){
	if($('.todo-list-checkbox:checked').length <= 0)
	{
		toastr.warning("",'Please select atleast one case',{timeout:5000});
		return false;
	}
}*/

function reminder(){
	$("#reminder_input").show();
	$("#reminder_clickhere").hide();
}
function remove_reminder(){
	$("#reminder_input").hide();
	$("#reminder_clickhere").show();
}
function add_field()
{
	todocounter++;
	// var total_text=document.getElementsByClassName("input_text");
	var append_reminder ="<div class='col-sm-12 p-0' style='float:left' id='input_text"+todocounter+"_wrapper'>";
	append_reminder +="<div class='col-sm-1 p-0' style='float: left;'><button class='btn btn-danger btn-mini' type='button' onclick=remove_field("+todocounter+")><i class='feather icon-trash autominus'></i></button></div>";
	append_reminder +="<div class='col-sm-3 p-0 form-group' style='float: left;'>";
	append_reminder +="<select class='input_text form-control' id='email_option_"+todocounter+"'> ";
	append_reminder +="<option>Email</option><option>SMS</option></select></div>";
	append_reminder +="<div class='col-sm-3 form-group' style='float: left;'>";
	append_reminder +="<select class='input_text form-control reminder_format' id='minute_"+todocounter+"'>";
	append_reminder +=" <option value='minutes'>minute(s)</option><option value='hours'>hour(s)</option value='days'>";
	append_reminder +="<option value='days'>day(s)</option><option value='week'>week(s)</option></select>";
	append_reminder +="</div><div class='col-sm-2 form-group' style='float: left;'>";
	append_reminder +="<select class='input_text form-control' id='reminder_format_value_"+todocounter+"'> ";
	append_reminder +="<option>5</option><option>10</option><option>15</option><option>20</option>";
	append_reminder +="<option>25</option><option>30</option><option>35</option><option>40</option>";
	append_reminder +="<option>45</option><option>50</option><option>55</option></select></div>";
	append_reminder +="<span>before the due date & time</span></div>";

	$("#input").append(append_reminder);
	rcounter = todocounter;
}

function remove_field(id){
	document.getElementById("input_text"+id+"_wrapper").innerHTML="";
}


function todo(){
	var select_receipent_array = [];
	var assign_to_array = [];
	var reminder_array = [];
	var assign_adv = [];
	var privated = "public";
	var desc = $("#desc").val();
	if(desc == ''){
		toastr.warning('Please Enter To Do Description', {timeOut: 5000});  
		return false;
	}
	//var datepicker = $("#datepicker").val();
	var datepickerdata = $("#datepickerdata").val();
	//var replace_date1 =  datepicker.replace('/','');
	//var datepicker1 = $("#datepicker1").val();
	//var replace_date1_todo =  datepicker1.replace('/','');
	var email_option = $("#email_option").val();
	var time = $("#time").val();
	var minute = $("#minute").val();
	//var private = $("#private").val();
	if(document.getElementById('private').checked){
		privated = "private";
	}
	var full_name = $("#full_name").val();
	var assign_to = $("#framework").val();
	$.each(assign_to,function(a,obj){
		assign_to_array.push(obj);
	});
	var adv = $("#advocate").val();
	$.each(adv,function(a,ad){
		assign_adv.push(ad);
	});
	// $.each(email_option,function(a,email){
	// 	email_option_array.push(email);
	// });
	// $.each(time,function(a,time){
	// 	time_array.push(time);
	// });
	// $.each(minute,function(a,min){
	// 	minute_array.push(min);
	// });
	for(var i = 0;i<=rcounter;i++){
		var email_option = $("#email_option_"+i).val();
		var time  = $("#reminder_format_value_"+i).val();
		var minute = $("#minute_"+i).val();
		var reminderVo = {
			email_option : email_option,
			time : time,
			minute : minute
		}
		reminder_array.push(reminderVo);
	}

	
	var relate_to = $("#relate").select2('data');
	$.each(relate_to,function(i,obj){
		select_receipent_array.push(obj);
	});

	var todoVo = new Object();
	todoVo.desc = desc;
	//todoVo.datepicker = replace_date1;
	//todoVo.datepicker1 = replace_date1_todo;
	// todoVo.option = option;
	// todoVo.time = time;
	// todoVo.minute = minute;
	todoVo.private = privated;
	todoVo.full_name = full_name;
	todoVo.assign_to_array = assign_to_array;
	todoVo.relateto = select_receipent_array;
	todoVo.reminder_array = reminder_array;
	todoVo.advocates = assign_adv;
	todoVo.datepickerdata = datepickerdata;
	$.ajax({
		url: 'submit_todo.php',
		type:'POST',
		data : "tododetail="+JSON.stringify(todoVo),
		dataType: 'json',
		async:false,
		beforeSend: function(){
			$('.flip-square-loader').show(); 
			toastr.success('Submit Data Successfully', 'Success Alert', {timeOut: 5000})             
		},
		success:function(data){
			// console.log(data);
			if(data.status == 'success'){
				location.reload();
			}
		},
		complete: function(){
			$('.flip-square-loader').hide();  

		},
		error: function(e){
			$('.flip-square-loader').hide();  
			toastr.error('Unable to Submit Data', 'Sorry For Inconvenience!', {timeOut: 5000})
		}
	});

}

// 	$("#relate").select2({
// ajax:{
// 		url: host+'/select_relateto.php',
// 		type:'GET',
// 		// data : 'tododetail='+JSON.stringify(todoVo),
// 		dataType: 'json',
// 		async:false,
// 		    beforeSend: function(){
//          $('.flip-square-loader').show(); 
//          toastr.success('Submit Data Successfully', 'Success Alert', {timeOut: 5000})             
//     },
// 		success:function(data){
// 			 var $sel_relateto = $("#sel_relateto");
//                     $sel_relateto.empty(); // remove old options
//                     $sel_relateto.append($("<option></option>")
//                             .attr("value", '').text('Please Select'));
//                     $.each(data, function(value, key) {
//                         $sel_relateto.append($("<option></option>")
//                                 .attr("value", value).text(key));
//                     });	

// 		},
// 		   complete: function(){
//         $('.flip-square-loader').hide();  

//     },
//     error: function(e){

//         $('.flip-square-loader').hide();  
//          toastr.error('Unable to Submit Data', 'Sorry For Inconvenience!', {timeOut: 5000})
//     }
// 				}
// 			});


// $(".select2").select2({
//  minimumInputLength: 2,
//   tags: [], 
//   ajax: { 
//   	url: host+'/select_relateto.php', 
//   	dataType: 'json', 
//   	type: "GET", 

//   	// data: function (term) { return { term: term }; },
//   	 results: function (data) {debugger; return 
//   		{ results: $.map(data, function (item) { return
//   		 { text: item.completeName, slug: item.slug, id: item.id } 
//   		}) 
//   	}; 
//   } 
// } 
// });


var rcounter =0;

$(document).on('change','.reminder_format',function(){
	var reminder_format_value='';
	var val = $(this).val();

	if(val == 'minutes'){
		for(var i =1 ; i <= 11;i++){
			reminder_format_value += '<option value="'+i*5+'">'+i*5+'</option>';
		}
	}
	if(val == 'hours'){
		for(var i =1 ; i <= 23;i++){
			reminder_format_value += '<option value="'+i+'">'+i+'</option>';
		}
	}
	if(val == 'days'){
		for(var i =1 ; i <= 31;i++){
			reminder_format_value += '<option value="'+i+'">'+i+'</option>';
		}
	}
	if(val == 'week'){
		for(var i =1 ; i <= 4;i++){
			reminder_format_value += '<option value="'+i+'">'+i+'</option>';
		}
	}
	$("#reminder_format_value_"+rcounter).html(reminder_format_value);
});