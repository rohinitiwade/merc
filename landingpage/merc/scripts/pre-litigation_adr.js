changeLanguage('English');
function changeLanguage(language){
	tinymce.init({
		selector : "#query",
		plugins : [
		"wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });

	if(language == 'Marathi'){
		google.setOnLoadCallback(OnLoad);
		OnLoad();
	}
}
google.load("elements", "1", {packages: "transliteration"});

function OnLoad() { 
	var options = {
		sourceLanguage:
		google.elements.transliteration.LanguageCode.ENGLISH,
		destinationLanguage:
		[google.elements.transliteration.LanguageCode.MARATHI],
		shortcutKey: 'ctrl+g',
		transliterationEnabled: true
	};	
	var control = new google.elements.transliteration.TransliterationControl(options);
	control.makeTransliteratable(["query_ifr"]);
	
} //end onLoad function
var con = 1;
function addDocument(){        
	con++;
	var multi_doc ='<div id="passportappendDiv_'+con+'">';
	multi_doc+='<input type="file" id="pet-file-'+con+'" class="document_upload" aria-label="File browser example" style="margin-top: 4px;" onchange=submitDoc('+con+')>';
	multi_doc+='<button class="btn btn-sm btn-danger" type="button" onclick="removePassport('+con+')"> ';
	multi_doc+='<i class="fa fa-minus" aria-hidden="true"></i></button></div></div>';
	$('#addedDocument').append(multi_doc);
}
function removePassport(con){
	document.getElementById("passportappendDiv_"+con).outerHTML="";
}