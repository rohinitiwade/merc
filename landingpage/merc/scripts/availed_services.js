function documnet.ready(){
	$.ajax({
		url:,
		type:,
		datatype:,
		success: function(response){
			var table_availedservices= '';
			table_availedservices+='<table class="table table-bordered"><thead class="bg-primary">';
			table_availedservices+='<tr>';
			table_availedservices+='<th class="court-report">File No</th>';
			table_availedservices+='<th class="court-report">Name of Court</th>';
			table_availedservices+='<th class="case-report">Case No / Year</th> ';                                 
			table_availedservices+='<th class="title-report">Name of Dept</th>';
			table_availedservices+='<th class="title-report">Title</th>';
			table_availedservices+='<th class="hearing-report">Hearing Date</th>';
			table_availedservices+='<th class="team-report">Team Member(s)</th>';
			table_availedservices+='<th class="services-report">Services</th>';
			table_availedservices+='</tr>';
			table_availedservices+='</thead>';
			table_availedservices+='<tbody>';
			$.each(response,function(i,obj){
				table_availedservices+='<tr style="cursor:pointer;">';
				table_availedservices+='<td class="rowclickable" style="padding: 10px;"></td>';
				table_availedservices+='<td class="rowclickable" style="padding: 10px;"></td>';                                 
				table_availedservices+='<td class="rowclickable" style="padding: 10px;"></td>';
				table_availedservices+='<td class="rowclickable" style="padding: 10px;"></td>'; 
				table_availedservices+='<td class="rowclickable" style="padding: 10px;"></td>';
				table_availedservices+='<td class="rowclickable"style="padding: 10px;"></td>';
				table_availedservices+='<td class="rowclickable" style="padding: 10px;"></td>';
				table_availedservices+='<td style="padding: 10px;">';
				table_availedservices+='<a href="drafting.php"><i class="icofont icofont-file-file services_logo" title="Drafting"></i></a>';
				table_availedservices+='<a href="legal_opinion.php"><i class="icofont icofont-file-text services_logo" title="Legal Opinion"></i></a>';
				table_availedservices+='<a href="legal_research.php"><i class="icofont icofont-document-search services_logo" title="Legal Research"></i></a>';
				table_availedservices+='<a href="legal_audit.php"><i class="icofont icofont-law-document services_logo" title="Legal Audit"></i></a>';
				table_availedservices+='<a href="petition_reply.php"><i class="icofont icofont-court services_logo" title="Petition Reply"></i></a>';
				table_availedservices+='<a href="translation_services.php"><i class="icofont icofont-radio-mic services_logo" title="Translation Services"></i></a>';
				table_availedservices+='<a href="q&a.php"><i class="icofont icofont-social-google-talk services_logo" title="Q & A"></i></a>';
				table_availedservices+='<a href="pre-litigation_adr.php"><i class="icofont icofont-law-alt-2 services_logo" title="Pre-Litigation ADR"></i></a>';
				table_availedservices+='<a href="post-litigation_adr.php"><i class="icofont icofont-order services_logo" title="Post-Litigation ADR"></i></a>';
				table_availedservices+='<a href="legal_vetting.php"><i class="icofont icofont-law-book services_logo" title="Legal Vetting"></i></a>';
				table_availedservices+='</td>';
				table_availedservices+='</tr>';
			});
			
			table_availedservices+='</tbody></table>';
			$("#availed-services-table").html(table_availedservices)
		},
		error: function(e){
			tostr.error('','Error',{timeOut: 50000})
		}
	})
}