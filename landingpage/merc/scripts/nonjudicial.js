changeLanguage('English');
function changeLanguage(language){
  tinymce.init({
    selector : "#query",
    plugins : [
    "wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "  directionality textcolor paste fullpage textcolor colorpicker " ],
        //removed textpattern from plugin because of getting bullets on enter key press in judgment
        toolbar1 : "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect | forecolor backcolor | print fullscreen | hr removeformat",
        menubar : false,
        contextmenu_never_use_native : true,
        toolbar_items_size : 'small',
        branding : false,
        height : 200
      });

  if(language == 'Marathi'){
    google.setOnLoadCallback(OnLoad);
    OnLoad();
  }
}
$(document).ready(function(){
// debugger;
getTeamMemberListDropdown();
function getTeamMemberListDropdown(){
// var hidValue = $("#hidSelectedOptions").val();
var selectedOptions = [];

$.ajax({
  type:'POST',
  url:host+'/your_team.php',
  dataType:"json",
  success:function(response){
    // console.log(response);
// var response = JSON.parse(response);
if(response.team.length > 0){
  var option = '<option value="">Select</option>';
  $.each(response.team,function(i,obj){
    option += '<option value='+obj.reg_id+'>'+obj.name+' '+obj.last_name+'</option>';
  });
  $('#framework').html(option);
  $('#framework').select2({
    placeholder: 'Select...',
    closeOnSelect: false,
    multiple: true,
    allowClear: false
  });
}
$.each(response.show_team,function(i,obj){
  selectedOptions.push(obj.reg_id);
});
$('#framework').val(selectedOptions).trigger('change');
},
error:function(){
  toastr.error("","Error fetching team list",{timeout:5000});
}
});
}

$.each($("#framework option"), function (i, item) {
  if (item.selected) {
    /*$(item).prop("disabled", true); */
    $(".select2-selection__choice__remove").hide();
  }else {
    /*$(item).prop("disabled", false);*/
    $(".select2-selection__choice__remove").show();
  }
});
});

function submitNonjudicial(){
   $(".add_casesubmitdiv").addClass("fetchdataloader");
    $(".add_casesubmit").hide();
  // debugger;
  var client_name = $("#client_name").val();

        var type_ofwork = $("#type_ofwork").val();
          // var your_team = $("#framework").val();
          var shortdescription = tinyMCE.get('query').getContent();
var assign_to = $("#framework").val();
  var assign_to_array = [];
  $.each(assign_to,function(a,obj){
   assign_to_array.push(obj);
 });
  var pet_file='';
  var formData = new FormData();
  var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','xlsx','xls','doc','docx'];
  if(!isEmpty($("#upload").val())){
    // if ($.inArray($("#pet_file").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    //  toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls','doc','docx' formats are allowed.",{timeout:5000});
    //  return false;
    // }
    upload = document.getElementById("upload").files[0];
  }
  else
    upload = '';

    

 formData.append("upload",upload);
  if(client_name != '')
 formData.append("client_name",client_name);
else{
  toastr.error("","Please Enter Client Name",{timeout:5000});
  return false;
}

 formData.append("type_ofwork",type_ofwork);
 formData.append("description",shortdescription);
 formData.append("assign_to_array",assign_to_array);

 var ajaxResult = $.ajax({
   url : host+"/submit_nonjudicial.php",
   type : 'POST',
   data : formData,
   cache : false,
   contentType : false,
   processData : false,
   success:function(response){
     $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
    var response = JSON.parse(response);
    window.location.href='view-nonjudicialcase.php?caseid='+ response.caseid;

    
 },
 error:function(){
   $(".add_casesubmitdiv").removeClass("fetchdataloader");
    $(".add_casesubmit").show();
  toastr.error("","Error in adding case",{timeout:5000});
}
});
}
 // function submitNonjudicial(){
 //       var client_name = $("#client_name").val();
 //        var type_ofwork = $("#type_ofwork").val();
 //          var your_team = $("#framework").val();
 //          var shortdescription = tinyMCE.get('query').getContent();

 //    $.ajax({
 //    url: 'submit_nonjudicial.php',
 //    type:'POST',
 //    data : "tododetail="+JSON.stringify(todoVo),
 //    dataType: 'json',
 //    async:false,
 //    beforeSend: function(){
 //      $('.flip-square-loader').show(); 
 //      toastr.success('Submit Data Successfully', 'Success Alert', {timeOut: 5000})             
 //    },
 //    success:function(data){
 //      // console.log(data);
 //      if(data.status == 'success'){
 //        location.reload();
 //      }
 //    },
 //    complete: function(){
 //      $('.flip-square-loader').hide();  

 //    },
 //    error: function(e){
 //      $('.flip-square-loader').hide();  
 //      toastr.error('Unable to Submit Data', 'Sorry For Inconvenience!', {timeOut: 5000})
 //    }
 //  });
 // }
