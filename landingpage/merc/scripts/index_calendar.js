 months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
 today = new Date();
 currentMonth = today.getMonth();
 currentYear = today.getFullYear();
 $(document).ready(function(){   
 	var option='',optionyear='';
 	for(var i=0;i<=12;i++){
 		if(i==currentMonth)
 			option+='<option value='+i+' selected>'+ months[i] +'</option>';
 		else
 			option+='<option value='+i+'>'+ months[i] +'</option>';
 	}
 	$("#month").html(option);

 	for(var i=currentYear-30;i<=currentYear+10;i++){
 		if(i==currentYear)
 			optionyear+='<option value='+i+' selected>'+ i +'</option>';
 		else
 			optionyear+='<option value='+i+'>'+ i +'</option>';
 	}
 	$("#year").html(optionyear);

 });

 selectYear = document.getElementById("year");
 selectMonth = document.getElementById("month");

 monthAndYear = document.getElementById("monthAndYear");
 showCalendar(currentMonth, currentYear);


 function next() {
 	currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
 	currentMonth = (currentMonth + 1) % 12;
 	showCalendar(currentMonth, currentYear);

    // ----------------- ajax call count display in next month --------------------- 
    getMonthDate();

    /*var selectedmonth = $("#month").val(); 
    var selectedmonth = +selectedmonth + 1;  
    if(selectedmonth<10) 
    {
    	selectedmonth='0'+selectedmonth;
    } 
    else{
    	selectedmonth = selectedmonth;
    } 
    var selectedyear = $("#year").val();    
    var firstdate = '0'+1 +'-'+ selectedmonth +'-'+ selectedyear; 
    var selectedmonth = $("#month").val(); 
    var selectedmonth = +selectedmonth + 1;  
    monthslastdate = [31, 29, 31, 30,31,30,31,31,30,31,30,31];
    var monthwiselastDate = monthslastdate[selectedmonth];
    if(selectedmonth<10) 
    {
    	selectedmonth='0'+selectedmonth;
    } 
    else{
    	selectedmonth = selectedmonth;
    } 
    var lastdate = monthwiselastDate +'-'+selectedmonth+'-'+selectedyear;
    getData(firstdate,lastdate);*/
    // ------------------

}

function previous() {
	currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
	currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
	showCalendar(currentMonth, currentYear);

    // ---- ajax call for  previous month display count------------
    getMonthDate();
    /*var selectedmonth = $("#month").val(); 
    var selectedmonth = +selectedmonth + 1;  
    if(selectedmonth<10) 
    {
    	selectedmonth='0'+selectedmonth;
    } 
    else{
    	selectedmonth = selectedmonth;
    } 
    var selectedyear = $("#year").val();    
    var firstdate = '0'+1 +'-'+ selectedmonth +'-'+ selectedyear; 
    var selectedmonth = $("#month").val(); 
    var selectedmonth = +selectedmonth + 1;  
    monthslastdate = [31, 29, 31, 30,31,30,31,31,30,31,30,31];
    var monthwiselastDate = monthslastdate[selectedmonth];
    if(selectedmonth<10) 
    {
    	selectedmonth='0'+selectedmonth;
    } 
    else{
    	selectedmonth = selectedmonth;
    } 
    var lastdate = monthwiselastDate +'-'+selectedmonth+'-'+selectedyear;
    getData(firstdate,lastdate);*/
}

function jump() {
	currentYear = parseInt(selectYear.value);
	currentMonth = parseInt(selectMonth.value);
	showCalendar(currentMonth, currentYear);
	getMonthDate();
	/*var selectedmonth = $("#month").val(); 
	var selectedmonth = +selectedmonth + 1;  
	if(selectedmonth<10) 
	{
		selectedmonth='0'+selectedmonth;
	} 
	else{
		selectedmonth = selectedmonth;
	} 
	var selectedyear = $("#year").val();    
	var firstdate = '0'+1 +'-'+ selectedmonth +'-'+ selectedyear; 
	var selectedmonth = $("#month").val(); 
	var selectedmonth = +selectedmonth + 1;  
	months = [31, 29, 31, 30,31,30,31,31,30,31,30,31];
	var monthwiselastDate = months[selectedmonth];
	if(selectedmonth<10) 
	{
		selectedmonth='0'+selectedmonth;
	} 
	else{
		selectedmonth = selectedmonth;
	} 
	var lastdate = monthwiselastDate +'-'+selectedmonth+'-'+selectedyear;
	getData(firstdate,lastdate);*/

}

function showCalendar(month, year) {


	let firstDay = (new Date(year, month)).getDay();

    tbl = document.getElementById("calendar-body"); // body of the calendar

    // clearing all previous cells
    tbl.innerHTML = "";

    // filing data about month and in the page via DOM.
    monthAndYear.innerHTML = months[month] + " " + year;
    selectYear.value = year;
    selectMonth.value = month;

    // creating all cells
    let date = 1;
    for (let i = 0; i < 6; i++) {
        // creates a table row
        let row = document.createElement("tr");

        //creating individual cells, filing them up with data.
        for (let j = 0; j < 7; j++) {
        	if (i === 0 && j < firstDay) {
        		cell = document.createElement("td");
        		cell.setAttribute("id",+date);
        		cellText = document.createTextNode("");
        		cell.appendChild(cellText);
        		row.appendChild(cell);

        	}
        	else if (date > daysInMonth(month, year)) {
        		break;
        	}

        	else {
        		cell = document.createElement("td");
        		cell.setAttribute("id",+date);
                cellText = document.createTextNode(date); // date is inserted in td
                countspan = document.createElement("span"); 
                countspan.setAttribute("id", "count_"+date); // count is inserted in span 
                countspan.setAttribute("class", "countspan");
                // ----
                // spanText = document.createTextNode(date);
                if (date === today.getDate() && year === today.getFullYear() && month === today.getMonth()) {
                	cell.classList.add("bg-info");
                } // color today's date
                // countspan.appendChild(spanText);
                cell.appendChild(countspan);
                cell.appendChild(cellText);
                row.appendChild(cell);
                date++;
            }


        }

        tbl.appendChild(row); // appending each row into calendar body.
    }
}


// check how many days in a month code from https://dzone.com/articles/determining-number-days-month
function daysInMonth(iMonth, iYear) {
	return 32 - new Date(iYear, iMonth, 32).getDate();
}
$(document).ready(function(){
	getMonthDate();
});

function getMonthDate(){
	var selectedmonth = $("#month").val(); 
	var selectedmonth = +selectedmonth + 1;  
	if(selectedmonth<10) 
	{
		selectedmonth='0'+selectedmonth;
	} 
	else{
		selectedmonth = selectedmonth;
	} 
	var selectedyear = $("#year").val();    
	var firstdate = '0'+1 +'-'+ selectedmonth +'-'+ selectedyear; 
	var selectedmonth = $("#month").val(); 
	var selectedmonth = +selectedmonth + 1;  
	monthslastdate = [0,31, 29, 31, 30,31,30,31,31,30,31,30,31];
	var monthwiselastDate = monthslastdate[selectedmonth];
	if(selectedmonth<10) 
	{
		selectedmonth='0'+selectedmonth;
	} 
	else{
		selectedmonth = selectedmonth;
	} 
	var lastdate = monthwiselastDate +'-'+selectedmonth+'-'+selectedyear;
	getData(firstdate,lastdate);
}


function getData(firstdate,lastdate){
	$.ajax({
		type:'POST',
		url:'calenderData.php',
		dataType:"json",
		data:{
			fdate: firstdate,
			ldate: lastdate ,
		},
		async:false,
		cache:false,
		success:function(response){
			// console.log(response);
			$.each(response,function(i,obj){
              // var countprint =  obj.hearing_date.split("-") ;   
              // $("#count_"+countprint[2]).css({"background":"linear-gradient(to right, #fe5d70, #fe909d)","color":"white" ,"float":"right","padding":"2px 5px","border-radius":"11px"}); 
              var countprint =  obj.hearing_date.split("-") ;  
              var cnt = +countprint[2]; 
              $("#count_"+cnt).text(obj.count);             
              $("#count_"+cnt).css({"background":"linear-gradient(to right, #fe5d70, #fe909d)","color":"white" ,"float":"right","padding":"2px 5px","border-radius":"11px"}); 
          });

		},
		error:function(e){

		}
	});
}
$(document).on('click',"td",function(){
	var id = (this).id;
	/*var countcheck = $(this).text();*/
	var counttext = $("#count_"+id).text();
	if(isEmpty(counttext))
		return false;
	var selectedmonth = (parseInt($("#month").val()) +1) < 10 ?'0'+(parseInt($("#month").val()) +1) : (parseInt($("#month").val()) +1);   
	var selectedyear = $("#year").val(); 
	var dateValue =('0' + id).slice(-2);
	var hearing_date = dateValue +'-'+selectedmonth+'-'+selectedyear;
	$.ajax({
		type:'POST',
		url:'calenderDatewiseData.php',
		dataType:"json",
		data:{
			hearing_date:  hearing_date
		},
		async:false,
		cache:false,
		success:function(response){
            // console.log(response);
            /*if(countcheck.length == 2  || countcheck.length == 1 ){
            	$("#hearingdetailmodal").modal('hide');
            }*/
            // else
            // {
            	// console.log("advocate",response);
            	var table = '<table class="table table-bordered main-table" id="hearingdetails-table"><thead class="bg-primary">';
            	table +='<tr>';
                table += '<th>#</th>';
            	table += '<th>Sr No.</th>';
            	table += '<th>Court</th>';
            	table += '<th>Case</th>';
            	table += '<th>Title</th>';
            	table += '<th>Hearing Date</th>';
            	table += '</tr></thead><tbody>';
            	$.each(response,function(i,obj){
            		i++;
            		table += '<tr>';
                    table += '<td onclick=viewData("'+obj.encrypted_caseid+'")><button type="button" class="btn btn-sm btn-primary" >View </button></td>';
            		table += '<td>'+i+'</td>';
            		if(obj.court_id == 3)
            		{
            			table += '<td>'+obj.court_name+' - '+obj.state+' - '+obj.district+' - '+obj.court_establishment+' </td>';
            		} 
            		else if(obj.court_id == 2)
            		{
            			table += '<td>'+obj.court_name+'  - '+obj.bench+' - '+obj.side+' - '+obj.hc_stamp_register+' </td>';
            		}
            		else if(obj.court_id == 1)
            		{
            			table += '<td>'+obj.court_name+'-'+obj.supreme_court+'</td>';
            		}
            		else{
            			table += '<td>'+obj.court_name+'</td>';
            		}
            		table += '<td>'+obj.case+'</td>';
            		if(obj.hearing_date == null){
            			obj.hearing_date = '';
            		}   
            		else if(obj.case_title == null){
            			obj.case_title = '';
            		}
            		table += '<td>'+obj.case_title+'</td>';
            		table += '<td>'+obj.hearing_date+'</td>';             
        // table += '<td>'+obj.gst+'</td></tr>';
    });
            	table +='</tbody></table>';
            	$("#hearingdetails").html(table);
            	$("#hearingdetails-table").dataTable();
            	$("#hearingdetailmodal").modal('show');
            /*}*/

        },
        error:function(e){

        }
    });

});

function viewData(caseid){
var viewurl = host+'/view-case.php?caseid='+caseid;
 window.open(viewurl, '_blank');
}