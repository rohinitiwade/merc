<?php include("header.php"); ?>
<?php //include("chat-sidebar.php"); ?>
<?php //include("chat-inner.php"); ?> 
<?php include("phpfile/sql_home.php");?>
<style type="text/css">
  .files {
    position: relative;
    width: 100%;
    float: left;
}
.files input {
    outline: 2px dashed #92b0b3;
    outline-offset: -10px;
    -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
    transition: outline-offset .15s ease-in-out, background-color .15s linear;
    padding: 120px 0px 85px 35%;
    text-align: center !important;
    margin: 0;
    width: 100% !important;
}
.fetchdataloader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 60px;
  /*float: right;*/
  height: 60px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}
.upload_btn{
  width: 14%;
  margin: 0 auto;
}
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php"); ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Add Documents   </h5>
              </div>
              <div class="page-body">
                <!-- <?php
                $date = date('Y-m-d');
                if(isset($_POST['submit'])){
                 extract($_POST);
                  $images = $_FILES["image"]["name"];
                 if($images!=""){
                  $target_dir = "upload_document/";
                  $ext = pathinfo($images, PATHINFO_EXTENSION);
                  $time = time($images).'_'.$images;
                  $target_file = $target_dir . $time;
                  $size = $_FILES["image"]["size"];
                  $up = move_uploaded_file($_FILES["image"]["tmp_name"], $target_file); 
                }
                if($images!="")
                {
                  $caseid =  strstr($case_id, '_', true); 
                   $case_no =substr($case_id, strpos($case_id, "_") + 1);  
                   // echo $case_year =substr($case_id, strpos($case_id, "/") + 1);  
                   if($case_type == 'Other'){
                    $final_type = $_POST['other_doc'];
                    $inserttype = mysqli_query($connection, "INSERT INTO `documents_type` (`documents_type`,`date_time`)VALUES('".$final_type."','".$date."')");
                   }
                   else{
                    $final_type =$case_type;
                   }
                    $insert = mysqli_query($connection, "INSERT INTO `add_document` (`case_id`,`type`,`size`,`title`,`link_document`,`term`,`description`,`judgment_date`,`expiry_date`,`purpose`,`first_party`,`second_party`,`headed_by`,`image`,`date_time`,`case_no`,`user_id`,`division`,`under_division`,`image_name`)VALUES('".$caseid."','".$final_type."','".$size."','".$ext."','".$documents."','".$term."','".$description."','".$judgment_date."','".$expiry_date."','".$purpose."','".$first_pary."','".$second_party."','".$headed_by."','".$time."','".date("Y-m-d H:i:s")."','".$case_no."','".$_SESSION['user_id']."','".$_SESSION['cityName']."','".$_SESSION['under_division']."','".$_FILES["image"]["name"]."')");

                   if($insert){ ?>
                    <script type="text/javascript">
                      window.location.href='manage-document.php';
                    </script>
                  <?php }
                }
              }

              ?> -->
              <div class="card form-group">
                <div class="card-block">
                      <div class="media">
                        <?php 
                                                      // $sel = mysqli_query($connection,"SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['id'])."'");
                                                      // $sels = mysqli_fetch_array($sel);
                                                      $sql1 = mysqli_query($connection, "SELECT * FROM `add_document` WHERE `doc_id`='".base64_decode($_GET['id'])."' AND `image_remove`=1") or mysqli_error($connection);
                 $rows1 = mysqli_fetch_array($sql1);
                                                      ?>
                                                       <input type="hidden" name="case_id"  id="case_id" value="<?php echo $rows1['case_id'];?>">
                                                        <input type="hidden" name="enc_case_id"  id="enc_case_id" value="<?php echo base64_encode($rows1['case_id']);?>">
                                                       <input type="hidden" name="case_no"  id="case_no" value="<?php echo $rows1['case_no'];?>">
                                                       <input type="hidden" name="doc_id"  id="doc_id" value="<?php echo base64_decode($_GET['id']);?>">
                                                        <input type="hidden" name="img"  id="img" value="<?php echo $rows1['image'];?>">
                                                        <input type="hidden" name="image_name"  id="image_name" value="<?php echo $rows1['image_name'];?>">
                                                        <input type="hidden" name="size"  id="size" value="<?php echo $rows1['size'];?>">
                                          <div class="media-body col-sm-12">
                                             <div class="tab-pane" id="documenttab" role="tabpanel" aria-labelledby="documenttab">
                                                <form method="post" action="#" id="#">
                                                   <div class="form-group files">
                                                      <label>Upload Your Case Papers </label>
                                                      
                                                      <!-- <input type="hidden" name="case_id"  id="case_ids" value="<?php echo base64_decode($_GET['caseid']);?>"> -->
                                                      
                                                      <input type="file" class="form-control form-group" multiple="" id="upload_doc"  accept="image/x-png,image/jpeg,application/pdf,application/msword" value="<?php echo $rows1['image'];?>" /><?php if($rows1['image']!=""){?>
                            <a href="downloadDocument.php?getid=<?php echo $rows1['doc_guid'];?>&&flag=alldoc?>">
                                  <i class="feather icon-file-pdf-o pdfcolor"></i><?php echo $rows1['image'];?></a>&nbsp;&nbsp;<a href="remove_document?id=<?php echo base64_encode($rows1['doc_id']);?>" onclick="return confirm('Are you sure you want to Remove Document?');"><b style="color: red;">Remove </b></a>
                                  <?php } ?>
                                                   </div>
                                                   <div class="form-group row col-sm-12">
                                                    <div class="form-group col-sm-7 row" id="areyouappearingas">
                <label for="exampleInputUsername1" class="col-sm-3 col-form-label">&nbsp;&nbsp;Have you completely uploaded case papers</label>
                <div class="form-radio col">
                  <div class="radio radio-primary radio-inline col-sm-4">
                    <label>
                      <input type="radio" class="chkPassport" name="complete_papers"  id="appearing_radio_one" value="Yes" <?php if($rows1['uploaded_papers']=='Yes'){echo "checked";}?>>

                      <i class="helper"></i>Yes
                    </label>
                  </div>
                  <div class="radio radio-primary radio-inline col-sm-4">
                    <label>
                      <input type="radio" class="chkPassport"  name="complete_papers" id="appearing_radio_two" value="No" <?php if($rows1['uploaded_papers']=='No'){echo "checked";}?>>

                      <i class="helper"></i>No
                    </label>
                  </div>
                </div>
              </div>
                                                      <div class="col-sm-5 form-group row">
                                                         <label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp;document type</label>
                                                           <div class=" col-sm-6">
                                                         <select class="form-control" id="doc_type" name="doc_type">
                                                            <option value="<?php echo $rows1['doc_type_id'];?>"><?php if($rows1['type'] !=''){echo $rows1['type'];}else{?>Please select document type<?php } ?></option>
                                                            <?php $document = mysqli_query($connection, "SELECT * FROM `documents_type` ORDER BY documents_type ASC");
                                                            while($seldocmnet = mysqli_fetch_array($document)){?>
                                                               <option value="<?php echo addslashes($seldocmnet['doc_id']);?>"><?php echo addslashes($seldocmnet['documents_type']);?></option>
                                                            <?php } ?>
                                                         </select>
                                                       </div>
                                                      </div>
                                                    
                                                   </div>

                                                     <div class="col-sm-12 upload_btn">
                                                      <div class="add_casesubmitdiv" style="padding-right: 0; ">
                                                         <button class="btn btn-sm btn-primary newsubmit add_casesubmit"  type="button" id="upload_documents" value="Upload" name="Upload" onclick="getUploadedDoc('u','<?php echo $_GET['flag'];?>')">Upload</button>
                                                       </div>
                                                      </div>
                                                   <!-- <hr>
                                                   <div class="row col-sm-12">
                                                      <div class="col-sm-3 form-group">
                                                         <select class="form-control" id="search-doc-filter">
                                                            <option value="">NA</option>
                                                            <option value="caseno">Case No</option>
                                                            <option value="petitioner">Petitioner</option>
                                                            <option value="respondent">Respondent</option>

                                                         </select>
                                                      </div>
                                                      <div class="col-sm-3 form-group" id="search-document-div" style="display: none;">
                                                        <input type="text" class="form-control" id="search-in-document">
                                                     </div>
                                                     <div class="col-sm-3 form-group">
                                                      <button type="button" class="btn btn-warning btn-sm">Search in Document</button>
                                                   </div>
                                                </div> -->
                                                <hr>
                                                <div id="documentList" class="table-responsive col-sm-12"></div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php include 'footer.php'; ?>
<!-- <script type="text/javascript" src="scripts/add_documents.js"></script> -->
<script>

$(document).ready(function(){
  // getUploadedDoc('m');
  $("#doc_type").select2();
})
  function getUploadedDoc(type,flag){
    debugger;
  /*$(".tab-pane fade").removeClass("active show");
  $("#documents").addClass('active show');*/
    $(".add_casesubmitdiv").addClass("fetchdataloader");
    $(".add_casesubmit").hide();
  var file = '',doc_type='';
  if(type == 'u'){
    // var fileExtension = ['jpeg', 'jpg', 'png', 'pdf','doc','docx'];
    // if ($.inArray($("#upload_doc").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
    //   toastr.warning("","Only '.jpeg','.jpg', '.png', '.pdf','xlsx','xls' formats are allowed.",{timeout:5000});
    //    $(".add_casesubmitdiv").removeClass("fetchdataloader");
    //   $(".add_casesubmit").show();
    //   return false;
    // }


    file = document.getElementById("upload_doc").files[0];
  // var case_id = caseid;
  // if(isEmpty(file)){
  //   toastr.error("","Please upload atleast on document",{timeout:5000});
  //   return false;
  // }
  var enc_case_id = $("#enc_case_id").val();
  doc_type = $("#doc_type").val();
  var doc_value = $( "#doc_type option:selected" ).text();
  // doc_type = $("#doc_type option:selected").val();
  var caseid = $("#case_id").val();
  var case_no = $("#case_no").val();
var doc_id = $("#doc_id").val();
var img  = $("#img ").val();
var image_name = $("#image_name").val();
var size = $("#size").val();
}
else if(type == 'm'){
   doc_type = $("#doc_type option:selected").val();
  var caseid = $("#case_id").val();
  var case_no = $("#case_no").val();
}
 if ($("input[name='complete_papers']").is(":checked")) {
    var uploaded_papers = $("input[name='complete_papers']:checked").val();
}
var formData = new FormData();
formData.append('file', file);
formData.append('case_no', case_no);
formData.append('case_id',caseid);
formData.append('doc_id',doc_id);
formData.append('doc_type',doc_type);
formData.append('doc_value',doc_value);
formData.append('type',type);
formData.append('image',img);
formData.append('image_name',image_name);
formData.append('size',size);
formData.append('uploaded_papers',uploaded_papers);
var ajaxReq = $.ajax({
  url : host+"/submit_mercdocument.php",
  type : 'POST',
  data : formData,
  cache : false,
  contentType : false,
  processData : false,
  success:function(response){
     $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
    $("#documentList").html('');
    $('input[type=file]').val('');

    var response_val = JSON.parse(response);
    // var tr ='';
    if(type == 'u'){
      if(response_val.status == 'success'){
        toastr.success("",'File Uploaded Successfully',{timeout:3000});
      
      if(flag == 'view'){
     window.location.href='view-case.php?caseid='+enc_case_id;
   }else{
    window.location.href='manage-document.php';
}
}
      else
        toastr.error("","Error in uploading files",{timeout:5000});
    }
  //   $.each(response_val.data,function(i,obj){
  //     $("#view_case_document").text(obj.doc_count);
  //     var filesize = bytesToSize(obj.size);
  //     tr +='<tr>';
  //     tr +='<td class="case-title"><span id="'+obj.doc_path+'" onclick=viewcases('+obj.doc_id+',this.id) class="viewDoc"><i class="feather icon-file-pdf-o "></i> '+obj.file_name+'</span></td>';
  //     tr +='<td class="case-title">'+filesize +'</td>';
  //     tr +='<td class="case-title">'+obj.type+'</td>';
  //     tr +='<td class="case-title">'+obj.uploaded_by+'</td>';
  //     tr +='<td class="case-title">'+obj.date_time+'</td>';
  //     tr+='<td><a class="btn btn-outline-warning doc_action btn-mini form-group" style="margin-bottom:0px;"  href="'+host+'/edit_document.php?id='+obj.encrypted_case_id+'" title="Edit"><i class="feather icon-edit"></i></a>';
  //     if(obj.title == 'doc'){
  //      tr+= '<a class="btn btn-outline-warning doc_action btn-mini form-group" href="'+obj.doc_path+'" title="Download" style="margin-bottom:0px;" download><i class="feather icon-eye"></i></a>'; 
  //    }else{
  //     tr+= ' <button title="Download" type="button" class="doc_action viewBtn btn btn-outline-info btn-mini form-group" ><a href="'+getDocUrl(obj.doc_path)+'" class="viewDoc" target="_blank" download><i class="feather icon-download"></i></a></button>';
  //   }
  //   tr+= ' <button type="button" onclick="deleteDoc('+obj.doc_id+')" title="Delete" class="doc_action btn btn-outline-danger btn-mini form-group"><i class="feather icon-trash"></i></button></td>';
  // });

    // if(tr !=''){
    //   var table = '<table class="table table-bordered" id="documentList-table">';
    //   table +='<thead><tr><th>Title</th><th>Size</th><th>Type</th><th>Uploaded By</th><th>Uploaded Date</th><th class="action_th">Action</th></tr></thead><tbody>';
    //   table+= tr +'</tbody></table>';
    // }
    // $("#documentList").html(table);
    // $("#documentList-table").dataTable();
  },error:function(){
    toastr.error("",'Error in uploading document',{timeout:5000});
     $(".add_casesubmitdiv").removeClass("fetchdataloader");
      $(".add_casesubmit").show();
  }
});
}

function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

 function getDocUrl(doc_name){
        if(!isEmpty(doc_name)){
         // var host=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
      // $('#pdfUrl').attr('href',host+"/casepdf"+response.pdfFile); 
      return host+'/upload_document/'+doc_name;
    }
    else
     return 'images/business+costume+male+man+office+user+icon-1320196264882354682.png';
 }
</script>







