  <?php include("header.php");?>
  <?php include("menu.php");?>
  
  <link rel="stylesheet" href="/admin/css/vertical-layout-light/add_cases.css">
  <script src="js/jquery-1.12.4.js"></script>

  <script src="js/aes.js"></script>
  <script src="js/pbkdf2.js"></script>
  <style type="text/css">
  .fetchdataloader {
  	border: 16px solid #f3f3f3;
  	border-radius: 50%;
  	border-top: 16px solid #3498db;
  	width: 60px;
  	float: right;
  	height: 60px;
  	-webkit-animation: spin 2s linear infinite; /* Safari */
  	animation: spin 2s linear infinite;
  }
  /* Safari */
  @-webkit-keyframes spin {
  	0% { -webkit-transform: rotate(0deg); }
  	100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
  	0% { transform: rotate(0deg); }
  	100% { transform: rotate(360deg); }
  }
  /*.no-bordered tr td {
    padding: 1%;
    }*/
</style>

<body>
	<div class="main-panel">        
		<div class="content-wrapper">
			<?php $case = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
			$selcase = mysqli_fetch_array($case);?>
			<?php include("slider.php");?>
			<input type="hidden" name="caseid" id="caseid" value="<?php echo base64_decode($_GET['caseid']);?>">
			<input type="hidden" name="courtid" id="court-id" value="<?php echo $selcase['court_id'];?>">
			<div class="row">
				<div class="col-md-12 grid-margin stretch-card">
					<div class="card form-group">
						<div class="card-body">
							<h2 class="card-title">Edit Case / प्रकरण संपादित करा  </h2>
							<hr/>
							<form class="forms-sample" method="POST" autocomplete="off">
								<div class="form-group row">
									<label for="exampleInputUsername1" class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Court / न्यायालय</b></label>
									<div class="col-sm-8">
										<select id="cases-court_id" class="form-control" name="court_id">
											<option value="">Please select / कृपया निवडा</option>
											<?php $courtname = mysqli_query($connection, "SELECT * FROM `court_list` ORDER BY court_id ASC");
											while($selcourtlist = mysqli_fetch_array($courtname)){?>
												<option value="<?php echo $selcourtlist['court_id'];?>"><?php echo $selcourtlist['court_name'];?></option>
											<?php } ?>
											<option value="9">Other</option>
										</select>
									</div>
								</div>

     
                <div class="form-group row" id="appearing">
                	<label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                		<b>Is the affidavit filed? / प्रतिज्ञापत्र दाखल आहे का?</b> <!-- <span style="color: red;">*</span> --></label>
                		<div class="col-sm-3">
                			<div class="form-check">
                				<label class="form-check-label checkboxlabel">
                					<input type="radio" class="chkPassport" name="affidaviteRadios" id="affidavitYes" onclick="affidavitShow('Yes')" value="Yes">
                					Yes / होय
                				</label>
                			</div>

                		</div>


                      <div class="col-sm-12 grid-margin stretch-card">
                      	<div class="card">
                      		<div class="card-body">
                      			<div class="mt-4">
                      				<div class="accordion accordion-solid-header" id="accordion-4" role="tablist">
                      					<div class="card border-bottom"><div class="card-header" role="tab" id="heading-10"><h6 class="mb-0"><a data-toggle="collapse" href="#collapse-10" aria-expanded="true" aria-controls="collapse-10">Your Advocates (<span id="appearing_as_first">Petitioner</span>) / आपले वकील (याचिकाकर्ता)</a>
                      					</h6></div>
                      					<div id="collapse-10" class="collapse show" role="tabpanel" aria-labelledby="heading-10" data-parent="#accordion-4">
                      						<div class="card-body">
                      							<div class="container">
                      								<table id="petitioneradv_table" class=" table order-list" width="100">
                      									<thead>
                      										<tr>
                      											<td>Full Name</td>
                      											<td>Email Address</td>
                      											<td>Phone Number</td>
                      											<td>Action</td>
                      										</tr>
                      									</thead>
                      									<tbody>
                      										<tr>
                      											<td class="col-sm-12"> 
                      												<input type="text" name="name" id="your_adv_name0" class="form-control" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      												<input type="mail" name="mail" id="your_adv_email0" class="form-control" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      												<input type="text" name="phone" id="your_adv_mobile0" class="form-control" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">

                      											</td>
                      										</tr>
                      									</tbody>
                      									<tfoot>
                      										<td class="col-sm-12" colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button" id="petitioner_advocate_add" onclick="addPetitionerAdv()"> Add More </button>
                      										</td>
                      									</tfoot> 
                      								</table>
                      							</div>
                      						</div>
                      					</div>
                      				</div>
                      				<div class="card border-bottom" id="petitioner_div">
                      					<div class="card-header" role="tab" id="heading-13">
                      						<h6 class="mb-0">
                      							<a class="collapsed" data-toggle="collapse" href="#collapse-13" aria-expanded="false" aria-controls="collapse-11">
                      								Opponents (<span class="appearing_as_second">Petitioner</span>)
                      							</a>
                      						</h6>
                      					</div>
                      					<div id="collapse-13" class="collapse" role="tabpanel" aria-labelledby="heading-13" data-parent="#accordion-4">
                      						<div class="card-body">
                      							<div class="container">
                      								<table id="petitioner_table" class="table order-list1" width="100">
                      									<thead>
                      										<tr>
                      											<td>Full Name</td>
                      											<td>Email Address</td>
                      											<td>Phone Number</td>
                      											<td>Action</td>
                      										</tr>
                      									</thead>
                      									<tbody>
                      										<tr>
                      											<td class="col-sm-12"> 
                      												<input type="text" name="name" class="form-control"  id="your_name0" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      												<input type="mail" name="mail"  class="form-control" id="your_email0" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      												<input type="text" name="phone"  class="form-control" id="your_mobile0" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      											</td>
                      										</tr>
                      									</tbody>      
                      									<tfoot>

                      										<td class="col-sm-12" colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button" onclick="addPetitioner()" id="petitioner_add"> Add More </button>
                      										</td>
                      									</tfoot>                              
                      								</table>
                      							</div>
                      						</div>
                      					</div>
                      				</div> 

                      				<div class="card border-bottom" id="acc_petition_id">
                      					<div class="card-header" role="tab" id="heading-11">
                      						<h6 class="mb-0">
                      							<a class="collapsed" data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">
                      								Opponents (<span class="appearing_as_second">Respondent</span>) / प्रतिस्पर्धी वकिल (प्रतिवादी)
                      							</a>
                      						</h6>
                      					</div>
                      					<div id="collapse-11" class="collapse" role="tabpanel" aria-labelledby="heading-11" data-parent="#accordion-4">
                      						<div class="card-body">
                      							<div class="container">
                      								<table id="repondant_table" class="table order-list1" width="100">
                      									<thead>
                      										<tr>
                      											<td>Full Name</td>
                      											<td>Email Address</td>
                      											<td>Phone Number</td>
                      											<td>Action</td>
                      										</tr>
                      									</thead>
                      									<tbody>
                      										<tr>
                      											<td class="col-sm-12"> 
                      												<input type="text" name="name" class="form-control"  id="opponent_name0" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      												<input type="mail" name="mail"  class="form-control" id="opponent_email0" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      												<input type="text" name="phone"  class="form-control" id="opponent_mobile0" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-12">
                      											</td>
                      										</tr>
                      									</tbody>      
                      									<tfoot>

                      										<td class="col-sm-12" colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button" onclick="addRespondant()" id="respondant_add"> Add More </button>
                      										</td>
                      									</tfoot>                              
                      								</table>
                      							</div>
                      						</div>
                      					</div>
                      				</div>
                      				

                      				<div class="card">
                      					<div class="card-header" role="tab" id="heading-12">
                      						<h6 class="mb-0">
                      							<a class="collapsed" data-toggle="collapse" href="#collapse-12" aria-expanded="false" aria-controls="collapse-12">
                      								Opponent Advocates (<span class="appearing_as_second">Respondent</span>) / प्रतिस्पर्धी वकिल (प्रतिवादी)
                      							</a>
                      						</h6>
                      					</div>
                      					<div id="collapse-12" class="collapse" role="tabpanel" aria-labelledby="heading-12" data-parent="#accordion-4">
                      						<div class="card-body">
                      							<div class="container">
                      								<table id="respondant_adv_table" class=" table order-list" width="100">
                      									<thead>
                      										<tr>
                      											<td>Full Name</td>
                      											<td>Email Address</td>
                      											<td>Phone Number</td>
                      											<td>Action</td>
                      										</tr>
                      									</thead>
                      									<tbody>
                      										<tr>
                      											<td class="col-sm-4">

                      												<input type="text" name="name"  id="opponent_adv_name0" class="form-control" autocomplete="off" />
                      											</td>
                      											<td class="col-sm-4">
                      												<input type="mail" name="mail"  id="opponent_adv_email0"  class="form-control" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-4">
                      												<input type="text" name="phone"  id="opponent_adv_mobile0"  class="form-control" autocomplete="off"/>
                      											</td>
                      											<td class="col-sm-2">
                      											</td>
                      										</tr>
                      									</tbody>  
                      									<tfoot>
                      										<td colspan="4" style="    text-align: right;"><button type="button" class="add-opponent add-more-button" onclick="respondantAdvAdd()" id="respondant_advocate_add"> Add More  / आणखी जोडा</button></td>
                      									</tfoot>                                
                      								</table>
                      							</div>
                      						</div>
                      					</div>
                      				</div>
                      			</div>
                      		</div>
                      	</div>
                      </div>
                  </div>
                 <!--   <div>
             <div id='translControl'></div>
    <br/><input  type="text" id="txtEngToHindi"/>
    <br /><input type="text" id="txtInEnglish"/>
</div> -->
<!-- <input type="submit" class="btn btn-primary mr-2" name="add_case" value="Submit"> -->
<button class="btn  mr-2 newsubmit btn-sm" name="add_case" type="button" onclick="submitCase()">Submit / समावेश करा </button>
<button class="btn btn-danger btn-sm" type="button"><a href="view_case.php?caseid=<?php echo($_GET['caseid']) ?>" style="color:white">Cancel / रद्द करा </a></button>
</form>
</div>
</div>
</div>
</div>
<?php include("footer.php");?>
</div>


</div>

</div>

<!-- main-panel ends -->

</div>
<!-- page-body-wrapper ends -->
</div>

<!-- <?php ?> -->


<link rel="stylesheet" href="css/vertical-layout-light/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" type="text/css">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script> -->

	<div class="modal fade" id="client" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-2" aria-hidden="true">
		<div class="modal-dialog modal-xs" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel-2"><b style="color: red;">Add Client</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="forms-sample">
						<div class="form-group row">
							<label for="exampleInputUsername2" class="col-sm-3 col-form-label">Full Name</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="exampleInputUsername2" placeholder="Full Name" autocomplete="off">
							</div>
						</div>
						<div class="form-group row">
							<label for="exampleInputEmail2" class="col-sm-3 col-form-label">Email Address</label>
							<div class="col-sm-9">
								<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email Id" autocomplete="off">
							</div>
						</div>
						<div class="form-group row">
							<label for="exampleInputMobile" class="col-sm-3 col-form-label">Mobile Number</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="exampleInputMobile" placeholder="Mobile number" autocomplete="off">
							</div>

						</div>
						<div class="modal-footer">
							<input type="button" class="btn btn-success" value="Submit">
							<button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div> 
	</div>  

	<div class="modal" id="data">
		<div class="modal-dialog modal-lg">

			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><b>Please Map the Following Details</b></h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body" style="padding: 0px;" id="modal-data">

					<!-- <form method="POST" action="submitdata.php"> -->



					</div>
					
				</div>

			</div>
		</div>
<!-- <script src='select2/dist/js/select2.min.js' type='text/javascript'></script>
	<link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'> -->
 <!--  <script type="text/javascript">
    function datafetch() {
    $('#result').html('<img src="images/AGNB.gif">');
        $.ajax({
          type: "Post",
          url: "employee.php",
          success: function(data) {
            var obj = $.parseJSON(data);      
            var result = "<ul>"
            $.each(obj, function() {
            result = result + "<li>First Name : " + this['firstname'] + " , Last Name : " + this['lastname'] + "</li>";
           });
          result = result + "</ul>"
          $("#result").html(result);
        }
   }); 
}
</script> -->
<!-- <script type="text/javascript" src="js/add_cases.js"></script> -->
<script type="text/javascript" src="js/jsapi"></script>
<script type="text/javascript" src="js/edit_case.js"></script>
<style type="text/css">

</style>
<div class="modal" id="data">
	<div class="modal-dialog ">
		<div id="response"></div>
	</div>
</div>

<div class="modal" id="anycondition">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Search</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body row">

				<input type="hidden" id="state_code" name="">
				<input type="hidden" id="court_code" name="">
				<input type="hidden" id="dist_code" name="">
				<input type="hidden" id="court_type" name="">
				<input type="hidden" id="stamp_id" name="">
				<form style="width: 100%;" id="fetch_case_modal">
					<div class="col-sm-12 form-group row">
						<div class="col-sm-3" id="appeal-type-div">
							<label>Appeal Type</label>
							<select class="form-control select" id="appeal_type"></select>
						</div>
						<div class="col-sm-3">
							<label>Appeal No</label>
							<input type="text" class="form-control" name="" id="appealno" autocomplete="off"> 
						</div>
						<div class="col-sm-3">
							<label>Appeal Year</label>
							<input type="text" class="form-control" name="" id="appeal_year" autocomplete="off"> 
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<div class="or_seperator_line">
							<span class="or_seperator">OR</span>
						</div>
					</div>
					<div class="col-sm-12 form-group row">
						<div class="col-sm-6">
							<label>Party (Respondent / Petitioner)</label>
							<input type="text" class="form-control" id="party_name_search" name="" autocomplete="off">
						</div>  
						<div class="col-sm-4" id="registration-year">
							<label> Party Registration Year </label>
							<input type="text" class="form-control" id="party_year" name="" autocomplete="off">
						</div>
						<div class="col-sm-2 form-group" style="display: flex; align-items: flex-end;">
							<button class="btn btn-success btn-sm" id="fetch_data_button" type="button" onclick="fetchDatafromApi()">Fetch data</button>
							<div id="loader-div" style=""></div> 
						</div>  

					</div>
				</form>

				<div class="col-sm-12 form-group table-responsive" id="fetch_result_table">

				</div>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
			</div>

		</div>

	</div>

