<link rel="stylesheet" type="text/css" href="styles/jquery.steps.css">
<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> 
<body>
<?php $pageNam =  basename($_SERVER["PHP_SELF"]);
$insert = mysqli_query($connection, "INSERT INTO `frequency_report` SET `user_id`='".$_SESSION['user_id']."',`case_id`='".base64_decode($_GET['caseid'])."',`division`='".$_SESSION['cityName']."',`page_name`='".$pageNam."',`date_time`='".date("Y-m-d H:i:s")."',`dates`='".date("Y-m-d")."'");?>
	<?php $case = mysqli_query($connection, "SELECT `case_id`,`case_no`,`court_id`,`case_title`,`user_id` FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
	$selcase = mysqli_fetch_array($case);
	$get_cases = mysqli_query($connection,"SELECT * FROM `case_law_search` WHERE `case_id`='".base64_decode($_GET['caseid'])."' AND `remove_status`=1");
	$count_law = mysqli_num_rows($get_cases);
	$todo_cases = mysqli_query($connection,"SELECT DISTINCT`todo_id` FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' ORDER BY todo_id DESC");
	$cnt_todocases = mysqli_num_rows($todo_cases);?>
	<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
	<?php include("menu.php") ?>
	<div class="pcoded-content">
	<div class="pcoded-inner-content">
	<div class="main-body">
		<div class="page-wrapper">
		    <div class="page-header m-b-10"><h5 class="card-title">Add <?php if($_SESSION['account'] == 'Individual'){ echo "Client";}else{?>Advocate<?php } ?> &nbsp; &nbsp;<a href="manage-client.php" class="btn-primary btn-sm">Manage <?php if($_SESSION['account'] == 'Individual'){ echo "Client";}else{?>Advocates<?php } ?></a></h5>
			 <div class="page-body">
				<div class="form-group">
				  <div class="card-block col-sm-12">
					<div id="wizard">
						<section>
						<form class="wizard-form" id="example-advanced-form" method="POST" action="submit_advocate.php">
						<h3> Personal Details </h3>
						<fieldset>
						<div class="form-group row">
						<label for="exampleInputUsername2" class="col-sm-2 col-form-label"><b>Full Name <span class="asterisk">*</span>&nbsp;: </b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="full_name" placeholder="Full Name" name="full_name" required=""></div>
					    <label for="exampleInputEmail2" class="col-sm-2 col-form-label"><b>Email Address:</b></label>
						<div class="col-sm-4">
						<input type="email" class="form-control" id="email" placeholder="Email Address" name="email"></div></div>
					    <div class="form-group row">
						<label for="exampleInputMobile" class="col-sm-2 col-form-label"><b>Phone Number:</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="mobile" placeholder="Phone Number" name="mobile" maxlength="10" minlength="10" onkeypress="return onlyNumberAllowed(event)"></div>  
						<label for="exampleInputPassword2" class="col-sm-2 col-form-label"><b>Age</b></label>
						<div class="col-sm-4"><input type="text" class="form-control" id="age" placeholder="Age" name="age" maxlength="2" minlength="2" onkeypress="return onlyNumberAllowed(event)" ></div></div>
						<div class="form-group row">
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Father's Name:</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="father_name" placeholder="Father's Name" name="father_name"></div>
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Company Name:</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="company_name" placeholder="Company Name" name="company_name"></div>
						</div>
					    <div class="form-group row">
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Website:</b></label>
				        <div class="col-sm-4">
						<input type="text" class="form-control" id="website" placeholder="Website" name="website"></div>
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>TIN#:</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="tin" placeholder="TIN" name="tin"></div>
						</div>
						<div class="form-group row">
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>GST Identification Number (GSTIN):</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="gst" placeholder="GST Identification Number (GSTIN)" name="gst" maxlength="15">
						</div>
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Permanent Account Number (PAN):</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="pan_no" placeholder="Permanent Account Number (PAN)" name="pan_no">
						</div>
						</div>
						<div class="form-group row">
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Hourly Rate (INR):</b></label>
						<div class="col-sm-10">
						<input type="text" class="form-control" id="hourly_rate" placeholder="Hourly Rate (INR)" name="hourly_rate">
						</div>
						</div>
				</fieldset>
						<h3> Office Address </h3>
				<fieldset>
						<div class="form-group row">
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 1:</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="office_line_1" placeholder="Address Line 1" name="office_line_1">
						</div>
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 2:</b></label>
						<div class="col-sm-4">
						<input type="text" class="form-control" id="office_line_2" placeholder="Address Line 2" name="office_line_2">
						</div>
						</div>
						<div class="form-group row">
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Country:</b></label>
						<div class="col-sm-4">
				<select name="country_office" class="form-control" id="country">
						<option value="<?php echo $_POST['country_office'];?>">Select Country</option>
						<option value="India">India</option>
				</select>
                      </div>
					<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>State:</b></label>
					<div class="col-sm-4">
						<select name="office_state" class="form-control" id="state">
							<option value="<?php echo $_POST['office_state'];?>"><?php if($_POST['office_state']!=""){ echo $_POST['office_state']; }else{?>Select State<?php } ?></option>
							<?php $geolocation = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `parent_id`=0");
							while($sellocation = mysqli_fetch_array($geolocation)){ ?>
								<option value="<?php echo $sellocation['id'];?>"><?php echo $sellocation['name'];?></option>
							<?php } ?>
						</select>
					</div>
					</div>
					<div class="form-group row">
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>City:</b></label>
						<div class="col-sm-4">
							<select id="cityadv" name="office_city" class="form-control">
								<option value="<?php echo $_POST['office_city'];?>"><?php if($_POST['office_city']!=""){ echo $_POST['office_city']; }else{?>Select State first<?php } ?></option>
							</select>
						</div>
						<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Zip/Postal Code:</b></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="office_zip" placeholder="Zip/Postal Code" name="office_zip"> 
						</div>
					</div>
					</fieldset>

         <h3> Home Address </h3>
		 <fieldset>
			<div class="form-group row">
				<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 1:</b></label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="home_line_1" placeholder="Address Line 1" name="home_line_1">
				</div>
				<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address Line 2:</b></label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="home_line_2" placeholder="Address Line 2" name="home_line_2">
				</div>
			</div>
			<div class="form-group row">
				<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Country:</b></label>
				<div class="col-sm-4">
					<select name="home_country" class="form-control">
						<option value="">Select Country</option>
						<option value="India">India</option>
					</select>
				</div>
				<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>State:</b></label>
				<div class="col-sm-4">
					<select name="home_state" class="form-control" id="state1">
						<option value="">Select State</option>
						<?php $geolocation = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `parent_id`=0");
						while($sellocation = mysqli_fetch_array($geolocation)){ ?>
							<option value="<?php echo $sellocation['id'];?>"><?php echo $sellocation['name'];?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>City:</b></label>
				<div class="col-sm-4">
					<select  name="home_city" class="form-control" id="cityadv1">
						<option value="">Select State first</option>
					</select>
				</div>
				<label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Zip/Postal Code:</b></label>
				<div class="col-sm-4">
					<input type="text" class="form-control" id="home_zip" placeholder="Zip/Postal Code" name="home_zip">
				</div>
			</div>
		</fieldset>
        <h3> Point of Contacts </h3>
		<fieldset>
			<table id="myTable" class=" table order-list advocatelist" width="100">
				<thead>
					<tr>
						<td>Full Name</td>
						<td>Email Address</td>
						<td>Phone Number</td>
						<td>Designation</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<input type="text" name="fullname[]" class="form-control" value="" />
						</td>
						<td>
							<input type="mail" name="email_id[]"  class="form-control" value="" />
						</td>
						<td >
							<input type="text" name="contact_no[]"  class="form-control" value="" />
						</td>
						<td>
							<input type="text" name="designation[]"  class="form-control" value="" />
						</td>
						<td ><a class="deleteRow"></a>
							<button type="button" class="btn btn-primary btn-sm add-opponent add-more-button"  id="addrow"> Add More </button>
						</td>
					</tr>
				</tbody>
			</table>
		</fieldset>
</form>
</section>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php include 'footer.php'; ?>

<script type="text/javascript">
	$(".add-client-li").addClass('active pcoded-trigger');
</script>
<script src="scripts/jquery.steps.js" type="text/javascript"></script>
<script src="scripts/jquery.validate.js" type="text/javascript"></script>
<script src="scripts/form-wizard.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/add-advocate.js"></script>

<script>
$(document).ready(function(){
    $(document).on('change','#country', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate.php',
                data:'country_id='+countryID,
                success:function(html){
                    $('#state').html(html);
                     $("#state").select2();
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
           
        }
    });
    $(document).on('change','#state', function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate.php',
                data:'state_id='+stateID,
                success:function(html){
                    $('#cityadv').html(html);
                    $("#cityadv").select2();
                }
            }); 
        }else{
        }
    });



    $(document).on('change','#country1', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate1.php',
                data:'country_id='+countryID,
                success:function(html){
                     $('#state1').html(html);
                     $("#state1").select2();
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
        }
    });


$(document).on('change','#state1', function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData_advocate1.php',
                data:'state_id='+stateID,
                success:function(html){
                    $('#cityadv1').html(html);
                    $("#cityadv1").select2();
                }
            }); 
        }else{
        }
    });
  
});
</script>








