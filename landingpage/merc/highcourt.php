<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
      <?php include("menu.php");?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="card">
            <div class="card-body">
              <h4 class="card-header" style="padding-top: 0px"><b style="color: green;">Import Cases</b></h4>
              <div class="row" style="margin-top: 25px;text-align: center;">
                <div class="col-12">
                  <div class="form-group row" id="bench">
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Bench</b></label>
                    <div class="col-sm-8">
                      <select  class="form-control highcourtidslist " name="Cases[hc_bench_id]" id="high_court_bench_list">
                        <option value="">Please select / कृपया निवडा</option>
                        <?php $bench = mysqli_query($connection, "SELECT * FROM `bench` ORDER BY `bench_name` ASC");
                              while($selbench = mysqli_fetch_array($bench)){?>
                              <option value="<?php echo $selbench['bench_id'];?>"><?php echo $selbench['bench_name'];?></option>
                              <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row" style="display: none;" id="side"> 
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Side</b></label>
                    <div class="col-sm-8">
                      <select id="high_court_side_list" class="form-control sideclass " name="Cases[high_court_id]">
                        <option value="">Please select / कृपया निवडा</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row" style="display: none;" id="searchby"> 
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Search By</b></label>
                    <div class="col-sm-8">
                      <select id="import-type" class="form-control" name="Import[type]" aria-invalid="false">
                        <option value="">Please select</option>
                        <option value="byadvname">Advocate Name</option>
                        <option value="bypartyname">Party Name</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row" style="display: none;" id="partyname"> 
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Party Name</b></label>
                    <div class="col-sm-8">
                      <select id="import-type" class="form-control" name="Import[type]" aria-invalid="false">
                      </select>
                    </div>
                  </div>
                  <div class="form-group row" style="display: none;" id="year"> 
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Year</b></label>
                    <div class="col-sm-8">
                      <select id="year" class="form-control" name="Import[type]" aria-invalid="false">
                      </select>
                    </div>
                  </div>
                  <div class="form-group row" style="display: none;" id="advocatename"> 
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Advocate Name</b></label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="">
                    </div>
                  </div>
                  <div class="form-group row" style="display: none;" id="appearing"> 
                    <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>Petitioner/Respondent</b></label>
                    <div class="col-sm-8">
                      <select id="import-appearing_as" class="form-control" name="Import[appearing_as]" aria-invalid="true">
                        <option value="">Please select</option>
                        <option value="0">Petitioner</option>
                        <option value="1">Respondent</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php include("footer.php");?>
<script type="text/javascript">
  var year = 1950;
var till = 2019;
var options = "";
for(var y=year; y<=till; y++){
  options += "<option>"+ y +"</option>";
}
document.getElementById("year").innerHTML = options;
</script>

<script>
$(document).ready(function(){
    $('#high_court_bench_list').on('change', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'data/pulldata.php',
                data:'high_court_bench_lists='+countryID,
                success:function(html){
                 $('#side').show();

                    $('#high_court_side_list').html(html);
                }
            }); 
        }
    });
    
    $('#high_court_side_list').on('change', function(){
        var stateID = $(this).val();
         $('#searchby').show();
        
    });
});
</script>