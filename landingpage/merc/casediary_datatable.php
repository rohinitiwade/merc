<?php
## Database configuration
include 'includes/dbconnect.php';

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = $_POST['search']['value']; // Search value

$from_datepicker = $_GET['from_datepicker'];
$to_datepicker = $_GET['to_datepicker'];

## Search 
$searchQuery = " ";
if($searchValue != ''){
   $searchQuery = " and (court_name like '%".$searchValue."%' or 
		case_no like '%".$searchValue."%' or
		case_title like '%".$searchValue."%' or
		team_member like '%".$searchValue."%' or
		case_description like '%".$searchValue."%' or
		hearing_date like '%".$searchValue."%' or
		team_member like '%".$searchValue."%' ) AND
		(hearing_date BETWEEN '".$from_datepicker."' AND '".$to_datepicker."' ) ";
}else{
	$searchQuery = " and (
		hearing_date BETWEEN '".$from_datepicker."' AND '".$to_datepicker."' ) ";
}


## Total number of records without filtering
$sel = mysqli_query($connection,"select count(*) as allcount from reg_cases WHERE 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of record with filtering
$sel = mysqli_query($connection,"select count(*) as allcount from reg_cases WHERE 1 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "select * from reg_cases WHERE 1 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = mysqli_query($connection, $empQuery);

$data = array();
while ($rows = mysqli_fetch_assoc($empRecords)) {	
	$data[] = array(
	  "case_id"=>$rows['case_id'],
	  "check_box"=>"<input type='checkbox' id='checkbox_".$rows['case_id']."' class='check' name='checkAll[]' data-id='".$rows['case_id']."' />",
	  	  
      "court_name"=>$rows['court_name'],
      "case_no"=>$rows['hc_case_type']." ".$rows['case_no']." / ".$rows['case_no_year'],
      "case_title"=>$rows['case_title'],
      "team_member"=>$rows['team_member'],
      "hearing_date"=>$rows['hearing_date'],
      "last_hearing_date"=>"",
      "stage"=>"",
      "case_description"=>$rows['case_description'],
	  "CNR"=>"<span id='cnr_id_".$rows['case_id']."'>".$rows['CNR']."</span>",
   );
   //print_r($data);
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecordwithFilter,
  "iTotalDisplayRecords" => $totalRecords,
  "aaData" => $data
);
//print_r($response);
echo json_encode($response);