<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<style type="text/css">
.fetchdataloader {
	border: 16px solid #f3f3f3;
	border-radius: 50%;
	border-top: 16px solid #3498db;
	width: 60px;
	float: right;
	height: 60px;
	-webkit-animation: spin 2s linear infinite; /* Safari */
	animation: spin 2s linear infinite;
}
#sc_info{
	color: red;
	width: 100%;
	float: left;
	/*text-align: right;*/
	/*position: absolute;*/
	/*z-index: 1;*/
	/*top: 54px;*/
	display: none;
}
.full-name{
	width: 55%;
}
.table{
	margin-bottom: 0px;
}
.fetchdata-dialog{
	max-width:80%;
}
#submit_data{
	text-align: right;
}
/* Safari */
@-webkit-keyframes spin {
	0% { -webkit-transform: rotate(0deg); }
	100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
	0% { transform: rotate(0deg); }
	100% { transform: rotate(360deg); }
}
</style>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>

			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class="card-title">Edit Case </h5>
							</div>

							<div class="page-body">
								<div class="card form-group">
									<!-- <div class="card-header ">
									</div> -->
									<input type="hidden" id="state_code" name="">
									<input type="hidden" id="court_code" name="">
									<input type="hidden" id="dist_code" name="">
									<input type="hidden" id="court_type" name="">
									<input type="hidden" id="stamp_id" name="">
									<?php $case = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
									$selcase = mysqli_fetch_array($case);?>
									<?php include("slider.php");?>
									<input type="hidden" name="caseid" id="caseid" value="<?php echo base64_decode($_GET['caseid']);?>">
									<input type="hidden" name="courtid" id="court-id" value="<?php echo $selcase['court_id'];?>">
									<div class="card-block">
										<form class="forms-sample row" method="POST" autocomplete="off">
											<div class="form-group col-sm-6 row">
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Court</label>
												<div class="col-sm-8">
													<select id="cases-court_id" class="form-control" name="court_id" disabled>
														<option value="">Please select</option>
														<?php $courtname = mysqli_query($connection, "SELECT * FROM `court_list` ORDER BY court_id ASC");
														while($selcourtlist = mysqli_fetch_array($courtname)){?>
															<option value="<?php echo $selcourtlist['court_id'];?>"><?php echo $selcourtlist['court_name'];?></option>
														<?php } ?>
														<!-- <option value="9">Other</option> -->
													</select>
												</div>
											</div>
											<div id="1" class="court-wise-div 1 col-sm-6 form-group row" style="display: none;">
												
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Supreme Court of India </label>
												<div class="col-sm-8">
													<select id="supreme_court" class="form-control select" name="cases_diary_no[]">
														<option value="">Please Select</option>
														<option value="Case Number">Case Number</option>
														<option value="Diary Number">Diary Number</option>
													</select>
												</div>
												
											</div>

											<div class="form-group col-sm-6 row 1" style="display: none;">
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp;Supreme Court of India Details</label>
												<div class="col-sm-8">
													<textarea class="form-control" id="supreme-court-details" rows="3"></textarea>
												</div>
											</div>

											<div id="supreme_court_casetype"  class="court-wise-div form-group row col-sm-6" style="display: none;">
												
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Case Type</label>
												<div class="col-sm-8">
													<div class="col-sm-12 form-group p-0" style="float: left;">
														<select id="cases_case_type" class="form-control form-group" name="case_type" data-pullbtnflag="0" style="width: 100%" style="background-color: #353eb3; color: white;" aria-invalid="true">
															<option value="">Please select</option>
															<?php $casetype = mysqli_query($connection, "SELECT * FROM `case_type` ORDER BY case_type ASC");
															while($selcasetype = mysqli_fetch_array($casetype)){?>
																<option value="<?php echo $selcasetype['master_id'];?>"><?php echo $selcasetype['case_type'];?></option>
															<?php } ?>
															<option value="0">Other</option>
														</select>
													</div>
													<input type="text" class="form-control" id="sc_casetype_other" name="">
												</div>
												
											</div>
											<div id="supreme_court_diary" class="court-wise-div form-group row col-sm-6" style="display: none;">												
												<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Diary Number
													<br/>
													<span class="lbl-short-desc">(Please enter the diary number assigned by court)</span></label>
													<div class="col-sm-4">
														<input type="text" name="diary_number" id="diary_number" value="" class="form-control" autocomplete="OFF" placeholder="Enter Diary Number">
													</div>
													<div class="col-sm-4">
														<select name="year" id="diary_year" class="form-control ">
															<option value=''>Select Year</option>
															<?php $year = mysqli_query($connection, "SELECT * FROM `year` ORDER BY date_time DESC");
															while($yearsel = mysqli_fetch_array($year)){?>
																<option value='<?php echo $yearsel['date_time'];?>'><?php echo $yearsel['date_time'];?></option>
															<?php } ?>

														</select>
													</div>
												</div>
												<div class="form-group row col-sm-6" id="cnr_div" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
														<span style="color: red;"><b>*</b></span>Do you have CNR #?
													</label>
													<div class="col form-radio">
														<div class="radio radio-inline">
															<label>
																<input type="radio" class="chkPassport" name="membershipRadios" onclick="cnr_allow('Yes')" id="membershipRadios" value="Yes">
																<i class="helper"></i> Yes
															</label>
														</div>
														<div class="radio radio-inline">
															<label>
																<input type="radio" class="chkPassport"  name="membershipRadios" onclick="cnr_allow('No')" id="membershipRadios" value="No" >
																<i class="helper"></i> No
															</label>
														</div>
													</div>
												</div>


												<div id="2" class="court-wise-div 2 form-group row col-sm-6" style="display: none;">

													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;High court</label>
													<div class="col-sm-8 highcourtselection">
														<select id="high-court-list" disabled class="form-control " name="Cases[high_court_id]" onchange="getBenchList()">
															<option value="">Please select</option>
															<?php $highcourt= mysqli_query($connection,"SELECT * FROM `high_court_list` WHERE `court_id`=3 ORDER BY high_court_name ASC");
															while($seligh = mysqli_fetch_array($highcourt)){?>
																<option value="<?php echo $seligh['court_id']; echo","; echo $seligh['master_id']; ?>"><?php echo $seligh['high_court_name'];?></option>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class="form-group row col-sm-6 2" id="bench" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Bench</label>
													<div class="col-sm-8">
														<select disabled  class="form-control highcourtidslist " name="Cases[hc_bench_id]" id="high_court_bench_list" onchange="highcourt_side_list()">
															<option value="">Please select</option>

														</select>
													</div>
												</div>

												<div class="form-group row col-sm-6 2" id="highcourt_side" style="display: none;"> 
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Side</label>
													<div class="col-sm-8">
														<select disabled id="high_court_side_list" class="form-control sideclass " name="Cases[high_court_id]" onchange="highcourt_casetype_list()">
															<option value="">Please select</option>
														</select>
													</div>
												</div>
												<div class="form-group row col-sm-6 2" id="stamp_register" style="display: none;"> 
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Stamp/Register</label>
													<div class="col-sm-8">
														<select id="cases-high_court_id" class="form-control select" name="Cases[high_court_id]">
															<option value="">Please select</option>
															<option value="1">Register</option>
															<option value="2">Stamp</option>
														</select>
													</div>
												</div>

												<div class="form-group row col-sm-6 2" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp;High Court Details </label>
													<div class="col-sm-8">
														<textarea class="form-control" id="high-court-details" rows="3"></textarea>
													</div>
												</div>
												<div class="form-group row col-sm-6 2" id="highcourt_casetype" style="display: none;"> 
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Case Type</label>
													<div class="col-sm-8 highcourtselection">
														<div class="col-sm-12 form-group p-0" style="float: left;">
															<select id="high_court_casetype" class="form-control " name="Cases[high_court_id]">
																<option value="">Please select</option>
															</select>
														</div>
														<input type="text" class="form-control" id="hc_casetype_other" name="">
													</div>
												</div>
												<!-- High Court Div End -->
												<!-- District Court div Starts -->
												<div id="3" class="court-wise-div 3 col-sm-6 form-group row" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;State</label>
													<div class="col-sm-8 highcourtselection">
														<select name="state" id="districtcourt_state" class="form-control" onchange="buildDistrict()" disabled>
															<option value="">Please select</option>
															<?php $sates = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `location_type`='STATE' ORDER BY `name` ASC");
															while($selsstae = mysqli_fetch_array($sates)){ ?>
																<option value="<?php  echo $selsstae['id']; echo"_"; echo $selsstae['master_id'];?>"><?php  echo $selsstae['name'];?></option>
															<?php } ?>
														</select>
													</div>
												</div>
												<div  class=" 3 col-sm-6 form-group row" style="display: none;" >
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;District</label>
													<div class="col-sm-8">
														<select id="district_court_city" disabled name="district" class="form-control esta" onchange="districtcourt_city()">
															<option value="">Select State first</option>
														</select>
													</div>
												</div>
												<div  class=" 3 col-sm-6 form-group row" style="display: none;" >
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Court Establisment</label>

													<div class="col-sm-8">
														<div class="col-sm-12 p-0 form-group" style="float: left;">
															<select id="district_court_establishment" disabled name="court_establishment" class="form-control" onchange="districtcourt_establishment()">
																<option value="">Select District First</option>
															</select>
														</div>
														<!-- <input type="text" class="form-control" id="dc_establishment_other" name="" style="display: none;"> -->
													</div>
												</div>
												<div class=" 3 col-sm-6 form-group row" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp;District Court Details </label>
													<div class="col-sm-8">
														<textarea class="form-control" id="district-court-details" rows="3"></textarea>
													</div>
												</div>
												<div  class="3 col-sm-6 form-group row" style="display: none;" >
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Case Type</label>
													<div class="col-sm-8">
														<div class="form-group col-sm-12 p-0" style="float: left;">
															<select id="district_court_casetype" name="court_establishment" class="form-control">

															</select>
														</div>
														<input type="text" class="form-control" id="district_court_casetype_other" name="" style="display: none;">
													</div>
												</div>
												<!-- District court ends -->
												<!-- Consumer forum Starts -->
												<div id="4" class="court-wise-div 4 form-group col-sm-6 row" style="display: none;">
													<div id="commission_forum">                
														<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Commissions (Consumer Forum)</b></label>
														<div class="col-sm-8">
															<select name="commissions" id="commission" class="form-control">
																<option value="">Please select</option>
																<option value="District Forum">District Forum</option>
																<option value="National Commission - NCDRC">National Commission - NCDRC</option>
																<option value="State Commission">State Commission</option>
															</select>
														</div>
													</div>
												</div>

												<div id="NCDRC" class="form-group row col-sm-6" style="display: none;">

													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Bench</b></label>
													<div class="col-sm-8">
														<select name="state" id="commission_bench" class="form-control select">
															<option value="">Please select</option>
															<option value="New Delhi">New Delhi</option>
														</select>

													</div>
												</div>
												<div id="state_commission_div col-sm-6 4 form-group row" style="display: none;">


													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>State</b></label>
													<div class="col-sm-8">
														<select name="state" class="form-control" id="commission_state">                        
														</select>
													</div>
												</div>
												<div id="commission_state_div col-sm-6 4 form-group" style="display: none;">

													<div class="form-group row" id="gs" style="display: none;">
														<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>District</b></label>
														<div class="col-sm-8">
															<select name="district1" id="commission_district" class="form-control">
																<option value="">Please select</option>
																<option value="1">North Goa</option>
																<option value="2">South Goa</option>
															</select>
														</div>
													</div> 
													<div class="form-group row col-sm-6 " id="ms" style="display: none;">
														<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>District</b></label>
														<div class="col-sm-8">
															<select name="state" class="form-control select" onchange="mahastate(this.options[this.selectedIndex].value)">
																<option value="">Please select</option>
																<?php $District_court = mysqli_query($connection, "SELECT * FROM `District_court` ORDER BY dist_name ASC");
																while($selDistrict_court = mysqli_fetch_array($District_court)){?>
																	<option value="<?php echo $selDistrict_court['dist_id'];?>"><?php echo $selDistrict_court['dist_name'];?></option>
																<?php } ?>

															</select>
														</div>
													</div>
												</div>
												<div class="form-group row col-sm-6 4" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp;<b>Commissions (Consumer Forum) Details </b></label>
													<div class="col-sm-8">
														<textarea class="form-control" id="commissions-details" rows="3"></textarea>
													</div>
												</div>    



												<div class="form-group row col-sm-6" style="display: none;" id="circuit">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Case type</b></label>
													<div class="col-sm-8">
														<select name="state" class="form-control" id="commission_state_casetype">
															<option value="">Please select</option>
															<option value="1">APPEAL EXECUTION(AE)</option>
															<option value="2">APPEAL EXECUTION(ae)</option>
															<option value="3">Appeal(A)</option>
															<option value="4">Appeal(a)</option>
															<option value="5">Caveat Cases(CV)</option>
															<option value="6">Caveat Cases(cv)</option>
															<option value="7">consumer case(CC)</option>
															<option value="8">consumer case(cc)</option>
															<option value="9">Execution Application(EA)</option>
															<option value="10">Execution Application(ea)</option>
															<option value="11">EXECUTION REVISION PETITION(ERP)</option>
															<option value="12">EXECUTION REVISION PETITION(erp)</option>
															<option value="13">First Appeal(FA)</option>
															<option value="14">First Appeal(fa)</option>
															<option value="15">Interlocutory Application(IA)</option>
															<option value="16">Interlocutory Application(ia)</option>
															<option value="17">Miscellaneous Application(ma)</option>
															<option value="18">Miscellaneous Application(MA)</option>
															<option value="19">Review Application(ra)</option>
															<option value="20">Review Application(RA)</option>
															<option value="21">Revision Petition(rp)</option>
															<option value="22">Revision Petition(RP)</option>
															<option value="23">Transfer Application(ta)</option>
															<option value="24">Transfer Application(TA)</option>
															<option value="0">Other</option>                          
														</select>
														<input type="text" class="form-control" id="commision_casetype_other" name="" placeholder="Other" autocomplete="off">
													</div>

												</div>
												<!-- consumer forum ends here -->
												<!-- Tribunal Authority Starts -->
												<div id="5" style="display: none;" class="5 col-sm-6 court-wise-div form-group row">

													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>Tribunals & Authorites</label>
													<div class="col-sm-8">
														<select id="tribunal_select" name="district2" class="form-control select"  onchange="buildTribunalStateList()">
															<option value="">Please Select</option>
															<?php $Tribunals = mysqli_query($connection, "SELECT * FROM `tribunals` ORDER BY tribunals_name ASC");
															while($tribulanssel = mysqli_fetch_array($Tribunals)){?>
																<option value="<?php echo $tribulanssel['tribunals_id'];?>"><?php echo $tribulanssel['tribunals_name'];?></option>
															<?php } ?>
														</select>
													</div>
												</div>
												<div class="form-group row 5 col-sm-6" id="tribunal_state_div" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;State</label>
													<div class="col-sm-8">
														<select id="tribunal_state" class="form-control select" onchange="tribunalState()">
															<option value="">Please Select</option>
														</select>
													</div>
												</div>
												<div class="form-group row col-sm-6 " id="tribunal_bench_div" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Bench</label>
													<div class="col-sm-8">
														<select id="tribunal_bench" class="form-control select">
															<option value="">Please Select</option>
														</select>
													</div>
												</div>
												<div class="form-group row 5 col-sm-6" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Tribunals and Authorities Details </label>
													<div class="col-sm-8">
														<textarea class="form-control" id="tribunal-details" rows="3"></textarea>
													</div>
												</div>
												<div  class="form-group row 5 col-sm-6" id="case-type-tribunal" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Case Type</label>
													<div class="col-sm-8 form-group" id="tribunal_casetype">
														<select id="tribunal_case" class="form-control select">
														</select>
													</div>
													<div class="col-sm-4"></div>
													<div class="col-sm-8" id="other-case-type" style="display: none;">
													<input type="text" class="form-control" name="other" id="tribunal_other" autocomplete="off">
												</div>
												</div>
												<!-- <div  class="form-group row col-sm-6" id="other-case-type" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Other</label>
													<div class="col-sm-8">
														
													</div>

												</div> -->

												<!-- tribunals and authorities ends here -->
												<!-- Revenue Starts -->
												<div id="6" style="display: none;" class="court-wise-div form-group row 6 col-sm-6">

													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Revenue Court</label>
													<div class="col-sm-8">
														<select id="revenue_states" name="district5" class="form-control select">
															<option value="">Please Select</option>
															<option>Maharashtra</option>
														</select>
													</div>
												</div>
												<div class="form-group row 6 col-sm-6" style="display: none;">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Revenue Court Details </label>
													<div class="col-sm-8">
														<textarea class="form-control" id="revenue-details" rows="3"></textarea>
													</div>
												</div>
												<div  class="form-group row 6 col-sm-6" style="display: none;" >
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Case Type</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" id="revenue_other" placeholder="Other" name="" autocomplete="off">
													</div>
												</div>

												<!-- Revenue Ends -->

												<!--Commissionerate Starts  -->
												<div id="7" style="display: none;" class="court-wise-div form-group row col-sm-6 7">

													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Commissionerate</label>
													<div class="col-sm-8">
														<select name="state" id="commissionerate_select" class="form-control select">
															<option value="">Please select</option>
															<option value="Charity Commissionerate">Charity Commissionerate</option>
															<option value="Commissionerate of Commercial Taxes">Commissionerate of Commercial Taxes</option>
														</select>
													</div>
												</div>  
												<div class="form-group row col-sm-6 " style="display: none;" id="commissionerate_state_div">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;State</label>
													<div class="col-sm-8">
														<select name="state" id="commissionerate_state" class="form-control select">
															<option value="">Please select</option>
															<option value="Maharashtra">Maharashtra</option>
														</select>
													</div>
												</div>
												<div class="form-group row col-sm-6 " style="display: none;" id="courtside">
													<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Court Side ID</b></label>
													<div class="col-sm-8">
														<select name="state" class="form-control select" id="commisionrate_courtside">
															<option value="">Please Select</option>
															<?php $Court_Side = mysqli_query($connection, "SELECT * FROM `Court_Side` ORDER BY city_name ASC");
															while($selcourtlist=mysqli_fetch_array($Court_Side)){?>
																<option value="<?php echo $selcourtlist['court_sideId'];?>"><?php echo $selcourtlist['city_name'];?></option>
															<?php } ?>

														</select>
													</div>
												</div>
												<div class="form-group row col-sm-6 " id="commissionerate_authority" style="display: none;">
													<label class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Authority</b></label>
													<div class="col-sm-8"><select id="authority_select" class="form-control select">
														<option value="">Please select</option>
														<?php $authority = mysqli_query($connection, "SELECT * FROM `authority` ORDER BY authority_name ASC");
														while($authrisesel = mysqli_fetch_array($authority)){?>
															<option value="<?php echo $authrisesel['authority_id'];?>"><?php echo $authrisesel['authority_name'];?></option>
														<?php } ?>
             <!--  <option value="1">Additional Commissioner of Commercial Taxes</option>
              <option value="2">Assistant Commissioner of Commercial Taxes</option>
              <option value="3">Commercial Tax Officer</option>
              <option value="4">Commissioner of Commercial Taxes</option>
              <option value="5">Deputy Commissioner of Commercial Taxes</option>
              <option value="6">Joint Commissioner of Commercial Taxes</option>
              <option value="7">Local Goods and Service Tax Officer</option>
              <option value="8">Professional Tax Officer</option> -->
          </select>
      </div>
  </div>
  <div class="form-group row col-sm-6 7" style="display: none;">
  	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp;<b>Commissionerate Details </b></label>
  	<div class="col-sm-8">
  		<textarea class="form-control" id="commissionerate-details" rows="3"></textarea>
  	</div>
  </div>
  <div class="form-group row col-sm-6 " style="display: none;" id="courtsidecasetype">
  	<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Case Type</b></label>
  	<div class="col-sm-8">
  		<select name="state" id="commissionerate_casetype" class="form-control select">
  			<option value="">Please select</option>
  			<option value="419636">AppealOrCaveatApplication</option>
  			<option value="419634">ChangeReportApplication</option>
  			<option value="419635">SchemeChangeApplication</option>
  			<option value="419633">SocietyRegistrationApplication</option>
  			<option value="419632">TrustRegistrationApplication</option>
  			<option value="0">Other</option>
  		</select>
  	</div>
  </div>

  <!-- Commisionarate ends -->

  <div id="court_other" class="row form-group col-sm-6" style="display: none;">
  	<label class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Case Type</b></label>
  	<div class="col-sm-8">
  		<input type="text" class="form-control" id="court_other_input" placeholder="Other" name="" autocomplete="off">
  	</div>
  </div>


  <!-- <div class="form-group row col-sm-6" id="case_insert_show" style="display: none;">
  	<div id="casedetails"></div>
  </div> -->

  <div class="form-group row col-sm-6" id="caseandyear">
  	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Case Number
  		<br/>
  		<span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court</span></label>
  		<div class="col-sm-4">
  			<input type="text" name="case_no" id="case_no" disabled value="" class="form-control" autocomplete="OFF" placeholder="Enter Case Number">
  		</div>
  		<div class="col-sm-1">
  			<label for="exampleInputUsername1" class=" col-form-label" style="float: right;">Year</label>
  		</div>
  		<div class="col-sm-3">
  			<select name="year" id="case_no_year" class="form-control " disabled>
  				<option value=''>Select Year</option>
  				<?php $year = mysqli_query($connection, "SELECT * FROM `year` ORDER BY date_time DESC");
  				while($yearsel = mysqli_fetch_array($year)){?>
  					<option value='<?php echo $yearsel['date_time'];?>'><?php echo $yearsel['date_time'];?></option>
  				<?php } ?>

  			</select>
  		</div>
  	</div>

  	<div class="form-group col-sm-6 row" id="appearing_model" style="display: none;">
  		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Appearing  Model</label>
  		<div class="col-sm-8 highcourtselection">
  			<select name="state" id="appearing_modal" class="form-control appearing_modals select">
  				<!-- <option value="">Please select</option> -->
            <!-- <?php $appearing = mysqli_query($connection,"SELECT * FROM `appearing_model` ORDER BY appearing_modal ASC");
            while($selappering = mysqli_fetch_array($appearing)){?>
              <option value="<?php echo $selappering['app_id'];?>"><?php echo $selappering['appearing_modal'];?></option>
              <?php } ?> -->
          </select>
      </div>
  </div>
  <!--vilas C-- ajax-->
  <!-- <div id="appearmoda"></div> -->

  <div class="form-group row col-sm-6" id="areyouappearingas" style="display: none;">
  	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Are you appearing as?</label>
  	<div class="form-radio col">
  		<div class="radio radio-primary radio-inline">
  			<label id="appearing_radio_one_label">
  				<input type="radio" class="chkPassport" name="appearing_radio" onclick="getAppearingInput()" checked id="appearing_radio_one" value="Petitioner">

  				<i class="helper"></i>Petitioner
  			</label>
  		</div>
  		<div class="radio radio-primary radio-inline">
  			<label id="appearing_radio_two_label">
  				<input type="radio" class="chkPassport"  name="appearing_radio" onclick="getAppearingInput()" id="appearing_radio_two" value="Respondent">

  				<i class="helper"></i>Respondent
  			</label>
  		</div>
  	</div>

  </div>
  <div class="form-group row col-sm-6" id="appearingas_input" style="display: none;">
        	<!-- <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><b>Petitioner </b></label>
         <div class="col-sm-8 form-group">
           <input type="text" id="petitioner" class="form-control" value="" />
         </div>
         <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><b>Attach Document </b></label>
         <div class="col-sm-8">
          <input type="file" id="pet_file" aria-label="File browser example" style="margin-top: 4px;">
        </div>
        <div class="col-sm-8" id="otin" style="margin-left: 313px;padding: 0px;padding-right: 16px;">
          <button type="button" class=" btn btn-success btn-sm" style="float: right;"><i class="fa fa-plus" style="margin-right: 5px;"></i>Fetch Data </button>
      </div> -->
  </div>
  <div id="cnr_yes" class="col-sm-6 form-group row" style="display: none;">
  	<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;CNR</label>
  	<div class="col-sm-8 row">
  		<div class="col-sm-8"><input class="form-control" type="text" name="cnr_number" id="name" minlength="16" maxlength="16"> </div>
  		<div class="col-sm-2">
  			<div class="fetchLoaderDiv"></div>
  			<button type="button" class=" btn btn-success btn-sm" id="fetch_cnr_data_id" onclick="datafetch()"> Fetch Data </button>

  		</div>
  	</div> 
  	<div class="loader-div" style=""> 
  	</div>
  </div> 
  <div class="form-group row col-sm-6" id="casedetails" style="display: none;">

  </div>
  <!-- <div class="form-group col-sm-6 row" id="ministry_div">
  	<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
  	Ministry Desk Number</label>
  	<div class="col-sm-8"> -->
  			<!-- <select class="form-control select" id="mantralay_no">
  				<option value="">-- Select --</option>
  			</select> -->
  			<!-- <input type="text" class="form-control" id="mantralay_no" name="">
  		</div>      
  	</div> -->
  	<!-- <div class="form-group row col-sm-6" id="divisional_comm_level_div">
  		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
  		विभागीय आयुक्त स्तर / Divisional Commissioner Level </label>
  		<div class="col-sm-8">
  			<div class="form-radio col">
  				<div class="radio radio-primary radio-inline">
  					<label>
  						<input type="radio" class="chkPassport" name="commissioner_level" value="Establishment">

  						<i class="helper"></i>आस्थापना शाखा / Establishment Branch 
  					</label>
  				</div>
  				<div class="radio radio-primary radio-inline">
  					<label>
  						<input type="radio" class="chkPassport" name="commissioner_level" value="Development">

  						<i class="helper"></i>विकास शाखा / Development Branch
  					</label>
  				</div>
  			</div>
  		</div>      
  	</div>  -->
  <!-- 	<div class="form-group row col-sm-6" id="department_div">
  		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
  		विभाग (जिल्हा परिषदे करिता ) / Department </label>
  		<div class="col-sm-8">
  			<select class="form-control select" id="department"></select>
  		</div>      
  	</div> -->
  	<div class="form-group row col-sm-6" id="appearing">
  		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
  		Date of filing</label>
  		<div class="col-sm-8 date-filling-div">
  			<div class="input-group">
  				<input type="text" id="dateoffilling" class="form-control" name="dateoffiling" value="" placeholder="Date of filing / दाखल करण्याची तारीख " />
  				<i class="feather icon-calendar input-group-addon" id="basic-addon1"></i>

  			</div>
  		</div>
  	</div>
  	<div class="col-sm-6 row form-group">
  		<label class="col-sm-4 col-form-label">Court Hall#</label>
  		<div class="col-sm-8">
  			<input type="text" id="court_hall" name="courthall" class="form-control" autocomplete="off"/>
  		</div>
  	</div>
  	<div class="col-sm-6 row form-group">
  		<label class="col-sm-4 col-form-label">Floor#</label>
  		<div class="col-sm-8">
  			<input type="text" id="floor" name="floor" class="form-control" autocomplete="off"/>
  		</div>
  	</div>
  	
  	<div class="form-group col-sm-6 row" id="subject-div" >
  		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Subject </label>
  		<div class="col-sm-8">
  			<input type="text" id="classification" class="form-control" name="subject" value="" autocomplete="off"/>
  		</div>
  	</div>

  	<div class="form-group col-sm-6 row" id="file-no-div" >
  		<label for="exampleInputUsername1" class="col-sm-4 col-form-label">&nbsp;&nbsp;File No.<br/>
  			<span><i>( Please enter file no.)</i></span></label>
  			<div class="col-sm-8">
  				<input type="text" id="nastikramank" class="form-control" value="" name="title" autocomplete="off"/>
  			</div>
  		</div>
  		<div class="form-group col-sm-6 row" id="nominal-div" >
  			<label for="exampleInputUsername1" class="col-sm-4 col-form-label">Nominal </label>
  			<div class="col-sm-8">
  				<input type="text" id="nominal" class="form-control" name="nominal" value="" autocomplete="off"/>
  			</div>
  		</div>
  		<div class="form-group row col-sm-6" id="" >
  			<label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Title <br/>
  				<span class="lbl-short-desc"><i>(Please enter the title which you can remember easily)</i></span></label>
  				<div class="col-sm-8">
  					<textarea class="form-control" rows="3" id="title" name="title" placeholder="Enter Title"></textarea>
  					<!-- <input type="text" id="title" class="form-control" value=""/> -->
  				</div>
  			</div>
  			<div class="form-group row col-sm-6" id="appearing">
  				<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
  				Before Hon'ble Judge(s)</label>
  				<div class="col-sm-8">
  					<input type="text" id="judge" name="judge" class="form-control" value="" autocomplete="off"/>
  				</div>
  			</div>
  			<div class="form-group col-sm-12 row" id="" >
  				<label for="exampleInputUsername1" class="col-sm-12 col-form-label">Description <br/>
  					<span class="lbl-short-desc"><i>(Please enter primary details about the case, subject, CNR Number, client, etc)</i></span>
  					<div class="form-radio col">
  						<div class="radio radio-primary radio-inline">
  							<label>
  								<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('English')" value="English" checked>
  								<i class="helper"></i>English
  							</label>
  						</div>
  						<!-- <div class="radio radio-primary radio-inline">
  							<label>
  								<input type="radio" class="chkPassport" name="changeLanguageRadio" onclick="changeLanguage('Marathi')" value="Marathi">
  								<i class="helper"></i>Marathi
  							</label>
  						</div> -->
  					</div>
  				</label>

  				<div class="col-sm-12">
  					<!-- <textarea id="txtEngToHindi" class="form-control"></textarea> -->
  					<textarea name="details" id="query"  id="note-textarea" class="ckeditor"></textarea> 
  					<!-- <textarea name="details" id="txtEngToHindi" class="ckeditor"></textarea> -->

  				</div>
  			</div>
  			<div class="form-group row col-sm-12" id="appearing">
  					<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
  						Is the vakalath filed?<!--  <span style="color: red;">*</span> --></label>

  						<div class="col-sm-8">
  							<div class="form-radio">
  								<div class="radio radio-primary radio-inline">
  									<label>
  										<input type="radio" class="chkPassport" name="vakalathRadios" id="vakalathYes" onclick="vakalathShow('Yes')" value="Yes">
  										<i class="helper"></i>Yes
  									</label>
  								</div>
  								<div class="radio radio-primary radio-inline">
  									<label>
  										<input type="radio" class="chkPassport" onclick="vakalathShow('No')" name="vakalathRadios" id="vakalathNo" value="No" checked="">
  										<i class="helper"></i> No
  									</label>
  								</div>  												
  							</div>
  						</div>

  					</div>
  					<div class="form-group row col-sm-12" id="vakalathBrowse" style="display: none;">
  						<!-- <label for="exampleInputUsername1" class="col-sm-2 col-form-label">Attach Document </label>
  						<div class="col-sm-3">
  							<input type="file" id="vakalat_file" aria-label="File browser example">
  						</div> -->
  						<label for="exampleInputUsername1" class="col-sm-2 col-form-label">Vakalath filling Date</label>
  						<div class="col-sm-3 form-group">

  						<div class="input-group">

  							<input type="text" class="form-control" id="vakalath_filling_date" autocomplete="off">
  							<i class="feather icon-calendar input-group-addon"></i>

  						</div>

  					</div>
  						
  					</div>
  			<div class="form-group row col-sm-12" id="appearing">
  				<label for="exampleInputUsername1" class="col-sm-4 col-form-label">
  					Is the affidavit filed? <!-- <span style="color: red;">*</span> --></label>
  					<div class="col-sm-8">
  						<div class="form-radio">
  							<div class="radio radio-primary radio-inline">
  								<label>
  									<input type="radio" class="chkPassport" name="affidaviteRadios" id="affidavitYes" onclick="affidavitShow('Yes')" value="Yes">
  									<i class="helper"></i>Yes
  								</label>
  							</div>
  							<div class="radio radio-primary radio-inline">
  								<label>
  									<input type="radio" class="chkPassport" onclick="affidavitShow('No')" name="affidaviteRadios" id="affidavitNo" value="No" checked="">
  									<i class="helper"></i>No
  								</label>
  							</div>
  							<div class="radio radio-primary radio-inline">
  								<label>
  									<input type="radio" class="chkPassport" onclick="affidavitShow('No')" name="affidaviteRadios" id="affidavitNotApplicable" value="Not Applicable" >

  									<i class="helper"></i>Not Applicable
  								</label>
  							</div>
  						</div>
  					</div>
  				</div>
  				<div class="form-group row col-sm-12" id="affidavitBrowse" style="display: none;">
  					<!-- <label for="exampleInputUsername1" class="col-sm-2 col-form-label">Attach Document </label>
  					<div class="col-sm-3">
  						<input type="file" id="attach_file" aria-label="File browser example">
  					</div> -->
  					<label for="exampleInputUsername1" class="col-sm-2 col-form-label">Affidavit filling Date</label>
  					<div class="col-sm-3 form-group">

  						<div class="input-group">

  							<input type="text" class="form-control" id="affidavit_filling_date" autocomplete="off">
  							<i class="feather icon-calendar input-group-addon"></i>

  						</div>

  					</div>

  				</div>
  				
  					<div class="col-sm-12 form-group">
  						<div class="accordion accordion-solid-header" id="accordion-4" role="tablist">
  							<div class="card border-bottom" id="petitioner_adv_div"><div class="card-header" role="tab" id="heading-10"><h6 class="mb-0"><a data-toggle="collapse" href="#collapse-10" aria-expanded="true" aria-controls="collapse-10">Your Advocates (<span id="appearing_as_first">Petitioner</span>)</a>
  							</h6></div>
  							<div id="collapse-10" class="collapse show" role="tabpanel" aria-labelledby="heading-10" data-parent="#accordion-4">
  								<table id="petitioneradv_table" class=" table order-list table-bordered">
  									<thead>
  										<tr>
  											<td class="full-name">Full Name</td>
  											<td>Email Address</td>
  											<td>Phone Number</td>
  											<td>Action</td>
  										</tr>
  									</thead>
  									<tbody>
  										<tr>
  											<td> 
  												<input type="text" name="name" id="your_adv_name0" class="form-control" autocomplete="off"/>
  											</td>
  											<td>
  												<input type="email" name="mail" id="your_adv_email0" class="form-control" autocomplete="off"/>
  											</td>
  											<td>
  												<input type="text" name="phone" minlength="10" maxlength="10" id="your_adv_mobile0" class="form-control" autocomplete="off"/>
  											</td>
  											<td>

  											</td>
  										</tr>
  									</tbody>
  									<tfoot>
  										<td colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button btn btn-info btn-sm" id="petitioner_advocate_add" onclick="addPetitionerAdv()"> Add More </button>
  										</td>
  									</tfoot> 
  								</table>
  							</div>

  						</div>
  						<div class="card border-bottom" id="petitioner_div">
  							<div class="card-header" role="tab" id="heading-13">
  								<h6 class="mb-0">
  									<a class="collapsed" data-toggle="collapse" href="#collapse-13" aria-expanded="false" aria-controls="collapse-11">
  										Opponents (<span class="appearing_as_second">Petitioner</span>)
  									</a>
  								</h6>
  							</div>
  							<div id="collapse-13" class="collapse" role="tabpanel" aria-labelledby="heading-13" data-parent="#accordion-4">

  								<table id="petitioner_table" class="table order-list table-bordered1" width="100">
  									<thead>
  										<tr>
  											<td class="full-name">Full Name</td>
  											<td>Email Address</td>
  											<td>Phone Number</td>
  											<td>Action</td>
  										</tr>
  									</thead>
  									<tbody>
  										<tr>
  											<td> 
  												<input type="text" name="name" class="form-control"  id="your_name0" autocomplete="off"/>
  											</td>
  											<td>
  												<input type="email" name="mail"  class="form-control" id="your_email0" autocomplete="off"/>
  											</td>
  											<td>
  												<input type="text" name="phone" minlength="10" maxlength="10"  class="form-control" id="your_mobile0" autocomplete="off"/>
  											</td>
  											<td>
  											</td>
  										</tr>
  									</tbody>      
  									<tfoot>

  										<td colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button btn btn-info btn-sm" onclick="addPetitioner()" id="petitioner_add"> Add More </button>
  										</td>
  									</tfoot>                              
  								</table>

  							</div>
  						</div> 

  						<div class="card border-bottom" id="acc_petition_id">
  							<div class="card-header" role="tab" id="heading-11">
  								<h6 class="mb-0">
  									<a class="collapsed" data-toggle="collapse" href="#collapse-11" aria-expanded="false" aria-controls="collapse-11">
  										Opponents (<span class="appearing_as_second">Respondent</span>)
  									</a>
  								</h6>
  							</div>
  							<div id="collapse-11" class="collapse" role="tabpanel" aria-labelledby="heading-11" data-parent="#accordion-4">
  								<table id="repondant_table" class="table order-list table-bordered1" width="100">
  									<thead>
  										<tr>
  											<td class="full-name">Full Name</td>
  											<td>Email Address</td>
  											<td>Phone Number</td>
  											<td>Action</td>
  										</tr>
  									</thead>
  									<tbody>
  										<tr>
  											<td> 
  												<input type="text" name="name" class="form-control"  id="opponent_name0" autocomplete="off"/>
  											</td>
  											<td>
  												<input type="email" name="mail"  class="form-control" id="opponent_email0" autocomplete="off"/>
  											</td>
  											<td>
  												<input type="text" name="phone" minlength="10" maxlength="10"  class="form-control" id="opponent_mobile0" autocomplete="off"/>
  											</td>
  											<td>
  											</td>
  										</tr>
  									</tbody>      
  									<tfoot>

  										<td colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button btn btn-info btn-sm" onclick="addRespondant()" id="respondant_add"> Add More </button>
  										</td>
  									</tfoot>                              
  								</table>

  							</div>
  						</div>


  						<div class="card">
  							<div class="card-header" role="tab" id="heading-12">
  								<h6 class="mb-0">
  									<a class="collapsed" data-toggle="collapse" href="#collapse-12" aria-expanded="false" aria-controls="collapse-12">
  										Opponent Advocates (<span class="appearing_as_second">Respondent</span>)
  									</a>
  								</h6>
  							</div>
  							<div id="collapse-12" class="collapse" role="tabpanel" aria-labelledby="heading-12" data-parent="#accordion-4">
  								<table id="respondant_adv_table" class=" table order-list table-bordered" width="100">
  									<thead>
  										<tr>
  											<td class="full-name">Full Name</td>
  											<td>Email Address</td>
  											<td>Phone Number</td>
  											<td>Action</td>
  										</tr>
  									</thead>
  									<tbody>
  										<tr>
  											<td class="col-sm-4">

  												<input type="text" name="name"  id="opponent_adv_name0" class="form-control" autocomplete="off" />
  											</td>
  											<td class="col-sm-4">
  												<input type="email" name="mail"  id="opponent_adv_email0"  class="form-control" autocomplete="off"/>
  											</td>
  											<td class="col-sm-4">
  												<input type="text" name="phone" minlength="10" maxlength="10" id="opponent_adv_mobile0" class="form-control" autocomplete="off"/>
  											</td>
  											<td class="col-sm-2">
  											</td>
  										</tr>
  									</tbody>  
  									<tfoot>
  										<td colspan="4" style="text-align: right;"><button type="button" class="add-opponent add-more-button btn btn-info btn-sm" onclick="respondantAdvAdd()" id="respondant_advocate_add"> Add More </button></td>
  									</tfoot>                                
  								</table>

  							</div>
  						</div>
  					</div>

  				</div>
  				<div class="col-12">
  					<button class="btn btn-info btn-sm" name="add_case" type="button" onclick="submitCase()">Submit</button>
  					<button class="btn btn-danger btn-sm" type="button"><a href="view-case.php?caseid=<?php echo($_GET['caseid']) ?>">Cancel </a></button>
  				</div>
  			</form>
  		</div>
  	</div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="modal" id="data">
	<div class="modal-dialog modal-lg fetchdata-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><b>Please Map the Following Details</b></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body" style="padding: 0px;" id="modal-data">

			</div>

		</div>

	</div>
</div>
<?php include 'footer.php'; ?>
<script src="scripts/aes.js"></script>
<script src="scripts/pbkdf2.js"></script>
<script type="text/javascript" src="scripts/jsapi.js"></script>
<script type="text/javascript" src="scripts/edit-case.js"></script>