<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> <!-- <link rel="stylesheet" href="css/vertical-layout-light/add_cases.css"> -->
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>

      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <?php if(isset($_POST['submit'])){
                  extract($_POST);
                  $fromdate = $_POST['form_date'];
                  $todate = $_POST['to_date'];
                }?>
                <h5 class="card-title">Edit Expenses</h5>
                <hr/>
                <?php
                $sql = mysqli_query($connection, "SELECT * FROM `add_expenses` WHERE `exp_id`='".base64_decode($_GET['id'])."'");
                $rows = mysqli_fetch_array($sql);

                if(isset($_POST['submit'])){
                  extract($_POST);
                  $caseno=$_POST['caseno'];
                  $date1=$_POST['date1'];
                  if($caseno!=''){
                    $insert = mysqli_query($connection, "UPDATE `add_expenses` SET `user_id`='".$_SESSION['user_id']."', `case_id`='".$caseno."',`date`='".$date1."',`particulars`='".$particulars."',`moneyspent`='".$moneyspent."',`method`='".$method."',`notes`='".$notes."',`division`='".$_SESSION['cityName']."',`under_division`='".$_SESSION['under_division']."' WHERE `exp_id`='".base64_decode($_GET['id'])."'");

                    $error=array();
                    $extension=array("jpeg","jpg","png","gif","pdf","xlsx","csv");
                    foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name) {
                      $file_name=$_FILES["image"]["name"][$key];
                      $file_tmp=$_FILES["image"]["tmp_name"][$key];
                      $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                      if(in_array($ext,$extension)) {
                        if(!file_exists("upload_expimage/".$txtGalleryName."/".$file_name)) {
                          move_uploaded_file($file_tmp=$_FILES["image"]["tmp_name"][$key],"upload_expimage/".$txtGalleryName."/".$file_name);
                            $addexpdoc =mysqli_query($connection, "INSERT INTO `addexpdoc`(`userid`,`exp_id`,`image`,`date_time`)VALUES('".$_SESSION['user_id']."','".base64_decode($_GET['id'])."','".$file_name."','".date("Y-m-d")."')");
                        }else {
                          $filename=basename($filename,$ext);
                          $newFileName=$filename.time().".".$ext;
                          move_uploaded_file($file_tmp=$_FILES["image"]["tmp_name"][$key],"upload_expimage/".$txtGalleryName."/".$newFileName);
                          $addexpdoc = mysqli_query($connection, "INSERT INTO `addexpdoc`(`userid`,`exp_id`,`image`,`date_time`)VALUES('".$_SESSION['user_id']."','".base64_decode($_GET['id'])."','".$file_name."','".date("Y-m-d")."')");
                        }
                      }
                    }
 ?>
                        <script>
                          window.location.href='my-expenses.php';
                        </script>
                        <?php 
                  }
                }
                ?>
                <div class="page-body">
                  <div class="form-group">
                    <div class="card col-sm-12">
                      <div class="card-block form-group row">
                        <div class="col-sm-12 form-group">
                          <form class="forms-sample" method="POST" enctype="multipart/form-data">
                            <div class="form-group row" style="padding-top: 30px;">
                              <div class="col-sm-6">
                                <div class="form-group row">
                                  <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Case</b></label>
                                  <div class="col-sm-8">
                                    <select class="form-control" name="caseno" id="diary_yearDesi" required>
                                      <option value="<?php echo $rows['case_id'];?>"><?php $casedocuments1 = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".$rows['case_id']."'"); $selcasedocuments1 = mysqli_fetch_array($casedocuments1); if($selcasedocuments1['case_id']!=""){ ?><?php echo $selcasedocuments1['case_type'];?> / 
                                  <?php echo $selcasedocuments1['case_no'];?> / 
                                  <?php echo $selcasedocuments1['case_no_year'];?> / 
                                  <?php echo $selcasedocuments1['case_title'];?><?php } ?>Please select / कृपया निवडा</option>
                                      <?php  
                                      $casedocuments = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_no`!='' AND `case_district`='".$_SESSION['cityName']."' AND `remove_status`=1");
                                while($selcses = mysqli_fetch_array($casedocuments)){?>
                                <option value="<?php echo $selcses['case_id'];?>">
                                  <?php echo $selcses['case_type'];?> / 
                                  <?php echo $selcses['case_no'];?> / 
                                  <?php echo $selcses['case_no_year'];?> / 
                                  <?php echo $selcses['case_title'];?>
                                </option>
                                <?php } ?>
                                      
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Particulars</b></label>
                                  <div class="col-sm-8">
                                    <input type="text" name="particulars" class="form-control" value="<?php echo $rows['particulars'];?>" placeholder="Enter Particulars" required>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Method</b></label>
                                  <div class="col-sm-8">
                                    <select class="form-control" name="method" id="state" required>
                                      <option value="<?php echo $rows['method'];?>"><?php if($rows['method']!=""){ echo $rows['method']; }else{?>Please select<?php } ?></option>
                                      <option value="Bank Transfer">Bank Transfer</option>
                                      <option value="Cash">Cash</option>
                                      <option value="Cheque">Cheque</option>
                                      <option value="Online Payment">Online Payment</option>
                                      <option value="Other">Other</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group row">
                                  <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Date</b></label>
                                  <div class="col-sm-8">
                                    <input type="text" name="date1" id="datepicker" value="<?php echo $rows['date'];?>" placeholder="Enter Date" class="form-control" required>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="exampleInputUsername1" class="col-sm-3 col-form-label" ><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Money Spent</b></label>
                                  <div class="col-sm-8">
                                    <input type="text" name="moneyspent" class="form-control" placeholder="Enter Amount" value="<?php echo $rows['moneyspent'];?>" required>
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="exampleInputUsername1" class="col-sm-3 col-form-label"><b>&nbsp;&nbsp;&nbsp;&nbsp;Notes</b></label>
                                  <div class="col-sm-8">
                                    <textarea class="form-control" name="notes" placeholder="Enter Note"><?php echo $rows['notes'];?></textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <span><b>Click on browse to select expenses attachment:</b></span><br><br>
                            <input type="file" name="image[]" multiple/>
                            <button type="button" class="btn btn-sm btn-primary" onclick="addDocument()">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            </button>
                            <div id="addedDocument"></div>
                            <br>
                            <?php $addimage = mysqli_query($connection, "SELECT `image`,`id` FROM `addexpdoc` WHERE `exp_id`='".base64_decode($_GET['id'])."'");
                                  while($selimages = mysqli_fetch_array($addimage)){
                                  $ext = pathinfo($selimages['image'], PATHINFO_EXTENSION); ?>

                                  <?php if($selimages['image']!=""){?><a href="upload_expimage/<?php echo $selimages['image'];?>"  download><?php } ?>
                                  <i class="feather icon-file-pdf-o pdfcolor"></i><?php  $ext = pathinfo($selimages['image'], PATHINFO_EXTENSION);
                                   if($ext=='pdf'){ 
                                  echo "<img src='images/pdf.png' width='30' title='".$selimages['image']."'>"; echo "<a href='remove_documentexp?id=".base64_encode($selimages['id'])."&&ids=".base64_encode($rows['exp_id'])."'>  Remove</a>"; }elseif($ext=='docx'){ 
                                  echo "<img src='images/word.png' width='30' title='".$selimages['image']."'>"; echo "<a href='remove_documentexp?id=".base64_encode($selimages['id'])."&&ids=".base64_encode($rows['exp_id'])."'>  Remove</a>"; }elseif($ext=='xlsx'){ 
                                  echo "<img src='images/excel.png' width='30' title='".$selimages['image']."'>"; echo "<a href='remove_documentexp?id=".base64_encode($selimages['id'])."&&ids=".base64_encode($rows['exp_id'])."'>  Remove</a>"; }elseif($ext=='csv'){  
                                  echo "<img src='images/csv.png' width='30' title='".$selimages['image']."'>"; echo "<a href='remove_documentexp?id=".base64_encode($selimages['id'])."&&ids=".base64_encode($rows['exp_id'])."'>  Remove</a>"; }elseif($ext=='png'){  
                                  echo "<img src='images/png.png' width='30' title='".$selimages['image']."'>"; echo "<a href='remove_documentexp?id=".base64_encode($selimages['id'])."&&ids=".base64_encode($rows['exp_id'])."'>  Remove</a>"; }elseif($ext=='jpg' || $ext=='jpeg'){  
                                  echo "<img src='images/jpg.png' width='30' title='".$selimages['image']."'>"; echo "<a href='remove_documentexp?id=".base64_encode($selimages['id'])."&&ids=".base64_encode($rows['exp_id'])."'>  Remove</a>"; }else{ 
                                   } ?></a><br/>
                                  <?php } ?><br/>
                            <input class="btn btn-sm btn-info newsubmit" value="Submit" type="submit" name="submit"> &nbsp;
                            <a href="my-expenses.php"><button class="btn btn-sm btn-danger" type="button"> Cancel </button></a>
                          </form>  
                        </div>

                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php include 'footer.php'; ?>
    <script type="text/javascript">
      $(".add-expenses-li").addClass('active pcoded-trigger');
    </script>
    <script type="text/javascript" src="scripts/add-expenses.js"></script>









