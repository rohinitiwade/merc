<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?>
<?php include("phpfile/sql_home.php"); ?>  
<?php session_start(); include("../includes/connection.php");?> 
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header m-b-10">
               <?php
              if(isset($_POST['submit'])){
              extract($_POST);
              if($sdate!=""){
                $timesheetquery.= " AND `edate` BETWEEN '".$sdate."' AND '".$todate."'";
                // $timesheetquery1.= "`division`='".$_SESSION['cityName']."' AND `under_division`='".$_SESSION['under_division']."' AND `edate` BETWEEN '".$sdate."' AND '".$todate."'";
              }
            }
            // else{
            //   $timesheetquery.= "`division`='".$_SESSION['cityName']."' AND `under_division`='".$_SESSION['under_division']."'";
            // }
            $counttime = mysqli_query($connection, "SELECT COUNT(id) as Counts FROM `add_timesheet` WHERE ".$timesheetquery."");
            $elcount = mysqli_fetch_array($counttime);
            ?>
              <h5 class="card-title">Manage Timesheets  (<?php echo $elcount['Counts'];?>)</h5>
            </div>
            <div class="page-body">
              <div class="card form-group">
                <div class="card-block">
                  <form name="Myform" action="" method="POST">
                  <div class="row">
                    <div class="col-sm-2 form-group">
                      <label><b>Select From Date</b></label>
                    </div>
                      <div class="col-sm-2 form-group">
                      <input type="text" name="sdate" class="form-control" id="datepicker2" placeholder="Select Date" required="">
                      </div>
                      <div class="col-sm-2 form-group">
                      <label><b>Select To Date</b></label>
                    </div>
                      <div class="col-sm-2 form-group">
                      <input type="text" name="todate" class="form-control" id="datepicker3" placeholder="Select Date" required="">
                      </div>
                       <div class="col-sm-1 form-group">
                          <input type="submit" name="submit" class="btn-primary btn-sm btn" value="Submit">
                      </div>
                       <div class="col-sm-3 form-group text-right">
                      <a href="add-timesheets.php" class="btn btn-info btn-sm form-group"><i class="fa fa-plus"></i>Add Timesheets</a>
                      <button class="btn btn-warning btn-sm form-group" id="myBtn" type="button" data-target="#filter" data-toggle="modal">Filter</button>
                      <button class="btn btn-primary btn-sm form-group" id="myBtn" type="button" data-target="#export" data-toggle="modal">Export</button>
                  </div>
                  </div>
                </form>
                  <div class="table-responsive">
                    <table id="order-listing" class="table table-bordered">
                      <thead class="bg-primary"> 
                        <tr>
                          <th>Sr.No.</th>
                          <th>Case</th>
                          <th>Date</th>
                          <th>Particulars</th>
                          <th>Time Spent (in hours)</th>
                          <th>Type</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php if (isset($_GET['page_no']) && $_GET['page_no']!="") {
                              $page_no = $_GET['page_no'];
                            } else {
                              $page_no = 1;
                            }
                            $total_records_per_page = 10;
                            $offset = ($page_no-1) * $total_records_per_page;
                            $previous_page = $page_no - 1;
                            $next_page = $page_no + 1;
                            $adjacents = "2"; 
                            $result_count = mysqli_query($connection,"SELECT * FROM add_timesheet  WHERE ".$timesheetquery."");
                            $total_records = mysqli_num_rows($result_count);
                            $total_no_of_pages = ceil($total_records / $total_records_per_page);
                            $second_last = $total_no_of_pages - 1;

                             $i=1+$offset;
                               $seltimesheet=mysqli_query($connection,"SELECT * FROM `add_timesheet` WHERE ".$timesheetquery." ORDER BY id DESC LIMIT $offset, $total_records_per_page");
                              while($row=mysqli_fetch_array($seltimesheet)){
                                $casedocuments =mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE  `case_id`='".$row['case_id']."'");
                                $selcses = mysqli_fetch_array($casedocuments);
                                ?>
                              <tr >
                                 <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcses['case_id'])?>"><?php echo $i?></td>
                                  <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcses['case_id'])?>" style="word-break:break-all;" width="200"> <?php echo $selcses['case_type'];?> / <?php echo $selcses['case_no'];?> / <?php echo $selcses['case_no_year'];?> / <?php echo $selcses['case_title'];?></td>
                                  <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcses['case_id'])?>"><?php echo date('F d, Y',strtotime($row['edate']));?></td>
                                  <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcses['case_id'])?>" style="word-wrap: break-word;"><?php echo $row['particulars'];?></td>
                                  <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcses['case_id'])?>"><?php echo $row['timespent'];?> : <?php echo $row['timeinmin'];?></td>
                                  <td class="rowclickable" data-id="caseid=<?php echo base64_encode($selcses['case_id'])?>"><?php echo $row['type'];?></td>
                                  <td>
                            <a href="update-timesheet.php?id=<?php echo base64_encode($row['id']);?>"><i class="fa fa-edit action-css" title="Edit Advocate" style="color: blue"></i></a>
                            <a href="delete-timesheet.php?id=<?php echo base64_encode($row['id']);?>"><i class="fa fa-trash action-css" onclick="return confirm('Are you sure you want to delete?');" title="Delete" style="color: red;"></i></a>
                          </td>
                        </tr> 
                        <?php $i++;}?>
                      </tbody>
                    </table>
                   <ul class="pagination">
                               <?php if ($page_no > 1){
                                echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>First Page</a></li>";} ?>
                                <li <?php if ($page_no <= 1){
                                  echo "class='disabled'";} ?>>
                                  <a <?php if ($page_no > 1){
                                    echo "href='?page_no=$previous_page&&status=".$_REQUEST['status']."'";} ?>>Previous</a>
                                  </li>
                                  <?php
                                  if ($total_no_of_pages <= 10){
                                    for ($counter = 1;$counter <= $total_no_of_pages;$counter++)
                                    {
                                      if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>"; }
                                      else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
                                    }
                                  }elseif ($total_no_of_pages > 10){
                                    if ($page_no <= 4){
                                      for ($counter = 1;$counter < 8;$counter++){
                                        if ($counter == $page_no){echo "<li class='active'><a>$counter</a></li>";
                                      }else{ echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";}
                                    }
                                    echo "<li><a>...</a></li>";
                                    echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
                                    echo "<li><a href='?page_no=$total_no_of_pages&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
                                  }elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4){
                                    echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
                                    echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
                                    echo "<li><a>...</a></li>";
                                    for ($counter = $page_no - $adjacents;$counter <= $page_no + $adjacents;$counter++)
                                    {
                                      if ($counter == $page_no){
                                        echo "<li class='active'><a>$counter</a></li>";
                                      }else{
                                        echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages&&status=".$_REQUEST['status']."'>$counter</a></li>";
                                      }
                                    }
                                    echo "<li><a>...</a></li>";
                                    echo "<li><a href='?page_no=$second_last&&status=".$_REQUEST['status']."'>$second_last</a></li>";
                                    echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>$total_no_of_pages</a></li>";
                                  }else{
                                    echo "<li><a href='?page_no=1&&status=".$_REQUEST['status']."'>1</a></li>";
                                    echo "<li><a href='?page_no=2&&status=".$_REQUEST['status']."'>2</a></li>";
                                    echo "<li><a>...</a></li>";
                                    for ($counter = $total_no_of_pages - 6;$counter <= $total_no_of_pages;$counter++)
                                    {
                                      if ($counter == $page_no){
                                        echo "<li class='active'><a>$counter</a></li>";
                                      }else{
                                        echo "<li><a href='?page_no=$counter&&status=".$_REQUEST['status']."'>$counter</a></li>";
                                      }
                                    }
                                  }
                                }
                                ?>
                                <li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
                                  <a <?php if($page_no < $total_no_of_pages) { echo "href='?page_no=$next_page&&status=".$_REQUEST['status']."'"; } ?>>Next</a>
                                </li>
                                <?php if($page_no < $total_no_of_pages){
                                  echo "<li><a href='?page_no=$total_no_of_pages&&status=".$_REQUEST['status']."'>Last &rsaquo;&rsaquo;</a></li>";
                                } ?>
                              </ul>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="export">
  <!-- <form name="myform" method="POST" action="downloadTimeSheet.php"> -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Export</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <!-- <div class="col">
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`case`" checked="checked" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Case</span>
                </label>
              </div>
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`datetime`" checked="checked" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Date</span>
                </label>
              </div>
            
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`particulars`" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Particulars</span>
                </label>
              </div>

              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`type`" checked="checked"/>
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Type</span>
                </label>
              </div>
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`timespent`" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Time Spent (in hours)</span>
                </label>
              </div>
              
            </div> -->
          
          <div class="col row">
            <div class="downlab col-sm-2"><label>Download as:</label></div>
            <div class="col-sm-6 form-radio">

              <div class="radio radio-inline">
                <label>
                  <input type="radio" name="downloadas" value="pdf" checked="">
                  <i class="helper"></i> PDF
                </label>
              </div>
              <div class="radio radio-inline">
                <label>
                  <input type="radio" name="downloadas" value="excel">
                  <i class="helper"></i> Excel
                </label>
              </div>
            </div>   
          </div>
           <div id="document-table-div-export"></div>
        </div>
        <div class="modal-footer">          
            <button value="submit" type="submit" class="btn btn-info btn-sm" onclick="download_doc()"> Download</button>
            <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  <!-- </form> -->
</div>
<div class="modal" id="filter">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="POST">
        <div class="modal-header">
          <h4>Filter</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-sm-4">
              <label>Cases</label><br>
              <select class="form-control" name="caseno" id="state" required>
                <option value="">Please Case No</option>
                <?php $caseno = mysqli_query($connection, "SELECT * FROM `add_timesheet` WHERE `case`!='' AND `division`='".$_SESSION['cityName']."'"); 
                while($selcases = mysqli_fetch_array($caseno)){
                 $casedocumentsq =mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE  `case_id`='".$selcases['case']."'");
                 $selcsesd = mysqli_fetch_array($casedocumentsq);
                 ?>
                 <option value="<?php echo $selcsesd['case'];?>"><?php echo $selcsesd['case_type'];?> / <?php echoselcsesdselcses['case_no'];?> / <?php echo $selcsesd['case_no_year'];?> / <?php echo $selcsesd['case_title'];?></option>
               <?php } ?>

             </select>
             <label>From</label><br>
             <input type="text" name="from_date" id="datepicker" class="form-control" placeholder="Enter From Date">
           </div>
           <div class="col-sm-4">
            <label>Team Member</label><br>
            <select name="team_member" class="form-control">
              <option value="">--Select Team Member--</option>
              <?php $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `parent_id`='".$_SESSION['user_id']."'");
              while($selteam = mysqli_fetch_array($team)){?>
                <option value="<?php echo $selteam['reg_id'];?>"><?php echo $selteam['name'];?> <?php echo $selteam['last_name'];?></option>
              <?php } ?>
            </select>
            <label>To</label>
            <input type="text" name="to_date" id="datepicker1" class="form-control" placeholder="Enter To Date">
          </div>
          <div class="col-sm-4">
            <label>Type</label><br>
            <select class="form-control" name="Type">
              <option value="">All</option>
              <option value="Effective">Effective</option>
              <option value="Non Effective / Procedural Appearance">Non Effective / Procedural Appearance</option>
            </select>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-danger btn-sm" name="Reset" type="reset">Reset</button>
        <button class="btn btn-info btn-sm" name="submitFil" type="submit">Submit</button>
      </div>
    </form>
  </div>

</div>

</div>


<?php include 'footer.php'; ?>
<script type='text/javascript' src='scripts/jspdf.min.js'></script>
 <script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
 <script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
 <script type="text/javascript" src="scripts/all_export.js"></script>
 <script type="text/javascript" src="scripts/manage-timesheet.js"></script>

