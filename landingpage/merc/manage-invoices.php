<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?>
<!-- <link rel="stylesheet" href="css/vertical-layout-light/add_cases.css"> -->
<body>

  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>

      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">

                <h5 class="card-title">
                  Invoices ( <?php 
                   $invoicecount = mysqli_query($connection,"SELECT * FROM `invoices` ORDER BY invoice_id DESC");
                   echo mysqli_num_rows($invoicecount);?>)
                 </h5>
               </div>
               <div class="page-body">
                <div class="form-group">
                  <div class="card col-sm-12">
                    <div class="card-block form-group row">
                      <div class="col-sm-12 form-group">
                        <form name="Myform" method="POST">
                          <div class="col-sm-12 form-group row">


                            <div class="col-sm-12" style="text-align: right">
                              <!-- <div class="col-sm-4"> -->

                                <!-- </div> -->
                                <!-- <div class="col-sm-4"> -->
                                  <a href="create-invoice.php" class="btn btn-sm btn-info" id="myBtn"
                                  style="">Create Invoice</a>
                                  <!-- </div> -->
                                  <!-- <div class="col-sm-4"> -->
                                    <button class="btn btn-sm btn-warning" id="myBtn"
                                    data-target="#export" type="button" data-toggle="modal" style="">Export</button>
                                    <!-- </div> -->
                                  </div>
                                </div>
                              </form>
                            </div>
                            <div class="table-responsive col-sm-12">
                              <input type="hidden" name="" id="caseid">
                              <table id="example" class="table table-bordered">
                                <thead class="bg-primary">
                                  <tr>
                                    <tr>
                                      <th>Sr.No.</th>
                                      <th>Invoice no.</th>
                                      <th>Client Name</th>
                                      <th>Date of Issue</th>
                                      <th>Total(INR)</th>
                                      <!-- <th>Status</th> -->
                                      
                                      <th>Actions</th>
                                      
                                    </tr>
                                  </tr>
                                </thead>
                                <tbody>


                                 <?php 
                                 $invoice = mysqli_query($connection,"SELECT * FROM `invoices` ORDER BY invoice_id DESC");
                                 $i=1;
                                 while($getInvoice= mysqli_fetch_array($invoice)){
                                  $inv_id = $getInvoice['invoice_id'];
                                  ?>
                                  <tr>
                                    <td>
                                      <?php echo $i; ?>
                                    </td>
                                    <td>
                                     <?php echo $getInvoice['invoice_number']; ?>
                                   </td>
                                   <td>
                                     <?php 
                                     $client = mysqli_query($connection,"SELECT * FROM `petitioner_respondent` WHERE `pet_res_id`='".$getInvoice['client_id']."'");
                                     $i=1;
                                     $getClient= mysqli_fetch_array($client);
                                     echo $getClient['full_name']; ?>
                                   </td>
                                   <td>
                                    <?php echo date('F d, Y',strtotime($getInvoice['date_of_issue'])); ?>
                                  </td>
                                  <td>
                                   <?php echo $getInvoice['total_amount']; ?>
                                 </td>


                                 <td>

                                    <a href="edit_document.php?id=NjM="><i class="feather icon-edit" title="Edit" style="color: orange;"></i></a>
                                    <!-- <i class="feather icon-info" title="Info"></i> -->
                                    <a href="upload_document/1595252620_Appeal.pdf" download="">
                                     <i class="feather icon-download" title="Download" style="color: blue;"></i>
                                   </a>
                                    <a href="view-invoice.php?createdid=<?php echo base64_encode($inv_id);?>" class="feather icon-eye viewdetailscase report-icon" title="View" target="_BLANK">
                                      </a> 
                                   <a href="delete_document.php?id=63" onclick="return confirm('Are you sure want to remove this Document?');"> 
                                    <i class="feather icon-trash" title="Delete" style="color: red;"></i>
                                  </a>
                                
                              </td>

                            </tr>

                            <?php $i++; } ?>
                          </tbody>
                          <tfoot>
                          </table>

                        </div>


                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php'; ?>

  <script type="text/javascript">
    $(".add-client-li").addClass('active pcoded-trigger');
  </script>
  <script type='text/javascript' src='scripts/jspdf.min.js'></script>
  <script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
  <script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>

