<?php $pageNam =  basename($_SERVER["PHP_SELF"]);
$insert = mysqli_query($connection, "INSERT INTO `frequency_report` SET `user_id`='".$_SESSION['user_id']."',`case_id`='".base64_decode($_GET['caseid'])."',`division`='".$_SESSION['cityName']."',`page_name`='".$pageNam."',`date_time`='".date("Y-m-d H:i:s")."',`dates`='".date("Y-m-d")."'");?>
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 

<style type="text/css">
#petitioner_add,#repondent_add{
  border:1px solid lightgray;
  padding: 2%;
}
</style>
<link rel="stylesheet" type="text/css" href="styles/tree_style.min.css">
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">

      <?php include("menu.php") ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Quasi Judicial Matter</h5>
              </div>
              <div class="card page-body">
                <input type="hidden" name="caseid" id="caseid" value="<?php echo base64_decode($_GET['caseid']);?>">
                <input type="hidden" id="fname" value="<?php echo $row['name'];?>">
                <input type="hidden" id="lname" value="<?php echo $row['last_name'];?>">
                <input type="hidden" id="email" value="<?php echo $row['email'];?>">
                <input type="hidden" id="mobile" value="<?php echo $row['mobile'];?>">
                <!-- partial -->
                <div class="card-block">
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Competant Authority</b></label>
                    <div class="col-sm-9">
                      <select id="competant_authority" class="form-control form-group" name="complaint_authority">
                        <option value="0">Other</option>
                      </select>
                      <br>
                      <input type="text" id="other_competant_authority" class="form-control" name="" placeholder="Enter Other Competant Authority">
                    </div>

                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Ministry</b></label>
                    <div class="col-sm-4">
                      <select class="form-control"  id="ministry_name">
                        <option value="">Select</option>
                      </select>
                      <!-- <input type="text" class="form-control" id="ministry_name" placeholder="Enter Ministry" name="ministry_name"> -->
                    </div>
                    <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Desk Number</b></label>
                    <div class="col-sm-4">
            <!-- <select class="form-control"  id="zp_name">
              <option value="">Select</option>
            </select> -->
            <input type="text" class="form-control" id="zp_name" placeholder="Enter Desk Number" autocomplete="off" name="zp_name">
          </div>

        </div>
        <div class="form-group row">
         <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Division </b></label>
         <div class="col-sm-4" >
          <select id="division" class="form-control" name="division">
            <option value="">Select  Division </option>
            <?php $division = mysqli_query($connection,"SELECT * FROM `division_list` ORDER BY division_name ASC");
            while($seldivision =mysqli_fetch_array($division)){?>
              <option value="<?php echo $seldivision['division_name'];?>"><?php echo $seldivision['division_name'];?></option>
            <?php } ?>
                <!-- <option value="2">Aurangabad</option>
                <option value="3">Pune</option>
                <option value="4">Nashik</option>
                <option value="5">Nagpur</option>
                <option value="6">Kokan</option>
                <option value="7">Amravati</option> -->
              </select>
            </div>
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Zilla Parishad Name</b></label>
            <div class="col-sm-4">
              <select id="district" name="under_division" class="form-control form-group">
                <option value="">Select Zilla Parishad</option>
                <option value="0">Other</option>
              </select>
              <br>
              <input type="text" class="form-control" id="other_district" name="" placeholder="Enter Other Zilla Parishad">
            </div>
            
          </div>

          
          <div class="form-group row" id="appearing">
            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
              <b>Divisional Commissioner Level
              </b></label>
              <div class="form-radio col-sm-8">
                <div class="radio radio-primary radio-inline">
                  <label>
                    <input type="radio" class="chkPassport" name="commissioner_level" value="Establishment" checked>
                    <i class="helper"></i>Establishment Branch 
                  </label>
                </div>
                <div class="radio radio-primary radio-inline">
                  <label>
                    <input type="radio" class="chkPassport" name="commissioner_level" value="Development">
                    <i class="helper"></i>Development Branch 
                  </label> 
                </div>
              </div>
              <div class="col-sm-6" style="float: left;">
                <div class="form-check">
                  <label class="form-check-label checkboxlabel">
                    &nbsp;
                  </label>
                </div>
              </div>

              <div class="col-sm-6" style="float: left;">
                <div class="form-check">
                  <label class="form-check-label checkboxlabel">
                    &nbsp;
                  </label>
                </div>
              </div>

            </div>
            <div class="form-group row">
             <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Department</b></label>
             <div class="col-sm-4">
              <input type="text" class="form-control" id="department"  autocomplete="off" placeholder="Enter Department" name="department">
            </div>
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Subject</b></label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="subject"  autocomplete="off" placeholder="Enter Subject" name="subject">
            </div>

          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Filing Date</b></label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="filing_date" placeholder="Enter Filing Date" name="filing_date">
            </div>
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Case Type </b></label>
            <div class="col-sm-4">
              <input type="text" class="form-control" placeholder="Enter Case Type"  autocomplete="off" id="case_type" name="case_type">
            </div>  

          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Case No./Year </b></label>
            <div class="col-sm-2">
              <input type="text" class="form-control"  autocomplete="off" placeholder="Case Number" id="case_no" name="case_no">
            </div>  
            <div class="col-sm-2">
              <input type="text" class="form-control"  autocomplete="off" placeholder="Case Year" id="case_year">
            </div> 

          </div>

          <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Title</b></label>
            <div class="col-sm-4">
              <input type="text" class="form-control"   autocomplete="off" placeholder="Enter Case Title" id="case_title" name="case_title">
            </div> 
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Stage</b></label>
            <div class="col-sm-4">
              <input type="text" class="form-control"  autocomplete="off" placeholder="Enter Stage" id="stage" name="Stage">
            </div> 
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Hearing Date</b></label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="hearing_date" placeholder="Enter Hearing Date" name="">
            </div>   
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Next Stage </b></label>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="next-stage"  autocomplete="off" placeholder="Enter Next Stage" name="hearing_stage ">
            </div>          
          </div>
<!-- 
          <div class="form-group row">
            <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Documents/ कागदपत्रे </b></label>
            <div class="col-sm-4">
              <div class="form-group">
                <input type="file" name="document" id="document_0">
                <button class="btn btn-info btn-sm pull-right" type="button" onclick="addDocuments()">Add</button>
              </div>
              <div class="mulitple_docs"></div>
            </div>
            <label class="col-sm-2 col-form-label"><b>Order Judgement </b></label>
            <div class="col-sm-4">
              <div class="form-group">
                <input type="file" name="order_judgment" id="order_judgement_0">                
                <button class="btn btn-info btn-sm pull-right" type="button" onclick="addOrderJudgement()">Add</button>
              </div>
              <div class="mulitple_orders"></div>
            </div>
          </div> -->

          <div class="form-group row">
            <div class="col-sm-6">
              <label class="col-form-label"><b>Petitioner</b></label>
              <div id="petitioner_add">
                <div class="form-group">
                  <label>Petitioner Name</label>
                  <input type="text" name="" class="form-control" id="pet_name"  autocomplete="off"> 
                </div>
                <div class="form-group">
                  <label>Petitioner Postal Address</label>
                  <textarea class="form-control" id="pet_address" rows="3"   autocomplete="off" style="resize: vertical;"></textarea>
                </div>
                <div class="form-group">
                  <label>Petitioner Email</label>
                  <input type="text" name="" class="form-control" id="pet_email"  autocomplete="off"> 
                </div>
                <div class="form-group">
                  <label>Petitioner Mobile</label>
                  <input type="text" name="" class="form-control" id="pet_mobile"  autocomplete="off">
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <label class="col-form-label"><b>Respondent</b></label>
              <div id="repondent_add">
               <div class="form-group">
                <label>Respondent Name</label>
                <input type="text" name="" class="form-control" id="resp_name"  autocomplete="off"> 
              </div>
              <div class="form-group">
                <label>Respondent Postal Address</label>
                <textarea class="form-control" id="resp_address"  autocomplete="off" rows="3" style="resize: vertical;"></textarea>
              </div>
              <div class="form-group">
                <label>Respondent Email</label>
                <input type="text" name="" class="form-control" id="resp_email"  autocomplete="off"> 
              </div>
              <div class="form-group">
                <label>Respondent Mobile</label>
                <input type="text" name="" class="form-control" id="resp_mobile"  autocomplete="off">
              </div>
            </div>
          </div>
        </div>
       <!--  <div class="form-group row" id="legal-help-div">
          <label class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b><span id="party-address"></span> Seek Legal Help/ कायदेशीर मदत घ्या
          </b></label>
          <div class="col-sm-9">
            <textarea class="form-control" id="legal-help" rows="2" name="suggetion"></textarea>
            <br> <input type="text" class="form-control" id="emp_no" placeholder="Enter Employee Number/ सेवार्थ क्रमांक " name="emap_id">
          </div>
        </div> -->
        <div class="form-group row">
          <div class="col-sm-3"></div>
          <div class="col-sm-9" style="text-align: right;">
            <!-- <button type="button" class="btn btn-warning btn-sm" onclick="seekLegalHelp()">Seek Legal Help</button> -->
            <button class="btn btn-info btn-sm" type="button" onclick="submitQuasi()">Update</button>
            <button class="btn btn-danger btn-sm" type="button" ><a href="cases-report">Cancel </a></button>
          </div>
        </div>
      </div>
    </div>      
  </div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php include("footer.php");?>
<script type="text/javascript" src="scripts/edit-quasi.js"></script>
