<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> 
<?php include("phpfile/sql_home.php");?>
<style type="text/css">
.fetchdataloader{border:16px solid #f3f3f3;border-radius:50%;border-top:16px solid #3498db;width:60px;float:right;height:60px;-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite}.select2-results__option[aria-selected=true]{display:none}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0)}100%{-webkit-transform:rotate(360deg)}}@keyframes spin{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php"); ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title"><b>Edit Documents</b></h5>
              </div>
              <div class="page-body">
                <?php
                 $sql = mysqli_query($connection, "SELECT * FROM `add_document` WHERE `doc_id`='".base64_decode($_GET['id'])."'") or mysqli_error($connection);
                 $rows = mysqli_fetch_array($sql);

                 $sql1 = mysqli_query($connection, "SELECT * FROM `add_document` WHERE `doc_id`='".base64_decode($_GET['id'])."' AND `image_remove`=1") or mysqli_error($connection);
                 $rows1 = mysqli_fetch_array($sql1);


                   if(isset($_POST['submit'])){
                   extract($_POST);
                      $images = $_FILES["image"]["name"];
                      if($images!=""){
                          $target_dir = "upload_document/";
                          $ext = pathinfo($images, PATHINFO_EXTENSION);
                          $time = time($images).'_'.$images;
                          $target_file = $target_dir . $time;
                          $size = $_FILES["image"]["size"];
                          $up = move_uploaded_file($_FILES["image"]["tmp_name"], $target_file); 
                        }else{
                          $time = $_POST['imageId'];
                        }
                       if($time!=""){
                           $insert = mysqli_query($connection, "UPDATE `add_document` SET `case_id`='".$case_id."',`type`='".$case_type."',`size`='".$size."',`title`='".$ext."',`link_document`='".$documents."',`term`='".$term."',`description`='".$description."',`judgment_date`='".$judgment_date."',`expiry_date`='".$expiry_date."',`purpose`='".$purpose."',`first_party`='".$first_pary."',`second_party`='".$second_party."',`headed_by`='".$headed_by."',`image`='".$time."',`date_time`='".date("Y-m-d H:i:s")."',`case_no`='".$case_no."' WHERE `doc_id`='".base64_decode($_GET['id'])."'");
                           if($insert){ ?>
                            <script type="text/javascript">
                             window.location.href='manage-document.php';
                            </script>
                           <?php } }else{ $meg = "Please Attach Documnt"; } } ?>
                <div class="card form-group">
                  <div class="card-block">
                    <form class="forms-sample row" method="POST" autocomplete="off" enctype="multipart/form-data">
                      <div class="col-sm-6 form-group row file-div" style="">
                        <div class="col-sm-4 file-text" style="">
                          <span style="color: red">*&nbsp;</span>Click on choose files to select one or many files:</div>
                        <div class="col-sm-8 file-iput" style="">
                          <div style="position:relative;">
                            <input type="hidden" value="<?php echo $rows1['image'];?>" name="imageId" >
                            <br>
                            <b style="color: red;"><?php echo $meg; ?></b>
                            <a class='btn btn-primary'>
                                <input type="file" class="col-sm-12" value="<?php echo $rows1['image'];?>" name="image"  size="30">
                            </a>
                            &nbsp;
                            <br/>
                            <span class='' id="upload-file-info" style="color: black;"></span>
                            <?php if($rows1['image']!=""){?>
                            <a href="upload_document/<?php echo $rows1['image'];?>" download>
                                  <i class="feather icon-file-pdf-o pdfcolor"></i><?php echo $rows1['image'];?></a>&nbsp;&nbsp;<a href="remove_document?id=<?php echo base64_encode($rows1['doc_id']);?>" onclick="return confirm('Are you sure you want to Remove Document?');"><b style="color: red;">Remove </b></a>
                                  <?php } ?>
                            <!-- <?php $ext = pathinfo($rows['image'], PATHINFO_EXTENSION); if($ext=='pdf'){  "<img src='images/pdf.png'"; }?><?php echo $rows['image'];?> -->
                    </div>
                        </div>
                      </div>
                      <div class="form-group col-sm-6 row type-div">
                        <label for="exampleInputUsername1" class="col-sm-4 col-form-label" style=""><span style="color: red;">*</span>&nbsp;&nbsp;Document Type</label>
                        <div class="col-sm-8">
                          <select class="form-control" id="type-select" name="case_type" required="">
                            <option value="<?php echo $rows['type'];?>"><?Php if($rows['type']!=""){ echo $rows['type']; }else{?>Please select<?php } ?></option>
                            <?php $document = mysqli_query($connection, "SELECT * FROM `documents_type` ORDER BY documents_type ASC");
                            while($seldocmnet = mysqli_fetch_array($document)){?>
                              <option value="<?php echo addslashes($seldocmnet['documents_type']);?>"><?php echo addslashes($seldocmnet['documents_type']);?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group col-sm-6 row document-div" style="">
                        <div class="col-sm-4">Would you like to link these documents with any of the cases: </div>
                        <div class="form-radio col">
                          <div class="radio radio-primary radio-inline">
                            <label>
                              <input type="radio" id="chkYes" name="documents" value="Yes" <?php if($rows1['link_document']=='Yes'){echo "checked";}?>>  
                              <i class="helper"></i>Yes
                            </label>
                          </div>
                          <div class="radio radio-primary radio-inline">
                            <label>
                              <input class="form-check form-chk" type="radio" id="chkNo" name="documents" value="No" <?php if($rows1['link_document']=='No'){echo "checked";}?>>      
                              <i class="helper"></i>No 
                            </label>
                          </div>
                        </div>
                      </div>
<!--        <div class="col-sm-6 form-group" style="display: none;" id="title_select" >
                        <label for="exampleInputUsername1" class="col-sm-4 col-form-label" style=""><span style="color: red;">*</span>&nbsp;&nbsp;Case Number</label>
                        <div class="col-sm-8 form-group">
                          <select class="form-control" id="document-select" name="case_id">
                          <option value="<?php echo $rows['case_id'];?>"><?php if($rows['case_id']!=""){
                           $regcase = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".$rows['case_id']."'");
                            $selcases = mysqli_fetch_array($regcase);
                             if($selcases['judicial'] == 'Non-Judicial'){
                            $case= $selcases['client_name'];
                          }else{
                          if($selcases['supreme_court'] == 'Diary Number')
                                      $case= $selcases['diary_no'].'/ '.$selcases['diary_year'].'/ '.$selcases['case_title'];
                                    else
                                    $case= $selcases['case_type'].' '.$selcases['case_no'].'/ '.$selcases['case_no_year'].'/ '.$selcases['case_title'];}

                                     echo $case; }else{?>Please Select Case <?php } ?></option>
                          <?php  
                         $casedocuments = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE ".$conditionquery."");
                        while($selcses = mysqli_fetch_array($casedocuments)){
                          if($selcses['judicial'] == 'Non-Judicial'){
                            $case= $selcses['client_name'];
                          }else{
                          if($selcses['supreme_court'] == 'Diary Number')
                                      $case= $selcses['diary_no'].'/ '.$selcses['diary_year'].'/ '.$selcses['case_title'];
                                    else
                                    $case= $selcses['case_type'].' '.$selcses['case_no'].'/ '.$selcses['case_no_year'].'/ '.$selcses['case_title'];}?>
                          <option value="<?php echo $selcses['case_id'];?>"><?php echo $case?> </option>
                        <?php } ?>
                      </select>
                        </div>
                      </div> -->


                      <div class="form-group col-sm-6 row type-div" style="display: none;" id="title_select">
                        <label for="exampleInputUsername1" class="col-sm-4 col-form-label" style=""><span style="color: red;">*</span>&nbsp;&nbsp;Case Number</label>
                        <div class="col-sm-8">
                                     <select class="form-control" id="document-select" name="case_id">
                          <option value="<?php echo $rows['case_id'];?>"><?php if($rows['case_id']!=""){
                           $regcase = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".$rows['case_id']."'");
                            $selcases = mysqli_fetch_array($regcase);
                             if($selcases['judicial'] == 'Non-Judicial'){
                            $case= $selcases['client_name'];
                          }else{
                          if($selcases['supreme_court'] == 'Diary Number')
                                      $case= $selcases['diary_no'].'/ '.$selcases['diary_year'].'/ '.$selcases['case_title'];
                                    else
                                    $case= $selcases['case_type'].' '.$selcases['case_no'].'/ '.$selcases['case_no_year'].'/ '.$selcases['case_title'];}

                                     echo $case; }else{?>Please Select Case <?php } ?></option>
                          <?php  
                         $casedocuments = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE ".$conditionquery."");
                        while($selcses = mysqli_fetch_array($casedocuments)){
                          if($selcses['judicial'] == 'Non-Judicial'){
                            $case= $selcses['client_name'];
                          }else{
                          if($selcses['supreme_court'] == 'Diary Number')
                                      $case= $selcses['diary_no'].'/ '.$selcses['diary_year'].'/ '.$selcses['case_title'];
                                    else
                                    $case= $selcses['case_type'].' '.$selcses['case_no'].'/ '.$selcses['case_no_year'].'/ '.$selcses['case_title'];}?>
                          <option value="<?php echo $selcses['case_id'];?>"><?php echo $case?> </option>
                        <?php } ?>
                      </select>
                        </div>
                      </div> 
                    <div class="col-sm-12 row form-group">
                      <div class="col-sm-6 row form-group">
                        <!-- <div class="col-sm-12 row form-group">
                          <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;Term:</label>
                          <div class="col-sm-8">
                           <input type="text" class="form-control" placeholder="Term" name="term" value="<?php echo $rows['term'];?>">
                         </div>
                       </div> -->
                       <!-- <div class="col-sm-12 row">
                        <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;Jugdment Date:</label>
                        <div class="col-sm-8 input-group date">
                         <input type="text" class="form-control" id="datepicker" placeholder="Select Jugdment Date" name="judgment_date" value="<?php echo $rows['judgment_date'];?>">
                         <span class="input-group-addon ">
                          <span class="icofont icofont-ui-calendar"></span>
                        </span>
                      </div>
                    </div> -->
                    <div class="col-sm-12 row form-group">
                      <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;  Purpose:</label>
                      <div class="col-sm-8">
                       <input type="text" class="form-control" placeholder="Purpose" name="purpose" value="<?php echo $rows['purpose'];?>">
                     </div>
                   </div>
                  <!--  <div class="col-sm-12 row form-group">

                     <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;  First Party:</label>
                <div class="col-sm-8">
                 <input type="text" class="form-control" name="first_pary" placeholder="First Party" value="<?php echo $rows['first_party'];?>">
               </div>
               
                 </div> -->
               </div>
               <div class="col-sm-6 row form-group">
                 <div class="col-sm-12 row form-group">
                  <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;  Description:</label>
                  <div class="col-sm-8">
                   <textarea class="form-control" placeholder="Description" name="description"><?php echo $rows['description'];?></textarea>
                 </div>
               </div>
               <!-- <div class="col-sm-12 row">
                <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;  Expiry Date:</label>
                <div class="col-sm-8 input-group date">
                  <input type="text" class="form-control" id="datepicker1" placeholder="Select Expiry Date" name="expiry_date" value="<?php echo $rows['expiry_date'];?>">
                  <span class="input-group-addon ">
                    <span class="icofont icofont-ui-calendar"></span>
                  </span>
                </div>
              </div> -->
              <!-- <div class="col-sm-12 row form-group">
               
                    <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;  Second Party:</label>
                    <div class="col-sm-8">
                     <input type="text" class="form-control" placeholder="Second Party" name="second_party" value="<?php echo $rows['second_party'];?>">
                   </div>
             </div> -->
             <!-- <div class="col-sm-12 row form-group">
              <label for="exampleInputUsername1" class="col-sm-4 col-form-label"><span style="color: red;"></span>&nbsp;&nbsp;  Headed By:</label>
              <div class="col-sm-8">
               <input type="text" class="form-control" name="headed_by" placeholder="Headed By" value="<?php echo $rows['headed_by'];?>">
             </div>
           </div> -->
         </div>
       </div>
       <div class="col-sm-6 row form-group" style="float: right;">
        <input type="submit" class="btn btn-sm btn-info newsubmit" value="Upload" name="submit">&nbsp;
        <a href="cancel-document?id=<?php echo $_GET['id'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you Want to Cancel?');">Cancel</a> 
      </div>
    </form>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/add_documents.js"></script>






