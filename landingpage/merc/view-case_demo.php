<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<?php $pageNam =  basename($_SERVER["PHP_SELF"]);
   $insert = mysqli_query($connection, "INSERT INTO `frequency_report` SET `user_id`='".$_SESSION['user_id']."',`case_id`='".base64_decode($_GET['caseid'])."',`division`='".$_SESSION['cityName']."',`page_name`='".$pageNam."',`date_time`='".date("Y-m-d H:i:s")."',`dates`='".date("Y-m-d")."'");?>
<!-- <link rel="stylesheet" href="css/vertical-layout-light/add_cases.css"> -->
<link rel="stylesheet" type="text/css" href="styles/view_case.css">
<!-- <link rel="stylesheet" type="text/css" href="styles/j-forms.css"> -->
<link rel="stylesheet" type="text/css" href="styles/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="styles/jquery.datetimepicker.css">
<style type="text/css">
   .mobile-view-tr{
   display: none;
   }
   .action_th{
      width: 13%!important;
   }
</style>
<body>
   <?php $case = mysqli_query($connection, "SELECT `case_id`,`case_no`,`court_id`,`case_title`,`user_id` FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
      $selcase = mysqli_fetch_array($case);
      $get_cases = mysqli_query($connection,"SELECT * FROM `case_law_search` WHERE `case_id`='".base64_decode($_GET['caseid'])."' AND `remove_status`=1");
      $count_law = mysqli_num_rows($get_cases);
      $todo_cases = mysqli_query($connection,"SELECT DISTINCT `todo_id` FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' ORDER BY todo_id DESC");
      $cnt_todocases = mysqli_num_rows($todo_cases);?>
   <div class="pcoded-main-container">
   <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <div class="pcoded-content">
         <div class="pcoded-inner-content">
            <div class="main-body">
               <div class="page-wrapper">
                  <div class="page-header m-b-10">
                     <h5 class="card-title"></h5>
                     <div class="page-body">
                        <div class="card form-group">
                           <div class="row">
                              <div id="complete_view_data">
                                 <h2 id="case_type_export_report" style="text-align: center;"></h2>
                                 <div id="details_main_left"></div>
                                 <div id="team-member_export_report"></div>
                                 <div id="activity_export_report"></div>
                                 <div id="hearindata_export_report"></div>
                                 <div id="documentList_export"></div>
                                 <div id="connected_case_export"></div>
                                 <div id="timesheet_export"></div>
                              </div>
                              <div class="card-header col-sm-12 row form-group p-0">
                                 <h4 class="col-sm-10 p-0 court_title_heading">
                                    <b id="district_court_title"></b>
                                 </h4>
                                 <div class="col-sm-2 p-0 form-group">
                                    <div class="col-sm-12 row p-0" id="right-side-div-view" style="margin: 0px">
                                       <h5 id="closed-status"></h5>
                                       <div class="col-sm-12 p-0">
                                          <select class="form-control" id="set_priority_select">
                                             <option value="">Set priority</option>
                                             <option value="Super Critical">Super Critical</option>
                                             <option value="Critical">Critical</option>
                                             <option value="Important">Important</option>
                                             <option value="Routine">Routine</option>
                                             <option value="Others">Others</option>
                                             <option value="Normal">Normal</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-2 form-group p-r-0" style="text-align: left;">
                                 <a href="edit-case.php?caseid=<?php echo base64_encode($selcase['case_id']); ?>" class="p-0 btn btn-link btn-md left_buttons viewcasebuttons" style="color: black"><i class="feather icon-edit viewcasebuttons" aria-hidden="true"></i> Edit</a>
                                 <?php if($selcase['user_id'] == $_SESSION['user_id']){?><button type="button" style="color: black" onclick="deletecase(<?php echo $selcase['case_id']; ?>)" class="p-0 left_buttons btn btn-link btn-md viewcasebuttons" ><i class="feather icon-trash viewcasebuttons"></i> Delete </button><?php } ?>
                                 <a href="add-cases.php" style="color: black" class="p-0 btn btn-link btn-md left_buttons viewcasebuttons" ><i class="feather icon-plus viewcasebuttons" aria-hidden="true"></i> Add </a>
                                 <!-- <span style="cursor:pointer" class="opensideview" onclick="openNav()">&#9776; Click to open side panel</span> -->
                                 <!-- <div id="mySidenav" class="sidenav">
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                    <div class="court_data"></div>
                                    <div class="casetype"></div>
                                    <div class="stage"></div>
                                    <div class="petitioner"></div>
                                    <div class="petitionerAdv"></div>
                                    <div class="respondent"></div>
                                    <div class="respondent_adv"></div> 
                                    <div class="history_date"></div> 
                                    </div> -->
                                 <div class="col-sm-12 p-0">
                                    <a href="cases-report.php" class="btn btn-warning btn-sm left_buttons viewcasebuttons"><i class="feather icon-file-text-o viewcasebuttons" aria-hidden="true"></i> Manage Cases</a>
                                 </div>
                              </div>
                              <div class="col-sm-8 form-group" style="text-align: center;">
                                 <h4 class="fetch_case_title" id="sub_case_nominal" style="display: none;"></h4>
                                 <h4 class="fetch_case_title p-0" id="sub_case_title"><?php echo $selcase['case_title'];?></h4>
                                 <h5 class="fetch_case_desc p-0" id="sub_case_desc"></h5>
                              </div>
                              <div class="col-sm-2" id="auto-update-div" style="text-align: right;"><span>Auto Update</span>
                                 <label class="toggle-switch toggle-switch-danger">                
                                 <input type="checkbox" checked="" id="auto_update" onchange="getAutoUpdate()">
                                 <span class="toggle-slider round"></span>
                                 </label>
                              </div>
                              <input type="hidden" name="caseid" id="caseid" value="<?php echo base64_decode($_GET['caseid']);?>">
                              <input type="hidden" name="courtid" id="court-id" value="<?php echo $selcase['court_id'];?>">
                              <input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>">
                              <input type="hidden" name="stamp" id="stamp" value="">
                              <input type="hidden" name="side" id="side" value="">
                              <input type="hidden" name="bench" id="bench" value="">
                              <input type="hidden" name="casetype" id="case_type" value="">
                              <input type="hidden" name="case_no_year" id="case_no_year" value="">
                              <input type="hidden" name="case_no" id="case_no" value="">
                              <h3 class="card-title" style="font-size: 20px;"></h3>
                              <div class="col-sm-12">
                                 <ul class="nav nav-tabs form-group" role="tablist">
                                    <li class="nav-item">
                                       <a class="nav-link active" id="case-details-tab" data-toggle="tab" href="#activity-case-details" role="tab" aria-controls="home-1" aria-selected="true">प्रकरण तपशील / Case Details</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" id="case-details-tab" data-toggle="tab" href="#party-details" role="tab" aria-controls="home-1" aria-selected="true">पक्ष / Parties</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" id="activity-tab" data-toggle="tab" href="#activity-history" role="tab" aria-controls="home-1" aria-selected="true" onclick="view_case_details()">याचिकेची संक्षिप्त माहिती / Case History</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" id="team-member-tab" data-toggle="tab" href="#team-member" role="tab" aria-controls="profile-1" aria-selected="false">संघ सदस्य / Team Members (<span id="team_member_count"></span>)</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" onclick="getUploadedDoc('d')" id="document-tab" data-toggle="tab" href="#documents_li" role="tab" aria-controls="contact-1" aria-selected="false">कागदपत्रे / Documents (<span id="view_case_document"></span>)</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link " id="note-tab" data-toggle="tab" href="#note-1" role="tab" aria-controls="contact-1" aria-selected="false">टिपणी / Notes <span id="notes-counter">(0)</span></a>
                                    </li>
                                    <?php
                                    $casetimessheet = mysqli_query($connection, "SELECT `case_id` FROM `add_timesheet` WHERE `case_id`='".base64_decode($_GET['caseid'])."'"); 
                                     $cnttime=mysqli_num_rows($casetimessheet);
                                    ?>
                                    <li class="nav-item" id="timesheet-li">
                                       <a class="nav-link" onclick="getTimesheet()" id="timesheet-tab" data-toggle="tab" href="#timesheet" role="tab" aria-controls="contact-1" aria-selected="false">वेळ पत्रक / TimeSheet(<?php echo $cnttime; ?>)</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link " id="to-do-tab" data-toggle="tab" href="#to-do" role="tab" aria-controls="contact-1" aria-selected="false">स्मरणपत्र / Reminder(<?php echo $cnt_todocases;?>)</a>
                                    </li>
                                    <li class="nav-item" id="connected-cases-li">
                                       <a class="nav-link " id="connected-tab" data-toggle="tab" href="#connected-cases" role="tab" aria-controls="contact-1" aria-selected="false" onclick="uploadConnectedCases('refresh')">जोडलेली प्रकरणे / Connected  Cases (<span id="connected-case"></span>)</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link " id="order-judgement-tab" data-toggle="tab" href="#order-cases" role="tab" aria-controls="contact-1" aria-selected="false" onclick="order()">ऑर्डर निर्णय / Orders Judgements</a>
                                    </li>
                                    <li class="nav-item tab-seeklegal">
                                       <a class="nav-link " id="seeklegaltab" onclick="getallquery(1)" data-toggle="tab" href="#seek-legal-help" role="tab" aria-controls="contact-1" aria-selected="false">कायदेशीर मदत घ्या / Seek Legal Help
                                       <span class="count_of_clr" id="clr_count"></span></a>
                                    </li>
                                    <li class="nav-item" id="relevant-case-li">
                                       <a class="nav-link " id="cases-referred-tab" data-toggle="tab" href="#cases-referred" role="tab" aria-controls="contact-1" aria-selected="false" onclick="getCasesReffered()">संबंधित प्रकरण कायदा / Relevant Case Law(<?php echo $count_law;?>)</a>
                                    </li>
                                 </ul>
                                 <div class="tab-content">
                                    <div class="tab-pane fade active show" id="activity-case-details">
                                       <div id="desktop-view-div">
                                          <div class="sidebar-case-section utype-lawyers row">
                                             <div class="court_data col-sm-4"></div>
                                             <div class="casetype col-sm-4">                       
                                             </div>
                                             <div class="history_date col-sm-4"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="party-details">
                                       <div id="desktop-view-div">
                                          <div class="sidebar-case-section row utype-lawyers">
                                             <div class="petitioner col-sm-4" style="display: none;"></div>
                                             <div class="petitionerAdv col-sm-4" style="display: none;"></div>
                                             <div class="respondent col-sm-4" style="display: none;"></div>
                                             <div class="respondent_adv col-sm-4" style="display: none;"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade " id="activity-history" role="tabpanel" aria-labelledby="home-tab">
                                       <div class="media col-sm-12 p-l-0">
                                          <div class="media-body col-sm-12 p-l-0">
                                             <div class="form-group row" style="float: right;">
                                                <select class="form-control" style="width: 104px;" id="export-to-pdf">
                                                   <option disabled selected hidden>Export</option>
                                                   <option value="exprt">as a PDF</option>
                                                </select>
                                                <button style="border: 0px;padding: 9px 12px;background-color: #dddddd;margin-left: 10px;">Filter</button>
                                             </div>
                                             <div id="show_view_case_default"></div>
                                             <div id="show_view_case"></div>
                                             <div id="show_hearing_data_list" class="table-responsive"></div>
                                             <textarea name="seeklegal" id="query" ></textarea>
                                             <table id="inform-to-client-tbl" class="table table-bordered">
                                                <tbody>
                                                   <tr>
                                                      <th class="client_inform">Client Name</th>
                                                      <th class="email_inform">Email Address</th>
                                                      <th class="phone_inform">Phone Number
                                                         <span class="sms-helper-info">(Maximum character limit is 1,500 for SMS)</span>
                                                      </th>
                                                   </tr>
                                                   <tr id="artivity-inform-client-wrapper-activity-client-87642" class="disabled ">
                                                      <td data-th="Client">1) Triratna Baudha Mahasangh Sahayak Gan1</td>
                                                      <td data-th="Email Address">
                                                         <input type="checkbox" disabled><label for="email-activity-client-87642"> (no email)</label>
                                                      </td>
                                                      <td data-th="Phone Number (Max character limit is 1,500 for SMS)">
                                                         <input type="checkbox" disabled>
                                                         <label for="mobile-activity-client-87642"> (no phone number)</label>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                             <div class="col-sm-12 row p-0">
                                                <b class="col-sm-5 d-flex">Would you like to record next hearing date or resolution?</b>
                                                <div class="form-radio">
                                                   <div class="radio radio-inline">
                                                      <label>
                                                      <input type="radio" class="chkPassport" name="recordHearingradio" value="Yes">
                                                      <i class="helper"></i> Yes
                                                      </label>
                                                   </div>
                                                   <div class="radio radio-inline">
                                                      <label>
                                                      <input type="radio" class="chkPassport" name="recordHearingradio" value="No" checked>
                                                      <i class="helper"></i> No
                                                      </label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="case-activity-info" style="">
                                                <div class="form-group col-sm-3" style="">
                                                   <label class="control-label" for="caseactivityinfo-case_status">Status:</label>
                                                   <div class="selector hover" id="uniform-caseactivityinfo-case_status">
                                                      <select id="record_status" class="form-control" name="CaseActivityInfo[case_status]" style="">
                                                         <option value="Next Hearing" selected="">Running</option>
                                                         <option value="Closed">Closed</option>
                                                         <option value="Transferred/NOC">Transferred/NOC Given</option>
                                                         <option value="Direction Matter">Direction Matters</option>
                                                         <option value="Order">Order Reserved</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <div class="row col-sm-12" error="dd-error">
                                                   <label class="control-label col-sm-2 d-flex">Who Attended:</label>
                                                   <div id="who_attended_div"></div>
                                                   <!-- <div class="form-check col-sm-2">
                                                      <label class="form-check-label checkboxlabel">
                                                      	<input type="checkbox" class="chkPassport" value="Others" name="who_attended">
                                                      	Others
                                                      	<i class="input-helper"></i>
                                                      </label>
                                                      </div> -->
                                                   <div class="col-sm-4" id="who_attended_others_div">
                                                      <input type="text" class="form-control" id="who_attended_others" name="">
                                                   </div>
                                                </div>
                                                <div class="col-sm-12 row">
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" for="caseactivityinfo-stage">Stage:</label>
                                                      <input type="text" id="caseactivityinfo-stage" class="form-control" name="CaseActivityInfo[stage]" maxlength="255">
                                                   </div>
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" for="caseactivityinfo-posted_for">Posted For:</label>
                                                      <input type="text" id="caseactivityinfo-posted_for" class="form-control" name="CaseActivityInfo[posted_for]" maxlength="255">
                                                   </div>
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" for="caseactivityinfo-action_taken">Action Taken:</label>
                                                      <input type="text" id="caseactivityinfo-action_taken" class="form-control" name="CaseActivityInfo[action_taken]" maxlength="255">
                                                   </div>
                                                   <div class="form-group col-sm-4">
                                                      <label class="control-label" id="hearing-date-label"> Next Hearing Date:</label>
                                                      <input type="text" id="caseactivityinfo-next_hearing" class="form-control">
                                                   </div>
                                                   <div class="form-group col-sm-4" style="">
                                                      <label class="control-label" for="caseactivityinfo-sessions">Session:</label>
                                                      <div class="selector" id="uniform-caseactivityinfo-sessions">
                                                         <select id="caseactivityinfo-sessions" class="form-control">
                                                            <option value="">Please Select</option>
                                                            <option value="1">Morning</option>
                                                            <option value="2">Evening</option>
                                                         </select>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <button class="btn btn-primary btn-sm" type="button" style="float: right;" onclick="submitCaseComment('submit')"> Submit</button>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="team-member" role="tabpanel" aria-labelledby="tm-tab">
                                       <div class="media">
                                          <input type="hidden" id="hidSelectedOptions" value="1,44" name="">
                                          <div class="media-body col-sm-12 table-responsive" id="team-member-table-div">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade"  id="documents_li" role="tabpanel" aria-labelledby="document-tab" >
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <div class="tab-pane" id="documenttab" role="tabpanel" aria-labelledby="documenttab">
                                                <form method="post" action="#" id="#">
                                                   <div class="form-group files">
                                                      <label>Upload Your File </label>
                                                      <input type="file" class="form-control form-group" multiple="" id="upload_doc"  accept="image/x-png,image/gif,image/jpeg" />
                                                   </div>
                                                   <div class="form-group row col-sm-12">
                                                      <div class="col-sm-4">
                                                         <select class="form-control" id="doc_type" name="doc_type">
                                                            <option value="">Please select document type</option>
                                                            <?php $document = mysqli_query($connection, "SELECT * FROM `documents_type` ORDER BY documents_type ASC");
                                                               while($seldocmnet = mysqli_fetch_array($document)){?>
                                                            <option value="<?php echo addslashes($seldocmnet['documents_type']);?>"><?php echo addslashes($seldocmnet['documents_type']);?></option>
                                                            <?php } ?>
                                                         </select>
                                                      </div>
                                                      <div class="col-sm-3">
                                                         <button class="btn btn-sm btn-primary newsubmit"  type="button" id="upload_documents" value="Upload" name="Upload" onclick="getUploadedDoc('u')">Upload</button>
                                                      </div>
                                                   </div>
                                                   <div id="documentList" class="table-responsive col-sm-12"></div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="note-1" role="tabpanel" aria-labelledby="note-tab">
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <div class="row col form-group">
                                                <input type="hidden" value="<?php echo $_GET['caseid']; ?>" id="caseid"/>
                                                <textarea rows="5" id="notes" name="notes" class="form-control"></textarea>						
                                             </div>
                                             <div class="row">
                                                <div class="col row">
                                                   <b>Mark this note as private </b>
                                                   <div class="form-radio">
                                                      <div class="radio radio-inline">
                                                         <label>
                                                         <input type="radio" name="np" value="yes">
                                                         <i class="helper"></i> Yes
                                                         </label>
                                                      </div>
                                                      <div class="radio radio-inline">
                                                         <label>
                                                         <input type="radio" name="np" value="no">
                                                         <i class="helper"></i> No
                                                         </label>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="col">
                                                   <button class="btn btn-danger btn-sm" type="button" id="btnNoteClear">Clear</button>
                                                   <button class="btn btn-primary btn-sm" type="button" id="btnNoteSave">Submit</button>
                                                </div>
                                             </div>
                                             <div  id="note_block">
                                                <?php
                                                   $sql = "SELECT `law_registration`.`name`,`law_registration`.`last_name`,`notes`.`userid`,`notes`.`caseid`,`notes`.`note`,`notes`.`type`,`notes`.`insert_date`,`notes`.`id` FROM `notes` LEFT JOIN law_registration ON law_registration.reg_id = `notes`.`userid` WHERE `userid`='" . $_SESSION['user_id'] . "' AND `caseid`='" . base64_decode($_GET['caseid']) . "' ORDER BY `notes`.`insert_date` DESC;";
                                                                             //echo "<pre>";
                                                                             //echo $sql;
                                                                             //echo "</pre>";
                                                   if ($result = mysqli_query($connection, $sql)) {
                                                   
                                                   	while ($row = mysqli_fetch_array($result)) {
                                                   		?>
                                                <div class="row content" id="note_element_<?php echo $row['id']; ?>">
                                                   <div class="col"  style="border:1px solid #ccc;background: #fff;margin-top:10px;" >
                                                      <div class="row" style="background: #fff;">
                                                         <div class="col col-md-50">
                                                            <div class="row justify-content-end" style="background: #fff;">
                                                               <div class="col case-note-item-actions" style="text-align:right;padding:5px;">
                                                                  <span id="note-creater" style="border-right: 1px solid;padding-right: 6px;"><?php echo $row['name'] . " " . $row['last_name']; ?></span>
                                                                  <span id="note-created-date" style="border-right: 1px solid;padding-right: 6px;"><?php echo $row['insert_date']; ?></span>
                                                                  <span class="download-item" style="padding:5px"> <a  href="ajaxdata/downloadnote.php?noteid=<?php echo $row['id']; ?>" target="_self" class="sprite download_icon" title="Download" data-pjax=""><i class="fa fa-download" aria-hidden="true"></i>
                                                                  </a></span>  
                                                                  <span class="edit-item" style="padding:5px">
                                                                  <input type="hidden" value="<?php echo $row['type']; ?>" id="type_<?php echo $row['id']; ?>" />
                                                                  <a onclick="editNote(<?php echo $row['id']; ?>)" class="case-notes-edit sprite edit-icon" title="Edit" data-ntype-checked="false"> 
                                                                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                  </a></span>
                                                                  <span class="delete-item" style="padding:5px">
                                                                  <a onclick="deleteNote(<?php echo $row['id']; ?>)" class="case-notes-delete sprite delete_icon" title="Delete" data-requestrunning="false"> 
                                                                  <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                                  </a></span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="row" style="background: #fff;">
                                                         <div class="col noteTextDetail" style="padding:5px;" id="noteText_<?php echo $row['id']; ?>">
                                                            <?php echo $row['note']; ?>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <?php   } } ?>
                                                <div class="content"></div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="seek-legal-help" role="tabpanel" aria-labelledby="seeklegaltab">
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <?php include("seek_legal.php");?>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="cases-referred" role="tabpanel">
                                       <div class="media">
                                          <div class="media-body col-sm-12 table-responsive" id="show_lawdata">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="timesheet" role="tabpanel" aria-labelledby="seeklegaltab">
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <?php include 'timesheet.php' ?>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="connected-cases" role="tabpanel" aria-labelledby="rc-tab">
                                       <?php include('connected_cases_add.php');?>
                                    </div>
                                    <div class="tab-pane fade" id="order-cases" role="tabpanel" aria-labelledby="rc-tab">
                                       <div class="media">
                                          <div class="table-responsive" id="order_cases_table">
                                          </div>
                                       </div>
                                    </div>
                                     <div class="tab-pane fade" id="exp-1" role="tabpanel" aria-labelledby="exp-tab">
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <div>
                                                <button class="btn btn-outline-warning btn-priamry btn-sm" type="button">Filter</button>
                                                <button class="btn btn-outline-dark btn-warning btn-sm" type="button">Export</button>
                                                <button class="btn btn-info btn-sm" type="button">Add New</button>
                                             </div>
                                             <h4><b>My August 2019 Expenses</b></h4>
                                             <table class="table" border="0.25">
                                                <thead>
                                                   <tr>
                                                      <th>Sr. no.</th>
                                                      <th>Date</th>
                                                      <th>Particulars</th>
                                                      <th>Money Spent(in INR)</th>
                                                      <th>Payment Method</th>
                                                      <th>Member</th>
                                                      <th>Actions</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="fee-1" role="tabpanel" aria-labelledby="fee-tab">
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <div class="col-sm-6" style="float: right;">
                                                <button class="btn btn-outline-warning btn-sm" type="button">Filter</button>
                                                <button class="btn btn-outline-dark btn-sm" type="button">Export</button>
                                                <button class="btn btn-info btn-sm" type="button">Add New</button>
                                             </div>
                                             <h4><b>My August 2019 Fee Recived</b></h4>
                                             <div class="table-responsive">
                                                <table class="table table-bordered">
                                                   <thead>
                                                      <tr>
                                                         <th>Sr. no.</th>
                                                         <th>Date</th>
                                                         <th>Particulars</th>
                                                         <th>Fee Recived(in INR)</th>
                                                         <th>Payment Method</th>
                                                         <th>Member</th>
                                                         <th>Actions</th>
                                                      </tr>
                                                   </thead>
                                                   <tbody>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="tab-pane fade" id="clr-1" role="tabpanel" aria-labelledby="clr-tab">
                                       <div class="media">
                                          <div class="media-body col-sm-12">
                                             <div class="card-body">
                                                <h4>Case Legal Research Response</h4>
                                                <hr/>
                                                <div class="bread_crumbs" style="text-align: center;">
                                                   <!--      <span class="replyheading">Case Legal Research Response</span>  -->
                                                </div>
                                                   <div class="col-sm-12">
                                                      <!-- <div class="col-sm-12"> -->
                                                      <div class="panel panel-primary">
                                                         <div class="">
                                                            <div class="panel-body">
                                                               <ul class="chat" style="font-size: 14px;">
                                                                  <li class="left ">
                                                                     <span class="chat-img pull-left" style="float: left;">
                                                                        <!-- <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" /> -->
                                                                     </span>
                                                                     <div class="chat-body ">
                                                                        <?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                                                                           $row = mysqli_fetch_array($fetch);?>
                                                                        <input type="hidden" id="email" value="MHMLS-<?php echo base64_decode($_GET['caseid']);?>">
                                                                        <div class="header">
                                                                           <small class="pull-right text-muted" style="width: 100%; float: left;"> </small>
                                                                           <!-- <span class="glyphicon glyphicon-time"></span>12 mins ago</small> -->
                                                                           <!-- <strong class="primary-font" style="color: green;">Name Of Matter:</strong> <span>Contract Labour</span><br> -->
                                                                           <!-- <b> Document Type :</b><span>Notice</span><br> -->
                                                                           <div id="queryshow"></div>
                                                                        </div>
                                                                     </div>
                                                                  </li>
                                                               </ul>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="pagen"></div>
                                                      <!-- </div> -->
                                                   </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- //todo start -->
                                    <div class="tab-pane fade p-0" id="to-do" role="tabpanel" aria-labelledby="rc-tab">
                                       <?php include('todo_add.php');?>
                                    </div>
                                    
                                    
                                   
                                    
                                    
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal" id="view_case_document_modal">
      <div class="modal-dialog modal-lg">
         <div id="casedocument"></div>
      </div>
   </div>
   <?php include 'footer.php'; ?>
   <script type="text/javascript">
      $(".view-case-li").addClass('active pcoded-trigger');
   </script>
   <script src="scripts/aes.js"></script>
   <script src="scripts/pbkdf2.js"></script>
   <script type="text/javascript" src="scripts/jquery.datetimepicker.full.min.js"></script>
   <script type="text/javascript" src="scripts/jsapi.js"></script>
   <script type="text/javascript" src="scripts/view_case.js"></script>
   <script type="text/javascript" src="scripts/export_view_case.js"></script>
   <script type="text/javascript" src="scripts/seeklegal.js"></script>
   <script type="text/javascript" src="scripts/connected_cases_add.js"></script>
   <script type="text/javascript" src="scripts/timesheet.js"></script>
   <script src='scripts/create_todo.js' type='text/javascript'></script>
   <script src="scripts/jquery.dataTables.min.js"></script>
   <script src="scripts/dataTables.bootstrap4.min.js" ></script>