<?php include("header.php") ?>
<link rel="stylesheet" type="text/css" href="styles/calendar.css">
<link rel="stylesheet" href="styles/jquery.datetimepicker.css">
<style>
  td{
  	cursor: pointer;
  }
    .jump-input option[value='12'] {display: none !important;}
</style>
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header">
              <h5 class="card-title">Calendar</h5>
            </div>
            <div class="page-body">
              <div class="card form-group">
                <div class="card-block">
                  <div class="col-sm-12 form-group row button_div">
                   <!--  <form>
                      <select name="dropdown" class="form-control" id="dropdown">
                        <option value="All">All</option>
                        <option value="To-Do's">Reminders</option>
                        <option value="Hearing Dates">Hearing Dates</option>       
                      </select>
                    </form> -->
                    <form class="form-inline form-group col-sm-5 col-lg-5 col-md-8 col-xs-12">
                      <label class="lead mr-2 ml-2" for="month">Jump To: </label>
                      <div class="col p-0"><select class="form-control form-group jump-input" name="month" id="month" onchange="jump()">

                      </select></div>
                      <div class="col"><select class="form-control form-group" name="year" id="year" onchange="jump()">
                      </select></div></form>
                      <!-- <form>
                        <select name="dropdown" class="form-control" id="dropdown" style="display: none;">
                          <option value="All" selected="">All Sessions</option>
                          <option value="morning">Morning Session</option>
                          <option value="evening">Evening Session</option>       
                        </select>
                      </form> -->
                    </div>
                    <div class="col-sm-12 card">
                      <!-- <div id="full_calendar"></div> -->                      
                      <h3 class="card-header" id="monthAndYear"></h3>
                      <div class="col-sm-12 table-responsive">
                      <table class="table table-bordered" id="calendar">
                        <thead>
                          <tr>
                            <th>Sun</th>
                            <th>Mon</th>
                            <th>Tue</th>
                            <th>Wed</th>
                            <th>Thu</th>
                            <th>Fri</th>
                            <th>Sat</th>
                          </tr>
                        </thead>

                        <tbody id="calendar-body">

                        </tbody>
                      </table>
                    </div>
                      <div class="form-inline form-group">
                        <div class="col-sm-6">
                          <button class="btn btn-outline-primary" id="previous" onclick="previous()">Previous</button>
                        </div>
                        <div class="col-sm-6">
                          <button class="btn btn-outline-primary pull-right" id="next" onclick="next()">Next</button></div>
                        </div>


                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   <!--  <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
         
          <div class="modal-header hearingTodoHeader">
            <h4 class="modal-title">Please provide Reminder list or Hearing Date</h4>
            <button type="button" class="close hearingTodoclose" data-dismiss="modal">&times;</button>
          </div>

          
          <div class="modal-body hearingTodoData">
            <div class="col-sm-12 row">
              <label class="col-sm-1">Type</label>
              <div class="form-radio col">
                <div class="radio radio-primary radio-inline">
                  <label>
                    <input type="radio" name="new_event" checked value="hearing_radio">

                    <i class="helper"></i>Hearing
                  </label>
                </div>
                <div class="radio radio-primary radio-inline">
                  <label>
                    <input type="radio" name="new_event" value="todo_radio">
                    <i class="helper"></i>Reminder
                  </label>
                </div>
              </div>
              <label id="view_case_button" class="col-sm-3">
                <a id="view_case_anchor_id" href="" title="View" target="_blank">
                  <button type="button" class="btn btn-success btn-sm">View Case</button>
                </a>
              </label>
            </div>
            <form id="hearing_data">
              <div class="col-sm-12 row" id="hearing"> 
                <input id="hearing_data_id" name="id" value="" type="hidden"/>               
                <div class="col-sm-12 form-group">
                  <b>Belong To: </b>
                 
                  <select id="belong_to" class="select2 form-control" name="belong_to" style="width:100%;"></select>
                </div>
                <div class="col-sm-6 form-group">
                  <b>Stage: </b>
                 
                  <input id="stage" class="form-control" type="text" value="" name="stage" />
                </div>
                <div class="col-sm-6 form-group">
                  <b>Posted For: </b>
                  
                  <input id="posted_for" class="form-control" type="text" value="" name="posted_for" />
                </div>
                <div class="col-sm-6 form-group">
                  <b>Action Taken: </b>
                  
                  <input id="action_taken" class="form-control" type="text" value="" name="action_taken" />
                </div>
                <div class="col-sm-6 form-group">
                  <b>Next Hearing Date: </b>
                  
                  <input id="next_hearing_date" class="form-control" type="text" value="" onFocus="blur();" />
                  <input id="next_hearing_date_alt" type="hidden" value="" name="next_hearing_date" />
                </div>
                <div class="col-sm-6 form-group">
                  <b>Session: </b>
                  
                  <select id="session_phase" class="form-control" name="session_phase">
                    <option value="" selected>-- Please Select --</option>
                    <option value="1">Morning</option>
                    <option value="2">Evening</option>
                  </select>
                </div>
                <div class="col-sm-12 form-group">
                  <b>Who Attended: </b>
                  <div id="who_attended_record"></div>
                  
                  <input id="who_attended_other" class="form-control" type="text" value="" name="who_attended[]" style="display:none;" />
                </div>
                <div class="col-sm-12">
                  <textarea id="hearing_description" ></textarea>
                  <textarea id="hearing_description_data" name="hearing_description" style="display:none;"></textarea>                       
                </div>                    
              </div>
            </form>

            <form id="to_do_data">
              <div class="col-sm-12 row" id="to-do-list">
                <input id="todo_data_id" name="id" value="" type="hidden"/> 
                <div class="col-sm-12 form-group">
                  <b>Description </b>
                  <textarea id="desc" class="form-control" rows="4"></textarea>
                </div>
                <div class="col-sm-12 form-group">
                  <b class="col-sm-12" style="padding: 0">Would you like to assign this to-do to any case?
                  </b> &nbsp;

                  <div class="form-radio col">
                    <div class="radio radio-primary radio-inline">
                      <label>
                        <input type="radio" name="assign_other">
                        <i class="helper"></i>Yes
                      </label>
                    </div>
                    <div class="radio radio-primary radio-inline">
                      <label>
                       <input type="radio" name="assign_other" checked>
                       <i class="helper"></i>No
                     </label>
                   </div>
                 </div>
               </div>
               <div class="col-sm-12 form-group">
                <b>Relate To: </b>
                <select id="relate" name="framework[]" multiple="multiple"  class="form-control" placeholder="Please Select">
                  <option value="">Please Select</option>
                </select>

              </div>

              <div class="col-sm-12 form-group">
                <b>Assign To: </b>
                 
                <select id="framework" name="framework[]" multiple class="form-control" >

                </select>

              </div>
              <div class="col-sm-12 form-group row">
                <b class="col-sm-12">Please select due date: </b>
                <div class="col-sm-6">
                  <input type="text" id="due_date_from" class="form-control" />
                  <input type="hidden" id="due_date_from_alt" />
                </div>
                <div class="col-sm-1"><span>To</span></div>
                <div class="col-sm-5">
                  <input type="text" id="due_date_to" class="form-control" />
                  <input type="hidden" id="due_date_to_alt" />
                </div>
              </div>

              <div class="col-sm-12 form-group">
                <div class="checkbox-zoom zoom-primary">
                  <label>
                    <input type="checkbox" class="check" id="checkAll"  id="private"/>
                    <span class="cr">
                      <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                    </span>
                    <span>Mark As Private</span>
                  </label>
                </div>

              </div>

              <div class="col-sm-12">
                <b>Please set your auto reminders </b>
                <div class="form-group" id="reminder_clickhere">
                  <p><b id="reminder_click_id">Click here</b> to add auto reminders</p>
                </div>

                <div id="reminder_input" style="display: none;">
                  <div class="row" id="input">
                    <div class="col-sm-1">
                      <i id="removereminder_click_id" class='fa fa-minus-circle'></i>
                    </div>
                    <div class="col-sm-3">
                      <select class="form-control" id="email_option_0" name='minute[]'>
                       
                        <option value="Email">Email</option>
                        <option value="SMS">SMS</option>
                      </select>
                    </div>
                    <div class="col-sm-2">
                      <select class="form-control" id="reminder_format_value_0" name='subject[]'>
                        <option value='5'>5</option>
                        <option value='10'>10</option>
                        <option value='15'>15</option>
                        <option value='20'>20</option>
                        <option value='25'>25</option>
                        <option value='30'>30</option>
                        <option value='35'>35</option>
                        <option value='40'>40</option>
                        <option value='45'>45</option>
                        <option value='50'>50</option>
                        <option value='55'>55</option>
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <select class="form-control reminder_format" id="minute_0" name='minute[]'>
                       
                        <option value="minutes">minute(s)</option>
                        <option value="hours">hour(s)</option>
                        <option value="days">day(s)</option>
                        <option value="week">week(s)</option>
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <span style="margin-top: 8px;">before the due date & time</span>
                    </div>
                  </div>
                  <button class="btn btn-info btn-sm" type="button"><i id="add_field" class="fa fa-plus-circle"></i>Add an additional reminder</button>
                </div>
              </div>
            </div>
          </form>
        </div>

       
        <div class="modal-footer">
          <button type="button" id="submit_btn_id" class="btn btn-info btn-sm newsubmit">Submit</button>
          <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div> -->
<div class="modal" id="hearingdetailmodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">View Hearing Dates</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       <div class="col-sm-12 col-md-12 col-xs-12 col-lg-12 table-responsive" id="hearingdetails"></div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

  <?php include 'footer.php'; ?>

  <script type="text/javascript" src="scripts/jquery.datetimepicker.full.min.js"></script>
  <script type="text/javascript" src="scripts/index_calendar.js"></script>  
  <!-- <script type="text/javascript" src="hearing_calendar/index.js"></script> -->


