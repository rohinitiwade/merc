<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
 <body onload="multiply()"> 
      <?php include("menu.php");?>
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="card">
            <div class="card-body">
              <h4 style="color: red;">User Details</h4>
              <hr/>
              <form name="myForm" method="POST" autocomplete="OFF" action="pay.php">
               <div class="row">
                <div class="col-12">
              <div class="">
                          <div class="container-fluid mt-5 d-flex justify-content-center w-100">
                            <div class="table-responsive w-100">
                                <table class="table">
                                  <thead>
                                    <tr class="bg-dark text-white">
                                        <th>Sr.No.</th>
                                        <th>Advocate Name</th>
                                        <th>Email Id</th>
                                        <th class="text-right">User</th>
                                        <th class="text-right">Total</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                      $team =mysqli_query($connection,"SELECT * FROM team  INNER JOIN licenses ON team.id=licenses.team_id WHERE licenses.user_id='".$_SESSION['user_id']."' ORDER BY licenses.user_id DESC");
                                      $selteam = mysqli_fetch_array($team);?>
                                    <tr class="text-right">
                                      <td class="text-left">1</td>
                                      <td class="text-left"><?php echo $selteam['full_name'];?>  <?php echo $selteam['last_name'];?></td>
                                      <td class="text-left"><?php echo $selteam['email'];?></td>
                                      <td class="text-left"><?php echo $selteam['user'];?>  </td>
                                      <td class="text-left"><?php echo $selteam['total'];?></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                          </div>
                      </div>
                      <?php  $law_registration = mysqli_query($connection, "SELECT * FROM `team`  WHERE `user_id`='".$_SESSION['user_id']."' ORDER BY id DESC");
                             $selreg = mysqli_fetch_array($law_registration);?>
                      <div class="col-sm-12">
                        <div class="card">
                          <div class="card-header"><h4 style="color: red;">Billing Details</h4></div>
                          <div class="card-body">
                            <div class="form-group row">
                              <label for="exampleInputUsername2" class="col-sm-2 col-form-label"><b>First Name&nbsp;<span style="color: red;">*</span>&nbsp;: </b></label>
                              <div class="col-sm-4">
                                <input type="text" class="form-control name" value="<?php echo $selreg['name'];?>" id="full_name" placeholder="First Name" name="full_name" required="">
                              </div>
                              <label for="exampleInputEmail2" class="col-sm-2 col-form-label"><b>Last Name:</b></label>
                              <div class="col-sm-4">
                                <input type="text" class="form-control name" value="<?php echo $selreg['last_name'];?>" id="name" placeholder="Last Name" name="last_name">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="exampleInputMobile" class="col-sm-2 col-form-label"><b>Phone Number:</b></label>
                              <div class="col-sm-4">
                                <input type="text" class="form-control name" value="<?php echo $selreg['mobile'];?>" minlength="10" maxlength="10" id="mobile" placeholder="Phone Number" name="mobile" required="">
                              </div>  
                              <label for="exampleInputPassword2" class="col-sm-2 col-form-label"><b>Email Address</b></label>
                              <div class="col-sm-4">
                                <input type="email" class="form-control" id="age" value="<?php echo $selreg['email'];?>" placeholder="Email Address" name="email">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Address :</b></label>
                              <div class="col-sm-10">
                                <textarea class="form-control" cols="12" placeholder="Address" name="address"><?php echo $selreg['address'];?></textarea>
                              </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Country:</b></label>
                                <div class="col-sm-4">
                                  <select name="country" class="form-control">
                                    <option value="India">India</option>
                                  </select>
                                </div>
                                <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>State:</b></label>
                                <div class="col-sm-4">
                                  <select name="state" class="form-control" id="states">
                                    <?php $selsattes = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `id`='".$selreg['state']."'"); 
                                    $slectoption = mysqli_fetch_array($selsattes);?>
                                    <option value="<?php echo $selreg['state']; ?>"><?php if($selreg['state']!=""){ echo $slectoption['name']; }else{?>Seelct State<?php } ?></option>
                                    <?php $geolocation = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `parent_id`=0");
                                    while($sellocation = mysqli_fetch_array($geolocation)){ ?>
                                    <option value="<?php echo $sellocation['id'];?>"><?php echo $sellocation['name'];?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                            </div>
                            <div class="form-group row">
                              <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>District:</b></label>
                              <div class="col-sm-4">
                                <select id="statess" name="city" class="form-control">
                                   <?php $citys = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `id`='".$selreg['district']."'"); 
                                    $selcity = mysqli_fetch_array($citys);?>
                                    <option value="<?php echo $selreg['district']; ?>"><?php if($selreg['district']!=""){ echo $selcity['name']; }else{?>Select State first<?php } ?></option>

                                </select>
                              </div>
                              <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>City:</b></label>
                              <div class="col-sm-4">
                                 <select id="District" name="village" class="form-control">
                                   <?php $citys1 =mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `id`='".$selreg['city']."'");     $selcity1 = mysqli_fetch_array($citys1);?>
                                    <option value="<?php echo $selreg['city']; ?>"><?php if($selreg['city']!=""){ echo $selcity1['name']; }else{?>Select District first<?php } ?></option>

                                </select>
                               
                              </div>
                            </div>
                            <div class="form-group row">
                               <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Zip/Postal Code:</b></label>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" id="office_zip" placeholder="Zip/Postal Code" name="office_zip" value="<?php echo $selreg['zip'];?>"> 
                              </div>
                              <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Land Mark:</b></label>
                              <div class="col-sm-4">
                                <input type="text" class="form-control" id="office_zip" placeholder="Enter land Mark" name="land_mark" value="<?php echo $selreg['land_mark'];?>"> 
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="exampleInputConfirmPassword2" class="col-sm-2 col-form-label"><b>Brief About Youself:</b></label>
                              <div class="col-sm-10">
                                <textarea class="form-control" placeholder="Brief About Youself" name="brief_details"><?php echo $selreg['brief_details'];?></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="item_name" value="My Test Product">
                    <input type="hidden" name="item_description" value="My Test Product Description">
                    <input type="hidden" name="item_number" value="3456">
                    <input type="hidden" name="amount" value="49.99">
                    <input type="hidden" name="address" value="ABCD Address">
                    <input type="hidden" name="currency" value="INR">
                    <input type="hidden" name="cust_name" value="phpzag">
                    <input type="hidden" name="email" value="test@phpzag.com">
                    <input type="hidden" name="contact" value="9999999999">
                    <!-- <input type="submit" class="btn btn-primary" value="Buy Now">
                     --><input type="submit" value="Proceed to Payment" name="submit" class="btn btn-success" style="position: relative;left: 868px;top: 10px;">
                </div>
                </div>
              </form>
              </div>
            </div>
            <?php include("footer.php");?>
        </div>

 <script type="text/javascript">
    $('#states').on('change',function(){
   var countryID = $(this).val();
   if(countryID){
     $.ajax({
       type:'POST',
       url:'ajaxData.php',
       data:'country_id='+countryID,
       success:function(html){
         $('#statess').html(html);
         $('#city').html('<option value="">Select District first</option>');
       }
     });
   }else{
     $('#statess').html('<option value="">Select State first</option>');
     $('#city').html('<option value="">Select District first</option>');
   }
 });
     $('#statess').on('change',function(){
   var countryID = $(this).val();
 
   if(countryID){
     $.ajax({
       type:'POST',
       url:'ajaxData.php',
       data:'state_id='+countryID,
       success:function(html){
         $('#District').html(html);
         $('#Districts').html('<option value="">Select District first</option>');
       }
     });
   }else{
     $('#Districts').html('<option value="">Select State first</option>');
     $('#city').html('<option value="">Select District first</option>');
   }
 });


    $('#states').on('change',function(){
   var stateID = $(this).val();

   if(stateID){
     $.ajax({
       type:'POST',
       url:'ajaxData.php',
       data:'state_id='+stateID,
       success:function(html){
         $('#city').html(html);
       }
     });
   }else{
     $('#city').html('<option value="">Select District first</option>');
   }
 });

  </script>
  <script type="text/javascript">
    $('#home_state').on('change',function(){
   var countryID = $(this).val();
   if(countryID){
     $.ajax({
       type:'POST',
       url:'ajaxData.php',
       data:'country_id='+countryID,
       success:function(html){
   
         $('#home_states').html(html);
         $('#city').html('<option value="">Select District first</option>');
       }
     });
   }else{
     $('#states').html('<option value="">Select State first</option>');
     $('#city').html('<option value="">Select District first</option>');
   }
 });

  $('#gst_sateids').on('change',function(){
   var gst_satse = $(this).val();
   if(gst_satse){
     $.ajax({
       type:'POST',
       url:'ajaxData.php',
       data:'gst_sate_id='+gst_satse,
       success:function(data){
         $('#gstcity_fetch').html(data);
         $('#gstcity_fetchs').html('<option value="">Select District first</option>');
       }
     });
   }else{
     $('#gstcity_fetchs').html('<option value="">Select State first</option>');
     $('#gstcity_fetchs').html('<option value="">Select District first</option>');
   }
 });

 $('#gstcity_fetch').on('change',function(){
   var gst_satse = $(this).val();

   if(gst_satse){
     $.ajax({
       type:'POST',
       url:'ajaxData.php',
       data:'gst_sate_id='+gst_satse,
       success:function(data){
         $('#gstcity_fetchdata').html(data);
         $('#gstcity_fetchs').html('<option value="">Select District first</option>');
       }
     });
   }else{
     $('#gstcity_fetchs').html('<option value="">Select State first</option>');
     $('#gstcity_fetchs').html('<option value="">Select District first</option>');
   }
 });


  $('#District1').on('change',function(){
   var gst_satse = $(this).val();
   if(gst_satse){
     $.ajax({
       type:'POST',
       url:'ajaxData.php',
       data:'gst_sate_id='+gst_satse,
       success:function(data){
         $('#gstcity_fetch').html(data);
         $('#gstcity_fetchs').html('<option value="">Select District first</option>');
       }
     });
   }else{
     $('#gstcity_fetchs').html('<option value="">Select State first</option>');
     $('#gstcity_fetchs').html('<option value="">Select District first</option>');
   }
 });


function multiply() {
  a = Number(document.getElementById('QTY').value);
  b = Number(document.getElementById('PPRICE').value);
  c = a * b;
  d = a * b;
  e = d * 18/100;
  n = e +d;

  document.getElementById('TOTAL').value = c;
  document.getElementById('SUBTOTAL').value = d;
  document.getElementById('GST').value = e;
  document.getElementById('NET').value = n;
}


  </script>
  <script src='select2/dist/js/select2.min.js' type='text/javascript'></script>
<link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
  <script type="text/javascript">
    $("#state").select2();
    $("#states").select2();
    $("#home_state").select2();
    $("#statess").select2();
    $(".gst_sate").select2();
    $(".gst_city").select2();
    $("#District").select2();
  </script>
  <script type="text/javascript">
jQuery.noConflict();
jQuery(document).ready(function($) {
    $('.name').keyup(function(event) {
        var textBox = event.target;
        var start = textBox.selectionStart;
        var end = textBox.selectionEnd;
        textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1).toLowerCase();
        textBox.setSelectionRange(start, end);
    });
});
</script>
<style type="text/css">
  .mt-5, .my-5 {
    margin-top: 1rem !important;
}
</style>

<script type="text/javascript">
    function ValidateEmail() {
      debugger;
        var email = document.getElementById("txtEmail").value;  
        var dataString = '&email1='+ email;
        $.ajax({
          type: "POST",
          url: "ajaxdata/post.php",
          data: dataString,
          success: function(response){
        var newText = removeSpaceFromString(response);
            if (newText == 'success') {
            lblError.innerHTML = "Valid Coupon Code.";
        }else{
          lblError1.innerHTML = "Invalid Coupon Code.";
           // $('.error').fadeOut(200).hide();
          }
        }
        });
        /*var lblError = document.getElementById("lblError");*/
    }

    function removeSpaceFromString(response){
         return $.trim(response);
    }
</script>
