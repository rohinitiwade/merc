<?php
include('includes/dbconnect.php');
session_start();
header('Access-Control-Allow-Origin: *');
// $edit      = $_POST['edit_data'];
// $data      = json_decode($edit, true);
// print_r($data);
$arr  = array();
$date = date('Y-m-d H:i:s');
$todo = mysqli_query($connection, "SELECT * FROM todo_list WHERE todo_id='" . $_POST['todo_id'] . "'");
while ($todos = mysqli_fetch_assoc($todo)) {
    $edit_data_object            = new stdClass();
    $edit_data_object->case_id   = TRIM($todos['case_id']);
    $edit_data_object->todo_id   = TRIM($todos['todo_id']);
    $edit_data_object->content   = TRIM($todos['content']);
    $edit_data_object->from_date = TRIM(date('d-m-Y', strtotime($todos['expiry_date'])));
    // $edit_data_object->to_date   = TRIM(date('d-m-Y H:i', strtotime($todos['to_date'])));
    $edit_data_object->private   = TRIM($todos['private']);
    $edit_data_object->user_id   = TRIM($todos['user_id']);
    $todo_details[]              = $edit_data_object;
    //reminder
    $todo_reminder               = mysqli_query($connection, "SELECT DISTINCT `email_option`,`minute`,`time_no` FROM `todo_reminder` WHERE `todo_id`='" . $todos['todo_id'] . "'");
    while ($fetch_toreminder = mysqli_fetch_array($todo_reminder)) {
        
        // $fetch_toreminders = mysqli_fetch_array($todo_reminders);
        $todo_rem               = new stdClass();
        $todo_rem->email_option = TRIM($fetch_toreminder['email_option']);
        $todo_rem->time_no      = TRIM($fetch_toreminder['time_no']);
        $todo_rem->minute       = TRIM($fetch_toreminder['minute']);
        $todo_rem->case_id      = TRIM($fetch_toreminder['case_id']);
        $todo_rem_details[]     = $todo_rem;
    }
    //relate to case details
    $todocases = mysqli_query($connection, "SELECT DISTINCT `relate_to`,`case_id` FROM `todo_reminder` WHERE `todo_id`='" . $todos['todo_id'] . "'");
    while ($seltodocases = mysqli_fetch_array($todocases)) {
        
        // $fetch_toreminders = mysqli_fetch_array($todo_reminders);
        $todo_relate            = new stdClass();
        $todo_relate->relate_to = TRIM($seltodocases['relate_to']);
        $todo_relate->case_id   = TRIM($seltodocases['case_id']);
        // $todo_rem->minute       = TRIM($seltodocases['minute']);
        // $todo_rem->case_id      = TRIM($seltodocases['case_id']);      
        $todo_relate_details[]  = $todo_relate;
    }
    //team
    $todo_team = mysqli_query($connection, "SELECT DISTINCT `assign_to` FROM `todo_team` WHERE `todo_id`='" . $todos['todo_id'] . "'");
    // while($todo_teams = mysqli_fetch_array($todo_team)){
    foreach ($todo_team as $key) {
        $reg                    = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='" . $key['assign_to'] . "'");
        $reg_fetch              = mysqli_fetch_array($reg);
        $todo_team              = new stdClass();
        $todo_team->assign_to   = TRIM($key['assign_to']);
        $todo_team->assign_name = TRIM($reg_fetch['name'] . ' ' . $reg_fetch['last_name']);
        $todo_team->toteam_id   = TRIM($key['toteam_id']);
        $todo_team_details[]    = $todo_team;
    }
    //advocates
    $todo_adv = mysqli_query($connection, "SELECT DISTINCT `advocate_id` FROM `assign_toadvocates` WHERE `todo_id`='" . $todos['todo_id'] . "'");
    // while($todo_teams = mysqli_fetch_array($todo_team)){
    foreach ($todo_adv as $advs) {
        $teamss1                      = mysqli_query($connection, "SELECT * FROM `advocate` WHERE `id`='" . $advs['advocate_id'] . "' ");
        $teams_fetchs1                = mysqli_fetch_array($teamss1);
        $todo_adv                     = new stdClass();
        $todo_adv->assign_to_advocate = TRIM($teams_fetchs1['full_name']);
        $todo_adv->adv_id = TRIM($advs['advocate_id']);
        
        $todo_adv_details[] = $todo_adv;
    }
    $arr = array(
        'status' => 'success',
        'todo_details' => $todo_details,
        'todo_reminder' => $todo_rem_details,
        'todo_team' => $todo_team_details,
        'todo_relate' => $todo_relate_details,
        'todo_advocate' => $todo_adv_details
    );
}
echo json_encode($arr, true);

?>