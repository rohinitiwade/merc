<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<?php session_start(); include("../includes/connection.php");?> 
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <div class="page-wrapper">
            <div class="page-header m-b-10">
              <h5 class="card-title">टाईमशीट्स व्यवस्थापित करा / Manage Timesheets</h5>
            </div>
            <div class="page-body">
              <div class="card form-group">
                <div class="card-block">
                  <?php 
                    $todates = $_POST['to_date'];
                    $fromdates = $_POST['from_date'];
                  if(isset($_POST['submit'])){
                    extract($_POST);
                  }?>
                  <div class="col-sm-12 form-group">
                    <a href="add-timesheets.php" class="btn btn-info btn-sm"><i class="fa fa-plus"></i>Add Timesheets</a>
                    <button class="btn btn-warning btn-sm" id="myBtn" data-target="#filter" data-toggle="modal">Filter</button>
                    <button class="btn btn-primary btn-sm" id="myBtn" data-target="#export" data-toggle="modal">Export</button>
                  </div>
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                          <th>Sr.No.</th>
                          <th>Case</th>
                          <th>Date</th>
                          <th>Particulars</th>
                          <th>Time Spent (in hours)</th>
                          <th>Type</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                               if($_POST['submitFil']!=""){
                              $seltimesheet=mysqli_query($connection, "SELECT * FROM `add_timesheet` WHERE `case`='".$_POST['caseno']."' OR `Type`='".$_POST['Type']."' OR `datetime`   BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."' AND `division`='".$_SESSION['cityName']."'");
                              }else{
                              $i=1;
                               $seltimesheet=mysqli_query($connection,"SELECT * from `add_timesheet` where `userid`='".$_SESSION['user_id']."' ORDER BY id DESC");
                             }
                             echo $count = mysqli_num_rows($seltimesheet);
                             if($count>0){

                              while($row=mysqli_fetch_array($seltimesheet)){
                                  $casedocuments =mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE  `case_id`='".$row['case_id']."'");
                                 $selcses = mysqli_fetch_array($casedocuments);
                                ?>
                              <tr>
                                 <td><?php echo $i?></td>
                                  <td style="word-break:break-all;" width="200"> <?php echo $selcses['case_type'];?> / <?php echo $selcses['case_no'];?> / <?php echo $selcses['case_no_year'];?> / <?php echo $selcses['case_title'];?></td>
                                  <td><?php echo $row['edate'];?></td>
                                  <td style="word-wrap: break-word;"><?php echo $row['particulars'];?></td>
                                  <td><?php echo $row['timespent'];?> : <?php echo $row['timeinmin'];?></td>
                                  <td><?php echo $row['type'];?></td>
                                  <td>
                            <a href="update-timesheet.php?id=<?php echo $row['id'];?>"><i class="fa fa-edit action-css" title="Edit Advocate"></i></a>
                            <a href="delete-timesheet.php?id=<?php echo $row['id'];?>"><i class="fa fa-trash action-css" onclick="return confirm('Are you sure you want to delete?');" title="Delete"></i></a>
                          </td>
                        </tr> 
                        <?php $i++;} }else{?>
                          <tr>
                            <td colspan="7">Data Not Found</td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                    <tr>
                      <td colspan="10">
                      </td>
                    </tr>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="export">
  <form name="myform" method="POST" action="downloadTimeSheet.php">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Export</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="col">
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`case`" checked="checked" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Case</span>
                </label>
              </div>
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`datetime`" checked="checked" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Date</span>
                </label>
              </div>
            
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`particulars`" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Particulars</span>
                </label>
              </div>

              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`type`" checked="checked"/>
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Type</span>
                </label>
              </div>
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input type="checkbox" name='checkboxvar[]' value="`timespent`" />
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                  </span>
                  <span>Time Spent (in hours)</span>
                </label>
              </div>
              
            </div>
          
          <div class="col row">
            <div class="downlab col-sm-2"><label>Download as:</label></div>
            <div class="col-sm-6 form-radio">

              <div class="radio radio-inline">
                <label>
                  <input type="radio" name="downloadas" value="pdf">
                  <i class="helper"></i> PDF
                </label>
              </div>
              <div class="radio radio-inline">
                <label>
                  <input type="radio" name="downloadas" value="excel" checked="">
                  <i class="helper"></i> Excel
                </label>
              </div>
            </div>   
          </div>
        </div>
        <div class="modal-footer">

          <button class="btn btn-danger btn-sm" type="button" data-dismiss="modal">Close</button>
            <button value="submit" type="submit" class="btn btn-info btn-sm"> Download</button>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="modal" id="filter">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="POST">
        <div class="modal-header">
          <h4>Filter</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">

          <div class="row">
            <div class="col-sm-4">
              <label>Cases</label><br>
              <select class="form-control" name="caseno" id="state" required>
                <option value="">Please Case No</option>
                <?php $caseno = mysqli_query($connection, "SELECT * FROM `add_timesheet` WHERE `case`!='' AND `division`='".$_SESSION['cityName']."'"); 
                while($selcases = mysqli_fetch_array($caseno)){
                 $casedocumentsq =mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE  `case_id`='".$selcases['case']."'");
                 $selcsesd = mysqli_fetch_array($casedocumentsq);
                 ?>
                 <option value="<?php echo $selcsesd['case'];?>"><?php echo $selcsesd['case_type'];?> / <?php echoselcsesdselcses['case_no'];?> / <?php echo $selcsesd['case_no_year'];?> / <?php echo $selcsesd['case_title'];?></option>
               <?php } ?>

             </select>
             <label>From</label><br>
             <input type="text" name="from_date" id="datepicker" class="form-control" placeholder="Enter From Date">
           </div>
           <div class="col-sm-4">
            <label>Team Member</label><br>
            <select name="team_member" class="form-control">
              <option value="">--Select Team Member--</option>
              <?php $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `parent_id`='".$_SESSION['user_id']."'");
              while($selteam = mysqli_fetch_array($team)){?>
                <option value="<?php echo $selteam['reg_id'];?>"><?php echo $selteam['name'];?> <?php echo $selteam['last_name'];?></option>
              <?php } ?>
            </select>
            <label>To</label>
            <input type="text" name="to_date" id="datepicker1" class="form-control" placeholder="Enter To Date">
          </div>
          <div class="col-sm-4">
            <label>Type</label><br>
            <select class="form-control" name="Type">
              <option value="">All</option>
              <option value="Effective">Effective</option>
              <option value="Non Effective / Procedural Appearance">Non Effective / Procedural Appearance</option>
            </select>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-danger btn-sm" name="Reset" type="reset">Reset</button>
        <button class="btn btn-info btn-sm" name="submitFil" type="submit">Submit</button>
      </div>
    </form>
  </div>

</div>

</div>


<?php include 'footer.php'; ?>

<script>
  $( function() {
    $( "#datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true
    });
    $( "#datepicker1" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true
    });
  } );
</script>