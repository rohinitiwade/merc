

<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> 
<?php include("phpfile/sql_home.php"); ?>
<style type="text/css">
  .profile_pic_icon{
    position: absolute;
    bottom: 0px;
    color: black;
    background-color: white;
    font-size: 16px;
  }
  .user-title{
    bottom: 0px!important;
  }
</style>
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Setting / सेटिंग</h5>
              </div>
              <?php $sql = mysqli_query($connection, "SELECT `name`,`last_name`,`image`,`designation`,`gender`,`email`,`mobile`,`address` FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                    $row = mysqli_fetch_array($sql);?>
              <div class="page-body">
                <div class="form-group">
                  <div class="card col-sm-12">
                    <div class="card-block form-group">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="cover-profile">
                            <div class="profile-bg-img">
                              <img class="profile-bg-img img-fluid" src="images/legal-banner.jpg" alt="bg-img">
                              <div class="card-block user-info">
                                <div class="col-md-12 row">
                                  <div class="media-left col-sm-2">
                                    <img class="user-img img-radius profile-image" src="uploads_profile/<?php echo $row['image'];?>" alt="user-img" style="width:100% !important;">
                                    <div class="file-field profile_pic_icon">
                                      <a class="btn-floating peach-gradient mt-0 float-left">
                                        <i class="fa fa-camera"></i>
                                        <input type="file" style="display: none;">
                                      </a>
                                    </div>
                                  </div>
                                  <div class="media-body row col-sm-10">
                                    <div class="col-lg-12 user-title">
                                      <h2><?php echo ucfirst($row['name']);?> <?php echo ucfirst($row['last_name']);?></h2>
                                      <span class="text-white"><?php echo ucfirst($row['designation']);?></span>
                                    </div>
                                    <div>
                                      <div class="pull-right cover-btn">
                                        <button type="button" class="btn btn-primary m-r-10 m-b-5">
                                          <i class="icofont icofont-plus">
                                          </i> Follow
                                        </button>
                                        <button type="button" class="btn btn-primary"><i class="icofont icofont-ui-messaging"></i> Message </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-header card">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                            <div class="slide"></div>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">User's Services</a>
                            <div class="slide"></div>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#contacts" role="tab">User's Contacts</a>
                            <div class="slide"></div>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#review" role="tab">Reviews</a>
                            <div class="slide"></div>
                          </li>
                        </ul>
                      </div>
                      <div class="tab-content">
                        <div class="tab-pane active" id="personal" role="tabpanel">
                          <div class="card">
                            <div class="card-header">
                              <h5 class="card-header-text">About Me</h5>
                              <button id="edit-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right"><i class="icofont icofont-edit"></i></button>
                            </div>
                            <div class="card-block">
                              <div class="view-info">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="general-info">
                                      <div class="row">
                                        <div class="col-lg-12 col-xl-6">
                                          <div class="table-responsive">
                                            <table class="table m-0">
                                              <tbody>
                                                <tr>
                                                  <th scope="row">Full Name</th>
                                                  <td><?php echo ucfirst($row['name']);?> <?php echo ucfirst($row['last_name']);?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Gender</th>
                                                  <td><?php echo ucfirst($row['gender']);?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Birth Date</th>
                                                  <td><?php echo ucfirst($row['dob']);?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Marital Status</th>
                                                  <td><?php echo ucfirst($row['married_status']);?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Location</th>
                                                  <td><?php echo ucfirst($row['address']);?></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                        <div class="col-lg-12 col-xl-6">
                                          <div class="table-responsive">
                                            <table class="table">
                                              <tbody>
                                                <tr>
                                                  <th scope="row">Email</th>
                                                  <td>
                                                    <a href="#!">
                                                      <span class="__cf_email__" data-cfemail="9edafbf3f1defbe6fff3eef2fbb0fdf1f3"><?php echo $row['email'];?></span>
                                                    </a>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Mobile Number</th>
                                                  <td><?php echo $row['mobile'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Twitter</th>
                                                  <td><?php echo $row['twitter'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Skype</th>
                                                  <td><?php echo $row['skeype'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Website</th>
                                                  <td>
                                                    <a href="#!"><?php echo $row['website'];?> </a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="edit-info">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="general-info">
                                      <div class="row">
                                        <div class="col-lg-6">
                                          <table class="table">
                                            <tbody>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-user">
                                                      </i>
                                                    </span>
                                                    <input type="text" class="form-control" placeholder="Enter Name" value="<?php echo ucfirst($row['name']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="form-radio">
                                                    <div class="group-add-on">
                                                      <div class="radio radiofill radio-inline">
                                                        <label>
                                                          <input type="radio" name="radio" <?php if($row['gender']=='Male'){ echo "checked"; } ?>><i class="helper"></i>Male</label>
                                                      </div>
                                                      <div class="radio radiofill radio-inline">
                                                        <label>
                                                          <input type="radio" name="radio" <?php if($row['gender']=='female'){ echo "checked"; } ?>>
                                                          <i class="helper"></i> Female
                                                        </label>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <input type="text" name="date1" id="datepicker" value="<?php echo ucfirst($row['dob']);?>" placeholder="Select Your Birth Date" class="form-control" required>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <select id="hello-single" class="form-control">
                                                    <option value="<?php echo $row['married_status'];?>"><?php if($row['married_status']!=""){ echo $row['married_status']; }else{?>---- Marital Status ----<?php } ?></option>
                                                    <option value="married">Married</option>
                                                    <option value="unmarried">Unmarried</option>
                                                  </select>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-location-pin"></i></span>
                                                    <input type="text" class="form-control" placeholder="Address" value="<?php echo $row['address'];?>">
                                                  </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-lg-6">
                                          <table class="table">
                                            <tbody>
                                            	<tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-user">
                                                      </i>
                                                    </span>
                                                    <input type="text" class="form-control" placeholder="Last Name" value="<?php echo ucfirst($row['last_name']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-mobile-phone"></i></span>
                                                    <input type="text" class="form-control" placeholder="Mobile Number" value="<?php echo ucfirst($row['mobile']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-social-twitter"></i></span>
                                                    <input type="text" class="form-control" placeholder="Twitter Id" value="<?php echo ucfirst($row['twitter']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-social-skype"></i></span>
                                                    <input type="email" class="form-control" placeholder="Skype Id" value="<?php echo ucfirst($row['skype']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-earth"></i></span>
                                                    <input type="text" class="form-control" placeholder="website" value="<?php echo ucfirst($row['website']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                      <div class="text-center">
                                        <a href="#!" class="btn btn-primary waves-effect waves-light m-r-20">Save</a>
                                        <a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="card">
                                <div class="card-header">
                                  <h5 class="card-header-text">Description About Me</h5>
                                  <button id="edit-info-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                    <i class="icofont icofont-edit">
                                    </i>
                                  </button>
                                </div>
                                <div class="card-block user-desc">
                                  <div class="view-desc">
                                    <p><?php
										for($i=1;$i<=4;$i++)
										{
											for($j=$i;$j>=$i;$j++)
											{
											echo "*";
										
											}
											echo "<br>";
										} 
										?>
                                    </p>
                                  </div>
                                  <div class="edit-desc">
                                    <div class="col-md-12">
                                      <textarea id="description">
                                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?" "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able To Do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pain.
                                        </p>
                                      </textarea>
                                    </div>
                                    <div class="text-center">
                                      <a href="#!" class="btn btn-primary waves-effect waves-light m-r-20 m-t-20">Save
                                      </a>
                                      <a href="#!" id="edit-cancel-btn" class="btn btn-default waves-effect m-t-20">Cancel
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="binfo" role="tabpanel">
                          <div class="card">
                            <div class="card-header">
                              <h5 class="card-header-text">User Services</h5>
                            </div>
                            <div class="card-block">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="card b-l-success business-info services m-b-20">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Shivani Hero</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-danger business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Dress and Sarees</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-info business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Shivani Auto Port</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-warning business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Hair stylist</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-danger business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">BMW India</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-success business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Shivani Hero</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="card">
                                <div class="card-header">
                                  <h5 class="card-header-text">Profit</h5>
                                </div>
                                <div class="card-block">
                                  <div id="main" style="height:300px;width: 100%;">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/settings.js"></script>
  <!-- <script type="text/javascript" src="scripts/my-expenses.js"></script> -->
