<?php
include('includes/dbconnect.php');
// mysqli_query('SET character_set_results=utf8');
session_start();
error_reporting(0);
$arr               = array();
$date              = date('Y-m-d H:i:s');
$update_autostatus = mysqli_query($connection, "UPDATE `reg_cases` SET `auto_update`='" . $_POST['autoupdate'] . "' WHERE `case_id`='" . $_POST['case_id'] . "' ");
 
$view_case         = mysqli_query($connection, "SELECT rc.case_id, rc.user_id, rc.court_id, rc.court_name, rc.case_no, rc.case_no_year, rc.case_title, rc.case_description, rc.diary_no, rc.case_type,
 rc.supreme_court, rc.nastikramank, rc.diary_year, rc.appearing_modal, rc.petitioner, rc.court_hall, rc.floor, rc.classification,
 rc.judge, rc.reffered_by, rc.section_category, rc.priority, rc.hearing_date, rc.do_you_have_CNR, rc.CNR, rc.high_court, rc.bench, rc.side, rc.hc_stamp_register,
 rc.is_the_affidavit_vakalath_filed, rc.affidavite_filling_date, rc.are_you_appearing_as, rc.name_of_matter, rc.state,rc.district,
 rc.court_establishment, rc.commissions, rc.tribunals, rc.tribunals_other, rc.commissionerate, rc.status, rc.case_status, rc.Stage, rc.date_time, 
rc.is_the_affidavit_vakalath_filed,rc.auto_update,rc.date_of_filling,rc.is_vakalat_filed,rc.vakalat_filling_date,rc.nominal,
rc.ministry_desk_no,rc.div_comm_level,rc.complaint_authority,rc.ministry,rc.zp_name,rc.case_type_master,rc.new_status,rc.court_details,rc.case_district,rc.under_division,rc.pet_address,rc.pet_name,rc.pet_mobile,rc.pet_email_id,rc.res_address,rc.res_name,rc.res_mobile,rc.res_email_id,rc.auto_caseid,rc.judicial,rc.type_of_litigation,rc.risk_category,cat.category_name FROM reg_cases as rc LEFT JOIN categories cat ON cat.category_id=rc.risk_category  WHERE rc.remove_status=1 AND rc.case_id='" . $_POST['case_id'] . "'");


//$view_case         = mysqli_query($connection, "SELECT rc.case_id, rc.user_id, rc.court_id, rc.court_name, rc.case_no, rc.case_no_year, rc.case_title, rc.case_description, rc.diary_no, rc.case_type, rc.supreme_court, rc.nastikramank, rc.supreme_court_file, rc.diary_year, rc.appearing_modal, rc.petitioner, rc.court_hall, rc.floor, rc.classification, rc.judge, rc.reffered_by, rc.section_category, rc.priority, rc.hearing_date, rc.do_you_have_CNR, rc.CNR, rc.high_court, rc.bench, rc.side, rc.hc_stamp_register, rc.is_the_affidavit_vakalath_filed, rc.affidavite_document, rc.affidavite_filling_date, rc.are_you_appearing_as, rc.name_of_matter, rc.state,rc.district, rc.court_establishment, rc.commissions, rc.tribunals, rc.tribunals_other, rc.commissionerate, rc.status, rc.case_status, rc.Stage, rc.date_time, rc.is_the_affidavit_vakalath_filed,rc.auto_update,rc.date_of_filling,ec.disposed,rc.is_vakalat_filed,rc.vakalat_filling_date,rc.nominal,rc.ministry_desk_no,rc.div_comm_level,rc.complaint_authority,rc.ministry,rc.zp_name,rc.case_type_master FROM reg_cases as rc LEFT JOIN ecourt_casedetails as ec ON rc.case_id=ec.case_id WHERE rc.case_id='" . $_POST['case_id'] . "' AND rc.remove_status=1");
$count_data        = mysqli_num_rows($view_case);
if ($count_data == 0) {
    $arr = array(
        'status' => 'error'
    );
}
while ($view_cases = mysqli_fetch_assoc($view_case)) {
    if ($view_cases['court_id'] == 1) {
        if ($view_cases['supreme_court'] == 'Diary Number') {
            $case_no      = $view_cases['diary_no'];
            $case_no_year = $view_cases['diary_year'];
        } else {
            $case_no      = $view_cases['case_no'];
            $case_no_year = $view_cases['case_no_year'];
        }
        
    } else {
        $case_no      = $view_cases['case_no'];
        $case_no_year = $view_cases['case_no_year'];
    }
    if ($view_cases['court_id'] == 2) {
        $high_court = $view_cases['high_court'];
    } else if ($view_cases['court_id'] == 5) {
        $high_court = $view_cases['tribunals'];
    }
    if ($view_cases['date_of_filling'] != '') {
        $datefill = date("F d, Y", strtotime($view_cases['date_of_filling']));
    }
    if($view_cases['new_status'] == 'judicial'){
       $court_name = 'Competant Authority - '.$view_cases['complaint_authority']; 
    }else{
         $court_name = $view_cases['court_name']; 
    }
    if($view_cases['affidavite_filling_date']!=''){
        $aff_date = date('d-m-Y',strtotime($view_cases['affidavite_filling_date']));
    }
     if($view_cases['vakalat_filling_date']!=''){
        $vak_date = date('d-m-Y',strtotime($view_cases['vakalat_filling_date']));
    }
    if($view_cases['judicial']!=''){
        $jud = '- '.$view_cases['judicial'];
    }
    $view_data_object                                  = new stdClass();
    $view_data_object->case_id                         = TRIM($view_cases['case_id']);
    $view_data_object->user_id                         = TRIM($view_cases['user_id']);
    $view_data_object->court_id                        = TRIM($view_cases['court_id']);
    $view_data_object->court_name                      = TRIM($court_name);
    $view_data_object->case_no                         = TRIM($case_no);
    $view_data_object->case_no_year                    = TRIM($case_no_year);
    $view_data_object->case_title                      =$view_cases['case_title'];
    $view_data_object->case_description                = TRIM($view_cases['case_description']);
    // $view_data_object->diary_no                        = TRIM($view_cases['diary_no']);
    $view_data_object->case_type                       = TRIM($view_cases['case_type']);
    $view_data_object->supreme_court                   = TRIM($view_cases['supreme_court']);
    $view_data_object->nastikramank                    = TRIM($view_cases['nastikramank']);
    // $view_data_object->supreme_court_file              = TRIM($view_cases['supreme_court_file']);
    // $view_data_object->diary_year                      = TRIM($view_cases['diary_year']);
    $view_data_object->appearing_modal                 = TRIM($view_cases['appearing_modal']);
    $view_data_object->petitioner                      = TRIM($view_cases['petitioner']);
    $view_data_object->court_hall                      = TRIM($view_cases['court_hall']);
    $view_data_object->floor                           = TRIM($view_cases['floor']);
    $view_data_object->classification                  = TRIM($view_cases['classification']);
    $view_data_object->judge                           = TRIM($view_cases['judge']);
    $view_data_object->reffered_by                     = TRIM($view_cases['reffered_by']);
    $view_data_object->section_category                = TRIM($view_cases['section_category']);
    $view_data_object->priority                        = TRIM($view_cases['priority']);
    $view_data_object->hearing_date                    = TRIM($view_cases['hearing_date']);
    $view_data_object->do_you_have_CNR                 = TRIM($view_cases['do_you_have_CNR']);
    $view_data_object->CNR                             = TRIM($view_cases['CNR']);
    $view_data_object->high_court                      = TRIM($high_court);
    $view_data_object->bench                           = TRIM($view_cases['bench']);
    $view_data_object->side                            = TRIM($view_cases['side']);
    $view_data_object->type_of_litigation                            = TRIM($view_cases['type_of_litigation']);
    $view_data_object->risk_category                            = TRIM($view_cases['category_name']);
    $view_data_object->hc_stamp_register               = TRIM($view_cases['hc_stamp_register']);
    $view_data_object->is_the_affidavit_vakalath_filed = TRIM($view_cases['is_the_affidavit_vakalath_filed']);
    // $view_data_object->affidavite_document             = TRIM($view_cases['affidavite_document']);
    $view_data_object->affidavite_filling_date         = TRIM($aff_date);
    $view_data_object->are_you_appearing_as            = TRIM($view_cases['are_you_appearing_as']);
    $view_data_object->name_of_matter                  = TRIM($view_cases['name_of_matter']);
    $view_data_object->state                           = TRIM($view_cases['state']);
    $view_data_object->district                        = TRIM($view_cases['district']);
    $view_data_object->court_establishment             = TRIM($view_cases['court_establishment']);
    $view_data_object->commissions                     = TRIM($view_cases['commissions']);
    // $view_data_object->tribunals = $view_cases['tribunals'];
    $view_data_object->tribunals_other                 = TRIM($view_cases['tribunals_other']);
    $view_data_object->commissionerate                 = TRIM($view_cases['commissionerate']);
    $view_data_object->status                          = TRIM($view_cases['status']);
    $view_data_object->case_status                     = TRIM($view_cases['case_status']);
    $view_data_object->disposed_status                 = TRIM($view_cases['disposed']);
    $view_data_object->date_time                       = TRIM($view_cases['date_time']);
    $view_data_object->auto_update                     = TRIM($view_cases['auto_update']);
    $view_data_object->date_of_filling                 = TRIM($datefill);
    $view_data_object->case_type_initials              = TRIM($view_cases['case_type_master']);
    $view_data_object->district_court_name             = TRIM($view_cases['district_court_name']);
    $view_data_object->court_establishment             = TRIM($view_cases['court_establishment']);
    $view_data_object->is_vakalat_filed                = TRIM($view_cases['is_vakalat_filed']);
    $view_data_object->vakalat_filling_date            = TRIM($vak_date);
    $view_data_object->nominal                         = TRIM($view_cases['nominal']);
    $view_data_object->ministry_desk_no                = TRIM($view_cases['ministry_desk_no']);
    $view_data_object->div_comm_level                  = TRIM($view_cases['div_comm_level']);
    $view_data_object->complaint_authority             = TRIM($view_cases['complaint_authority']);
    $view_data_object->ministry                        = TRIM($view_cases['ministry']);
    $view_data_object->zp_name                         = TRIM($view_cases['under_division']);
    $view_data_object->court_details                   = TRIM($view_cases['court_details']);
    $view_data_object->division                        = TRIM($view_cases['case_district']);
    $view_data_object->pet_address                            = TRIM($view_cases['pet_address']);
    $view_data_object->pet_name                            = TRIM($view_cases['pet_name']);
    $view_data_object->pet_mobile                            = TRIM($view_cases['pet_mobile']);
    $view_data_object->pet_email_id                            = TRIM($view_cases['pet_email_id']);
    $view_data_object->res_address                            = TRIM($view_cases['res_address']);
    $view_data_object->res_name                            = TRIM($view_cases['res_name']);
    $view_data_object->res_mobile                            = TRIM($view_cases['res_mobile']);
    $view_data_object->res_email_id                            = TRIM($view_cases['res_email_id']);
    $view_data_object->autoid                            = TRIM($view_cases['auto_caseid'].$jud);
    $case_details[]                                    = $view_data_object;
    
    $appearing_modal = mysqli_query($connection, "SELECT `appearing_modal` FROM `appearing_model` WHERE `app_id`='" . $view_cases['appearing_modal'] . "'");
    $appear          = mysqli_fetch_array($appearing_modal);
    $reg             = mysqli_query($connection, "SELECT CONCAT(name, ' ', last_name) AS user_name,gender,image FROM `law_registration` WHERE `reg_id`='" . $view_cases['user_id'] . "'");
    $regs            = mysqli_fetch_array($reg);
    
    if ($view_cases['date_time'] != '') {
        $created_date = date('d-m-Y', strtotime($view_cases['date_time']));
        $created_time = date('H:i', strtotime($view_cases['date_time']));
    }

if($_POST['court_id'] == 1){
  $hearing_history = mysqli_query($connection, "SELECT `hearing_date`,`purpose_of_hearing`,`posted_for`,`action_taken`,`session_phase` FROM `history_of_case_hearing` WHERE `reg_id`='" . $_POST['case_id'] . "'  ORDER BY hear_no ASC limit 1");
}else{
$hearing_history = mysqli_query($connection, "SELECT `hearing_date`,`purpose_of_hearing`,`posted_for`,`action_taken`,`session_phase` FROM `history_of_case_hearing` WHERE `reg_id`='" . $_POST['case_id'] . "'  ORDER BY hear_no DESC limit 1");
}
$case_hearing    = mysqli_fetch_array($hearing_history);

if ($case_hearing['hearing_date'] != '') {
    $act_datehear  = $case_hearing['hearing_date'];
    $act_hear_date = date("F d, Y", strtotime($act_datehear));
}
$stage         = $case_hearing['purpose_of_hearing'];
// }
$activity_data = array(
    'stage' => $stage,
    'posted_for' => TRIM($case_hearing['posted_for']),
    'action_taken' => TRIM($case_hearing['action_taken']),
    'next_hearing_date' => $act_hear_date,
    'session_phase' => TRIM($case_hearing['session_phase'])
);

$hear = mysqli_query($connection, "SELECT `hearing_date`,`date_time`,`hear_no`,`reg_id`,`user_id`,`ec_caseid`,`case_no`,`reg_num`,`business_on_date`,`judge`,`purpose_of_hearing`,`cnr_num`,`posted_for`,`session_phase`,`act_id`,`activity_log_type`,`user_comment` FROM `history_of_case_hearing` WHERE  `reg_id`='" . $_POST['case_id'] . "' AND `court_id`='" . $_POST['court_id'] . "' ORDER BY hearing_date DESC");
while ($hears = mysqli_fetch_array($hear)) {
    // $select_activitylog = mysqli_query($connection, "SELECT * FROM `activity_log` WHERE  `case_id`='" . $_POST['case_id'] . "' AND `user_id`='" . $_SESSION['user_id'] . "' AND `remove_status`=1  ORDER BY `activity_log_id` DESC");
    // $fetch_activity = mysqli_fetch_array($select_activitylog);

    if ($hears['hearing_date'] != '') {
        $datehear  = $hears['hearing_date'];
        $hear_date = date("F d, Y", strtotime($datehear));
    }
    $date         = date('d-m-Y', strtotime($hears['date_time']));
    $time         = date('H:s', strtotime($hears['date_time']));
    $hears_data[] = array(
        'hear_no' => $hears['hear_no'],
        'case_id' => $hears['reg_id'],
        'user_id' => $hears['user_id'],
        'ec_caseid' => $hears['ec_caseid'],
        'case_no' => $hears['case_no'],
        'reg_num' => $hears['reg_num'],
        'business_on_date' => $hears['business_on_date'],
        'judge' => $hears['judge'],
        'user_comment' => $hears['user_comment'],
        'next_hearing_date' => $hear_date,
        'stage' => $hears['purpose_of_hearing'],
        'cnr_num' => $hears['cnr_num'],
        'date' => $date,
        'time' => $time,
        'posted_for' => $hears['posted_for'],
        'action_taken' => $hears['action_taken'],
        'session_phase' => $hears['session_phase'],
        'activity_log_id' => $hears['act_id'],
        // 'activity_log_type' => $hears['activity_log_type']
    );
}
//respondent
$respondent = mysqli_query($connection, "SELECT `case_id`,`court_id`,`user_id`,`full_name`,`email`,`mobile`,`date_time`,`case_no` FROM `petitioner_respondent` WHERE `case_id`='" . $_POST['case_id'] . "' AND  `full_name`!='' AND `designation`='Respondent' ORDER BY pet_res_id ASC");
while ($respondents = mysqli_fetch_array($respondent)) {
    $respondent_data[] = array(
        'case_id' => $respondents['case_id'],
        'court_id' => $respondents['court_id'],
        'user_id' => $respondents['user_id'],
        'respondent_name' => TRIM($respondents['full_name']),
        'respondent_email' => TRIM($respondents['email']),
        'respondent_mobile' => TRIM($respondents['mobile']),
        'date_time' => $respondents['date_time'],
        'case_no' => $respondents['case_no']
    );
}
//petitioner
$petitioner = mysqli_query($connection, "SELECT `case_id`,`court_id`,`user_id`,`full_name`,`email`,`mobile`,`date_time`,`case_no` FROM `petitioner_respondent` WHERE `case_id`='" . $_POST['case_id'] . "' AND  `full_name`!='' AND `designation`='Petitioner' ORDER BY pet_res_id ASC");
while ($petitioners = mysqli_fetch_array($petitioner)) {
    $petitioners_data[] = array(
        'case_id' => $petitioners['case_id'],
        'court_id' => $petitioners['court_id'],
        'user_id' => $petitioners['user_id'],
        'petitioner_name' => TRIM($petitioners['full_name']),
        'petitioner_email' => TRIM($petitioners['email']),
        'petitioner_mobile' => TRIM($petitioners['mobile']),
        'date_time' => $petitioners['date_time'],
        'case_no' => $petitioners['case_no']
    );
}
//respondent advocate
$res_adv = mysqli_query($connection, "SELECT `case_id`,`court_id`,`user_id`,`full_name`,`email`,`mobile`,`date_time`,`case_no` FROM `advocate` WHERE `case_id`='" . $_POST['case_id'] . "' AND  `full_name`!='' AND `designation`='Respondent Advocate' ORDER BY id ");
while ($res_advs = mysqli_fetch_array($res_adv)) {
    $res_advs_data[] = array(
        'case_id' => $res_advs['case_id'],
        'court_id' => $res_advs['court_id'],
        'user_id' => $res_advs['user_id'],
        'resadvocate_name' => TRIM($res_advs['full_name']),
        'resadvocate_email' => TRIM($res_advs['email']),
        'resadvocate_mobile' => TRIM($res_advs['mobile']),
        'date_time' => $res_advs['date_time'],
        'case_no' => $res_advs['case_no']
    );
}
//petitioner advocate
$pet_adv = mysqli_query($connection, "SELECT `case_id`,`court_id`,`user_id`,`full_name`,`email`,`mobile`,`date_time`,`case_no` FROM `advocate`  WHERE `case_id`='" . $_POST['case_id'] . "' AND  `full_name`!='' AND `designation`='Petitioner Advocate' ORDER BY id  ");
while ($pet_advs = mysqli_fetch_array($pet_adv)) {
    $pet_advs_data[] = array(
        'case_id' => $pet_advs['case_id'],
        'court_id' => $pet_advs['court_id'],
        'user_id' => $pet_advs['user_id'],
        'advocate_name' => TRIM($pet_advs['full_name']),
        'advocate_email' => TRIM($pet_advs['email']),
        ' advocate_mobile' => TRIM($pet_advs['mobile']),
        'date_time' => $pet_advs['date_time'],
        'case_no' => $pet_advs['case_no']
    );
}

$team = mysqli_query($connection, "SELECT tt.user_id,tt.case_id,tt.assign_to,tt.case_no FROM todo_team tt  WHERE  tt.case_id='" . $_POST['case_id'] . "' AND `relate_toid`=0 AND tt.flag='add' AND tt.division='" . $_SESSION['cityName'] . "' ORDER BY toteam_id DESC");
while ($teams = mysqli_fetch_array($team)) {
    $law          = mysqli_query($connection, "SELECT `name`,`last_name`,`email`,`mobile`,`designation`,`state`,`district`,`city`,`gender`,`address` FROM `law_registration` WHERE `reg_id`='" . $teams['assign_to'] . "'");
    $laws         = mysqli_fetch_array($law);
    // $full_name  = $teams['name'].' '.$teams['last_name'];
    $teams_data[] = array(
        'case_id' => $teams['case_id'],
        'user_id' => $teams['user_id'],
        'first_name' => $laws['name'],
        'last_name' => $laws['last_name'],
        'email' => $laws['email'],
        'mobile' => $laws['mobile'],
        'address' => $laws['address'],
        'designation' => $laws['designation'],
        'state' => $laws['state'],
        'district' => $laws['district'],
        'city' => $laws['city'],
        'gender' => $laws['gender'],
        'case_no' => $teams['case_no'],
        'team_id' => $teams['assign_to']
    );
}

//edit activity log
// $edit_log = mysqli_query($connection,"SELECT * FROM `activity_log` WHERE `case_id`='".$_POST['case_id']."'")

$select_connect = mysqli_query($connection, "SELECT cc.user_id, cc.case_id,cc.status,cc.status_data,cc.connected_id,cc.date_time,cc.delete_status,cc.priority,cc.connected_main_id FROM connected_cases cc  WHERE  cc.case_id='" . $_POST['case_id'] . "' AND cc.delete_status=1   ORDER BY connected_main_id ASC");
$connect_count  = mysqli_num_rows($select_connect);

$document  = mysqli_query($connection, "SELECT count(case_id) AS total_count FROM `add_document` WHERE `case_id`='" . $_POST['case_id'] . "' AND `remove_status`=1");
$count_doc = mysqli_fetch_array($document);


$service    = mysqli_query($connection, "SELECT `service` FROM `avail_services` WHERE `case_id`='" . $_POST['case_id'] . "'");
$services   = mysqli_fetch_array($service);
$user_image = $regs['image'];
$arr        = array(
    'status' => 'success',
   'case_status'=>  $view_cases['case_status'],
    'data' => $case_details,
    'user_name' => $regs['user_name'],
    'gender' => $regs['gender'],
    'appearing_modal' => $appear['appearing_modal'],
    'pet_file' => $petitioner_file,
    'affidavit_file' => $affidavit_file,
    'created_date' => $created_date,
    'created_time' => $created_time,
    'hearing_data' => $hears_data,
    'respondent_data' => $respondent_data,
    'petitioners_data' => $petitioners_data,
    'res_advocate_data' => $res_advs_data,
    'pet_advocate_data' => $pet_advs_data,
    'team_data' => $teams_data,
    'connected_count' => $connect_count,
    'document_count' => $count_doc['total_count'],
    'user_image' => $user_image,
    'stage' => $activity_data,
    'service' => $services['service'],
    'hearing_date' => $view_cases['hearing_date']
);
}
// }
// else{
//  $arr = array('status' => 'error');
// }
echo json_encode($arr, true);
?>