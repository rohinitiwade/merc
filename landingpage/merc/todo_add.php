<!-- <style type="text/css">
.button_submit{
 padding: 12px 10px;
}
.todocontent{
 font-size: 11px;
}
#navb .nav-item{
  text-align: center;
}
</style> -->
<div class="media">
  <div class="media-body col-sm-12">
    <div class="row">
      <!-- <div class="col-sm-12"> -->
        <!-- <div class="card"> -->
          <!-- <div class="card-header"><h4>Create To-Dos</h4></div>  -->
          <div class="card-body todo_content">
            <div class="row">
              <div class="input-group input-search-div-header">
                <i class="fa fa-pencil input-group-addon" id="basic-addon1"></i>
                <textarea class="form-control create" id="desc" aria-label="With textarea" placeholder="Click here to write to-do description"></textarea>
                <input type="hidden" name="case_id" id="case_id" value="<?php echo base64_decode($_GET['caseid']);?>">
              </div>
            </div>
          </div>
          <!-- <div style="transform: translateY(20px);"><h4><b>Please select due date</b></h4></div> -->

          <div class="row col-sm-12">
            <div class="col-sm-7">
              <div class="form-group">
                <h5 class="heading">Please select due date</h5>
              </div>
              <!--       <input type="text" class="form-control" id="example"> -->

              <div class="form-group col-sm-12 row">
                <div class="col-sm-5 p-0">
                  <!-- <input type="text" class="form-control" id="datepicker" name="startdate"> -->
                  <div class="form-group">
                    <div class="input-group date" id="datetimepicker6">
                      <input type="text" class="form-control" id="datepicker_todo" name="startdate" aria-describedby="basic-addon2" value="">
                      <span class="input-group-addon ">
                        <span class="icofont icofont-ui-calendar"></span>
                      </span>
                    </div>

                  </div>
                </div>
                <div class="col-sm-1 d-flex">
                  <span style="text-align: center;"><h5><b>to</b></h5></span>
                </div>
                <div class="col-sm-5 p-0">
                  <!-- <input type="text" class="form-control" id="datepicker1" name="enddate"> -->
                  <div class="input-group date" id="datetimepicker6">
                   <input type="text" class="form-control" id="datepicker1_todo" name="enddate"  aria-describedby="basic-addon2" value="">

                   <span class="input-group-addon ">
                    <span class="icofont icofont-ui-calendar"></span>
                  </span>
                </div>
              </div>

            </div>
          </div>
          <div class="col-sm-5">
            <div class="form-group row">
              <h5 class="heading"> Please set your auto reminders </h5>
            </div>
            <div class="form-group row" id="reminder_clickhere" style="margin-left: 0px;">
              <b onclick="reminder()" style="cursor: pointer;padding-left: 11px;"> Click here </b>&nbsp; to add auto reminders
            </div>
            <div id="reminder_input" style="display: none;">

              <div class="row" id="input">
                <i class='fa fa-minus-circle' style='color: red;cursor: pointer;margin-top: 9px; font-size: 20px;' onclick="remove_reminder()"></i>
     <!--       <button type="button" class="btn btn-default btn-sm">
          <i class='fas fa-trash-alt remaindertrash'  onclick="remove_reminder()"></i>
        </button> -->


        <div class="col-sm-4">
          <select class="form-control" id="email_option_0" name='minute[]'>
            <!-- <option value="">Select Option</option> -->
            <option value="Email">Email</option>
            <option value="SMS">SMS</option>
          </select>
        </div>
        <div class="col-sm-4">
          <select class="form-control reminder_format" id="minute_0" name='minute[]'>
            <!--  <option value="">Select Option</option> -->
            <option value="minutes">minute(s)</option>
            <option value="hours">hour(s)</option>
            <option value="days">day(s)</option>
            <option value="week">week(s)</option>
          </select>
        </div>
        <div class="col-sm-3">
          <select class="form-control" id="reminder_format_value_0" name='subject[]'>
            <!-- <option value="">Select Option</option> -->
            <option>5</option>
            <option>10</option>
            <option>15</option>
            <option>20</option>
            <option>25</option>
            <option>30</option>
            <option>35</option>
            <option>40</option>
            <option>45</option>
            <option>50</option>
            <option>55</option>
          </select>
        </div>

        <span style="margin-top: 8px;">before the due date & time</span>

      </div>
      <span> <i class="feather icon-plus-circle" style="color: green;cursor: pointer;margin-right: 5px;font-size: 19px;" onclick="add_field();"></i>Add an additional reminder</span><br>
    </div>
  </div>

</div>
<div class="col-sm-12 p-0 form-group">
  <div class="checkbox-zoom zoom-primary">
    <label>
     <input type="checkbox" id="private">
     <span class="cr">
      <i class="cr-icon icofont icofont-ui-check txt-primary">
      </i>
    </span>
    <span>Mark As Private</span>
  </label>
</div>
</div>


<div class=" row col-sm-12 p-0 ">

 <!--  <div class="col-sm-4">
    <div class="form-group">
      <label for="usr" class="col-form-label"><b>Relate To</b></label>
      
      <select id="relate" name="framework[]" multiple="multiple"  class="form-control" placeholder="Please Select">
        <option value="">Please Select</option>
        <?php $courtname = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_title`!='' ORDER BY case_id DESC");
        while($selcourtlist = mysqli_fetch_array($courtname)){?>
          <option value="<?php echo $selcourtlist['case_id'];?>"><?php echo $selcourtlist['case_title'];?></option>
        <?php } ?>  
      </select>
    </div>
  </div> -->
  <div class="col-sm-6">
    <div class="col-sm-4">
      <label for="usr" class="col-form-label"><b>Assign To</b></label>
    </div>
    <div class="row">
     <div class="col-sm-12 form-group framework-div">
       <!-- <input type="text" class="form-control" id="full_name" placeholder="Full Name" name="full_name"> -->
       <select id="framework" name="framework[]" multiple class="form-control" >
        <?php $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `parent_id`='".$_SESSION['user_id']."' ORDER BY reg_id DESC");
        while($teams = mysqli_fetch_array($team)){?>
          <option value="<?php echo $teams['reg_id'];?>"><?php echo  $teams['name'];?>  <?php echo  $teams['last_name'];?> </option>
        <?php } ?>
      </select> 
    </div>
  </div>
</div>
<!-- <div class="col-sm-6">
  <div class="col-sm-6">
    <label for="usr" class="col-form-label"><b>Assign To Advocates</b></label>
  </div>
  <div class="row">
   <div class="col-sm-12 form-group framework-div">
     
     <select id="advocate" name="framework[]" multiple class="form-control" >
      <?php $adv = mysqli_query($connection, "SELECT * FROM `advocate` WHERE `remove_status`='1' AND `division`='".$_SESSION['cityName']."' ORDER BY id DESC");
      while($advs = mysqli_fetch_array($adv)){?>
        <option value="<?php echo $advs['id'];?>"><?php echo  $advs['full_name'];?> </option>
      <?php } ?>
    </select> 
  </div>
</div>
</div> -->
 <!-- <div class="col-sm-2">
 <label for="usr" class="col-form-label"><b>Assign To Advocate</b></label>
</div>
  <div class="col-sm-5">
   <div class="form-group framework-div">
     <input type="text" class="form-control" id="full_name" placeholder="Full Name" name="full_name">
     <select id="framework" name="framework[]" multiple class="form-control" >
       <option></option>
    </select> 
  </div>
</div> -->

<div class="col-sm-12" style="float: right;"> 
  <button class="btn btn-primary btn-sm" onclick="todoView()">Submit</button>
</div> 
</div>

<hr style="border: 1px solid grey;width: 100%">

<div class="col-sm-12 todo_content">
  <div class="todo_nav">
    <nav class="navbar navbar-expand-lg" style="padding: 0px">
      <div class="collapse navbar-collapse" id="navb">
        <ul class="navbar-nav mr-auto" id="action-span">
         <li class="nav-item">
          <div class="nav-link"><a href="create-todo.php?name=All"><b>All</b> <span class="main_back <?php if($_GET['name']=='All'){ echo 'list_active'; }?>"><?php $all = mysqli_query($connection, "SELECT * FROM `todo_list` WHERE `division`='".$_SESSION['cityName']."' AND `case_id`='".base64_decode($_GET['caseid'])."'");  $count = mysqli_num_rows($all); ?><b style="color: black !important;"><?php echo $count;?></b></span></a></div>
        </li>
        <li class="nav-item">
          <div class="nav-link"><a href="create-todo.php?name=pending"><b>Pending</b> <span class="main_pending "><b style="color: white !important;">0</b></span></a></div>
        </li> 
        <li class="nav-item">
          <div class="nav-link"><a href="create-todo.php?name=Upcomming"><b>Upcoming</b> <span class="main_upcomming "><?php $Upcoming = mysqli_query($connection, "SELECT * FROM `todo_list` WHERE `division`='".$_SESSION['cityName']."' AND `to_date`>'".date("Y-m-d H:i:s")."' AND `case_id`='".base64_decode($_GET['caseid'])."'");  $Upcoming1 = mysqli_num_rows($Upcoming); ?>
          <b style="color: white !important;"><?php echo $Upcoming1;?></b></span></a></div>
        </li>
        <li class="nav-item">
          <div class="nav-link"><a href="create-todo.php?name=Completed"><b>Completed</b> <span class="main_completed list"><?php $complete = mysqli_query($connection, "SELECT * FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `division`='".$_SESSION['cityName']."' AND `to_date`<='".date("Y-m-d H:i:s")."' AND `case_id`='".base64_decode($_GET['caseid'])."'"); $complete1 = mysqli_num_rows($complete); ?><b style="color: white !important;"><?php echo $complete1;?></b></span></a></div>
        </li>
      </ul>
      <?php if(isset($_POST['toAll'])){ 
        extract($_POST);
        $toAll;
        $sql =  mysqli_query($connection, "SELECT * FROM `todo_list` WHERE `user_id`='".$toAll."'");
      } ?>
      <form class="form-inline my-2 my-lg-0" method="POST">
        <select  class=" form-control" id="" name="toAll" onchange='this.form.submit()'>
          <option value="<?php echo $_POST['toAll'];?>">--Select To-Do--</option>
          <option value="All"> Everyone's to-dos </option>
          <option value="<?php echo $_SESSION['user_id'];?>"> My to-dos  </option>
          <?php $todolist = mysqli_query($connection, "SELECT * FROM `law_registration`WHERE `division`='".$_SESSION['cityName']."' AND `reg_id`!='".$_SESSION['user_id']."'");
          while($todolistsel = mysqli_fetch_array($todolist)){?>

            <option value="<?php echo $todolistsel['reg_id'];?>"> <?php echo $todolistsel['name'];?>  <?php echo $todolistsel['last_name'];?> </option>
          <?php } ?>
        </select>&nbsp;&nbsp;
       <!--  <input class="col-sm-5 form-control search_input" type="text" placeholder="Search" name="">
        <button class="button_submit" type="button">Search</button> -->
      </form>
    </div>
  </nav>
</div>

</div>
<?php 
$showRecordPerPage = 10;
if(isset($_GET['page']) && !empty($_GET['page'])){
  $currentPage = $_GET['page'];
}else{
  $currentPage = 1;
}
$startFrom = ($currentPage * $showRecordPerPage) - $showRecordPerPage;
$totalEmpSQL = "SELECT * FROM todo_list WHERE `user_id`='".$_SESSION['user_id']."' AND `division`='".$_SESSION['cityName']."'";
$allEmpResult = mysqli_query($conn, $totalEmpSQL);
$totalEmployee = mysqli_num_rows($allEmpResult);
$lastPage = ceil($totalEmployee/$showRecordPerPage);
$firstPage = 1;
$nextPage = $currentPage + 1;
$previousPage = $currentPage - 1;
if($_GET['name']=="Upcomming"){
  $sql =  mysqli_query($connection,"SELECT `todo_id`,`case_id`,`from_date`,`private` FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' AND `to_date`>'".date("Y-m-d H:i:s")."'  ORDER BY todo_id DESC LIMIT $startFrom, $showRecordPerPage");
}elseif($_GET['name']=="Completed"){
  $sql =  mysqli_query($connection,"SELECT `todo_id`,`case_id`,`from_date`,`private` FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' AND `to_date`<='".date("Y-m-d H:i:s")."' ORDER BY todo_id DESC LIMIT $startFrom, $showRecordPerPage");
}elseif($_POST['toAll']!=""){
  $sql =  mysqli_query($connection,"SELECT `todo_id`,`case_id`,`from_date`,`private` FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' AND `user_id`='".$toAll."' ORDER BY todo_id DESC LIMIT $startFrom, $showRecordPerPage");
}else{
  $sql =  mysqli_query($connection,"SELECT `todo_id`,`case_id`,`from_date`,`private` FROM `todo_list` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_id`='".base64_decode($_GET['caseid'])."' AND `division`='".$_SESSION['cityName']."' ORDER BY todo_id DESC LIMIT $startFrom, $showRecordPerPage");
}
$i=1;
$reminder_cnt = mysqli_num_rows($sql);
while($assigns = mysqli_fetch_array($sql)){
  // $sql1 = mysqli_query($connection, "SELECT *  FROM `todo_reminder` WHERE `todo_id`='".$assigns['todo_id']."' AND `division`='".$_SESSION['cityName']."'");
  // $rows = mysqli_fetch_array($sql1);
  $todo_team = mysqli_query($connection, "SELECT `content`  FROM `todo_list` WHERE `todo_id`='".$assigns['todo_id']."' AND `division`='".$_SESSION['cityName']."'");
  $rowstodo_team1 = mysqli_fetch_array($todo_team); ?>
  <div class="table-responsive">
    <table class="table">
      <tbody>
        <tr>
          <td><?php echo $i;?>) <a href="edit_todo.php?id=<?php echo base64_encode($assigns['todo_id']);?>" class="btn btn-info btn-mini btn-sm"><i class="feather icon-edit createtodolist" cursor="pointer" title="Edit" ></i></a>
           <a href="delete_todo.php?id=<?php echo base64_encode($assigns['todo_id']);?>&&flag=view&&cid=<?php echo $_GET['caseid'];?>" class="btn btn-danger btn-sm btn-mini"><i class="feather icon-trash createtodolist" cursor="pointer" title="Delete" onclick="return confirm('Are you sure you want to Delete?');"></i></a>
         </td>
         <td>
          <div class="checkbox-zoom zoom-primary">
            <label>
              <input type="checkbox" value="private" id="private" class="private">
              <span class="cr">
                <i class="cr-icon icofont icofont-ui-check txt-primary">
                </i>
              </span>
              <span><?php echo $rowstodo_team1['content'];?> </span>
            </label>
          </div>
          <p class="m-b-0"><i><b>Visibility: <?php echo ucfirst($assigns['private']);?></b>
            <?php  
            $reminder = mysqli_query($connection,"SELECT * FROM `todo_reminder` WHERE `todo_id`='".$assigns['todo_id']."' AND `division`='".$_SESSION['cityName']."'");
            $reminders = mysqli_fetch_array($reminder); 
            $rem_data = mysqli_query($connection,"SELECT * FROM `todo_reminder` WHERE `reminder_id`='".$reminders['reminder_id']."' AND `division`='".$_SESSION['cityName']."'");
            $rem_datas = mysqli_fetch_array($rem_data);?> 
          </i>     
        </p>
        <p class="m-b-0">
          <i><b>Auto Reminders:</b><?php echo $rem_datas['time_no'];?> <?php echo $rem_datas['minute'];?> before via <?php echo $rem_datas['email_option'];?></i>
        </p>

      </td>
      <td>
        <p class="m-b-0"><?php $sql2 = mysqli_query($connection, "SELECT `case_type`,`case_no`,`case_no_year`,`case_title` FROM `reg_cases` WHERE `case_id`='".$assigns['case_id']."'");
        $rows2 = mysqli_fetch_array($sql2);?>
        <?php  echo $rows2['case_type']; echo " "; echo $rows2['case_no']; echo " / "; echo $rows2['case_no_year']; echo " "; echo $rows2['case_title'];?></p>
      </td>
      <td>
       <p class="m-b-0"><i><b> Due at:</b></i><span> <?php echo date('d-m-Y H:i',strtotime($assigns['from_date']));?></span></p>
       <p class="m-b-0"><i><b>Assigned by: </b> <?php $reg =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
       $regs = mysqli_fetch_array($reg); echo $regs['name']; echo " "; echo $regs['last_name']; ?> </i></p>
       <p class="m-b-0"><i><b> Assigned to: </b><?php  $teams = mysqli_query($connection, "SELECT * FROM `todo_team` WHERE `todo_id`='".$assigns['todo_id']."'");
       while($teams_fetch = mysqli_fetch_array($teams)){
        $teamss =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$teams_fetch['assign_to']."'");
        $teams_fetchs = mysqli_fetch_array($teamss);?><?php echo $teams_fetchs['name'];?> <?php echo $teams_fetchs['last_name']; echo ", "; }?></i></p>
        <p class="m-b-0"><i><b> Assigned to Advocates: </b><?php  $teams = mysqli_query($connection, "SELECT * FROM `assign_toadvocates` WHERE `todo_id`='".$assigns['todo_id']."'");
        while($teams_fetch = mysqli_fetch_array($teams)){
          $teamss =mysqli_query($connection,"SELECT * FROM `advocate` WHERE `id`='".$teams_fetch['advocate_id']."'");
          $teams_fetchs = mysqli_fetch_array($teamss);?><?php echo $teams_fetchs['full_name'];?> <?php  echo ", "; }?></i></p>
        </td>
      </tr>

      <?php $i++; } ?>
    </tbody>
    <tfoot>

      <tr>
        <?php if($reminder_cnt >0){ ?>  <td>
         <ul class="pagination" style="float: right;">
          <?php if($currentPage != $firstPage) { ?>
            <li class="page-item">
              <a class="page-link" href="?page=<?php echo $firstPage ?>&&name=<?php echo $_GET['name'];?>" tabindex="-1" aria-label="Previous">
                <span aria-hidden="true">First</span>
              </a>
            </li>
          <?php } ?>
          <?php if($currentPage >= 2) { ?>
            <li class="page-item"><a class="page-link" href="?page=<?php echo $previousPage ?>&&name=<?php echo $_GET['name'];?>"><?php echo $previousPage ?></a></li>
          <?php } ?>
          <li class="page-item active"><a class="page-link" href="?page=<?php echo $currentPage ?>&&name=<?php echo $_GET['name'];?>"><?php echo $currentPage ?></a></li>
          <?php if($currentPage != $lastPage) { ?>
            <li class="page-item"><a class="page-link" href="?page=<?php echo $nextPage ?>&&name=<?php echo $_GET['name'];?>"><?php echo $nextPage ?></a></li>
            <li class="page-item">
              <a class="page-link" href="?page=<?php echo $lastPage ?>&&name=<?php echo $_GET['name'];?>" aria-label="Next">
                <span aria-hidden="true">Last</span>
              </a>
            </li>
          <?php } ?>
        </ul>
      </td>
    <?php } else{?>
      <td><p>There are no pending or upcoming reminders.</p></td>
    <?php } ?>
  </tr>
</tfoot>
</table>

</div>
</div>
</div>
</div>


