<?php include("header.php");?>
<!-- <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css"> -->
<link rel="stylesheet" href="css/vertical-layout-light/client.css"> 
<?php include("menu.php");?>
<!-- partial -->
<style type="text/css">
  .modal{
    top: 0px!important;
  }
  .pagination>li>a, .pagination>li>span {
    position: relative;
    float: left;
    padding: 6px 12px;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;
  }
  .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
    z-index: 3;
    color: #fff;
    cursor: default;
    background-color: #337ab7;
    border-color: #337ab7;
  }
  .pagination{
    float: right !important;
  }
</style>
<div class="main-panel">
  <div class="content-wrapper">

    <div class="card form-group">
      <div class="card-body">
        <h4 class="card-header client_card_header">Uploaded Matters</h4>
        <div class="form-group row client_topbuttons">
          <div class="col-sm-3"></div>

          <div class="col-sm-4"></div>
        </div>
        <div class="row main">
          <div class="col-12">

            <div class="table-responsive">
              <table id="order-listing" class="table">
                <thead>
                  <tr>
                    <th>Sr No.</th>
                    <th >TYPE OF MATTER</th>
                    <!-- <th>DETAILS</th> -->
                    <th style="text-align: center;">DOCUMENT TYPE</th>
                     <th style="text-align: center;">DOCUMENTS</th>
                    <th style="text-align: center;">INSTRUCTION</th>
                    <!-- <th style="text-align: center;">SERVICES OPTED</th> -->
                    <!-- <th style="width: 11%; text-align: center;">ACTION</th> -->
                    <!-- <th style="width: 22%; text-align: center;">ADD</th> -->
                    <!-- <th style="width: 100%; text-align: center;">PROCESS</th> -->
                  </tr>
                </thead>
                <tbody>
                 <?php  
                 if (isset($_GET['page_no']) && $_GET['page_no']!="") {
                  $page_no = $_GET['page_no'];
                } else {
                  $page_no = 1;
                }

                $total_records_per_page = 50;
                $offset = ($page_no-1) * $total_records_per_page;
                $previous_page = $page_no - 1;
                $next_page = $page_no + 1;
                $adjacents = "2"; 
                $result_count = mysqli_query($connection,"SELECT COUNT(*) As total_records FROM `case_registration` WHERE `case_id`='".base64_decode($_GET['caseid'])."' AND `remove`='Add'");
                $total_records = mysqli_fetch_array($result_count);
                $total_records = $total_records['total_records'];
                $total_no_of_pages = ceil($total_records / $total_records_per_page);
                $second_last = $total_no_of_pages - 1; // total page minus 1
                $docdetails = mysqli_query($connection,"SELECT `case_name`,`case_details`,`case_id` FROM `case_registration` WHERE `case_id`='".base64_decode($_GET['caseid'])."' AND `remove`='Add' ORDER BY `case_id` DESC LIMIT $offset, $total_records_per_page");
                $is=1;
                while($seldetails = mysqli_fetch_array($docdetails)){
                $doc =mysqli_query($connection,"SELECT `document_type` FROM `add_document` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
                $docs= mysqli_fetch_array($doc);?>
                <tr>
                  <td ><?php echo $is; ?></td>
                  <input type="hidden" name="email_id" id="email_id" value="<?php echo $rows['email_id'];?>">
                  <td><?php echo $seldetails['case_name']; ?></td>
                  <td><?php echo $docs['document_type']; ?></td>
                  <td><button type="button" class="btn btn-info btn-xs add_btn view_all" title="View All Documents" onclick="viewdocument(<?php echo $seldetails['case_id'];?>,<?php echo $count_doc;?>)">View All Documents</button></td>
                  <td class="editproTab">
                    <?php $string = strip_tags($seldetails['case_details']); if (strlen($string) > 300) {?><div class="desc"><?php echo $seldetails['case_details']; ?></div><b class="read_show" data-toggle="modal" data-target="#addmain" onclick="readmain(<?php echo $seldetails['case_id'];?>)">Read More</b><?php } else{?><div class="desc"><?php echo $seldetails['case_details']; ?></div><?php } ?>
                  </td>
            		</tr>
                <?php $is++; } ?>
                </tbody>
              </table>
          <ul class="pagination">
            <?php // if($page_no > 1){ echo "<li><a href='?page_no=1'>First Page</a></li>"; } ?>

            <li <?php if($page_no <= 1){ echo "class='disabled'"; } ?>>
              <a <?php if($page_no > 1){ echo "href='?page_no=$previous_page'"; } ?>>Previous</a>
            </li>

            <?php 
            if ($total_no_of_pages <= 10){     
              for ($counter = 1; $counter <= $total_no_of_pages; $counter++){
                if ($counter == $page_no) {
                  echo "<li class='active'><a>$counter</a></li>"; 
                }else{
                  echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages'>$counter</a></li>";
                }
              }
            }
            elseif($total_no_of_pages > 10){

              if($page_no <= 4) {     
                for ($counter = 1; $counter < 8; $counter++){    
                  if ($counter == $page_no) {
                    echo "<li class='active'><a>$counter</a></li>"; 
                  }else{
                    echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages'>$counter</a></li>";
                  }
                }
                echo "<li><a>...</a></li>";
                echo "<li><a href='?page_no=$second_last'>$second_last</a></li>";
                echo "<li><a href='?page_no=$total_no_of_pages&&page=$total_no_of_pages'>$total_no_of_pages</a></li>";
              }

              elseif($page_no > 4 && $page_no < $total_no_of_pages - 4) {    
                echo "<li><a href='?page_no=1'>1</a></li>";
                echo "<li><a href='?page_no=2'>2</a></li>";
                echo "<li><a>...</a></li>";
                for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {     
                  if ($counter == $page_no) {
                    echo "<li class='active'><a>$counter</a></li>"; 
                  }else{
                    echo "<li><a href='?page_no=$counter&&page=$total_no_of_pages'>$counter</a></li>";
                  }                  
                }
                echo "<li><a>...</a></li>";
                echo "<li><a href='?page_no=$second_last'>$second_last</a></li>";
                echo "<li><a href='?page_no=$total_no_of_pages'>$total_no_of_pages</a></li>";      
              }

              else {
                echo "<li><a href='?page_no=1'>1</a></li>";
                echo "<li><a href='?page_no=2'>2</a></li>";
                echo "<li><a>...</a></li>";

                for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                  if ($counter == $page_no) {
                    echo "<li class='active'><a>$counter</a></li>"; 
                  }else{
                    echo "<li><a href='?page_no=$counter'>$counter</a></li>";
                  }                   
                }
              }
            }
            ?>

            <li <?php if($page_no >= $total_no_of_pages){ echo "class='disabled'"; } ?>>
              <a <?php if($page_no < $total_no_of_pages) { echo "href='?page_no=$next_page'"; } ?>>Next</a>
            </li>
            <?php if($page_no < $total_no_of_pages){
              echo "<li><a href='?page_no=$total_no_of_pages'>Last &rsaquo;&rsaquo;</a></li>";
            } ?>
          </ul>
                </div>
              </div>
            </div>
          </div>
          <?php include("footer.php");?>
        </div>
        <style type="text/css">
          #dataTables_info,#order-listing_previous,#order-listing_paginate{
            display: none;
          }
        </style>
        <form name="myform" method="POST" action="downloadClient.php">
          <div class="modal" id="export">
            <div class="modal-dialog">
              <div class="modal-content" style="margin-left: -185px;width: 180%;">
                <div class="modal-header" style="background-color: #2d4866;color: white;">
                  <h4>Export</h4>
                  <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="full_name" checked="" />Full Name<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="website" />Website<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="office_address" />Office Address<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="age" />Age
                    </div>
                    <div class="col-sm-3">
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="email" checked=""/>Email Address<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="gst" />GSTIN<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="home_adress" />Home Address<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="father_name" />Father's Name
                    </div>
                    <div class="col-sm-3">
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="mobile" checked=""/>Phone Number<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="hourly_rate" />Hourly Rate (INR)<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="date_time" />Created Date<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="tin" />TIN #
                    </div>
                    <div class="col-sm-3">
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="company_name" />Company Name<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="pan_no" />PAN Number<br>
                      <input type="checkbox" style="margin-right: 5px;" name='checkboxvar[]' value="poin_of_contact" />Point of Contact(s)
                    </div>
                  </div><br><br>
                  <div class="row col-sm-12">
                    <div class="downlab"><h4>Download as:</h4></div>
                    <input type="radio" name="downloadas" value="pdf" style="margin-top: 4px;margin-left: 20px;">PDF
                    <input type="radio" name="downloadas" value="excel" style="margin-top: 4px;margin-left: 10px;" checked="">Excel
                    <button style="margin-top: -9px;border: 0px;margin-left: 35px;padding: 5px;background-color: #2d4866;color: white;">Download</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>

        <div class="modal" id="viewadvocate">
          <div class="modal-dialog">
            <div id="casedetails"></div>
          </div>
        </div>

<!--   <div class="card" id="mcar" style="display: none;width: 700px;position: absolute;left: 374px;">
  <div class="card-header">Matter Details</div>
  <div class="card-body">
    <div id="litigationdetails"></div>
  </div>
</div> -->

<div class="modal" id="exampleModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" >
        <h4 class="modal-title" style="font-weight: bold;margin-left: 10px">Matter Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <div id="litigationdetails"></div>
     </div>
   </div>
 </div>
</div>

<div class="modal" id="avail">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="border:none !important;padding: 5px; background-color: rgba(82, 117, 155, 0.5);">
        <h4 class="modal-title" style="font-weight: bold;margin-left: 10px">Avail More Services</h4>
        <button type="button" class="close" data-dismiss="modal" style="margin-top: -10px;margin-right: 10px;">&times;</button>
      </div>
      <div class="modal-body">
       <div id="avails"></div>
     </div>
   </div>
 </div>
</div>

<div class="modal" id="addcc">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body" style="padding: 0px;">
        <div id="info"></div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="viewdoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div id="doc" style="background-color: white;"></div>
  </div>
</div>
<script type="text/javascript">
  $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
  });

</script>
<script type="text/javascript" src="js/my_cases.js"></script>



