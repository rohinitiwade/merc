<link rel="stylesheet" type="text/css" href="styles/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="styles/datatable-buttons/buttons.dataTables.min.css">
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 


<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>

      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Groups</h5>
              </div>

              <div class="page-body">
                <div class="card form-group">
                 <div class="card-block">
                  <div class="col-12 form-group">
                    <button class="btn btn-info btn-sm" type="button" id="myBtn" data-target="#filter" data-toggle="modal">Add New</button>


                  </div>
                  <div class="col-12 table-responsive">
                    <table id="order-listing" class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#.</th>
                          <th>Group Name</th>
                          <th># of member(s)</th>
                          <th>Created By</th>
                          <th>Created At</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr> 
                          <td></td>
                          <td></td>
                          <td></td>
                          <td ></td>
                          <td ></td>
                          <td>
                            <i class="far fa-edit" style="margin-right: 5px" title="Edit"></i>
                            <i class="far fa-trash-alt" style="margin-right: 5px" title="Delete"></i>
                          </td>
                        </tr> 
                      </tbody>
                    </table>

                  </div>
                  <div class="modal" id="filter">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4>Create Group</h4>
                          <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-sm-4">
                              <label>Group Name</label><br>
                              <input type="text" class="form-control" placeholder="Enter Group Name">
                            </div>
                            <div class="col-sm-4">
                              <label>Team Member</label><br>
                              <div class="form-group">
                                <select id="framework" name="framework[]" multiple class="form-control" >
                                  <?php $team = mysqli_query($connection, "SELECT * FROM `team` ORDER BY id ASC");
                                  while($teams = mysqli_fetch_array($team)){?>
                                    <option value="<?php echo $teams['id'];?>"><?php echo  $teams['full_name'];?>  </option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                          </div><br><br>
                          <div class="row">
                            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary btn-sm">Submit</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/groups.js"></script>







