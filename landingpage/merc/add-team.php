<?php include("header.php"); ?>
<?php //include("chat-sidebar.php"); ?>
<?php //include("chat-inner.php"); ?> <!-- <link rel="stylesheet" href="css/vertical-layout-light/add_cases.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script>
$(document).ready(function(){
    $('#stateid').on('change', function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData_team.php',
                data:'state_id='+stateID,
                success:function(html){
                	alert(html);
                    $('#city').html(html);
                    $('#town').html('<option value="">Select City first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select State first</option>');
            $('#city').html('<option value="">Select City first</option>'); 
        }
    });

    $('#city').on('change', function(){
        var cityID = $(this).val();
        if(cityID){
            $.ajax({
                type:'POST',
                url:'ajaxData_team.php',
                data:'city_id='+cityID,
                success:function(html){
                    $('#town1').html(html);
                    $('#town').html('<option value="">Select City first</option>'); 
                }
            }); 
        }else{
            $('#town').html('<option value="">Select City first</option>');
        }
    });
    
   //----//
});
</script>
<body>
	<div class="pcoded-main-container">
		<div class="pcoded-wrapper">
			<?php include("menu.php") ?>
			<div class="pcoded-content">
				<div class="pcoded-inner-content">
					<div class="main-body">
	
						<div class="page-wrapper">
							<div class="page-header m-b-10">
								<h5 class=" m-b-20">Add Team Member</h5>
								<?php if(isset($_POST['submit'])){
									extract($_POST);
									$fromdate = $_POST['form_date'];
									$todate = $_POST['to_date'];
								}?>
								
								<?php
								if(isset($_POST['submit'])){
									extract($_POST);
									$caseno=$_POST['caseno'];
									$date1=$_POST['date1'];
									if($caseno!=''){
										$insert = mysqli_query($connection, "INSERT INTO  `add_expenses` SET `user_id`='".$_SESSION['user_id']."', `case_id`='".$caseno."',`date`='".$date1."',`particulars`='".$particulars."',`moneyspent`='".$moneyspent."',`method`='".$method."',`notes`='".$notes."',`division`='".$_SESSION['cityName']."',`under_division`='".$_SESSION['under_division']."',`datetime`='".date("Y-m-d")."'");

										$lastId =  mysqli_insert_id($connection);

										$error=array();
										$extension=array("jpeg","jpg","png","gif","pdf","xlsx","csv");
										foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name) {
											$file_name=$_FILES["image"]["name"][$key];
											$file_tmp=$_FILES["image"]["tmp_name"][$key];
											$ext=pathinfo($file_name,PATHINFO_EXTENSION);
											if(in_array($ext,$extension)) {
												if(!file_exists("upload_expimage/".$txtGalleryName."/".$file_name)) {
													move_uploaded_file($file_tmp=$_FILES["image"]["tmp_name"][$key],"upload_expimage/".$txtGalleryName."/".$file_name);
													 $addexpdoc = mysqli_query($connection, "INSERT INTO `addexpdoc`(`userid`,`exp_id`,`image`,`date_time`)VALUES('".$_SESSION['user_id']."','".$lastId."','".$file_name."','".date("Y-m-d")."')");
												}else {
													$filename=basename($filename,$ext);
													$newFileName=$filename.time().".".$ext;
													move_uploaded_file($file_tmp=$_FILES["image"]["tmp_name"][$key],"upload_expimage/".$txtGalleryName."/".$newFileName);
													$addexpdoc = mysqli_query($connection, "INSERT INTO `addexpdoc`(`userid`,`exp_id`,`image`,`date_time`)VALUES('".$_SESSION['user_id']."','".$lastId."','".$file_name."','".date("Y-m-d")."')");
												} ?>
												<script>
													window.location.href='my-expenses.php';
												</script>
												<?php 
											}
										}

									}
								}
								?>

								
									<!-- <div class="form-group"> -->
										<div class="card col-sm-12">
											<!-- <h5 class="card-title">Add Team Member</h5> -->
											<div class="page-body">
											<div class="card-block form-group row">
												<!-- <div class="col-sm-6 form-group"> -->
													<form class="row">
												<!-- 	  <div class="form-group">
													    <label for="exampleInputEmail1"><b style="font-size: 16px;"><span style="color: red;">* </span>Enter Name</b></label>
													    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" required="">
													  </div> -->

													  <div class="col-sm-6 col-lg-6 col-xs-12 col-md-12 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;"> <b style="font-size: 16px;"><span style="color: red;">* </span>Enter Name</b>
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8">
										 <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" required="">
									</div>
								</div>

														  <div class="col-sm-6 col-lg-6 col-xs-12 col-md-12 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;">	 <b style="font-size: 16px;"><span style="color: red;">* </span>Enter Last Name</b>
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8">
									   <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Last Name" required="">
									</div>
								</div>
										<!-- 			  <div class="form-group">
													    <label for="exampleInputPassword1"><b style="font-size: 16px;"><span style="color: red;">* </span>Enter Last Name</b></label>
													    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Last Name" required="">
													  </div> -->
												<!-- 	  <div class="form-group">
													    <label for="exampleInputPassword1"><b style="font-size: 16px;"><span style="color: red;">* </span>Enter Mobile Number</b></label>
													    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Mobile Number" required="">
													  </div> -->
													  						  <div class="col-sm-6 col-lg-6 col-xs-12 col-md-12 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;"> <b style="font-size: 16px;"><span style="color: red;">* </span>Enter Mobile Number</b>
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8">
										   <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Mobile Number" required="">
									</div>
								</div>

																  						  <div class="col-sm-6 col-lg-6 col-xs-12 col-md-12 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;"> <b style="font-size: 16px;"><span style="color: red;">* </span>Enter Email Id</b>
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8">
										  <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Enter Email Id" required="">
									</div>
								</div>

											<!-- 		  <div class="form-group">
													    <label for="exampleInputPassword1"><b style="font-size: 16px;"><span style="color: red;">* </span>Enter Email Id</b></label>
													    <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Enter Email Id" required="">
													  </div> -->
												<!-- 	  <div class="form-group">
													    <label for="exampleInputPassword1"><b style="font-size: 16px;"><span style="color: red;">* </span>Enter Mobile Number</b></label>
													    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Enter Mobile Number" required="">
													  </div> -->

													  <div class="col-sm-6 col-lg-6 col-xs-12 col-md-12 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;"> <b style="font-size: 16px;"><span style="color: red;">* </span>Select State</b>
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8">
											    <select  id="stateid" name="state" class="form-control" required="" >
                                                                        <option value="">Select State</option>
                                                                        <?php  $state = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `location_type`='STATE'");
                                                                        while($selstate = mysqli_fetch_array($state)){?>
                                                                        <option value="<?php echo $selstate['id'];?>"><?php echo $selstate['name'];?></option>
                                                                     <?php } ?>
                                                                    </select>
									</div>
								</div>
											<!-- 		  <div class="form-group">
													    <label for="exampleInputPassword1"><b style="font-size: 16px;"><span style="color: red;">* </span>Select State</b></label>
													    <select  id="stateid" name="state" class="form-control" required="" >
                                                                        <option value="">Select State</option>
                                                                        <?php  $state = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `location_type`='STATE'");
                                                                        while($selstate = mysqli_fetch_array($state)){?>
                                                                        <option value="<?php echo $selstate['id'];?>"><?php echo $selstate['name'];?></option>
                                                                     <?php } ?>
                                                                    </select>
													  </div> -->

											<!-- 		  <div class="form-group">
													    <label for="exampleInputPassword1"><b style="font-size: 16px;"><span style="color: red;">* </span>Select City</b></label>
													    <select id="city" name="city" class="form-control">
                                                                        <option value="">Select City Name</option>
                                                                    </select>
													  </div> -->

													 <div class="col-sm-6 col-lg-6 col-xs-12 col-md-12 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;"> <b style="font-size: 16px;"><span style="color: red;">* </span>Select City</b>
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8">
										    <select id="city" name="city" class="form-control">
                                                                        <option value="">Select City Name</option>
                                                                    </select>
									</div>
								</div>
										<!-- 			  <div class="form-group">
													    <label for="exampleInputPassword1"><b style="font-size: 16px;"><span style="color: red;">* </span>Select Town</b></label>
													     <select id="town1" name="town" class="form-control">
                                                                        <option value="">Select Town Name</option>
                                                                    </select>
													  </div> -->
                              <div class="col-sm-6 col-lg-6 col-xs-12 col-md-12 row form-group">
									<label for="exampleInputUsername1" class="col-sm-4 col-form-label" style="text-align: left;"> <b style="font-size: 16px;"><span style="color: red;">* </span>Select Town</b>
										<!-- <span class="lbl-short-desc">(Please enter the <span>Case number</span> assigned by court / कृपया कोर्टाने नियुक्त केलेला केस नंबर प्रविष्ट करा)</span> -->
									</label>
									<div class="col-sm-8">
										  		     <select id="town1" name="town" class="form-control">
                                                   <option value="">Select Town Name</option>
                                                          </select>
									</div>
								</div>
<div class="row col-sm-12">
												<!-- <button type="submit" class="btn btn-primary">Submit</button> -->

													   <button type="submit" class="btn btn-sm btn-primary newsubmit"  name="submit">Submit</button>&nbsp;&nbsp;
													  <a href=""><button class="btn btn-sm btn-danger" type="button">Cancel </button></a>
													</div>
													</form>
												<!-- </div> -->

											</div>

										</div>
									</div>
								<!-- </div> -->
							<!-- </div> -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include 'footer.php'; ?>
		<script type="text/javascript">
			$(".add-expenses-li").addClass('active pcoded-trigger');
		</script>
		<script type="text/javascript" src="scripts/add-expenses.js"></script>









