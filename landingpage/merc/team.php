<!-- <link rel="stylesheet" type="text/css" href="styles/dataTables.bootstrap4.min.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="styles/datatable-buttons/buttons.dataTables.min.css"> -->
<?php include("header.php") ?>
<?php //include("chat-sidebar.php") ?>
<?php //include("chat-inner.php") ?> 
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>

      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Team Members (<?php 
                  if($_SESSION['user_id']!='77'){
                 $teams = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE  `reg_id`!='77'");
                
              }else{
                  $teams = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE  `reg_id`='77'");
              }
              echo $cont = mysqli_num_rows($teams);?>)</h5>
             </div>

             <div class="page-body">
              <div class="card form-group">
               <div class="card-block">
                <div class="col-12 form-group">
                  <!-- <button type="button" class="btn btn-primary btn-sm" data-target="#logindetails" data-toggle="modal">User All Login History</button> -->
                  <!-- <a href="add-team.php" class="btn btn-info btn-sm  teamMember" ><i class="fa fa-plus" ></i>Add Team Member</a> -->

                  <a href="add-team" class="btn btn-warning btn-sm teamMember"><i class="fa fa-plus"  style="position: relative;left: -3px;"></i>Team Member</a>
                  <!-- <a href="groups.php" class="btn teamMember btn-info btn-sm "><i class="fa fa-users"></i>Groups</a> -->
                  <button type="button" class="btn btn-primary btn-sm" id="myBtn" data-target="#export" data-toggle="modal">Export</button>
                </div>
                <div class="col-12 table-responsive">
                  <table id="manage_team_dataTable" class="table table-bordered" >
                    <thead class="bg-primary">
                      <tr>
                        <th class="team_tableheader">Sr.No.</th>
                        <th class="team_tableheader">First Name</th>
                        <th class="team_tableheader">Last Name</th>
                        <!-- <th class="team_tableheader">Account Type</th> -->
                        <!-- <th class="team_tableheader">Working Rating</th> -->
                        <th class="team_tableheader">Email-Address</th>
                        <th class="team_tableheader">Mobile no.</th>
                         <th class="team_tableheader">Address</th>
                        <th class="team_tableheader">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        // $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE (`parent_id`='".$_SESSION['user_id']."'  AND (`division`='".$_SESSION['cityName']."' OR `division`='')) OR (`reg_id`='".$_SESSION['user_id']."' ) ORDER BY reg_id ASC");
                      if($_SESSION['user_id']!='77'){
                      $team = mysqli_query($connection, "SELECT * FROM `law_registration`  WHERE  `reg_id`!='77'");
                  }else{
                  	 $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='77'");
                  }
                      $i=1; 
                      while($selteam = mysqli_fetch_array($team)){
                        $team_comment = mysqli_query($connection, "SELECT `user_id`,`comment`,`rating`,`commented_by` FROM `team_comment` WHERE `user_id`='".$selteam['reg_id']."' ORDER BY id DESC");
                        $selrating = mysqli_fetch_array($team_comment);?>
                        <tr>
                          <td class="team_tablebody"><?php echo $i;?></td>
                          <td class="team_tablebody"><?php echo $selteam['name'];?></td>
                          <td class="team_tablebody"><?php echo $selteam['last_name'];?></td>
                          <!-- <td class="team_tablebody"><?php echo $selteam['designation'];?></td> -->
                          <!-- <td class="team_tablebody"><?php echo $selrating['rating'];?></td> -->
                          <td class="team_tablebody"><?php echo $selteam['email'];?></td>
                          <td class="team_tablebody"><?php echo $selteam['mobile'];?></td>
                            <td class="team_tablebody"><?php echo $selteam['address'];?></td>
                          <td class="team_tablebody">
                            <i class="fa fa-briefcase team_action" data-toggle="modal" data-target="#viewadvocate" onclick="viewcases(<?php echo $selteam['reg_id'];?>)" title="View Cases"></i>
                            <!-- <i class="fa fa-file team_action"  data-toggle="modal" data-target="#frequency_report" onclick="frequency(<?php echo $selteam['reg_id'];?>)" title="Frequency Report"></i> -->
                            <a data-toggle="modal" data-target="#viewhistory" onclick="loginhistory(<?php echo $selteam['reg_id'];?>)">
                              <i class="fa fa-history team_action" title="View Login History"></i>
                            </a>
                            <!-- <i class="fa fa-comment team_action" data-toggle="modal" data-target="#comment-modal" onclick="teamcomment(<?php echo $selteam['reg_id'];?>)" title="View Work Status"></i> -->
                            <i class="fa fa-edit team_action"  data-toggle="modal" data-target="#editteam" onclick="editteam(<?php echo $selteam['reg_id'];?>)" title="Edit Case"></i>
                            <!-- <?php if($selteam['reg_id']!=$_SESSION['user_id']){?> -->
                              <a href="delete_member.php?id=<?php echo base64_encode($selteam['reg_id']);?>"  class="trashicon" style="color:black;"><i class="fa fa-trash team_action" title="Delete" onclick="return confirm('Are you sure you want to Remove Team member?');"></i></a>
                              <!-- <?php }else{ ?><label class="badge badge-info">You are Admin </label><?php } ?> -->
                            </td>
                            </tr>
                            <?php $i++; } ?>
                          </tbody>
                        </table>

                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal" id="viewadvocate">
    <div class="modal-dialog">
      <div id="casedetails"></div>
    </div>
  </div>
  <div class="modal" id="frequency_report">
    <div class="modal-dialog">
      <div id="freq"></div>
    </div>
  </div>
    <div class="modal" id="export">
      <div class="modal-dialog modal-lg">

        <div class="modal-content" >
          <div class="modal-header">
            <h4>Export</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form method="POST" action="downloadClient.php">

              <div class="row form-group col-sm-12">

                <div class="form-radio col">
                  <div class="col-sm-5 radio radio-primary radio-inline">
                    <label>
                      <input type="radio" class="chkPassport" name="downloadas"  value="excel"> 
                      <i class="helper"></i>Excel
                    </label>
                  </div>
                  <div class="col-sm-6 radio radio-primary radio-inline">
                    <label>
                      <input type="radio" class="chkPassport" name="downloadas" value="pdf">      
                      <i class="helper"></i>PDF
                    </label>
                  </div>
                  <div id="document-table-div-export"></div>
                </div> 
                <button type="button" class="btn btn-warning btn-sm" name="download_advocate" onclick="download_doc()">Download</button>
              </div>
            </form>
          </div>
        </div>

      </div>
    </div>
    <div class="modal" id="logindetails">
      <div class="modal-dialog">
        <div class="modal-content" style="margin-left: -225px;width: 195%;">
          <div class="modal-header">
            <h4>Login History</h4>
            <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
          </div>
          <div class="modal-body">
            <form>
              <div class="row">
                <div class="col-sm-4">
                  <label for="email">User Name</label><br>
                  <select name="username" class="form-control" id="state">
                    <option value="">----Please Select-----</option>
                    <?php $teammember = mysqli_query($connection, "SELECT * FROM `team` ORDER BY  full_name ASC"); 
                    while($seectteammem = mysqli_fetch_array($teammember)){?>
                      <option value="<?php echo $seectteammem['id'];?>"><?php echo $seectteammem['full_name'];?>  <?php echo $seectteammem['last_name'];?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-sm-4">
                  <label for="email">From Date</label><br>
                  <input type="date" class="form-control" id="pwd" placeholder="Enter From Date" name="edate">
                </div>
                <div class="col-sm-4">
                  <label for="pwd">To Date</label>
                  <input type="date" class="form-control" id="pwd" placeholder="Enter To Date" name="edate">
                </div>
              </div><br><br>
              <div class="row">
                <input name="Reset" type="reset" onclick="reset()" style="margin-top: -9px;border: 0px;margin-left: 25px;padding: 5px;background-color: #2d4866;color: white;">
                <button style="margin-top: -9px;border: 0px;margin-left: 5px;padding: 5px;background-color: #2d4866;color: white;">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="modal" id="editteam">
      <div class="modal-dialog modal-lg">
        <div class="modal-content" id="edit_team">

          <!-- Modal Header -->
         <!--  <div class="modal-header">
            <h4 class="modal-title">Edit Team Member Details</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div> -->

          <!-- Modal body -->
         <!--  <div class="modal-body">
            <div></div>
          </div> -->

          <!-- Modal footer -->
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
          </div>
 -->
        </div>
      </div>
    </div>
    <div class="modal" id="viewhistory">
      <div class="modal-dialog">
        <div id="loghisto"></div>
      </div>
    </div>
    <div class="modal" id="comment-modal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div id="teamcomment"></div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>

        </div>
      </div>
    </div>
    <div class="modal" id="assignmember">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h5 class="modal-title">Assign Team Member</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <?php if(isset($_POST['add_member'])){
            extract($_POST);
            if($Teammember!=""){
              foreach ($Teammember as  $value) {
                $update = mysqli_query($connection, "UPDATE `law_registration` SET `parent_id`='".$member."' WHERE `reg_id`='".$value."'");

              }
            }
          }?>

          <!-- Modal body -->
          <div class="modal-body">
           <form action="" method="POST">
            <div class="form-group col-sm-12 row">
              <label for="email" class="col-sm-3"><b>Select Member:</b></label>
              <div class="col-sm-9">             
                <select class="form-control" name="Teammember[]" id="team-member" multiple>
                  <option value="">--Select Member--</option>
                  <?php $tamassig = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `parent_id`=0 AND `head`!='head'");
                  while($assignsel = mysqli_fetch_array($tamassig)){ ?>
                    <option value="<?php echo $assignsel['reg_id'];?>"><?php echo $assignsel['name'];?> <?php echo $assignsel['last_name'];?> (<?php echo $assignsel['division'];?>)</option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group col-sm-12 row">
              <label for="email" class="col-sm-3"><b>Assign To:</b></label>
              <div class="col-sm-9">             
                <select class="form-control" name="member" id="" >
                  <option value="">--Select Member--</option>
                  <?php $assignto = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `head`='head'");
                  while($selassignto = mysqli_fetch_array($assignto)){ ?>
                    <option value="<?php echo $selassignto['reg_id'];?>"><?php echo $selassignto['name'];?> <?php echo $selassignto['last_name'];?> (<?php echo $selassignto['division'];?>)</option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group form-check">
              <div class="checkbox-zoom zoom-primary">
                <label>
                  <input class="form-check-input" type="checkbox">
                  <span class="cr">
                    <i class="cr-icon icofont icofont-ui-check txt-warning"></i>
                  </span>
                  <span> Remember me</span>
                </label>
              </div>

            </div>
            <input type="submit" class="btn btn-primary btn-sm pull-right submit_modal" name="add_member" value="Submit">
            <button type="button" class="btn btn-danger btn-sm pull-right" data-dismiss="modal">Close</button>
          </form> 
        </div>
        <!-- Modal footer -->

      </div>
    </div>
  </div>

  <?php include 'footer.php'; ?>
<!--  <script src="scripts/buttons/dataTables.buttons.min.js" type="text/javascript"></script>

 <script src="scripts/buttons/pdfmake.min.js" type="text/javascript"></script>



 <script src="scripts/buttons/jszip.min.js" type="text/javascript"></script>


 <script src="scripts/buttons/buttons.print.min.js" type="text/javascript"></script>
 <script src="scripts/buttons/buttons.html5.min.js" type="text/javascript"></script> -->
 <script type='text/javascript' src='scripts/jspdf.min.js'></script>
 <script type='text/javascript' src="scripts/jspdf.plugin.autotable.min.js"></script>
 <script type='text/javascript' src="scripts/jquery.table2excel.min.js"></script>
 <script type="text/javascript" src="scripts/all_export.js"></script>
 <script type="text/javascript" src="scripts/manage-team.js"></script>







