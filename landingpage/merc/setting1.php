<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> 
<?php include("phpfile/sql_home.php"); ?>
<style type="text/css">
  .profile_pic_icon{
    position: absolute;
    bottom: 0px;
    color: black;
    background-color: white;
    font-size: 16px;
  }
  .user-title{
    bottom: 0px!important;
  }
</style>
<!-- <script>
$(document).ready(function(){
    alert("OK");
 $(document).on('change', '#file', function(){

  var name = document.getElementById("file").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase();
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
   alert("Invalid Image File");
  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("file").files[0]);
  var f = document.getElementById("file").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 2000000)
  {
   alert("Image File Size is very big");
  }
  else
  {
   form_data.append("file", document.getElementById('file').files[0]);
   $.ajax({
    url:"upload.php",
    method:"POST",
    data: form_data,
    contentType: false,
    cache: false,
    processData: false,
    beforeSend:function(){
     $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
    },   
    success:function(data)
    {
     $('#uploaded_image').html(data);
    }
   });
  }
 });
});
</script> -->
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title">Setting / सेटिंग</h5>
              </div>
              <?php $sql = mysqli_query($connection, "SELECT `name`,`last_name`,`image`,`designation`,`gender`,`email`,`mobile`,`address`,`dob`,`maritalstatus`,`twitterid`,`skypeid`,`website`,`about` FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                    $row = mysqli_fetch_array($sql);?>
                    <?php if(isset($_POST['submit'])){
                             extract($_POST);
                             $last_name=$_POST['last_name'];
                             $date1=$_POST['date1'];
                             $timestamp = strtotime($date1);
						                	$new_date = date("d-m-Y", $timestamp);
                             $gender=$_POST['gender'];
                             $maritalstatus=$_POST['maritalstatus'];
                             $details=$_POST['details'];                            	
                             if($name!=''){
                            $errors= array();
                            $file_name = $_FILES['file']['name'];
                            if($file_name){
             						  	$file_size =$_FILES['file']['size'];
        						        $file_tmp =$_FILES['file']['tmp_name'];
        						        $file_type=$_FILES['file']['type'];
        						        $file_ext=strtolower(end(explode('.',$_FILES['file']['name'])));
        						      if($file_size > 2097152){
        						         $errors[]='File size must be excately 2 MB';
        						      }			      
        						         move_uploaded_file($file_tmp,"uploads_profile/".$file_name);
        						         //echo $file_name = $_POST['fileimage'];
        						      }else{
                            echo $file_name = $_POST['fileimage'];
                          }
                              $Update=mysqli_query($connection,"UPDATE `law_registration` SET `name`='".$name."',`last_name`='".$last_name."',`gender`='".$gender."',`mobile`='".$mobile."',`dob`='".$new_date."',`maritalstatus`='".$maritalstatus."',`twitterid`='".$twitterid."',`skypeid`='".$skypeid."',`image`='".$file_name."',`address`='".$address."',`about`='".$details."' WHERE `reg_id`='".$_SESSION['user_id']."' ");
                             	if($Update)
                             	{
                             		?>
                             		<script type="text/javascript">
                             			alert("Profile Update Successfully");
                                  window.location.href='setting';
                             		</script>
                             		<?php
                             	}
                             }
                         } ?>
                    <form name="MyForm" method="POST" enctype="multipart/form-data">
              <div class="page-body">
                <div class="form-group">
                  <div class="card col-sm-12">
                    <div class="card-block form-group">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="cover-profile">
                            <div class="profile-bg-img">
                              <img class="profile-bg-img img-fluid" src="images/legal-banner.jpg" alt="bg-img">
                              <div class="card-block user-info">
                                <div class="col-md-12 row">
                                	<?php if ($row['image']!=''){?>
                                  <div class="media-left col-sm-2">
                                    <img class="user-img img-radius profile-image" src="uploads_profile/<?php echo $row['image'];?>" alt="user-img" style="width:100% !important	;">
                                    <div class="file-field profile_pic_icon">
                                    </div>
                                    <!-- <?php echo $row['image'];?> -->
                                    <input type="file" name="file" value="<?php echo $row['image'];?>"  id="uploadimg"  style="font-size:10px">
                                    <input type="hidden" name="fileimage" value="<?php echo $row['image'];?>">
                                    
                                  </div>
                              <?php } else { ?>
                              <div class="media-left col-sm-2">
                                    <img class="user-img img-radius profile-image" id="upload_image" src="uploads_profile/<?php if($row['gender']=='Male'){echo $row['image']='1576303937.png';}elseif($row['gender']=='Female') { echo $row['image']='femaleicon.png';}?>" alt="user-img" style="width:100% !important	;">
                                    <div class="file-field profile_pic_icon">
                                    </div>
                                    <input type="file" name="file" id="uploadimg" value="<?php echo $row['image'];?>" class="fa fa-camera" style="font-size:10px">
                                     <!-- <input type="hidden" name="file" value="<?php echo $row['image'];?>">  -->
                                  </div>
                                  <?php }?> 
                                  <div class="media-body row col-sm-10">
                                    <div class="col-lg-12 user-title">
                                      <h2><?php echo ucfirst($row['name']);?> <?php echo ucfirst($row['last_name']);?></h2>
                                      <span class="text-white"><?php echo ucfirst($row['designation']);?></span>
                                    </div>
                                    <div>
                                      <div class="pull-right cover-btn">
                                        <button type="button" class="btn btn-primary m-r-10 m-b-5">
                                          <i class="icofont icofont-plus">
                                          </i> Follow
                                        </button>
                                        <button type="button" class="btn btn-primary"><i class="icofont icofont-ui-messaging"></i> Message </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- <div class="container" style="width:700px;">
                       <h2 align="center">Upload File without using Form Submit in Ajax PHP</h2>
                       <br />
                       <label>Select Image</label>
                       <input type="file" name="file" id="file" />
                       <br />
                       <span id="uploaded_image"></span>
                      </div> -->
                      <div class="tab-header card">
                        <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                            <div class="slide"></div>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#binfo" role="tab">User's Services</a>
                            <div class="slide"></div>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#contacts" role="tab">User's Contacts</a>
                            <div class="slide"></div>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#review" role="tab">Reviews</a>
                            <div class="slide"></div>
                          </li>
                        </ul>
                      </div>
                      <div class="tab-content">
                        
                        
                        <div class="tab-pane active" id="personal" role="tabpanel">
                          <div class="card">
                            <div class="card-header">
                              <h5 class="card-header-text">About Me</h5>
                              <button id="edit-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right"><i class="icofont icofont-edit"></i></button>
                            </div>
                            <div class="card-block">
                              <div class="view-info">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="general-info">
                                      <div class="row">
                                        <div class="col-lg-12 col-xl-6">
                                          <div class="table-responsive">
                                            <table class="table m-0">
                                              <tbody>
                                                <tr>
                                                  <th scope="row">Full Name</th>
                                                  <td><?php echo ucfirst($row['name']);?> <?php echo ucfirst($row['last_name']);?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Gender</th>
                                                  <td><?php echo ucfirst($row['gender']);?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Birth Date</th>
                                                  <td><?php echo $row['dob'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Marital Status</th>
                                                  <td><?php echo $row['maritalstatus'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Location</th>
                                                  <td><?php echo ucfirst($row['address']);?></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                        <div class="col-lg-12 col-xl-6">
                                          <div class="table-responsive">
                                            <table class="table">
                                              <tbody>
                                                <tr>
                                                  <th scope="row">Email</th>
                                                  <td>
                                                    <a href="#!">
                                                      <span class="__cf_email__" data-cfemail="9edafbf3f1defbe6fff3eef2fbb0fdf1f3"><?php echo $row['email'];?></span>
                                                    </a>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Mobile Number</th>
                                                  <td><?php echo $row['mobile'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Twitter</th>
                                                  <td><?php echo $row['twitterid'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Skype</th>
                                                  <td><?php echo $row['skypeid'];?></td>
                                                </tr>
                                                <tr>
                                                  <th scope="row">Website</th>
                                                  <td>
                                                    <a href="#!"><?php echo $row['website'];?> </a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="edit-info">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="general-info">
                                      <div class="row">
                                        <div class="col-lg-6">
                                          <table class="table">
                                            <tbody>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-user">
                                                      </i>
                                                    </span>
                                                    <input type="text" name="name" class="form-control" placeholder="Enter Name" value="<?php echo ucfirst($row['name']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="form-radio">
                                                    <div class="group-add-on">
                                                      <div class="radio radiofill radio-inline">
                                                        <label>
                                                          <input type="radio" value="Male" name="gender" <?php if($row['gender']=='Male'){ echo "checked"; } ?>><i class="helper"></i>Male</label>
                                                      </div>
                                                      <div class="radio radiofill radio-inline">
                                                        <label>
                                                          <input type="radio" value="Female" name="gender" <?php if($row['gender']=='Female'){ echo "checked"; } ?>>
                                                          <i class="helper"></i> Female
                                                        </label>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <input type="text" name="date1" id="datepicker" value="<?php echo $row['dob'];?>" placeholder="Select Your Birth Date" class="form-control" required>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <select id="hello-single" name="maritalstatus" class="form-control">
                                                    <option value="<?php echo $row['maritalstatus'];?>"><?php if($row['maritalstatus']!=""){ echo $row['maritalstatus']; }else{?>---- Marital Status ----<?php } ?></option>
                                                    <option value="Married">Married</option>
                                                    <option value="Unmarried">Unmarried</option>
                                                  </select>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-location-pin"></i></span>
                                                    <input type="text" name="address" class="form-control" placeholder="Address" value="<?php echo $row['address'];?>">
                                                  </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-lg-6">
                                          <table class="table">
                                            <tbody>
                                            	<tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-user">
                                                      </i>
                                                    </span>
                                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?php echo ucfirst($row['last_name']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-mobile-phone"></i></span>
                                                    <input type="text" name="mobile" class="form-control" placeholder="Mobile Number" value="<?php echo ucfirst($row['mobile']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-social-twitter"></i></span>
                                                    <input type="text" name="twitterid" class="form-control" placeholder="Twitter Id" value="<?php echo ucfirst($row['twitterid']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-social-skype"></i></span>
                                                    <input type="text" name="skypeid" class="form-control" placeholder="Skype Id" value="<?php echo ucfirst($row['skypeid']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>
                                                  <div class="input-group">
                                                    <span class="input-group-addon">
                                                      <i class="icofont icofont-earth"></i></span>
                                                    <input type="text" name="website" class="form-control" placeholder="website" value="<?php echo ucfirst($row['website']);?>">
                                                  </div>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                     <!--  <div class="text-center">
                                        <a href="#!" class="btn btn-primary waves-effect waves-light m-r-20">Save</a>
                                        <a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
                                      </div> -->
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="card">
                                <div class="card-header">
                                  <h5 class="card-header-text">Description About Me</h5>
                                  <button id="edit-info-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right">
                                    <i class="icofont icofont-edit">
                                    </i>
                                  </button>
                                </div>
                                <div class="card-block user-desc">
                                  <div class="view-desc">
                                      <textarea name="details" id="query"  id="note-textarea" class="ckeditor" ><?php echo $row['about'];?></textarea>
                                      <?php
                                      include_once 'ckeditor/ckeditor.php';
                                      $ckeditor = new CKEditor('content');
                                      $ckeditor->basePath = 'ckeditor/';                               
                                      $ckeditor->replace(""); ?>
                                    <!-- <p><?php echo $row['about'];?></p> -->
                                  </div>
                                  <div class="edit-desc">
                                    <div class="text-center">
                                      <input type="submit" value="Update"  name="submit" class="btn btn-success waves-effect waves-light m-r-20 m-t-20">
                                      
                                      <a href="#!" id="edit-cancel-btn" class="btn btn-default waves-effect m-t-20">Cancel
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                        <div class="tab-pane" id="binfo" role="tabpanel">
                          <div class="card">
                            <div class="card-header">
                              <h5 class="card-header-text">User Services</h5>
                            </div>
                            <div class="card-block">
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="card b-l-success business-info services m-b-20">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Shivani Hero</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-danger business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Dress and Sarees</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.
                                          </p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-info business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Shivani Auto Port</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-warning business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Hair stylist</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-danger business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">BMW India</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="card b-l-success business-info services">
                                    <div class="card-header">
                                      <div class="service-header">
                                        <a href="#">
                                          <h5 class="card-header-text">Shivani Hero</h5>
                                        </a>
                                      </div>
                                      <span class="dropdown-toggle addon-btn text-muted f-right service-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="tooltip">
                                      </span>
                                      <div class="dropdown-menu dropdown-menu-right b-none services-list">
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-edit"></i> Edit
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-ui-delete"></i> Delete
                                        </a>
                                        <a class="dropdown-item" href="#!">
                                          <i class="icofont icofont-eye-alt"></i> View
                                        </a>
                                      </div>
                                    </div>
                                    <div class="card-block">
                                      <div class="row">
                                        <div class="col-sm-12">
                                          <p class="task-detail">Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod temp or incidi dunt ut labore et.Lorem ipsum dolor sit amet, consecte.</p>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="card">
                                <div class="card-header">
                                  <h5 class="card-header-text">Profit</h5>
                                </div>
                                <div class="card-block">
                                  <div id="main" style="height:300px;width: 100%;">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/settings.js"></script>
<!-- <script type="text/javascript">
	$(document).ready(function () {
		$(document).on('change','#file',function(){
			var property=document.getElementById("file").files[0];
			var image_name=property.name;
			var image_extension=iamge_name.split(".").pop().toLowerCase();
			if(jQuery.inArray(image_extension,['png','jpg','jpeg'])==-1)
			{
				alert("Invalid Image File");
			}
			var image_size=property.size;
			if(image_size>2000000){
				alert("Image size id high");
			}
			else
			{
				var form_data = new FormData();
				form_data.apend("file",property);
				$.ajax({
					url:"pimgaeupld.php",
					method:"POST",
					data:form_data,
					contentType:false,
					cache:false,
					processData:false,
					beforeSend:function(){
						$('#upload_image').html("<label class='text-success'>Image Uploading ...</label>");
					},
					success:function(data)
					{
						$('#upload_image').html(data);
					}
				})
			}

		});
	});
</script> -->
  <!-- <script type="text/javascript" src="scripts/my-expenses.js"></script> -->
