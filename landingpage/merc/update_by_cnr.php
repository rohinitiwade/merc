<?php include("header.php");?>
<?php include("menu.php");?>

<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
.span_btn{
	background: orange;
	color: #fff;
	font-weight: bold;
	font-size: 14px;
	font-family: Calibri;
	padding: 5px 10px;
	border-radius: 3px;
}
#order_listing tr th,
#order_listing tr td{
	padding: 10px;
	line-height: 1.462;
    white-space: normal !important;
    word-break: break-word;
    vertical-align: top;
}
</style>

<div class="main-panel">
	<div class="content-wrapper">
    <?php include("slider.php");?>
    <div class="card">
		<div class="card-body">
			<h4 class="card-title"></h4>
			
			<?php
			$query = "SELECT RC.CNR, RC.court_id, CL.court_name FROM `reg_cases` RC 
			LEFT JOIN court_list CL ON RC.court_id = CL.court_id
			WHERE RC.user_id = ".$_SESSION['user_id']." AND RC.CNR != '' AND CL.court_name != '' AND hearing_date <= '".date("Y-m-d")."'";
			$sql = mysqli_query($connection, $query);
			
			$cnr_array = array();			
			while($sel_cnr = mysqli_fetch_array($sql)){
				$cnr_object = new stdClass();
				$cnr_object->CNR = $sel_cnr['CNR'];
				$cnr_object->court_name = $sel_cnr['court_name'];
				$cnr_array[] = $cnr_object;
			}
			//print_r($cnr_array);
			?>
			<?php //echo implode(",",$cnr_array); ?>
			<?php //echo json_encode($cnr_array); ?>
			<span id="cnr_array" style="display:none;"><?php echo json_encode($cnr_array); ?></span>
			
			<span class="span_btn">Total CNR found : <?php echo  mysqli_num_rows($sql);?></span>
			&nbsp;
			<button id="fetch_data_cnr" class="btn">Fetch Data by CNR</button>
			
			<button id="update_data_cnr" class="btn" style="display:none;">Update CNR</button>
			&nbsp;
			<span class="span_btn cnr_exist_btn" style="display:none;">CNR already exist : <span id="cnr_exist">0</span></span>
			&nbsp;
			<span class="span_btn cnr_updated_btn" style="display:none;">CNR updated : <span id="cnr_updated">0</span></span>
			&nbsp;
			<span class="span_btn cnr_skiped_btn" style="display:none;">CNR skiped : <span id="cnr_skiped">0</span></span>
		</div>
	</div>
	<br/>
	<div class="card">
		<div class="card-body">
			<h4 class="card-header">My Law Diary</h4>
			<br/>
			<div class="form-row">
			<div class="col-sm-2">
				<label for="from_datepicker" class=" col-form-label">
					<b>From</b></label>
				<input type="text" id="from_datepicker" class="form-control" placeholder="From Date">
			</div>
			<div class="col-sm-2">
				<label for="to_datepicker" class=" col-form-label">
					<b>To</b></label>
				<input type="text" id="to_datepicker" class="form-control" placeholder="To Date">
			</div>
			<div class="col-sm-2">
				<label for="exampleInputUsername1" class=" col-form-label">
					<b>&nbsp;</b></label>
					<br/>
				<button id="submit_date" class="btn newsubmit btn-sm">Search</button>
			</div>
			</div>
			<br/>
			<div class="table-responsive">
				<table id="order_listing" class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th><input type="checkbox" class="check" id="checkAll" /></th>
							<th>Court</th>
							<th>Case No / Year</th>									
							<th>Title</th>
							<th>Team Member(s)</th>
							<th>Hearing Date</th>
							<th>Last Hearing Date</th>
							<th>Stage</th>
							<th>Case Description</th>
							<th>CNR</th>
						</tr>
					</thead>
					
					
				</table>
			</div>
			
		</div>
  </div>
		</div>
		
<?php include("footer.php");?>
	</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/pbkdf2.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/update_by_cnr.js"></script>