<?php include("header.php"); ?>
<?php include("chat-sidebar.php"); ?>
<?php include("chat-inner.php"); ?> 
<body>
  <div class="pcoded-main-container">
    <div class="pcoded-wrapper">
      <?php include("menu.php") ?>
      <div class="pcoded-content">
        <div class="pcoded-inner-content">
          <div class="main-body">
            <div class="page-wrapper">
              <div class="page-header m-b-10">
                <h5 class="card-title"> Add Timesheet </h5>
              </div>
              <div class="page-body">
                <div class="card form-group">									
                  <div class="card-block">
                <?php
    						if(isset($_POST['submit'])){
    						extract($_POST);
    						$date = date('Y-m-d H:i:s');
    						if($case!=""){
    						$insert =  mysqli_query($connection,"INSERT INTO `add_timesheet` SET `case_id`='".$case."', `user_id`='".$_SESSION['user_id']."',`edate`='".$date1."',`particulars`='".$particulars."',`timespent`='".$timespent."',`timeinmin`='".$timeinmin."',`type`='".$type."',`datetime`='".$date."',`division`='".$_SESSION['cityName']."',`under_division`='".$_SESSION['under_division']."'");
    						if($insert){
    						?>
    						<script type="text/javascript">
    						    alert('Timesheet Add Succefully');
    						    window.location.href='manage-timesheets';
    						 </script>
				         	<?php } } }?>
                    <form class="forms-sample" method="POST" action="">
                      <div class="form-group row forms-sample-div" style="">
                        <div class="col-sm-6">
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;"><b>*</b></span>&nbsp;&nbsp;Case
                            </label>
                            <div class="col-sm-8">
                              <select class="form-control" id="document" name="case" required="">
                                <option value="">--Search Case--</option>
                                <?php  
                              $casedocuments = mysqli_query($connection, "SELECT `case_id`,`court_id`,`case_type_master`,`case_type`,`case_no`,`case_no_year` FROM `reg_cases` WHERE `user_id`='".$_SESSION['user_id']."' AND `case_no`!='' AND `remove_status`=1");
                        while($selcses = mysqli_fetch_array($casedocuments)){?>
                                <option value="<?php echo $selcses['case_id'];?>">
                                  <?php if($selcses['court_id'] == 2){ echo $selcses['case_type_master'];}else{echo $selcses['case_type'];}?> 
                                  <?php echo $selcses['case_no'].'/'. $selcses['case_no_year'];?>
                                </option>
                                <?php } ?>
                              </select>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;"><b>*</b>
                              </span>&nbsp;&nbsp;Particulars
                            </label>
                            <div class="col-sm-8">
                            	<textarea class="form-control" name="particulars" placeholder="Enter Particulars Here" required=""></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;">
                                <b>*</b>
                              </span>&nbsp;&nbsp;Date
                            </label>
                            <div class="col-sm-8">
                              <input type="text" name="date1" placeholder="Enter Date" id="datepickertime" class="form-control" required="">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="exampleInputUsername1" class="col-sm-3 col-form-label">
                              <span style="color: red;">
                                <b>*</b>
                              </span>&nbsp;&nbsp;Time Spent(in Hours)
                            </label>
                            <div class="col-sm-4 form-group">
                              <select class="form-control" name="timespent" id="time-spent">
                              </select>
                            </div>
                            <div class="col-sm-4 form-group">
                              <select class="form-control" name="timeinmin" id="time-in-min">
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-12 row">
                        <label class="col-sm-2">Type</label>
                        <div class="col form-radio">
                          <div class="radio radio-inline">
                            <label>
                              <input type="radio" value="Effective" name="type">
                              <i class="helper"></i> Effective
                            </label>
                          </div>
                          <div class="radio radio-inline">
                            <label>
                              <input type="radio" value="Non Effective" name="type">
                              <i class="helper">
                              </i> Non Effective / Procedural Appearance
                            </label>
                          </div>
                        </div>
                        <div class="col-sm-12 col-xs-12 text-center">
                        <input class="btn btn-sm btn-info newsubmit" value="Submit" type="submit" name="submit"> &nbsp;
                        <a href="manage-timesheets.php" class="btn btn-sm btn-danger">Cancel
                        </a>
                      </div>
                      </div>
                    </form> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 'footer.php'; ?>
  <script type="text/javascript" src="scripts/add-timesheet.js"></script>
  <script>
  	$(document ).ready(function(){
      $("#document").select2();
      $("#time-spent").select2();
      $("#time-in-min").select2();
       });
  </script>
</body>
<script>
  $( function() {
    $( "#datepickertime").datepicker({
      dateFormat: 'yy-mm-dd',
      changeYear: true,
      changeMonth: true
    });
  } );
</script>