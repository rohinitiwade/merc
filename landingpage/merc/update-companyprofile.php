<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
      <?php include("menu.php");?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="card">
            <div class="card-body">
              <h4 class="card-header" style="padding-top: 0px">Update Company Profile</h4>
              <div class="row" style="margin-top: 25px;">
                <div class="col-12">
                  <div class="form-group row">
                    <div class="col-sm-6 imgdiv">
                      <img src="http://crm.airinfotech.in/law/images/4.jpg">
                    </div>
                    <div class="col-sm-6 uploaddiv">
                      <h3>Upload Company Logo</h3>
                      <span>You can upload JPG, GIF or PNG (file size limit is 4 MB). And, size should be 200px X 200px</span><br><br>
                      <button class="upload_button">Change</button>
                      <button class="upload_button">Delete</button>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Company Name</b></label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Company Email Address</b></label>
                    <div class="col-sm-3">
                      <input type="text" class="form-control">
                    </div>
                    <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Website</b></label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>GST Identification Number(GSTIN)</b></label>
                    <div class="col-sm-3">
                      <input type="text" class="form-control">
                    </div>
                    <label class="col-sm-2 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>Permanent Account Number(PAN)</b></label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><span style="color: red;"><b>*</b></span>&nbsp;&nbsp;<b>About Company</b></label>
                    <div class="col-sm-9">
                      <textarea class="form-control" rows="4" cols="50"></textarea>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="container">
                      <table id="myTable" class=" table order-list" width="100">
                      <thead>
                          <tr>
                              <td>Full Name</td>
                              <td>Email Address</td>
                              <td>Phone Number</td>
                              <td>Designation</td>
                              <td>Action</td>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td class="col-sm-4">
                                  <input type="text" name="name" class="form-control" />
                              </td>
                              <td class="col-sm-4">
                                  <input type="mail" name="mail"  class="form-control"/>
                              </td>
                              <td class="col-sm-4">
                                  <input type="text" name="phone"  class="form-control"/>
                              </td>
                               <td class="col-sm-4">
                                  <input type="text" name="designation"  class="form-control"/>
                              </td>
                              <td class="col-sm-2"><a class="deleteRow"></a>
                              </td>
                            </tr>
                        </tbody>
                        <tfoot>
                           <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td colspan="" style="text-align: left;">
                                 <button type="button" class="add-opponent add-more-button"  id="addrow"> Add More </button>
                              </td>
                           </tr>
                           <tr>
                           </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                  <div class="page_buttons">
                    <button class="page_submit">Submit</button>
                    <a type="button" href="index.php" class="page_cancel btn btn-danger">Cancel</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
<?php include("footer.php");?>
<style type="text/css">
  .page_buttons{
    width: 100%;
    text-align: right;
  }
  .page_submit{
    border:0px;
    background-color: #2d4866;
    color: white;
    padding: 8px 10px;
  }
  .page_cancel{
    border:0px;
    color: white;
    padding: 8px 10px;
  }
  .upload_button{
    border:0px;
    background-color: #2d4866;
    color: white;
    padding: 5px 8px;
  }
  .imgdiv{
    text-align: right;
    float: left;
  }
  .uploaddiv{
    align-self: center;
  }
 .imgdiv img{
    height: 110px;
    /*width: 200px;*/
    border-radius: 100px;
    filter: drop-shadow(1px 2px 8px black);
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  var actions = $("table td:last-child").html();
  // Append table with add row form on add new button click
    $(".add-new").click(function(){
    $(this).attr("disabled", "disabled");
    var index = $("table tbody tr:last-child").index();
        var row = '<tr>' +
            '<td><input type="text" class="form-control" name="name" id="name"></td>' +
            '<td><input type="text" class="form-control" name="department" id="department"></td>' +
            '<td><input type="text" class="form-control" name="phone" id="phone"></td>' +
            '<td><input type="text" class="form-control" name="designation" id="designation"></td>' +
      '<td>' + actions + '</td>' +
        '</tr>';
      $("table").append(row);   
    $("table tbody tr").eq(index + 1).find(".add, .edit").toggle();
        $('[data-toggle="tooltip"]').tooltip();
    });
  // Add row on add button click
  $(document).on("click", ".add", function(){
    var empty = false;
    var input = $(this).parents("tr").find('input[type="text"]');
        input.each(function(){
      if(!$(this).val()){
        $(this).addClass("error");
        empty = true;
      } else{
                $(this).removeClass("error");
            }
    });
    $(this).parents("tr").find(".error").first().focus();
    if(!empty){
      input.each(function(){
        $(this).parent("td").html($(this).val());
      });     
      $(this).parents("tr").find(".add, .edit").toggle();
      $(".add-new").removeAttr("disabled");
    }   
    });
  // Edit row on edit button click
  $(document).on("click", ".edit", function(){    
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
      $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
    });   
    $(this).parents("tr").find(".add, .edit").toggle();
    $(".add-new").attr("disabled", "disabled");
    });
  // Delete row on delete button click
  $(document).on("click", ".delete", function(){
        $(this).parents("tr").remove();
    $(".add-new").removeAttr("disabled");
    });
});
$(document).ready(function () {
    var counter = 0;

    $("#addrow").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><input type="text" class="form-control" name="name' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="mail' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="phone' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="designation' + counter + '"/></td>';

        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });



    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();       
        counter -= 1
    });


});
</script>