<?php include("header.php") ?>
<link rel="stylesheet" type="text/css" href="styles/bill-page.css">
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<style type="text/css">
	.card-header h4{
		border-bottom:1px solid lightgray;
	}
</style>
<?php session_start(); ?> 
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<?php include("menu.php") ?>
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<div class="main-body">
					<div class="page-wrapper">
						<div class="page-header m-b-10">
							<h5 class="card-title"> Bill Payment </h5>
						</div>
						<div class="page-body">
							<div class="card">
								<div class="card-block">
									<div class="col-sm-12 form-group" style="text-align: right;">
										<a class="add_new_bill btn btn-primary btn-sm" href="">View Bill</a>                    
									</div>

									<div class="card-header p-0">
										<h4>kjfdjfd</h4>
									</div>

									<div class="invoice-payment"> 
										<div class="invoice-number form-group">Bill: 02</div>
										<div class="net-balance form-group">
											The Balance due on this bill is <strong>46.42</strong> INR.
										</div>
										<form id="invoice-payment-form" class="row" action="" method="post" enctype="multipart/form-data">

											<div class="row form-group col-sm-6">
												<b class="col-sm-4"><span class="manditary-field">*</span>Payment (INR):</b>
												<div class="required col-sm-8">
													<input type="text" id="billspayments-payment" class="form-control" value="46.42" aria-required="true">

												</div>                    
											</div>
											<div class="row form-group col-sm-6">
												<b class="col-sm-4"><span class="manditary-field">*</span>Method:</b>
												<div class="required col-sm-8">
													<select class="form-control">
														<option value="">Please select</option>
														<option value="Bank Transfer">Bank Transfer</option>
														<option value="Cash">Cash</option>
														<option value="Cheque">Cheque</option>
														<option value="Online Payment">Online Payment</option>
														<option value="Other">Other</option>
													</select>
												</div>
											</div>
											<div class="row form-group col-sm-6 otherPaymentMethodTextBox hide">
												<b class="col-sm-4"><span class="manditary-field">*</span>Other payment method:</b>
												<div class="col-sm-8">
													<input type="text" id="billspayments-other_payment_method" class="form-control">
												</div> 
											</div>
											<div class="row form-group col-sm-6">
												<b class="col-sm-4"><span class="manditary-field">*</span>Date:</b>
												<div class="col-sm-8 required">
													<input type="text" id="payment_date" readonly class="hasDatepicker form-control">
												</div>                    
											</div>
											<div class="row form-group col-sm-6">
												<b class="col-sm-4">Notes:</b>
												<div class="col-sm-8">
													<textarea id="billspayments-notes" class="form-control" name="BillsPayments[notes]"></textarea>
												</div>         
											</div>
											<div class="row form-group col-sm-6">
												<b class="lbl col-sm-4"><span>Attachment(s):</span></b>
												<div class="form-group col-sm-8">
													<input type="file" multiple="multiple">
												</div> 
											</div>
											<div class="row form-group col-sm-6">
												<div class="txt_bx">                                        
													<span class="submit_btn_group">
														<button type="submit" id="submitPayment" class="btn btn-info btn-sm" name="InvoiceSubmit" value="Save">Save</button>
														<button type="button" class="btn btn-danger btn-sm" name="InvoiceCancel" value="Cancel" onclick="history.back();">Cancel</button>
													</span>
												</div>
											</div>

										</form>        </div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--</div> -->
	<?php include 'footer.php'; ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$( "#payment_date" ).datepicker({
				dateFormat: 'yy/mm/dd',
				changeYear: true,
				changeMonth: true,
				yearRange: "-100:+0"
			});
		})
	</script>