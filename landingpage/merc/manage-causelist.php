<?php include("header.php");?>
  <link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
      <?php include("menu.php");?>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <?php include("slider.php");?>
          <div class="card form-group">
            <div class="card-body">
              <h4 class="card-header" style="padding-top: 0px">Cause Lists (0)</h4>
              <a href="add-causelist.php"><button type="button" class="add-more-button"style="position: absolute;left: 700px;height: 38px;z-index: 1;top: 85px;"> Add New </button></a>
              <div class="row" style="margin-top: 25px;">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th style="background-color: #353eb3; color: white;">Sr.No.</th>
                            <th style="background-color: #353eb3; color: white;">Advocate Name</th>
                            <th style="background-color: #353eb3; color: white;">Court</th>
                            <th style="background-color: #353eb3; color: white;">Court Name</th>
                            <th style="background-color: #353eb3; color: white;">Bench</th>
                            <th style="background-color: #353eb3; color: white;">Actions</th>
                        </tr>
                      </thead>
                      <?php  $sql=mysqli_query($connection,"SELECT cl.*,co.*,hc.*,be.* FROM causelist as cl ,court_list as  co,high_court_list as hc ,bench as be where cl.court!='' and cl.user_id='".$_SESSION['user_id']."' AND cl.court=co.court_id AND cl.highcourt_name=hc.court_id AND cl.bench=be.bench_id");
                      $i=1;
                      while($row=mysqli_fetch_array($sql)) {?>
                      <tbody>
                   			<tr>
                   				<td><?php echo  $i;?></td>
                   				<td><?php echo  $row['advocate_name'];?></td>
                   				<td><?php echo  $row['court_name'];?></td>
                   				<td><?php echo  $row['high_court_name'];?></td>
                   				<td><?php echo  $row['bench_name'];?></td>
                   				<td><a href="update-causelist.php?id=<?php echo $row['cause_listid'];?>"><i class="fa fa-edit action-css" style="" title="Edit Causelist"></i></a>
                      <a href="delete_exp.php?id=<?php echo $row['cause_listid'];?>" onclick="return confirm('Are you sure you want to delete?');"><i class="fa fa-trash action-css" style="" title="Delete"></i></a></td>
                   			</tr>
                      </tbody>
                      <?php $i++;}?>
                    </table>
                       <tr>
                        <td colspan="10"></td>

                      </tr>
                </div>
                </div>
              </div>
            </div>
          </div>
          <?php include("footer.php");?>
        </div>

<script type="text/javascript">
  $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
});

</script>