<!-- <link rel="stylesheet" type="text/css" href="styles/caselaw-search.css"> -->
<?php include("header.php") ?>
<?php include("chat-sidebar.php") ?>
<?php include("chat-inner.php") ?> 
<?php session_start(); ?> 
<style type="text/css">
  .heading_legal{
  font-size: 16px;
    text-align: center;
    padding-bottom: 2%;
}
#LegalResponse td{
  font-size: 14px;
    padding: 0.7rem;
}
.subhead_legal{
  width: 25%;
}
</style>
<div class="pcoded-main-container">
  <div class="pcoded-wrapper">
    <?php include("menu.php") ?>
    <div class="pcoded-content">
      <div class="pcoded-inner-content">
        <div class="main-body">
          <?php $case = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".$_GET['caseid']."'");
          $selcase = mysqli_fetch_array($case);
           if($selcase['supreme_court'] == 'Diary Number')
                                     $case =  $selcase['diary_no'].'/ '.$selcase['diary_year'];
                                    else
                                     $case = $selcase['case_type'].' '.$selcase['case_no'].'/ '.$selcase['case_no_year']; ?>
          <div class="page-wrapper">
            <div class="page-body">
             <div class="card">
              <div class="card-block">
                <div class="row">
                  <input type="hidden" name="case_id" id="case_id" value="<?php echo base64_decode($_GET['caseid'])?>">
                  <input type="hidden" name="org_id" id="org_id" value="<?php echo ($_SESSION['organisation_id'])?>">
                    <div id="legal_data" class="col-sm-12"></div>
               </div>
             </div>
           </div>
         </div>
<!-- <div id="styleSelector">
</div> -->
</div>
</div>
</div>
</div>
</div>

<div id="ask-query" class="modal fade" role="dialog">
  <div class="modal-dialog" style="max-width: 928px;">
    <div class="modal-content">
      <div class="" style="background-color: white;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: #f10038;
        text-align: center;">Ask a Query</h4>

      </div>
      
      <div class="modal-body">

         <form class="forms-sample row" autocomplete="off">
         <div class="form-group col-sm-12" id="" >
    <label for="exampleInputUsername1" class="col-form-label"> Raise your Question Here 
      <!-- <span class="lbl-short-desc"><i>(Please enter primary details about the case, subject, CNR Number,etc)</i></span> -->
    </label>

    <!-- <textarea name="details" id="txtEngToHindi" class="form-control"></textarea>  -->
    <textarea name="details" id="askquery" class="ckeditor"></textarea> 
    <div class="col-sm-12 pt-3" style="text-align:center;"><button class="btn btn-sm btn-info" type="button" onclick="askQueToCLR()">Submit</button></div>
  </div>
         </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>


  </div>


</div>
<!-- </div>
</div> -->
<?php include 'footer.php'; ?>
<script type="text/javascript" src="scripts/viewLegalAudit.js"></script>

