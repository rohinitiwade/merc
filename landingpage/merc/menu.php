<!-- .pcoded[theme-layout="vertical"] .pcoded-navbar .pcoded-item>li .submenu{
	height:auto;
} -->

<nav class="pcoded-navbar">
	<div class="pcoded-inner-navbar main-menu">
		<!-- <div class="pcoded-navigatio-lavel">Navigation</div>
		<ul class="pcoded-item pcoded-left-item">
			
		</ul>
		<div class="pcoded-navigatio-lavel">Add/Manage Cases</div> -->
		<ul class="pcoded-item pcoded-left-item">

		<li class="header-notification mobilelogout">
		<div class="input-group input-search-div-header">
			<form class="form-inline  " method="POST" autocomplete="off"> 
				<input class="form-control caseno_search form-control-sm searchinput" id="caseno_search" type="text" placeholder="Enter Case no./ Party Name" aria-label="Search">
				<i class="feather icon-search input-group-addon" id="basic-addon1"></i>
			</form>
		</div>
		<div id="suggesstion-box" class="suggesstion-box animated fadeInLeft"></div>
		<div id="party-name-div" class="party-name-div animated fadeInLeft"></div>
	</li>
			<li class="dashboard">
				<a href="index.php">
					<span class="pcoded-micon"><i class="feather icon-home"></i></span>
					<span class="pcoded-mtext">Dashboard</span>
				</a>
			</li>
			<li class="pcoded-hasmenu">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
					<span class="pcoded-mtext">Add/Manage Cases</span>
				</a>
				<ul class="pcoded-submenu">
								<li class="add-case-li">
						<a href="add-casesmerc.php">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext"> Add Case</span>
						</a>

					</li> 
							<!-- 	<li class="add-nonjudicialcase-li">
						<a href="nonjudicial_addcase.php">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Add Non Judicial Case</span>
						</a>

					</li>  -->
<!-- 											<li class="pcoded-hasmenu">
					<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
					<span class="pcoded-mtext">Add Cases</span>
				</a>
		<ul class="pcoded-submenu">
				<li class="add-case-li">
						<a href="add-cases">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext"> Judicial Add Case</span>
						</a>

					</li> 
				<li class="add-case-li">
						<a href="nonjudicial_addcase">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Non Judicial Add Case</span>
						</a>

					</li> 
		</ul>

			</li> -->
					<li class="manage-case-li ">
						<a href="cases-report.php">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Manage Case</span>
						</a>
					</li>
					<li class="add-nonjudicialcase-li">
						<a href="multipleDocument.php">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Attach Document</span>
						</a>

					</li> 
						<li class="add-nonjudicialcase-li">
						<a href="multipleCaseidReport.php">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Case Id Report</span>
						</a>

					</li> 
				</ul>




			</li>
			<li class="pcoded-hasmenu">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="icofont icofont-book-alt"></i></span>
					<span class="pcoded-mtext">Legal Support</span>
				</a>
				<ul class="pcoded-submenu">
					<li class="add-client-li">
						<a href="legal-support.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">Request For Legal Support</span>
						</a>
					</li>
					<li class=" ">
						<a href="viewLegalSupport.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">View Response</span>
						</a>
					</li>
				</ul>
			</li>
			<!-- <li class="pcoded-hasmenu">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="icofont icofont-group"></i></span>
					<span class="pcoded-mtext">Team</span>
				</a>
				<ul class="pcoded-submenu">

					<li class=" ">
						<a href="team.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">Manage Team</span>
						</a>
					</li>
				</ul>
			</li> -->
			<li class="pcoded-hasmenu">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="feather icon-file"></i></span>
					<span class="pcoded-mtext">Documents</span>
				</a>
				<ul class="pcoded-submenu">

					<li class=" ">
						<a href="merc_document.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">Add Document</span>
						</a>
					</li>
					<li class=" ">
						<a href="manage-document.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">Manage Document</span>
						</a>
					</li>
				</ul>
			</li>

			<!-- <li class=" ">
				<a href="legal-support.php">
					<span class="pcoded-micon"><i class="icofont icofont-book-alt"></i></span>
					<span class="pcoded-mtext">Legal Support</span>
				</a>
			</li> -->
			<!-- <li class="pcoded-hasmenu">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
					<span class="pcoded-mtext">Avail Services</span>
				</a>
				<ul class="pcoded-submenu avail_services_scroll">
					<li class="drafting-li">
						<a href="drafting">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Drafting</span>
						</a>
					</li>
					<li class="legal_opinion-li ">
						<a href="legal_opinion">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Legal Opinion</span>
						</a>
					</li>
					<li class="legal_research-li ">
						<a href="legal_research">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Legal Research</span>
						</a>
					</li>
					<li class="legal_audit-li ">
						<a href="legal_audit">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Legal Audit</span>
						</a>
					</li>
					<li class="petition_reply-li ">
						<a href="petition_reply">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Petition Reply</span>
						</a>
					</li>
					<li class="translation_services-li ">
						<a href="translation_services">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Translation Services</span>
						</a>
					</li>
					<li class="q&a-li ">
						<a href="q&a">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Q & A</span>
						</a>
					</li>
					<li class="pre-litigation_adr-li ">
						<a href="pre-litigation_adr">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Pre-Litigation ADR</span>
						</a>
					</li><li class="post-litigation_adr-li ">
						<a href="post-litigation_adr">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Post-Litigation ADR</span>
						</a>
					</li><li class="legal_vetting-li ">
						<a href="legal_vetting">
							<span class="pcoded-micon"><i class="feather icon-aperture rotate-refresh"></i><b>A</b></span>
							<span class="pcoded-mtext">Legal Vetting</span>
						</a>
					</li>
				</ul>
			</li> -->
			<li class=" ">
				<a href="calendar.php">
					<span class="pcoded-micon"><i class="feather icon-calendar"></i></span>
					<span class="pcoded-mtext">Calendar</span>
				</a>
			</li>
			<!-- <li class=" ">
				<a href="create-todo.php?name=All">
					<span class="pcoded-micon"><i class="feather icon-list"></i></span>
					<span class="pcoded-mtext">Create Todo List</span>
				</a>
			</li>
			
			<li class=" ">
				<a href="add-causelist.php">
					<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
					<span class="pcoded-mtext">Cause List</span>
				</a>
			</li>
			<li class=" ">
				<a href="my-law-diary.php">
					<span class="pcoded-micon"><i class="icofont icofont-book-alt"></i></span>
					<span class="pcoded-mtext">Daily Cases</span>
				</a>
			</li>
			<li class="pcoded-hasmenu">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="feather icon-clock"></i></span>
					<span class="pcoded-mtext">Timesheet</span>
				</a>
				<ul class="pcoded-submenu">
					<li class="add-expenses-li">
						<a href="add-timesheets.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">Add Timesheets</span>
						</a>
					</li>
					<li class="my-expenses-li">
						<a href="manage-timesheets.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">Manage Timesheets</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="pcoded-hasmenu">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="icofont icofont-cur-rupee"></i></span>
					<span class="pcoded-mtext">Expenses</span>
				</a>
				<ul class="pcoded-submenu">
					<li class="add-expenses-li">
						<a href="add-expenses.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">Add Expenses</span>
						</a>
					</li>
					<li class="my-expenses-li">
						<a href="my-expenses.php">
							<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
							<span class="pcoded-mtext">My Expenses</span>
						</a>
					</li>
				</ul>
			</li>
			
			<li class=" ">
				<a href="availed_services.php">
					<span class="pcoded-micon"><i class="feather icon-briefcase"></i></span>
					<span class="pcoded-mtext">Availed Services</span>
				</a>
			</li> -->
			<!-- <li class=" ">
				<a href="non-litigation">
					<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
					<span class="pcoded-mtext">Non Litigation Matter</span>
				</a>
			</li>
			<?php if($_SESSION['designation']=='Principal Secretary' || $_SESSION['designation']=='Under Secretary'){?>
			<li class="">
				<a href="registration">
					<span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
					<span class="pcoded-mtext">Registration List</span>
				</a>
			</li>
			<?php } ?> -->
		</ul>


		<!-- <div class="pcoded-navigatio-lavel">Advocates</div>
		<ul class="pcoded-item pcoded-left-item">
			
		</ul>
		<div class="pcoded-navigatio-lavel">Team</div>
		<ul class="pcoded-item pcoded-left-item">
			
		</ul>

		<div class="pcoded-navigatio-lavel">Documents</div>
		<ul class="pcoded-item pcoded-left-item">
			
		</ul>
		<div class="pcoded-navigatio-lavel">Cause list</div>
		<ul class="pcoded-item pcoded-left-item">				
			
			
		</ul>
		<div class="pcoded-navigatio-lavel">Expenses</div>
		<ul class="pcoded-item pcoded-left-item">
			
		</ul>
		<div class="pcoded-navigatio-lavel">Daily Cases</div>
		<ul class="pcoded-item pcoded-left-item">				
			
			
		</ul>
		<div class="pcoded-navigatio-lavel">Reminder List</div>
		<ul class="pcoded-item pcoded-left-item">				
			
			
		</ul>
		<div class="pcoded-navigatio-lavel">Non Litigation Matter</div>
		<ul class="pcoded-item pcoded-left-item">				
			
			
		</ul> -->
		

	</div>
</nav>
<style type="text/css">
	.avail_services_scroll{
		height: 300px;
		overflow: auto;
	}
	@media only screen and (max-width: 800px){
#country-list{
	width: 238px !important;
	z-index: 99 !important;
}
}
</style>