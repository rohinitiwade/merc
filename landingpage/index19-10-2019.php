<?php include("header.php"); include("modal.php");?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

	<style type="text/css">
.vm{background-color:#fffaf0;border-radius:22px;margin-bottom:10px}.vm:hover{box-shadow:0 4px 8px 0 rgba(0,0,0,.2),0 6px 20px 0 rgba(0,0,0,.19)}.news-content{padding:20px;background-color:#fff;margin:0}.slideshow-container{position:relative;background:#f1f1f1f1}.mySlides{display:none;padding:30px;text-align:center;height:100px}.next,.prev{cursor:pointer;position:absolute;top:50%;width:auto;margin-top:-30px;padding:16px;color:#888;font-weight:700;font-size:20px;border-radius:0 3px 3px 0;user-select:none}.next{position:absolute;right:0;border-radius:3px 0 0 3px}.next:hover,.prev:hover{background-color:rgba(0,0,0,.8);color:#fff}.dot-container{text-align:center;position:relative;top:-30px}.dot{cursor:pointer;height:15px;width:15px;margin:0 2px;background-color:#bbb;border-radius:50%;display:inline-block;transition:background-color .6s ease}.active,.dot:hover{background-color:#717171}q{font-style:italic}.author{color:#6495ed}.h2class{font-size:16px}	</style>
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="5000">
      <ol class="carousel-indicators" aria-hidden="true">
        <!-- <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li> -->
	<!-- 	<li data-target="#myCarousel" data-slide-to="3"></li>
		<li data-target="#myCarousel" data-slide-to="4"></li>
		<li data-target="#myCarousel" data-slide-to="5"></li> -->
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active ">
          <div class="container slide">
          
          </div>
        </div>
        <div class="item">
          <div class="container slider2">
            <div class="carousel-caption">
            </div>
          </div>
        </div>
    </div>
	</div>
  </div>

<div aria-role='main'>
<div class="container">
<div class="stats container_cs" style="width: 1199px;">
  <div class="row">
    <div class="row form-group" style="margin-bottom: 10px;">
    	<div class="col-md-4 card text-white bg-secondary mb-3" style="width: 389px;margin-right: 10px;"><center><h3>Case or Matter Management</h3><hr/></center>
      <p class=" slideUp" >Our case management system has made managing information easier than ever before. Create a case in just a few seconds. The case will create its activity stream as you keep adding information, making updates and attaching documents. Everything comes together seamlessly to provide the big picture systematically.</p>
      <p>
      	<a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Matter">View More</a></p></div>

      <div class="col-md-4 card text-white bg-primary" style="width: 389px;margin-right: 10px;"><center><h3>Customized Cause List </h3><hr/></center>
      <p class=" slideUp" >Generate your High Court cause list in PDF format with just one click. Our legal solutions make creating lists fast and efficient. Easily segregate cases listed in different courts. You can also generate cause lists for your team. Empower everyone to track the schedule of cases to manage their work with utmost efficiency.</p>
      <p>
      	<a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Customized">View More</a>
      </p></div>


      <div class="col-md-4 card text-white bg-success"><center><h3>Hearing Dates Management</h3><hr/></center>
      <p class=" slideUp" >Identify in advance conflicts in hearing dates falling on the same day. Eliminate last-minute rush to courts. By getting intimated by our automated reminders well ahead of time, you can plan both your work, teamwork in an informed manner. Our legal management for hearing dates will make sure your schedules never clash.</p>
      <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Hearing">View More</a></p></div>
    </div>
    <div class="row form-group" style="margin-bottom: 10px;">
       <div class="col-md-4 card text-white bg-danger" style="width: 389px;margin-right: 10px;"><center><h3>To-Dos Management</h3><hr/></center>
      <p class=" slideUp" >Create tasks with a single click with our law software. They will automatically get synced to the calendar and send you notifications and reminders. This feature is especially helpful if you or your team members set aside a variety of to-do tasks each day. To-dos are indispensable. </p>
      <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#To-Dos">View More</a></p></div>

       <div class="col-md-4 card text-white bg-warning" style="width: 389px;margin-right: 10px;"><center><h3>Sync with the courts</h3><hr/></center>
      <p class=" slideUp" >Synchronise your case posting dates with Courts. Load publicly available data that is useful to your case. Our case management software will automatically pick up the next hearing date and record. It will also notify you via email and SMS. This efficient data sync saves time and keeps errors at bay.</p>
      <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Sync">View More</a></p></div>

       <div class="col-md-4 card text-white bg-info"><center><h3>Easy Legal Billing / Invoice</h3><hr/></center>
      <p class=" slideUp" >Manage My Lawsuits' invoice management simplifies and adds structure to the client invoicing process. Send professional invoices and payment reminders to clients, and view invoice details for each client. Record how payments have been made and generate reports within seconds.</p>
      <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Easy">View More</a></p></div>
    </div>
    <div class="row form-group">
       <div class="col-md-4 card text-white bg-success" style="width: 389px;margin-right: 10px;"><center><h3>Documents Management</h3><hr/></center>
      <p class=" slideUp" >Upload and download all case-related documents to improve collaboration, storage, security and linking of data across different cases that you can access from anywhere, at any time. And, no one can see your private documents, not even your team members. Our document management system makes it easy.</p>
      <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Documents">View More</a></p></div>

       <div class="col-md-4 card text-white bg-primary" style="width: 389px;margin-right: 10px;"><center><h3>Firm and User Management</h3><hr/></center>
      <p class=" slideUp" >With our law firm case management software, work allocation and collaboration is as efficient as it can get. Add co-workers/associates to the system within minutes. Assign tasks, create to-dos, store case notes, and stay on top of team activities without expending too much time. Review their login history & other statistics.</p>
      <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Firm">View More</a></p></div>

       <div class="col-md-4 card text-white bg-secondary"><center><h3>Clients Management</h3><hr/></center>
      <p class=" slideUp" >Add any number of clients and their identifiable data. Organise client information in a structured way. Easily and accurately match each legal case with a client. Send emails and invoices to clients, add updates, make changes as and when needed to maintain the exactness of client data with Manage My lawsuits solutions.</p>
      <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Clients">View More</a></p></div>
  </div>
 </div>
</div>
<div class="stats container_cs">

  <div class="row">
    <div class="col-md-12 bg-red"><h2 style="color: white;">Start your 15 days Free Trial Today&nbsp;&nbsp;<a href="http://crm.airinfotech.in/law/pricing.php"><img src="images/Get-Started-Now-Button-PNG-Photo.png" width="250"></a></h2>
</div>
  </div>
</div>
</div>


<div class="container " id="newsandhelp" style="margin-top:10px;">
  <div class="row">
  	<div class="card col-sm-8">
      	<div class="news-help-header">
	  	<h2 class="h2class" tabindex="0">What our client's are saying</h2>
	  	</div>
      	<div class="news-content" tabindex="0">
      		<div class="mySlides">
  				<q>I love you the more in that I believe you had liked me for my own sake and for nothing else</q>
  				<p class="author">- John Keats</p>
			</div>
			<div class="mySlides">
  				<q>But man is not made for defeat. A man can be destroyed but not defeated.</q>
  				<p class="author">- Ernest Hemingway</p>
			</div>
			<div class="mySlides">
  				<q>I have not failed. I've just found 10,000 ways that won't work.</q>
  				<p class="author">- Thomas A. Edison</p>
			</div>
			<a class="prev" onclick="plusSlides(-1)" style="left: 15px;">❮</a>
			<a class="next" onclick="plusSlides(1)" style="right: 15px;">❯</a>
		</div>
		<div class="dot-container">
  			<span class="dot" onclick="currentSlide(1)"></span> 
  			<span class="dot" onclick="currentSlide(2)"></span> 
  			<span class="dot" onclick="currentSlide(3)"></span> 
		</div>
	</div>	
    <div class="card col-sm-4">
    	<div class="news-help-header">
    		<h2 class="h2class" tabindex="0">News</h2>
    	</div>
    	<div class="news-content" tabindex="0">
     		<marquee style="height:95px; font-size:1em;align:left;" behavior="scroll" direction="up" scrolldelay="400" onmouseover="this.stop();" onmouseout="this.start();">
      			<ul>
        			<li><i class='fa fa-angle-double-right' aria-hidden='true'></i>&nbsp;&nbsp;<a class='newsline' id='newsid' href=' target='_blank' rel = 'noopener noreferrer' style='text-align:left;'></a>&nbsp;</li>
        			<li><i class='fa fa-angle-double-right' aria-hidden='true'></i>&nbsp;&nbsp;<a class='newsline' id='newsid' href='s' target='_blank' rel = 'noopener noreferrer' style='text-align:left;'></a>&nbsp;</li>
        			<li><i class='fa fa-angle-double-right' aria-hidden='true'></i>&nbsp;&nbsp;<a class='newsline' id='newsid' href='' target='_blank' rel = 'noopener noreferrer' style='text-align:left;'></a>&nbsp;</li>
        			<li><i class='fa fa-angle-double-right' aria-hidden='true'></i>&nbsp;&nbsp;<a class='newsline' id='newsid' href='' target='_blank' rel = 'noopener noreferrer' style='text-align:left;'>eBook: Case Management through CIS 3.0</a>&nbsp;[12 Sep, 2018]</li>
        		</ul>
      		</marquee>     
       </div>
    </div>
   </div>
</div>
  </div> 


<!-- <div class="container" id="services">
<div class="services container_cs">
  <div class="row services">
    <div class="col-md-4">
	<div class="borderclass" style="cursor:default !important;">
	<a href="http://hcservices.ecourts.gov.in/" id="case-status" title="High courts Services " tabindex="0">
		<div class="services-sprite-container"><img class="hcdchc1" alt="High Court Services" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACbAQMAAAA3GeACAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAB1JREFUeNrtwQENAAAAwqD3T20PBxQAAAAAAACXBhoJAAFsyjjLAAAAAElFTkSuQmCC"></div>
    </a><div class="services_content"><a href="http://hcservices.ecourts.gov.in/" id="case-status" title="High courts Services " tabindex="0">
    <p class="services_title" aria-hidden="true">High Court Services</p>
    <p class="services_data">Access to Services of e-Courts: Cause lists, Case Status, Orders/Judgments of High Courts</p>
	</a><div class="row text-center"><a href="http://hcservices.ecourts.gov.in/" id="case-status" title="High courts Services " tabindex="0">
	</a><a href="http://hcservices.ecourts.gov.in/" target="" rel = "noopener noreferrer" title="High courts Services" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-university linkbtnicon" aria-hidden="true"></i> Click here for High Court Services</a>
	</div>
</div>
	
	</div>
     </div> -->

<!--    <div class="col-md-4">
   <div class="borderclass">
<a href="http://njdg.ecourts.gov.in/hcnjdg_public/" target="_blank" rel = "noopener noreferrer" title="High Court NJDG External website that opens a new window" tabindex="0">
<div class="services-sprite-container"><img class="hcdchc2" alt="High Court NJDG" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACbAQMAAAA3GeACAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAB1JREFUeNrtwQENAAAAwqD3T20PBxQAAAAAAACXBhoJAAFsyjjLAAAAAElFTkSuQmCC"></div>
    </a><div class="services_content"><a href="http://njdg.ecourts.gov.in/hcnjdg_public/" target="_blank" rel = "noopener noreferrer" title="High Court NJDG External website that opens a new window" tabindex="0">
    <p class="services_title">High Court NJDG</p>
    <p class="services_data">NJDG works as a monitoring tool to identify, manage and reduce pendency of cases.</p>
	</a><div class="row text-center"><a href="http://njdg.ecourts.gov.in/hcnjdg_public/" target="_blank" rel = "noopener noreferrer" title="High Court NJDG External website that opens a new window" tabindex="0">
	</a><a href="http://njdg.ecourts.gov.in/hcnjdg_public/" target="_blank" rel = "noopener noreferrer" title="High Court NJDG External website that opens a new window" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-link linkbtnicon" aria-hidden="true"></i> Click here for High Court NJDG</a>
	</div>
</div>
	
	</div>
     </div>

	  <div class="col-md-4">
	<div class="borderclass">
    <a href="static/highcourts.html" target="" rel = "noopener noreferrer" title="High Courts of India" tabindex="0">
	<div class="services-sprite-container"><img class="hcdchc3" alt="High Courts of India" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACbAQMAAAA3GeACAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAB1JREFUeNrtwQENAAAAwqD3T20PBxQAAAAAAACXBhoJAAFsyjjLAAAAAElFTkSuQmCC"></div>
    </a><div class="services_content"><a href="static/highcourts.html" target="" rel = "noopener noreferrer" title="High Courts of India" tabindex="0">
    <p class="services_title">High Courts of India</p>
    <p class="services_data">Dissemination of information related to high courts to stakeholders of e-Courts</p>
	</a><div class="row text-center"><a href="static/highcourts.html" target="" rel = "noopener noreferrer" title="High Courts of India" tabindex="0">
	</a><a href="static/highcourts.html" title="High Courts of India" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-university linkbtnicon" aria-hidden="true"></i> Click here for High Courts of India</a>
	</div>
</div>
	
	</div>
     </div>
   </div>
</div>

<div class="services container_cs">
  <div class="row services">
    <div class="col-md-4">
	<div class="borderclass" style="cursor:default !important;">
	<a href="https://services.ecourts.gov.in/" id="case-status" title="District courts Services " tabindex="0">
		<div class="services-sprite-container"><img class="hcdcdc1" alt="District Court Services" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACbAQMAAAA3GeACAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAB1JREFUeNrtwQENAAAAwqD3T20PBxQAAAAAAACXBhoJAAFsyjjLAAAAAElFTkSuQmCC"></div>
    </a><div class="services_content"><a href="https://services.ecourts.gov.in/" id="case-status" title="District courts Services " tabindex="0">
    <p class="services_title" aria-hidden="true">District Court Services</p>
    <p class="services_data">Access to Services of e-Courts: Cause lists, Case Status, Orders/Judgments &amp; NJDG</p>
	</a><div class="row text-center"><a href="https://services.ecourts.gov.in/" id="case-status" title="District courts Services " tabindex="0">
	</a><a href="https://services.ecourts.gov.in/" target="" rel = "noopener noreferrer" title="District Court Services" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-university linkbtnicon" aria-hidden="true"></i> Click here for District Court Servies</a>
	</div>
</div>
	
	</div>
     </div>

   <div class="col-md-4">
   <div class="borderclass">
<a href="http://njdg.ecourts.gov.in/njdg_public/" target="_blank" rel = "noopener noreferrer" title="District Court NJDG website External website that opens a new window" tabindex="0">
<div class="services-sprite-container"><img class="hcdcdc2" alt="District Court NJDG" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACbAQMAAAA3GeACAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAB1JREFUeNrtwQENAAAAwqD3T20PBxQAAAAAAACXBhoJAAFsyjjLAAAAAElFTkSuQmCC"></div>
    </a><div class="services_content"><a href="http://njdg.ecourts.gov.in/njdg_public/" target="_blank" rel = "noopener noreferrer" title="District Court website External website that opens a new window" tabindex="0">
    <p class="services_title">District Court NJDG</p>
    <p class="services_data">NJDG works as a monitoring tool to identify, manage and reduce pendency of cases.
</p>
	</a><div class="row text-center"><a href="http://njdg.ecourts.gov.in/njdg_public/" target="_blank" rel = "noopener noreferrer" title="District Court website External website that opens a new window" tabindex="0">
	</a><a href="http://njdg.ecourts.gov.in/njdg_public/" target="_blank" rel = "noopener noreferrer" title="District Court website External website that opens a new window" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-link linkbtnicon" aria-hidden="true"></i> Click here for District Court NJDG</a>
	</div>
</div>
	
	</div>
     </div>

	  <div class="col-md-4">
	<div class="borderclass">
    <a href="https://districts.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="District Court website External website that opens a new window" tabindex="0">
	<div class="services-sprite-container"><img class="hcdcdc3" alt="District Courts of India" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACbAQMAAAA3GeACAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAB1JREFUeNrtwQENAAAAwqD3T20PBxQAAAAAAACXBhoJAAFsyjjLAAAAAElFTkSuQmCC"></div>
    </a><div class="services_content"><a href="https://districts.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="District Court website External website that opens a new window" tabindex="0">
    <p class="services_title">District Courts of India</p>
    <p class="services_data">Access to Services of e-Courts: Cause lists, Case Status, Orders/Judgments &amp; NJDG</p>
	</a><div class="row text-center"><a href="https://districts.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="District Court website External website that opens a new window" tabindex="0">
	</a><a href="https://districts.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="District Courts of India" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-university linkbtnicon" aria-hidden="true"></i> Click here for District Courts of India</a>
	</div>
</div>
	
	</div>
     </div>
   </div>
</div>



<!-*********************** -->
<!-- <div class="services container_cs">
  <div class="row services">
    <div class="col-md-4">
	<div class="borderclass" style="cursor:default !important;">
	<a href="https://efiling.ecourts.gov.in/"  target="_blank" rel = "noopener noreferrer"  title="e-Filing External website that opens a new window" tabindex="0">
		<div class="services-sprite-container"><img class="FPVCefiling-img" alt="e-Filing" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACcAQMAAAAqHNC6AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABxJREFUeNrtwQEBAAAAgiD/r25IQAEAAAAAAPBuGjQAARzOiPIAAAAASUVORK5CYII="></div>
    </a><div class="services_content"><a href="https://efiling.ecourts.gov.in/"  target="_blank" rel = "noopener noreferrer"  title="e-Filing External website that opens a new window" tabindex="0">
    <p class="services_title" aria-hidden="true">e-Filing</p>
    <p class="services_data">e-Filing application enables electronic filing of legal papers.</p>
	</a><div class="row text-center"><a href="https://efiling.ecourts.gov.in/"   target="_blank" rel = "noopener noreferrer" title="e-Filing External website that opens a new window" tabindex="0">
	</a><a href="https://efiling.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="e-Filing External website that opens a new window" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-pencil-square-o linkbtnicon" aria-hidden="true"></i> Click here for e-Filing</a>
	</div>
</div>
	
	</div>
     </div>

   <div class="col-md-4">
   <div class="borderclass">
<a href="https://pay.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="ePay website External website that opens a new window" tabindex="0">
<div class="services-sprite-container"><img class="FPVCepay-img" alt="ePay" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACcAQMAAAAqHNC6AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABxJREFUeNrtwQEBAAAAgiD/r25IQAEAAAAAAPBuGjQAARzOiPIAAAAASUVORK5CYII=">
</div>
    </a><div class="services_content"><a href="https://pay.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="ePay website External website that opens a new window" tabindex="0">
    <p class="services_title">ePay</p>
    <p class="services_data">ePay is a way of paying for court through an electronic medium, without the use of cheque or cash.</p>
	</a><div class="row text-center"><a href="https://pay.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="ePay website External website that opens a new window" tabindex="0">
	</a><a href="https://pay.ecourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="ePay website External website that opens a new window" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-inr linkbtnicon" aria-hidden="true"></i> Click here for ePay</a>
	</div>
</div>
	
	</div>
     </div>

	  <div class="col-md-4">
	<div class="borderclass">
    <a href="http://vcourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="Virtual Courts External website that opens a new window" tabindex="0">
	<div class="services-sprite-container"><img class="FPVCvc-img" alt="Virtual Courts" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAACcAQMAAAAqHNC6AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABxJREFUeNrtwQEBAAAAgiD/r25IQAEAAAAAAPBuGjQAARzOiPIAAAAASUVORK5CYII="></div>
    </a><div class="services_content"><a href="http://vcourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="Virtual Courts website External website that opens a new window" tabindex="0">
    <p class="services_title">Virtual Courts</p>
    <p class="services_data">Eliminating presence of litigant or lawyer in the court and adjudication of the case online</p>
	</a><div class="row text-center"><a href="http://vcourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="Virtual Courts website External website that opens a new window" tabindex="0">
	</a><a href="http://vcourts.gov.in/" target="_blank" rel = "noopener noreferrer" title="Virtual Courts External website that opens a new window" tabindex="0" class="btn btn-default linkbtn"><i class="fa fa-university linkbtnicon" aria-hidden="true"></i> Click here for Virtual Courts</a>
	</div>
</div>
	 -->
	</div>
     </div> 
   </div>
</div>
</div>
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 2000);
}
</script>

<!--============== -->

<?php include("footer.php");?>

