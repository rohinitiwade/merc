<!DOCTYPE html>
<?php session_start();?>
<?php include("includes/dbconnect.php");?>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8">
	<title>Maharashtra LMS
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Landing page template for creative dashboard">
	<meta name="keywords" content="Landing page template">
	<link rel="icon" href="assets/logos/favicon.ico" type="image/png" sizes="16x16">

	<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="assets/css/login.css">
	<link rel="stylesheet" href="assets/css/responsive.css">
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="assets/css/ionicons.min.css">
	<style type="text/css">
		.flex-features {
			background: #2d4866;
			padding-top: 70px;
			padding-bottom: 70px;
		}
		.justify-content-md-center .col-sm-5{
			padding: 1%;
		}
		.ribbon-front {
			background-color: #ffa300;
			height: 43px;
			width: 50%;
			float: left;
			position: relative;
			left: -43px;
			z-index: 2;
			top: -34px
		}
		.ribbon-front h2 {
			color: #fff;
			font-weight: bold;
			text-align: center;
		}
		.ribbon-edge-topleft2 {
			top: -34px;
			border-width: 22px 20px 21px 37px;
			transform: rotate(-180deg);
			border-radius: 1px;
			left: 160px;
			border-color: transparent #ffa300 transparent transparent;
			position: absolute;
			z-index: 1;
			border-style: solid;
			height: 0;
			width: 0;
		}
		.ribbon-edge-bottomleft {
			left: -44px;
			border-color: transparent #e17e02 transparent transparent;
			top: 8px;
			position: absolute;
			z-index: 1;
			border-style: solid;
			height: 0;
			width: 0;
			border-width: 0 10px 15px 0;
		}
		.lock-icon{
			font-size: 3em;
		}
	</style>
</head>
<body>

	<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
		<div class="container loginheader">
			<nav class="navbar navbar-expand-lg navbar-light navbar-default" role="navigation">
				<div class="container">
					<a class="navbar-brand page-scroll" href="#main">
						<img src="assets/logos/new_logo_small.png" alt="MHLMS Logo">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon">
						</span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav mr-auto">
						</ul>
						<ul class="navbar-nav my-2 my-lg-0">
							<li class="nav-item">
								<a class="nav-link page-scroll" href="index.php#main">Home
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link page-scroll" href="index.php#features">About Us
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link page-scroll" href="index.php#services">Our Services
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Testimonial 
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="index.php#contact-us">Contact Us
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link page-scroll" href="registration">Registration
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="login">Login
								</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
		<div class="main" id="main">
			<?php
			 if(isset($_POST['recover-submit'])){
     $email=$_POST['email'];
    $sql = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `email`='".$email."' OR `mobile`='".$email."'");
    $row = mysqli_fetch_array($sql);
    $count=mysqli_num_rows($sql);
    if($count==1){ 
      $_SESSION['data'] = $row['mobile'];
      $_SESSION['data1'] = $row['email'];
      ?>
               <script type="text/javascript">
               window.location.href='passworddata';
              </script>
              <?php 
                //header("location:admin/index.php");
            }else{
                 $msg = "<h3>No Search Result</h3>";
                 $msg .= "Please enter your email address or phone number to search for your account.";
            }
          }?>
			<div class="hero-section app-hero">
				<div class="hero-content flex-features text-center">
					<div class="container">
						<div class="row justify-content-md-center">
							<div class="col-sm-5">
								<form accept-charset="UTF-8" role="form" autocomplete="OFF" method="POST">

                    <!-- <div class="ribbon-front">
                      <h2>Forgot Password?
                      </h2>
                    </div>
                    <div class="ribbon-edge-topleft2"> 
                    </div>
                    <div class="ribbon-edge-bottomleft">
                    </div> -->
                    <h3><i class="ion-android-lock lock-icon"></i></h3>
                    <h4>Forgot Password?
                    </h4>
                   
                
                <input type="text" class="form-control" placeholder="Email address / Mobile Number"  name="email" value="<?php echo $_POST['email'];?>" >
                <!-- <input type="password" name="password" placeholder="Password" class="form-control"> -->

                <button type="submit" class="btn btn-success btn-sm btn-block" name="recover-submit">Reset Password
                </button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<div class="footer">
	<div class="container">
		<div class="col-md-12 text-center">
			<img src="assets/logos/new_logo_small.png" alt="MHLMS Logo" style="width: 9%;">
			<ul class="footer-menu">
				<li>
					<a href="http://demo.com/">Site
					</a>
				</li>
				<li>
					<a href="#">Support
					</a>
				</li>
				<li>
					<a href="#">Terms
					</a>
				</li>
				<li>
					<a href="#">Privacy
					</a>
				</li>
			</ul>
			<div class="footer-text">
			</div>
		</div>
	</div>
</div>
<script src="assets/js/jquery-2.1.1.js">
</script>
<script src="assets/js/bootstrap.min.js">
</script>
