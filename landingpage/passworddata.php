<!DOCTYPE html>
<?php session_start(); include("includes/sms.php"); include('includes/common_url.php'); include("includes/dbconnect.php");
$_SESSION['data']; 
 $_SESSION['data1'];?>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8">
  <title>Case Tracker
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Landing page template for creative dashboard">
  <meta name="keywords" content="Landing page template">
  <link rel="icon" href="assets/logos/favicon.ico" type="image/png" sizes="16x16">
  <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
  <link rel="stylesheet" href="assets/css/login.css">
  <link rel="stylesheet" href="assets/css/responsive.css">

  <style type="text/css">
    .forgot-password-form{
      width: 40%;
      display: inline-block;
      padding: 3%;
      background: white;
      box-shadow: 0 0 10px 0 rgba(0,0,0,.5);
    }
    .flex-features {
    padding-top: 70px;
    padding-bottom: 70px;
}
  </style>
</head>
<body>

  <div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
    <div class="container loginheader">
      <nav class="navbar navbar-expand-lg navbar-light navbar-default" role="navigation">
        <div class="container">
          <a class="navbar-brand page-scroll" href="#main">
            <img src="assets/logos/new_logo_small.png" alt="MHLMS Logo">
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">
            </span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            </ul>
            <ul class="navbar-nav my-2 my-lg-0">
              <li class="nav-item">
                <a class="nav-link page-scroll" href="index.php#main">Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="index.php#features">About Us
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="index.php#services">Our Services
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Testimonial 
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="index.php#contact-us">Contact Us
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link page-scroll" href="registration">Registration
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="login">Login
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
      <?php  $sql = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `email`='".$_SESSION['data1']."' OR `mobile`='".$_SESSION['data']."'");
    $row = mysqli_fetch_array($sql);
   if($_POST['recover-submit']){
    extract($_POST);
    // echo $verify;
    // echo "<br/>";
    // echo $row['mobile'];
     $characters = '123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
            for ($i = 0; $i < 6; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
        $pin=$randomString;
    if($verify==$row['mobile']){
      

      $mobile = $_SESSION['data'];
      if($mobile!=""){
        $mobile='9970326928';
      // $mes = "Your MHLMS account Recovery code ".$pin." ";
      $mes = "Greetings! Please enter ". $pin ." as your verification code to reset password.";
        $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($smsusername)."&password=".urlencode($smspassword)."&mobile=".urlencode($mobile)."&sender=".urlencode($sender)."&message=".urlencode($mes)."&type=".urlencode('3'); 
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
            $arr = json_decode($curl_scraped_page, true);
            $status= $arr["status"];  // Output: 65
            // echo $arr["mobilenumbers"];  // Output: 80
            $rem_credits= $arr["remainingcredits"];   // Output: 78
            $msgcount= $arr["msgcount"];  // Output: 90
            $selectedRoute=$arr["selectedRoute"];
            $refid=$arr["refid"];
            $insert = mysqli_query($connection, "INSERT INTO `forget_password` SET `user_id`='".$row['reg_id']."',`status`='".$status."',`pin`='".$pin."',`via`='Sms',`date_time`='".date("Y-m-d")."'");
            $update = mysqli_query($connection, "UPDATE `law_registration` SET `forget_pin`='".$pin."' WHERE `reg_id`='".$row['reg_id']."'");

            header("location:securitycode?flag=Mobile");

            }
          }elseif($verify==$row['email']){
            $from1 = 'noreply@mlms.com'; 
              $fromName ='Case Tracker'; 
              $to = "rohinit.air@gmail.com";
$subject = "Welcome to Case Tracker, Reset Password";

$message = "Greetings! Please enter ". $pin ." as your verification code to reset password.
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
// $headers .= 'From: <webmaster@example.com>' . "\r\n";
$headers .= 'From: '.$fromName.'<'.$from1.'>' . "\r\n"; 
 $headers .= 'Cc: myboss@example.com' . "\r\n";

if(mail($to, $subject, $message, $headers)){ 
              
             $insert = mysqli_query($connection, "INSERT INTO `forget_password` SET `user_id`='".$row['reg_id']."',`status`='".$status."',`pin`='".$pin."',`via`='Mail',`date_time`='".date("Y-m-d")."'");
            $update = mysqli_query($connection, "UPDATE `law_registration` SET `forget_pin`='".$pin."' WHERE `reg_id`='".$row['reg_id']."'");

            header("location:securitycode?flag=Mail");
             }else{ 
                 echo 'Email sending failed.'; 
              }

    }
}
   ?>
    <div class="main" id="main">
      <div class="hero-section app-hero">
        <div class="hero-content flex-features text-center">
          <div class="container">
            <div class="forgot-password-form">
              <div class="text-center">
                <h4 class="text-center" style="color: green;">Reset Your Password?</h4>
                <p>You can reset your password here.</p>
                <div class="panel-body">
                  <form id="register-form" role="form" autocomplete="off" class="form" method="post">
                    <div class="form-group">
                      <div class="input-group">
                        <input type="radio" name="verify" checked value="<?php echo $row['email'];?>">&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-envelope color-blue"></i>&nbsp;&nbsp;&nbsp;<?php echo substr($row['email'],0,3);?>****@*****

                     </div>
                     <div class="input-group">
                       <input type="radio" name="verify" value="9561190398">&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;********<?php echo substr($row['mobile'],8,2);?>                       </div>
                     </div>
                     <div class="col-md-12">
                      <hr>
                    </div>
                    <div class="col-md-2" style="float: right;">
                      <a href="login" class="btn btn-danger btn-sm">Back</a>
                    </div>
                    <div class="col-md-2" style="float: right;">
                      <input name="recover-submit" class="btn btn-primary btn-sm" value="Submt" type="submit">
                    </div>
                    <input type="hidden" class="hide" name="token" id="token" value=""> 
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
   <div class="footer">
          <div class="container">
            <div class="col-md-12 text-center">
              <img src="assets/logos/new_logo_small.png" alt="MHLMS Logo" style="width: 9%;">
              <ul class="footer-menu">
                <li>
                  <a href="http://demo.com/">Site
                  </a>
                </li>
                <li>
                  <a href="#">Support
                  </a>
                </li>
                <li>
                  <a href="#">Terms
                  </a>
                </li>
                <li>
                  <a href="#">Privacy
                  </a>
                </li>
              </ul>
              <div class="footer-text">
              </div>
            </div>
          </div>
        </div>
</body>
</html>