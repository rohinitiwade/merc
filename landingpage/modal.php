<div id="Matter" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Case or Matter Management</b></h4>
      </div>
      <div class="modal-body">
        <p>Our case management system has made managing information easier than ever before. Create a case in just a few seconds. The case will create its activity stream as you keep adding information, making updates and attaching documents. Everything comes together seamlessly to provide the big picture systematically.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="Customized" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Customized Cause List</b></h4>
      </div>
      <div class="modal-body">
        <p>Generate your High Court cause list in PDF format with just one click. Our legal solutions make creating lists fast and efficient. Easily segregate cases listed in different courts. You can also generate cause lists for your team. Empower everyone to track the schedule of cases to manage their work with utmost efficiency.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<div id="Hearing" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Customized Cause List</b></h4>
      </div>
      <div class="modal-body">
        <p>Identify in advance conflicts in hearing dates falling on the same day. Eliminate last-minute rush to courts. By getting intimated by our automated reminders well ahead of time, you can plan both your work, teamwork in an informed manner. Our legal management for hearing dates will make sure your schedules never clash.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="To-Dos" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">To-Dos Management</b></h4>
      </div>
      <div class="modal-body">
        <p>Create tasks with a single click with our law software. They will automatically get synced to the calendar and send you notifications and reminders. This feature is especially helpful if you or your team members set aside a variety of to-do tasks each day. To-dos are indispensable. </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="Sync" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Sync with the courts</b></h4>
      </div>
      <div class="modal-body">
        <p>Synchronise your case posting dates with Courts. Load publicly available data that is useful to your case. Our case management software will automatically pick up the next hearing date and record. It will also notify you via email and SMS. This efficient data sync saves time and keeps errors at bay.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="Easy" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Easy Legal Billing / Invoice</b></h4>
      </div>
      <div class="modal-body">
        <p>Manage My Lawsuits' invoice management simplifies and adds structure to the client invoicing process. Send professional invoices and payment reminders to clients, and view invoice details for each client. Record how payments have been made and generate reports within seconds.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="Documents" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Documents Management</b></h4>
      </div>
      <div class="modal-body">
        <p>Upload and download all case-related documents to improve collaboration, storage, security and linking of data across different cases that you can access from anywhere, at any time. And, no one can see your private documents, not even your team members. Our document management system makes it easy.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="Firm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Firm and User Management</b></h4>
      </div>
      <div class="modal-body">
        <p>With our law firm case management software, work allocation and collaboration is as efficient as it can get. Add co-workers/associates to the system within minutes. Assign tasks, create to-dos, store case notes, and stay on top of team activities without expending too much time. Review their login history & other statistics.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="Clients" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: red;">Clients Management</b></h4>
      </div>
      <div class="modal-body">
        <p>Add any number of clients and their identifiable data. Organise client information in a structured way. Easily and accurately match each legal case with a client. Send emails and invoices to clients, add updates, make changes as and when needed to maintain the exactness of client data with Manage My lawsuits solutions.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>