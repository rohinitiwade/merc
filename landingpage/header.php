<?php session_start();  ?>
<!DOCTYPE html>
<html lang="en">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="utf-8">
      <title>Case Tracker</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="Landing page template for creative dashboard">
      <meta name="keywords" content="Landing page template">
      <link rel="icon" href="assets/logos/favicon.ico" type="image/png" sizes="16x16">
      <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
      <link rel="stylesheet" href="assets/css/animate.css">
      <link rel="stylesheet" href="assets/css/owl.carousel.css">
      <link rel="stylesheet" href="assets/css/owl.theme.css">
      <link rel="stylesheet" href="assets/css/magnific-popup.css">
      <link rel="stylesheet" href="assets/css/animsition.min.css">
      <link rel="stylesheet" href="assets/css/ionicons.min.css">
      <link rel="stylesheet" href="assets/css/responsive.css">
      <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
   </head>
   <body>
      <div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
      <div class="container">
         <nav class="navbar navbar-expand-lg navbar-light navbar-default navbar-fixed-top" role="navigation">
            <div class="container loginheader">
               <a class="navbar-brand page-scroll" href="#main">
               <img src="assets/logos/AIR VI Logo JPG.jpg" alt="MHLMS Logo" style="width: 50%;">
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto"></ul>
                  <ul class="navbar-nav my-2 my-lg-0">
                     <li class="nav-item"><a class="nav-link page-scroll" href="#main">Home</a></li>
                     <li class="nav-item"><a class="nav-link page-scroll" href="#features">About Us</a></li>
                     <li class="nav-item"><a class="nav-link page-scroll" href="#services">Our Services</a></li>
                     <li class="nav-item"><a class="nav-link" href="#">Testimonial</a></li>
                     <li class="nav-item"><a class="nav-link" href="#contact-us.php">Contact Us</a></li>
                     <li class="nav-item"><a class="nav-link page-scroll" href="registration.php">Registration</a></li>
                     <!-- <li class="nav-item"><a class="nav-link" href="login">Login</a></li> -->
                     <li class="nav-item"><a class="nav-link" href="loginform.php">Login</a></li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>

   <style type="text/css">
      @media only screen and (max-width: 800px){
       .navbar-default .navbar-collapse {
    text-align: center;
    border-color: transparent;
    background-color: #fff;
}

}
   </style>

