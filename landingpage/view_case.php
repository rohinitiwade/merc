<?php include("header.php");?>
<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<link rel="stylesheet" href="js/dist/dropzone.css">
<link rel="stylesheet" href="js/dist/basic.css">
<link rel="stylesheet" href="css/view_case.css">
<?php include("menu.php");?>
<!-- partial -->
<?php $case = mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
$selcase = mysqli_fetch_array($case);?>
<style type="text/css">

  </style>
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="card">
        <div class="card-body">
          <!-- <h3 class="card-header form-group" style="padding-top: 0px"><?php echo $selcase['case_title'];?></h3> -->
          <h3 class="card-header form-group title_h3" style="padding-top: 0px" id="district_court_title"></h3>
          <div class="col-12 form-group" style="text-align: right;">
            <!-- <a href="add-cases.php" class="btn btn-success add-more-button btn-sm"><i class="fas fa-plus"></i> Add Cases</a> -->
            <!-- <a href="myqueries.php?caseid=<?php echo $_GET['caseid'];?>" class="btn btn-warning btn-sm queries" >Response From CLR </a> -->
            <a href="add-cases.php" class="btn-sm btn-outline-success"><i class="fa fa-plus"></i> Add Cases</a>
            <a href="cases-report.php" class="btn btn-danger btn-sm">Manage Cases</a>
            <!-- <button type="button"  data-toggle="modal" class="btn btn-info btn-sm" data-target="#casedetails" onclick="casedetails(<?php echo $_GET['caseid']?>)"> Cases Details </button> -->
          </div>
          <input type="hidden" name="caseid" id="caseid" value="<?php echo base64_decode($_GET['caseid']);?>">
          <input type="hidden" name="courtid" id="court-id" value="<?php echo $selcase['court_id'];?>">
          <input type="hidden" name="id" id="id" value="<?php echo $_GET['id'];?>">
          <div class="row">
            <div class="col-3 p-0">
              <div class="sidebar-case-section utype-lawyers">
               <div id="court_data"></div>
               <div id="casetype"></div>
               <div id="stage"></div>
               <div id="petitioner"></div>
               <div id="petitionerAdv"></div>
               <div id="respondent"></div>
               <div id="respondent_adv"></div> 
               <div id="history_date"></div>    
             </div>
           </div>
           <div class="col-9 case_title">
            <div class="row case_title_div">
              <div class="col-8"><h3 class="fetch_case_title" id="sub_case_title"></h3></div>
              <div class="col-3 set_priority">
               <select class="form-control" id="set_priority_select">
                 <option value="">Set priority</option>
                 <option value="Super_Critical">Super Critical</option>
                 <option value="Critical">Critical</option>
                 <option value="Important">Important</option>
                 <option value="Routine">Routine</option>
                 <option value="Others">Others</option>
                 <option value="Normal">Normal</option>
               </select>
             </div>
             <div class="col-1">
               <label class="toggle-switch toggle-switch-danger">
                <input type="checkbox" checked="">
                <span class="toggle-slider round"></span>
              </label>
            </div>
          </div>
          <div class="table-responsive">
            <h3 class="card-title" style="font-size: 20px;"></h3>
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home-1" role="tab" aria-controls="home-1" aria-selected="true">Activity/History</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="tm-tab" data-toggle="tab" href="#tm" role="tab" aria-controls="profile-1" aria-selected="false">Team Members(1)</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="documenttab" data-toggle="tab" href="#contact-1" role="tab" aria-controls="contact-1" aria-selected="false">Documents(0)</a>
              </li>
              <li class="nav-item">
                <a class="nav-link " id="note-tab" data-toggle="tab" href="#note-1" role="tab" aria-controls="contact-1" aria-selected="false">Notes(0)</a>
              </li>
              <li class="nav-item">
                <a class="nav-link " id="contact-tab" data-toggle="tab" href="#contact-1" role="tab" aria-controls="contact-1" aria-selected="false">To-Dos(0)</a>
              </li>
              <li class="nav-item">
                <a class="nav-link " id="rc-tab" data-toggle="tab" href="#rc-1" role="tab" aria-controls="contact-1" aria-selected="false">Order And Judgment(0)</a>
              </li>
              <li class="nav-item">
                <a class="nav-link " id="rc-tab" data-toggle="tab" href="#rc-1" role="tab" aria-controls="contact-1" aria-selected="false">Connected  Cases(0)</a>
              </li>


                <li class="nav-item">
                  <a class="nav-link " id="seeklegaltab" data-toggle="tab" href="#time-1" role="tab" aria-controls="contact-1" aria-selected="false">Seek Legal Help</a>
                </li>
              <!--   <li class="nav-item">
                  <a class="nav-link " id="exp-tab" data-toggle="tab" href="#exp-1" role="tab" aria-controls="contact-1" aria-selected="false">Expenses</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link " id="fee-tab" data-toggle="tab" href="#fee-1" role="tab" aria-controls="contact-1" aria-selected="false">Fee Recived</a>
                </li> -->
              <!--   <li class="nav-item">
                  <a class="nav-link " id="clr-tab" data-toggle="tab" style="background: #a8bbd1;" href="#clr-1" role="tab" aria-controls="contact-1" aria-selected="false">Response From Researcher</a>
                </li> -->
                <span class="count_of_clr" id="clr_count"></span>
              </ul>
              <div class="tab-content">
                <div class="tab-pane fade active show" id="home-1" role="tabpanel" aria-labelledby="home-tab">
                  <div class="media">
                    <div class="media-body">
                     <div class="form-group row" style="float: right;">
                      <select class="form-control" style="width: 104px;">
                       <option disabled selected hidden>Export</option>
                       <option>as a PDF</option>
                     </select>
                     <button style="border: 0px;padding: 9px 12px;background-color: #dddddd;margin-left: 10px;">Filter</button>
                   </div><br><br><br>
                   <div id="show_view_case_default"></div>
                   <div id="show_view_case"></div>
                   <div id="show_hearing_data_list"></div>
                   <div id="activity-log-div"></div>
                   <textarea name="seeklegal" id="query" class="ckeditor" ></textarea>
                   <?php
                   include_once 'ckeditor/ckeditor.php';
                   $ckeditor = new CKEditor('content');
                   $ckeditor->basePath = 'ckeditor/';                               
                   $ckeditor->replace(""); ?>
                   <span>
                    <b>Would you like to inform advocate(s)?</b>
                    <input type="radio" name="inform" style="margin-left: 20px;position: relative;top: 1px;margin-top: 20px;">Yes
                    <input type="radio" name="inform" style="margin-left: 20px;position: relative;top: 1px;">No
                  </span><br>
                  <span>
                    <b>Would you like to record next hearing date or resolution?</b>
                    <input type="radio" name="inform" style="margin-left: 20px;position: relative;top: 1px;margin-top: 20px;">Yes
                    <input type="radio" name="inform" style="margin-left: 20px;position: relative;top: 1px;">No
                  </span><br>
                  <button class="btn btn-primary btn-sm" type="button" style="float: right;" onclick="submitCaseComment()"> Submit</button>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="tm" role="tabpanel" aria-labelledby="tm-tab">
              <div class="media">
                <div class="media-body">
                  <table class="table" border="0.25">
                    <thead>
                      <tr>
                        <th style="background-color: #610b9e; color: white;">First Name</th>
                        <th style="background-color: #610b9e; color: white;">Last Name</th>
                        <th style="background-color: #610b9e; color: white;">Email</th>
                        <th style="background-color: #610b9e; color: white;">Mobile no.</th>
                        <th style="background-color: #610b9e; color: white;">Actions</th>
                      </tr>
                      <?php $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `reg_id`='".$selcase['user_id']."'");
                      while($selmember = mysqli_fetch_array($team)){?>
                        <tr>
                          <td><?php echo $selmember['name'];?></td>
                          <td><?php echo $selmember['last_name'];?></td>
                          <td><?php echo $selmember['email'];?></td>
                          <td><?php echo $selmember['mobile'];?></td>
                          <td></td>
                        </tr>
                      <?php } ?>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
       <div class="tab-pane fade " id="contact-1" role="tabpanel" aria-labelledby="contact-tab">
              <div class="media">
                <div class="media-body">
                  <div class="tab-pane" id="documenttab" role="tabpanel" aria-labelledby="documenttab">
                    <div class="media">
                      <div class="media-body">
                       <div class="container">
                        <div class="row" style="background: none;">
                          <div class="col-md-12">
                           <form action="/file-upload"
                           class="dropzone needsclick dz-clickable"
                           id="my-awesome-dropzone"></form>

                         </div>

                       </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </div>
            <div class="tab-pane fade" id="note-1" role="tabpanel" aria-labelledby="note-tab">
              <div class="media">
                <div class="media-body">
                  <textarea rows="10" cols="158"></textarea>
                  <b>Mark this note as private <input type="radio" name="np">Yes <input type="radio" name="np">No</b>
                  <a href="manage-document.php" class="btn btn-danger" style="float: right; color: white" onclick="return confirm('Are you sure you Want to Cancel?');"><b>Cancel</b></a>
                  <button class="btn btn-primary" style="color: white;margin-right: 10px; float: right;"><b>Upload</b></button>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="rc-1" role="tabpanel" aria-labelledby="rc-tab">
       <?php include('connected_cases_add.php');?>
            </div>
            <div class="tab-pane fade" id="time-1" role="tabpanel" aria-labelledby="seeklegaltab">
              <div class="media">
                <div class="media-body">
             <form id="formSubmit" class="smart-form client-forms" method="POST" enctype="multipart/form-data" autocomplete="off" data-select2-id="formSubmit">
     <?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
     $row = mysqli_fetch_array($fetch);
       $reg_cases = mysqli_query($connection,"SELECT * FROM `reg_cases` WHERE `case_id`='".base64_decode($_GET['caseid'])."'");
     $regs = mysqli_fetch_array($reg_cases);
     $regs['name_of_matter'];
     $fullname = $row['name']." ".$row['last_name']; ?>
     <input type="hidden" id="fname" value="MHMLS-<?php echo $regs['case_id'];?>">
     <input type="hidden" id="lname" value="<?php echo $fullname;?>">
     <input type="hidden" id="email" value="MHMLS-<?php echo $regs['case_id'];?>">
     <input type="hidden" id="mobile" value="<?php echo $row['mobile'];?>">
     <div class="clrresponse_row" data-select2-id="9">
                    <!-- <div class="col-sm-6">
                       
                      <div class="form-group" data-select2-id="31">
                       
                        <label for="">Subject</label>
                        <div class="input-group mb-3" data-select2-id="30">

                          <select id="subjectlist" class="form-control sideclass" name="Cases[high_court_id]">
                            <option value="">Please select / कृपया निवडा</option>
                          </select>       
                        </div>
                      </div>
                    </div> -->

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="">Name of Department</label>
                        <div class="input-group mb-3 " data-select2-id="6">

                         <!--  <select id="actlist" class="form-control sideclass" name="Cases[high_court_id]">
                            <option value="">Please select / कृपया निवडा</option>
                          </select> -->
                          <input type="text" class="form-control same" id="actlist" name="state" value="<?php echo $regs['name_of_matter'];?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="">Question<span class="text-danger"> *</span></label>
                       <!--  <textarea class="form-control rounded-0" id="question_id" rows="10"></textarea> -->
                            <textarea name="seek-legal" id="question_id" class="ckeditor" ></textarea>
                               <?php
                   include_once 'ckeditor/ckeditor.php';
                   $ckeditor = new CKEditor('content');
                   $ckeditor->basePath = 'ckeditor/';                               
                   $ckeditor->replace(""); ?>

                      </div> 
                    </div>
                    <div class="col-sm-12">
                        <button class="btn btn-sm close_btn newsubmit" title="Add" id="submit" onclick="getclr()">Submit </button>
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  <div class="form-group col-sm-12" style="text-align: center;">
                    
                    <!-- <button class="btn btn-sm btn-danger" title="Add" id="submit" onclick="getclr()">Cancle </button> -->
                  </div>
                </form>
                </div>
              </div>



                      <div class="media responsebody">
                <div class="media-body">
                 <div class="card-body">
                  <h4>Case Legal Research Response</h4>
                  <hr/>
                  <div class="bread_crumbs" style="text-align: center;"> 
                   <!--      <span class="replyheading">Case Legal Research Response</span>  -->
                 </div>

                 <div  style="margin-bottom: 4%;">
                  <div class="col-sm-12">
                   <!-- <div class="col-sm-12"> -->
                    <div class="panel panel-primary">
                     <div class="">
                      <div class="panel-body">
                       <ul class="chat" style="font-size: 14px;">
                        <li class="left "><span class="chat-img pull-left" style="float: left;">
                         <!-- <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" /> -->
                       </span>
                       <div class="chat-body ">
                        <?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                        $row = mysqli_fetch_array($fetch);?>
                        <input type="hidden" id="email" value="MHMLS-<?php echo base64_decode($_GET['caseid']);?>">
                        <div class="header">
                          <small class="pull-right text-muted" style="width: 100%; float: left;"> </small>
                          <!-- <span class="glyphicon glyphicon-time"></span>12 mins ago</small> -->
                          <!-- <strong class="primary-font" style="color: green;">Name Of Matter:</strong> <span>Contract Labour</span><br> -->
                          <!-- <b> Document Type :</b><span>Notice</span><br> -->


                          <div id="queryshow"></div>


                        </div>
                      </div>
                    </li>
                  </ul>
                </div>

              </div>

            </div>
            <div id="pagen"></div>

            <!-- </div> -->


          </div>
        </div>
      </div>
    </div>
  </div>


            </div>
            <div class="tab-pane fade" id="exp-1" role="tabpanel" aria-labelledby="exp-tab">
              <div class="media">
                <div class="media-body">
                  <div>
                    <button class="btn btn-outline-warning" style="float: right;">Filter</button>
                    <button class="btn btn-outline-dark" style="float: right;margin-right: 10px">Export</button>
                    <button class="btn" style="background-color: #610b9e;color: white;float: right;margin-right: 10px;"><b>Add New</b></button>
                  </div>
                  <h4><b>My August 2019 Expenses</b></h4><br>
                  <table class="table" border="0.25">
                    <thead>
                      <tr>
                        <th style="background-color: #610b9e; color: white;">Sr. no.</th>
                        <th style="background-color: #610b9e; color: white;">Date</th>
                        <th style="background-color: #610b9e; color: white;">Particulars</th>
                        <th style="background-color: #610b9e; color: white;">Money Spent(in INR)</th>
                        <th style="background-color: #610b9e; color: white;">Payment Method</th>
                        <th style="background-color: #610b9e; color: white;">Member</th>
                        <th style="background-color: #610b9e; color: white;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="fee-1" role="tabpanel" aria-labelledby="fee-tab">
              <div class="media">
                <div class="media-body">
                  <div>
                    <button class="btn btn-outline-warning" style="float: right;">Filter</button>
                    <button class="btn btn-outline-dark" style="float: right;margin-right: 10px">Export</button>
                    <button class="btn" style="background-color: #610b9e;color: white;float: right;margin-right: 10px;"><b>Add New</b></button>
                  </div>
                  <h4><b>My August 2019 Fee Recived</b></h4><br>
                  <table class="table" border="0.25">
                    <thead>
                      <tr>
                        <th style="background-color: #610b9e; color: white;">Sr. no.</th>
                        <th style="background-color: #610b9e; color: white;">Date</th>
                        <th style="background-color: #610b9e; color: white;">Particulars</th>
                        <th style="background-color: #610b9e; color: white;">Fee Recived(in INR)</th>
                        <th style="background-color: #610b9e; color: white;">Payment Method</th>
                        <th style="background-color: #610b9e; color: white;">Member</th>
                        <th style="background-color: #610b9e; color: white;">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="clr-1" role="tabpanel" aria-labelledby="clr-tab">
              <div class="media">
                <div class="media-body">
                 <div class="card-body">
                  <h4>Case Legal Research Response</h4>
                  <hr/>
                  <div class="bread_crumbs" style="text-align: center;"> 
                   <!--      <span class="replyheading">Case Legal Research Response</span>  -->
                 </div>

                 <div  style="margin-bottom: 4%;">
                  <div class="col-sm-12">
                   <!-- <div class="col-sm-12"> -->
                    <div class="panel panel-primary">
                     <div class="">
                      <div class="panel-body">
                       <ul class="chat" style="font-size: 14px;">
                        <li class="left "><span class="chat-img pull-left" style="float: left;">
                         <!-- <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" /> -->
                       </span>
                       <div class="chat-body ">
                        <?php   $fetch =mysqli_query($connection,"SELECT * FROM `law_registration` WHERE `reg_id`='".$_SESSION['user_id']."'");
                        $row = mysqli_fetch_array($fetch);?>
                        <input type="hidden" id="email" value="MHMLS-<?php echo base64_decode($_GET['caseid']);?>">
                        <div class="header">
                          <small class="pull-right text-muted" style="width: 100%; float: left;"> </small>
                          <!-- <span class="glyphicon glyphicon-time"></span>12 mins ago</small> -->
                          <!-- <strong class="primary-font" style="color: green;">Name Of Matter:</strong> <span>Contract Labour</span><br> -->
                          <!-- <b> Document Type :</b><span>Notice</span><br> -->


                          <div id="queryshow"></div>


                        </div>
                      </div>
                    </li>
                  </ul>
                </div>

              </div>

            </div>
            <div id="pagen"></div>

            <!-- </div> -->


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
<?php include("footer.php");?>
<script type="text/javascript">
  $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
  });

</script>
<style type="text/css">
.add-more-button1 {
  position: relative;
  background: #43398a url(images/plus_icon.png) no-repeat scroll 9px 10px;
  border: 0 none;
  border-radius: 3px;
  color: #fff;
  font-size: 15px;
  height: 34px;
  line-height: 20px;
  padding: 0 10px 0 30px;
}
</style>
<div class="modal fade" id="casedetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Case Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script src="js/view_case.js"></script>
<script src="js/seeklegal.js"></script>
<script src="js/connected_cases_add.js"></script>

<script type="text/javascript" src="js/dist/dropzone.js"></script>
<!-- <script src="js/connectfile.js"></script> -->
<script type="text/javascript">
  function casedetails(id) {
    var id = +id;
    alert(id);
/*var name = document.getElementById("name").value;
var email = document.getElementById("email").value;
var password = document.getElementById("password").value;
var contact = document.getElementById("contact").value;*/
// Returns successful data submission message when the entered information is stored in database.
var dataString = 'name1=' + name + '&email1=' + email + '&password1=' + password + '&contact1=' + contact;
if (name == '' || email == '' || password == '' || contact == '') {
  alert("Please Fill All Fields");
} else {
// AJAX code to submit form.
$.ajax({
  type: "POST",
  url: "ajaxjs.php",
  data: dataString,
  cache: false,
  success: function(html) {
    alert(html);
  }
});
}
return false;
}
</script>
