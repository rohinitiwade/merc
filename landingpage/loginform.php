<?php session_start();?>

<!DOCTYPE html>
<?php session_start();?>
<?php include("includes/dbconnect_casetracker.php");?>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <head>
    <meta charset="utf-8">
    <title>Maharashtra LMS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Landing page template for creative dashboard">
    <meta name="keywords" content="Landing page template">
    <link rel="icon" href="assets/logos/favicon.ico" type="image/png" sizes="16x16">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="assets/css/login.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
  </head>
  <body>
    <div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
      <div class="container loginheader">
        <nav class="navbar navbar-expand-lg navbar-light navbar-default" role="navigation">
          <div class="container">
            <a class="navbar-brand page-scroll" href="#main">
              <img src="assets/logos/AIR VI Logo JPG.jpg" alt="MHLMS Logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon">
              </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
              </ul>
              <ul class="navbar-nav my-2 my-lg-0">
                <li class="nav-item">
                  <a class="nav-link page-scroll" href="index.php#main">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link page-scroll" href="index.php#features">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link page-scroll" href="index.php#services">Our Services</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Testimonial </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="index.php#contact-us">Contact Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link page-scroll" href="registration">Registration</a>
                </li>
                <!-- <li class="nav-item">
                  <a class="nav-link" href="login">Login</a>
                </li> -->
                <li class="nav-item">
                  <a class="nav-link" href="loginform">Login</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div class="main" id="main">
        <?php
			$dates  = date("Y-m-d");
			$time = date("H:i:s");
			$ipaddress = getenv("REMOTE_ADDR") ; 
			$ip = $ip_server = $_SERVER['SERVER_ADDR'];
			$date_time =  date("Y-m-d H:i:s");
			if (isset($_POST['login_script'])) {
			$email    = $_POST['email'];
			$password = $_POST['password'];
			$sql   = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE email='" . $email . "' and password='" . md5($password) . "'");
			$count = mysqli_num_rows($sql);
			if($count==0){
            $sql   = mysqli_query($connection1, "SELECT * FROM `law_registration` WHERE email='" . $email . "' and password='" . md5($password) . "'");
			}else{
			$msg = "Wrong Username Or Password";
			}
		    $row   = mysqli_fetch_array($sql);
			if($email =='' && $password == ''){
			  $msg = "Please Enter Username and Password";
			}
			elseif($email == ''){
			$msg = "Please Enter Username";
			}elseif($password == ''){
			  $msg = "Please Enter Password";
			}
			elseif($count==0){
			$msg = "Wrong Username Or Password";
			}
			if($row['account']=='Organisation' && $row['organisation_id'] == 11){
      // if ($count==1 && $row['approve']==1) {
      $_SESSION['user_id']        = $row['reg_id'];
      $_SESSION['cityName']       = $row['division'];
      $_SESSION['department']     = $row['department'];
      $_SESSION['designation']    = $row['designation'];
      $_SESSION['individual']    = $row['individual'];
      $_SESSION['account']        = $row['account'];
      $_SESSION['organisation_id'] = $row['organisation_id'];
      $login =  mysqli_query($connection, "INSERT INTO `login_history` SET `user_id`='" . $_SESSION['user_id'] . "',`login_date`='" . $dates . "',`login_time`='" . $time . "',`date_time`='" . $date_time . "',`remote_adder`='" . $ipaddress . "'");?>
              <script type="text/javascript">
                window.location.href='merc';
              </script>
              <?php
      // }elseif($row['approve']==0 && $count==1) {
      // $msg = "Pending for Approval";
      // }
      }elseif($row['account']=='Organisation'){
			if ($count==1 && $row['approve']==1) {
			$_SESSION['user_id']        = $row['reg_id'];
			$_SESSION['cityName']       = $row['division'];
			$_SESSION['department']     = $row['department'];
			$_SESSION['designation']    = $row['designation'];
      $_SESSION['individual']    = $row['individual'];
			$_SESSION['account']        = $row['account'];
			$_SESSION['under_division'] = $row['under_division'];
			$login =  mysqli_query($connection, "INSERT INTO `login_history` SET `user_id`='" . $_SESSION['user_id'] . "',`login_date`='" . $dates . "',`login_time`='" . $time . "',`date_time`='" . $date_time . "',`remote_adder`='" . $ipaddress . "'");?>
			        <script type="text/javascript">
			          window.location.href='casetracker';
			        </script>
			        <?php
			}elseif($row['approve']==0 && $count==1) {
			$msg = "Pending for Approval";
			}
			}elseif($row['account']=='Individual'){
			if ($count == 1 && $row['approve']==0) {
			$_SESSION['user_id']        = $row['reg_id'];
			$_SESSION['cityName']       = $row['division'];
			$_SESSION['department']     = $row['department'];
			$_SESSION['designation']    = $row['designation'];
      $_SESSION['individual']    = $row['individual'];
			$_SESSION['account']        = $row['account'];
			$_SESSION['under_division'] = $row['under_division'];
			$login = mysqli_query($connection, "INSERT INTO `login_history` SET `user_id`='" . $_SESSION['user_id'] . "',`login_date`='" . $dates . "',`login_time`='" . $time . "',`date_time`='" . $date_time . "',`remote_adder`='" . $ipaddress . "'");
			$select =  mysqli_query($connection, "SELECT * FROM `individual_account` WHERE `reg_id`='".$_SESSION['user_id']."'");
			$sellogin = mysqli_fetch_array($select);
			if($sellogin['expiry_date']<=date("Y-m-d H:i")){
			header("location:http:casetracker/pricing.php");
			}else{
			?>
			        <script type="text/javascript">
			          window.location.href='casetracker';
			        </script>
			        <?php
			}
			}
			}
			}?>
        <div class="hero-section app-hero">
          <div class="hero-content flex-features text-center">
            <div class="container">
              <div class="row justify-content-md-center">
                <div class="col-lg-5 col-sm-5 col-md-8 col-xs-12">
                  <form accept-charset="UTF-8" role="form" autocomplete="OFF" method="POST">
                    <div class="ribbon-front">
                      <h2>Log In
                      </h2>
                    </div>
                    <div class="ribbon-edge-topleft2"> 
                    </div>
                    <div class="ribbon-edge-bottomleft">
                    </div>
                    <h4 style="color: red;float: left;
                               width: 30%;">
                      <b>
                        <?php if($_GET['flag']!=""){ echo $_GET['flag']; } ?>
                      </b>
                    </h4>
                    <span class="msg">
                      <b>
                        <?php echo $msg;?>
                      </b>
                    </span>
                    <input type="text" class="form-control" placeholder="Your Email Address"  name="email" value="<?php echo $_POST['email'];?>" >
                    <input type="password" name="password" placeholder="Password" class="form-control">
                    <div class="col-12 row form-group">
                      <label class="col-sm-6">
                        <a href="registration.php" class="text-left f-w-600">Create an account</a>
                      </label>
                      <!-- <a href="forgot_password" class="text-right f-w-600"> Forgot Password?</a> -->
                      <label class="col-sm-6 p-0" style="text-align: right;"> 
                        <a href="forgot_password.php" class="text-right f-w-600">Forgot Password ?</a>
                      </label>
                    </div>
                    <button type="submit" class="btn btn-success btn-sm btn-block" name="login_script">Log in
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer">
          <div class="container">
            <div class="col-md-12 text-center">
              <img src="assets/logos/AIR VI Logo JPG.jpg" alt="MHLMS Logo" style="width: 9%;">
              <ul class="footer-menu">
                <li><a href="http://demo.com/">Site</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="#">Privacy</a></li>
              </ul>
              <div class="footer-text">
              </div>
            </div>
          </div>
        </div>
        <script src="assets/js/jquery-2.1.1.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
                <style type="text/css">
      @media only screen and (max-width: 800px){
       .navbar-default .navbar-collapse {
    text-align: center;
    border-color: transparent;
    background-color: #fff;
}

.ribbon-edge-topleft2 {
    left: 125px !important;
}
}
      @media only screen and (max-width: 450px){

.ribbon-edge-topleft2 {
    left: 106px !important;
}
}
      @media only screen and (max-width: 400px){

.ribbon-edge-topleft2 {
    left: 93px !important;
}
}
      @media only screen and (max-width: 350px){

.ribbon-edge-topleft2 {
    left: 77px !important;
}
}
   </style>




