<?php include("header.php");?>
<style type="text/css">
  .form-group {
    position: relative;
    margin-bottom: 1.5rem;
  }

  .form-control-placeholder {
    position: absolute;
    top: 0;
    padding: 7px 0 0 13px;
    transition: all 200ms;
    opacity: 0.5;
  }

  .form-control:focus + .form-control-placeholder,
  .form-control:valid + .form-control-placeholder {
    font-size: 75%;
    transform: translate3d(0, -100%, 0);
    opacity: 1;
  }
  .msg-error {
    color: #c65848;
  }
  .g-recaptcha.error {
    border: solid 2px #c64848;
    padding: .2em;
    width: 19em;
  }
  .formElement input[type="text"], select{
    max-width: 400px! important; 
  }

  .registration {
    margin: 2% auto 7%;
    width: 55%;
    background: #fff;
    border-radius: 0.5em;
    -webkit-border-radius: 0.5em;
    -o-border-radius: 0.5em;
    -moz- border-radius: 0.5em;
    margin-top: 128px;
  }
  .h2.ribbon-wrapper {
    position: relative;
  }
  .h2 .ribbon-front {
    background-color: #ffa300;
    height: 43px;
    width: 145px;
    position: relative;
    left: -10px;
    z-index: 2;
    top: 22px;
  }
  .h2 h2 {
    color: #fff;
    font-size: 0.8em;
    /* padding: 7px 9px 5px 27px; */
    /* font-weight: 300; */
    line-height: 37px;
    text-align: center;
  }
  .ribbon-edge-topleft2 {
    top: 22px;
    border-width: 22px 20px 21px 37px;
    transform: rotate(-180deg);
    border-radius: 1px;
  }
  .ribbon-edge-topleft2 {
    left: 135px;
    border-color: transparent #ffa300 transparent transparent;
  }
  .ribbon-edge-topleft2 {
    position: absolute;
    z-index: 1;
    border-style: solid;
    height: 0px;
    width: 0px;
  }
  .h2 .ribbon-edge-bottomleft {
    border-width: 0 10px 15px 0;
  }
  .h2 .ribbon-edge-bottomleft {
    left: -10px;
    border-color: transparent #e17e02 transparent transparent;
  }
  .h2 .ribbon-edge-bottomleft, .h2 .ribbon-edge-bottomright {
    top: 65px;
  }
  .h2 .ribbon-edge-topleft, .h2 .ribbon-edge-topright, .h2 .ribbon-edge-bottomleft, .h2 .ribbon-edge-bottomright {
    position: absolute;
    z-index: 1;
    border-style: solid;
    height: 0px;
    width: 0px;
  }
  ul {
    list-style: none;
    margin: 0px;
    padding: 0px;
  }
  form li {
    list-style: none;
    width: 100%;
    font-weight: 500;
    border: 1px solid #ccc;
    background: #fff;
    margin: 1em 0;
    outline: none;
  }
  input[type="text"], input[type="password"] {
    width: 100%;
    padding: 1em 0em 1em 2.7em;
    color: #858282;
    font-size: 15px;
    outline: none;
    background: none;
    font-weight: 500;
    /*border: none;*/
    height: 45px !important;
  }
  .submit input[type="submit"] {
    font-size: 20px;
    font-weight: 400;
    color: #fff;
    cursor: pointer;
    outline: none;
    padding: 21px 20px;
    width: 100%;
    border: none;
    transition: 0.1s all;
    -webkit-transition: 0.1s all;
    -moz-transition: 0.1s all;
    -o-transition: 0.1s all;
    background: #1d9c14;
    border-bottom-right-radius: 0.44em;
    -webkit-border-bottom-right-radius: 0.4em;
    -o-border-bottom-right-radius: 0.4em;
    -moz-border-bottom-right-radius: 0.4em;
    border-bottom-left-radius: 0.4em;
    -webkit-border-bottom-left-radius: 0.4em;
    -o-border-bottom-left-radius: 0.4em;
    -moz-border-bottom-left-radius: 0.4em;
  }
  body{
    background-color: #2d4866;
  }
  /* @media (max-width: 1366px)*/
  form {
    padding: 12% 4% 6% 4%;
  }
  .icon {
    height: 20px;
    width: 20px;
    display: block;
    float: left;
    margin: 12px -28px 0px 0px;
    border-right: 1px solid #999;
    padding: 5px 16px 10px 10px;
    color: black !important;
  }
  .msg{
    color: #d01515;
  }
  .log-in{
    color: #63a720;
    font-size: large;
  }
 </style>
<script type="text/javascript">
$(document).ready(function(){
    $('#state').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'country_id='+countryID,
                success:function(html){
                    $('#states').html(html);
                    $('#city').html('<option value="">Select District first</option>'); 
                }
            }); 
        }else{
            $('#states').html('<option value="">Select State first</option>');
            $('#city').html('<option value="">Select District first</option>'); 
        }
    });
    $('#states').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'state_id='+stateID,
                success:function(html){
                $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select District first</option>'); 
        }
    });
});
</script>
  <section id="about-us">
    <div class="container">
      <div class="skill-wrap clearfix">
        <div class="center wow fadeInDown">
          <h2>Free 15 Day Trial</h2>
          <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
        </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 form-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Registration Form</div>
                <div class="panel-body">
                    <form role="Form" method="post" action="submit.php" accept-charset="UTF-8">
                      <div class="">
                <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="text" id="name" class="form-control" autocomplete="OFF" name="name" value="<?php echo $_POST['name'];?>" required>
                    <label class="form-control-placeholder" for="name">Name</label>
                  </div>
                </div>
                  <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="text" id="last_name" class="form-control" name="last_name"  value="<?php echo $_POST['last_name'];?>"  required>
                    <label class="form-control-placeholder" for="last_name">Last Name</label>
                  </div>
                </div>
                 <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="text" id="mobile" class="form-control" name="mobile"  value="<?php echo $_POST['mobile'];?>"  required>
                    <label class="form-control-placeholder" for="mobile">Mobile Number</label>
                  </div>
                </div>
                 <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="text" id="email" class="form-control" name="email" value="<?php echo $_POST['email'];?>" required>
                    <label class="form-control-placeholder" for="email">Enter Email Address</label>
                  </div>
                </div>
                <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="password" id="password1" class="form-control" name="password" value="<?php echo $_POST['password'];?>" onkeyup='check();' required>
                    <label class="form-control-placeholder" for="password">Password</label>
                  </div>
                </div>
                 <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="password" id="confirm_passwords" class="form-control" name="confirm_password" value="<?php echo $_POST['confirm_password'];?>"  onkeyup='check();' required>
                    <label class="form-control-placeholder" for="confirm_password">Confirm Password</label>
                    <span id='message'></span>
                  </div>
                </div>

                <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <select  name="state" id="state" class="form-control" style="margin: 0px;">
                      <option  value="<?php echo $_POST['state'];?>">-- Select State--</option>
                      <?php $satte = mysqli_query($connection,"SELECT * FROM `geo_locations` WHERE `location_type` ='STATE'  ORDER BY `name` ASC");
                      while($selsatte = mysqli_fetch_array($satte)){?>
                      <option value="<?php echo $selsatte['id'];?>"><?php echo $selsatte['name'];?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                 <div class="col-md-6 mx-auto">
                  <div class="form-group">
                     <select id="states" name="district" class="form-control" required="">
                        <option value="<?php echo $_POST['states'];?>">Select State first</option>
                    </select>
                  </div>
                </div>
                 <div class="col-md-6 mx-auto">
                  <div class="form-group">
                     <select id="city" name="city" class="form-control">
                        <option value="<?php echo $_POST['city'];?>">Select District First</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 mx-auto">
                  <div class="form-group">
                     <select id="gender" class="form-control" name="gender" required="">
                        <option value="<?php echo $_POST['gender'];?>">Select Gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="other">Other</option>
                      </select>
                  </div>
                </div>
                <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="text" id="password1" class="form-control" name="designation" value="<?php echo $_POST['designation'];?>" onkeyup='check();' required>
                    <label class="form-control-placeholder" for="password">Designation</label>
                  </div>
                </div>
                 <div class="col-md-6 mx-auto">
                  <div class="form-group">
                    <input type="text" id="confirm_passwords" class="form-control" name="zip" value="<?php echo $_POST['zip'];?>"  onkeyup='check();' required>
                    <label class="form-control-placeholder" for="confirm_password">Zip/Postal Code</label>
                    <span id='message'></span>
                  </div>
                </div>
                 <div class="col-md-12 mx-auto">
                  <div class="form-group">
                     <textarea name="address" class="form-control" autocomplete="OFF" required="" placeholder="Enter Address"><?php echo $_POST['gender'];?></textarea>
                  </div>
                </div>
                 <div class="col-md-12 mx-auto">
                  <div class="form-group">
                    <div class="row">
                      <input type="checkbox" value=""  required="">
                      <a href="#" data-toggle="modal" data-target="#term"><b>By signing up, you agree to the Terms of Service and Privacy Policy</b></a>
                    </div>
                   
                    <h4 style="color: red;"><a href="login.php" style="color: red;">Already have an account? Log In</a></h4>

                  </div>
                </div>

              </div>
              <span class="msg-error error"></span>
<!-- <div id="recaptcha" class="g-recaptcha" data-sitekey="6Ld4Jh8TAAAAAD2tURa21kTFwMkKoyJCqaXb0uoK"></div>
 -->
<!-- <button class="btn" id="btn-validate">Validate reCAPTCHA</button>
        <!-<div  class="col-md-6 col-md-offset-4"> -->    
            <center><input type="submit" class="btn btn-primary btn-lg" id="submitbtn" name="submittrial" value="Sign Up" onclick = "return Validate()"></center>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
  </form>

      </div>
<hr/>
      <!-- our-team -->
     
      <!--section-->
    </div>
    <!--/.container-->
  </section>
  <?php include("footer.php");?>

<style type="text/css">
  .form-group {
  position: relative;
  margin-bottom: 1.5rem;
}

.form-control-placeholder {
  position: absolute;
  top: 0;
  padding: 7px 0 0 13px;
  transition: all 200ms;
  opacity: 0.5;
}

.form-control:focus + .form-control-placeholder,
.form-control:valid + .form-control-placeholder {
  font-size: 75%;
  transform: translate3d(0, -100%, 0);
  opacity: 1;
}
msg-error {
  color: #c65848;
}
.g-recaptcha.error {
  border: solid 2px #c64848;
  padding: .2em;
  width: 19em;
}
.formElement input[type="text"], select{
  max-width: 400px! important; 
}
</style>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
  $('#password1, #confirm_passwords').on('keyup', function () {
  if ($('#password1').val() == $('#confirm_passwords').val()) {
    $('#message').html('Matching').css('color', 'green');
  } else 
    $('#message').html('Not Matching').css('color', 'red');
})


  $( '#btn-validate' ).click(function(){
  var $captcha = $( '#recaptcha' ),
      response = grecaptcha.getResponse();
  
  if (response.length === 0) {
    $( '.msg-error').text( "reCAPTCHA is mandatory" );
    if( !$captcha.hasClass( "error" ) ){
      $captcha.addClass( "error" );
    }
  } else {
    $( '.msg-error' ).text('');
    $captcha.removeClass( "error" );
    alert( 'reCAPTCHA marked' );
  }
})
  </script>
  <div id="term" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b style="color: green;">Terms And Conditions</b></h4>
      </div>
      <div class="modal-body">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script src='select2/dist/js/select2.min.js' type='text/javascript'></script>
<link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
<script>
  $(document).ready(function(){
 
  // Initialize select2
  $("#state").select2();
  $("#states").select2();
  $("#city").select2();
  $("#selUser4").select2();
  $("#selUser5").select2();
  // Read selected option
  $('#but_read').click(function(){
    var username = $('#selUser option:selected').text();
    var userid = $('#selUser').val();

    $('#result').html("id : " + userid + ", name : " + username);

  });
});
  </script>