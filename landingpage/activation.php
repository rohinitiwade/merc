<?php include("includes/dbconnect.php"); session_start(); $_SESSION['user_id'];  $_SESSION['mobile'];?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com//polygon/adminty/files/extra-pages/landingpage/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Jan 2020 12:55:43 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<title>Maharashtra Litigation Management System</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Landing page template for creative dashboard">
	<meta name="keywords" content="Landing page template">

	<link rel="icon" href="assets/logos/favicon.ico" type="image/png" sizes="16x16">

	<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />

	<link href="assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<style>
		#about-us{
			min-height: 396px;
			display: flex;
			align-items: center;
		}
		.footer{
			padding-top: 30px;
			padding-bottom: 30px;
		}
		.otp-div-style{
			border-radius: 10px;
			box-shadow: 0px 0px 5px 0px rgba(0,0,0,.5);
			padding: 4%;
		}
		.header-background{
			background: #404E67;
		}

		.header-background .my-lg-0 li a{			
			color: white!important;
		}
	</style>
</head>
<body>
	<div class="wrapper animsition" data-animsition-in-class="fade-in" data-animsition-in-duration="1000" data-animsition-out-class="fade-out" data-animsition-out-duration="1000">
		<div class="header-background">
			<div class="container">
				<nav class="navbar navbar-expand-lg navbar-light navbar-default" role="navigation">
					<div class="container">
						<a class="navbar-brand page-scroll" href="#main">
							<img src="assets/logos/new_logo_small.png" alt="MHLMS Logo">
						</a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav mr-auto">
							</ul>
							<ul class="navbar-nav my-2 my-lg-0">
								<li class="nav-item">
									<a class="nav-link page-scroll" href="index.php#main">Home</a>
								</li>
								<li class="nav-item">
									<a class="nav-link page-scroll" href="index.php#features">About Us</a>
								</li>
								<li class="nav-item">
									<a class="nav-link page-scroll" href="index.php#services">Our Services</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Testimonial</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="index.php#contact-us">Contact Us</a>
								</li>
								<li class="nav-item">
									<a class="nav-link page-scroll" href="registration">Registration</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="login">Login</a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>


		<?php 
		$sqlww = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `email`='".base64_decode($_GET['email'])."' AND `mobile`='".base64_decode($_GET['flag'])."'");
		$showid = mysqli_fetch_array($sqlww);
		?>
		<section id="about-us">
			<div class="container">
				<center>
					<div class="col-sm-6 otp-div-style">
						<div class="container">
							<div class="error"></div>
							<?php if(isset($_POST['submit'])){
								extract($_POST);
								$sql = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `activate_email`='".$_POST['otpsend']."'");
								$count = mysqli_num_rows($sql);
								$rows = mysqli_fetch_array($sql);
								if($count==1){
									$update = mysqli_query($connection, "UPDATE `law_registration` SET `activate_email`='1' ,`otp`='1' WHERE `email`='".$rows['email']."' AND `mobile`='".$rows['mobile']."' AND `activate_email`='".$_POST['otpsend']."'");
									echo "<script>
									alert('OTP Sucessflly Matched');
									window.location.href='login.php';
									</script>";
								}else{
									$error ="OTP Not Matched Please Add Correct OTP";
								}
							}?>
							<form id="frm-mobile-verification" method="POST">
								<div class="form-heading" style="text-align: center;margin-bottom: 25px;">
									<h3><b>One Time Pin</b> <b style="color: red;"><?php echo $error; ?></b></h3>
									<!-- <h5></h5> -->
								</div>
								<div class="form-group" style="text-align: center;">
									<input type="text" name="otpsend" id="mobile" class="form-control" maxlength="6" placeholder="Enter OTP" required="" autocomplete="OFF">
									<!-- <div><b>Time left = <span id="timer"></b></span></div> -->  

								</div>
								<div class="form-group">
									<p>Didn't receive an OTP?  <a href="#" onclick="myFunction(<?php echo base64_decode($_GET['names'])?>)" > <b> Click here </b> </a>  to request a new one.</p>
								</div>
								<div class="form-group" style="text-align: center;">
									<input type="submit" class="btn-primary btn btn-sm" name="submit" value="Submit"  autocomplete="OFF">   
								</div>
							</form>
						</div>
					</div>
				</center>
			</div>
		</section>
		<div class="footer">
			<div class="container">
				<div class="col-md-12 text-center">
					<img src="assets/logos/new_logo_small.png" alt="MHLMS Logo" style="width: 9%;">
					<ul class="footer-menu">
						<li><a href="#">Site</a></li>
						<li><a href="#">Support</a></li>
						<li><a href="#">Terms</a></li>
						<li><a href="#">Privacy</a></li>
					</ul>
					<div class="footer-text">
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="assets/js/jquery-2.1.1.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script type = "text/javascript">

		let timerOn = true;

		function timer(remaining) {
			var m = Math.floor(remaining / 60);
			var s = remaining % 60;

			m = m < 10 ? '0' + m : m;
			s = s < 10 ? '0' + s : s;
			document.getElementById('timer').innerHTML = m + ':' + s;
			remaining -= 1;

			if(remaining >= 0 && timerOn) {
				$.ajax({
					type: 'post',
					url: 'post.php',
					data: $('form').serialize(),
					success: function () {
					}
				});
				setTimeout(function() {
					timer(remaining);
				}, 1000);
				return;
			}

			if(!timerOn) {
    // Do validate stuff here
    return;
}

  // Do timeout stuff here
  alert('Timeout for otp');
}

timer(120);


</script>
<script>
	function myFunction(mobile) {
		var mobile = +mobile;
		var dataString = 'mobile1=' + mobile;
		if (mobile == '') {
			alert("Please Fill All Fields");
		} else {
// AJAX code to submit form.
$.ajax({
	type: "POST",
	url: "otpsend.php",
	data: dataString,
	cache: false,
	success: function(html) {
		$("#data").html(html);
		alert(html);
	}
});
}
return false;
}
</script>
<!-- <style type="text/css">
	body{padding-top:20px;}

</style> -->