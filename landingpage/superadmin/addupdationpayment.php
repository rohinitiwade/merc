<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
   <title>New Order Form</title>
  <?php include("head.php");?>
   <style>
   label{color:#337ab7!important;font-size:12px!important}.col-md-12{padding:0!important}.payment_new_label{font-size:11px!important}.table>tbody>tr>td{border-top:none!important}input.cheque{border:none}input.error{border:1px solid red;margin-left:10px}.error{text-align:left!important}#updation_search-error{margin-left:80px;margin-top:5px;line-height:28px}.Search_div{width:100%;float:left}.Search_div label{font-size:14px;line-height:30px}.ms-options-wrap>button,.ms-options-wrap>button:focus{position:relative;border-bottom:1px solid #ccd0d4!important;width:90%;text-align:left;border:none;background-color:#fff;margin-top:0;font-size:13px;color:#000;outline-offset:0;white-space:nowrap}.ms-options-wrap>.ms-options{width:90%}#tab1default,#tab2default{margin-top:0}.new_head_title{font-size:16px!important;margin-bottom:20px}.head_reg{margin-bottom:15px;width:100%;float:left;text-align:center;font-size:20px}.register-content .form-control{display:block;width:90%;padding:0;border:none;border-bottom:1px solid #ccd0d4;border-radius:0!important;font-size:12px}.register-buttons{width:12%;margin:3% auto;float:none;text-align:center}.btn-group-lg>.btn,.btn-lg{padding:4px 16px;font-size:15px}.m-b-15{margin-bottom:5px!important}.updation_search_class{width:80%;border-radius:5px;margin-left:15px;padding:10px;border:1px solid #ccc}.wrapper_login{background:linear-gradient(rgba(20,20,20,.5),rgba(20,20,20,.5)),url(img/images.jpg) 100% no-repeat}.reset_filed{margin-left:15px;padding:5px 20px}.register{width:50%}input[type=radio]{margin:0 6px!important}.payment_box{color:#fff;padding:20px;display:none;margin-top:20px}.payment_1{display:block}.ms-options-wrap>button,.ms-options-wrap>button:focus{padding:1px 20px 5px 5px} .common .error{color:#555!important;} .error{color:red!important;} .ms-options-wrap > .ms-options > ul label {padding: 3px 10%;} .upadtion{
    color: #f44271;
    font-weight: bold;
  }
</style>
<script type="text/javascript">
      $(document).ready(function(){
          $("#scheme").hide();
           $("#dongletype").on('change',function(){ 
                var sch = $(this).val();
                if(sch == "Scheme")
                {
                  $("#scheme").show();
                }
                else{
                  $("#scheme").hide();
                }
           });
      });
</script>

<script type="text/javascript">
$(document).ready(function(){
  var t = 0;
var i = $('.orderform_table tr').length;
$(".add-row").on('click',function(){
  html = '<tr>';
  html += '<td style="vertical-align: text-bottom;"><input type="checkbox" name="record"/></td>';
  html += '<td><label class="control-label payment_new_label">Cheque No</label><input name="che_no[]" type="text" class="form-control" id="che_no_'+i+'" placeholder="Cheque No"  autocomplete="OFF"></td>';
  html += '<td style="width: 200px;"><label class="control-label payment_new_label">Bank Name</label><input name="bank_name[]" type="text" class="form-control" id="bank_name_'+i+'" placeholder="Bank Name"  autocomplete="OFF"><div id="suggesstion-bank_'+i+'"></div></td>';
  html += '<td style="width: 200px;"><label class="control-label payment_new_label">Branch Name</label><input name="branch_name[]" type="text" class="form-control" id="bank_name_'+i+'" placeholder="Branch Name"  autocomplete="OFF"><div id="suggesstion-branch'+i+'"></div></td>';
  html += '<td> <label class="control-label payment_new_label">IFSC Code</label> <input name="ifsc_code[]" type="text" class="form-control" id="ifsc_code_'+i+'" placeholder="IFSC Code"  autocomplete="OFF"><div id="suggesstion-ifc_'+i+'"></div> </td>';
  html += '<td> <label class="control-label payment_new_label">Amount Received</label> <input name="cashamt[]" type="text" class="form-control amount_recieve" id="cash_amt_'+i+'" placeholder="Amount Received" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"  autocomplete="OFF"> </td>';
  html += '<td> <label class="control-label payment_new_label">PDC Date</label> <input type="date" name="next_due_date[]" class="form-control" id="next_due_date_'+i+'" placeholder="Cheque Date" id="datepicker" autocomplete="OFF"> </td>';
  html += '<td> <div class="form-group"> <label>Cheque Image</label> <input type="file" class="form-control" id="cheque_'+i+'" name="cheque[]"></div></td>';
  html += '</tr>';
  i++;
  t++;

  $('.orderform_table').append(html);
});
// Find and remove selected table rows
  $(".delete-row").click(function(){
    $(".orderform_table").find('input[name="record"]').each(function(){
    if($(this).is(":checked")){
    $(this).parents("tr").remove();
    calculation();
    }
  });
});
}); 
</script>
<script>
 $(document).on("change keyup blur", ".amount_recieve", function() {
  calculation();
  });
  function calculation(){
   var sum = 0;
    $(".amount_recieve").each(function(){
        sum += +$(this).val().replace(/,/g, '');
    });
    $("#subTotal").val(sum); 

    $(document).on("change keyup blur onload click", function(){ 
      var getTotal= $("#amount").val();
      var getRemainingAmount = $("#subTotal").val();
        if(getTotal !==''){
        var remianing_amount = getTotal.replace(/,/g, '') - getRemainingAmount;
        $("#remianing_amount").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount").val('');
        }
   }); 
  }
</script>
   <script src="ajax/countyState.js"></script>
   <script src="ajax/CheckData45.js"></script>
   <script>
     $(function() {
  var my_options = $('.facilities select option');
  var selected = $('.facilities').find('select').val();
  my_options.sort(function(a,b) {
    if (a.text > b.text) return 1;
    if (a.text < b.text) return -1;
    return 0
  })
  $('.facilities').find('select').empty().append( my_options );
  $('.facilities').find('select').val(selected);
  $('.facilities').find('select').attr('multiple', true);
  $('.facilities').find('select option[value=""]').remove();
  $('.facilities').find('select').multiselect();
})
   </script>
<script>
 $(document).on("change keyup blur", ".amount_recieve3", function() {
      calculation3();
  });
  function calculation3(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount3").val();
      var getRemainingAmount = $("#subTotal3").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount3").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount3").val('');
        }
   }); 
  }    
 $(document).on("change keyup blur", ".amount_recieve4", function() {
      calculation4();
  });

  function calculation4(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount4").val();
      var getRemainingAmount = $("#subTotal4").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount4").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount4").val('');
        }
   }); 
  }    
 $(document).on("change keyup blur", ".amount_recieve5", function() {
      calculation5();
  });
  function calculation5(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount5").val();
      var getRemainingAmount = $("#subTotal5").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount5").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount5").val('');
        }
   }); 
  }   
 $(document).on("change keyup blur", ".amount_recieve6", function() {
      calculation6();
  });

  function calculation6(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount6").val();
      var getRemainingAmount = $("#subTotal6").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount6").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount6").val('');
        }
   }); 
  }   
      $(document).ready(function(){
        $('#forms').addClass('active');
        $('#order_form_add').addClass('active');
      });</script>
   <script type="text/javascript">
      $(document).ready(function(){
          $("#name").autocomplete({
              source:'autocomplete.php',
              minLength:1
          });
      });
   </script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".payment_box").not(targetBox).hide();
        $(targetBox).show();
        if(inputValue=='payment_1'){
          $('.payment_3').val('');
          $('.payment_4').val('');
          $('.payment_5').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_3'){
          $('.payment_5').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_4'){
          $('.payment_5').val('');
          $('.payment_3').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_5'){
          $('.payment_3').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_6'){
          $('.payment_5').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_3').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_7'){
          $('.payment_5').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_3').val('');
        }
    });
});
</script>
   <?php include_once("header.php"); ?>
   <?php include_once("sidebar.php"); ?>  
<script>
 $(document).on("change keyup blur", ".amount_recieve2", function() {
      calculation2();
  });
  function calculation2(){
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount2").val().replace(/,/g, '');
      var getRemainingAmount = $("#subTotal2").val().replace(/,/g, '');
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount2").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount2").val('');
        }
   }); 
  }
</script>
   <div id="content" class="content">
      <ol class="breadcrumb" style="width: 100%;">
         <li><a href="http://crm.airinfotech.in/telecaller/">Home</a></li>
         <li><a href="http://crm.airinfotech.in/telecaller/">Dashboard</a></li>
         <li class="active" style="color: green;">Updation of Client</li>
      </ol>
      <div class="details">
         <h3 class="col-sm-8 details11" style="margin-top: 0px; color: green;"><span>Updation of Client&nbsp;&nbsp;&nbsp;&nbsp;</span><a href="http://crm.airinfotech.in/telecaller/" class="btn btn-primary btn-xs">Back To Home Page</a>
          &nbsp;&nbsp;&nbsp;
        <a href="allotedclient2.php" class="btn btn-warning btn-xs">Alloted Client List</a></h3>
         <?php include("timezone.php");?>
        <hr style="float: left;width: 100%;">
        <div class="col-md-12">
             <?php 
          if(!empty($_GET["cid"])) {
          $query = mysql_query("SELECT * FROM `order_form` WHERE `updt_clientId`='". $_GET["cid"] ."' ");
          $fetch= mysql_fetch_array($query);?>
                      <div class="register-content">
                          <div class="new_head_title" style="color: green;"><b>Basic Information</b></div>
                          <hr>
                          <div class="row row-space-10">
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label"> First Name <span class="text-danger">*</span></label>                              
                              <input type="text" class="form-control" name="name" id="name" value="<?php echo $fetch['name'];?>"onkeyup="cust_name()" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_fname"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Last Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="last_name" value="<?php echo $fetch['last_name'];?>" id="lname" onkeyup="cust_name()" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_lname"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Email<span class="text-danger">*</span></label>
                            <input type="text" class="form-control email_check"  id="email" name="email" value="<?php echo $fetch['email'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_email"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Mobile No. <span class="text-danger">*</span></label>
                              <input type="text" class="form-control"  id="mobile" onkeypress="return isNumber(event)" maxlength="10" name="mobile" value="<?php echo $fetch['mobile'];?>, <?php echo $fetch['rphone'];?>" autocomplete="OFF"/>
                          <div style="color:#F00; font-size:12px; text-align:left" id="error_mobile"></div>
                            </div>
                          </div>
                          <div class="row row-space-10">
                            <div class="col-sm-3 col-xs-6">
                              <label class="control-label">Phone No. <span class="text-danger"></span></label>
                           <input type="text" class="form-control"  id="rphone" maxlength="10"  name="rphone" value="<?php echo $_POST['rphone'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_phone(r)"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6" >
                              <label class="control-label">Address<span class="text-danger">*</span></label>
                               <textarea class="form-control" rows="2" id="add" name="address" autocomplete="OFF"><?php echo $fetch['address'];?></textarea>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_add"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Pin Code</label>
                                 <input type="text" class="form-control" id="pin" maxlength="6" name="pincode" value="<?php echo $fetch['pincode'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_pin"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15 common">
                              <label class="control-label">Select State<span class="text-danger">*</span></label>
                             <?php $state = mysql_query("SELECT * FROM `states` WHERE `state_id`='".$fetch['state_name']."'");
                              $selState = mysql_fetch_array($state);?>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="state_name" value="<?php echo $selState['state_name'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_state"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Select District<span class="text-danger">*</span></label>
                             <?php $districts = mysql_query("SELECT * FROM `cities` WHERE `city_id`='".$fetch['city']."'");
                              $seldistrict = mysql_fetch_array($districts);?>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="state_name" value="<?php echo $seldistrict['city_name'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_city"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15 displayDropdown">
                              <label class="control-label">Enter City/Town<span class="text-danger">*</span></label>
                              <input type="text" class="form-control email_check"  id="town" name="town" alue="<?php echo $fetch['district'];?>" autocomplete="OFF" placeholder="Enter Town / City" />
                                  <div id="suggesstion-town"></div>
                            </div>
                             <div class="col-sm-3 col-xs-6 m-b-15">
                              <div class="form-group">
                        </div>
                            </div>
                          </div>
                           <div class="new_head_title" style="color: green;"><b>Product Details</b></div>
                           <hr>
                          <div class="row row-space-10">
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                <label class="control-label">Business Associate Name</label>
                                <?php 
                                   $sql= mysql_query("select * from `registration` where `id`='".$_SESSION['user_id']."'");
                                              $row = mysql_fetch_array($sql);?>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $row['name']; ?> <?php echo $row['last_name']; ?>" readonly>
                              </div>
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Dongal Type</label>
                                  <select class="form-control" name="dongletype_0" id="dongletype" value="<?php echo $_POST['dongletype_0'];?>"  placeholder="Dongle Type" autocomplete="OFF">
                                  <option value="<?php echo $fetch['dongletype_0'];?>"><?php if($fetch['dongletype_0']!=""){ echo $fetch['dongletype_0']; }else{?>--Select Type--<?php } ?></option>
                                  <option value="Individual">Individual</option>
                                  <option value="Network">Network</option>
                                  <option value="Scheme">Scheme</option>
                                  <option value="Bulk">Bulk</option>
                                  <option value="Complimentary">Complimentary</option>
                                  </select>
                              </div>
                               <div class="col-sm-3 col-xs-6 m-b-15 displayDropdown">
                                <label class="control-label">Select Product</label>
                              <div class='facilities'><span class='input'>
                                <select name="product_0[]">
                                   <option value="<?php echo $fetch['product_0'];?>"><?php if($fetch['product_0']!=""){ echo $fetch['product_0']; }else{?>-----Select Product-----<?php } ?></option>
                                  
                                   <option value="Lesearch Comprehensive (LE-COMP)">Lesearch Comprehensive</option>
                                   <option value="Lesearch Individual (SC)">Lesearch Individual (SC)</option>
                                   <option value="Lesearch Individual (HC)">Lesearch Individual (HC)</option>
                                   <option value="Lesearch Individual (CRILJ)">Lesearch Individual (CRILJ)</option>
                                   <option value="BHCR (Bombay High Court)">BHCR (Bombay High Court)</option>
                                   <option value="Allahabad High Court (ALJ)">Allahabad High Court (ALJ)</option>
                                   <option value="Privi Council (PC)">Privi Council (PC)</option>
                                   <option value="Case Law Navigator (CASE-LAW)">Case Law Navigator (CASE-LAW)</option>
                                </select>
                              </span>
                              </div>
                              </div>
                            <div class="row row-space-10">
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Select Scheme</label>
                              <select class="form-control" name="scheme_0">
                              <option title="Bar Council of India" value="<?php echo $fetch['scheme_0'];?>"><?php if($fetch['scheme_0']!=""){ echo $fetch['scheme_0']; }else{?>--Select Scheme--<?php } ?></option>
                              <option title="Bar Council of India"> BCI </option>
                              <option title="Bar Council Maharashtra Goa"> BCMG </option>
                              <option title="CENTINARY SCHME"> CENT </option>
                             <!--  <option title="DEMO DVD PROJECT"> DEMO-PRO </option> -->
                              <option title="Journal Scheme"> JRNL </option>
                              <option title="Air Case Navigator"> NAVIGATOR </option>
                              <option title="Open General License Scheme"> OGL </option>
                              <option title="ONLINE"> ONLINE </option>
                              </select>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">HASP ID / Online Client</label>
                               <input type="text" class="form-control" name="haspid" id="haspid" value="<?php echo $fetch['haspid']; ?>" autocomplete="OFF">
                               <span style="color: red;">Note: For Online client write (Online)</span>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">DVD No.<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="dvdno" id="dvdno" value="<?php echo $fetch['dvdno'];?>"  autocomplete="OFF">
                            </div>
                             <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">DVD Year<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="year_0" id="year_0" value="<?php echo $fetch['year_0'];?>" autocomplete="OFF">
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15 sales">
                              <label class="control-label">Sales By<span class="text-danger"></span></label>
                              <select name="salesfrom" class="form-control">
                                <option value="<?php echo $fetch['sales_from'];?>"><?php if($fetch['sales_from']!=""){ echo $fetch['sales_from']; }else{?>-----Select Sale by-----<?php } ?> </option>
                                <option value="AIS">AIS</option>
                                <option value="AIR Infotech">AIR Infotech</option>
                              </select>
                            </div>
                             
                          </div>
                          </div>
                            <div class="new_head_title"><b style="color: green;">Payment History</b></div>
                          <hr>
                          <?php  $ORDERPAYment = mysql_query("SELECT * FROM `order_form_payment_mode` WHERE `client_id`='".$fetch['id']."'");?>
                          <?php while($selectQuery = mysql_fetch_array($ORDERPAYment)){?>
                          <div class="row row-space-10">
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Payment Status<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="exname" id="exname" value="<?php if($selectQuery['payment_status']!=''){echo $selectQuery['payment_status'];}else{echo "New";} ?>" readonly autocomplete="OFF">
                           </div>
                          <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Payment Mode<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['payment_mode']; ?>" readonly autocomplete="OFF">
                           </div>
                              <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Cheque No.<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['cheque_no']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Bank Name<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['bank_name']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Reccived Amount<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['reccived_amt']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Post Dated Cheque<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo date("d-m-Y",strtotime($selectQuery['due_date']));?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Status<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['cheque_status'];?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Date Time<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo date("d-m-Y",strtotime($selectQuery['date_time']));?>" readonly autocomplete="OFF">
                           </div>
                          </div>

                          <?php } ?>
                           <form name="myform" method="POST" action="submit_data.php" enctype="multipart/form-data">
                          <input type="hidden" name="exnameId" value="<?php echo $fetch['ex_id'];?>">
                           <div class="new_head_title" style="color: green;"><b>Add Payment  </b></div>
                           <hr>
                              <div class="row row-space-10w">
                                <div class="col-sm-12 m-b-15">
                                  <div class="payment_div">
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_1" checked="checked">By Cheque</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_3">By NEFT</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_4">By RTGS</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_5">Bank Deposit</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_6">Paytm</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_7">Bhim App</label>
                                  </div>
                                  <div class="payment_1 payment_box">
                                      
                                    <table class="table orderform_table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><input type="checkbox" name="record"></td>
                                          <td> 
                                            <label class="control-label payment_new_label">Cheque No</label> 
                                            <input name="che_no[]" type="text" class="form-control payment_1" id="che_no_1" placeholder="Cheque No" autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;" class="displayDropdown"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                          <input type="text" class="form-control payment_1" id="bank_name" name="bank_name[]" value="<?php echo $_POST['bank_name'];?>" autocomplete="OFF" placeholder="Enter Bank Name" />
                                          <div id="suggesstion-bank"></div>
                                          
                                          </td>
                                          <td style="width: 200px;" class="displayDropdown"> 
                                         <label class="control-label payment_new_label">Branch Name</label>  
                                          <input type="text" class="form-control payment_1"  id="branch_name" name="branch_name[]" value="<?php echo $_POST['branch_name'];?>" autocomplete="OFF" placeholder="Enter Branch name"/>
                                          <div id="suggesstion-branch"></div>
                                          
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control payment_1" id="ifsc_code" placeholder="IFSC Code" autocomplete="OFF">
                                             <div id="suggesstion-ifc"></div>
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cashamt[]" type="text" class="form-control amount_recieve payment_1" id="cash_amt_1" placeholder="Amount Received" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" 
                                             autocomplete="OFF">
                                          </td>
                                        
                                          <td> 
                                            <label class="control-label payment_new_label">Cheque Date</label> 
                                            <input type="date" name="next_due_date[]" class="form-control payment_1" id="next_due_date_1" placeholder="Cheque Date" id="datepicker" autocomplete="OFF">
                                          </td>
                                          <td> 
                                           <div class='form-group'> <label>Cheque Image</label> <input type='file'  class='form-control' id="cheque_1" name="cheque[]"></div>
                                          </td>

                                        </tr>
                                      </tbody>
                                    </table>

                                        <input type="button" class="add-row btn btn-primary btn-xs" value="Add Cheque">
                                        <input type="button" class="delete-row btn  btn-primary btn-xs" value="Delete Cheque"> 

                                     <div class="col-sm-12 by_cheque">
                                      <div class="col-sm-3 m-b-15">
                              <label class="control-label">Total Amount</label>
                              <input type="text" class="form-control payment_1" name="total_amount" id="amount" value="<?php echo $_POST['total_amount'];?>" onkeypress="return IsNumeric(event);" placeholder="Total Amount" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                          </div>
                          <div class="col-sm-3 m-b-15">
                              <label class="control-label">Total Amount Received</label>
                              <input type="text" class="form-control payment_1" id="subTotal" name="cash_amtc" placeholder=" Total Received Amount" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF" >
                          </div>
                           <div class="col-sm-3 m-b-15">
                              <label class="control-label">Remaining Amount</label>
                              <input type="text" class="form-control payment_1" name="remianing_amount" id="remianing_amount" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                          </div>
                        </div>
                                  </div>
   
                                  <div class="payment_2 payment_box">
                                    <table class="table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><!-- <input type="checkbox" name="record"> --></td>
                                          <td> 
                                            <label class="control-label payment_new_label">DD No</label> 
                                            <input name="che_no[]" type="text" class="form-control" onkeypress="return IsNumeric(event);" placeholder="DD No"  autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                            <input name="bank_name[]" type="text" class="form-control" placeholder="Bank Name" >
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control" placeholder="IFSC Code"  autocomplete="OFF">
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cashamt[]" type="text" class="form-control" placeholder="Amount Received"  autocomplete="OFF">
                                          </td>
                                          
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="payment_3 payment_box">
                                   <div class="col-sm-3 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve2 payment_3" name="reccived_amountNEFT" id="amount2" value="<?php echo $_POST['amount'];?>" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF">
                                </div>
                                <div class="col-sm-3 m-b-15">
                                    <label class="control-label"> Amount Received</label>
                                    <input type="text" class="form-control payment_3" id="subTotal2" name="reccived_amountNEFTrec" placeholder="  Amount Received" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_3" name="remianing_amountNEFT" id="remianing_amount2" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                              <div class="col-sm-3 m-b-15">
                                  <label class="control-label">Reference No. / UTR No.</label>
                                  <input type="text" class="form-control payment_3" name="utr_noneft" id="utr_no" value="" placeholder="Reference No. / UTR No." autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                         
                                  </div>  

                                <div class="payment_4 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve3 payment_4" name="totalRTGS" id="amount3" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_4" id="subTotal3" name="cash_amtRTGS" placeholder="Total Amount Received" onkeypress="return IsNumeric(event);" value="" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_4" name="remianing_amountRTGS" id="remianing_amount3" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>UTR No.</label>
                                      <input type="text" class="form-control payment_4" id="utr_no" name="utr_noRTGS" placeholder="UTR No." autocomplete="OFF">
                                    </div>
                                  
                                  </div>

                                     <div class="payment_5 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve4 payment_5" name="recAmountBankAccount" id="amount4" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_5" id="subTotal4" name="cash_amtBankAccount" placeholder="Total Amount Received" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_5" name="remianing_amountBankAccount" id="remianing_amount4" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                  <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>Deposite Receipt</label>
                                      <input type="file" class="form-control payment_5" name="deposite_receiptBankAccount" id="deposite_receipt" value="" autocomplete="OFF">
                                      
                                    </div>
                         
                                  </div>

                                  <div class="payment_6 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve5 payment_6"" name="recAmount" id="amount5" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_6"" id="subTotal5" name="cash_amt" placeholder="Total Amount Received" onkeypress="return IsNumeric(event);" value="" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div> 
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_6"" name="remianing_amount5" id="remianing_amount5" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>Order No.</label>
                                      <input type="text" class="form-control payment_6" id="utr_no" name="utr_no" placeholder="UTR No." autocomplete="OFF">
                                    </div>
                                  
                                  </div>

                                  <div class="payment_7 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve6 payment_7 name="recAmountb" id="amount6" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_7" id="subTotal6" name="cash_amtb" placeholder="Total Amount Received" onkeypress="return IsNumeric(event);" value="" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_7" name="remianing_amount7" id="remianing_amount6" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>Order No.</label>
                                      <input type="text" class="form-control payment_7" id="utr_no" name="utr_nobhim" placeholder="Order No." autocomplete="OFF">
                                    </div>
                                  </div>
                                  <div class="row row-space-10 status">
                            <div class="col-sm-4 col-xs-6 m-b-15">
                            <label class="control-label">Enter Client Status Here<span class="text-danger">*</span></label>
                          <textarea class="form-control" name="resolve" required></textarea>
                          </div>
                        </div>
                                   <div class="register-buttons">
                                   <input type="hidden" value="<?php echo $fetch['id'];?>" name="fetchID">
                                          <input type="submit" class="btn btn-primary" value="Submit" name="payment_updation">
                                    </div>
                                </div>
                                <hr>
                              </div>
                        </form>
                      </div>
                    </div>
               <?php }?>
   </div>
   
   <script type="text/javascript">
      $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
   </script>            
   <!-- ================== BEGIN BASE JS ================== -->
   <?php include("footer.php");?>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
<script>
$(document).ready(function(){
  $("#town").keyup(function(){
    $.ajax({
    type: "POST",
    url: "townSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#town").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      $("#suggesstion-town").show();
      $("#suggesstion-town").html(data);
      $("#town").css("background","#FFF");
    }
    });
  });
});
function selecttown(val) {
$("#town").val(val);
$("#suggesstion-town").hide();
}
$(document).ready(function(){
  $("#bank_name").keyup(function(){
    $.ajax({
    type: "POST",
    url: "banknameSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      $("#suggesstion-bank").show();
      $("#suggesstion-bank").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbank(val) {
$("#bank_name").val(val);
$("#suggesstion-bank").hide();
}
$(document).ready(function(){
  $("#branch_name").keyup(function(){
    var bank = $("#bank_name").val();
    $.ajax({
    type: "POST",
    url: "branchSearch.php",
    data:'keyword='+$(this).val() +'&bank='+bank,
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      $("#suggesstion-branch").show();
      $("#suggesstion-branch").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbranch(val) { 
  var branch = val;
  var bank = $("#bank_name").val();
    $.ajax({
            type:'POST',
            url:'ifscSearch.php',
            data:'branchname='+branch + '&bank='+bank,
            success:function(data){
                  $("#ifsc_code").val(data);           
                }
            }); 
  $("#branch_name").val(val);
  $("#suggesstion-branch").hide();
}
</script>
<script type="text/javascript">
  $(document).ready(function(){
      $("#displays").keyup(function(){
          $.ajax({
          type: "POST",
          url: "auto_client.php",
          data:'clientSearch='+$(this).val(),
          beforeSend: function(){
          $("#displays").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
          },
          success: function(data){
              $("#client-box").show();
              $("#client-box").html(data);
              $("#displays").css("background","#FFF");
              $('#DivHide').hide();
          }
          });
      });
  });
        function selectCountry(val) {
    var id = +val;
     $("#client-box").hide();
            $.ajax({
            type: "POST",
            url: "displays.php",
            data:'ids='+id,
            beforeSend: function(){
            $("#displays").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
            },
            success: function(data){
                $("#update_clients").show();
                $("#updation_data").html(data);
                $("#displays").css("background","#FFF");                             
            }
            });
      }
  $(document).ready( function() {
      $(document).on('change', '.btn-file :file', function() {
    var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
    });
    $('.btn-file :file').on('fileselect', function(event, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = label;
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });   
  });
</script>
<script src="https://rawgit.com/nobleclem/jQuery-MultiSelect/master/jquery.multiselect.js"></script>
   </body>
</html>
<style>
#bank-lists,#country-lists{float:left;list-style:none;margin:0;padding:0;z-index:1}#bank_name,#town{padding:4px}#bank-lists{position:absolute;width:17%}#country-lists{width:40%;position:relative;height:310px;overflow:auto}#bank-lists li,#country-lists li{padding:10px;cursor:pointer;background:#f0f0f0;border-bottom:#bbb9b9 1px solid}#country-lists li:hover{background:#F0F0F0}.btn-file{position:relative;overflow:hidden}.btn-file input[type=file]{s
    background:#fff;position:absolute;top:0;right:0;min-width:100%;min-height:100%;font-size:100px;text-align:right;filter:alpha(opacity=0);opacity:0;outline:0;background:#fff;cursor:inherit;display:block}#img-upload{width:100%}</style><style type="text/css">.btn-bs-file{position:relative}.btn-bs-file input[type=file]{position:absolute;top:-9999999;filter:alpha(opacity=0);opacity:0;width:0;height:0;outline:0;cursor:inherit}.client_list{margin-left:5%}
</style>
