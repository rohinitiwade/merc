<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
  <?php include("head.php");?>
<script>
  $(document).ready(function(){
    $('#data-tableDaily').dataTable();
  });
  $(document).ready(function(){
    $("#all_report").addClass("active");
    $("#dailycalling_report").addClass("active");
    $("#callDate").datepicker({
        todayHighlight: !0,
        autoclose: true,
        format:'yyyy-mm-dd',
        orientation: 'bottom'
    });
  });
</script>
</head>
<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>  
    <div id="content" class="content">
        <ol class="breadcrumb" style="width: 100%;">
            <li><a href="http://crm.airinfotech.in/telecaller/">Home</a>/</li>
            <li><a href="http://crm.airinfotech.in/telecaller/">Dashboard</a>/</li>
            <li class="active">Today Calling List</li>
        </ol>
<div class="details">
  <h3 class="col-sm-8" style="margin-top: 0px; color: green;"><span>Daily Calling List &nbsp;</span><a href="http://crm.airinfotech.in/telecaller/" class="btn btn-primary btn-xs">Back To Home Page</a>&nbsp;<a href="allotedclient2.php" class="btn btn-warning btn-xs">Alloted Client List</a>&nbsp;<a href="nextappointment.php" class="btn btn-success btn-xs">Next Appointment List </a>&nbsp;<a href="convertedclient.php" class="btn btn-danger btn-xs">Converted Client List</a>
  </h3>  
   <?php include("timezone.php");?>
  <hr style="float: left;width: 100%;">
<?php if(isset($_POST['submit'])){
  $dates = $_POST['date'];
} ?>
<form name="" method="POST">
<div class="row">
  <div class="col-sm-1 selectPortion"><h5>Select Date</h5></div>
  <div class="col-sm-2 col-xs-5"><input type="text"  name="date" id="callDate" class="form-control" value="" autocomplete="OFF"></div>
  <div class="col-sm-4 col-xs-2"><input type="submit" value="Submit" name="submit" class="btn btn-primary btn-sm"></div>
</div>
</form>
  <hr style="float: left;width: 100%;">
  <div class="col-sm-12 table-responsive dsr_form_reports_tb">
    <table id="data-tableDaily" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
      <th>Sr</th>
      <th>Client Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>City</th>
      <th>Client Status</th>
      <th>Called Details</th>
      <th>Appintment Date</th>
      <th>Called For</th>
      <th>View Details</th>
      <th>Source</th>
    </tr>
</thead>
    <tbody> 
     <?php
     if($dates==""){
      $sql=mysql_query("SELECT DISTINCT `client_id`,`next_appointment` FROM `clientstatus` WHERE `ex_id`='".$_SESSION['user_id']."' AND `call_date`='".date("Y-m-d")."' ORDER BY id DESC");
    }else{
     $sql=mysql_query("SELECT DISTINCT `client_id`,`next_appointment` FROM `clientstatus` WHERE `ex_id`='".$_SESSION['user_id']."' AND `call_date`='".$dates."' ORDER BY id DESC");
    }
      $i=1;
      while($row=mysql_fetch_array($sql)){ 
        $sel2=mysql_query("SELECT * FROM `airinfot_newcustomercontact` WHERE `custid`='".$row['client_id']."'");
        $fetch3=mysql_fetch_array($sel2);?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $fetch3['name'];?>  <?php echo $fetch3['last_name'];?></td>
        <td><?php echo $fetch3['email'];?></td>
        <td><?php echo $fetch3['personal_mobile'];?> <?php echo $fetch3['personal_phoness'];?> <?php echo $fetch3['work_phone'];?></td>
        <td><?php echo $fetch3['personal_city']?></td>
        <td><a data-toggle="modal" data-target="#myModalDSR" class="btn btn-success btn-xs" onclick="Status(<?php echo $fetch3['custid'];?>);">Add Client Status</a>
        </td>
        <td>
          <?php  $clientStatus = mysql_query("SELECT * FROM `clientstatus` WHERE `ex_id`='".$_SESSION['user_id']."' AND `client_id`='".$row['client_id']."'"); $selClient = mysql_num_rows($clientStatus); if($selClient>0){?>
          <a data-toggle="modal" data-target="#history" class="btn btn-warning btn-xs" onclick="history(<?php echo $fetch3['custid'];?>);">Called Details</a>
          <?php } ?>
        </td>
        <td><?php echo $row['next_appointment']?></td>
        <td id="change_input<?php echo $i;?>"><input type="button" onclick="calledfor(<?php echo $fetch3['custid'];?>)" value="Called For" data-target="#reasonModal" data-toggle="modal" class="btn btn-xs btn-primary" name=""></td>
        <td><button class="btn btn-info btn-xs" type="button" data-target="#callingModal" onclick="details(<?php echo $fetch3['custid'];?>)" data-toggle="modal">View Details</button></td>
        <td><?php echo $fetch3['cust_status']?></td>
      </tr>
     <?php $i++;} ?>
    </tbody>
    </table>
  </div>
</div>
</div>
<div id="callingModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">View Client Details</h3>
      </div>
      <div class="modal-body">
      <div id="details"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
 
  <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

<div id="myModalDSR" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="info"></div>
  </div>
</div>
<div id="history" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="his"></div>
  </div>
</div>

<div id="reasonModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
<div id="calledfor"></div>
  </div>
</div>
 
<script>
  function Status(id) {
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/dsrStatus.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#info' ).html(result);
  }
});
  }
   function details(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/details.php?flag=calling",
    data: "id="+id,
    cache: false,
    success: function(data){
    $( '#details' ).html(data);
  }
});
  }
  function calledfor(id) {
   var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/calledfor.php?flag=daily",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#calledfor' ).html(result);
  }
});
  }
    function history(id) {
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/dsrhistory.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#his' ).html(result);
  }
});
  }
</script>
   <?php include("footer.php"); ?>
