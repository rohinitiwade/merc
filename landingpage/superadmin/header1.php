<?php session_start(); include("includes/connection.php"); ?>
<?php  $designation=$_SESSION['designation'];
$id=$_SESSION['user_id']; ?>
<?php if($id==""){?>
<script>
    window.location.href='http://crm.airinfotech.in/';
</script>
<?php }?>
 <?php
 $pagename =  basename($_SERVER["PHP_SELF"]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>CRM Admin | Profile Page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet" />
    <link href="assets/css/style.min.css" rel="stylesheet" />
    <link href="assets/css/style-responsive.min.css" rel="stylesheet" />
    <link href="assets/css/theme/default.css" rel="stylesheet" id="theme" />
    <link rel="stylesheet" type="text/css" href="css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css"> 
    <link rel="stylesheet" type="text/css" href="css/responsive.css">  
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
    <!-- ================== END BASE CSS STYLE ================== -->
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="assets/plugins/pace/pace.min.js"></script>
   
    <!-- ================== END BASE JS ================== -->
    <!-- <script>
    $(document).ready(function(){
        $('.dropdown').click(function(e){
             $('.dropdown').removeClass('open');
             var $this = $(this);
             if(!$this.hasClass('open')){
                $this.addClass('open');
             }

        });
    });
</script> -->
<script>
  $(document).ready(function(){
    $("#other_sub").hide();
    $("#half_day").hide();
    $("#full_day").hide();
    $(".from_to").hide();
      $("#new_btn").click(function () {
      $("#sidebar").css("display", "block");
      //$("#new_btn").css("display","none");
    });
    $(".closebtn").click(function () {
      $("#sidebar").css("display", "none");

      //$("#new_btn").css("display","block");
    });
$("#complaints").on('change',function(){
  var val = $(this).val();
  if(val == "Other"){
    $("#other_sub").show();
  }
  else{
    $("#other_sub").hide();
  }
});
$("#leavetype").on('change',function(){
  var val = $(this).val();
  if(val == "half"){
    $("#half_day").show();
    $(".from_to").hide();
    $("#full_day").hide();
  }
  else if(val == "full"){
    $("#full_day").show();
    $("#half_day").hide();
    $(".from_to").hide();
  }
  else{
    $("#half_day").hide();
    $(".from_to").show();
    $("#full_day").hide();
  }
});
  });
</script>
<style>
.setting_dropdown
{
  box-shadow: 0px 2px 5px 1px #888888 !important;
}
#outer_bor h2 {
    margin: 0 auto;
    padding: 0px 0px 0px 6px;
    font: bolder 14px/32px Cambria, serif;
    color: #006064;
    background-color: #ebebeb;
    text-align: left;
}
.crm_support_id {
    margin: 0 auto;
    padding: 0px 0px 0px 6px;
    font: bolder 14px/32px Cambria, serif;
    color: #006064;
    background-color: #ebebeb;
    text-align: left;
}
.profile_sec td
{
  padding: 8px;
}
.show {
  display:block;
 }
    #outer_bor {
    width: 100%;
    margin: 0 auto;
    padding: -1px;
    border: 1px solid #d5d5d5;
    background-color: #FFFFFF;
}
.profile_sec {
    margin: 0 auto;
    padding: 0px;
    border: 1px solid #e6e6e6;
    background-color: #f7f7f7;
    border: 1px solid #d7d7d7;
    text-align: center;
}
.my_icon{
  height: 31px;
  width: 31px;
  line-height: 32px;
  font-size: 14px;
  color: #fff;
  text-align: center;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
}
.upload_pic_modal h4 {
    width: 94%;
    float: left;
    color: #23527c;
    margin-left: 1%;
    text-align: center;
}
.upload_pic_modal .modal-dialog {
    width: 30%;
    margin: 30px auto;
}
.uploaded_pic
{
 width: 130px;
height: 140px;
float: left;
border: 1px solid #ccc;
padding: 10px;
}
.file_div
{
  width: 50%;
  float: right;
}
.upload_pic_modal .modal-body
{
width: 100%;
float: left;
}
 a.btn:hover {
     -webkit-transform: scale(1.1);
     -moz-transform: scale(1.1);
     -o-transform: scale(1.1);
 }
 .btn:hover {
     -webkit-transform: scale(1.1);
     -moz-transform: scale(1.1);
     -o-transform: scale(1.1);
 }
 
</style>
</head>
<body>
 <?php $ProfileUp = mysql_query("SELECT `image` FROM `registration` WHERE `id`='".$_SESSION['user_id']."' ");?>
<?php $ProfilReg = mysql_fetch_array($ProfileUp);?>
<?php if($ProfilReg['image']==""){?>
  <div id="ProfileUpload" class="modal fade upload_pic_modal" role="dialog">
  <div class="modal-dialog">
    <?php if(isset($_POST['uploadImage']))
                {
                  //echo "Done";
                  if($_POST['uploadImage']!=""){
                  $errors= array();
                  $file_name = time($_FILES['ProfileImage']['name']);
                  $file_size =$_FILES['ProfileImage']['size'];
                  $file_tmp =$_FILES['ProfileImage']['tmp_name'];
                  $file_type=$_FILES['ProfileImage']['type'];
                  $file_ext=strtolower(end(explode('.',$_FILES['ProfileImage']['name'])));
                  $target=$file_name.".".$file_ext;
                  $expensions= array("jpeg","jpg","png");            
                  if(in_array($file_ext,$expensions)=== false){
                   //   echo "extension not allowed, please choose a JPEG or PNG file.";
                  }
                  if($file_size > 100000){
                   //echo 'File size must be excately 1 MB';
                  }
                  if(empty($errors)==true){
                     move_uploaded_file($file_tmp,"../nationalmanager/uploadImages/userProfile/".$file_name.".".$file_ext."");
                    //echo "Success";
                     if($file_ext!="") {
                       $imgupdate=mysql_query("UPDATE `registration` SET `image`='$target' WHERE `id`='".$_SESSION['user_id']."'");
                     }
                    //header("Location:user_profile.php?userId=".$_SESSION['user_id']."");
                  }else{
                    // print_r($errors);
                  }
               }
            }
?>
          <form name="Formss" method="POST" enctype="multipart/form-data">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class=".modal-title"><b>Please Upload Profile Image</b></h4>
                </div>
                <div class="modal-body" style="border: none;">
                  <div class="form-group">
                      <img id='img-upload'/ class="uploaded_pic" src="img/user1.png">
                      <div class="input-group file_div">
                          <span class="input-group-btn">
                              <span class="btn-bs-file btn btn-xs btn-warning btn-file">
                                Browse… <input type="file" id="imgInp" name="ProfileImage" required="" autocomplete="OFF">
                              </span>
                          </span>
                          <input type="text" class="form-control" readonly style="margin-left: 8px;" name="ProfileImage" autocomplete="OFF">
                      </div>
                  </div>
                </div>
                <div class="modal-footer" style="border: none;">
                  <input type="submit" class="btn btn-primary" value="Upload Image" name="uploadImage">
                  <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
              </div>
          </form>
            </div>
          </div>
          <?php } ?>
    <!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><div class="status">&nbsp;</div></div>
  <!-- end #page-loader -->
    <!-- begin #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
<div class="modal fade" id="user_change_pass" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
      <form method="POST">
        <div class="modal-header">
        <?php if(isset($_POST['password_change'])){
        extract($_POST);
         $oldPass = md5($old_pass);
          if($oldPass!="")
          {
             $sql= mysql_query("SELECT `password`,`id`,`email_id`,`username` FROM `registration` WHERE `id`='".$_SESSION['user_id']."'");
             $res = mysql_fetch_array($sql);
             //echo $res['password'];
             if($res['password']==$oldPass)
             {
                 if($new_pass==$conf_pass)
                 {
                   $update = mysql_query("UPDATE `registration` SET `password`='".md5($new_pass)."',`Password_hint`='".$new_pass."' WHERE `id`='".$_SESSION['user_id']."'");
                                $to = $res['email_id'];
                                $subject = "CRM Change Password";
                                $message = "
                                <html>
                                <head>
                                <title>CRM Change Password</title>
                                </head>
                                <body>
                                <table>
                                <tr>
                                <th>Username</th>
                                <th>Password</th>
                                </tr>
                                <tr>
                                <td>".$res['username']."</td>
                                <td>".$new_pass."</td>
                                </tr>
                                </table>
                                </body>
                                </html>
                                ";
                                // Always set content-type when sending HTML email
                                $headers = "MIME-Version: 1.0" . "\r\n";
                                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                                // More headers
                                $headers .= 'From: CRM change password<crm@airinfotech.in>' . "\r\n";
                                $headers .= 'Cc: myboss@example.com' . "\r\n";
                                mail($to,$subject,$message,$headers);
                                }else{
                                    echo '<script>
                                    alert("New Password And Confirm Password Not Match");
                                    </script>'; }
             }else{ 
               echo '<script>
                alert("Old Password Does Not Match");
                </script>';
                echo $message= "Old Password Does Not Match";
             }
          }
        }?>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Change Password</h3>
        </div>
        <div class="modal-body">
              <label>Old Password</label>
              <input type="password" class="form-control" placeholder="Enter old password here..." name="old_pass" id="old_pass" required autocomplete="OFF">
              <br>
              <label>New Password</label>
              <input type="password" class="form-control" placeholder="Enter new password here..." name="new_pass" id="new_pass" required autocomplete="OFF"><br>
              <label>Confirm New Password</label>
              <input type="password" class="form-control" placeholder="Confirm new password here..." name="conf_pass" id="Confirm_pass" required autocomplete="OFF">
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" name="password_change" value="Submit">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
         </form>
      </div>
    </div>
  </div>  
<!-- begin #header -->
    <div id="header" class="header navbar navbar-fixed-top">
    <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
          <a href="index.php" class="navbar-brand"><img src="img/logo_white.png" width="50"></a>
          <!-- <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button> -->
        </div>
        <span id="new_btn" class="new_btn">&#9776;</span>
        <!-- end mobile sidebar expand / collapse button -->
      <div class="container-fluid">
        <!-- begin header navigation right --> 
        <ul class="nav navbar-nav navbar-right">
                 <!-- <li class="navbar-form">
                  <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#leaves">Apply Leaves
</button>
                </li> -->
                <li class="navbar-form">
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#Ticketopen">Open TIcket</button>
                </li>
                 <li class="navbar-form">
                    <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#TicketCreate">Create Ticket</button>
                </li>
                <li class="navbar-form">
                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#supportCreate">Support</button>
                </li>
                <li class="navbar-form">
                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#leaves">Apply leave</button>
                </li>
                <li class="navbar-form">
                     <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#Rating">Rating</button>
                </li>
                <li class="navbar-form">
                     <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#editorial">Editorial Module</button>
                </li>
                <!-- <li class="navbar-form">
                <a href="marketing_material.php" style="padding: 0px"><input type="button" class="btn btn-danger btn-sm" value="Marketing Material" name=""></a>
                </li> -->
          <li>
            <script>
                $(document).ready(function(){
                    $("#search-box").keyup(function(){
                        $.ajax({
                        type: "POST",
                        url: "searchMenu.php",
                        data:'keyword='+$(this).val(),
                        beforeSend: function(){
                            $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                            $("#suggesstion-box").show();
                            $("#suggesstion-box").html(data);
                            $("#search-box").css("background","#FFF");
                        }
                        });
                    });
                });
                //To select country name
                function selectCountry(val) {
                $("#search-box").val(val);
                $("#suggesstion-box").hide();
                }
                </script>
                <style>
                    /*.frmSearch {border: 1px solid #a8d4b1;}*/
                    #country-list{float:left;list-style:none;margin-top:1px;padding:0;width:300px;position: absolute;}
                    #country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
                    #country-list li:hover{background:#ece3d2;cursor: pointer;}
                    #search-box{padding: 5px;border: #a8d4b1 1px solid;border-radius:4px;}
                     #country-list1{float:left;list-style:none;margin-top:1px;padding:0;width:300px;position: absolute;}
                    #country-list1 li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid;}
                    #country-list1 li:hover{background:#ece3d2;cursor: pointer;}
                    #search-box1{padding: 5px;border: #a8d4b1 1px solid;border-radius:4px;}
                    </style>
            <form class="navbar-form full-width" method="POST">
              <div class="form-group">
                                <div class="frmSearch">
                                    <input type="text" id="search-box" class="form-control" placeholder="Enter Name / Enter Haspid" autocomplete="OFF" required="" />
                                    <div id="suggesstion-box" class="animated fadeInLeft"></div>
                                </div>
                <button type="submit" class="btn btn-search" name="keyword"><i class="fa fa-search"></i></button>
              </div>
            </form>
          </li>
           <?php 
            $reminder=mysql_query("SELECT DISTINCT `client_id`,`reminder_call` FROM `clientstatus` WHERE `callStatus`='converted' AND `reminder_call`!=0 AND `ex_id`='".$_SESSION['user_id']."'");
            $reminders = mysql_fetch_array($reminder);
             $seladteDAte = date('Y-m-d',strtotime(' + 45 day', strtotime($reminders['reminder_call'])));
          ?>
          <li class="dropdown" id="noti_crm">
            <a  class="f-s-14 dropdown-toggle" data-toggle="dropdown" data-toggle="tooltip" data-placement="bottom" title="Reminder Call">
              <i class="fa fa-bell-o " style="color: yellow;"></i>
              <?php  if($seladteDAte<=date("Y-m-d")){  echo '<span class="label" style="background-color:blue;">'; echo $seladteDAte = mysql_num_rows($reminder);
 '</span>'; }?>
            </a>
                       <!--  <script type="text/javascript">
                        function myFunction(){
                            //alert("OK");
                            $.ajax({
                                url: "view_notification.php",
                                type: "POST",
                                processData:false,
                                success: function(data){
                                 //alert(data);
                                    $("#notification-count").remove();                  
                                    $("#notification-latest").show();$("#notification-latest").html(data);
                                },
                                error: function(){}           
                            });
                            $(this).removeClass('open')
                         }
                         
                         $(document).ready(function() {
                            $('body').click(function(e){
                                if ( e.target.id != 'notification-icon'){
                                    $("#notification-latest").hide();
                                }
                            });
                        });      
                    </script> -->
            <ul id="myDropdown_bell" class="dropdown-menu drop_menu_bell media-list pull-right animated fadeInDown bell_drop setting_dropdown">
                  <li class="dropdown-header">Reminder Client after 45 Days (<?php  $seladteg = date('Y-m-d',strtotime(' + 45 day', strtotime($reminders['reminder_call']))); if($seladte<=date("Y-m-d")){echo $remindercount; }?>)</li>
                      <?php 
                         $sql3="SELECT DISTINCT `client_id`,`reminder_call` FROM `clientstatus` WHERE `callStatus`='converted' AND `reminder_call`!=0 AND `ex_id`='".$_SESSION['user_id']."' ORDER BY id DESC";
                          $result3=mysql_query($sql3);
                         while($rows3 = mysql_fetch_array($result3)) {
                          $reg = mysql_query("SELECT * FROM `airinfot_newcustomermaster` WHERE `custid`='".$rows3['client_id']."'");
                          $SelReg = mysql_fetch_assoc($reg);
                          $count=mysql_num_rows($result3);
                         // date("Y-m-d");
                           $rows3['reminder_call'];
                               $seladteg = date('Y-m-d',strtotime(' + 45 day', strtotime($result3['reminder_call'])));
                               if($seladte<=date("Y-m-d")){
                               $startTimeStamp = strtotime($seladte);
                               $endTimeStamp = strtotime(date("Y-m-d"));
                               $timeDiff = abs($endTimeStamp - $startTimeStamp);
                               $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                              $regis = mysql_query("SELECT * FROM `registration` WHERE `id`='".$_SESSION['user_id']."'");
                              $seRegist = mysql_fetch_array($regis);?>
                            <li class="media">
                                <a href="reminderclient.php">
                                    <div class="media-left"><img src="../nationalmanager/uploadImages/userProfile/<?php echo $seRegist['image'];?>" width="50"></div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Reminder Call to <?php  echo $SelReg['name']; ?> <?php  echo $SelReg['last_name']; ?></h6>
                                        <div class="text-muted f-s-11"><?php echo $rows3['reminder_call'];?></div>
                                    </div>
                                </a>
                            </li> 
                            <?php    }  } ?>
                          
                            <li class="dropdown-footer text-center">
                                <a href="reminderclient.php">View All</a>
                            </li>
            </ul>
          </li>
                    <?php 
                    $result=mysql_query("SELECT * FROM `clientstatus` WHERE `next_appointment`='".date("d/m/Y")."' AND `ex_id`='".$_SESSION['user_id']."'");
                    $count = mysql_num_rows($result);
                    $rows = mysql_fetch_array($result);
                    ?>
          <li class="dropdown" id="noti_crm">
            <a  class="f-s-14 dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o " style="color: white;"></i>
              <?php if($count>0)  { echo '<span class="label">'; echo $count; '</span>';} ?>
            </a>
                        <script type="text/javascript">
                        function myFunction() {
                            //alert("OK");
                            $.ajax({
                                url: "view_notification.php",
                                type: "POST",
                                processData:false,
                                success: function(data){
                                 //alert(data);
                                    $("#notification-count").remove();                  
                                    $("#notification-latest").show();$("#notification-latest").html(data);
                                },
                                error: function(){}           
                            });
                            $(this).removeClass('open')
                         }
                         $(document).ready(function() {
                            $('body').click(function(e){
                                if ( e.target.id != 'notification-icon'){
                                    $("#notification-latest").hide();
                                }
                            });
                        });      
                    </script>
            <ul id="myDropdown_bell" class="dropdown-menu drop_menu_bell media-list pull-right animated fadeInDown bell_drop setting_dropdown">
                  <li class="dropdown-header">Notifications (<?php echo $count;?>)</li>
                      <?php 
                         $sql3="SELECT * FROM clientstatus WHERE `ex_id`='".$_SESSION['user_id']."' AND `next_appointment`='".date("d/m/Y")."'";
                          $result3=mysql_query($sql3);
                         while($rows3 = mysql_fetch_array($result3)) {
                          $reg = mysql_query("SELECT * FROM `airinfot_newcustomermaster` WHERE `custid`='".$rows3['client_id']."'");
                          $SelReg = mysql_fetch_assoc($reg);
                          $count=mysql_num_rows($result3);
                          $registartion = mysql_query("SELECT * FROM `registration` WHERE `id`='".$rows3['ex_id']."'");
                          $registartionSel = mysql_fetch_assoc($registartion);
                          ?>
                            <?php if($count>0){ ?>
                            <li class="media">
                                <a href="nextappointment.php">
                                    <div class="media-left"><img src="../nationalmanager/uploadImages/userProfile/<?php echo $registartionSel['image'];?>" width="50"></div>
                                    <div class="media-body" style="vertical-align: middle;">
                                        <h6 class="media-heading"><?php  echo $SelReg['name']; ?> <?php  echo $SelReg['last_name']; ?></h6>
                                         <div class="text-muted f-s-11">Next Appointment Date <?php echo $rows3['next_appointment'];?></div>
                                    </div>
                                </a>
                            </li> 
                            <?php } } ?>
                            <li class="dropdown-footer text-center">
                                <a href="nextappointment.php">View more</a>
                            </li>
            </ul>
          </li>
               <li class="dropdown navbar-user">
                <a >
                   <?php $sql = mysql_query("SELECT `name`,`last_name`,`email_id`,`image` FROM `registration` WHERE `id`='".$id."'");
                   $res = mysql_fetch_array($sql);?>
              <img src="../nationalmanager/uploadImages/userProfile/<?php echo $res['image'];?>" alt="" /> &nbsp;&nbsp;&nbsp;
              <span class="hidden-xs" style="color: white;"><?php echo ucfirst($res['name']);?>&nbsp;&nbsp;&nbsp;<?php echo ucfirst($res['last_name']);?></span> 
              </a>
                    </li>
                      <li class="dropdown">
                      <a  class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog" style="color: white;"></i></a>
                      <ul class="dropdown-menu setting_dropdown">
                       <li class="arrow"></li>
                       <li><a href="user_profile.php?userId=<?php echo $_SESSION['user_id'];?>"><i class="fa fa-eye"></i> View Profile</a></li>
                       <li><a data-toggle="modal" data-target="#user_change_pass" style="cursor: pointer;"><i class="fa fa-lock"></i>Change Password</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a onclick="logoutFunction()"><i class="fa fa-power-off" style="color: white;"></i></a>
                    </li>
                    <!-- <li class="dropdown navbar-user" >
                        <a href="javascript:;" class="dropbtn" onclick="myFunction_log()">
                            <i class="fa fa-power-off" style="color: white;"></i>
                        </a>
                        <ul id="myDropdown_logout" class="dropdown-menu drop_menu_log animated fadeInRight">
                            <li class="arrow"></li>
                            <li><a href="user_profile.php?userId=<?php echo $_SESSION['user_id'];?>"><i class="fa fa-eye"></i> View Profile</a></li>
                            <li><a data-toggle="modal" data-target="#user_change_pass" style="cursor: pointer;"><i class="fa fa-lock"></i>Change Password</a></li>
                            <li><a href="logout.php"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li> -->      
        </ul>
        <!-- end header navigation right -->
  </div>          
      <!-- end container-fluid -->
</div>
<div class="container">
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="TicketCreate" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Create Ticket</h3>
        </div>
        <div class="modal-body">
           <form name="Supports" method="POST" action="submit.php?page=<?php echo $pagename; ?>">
              <div class="form-group">
                <label for="email">Support:</label>
                <select name="ticeketname" class="form-control" required="required">
                  <option value="">--Select--</option>
                  <option value="Technical">Technical</option>
                  <option value="Suggestion">Suggestion</option>
                  <option value="Product">Product</option>
                  <option value="Crm Team">CRM Team</option>
                </select>
              </div>
              <!-- <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" class="form-control" id="email" value="support@airinfotech.in" readonly="readonly">
              </div> -->
              <div class="form-group">
                <label for="pwd">Details:</label>
                <textarea class="form-control" name="Details" required=""></textarea>
              </div>
              <input type="submit" class="btn btn-info" value="Submit" name="ticketMessages">
            </form> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="Ticketopen" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Open Ticket</h3>
        </div>
        <div class="modal-body">
           <div class="col-md-12 table-responsive dsr_form_reports_tb">
    <table id="data-tableAlloted" class="table table-bordered">
    <thead>
    <tr>
      <th>Sr.No.</th>
      <th>My Message</th>
      <th>Reply Status</th>
      <th>Resolve Message</th>
      <th>Resolve By</th>
      <th>Reply Date</th>
      <th>Create Date</th>
    </tr>
</thead>
    <tbody class="alloted_client_body"> 
     <?php
     $sql=mysql_query("SELECT * FROM `ticket` WHERE `ex_id`='".$_SESSION['user_id']."'");
      $i=1;
      while($row=mysql_fetch_array($sql)){ ?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row['message'];?></td>
        <td><?php echo $row['resolve_status'];?></td>
        <td><?php echo $row['resolve_message'];?></td>
        <td><?php echo $row['solve_by'];?></td>
        <td><?php echo $row['resolve_date_time'];?></td>
        <td><?php echo $row['sent_date_time'];?></td>
      </tr>
     <?php $i++;} ?>
    </tbody>
    </table>
  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="Rating" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h2 class="crm_support_id">My Rating</h2>
        </div>
        <div class="modal-body">
          <div id="outer_bor">
  <div id="view_profile">
 <div class="col-md-12 table-responsive dsr_form_reports_tb">
    <table id="data-table" class="table table-bordered">
    <thead>
    <tr>
      <th>Sr.No.</th>
      <th>My Rating</th>
      <th>Date TIme</th>
    </tr>
</thead>
    <tbody class="alloted_client_body"> 
     <?php
     $sql=mysql_query("SELECT * FROM `se_rating` WHERE `ex_id`='".$_SESSION['user_id']."'");
      $i=1;
      while($row=mysql_fetch_array($sql)){?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $row['rating'];?></td>
        <td><?php echo $fetch3['date_time'];?></td>
      </tr>
     <?php $i++;} ?>
    </tbody>
    </table>
  </div>
  </div>
</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade" id="supportCreate" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <h2 class="crm_support_id">Crm Support Team</h2>
          <div id="outer_bor">
  <div id="view_profile">
  <table class="profile_sec" width="100%" border="0" align="center" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td rowspan="5" width="25%" align="center">
        <!-- <img src="uploadedImage/faculty/1509270101.png" style="border:1px solid #cecece; padding:1px;" width="110" height="130"> -->
          <img src="../img/logo_white.png" style="border:1px solid #cecece; padding:1px; background-color: #275fa9;" width="110" height="130">
        </td>
      <td width="18%"><h5>Name:</h5></td>
      <td width="52%"><h5>CRM Team</h5></td>
    </tr>
    <tr>
      <td><h5>Phone No:</h5></td>
      <td><h5>9975988452</h5></td>
    </tr>
    <tr>
      <td><h5>Email:</h5></td>
      <td><h5>crm@airinfotech.in</h5></td>
    </tr>
  </tbody></table>
  </div>
</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
    <!-- en #header -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Bar chart js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
     <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<script>
    $(document).ready(function(){
/*MARQUEE CODE*/
    $(".dropdown-toggle").dropdown();
        $("marquee").hover(function () { 
          this.stop();
        }, function () {
       this.start();
   });
 });
 /*MINIMIZE CHAT WINDOW*/
    </script>
     <script>
  $(window).load(function(){     
   $('#ProfileUpload').modal('show');
    }); 
</script>
 <script>
  /*$(document).ready(function(){
    $(".dropdown").click(function(){
      $(this).toggleClass('open');
    });
  });*/
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function logoutFunction() {
    if (confirm("Are you sure you want to logout !")) {
       window.location = "logout.php";
    } 
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-menu");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
  /*$(document).on('click',function(){
    $('.dropdown').removeClass('open');

  });*/
 </script>
 <script type="text/javascript" src="highslide/highslide-with-gallery.js"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
<script type="text/javascript">
    hs.graphicsDir = 'highslide/graphics/';
    hs.align = 'center';
    hs.transitions = ['expand', 'crossfade'];
    hs.wrapperClassName = 'dark borderless floating-caption';
    hs.fadeInOut = true;
    hs.dimmingOpacity = .7;
    // Add the controlbar
    if (hs.addSlideshow) hs.addSlideshow({
        //slideshowGroup: 'group1',
        interval: 3000,
        repeat: false,
        useControls: true,
        fixedControls: 'fit',
        overlayOptions: {
            opacity: .6,
            position: 'bottom center',
            hideOnMouseOut: true
        }
    });
</script>
<div class="modal fade" id="editorial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><b style="color: green;">Editorial Module</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="" method="POST" action="submit.php?pagename=<?php echo $pagename;?>">
        <div class="form-group" required>
          <label for="email"> Complaints Against:</label>
          <select name="subject" class="form-control" id="complaints">
            <option value="">--Select Subject --</option>
            <option value="Case Law Search-Ontopic">Case Law Search-Ontopic</option>
            <option value="Case Not Found in AIR">Case Not Found in AIR</option>
            <option value="Jugment Requirtment">Jugment Requirtment</option>
            <option value="Bare act rules notifications">Bare act rules notifications</option>
            <option value="Other">Other</option>
          </select>
        </div>
        <div class="form-group" id="other_sub">

          <label>Other Subject:</label>

          <input type="text" class="form-control" placeholder="Enter Subject" name="other_subject" autocomplete="OFF">

        </div>

        <div class="form-group" id="">

          <label>Name :</label>

          <input type="text" class="form-control" placeholder="Enter Client Name" name="name" autocomplete="OFF" required>

        </div>

        <div class="form-group" id="">

          <label>Mobile Number :</label>

          <input type="text" class="form-control" placeholder="Enter Mobile Number" name="mobile" autocomplete="OFF" maxlength="15">

        </div>

        <div class="form-group" id="">

          <label>Email Id:</label>

          <input type="email" class="form-control" placeholder="Enter Email Id" name="email" autocomplete="OFF">

        </div>

        <div class="form-group">

          <label for="pwd">Details :</label>

          <textarea class="form-control" required="required" name="details" autocomplete="OFF"></textarea>

        </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-warning" value="Submit" name="editorial">

      </div>

      </form> 

    </div>

  </div>

</div>


<div class="modal fade" id="leaves" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><b style="color: green;">Apply Leave</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form name="" method="POST" action="submit.php">
         <div class="form-group">
          <label for="email">Leave For:</label>
          <select class="form-control" id="leavetype" required="" name="leavefor">
            <option disabled selected value="">-- Select --</option>
            <option value="half">Half Day</option>
            <option value="full">Full Day / One Day</option>
            <option value="more">More than 1 Day</option>
          </select>
        </div> 
        <div class="form-group" id="half_day">
          <label>Date:</label>
          <input type="text" class="form-control" value="<?php echo date("d/m/Y");?>" name="half_day" readonly>
        </div>

         <div class="form-group" id="full_day">
          <label>Date:</label>
          <input type="text" class="form-control" id="datepickerdate" name="fullday">
        </div>
        <div class="from_to">
        <div class="form-group">

          <label for="email">From :</label>

          <input type="text" class="form-control" id="datepickerleaves" name="from">

        </div>

        <div class="form-group">

          <label for="email">To :</label>

          <input type="text" class="form-control" id="datepickerleavesto" name="to">

        </div>

      </div>

        <div class="form-group">

          <label for="pwd">Reason :</label>

          <textarea class="form-control" required="required" name="reason"></textarea>

        </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

        <input type="submit" class="btn btn-primary" value="Leaves Apply" name="leaves">

      </div>

      </form> 

    </div>

  </div>

</div>





  <script>

  $( function() {

    $( "#datepickerleaves" ).datepicker();

    $( "#datepickerleavesto" ).datepicker();

    $( "#datepickerdate" ).datepicker({

       todayHighlight: !0,

        startDate:'+0d',

        autoclose: true,

        format:'dd/mm/yyyy'

    }); 

 } );

  </script>

   <script>

  /*$(window).load(function(){     

   $('#myModal').modal('show');

    }); 
*/
</script>

<script>

  $(document).ready( function() {

      $(document).on('change', '.btn-file :file', function() {

    var input = $(this),

      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

    input.trigger('fileselect', [label]);

    });

    $('.btn-file :file').on('fileselect', function(event, label) {

        var input = $(this).parents('.input-group').find(':text'),

            log = label;

        if( input.length ) {

            input.val(log);

        } else {

            if( log ) alert(log);

        }

    });

    function readURL(input) {

        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function (e) {

                $('#img-upload').attr('src', e.target.result);

            }

            reader.readAsDataURL(input.files[0]);

        }

    }



    $("#imgInp").change(function(){

        readURL(this);

    });   

  });

</script>

<script>

    $(document).ready(function(){

        $("#data-table").dataTable();

    });

</script>
<script type="text/javascript">
$(document).ready(function () {
    //Disable full page
    $('body').bind('cut copy', function (e) {
        e.preventDefault();
    });
    
    //Disable part of page
    $('#id').bind('cut copy', function (e) {
        e.preventDefault();
    });
});
</script>
<script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>
<script type="text/javascript">
$(document).ready(function () {
    //Disable full page
    $("body").on("contextmenu",function(e){
        return false;
    });
    
    //Disable part of page
    $("#id").on("contextmenu",function(e){
        return false;
    });
});
</script>
<script type="text/javascript"> 
$(document).ready(function() {
   function copyToClipboard() {
  // Create a "hidden" input
  var aux = document.createElement("input");
  // Assign it the value of the specified element
  aux.setAttribute("value", "Você não pode mais dar printscreen. Isto faz parte da nova medida de segurança do sistema.");
  // Append it to the body
  document.body.appendChild(aux);
  // Highlight its content
  aux.select();
  // Copy the highlighted text
  document.execCommand("copy");
  // Remove it from the body
  document.body.removeChild(aux);
  alert("Print screen desabilitado.");
}

$(window).keyup(function(e){
  if(e.keyCode == 44){
    copyToClipboard();
  }
});
}); 
</script>
<script type="text/javascript"> $(document).ready(function() {
    $(window).keyup(function(e){
      if(e.keyCode == 44){
        $("body").hide();
        window.location.href='logout2.php';
      }

    }); }); 
</script>
</head>

