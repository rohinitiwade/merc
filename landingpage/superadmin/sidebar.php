<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script>
$(document).ready(function(){
    $(".minifybtn").click(function(){
       if($("#page-container").hasClass('page-sidebar-minified')){
       	
	$(".closebtn").show();
}
else{
	$(".closebtn").hide();
}
    });
});
</script>
<style type="text/css">
	.sideMenus1{
	width: 67%;
	float: left;
}
.sideMenus {

    width: 100%;
    float: left;
}
  .sidebar .nav > li > a{
			color: #d1d1d2;
		}
		.sidebar .nav > li > a:hover{
			color: #b7b7b7;
		}
		.sidebar .sub-menu > li > a{
			color: #d1d1d2;
		}
</style>
		<div id="sidebar" class="sidebar">
			<span class="closebtn" >&times;</span>
			<div data-scrollbar="true" data-height="100%">
				
				<ul class="nav">
					<li class="nav-header sideMenus1" id="nat_dash"><span style="font-size: 14px; color: #d1d1d2;">Side Menu</span></li>
					<li class="has-sub sideMenus" id="nat_dash">
						<a href="http://crm.airinfotech.in/telecaller/">
						    <i class="fa fa-dashboard"></i>
						    <span>Dashboard</span>
					    </a>
					</li>					
                    <li class="has-sub sideMenus" id="all_report">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
							<i class="fa fa-file-text-o"></i> 
							<span>Reports</span>
						</a>
						<ul class="sub-menu">
							<li id="new_client_report"><a href="organisation">Organisation List</a></li>
							<li id="allotedclient_report"><a href="individual">Individual List</a></li>
						</ul>
						
					</li>
				
					<li class="has-sub sideMenus" id="client_log"> 
						<a href="loginHistory.php">
							<i class="fa fa-star"></i> 
							<span>Login History</span>
						</a>
					</li>
				
			       <!-- begin sidebar minify button -->
					<li class="sideMenus"><a href="javascript:;" class="sidebar-minify-btn minifybtn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		