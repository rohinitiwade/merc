<?php
$connection_type = 'mysqli';
if ($connection_type == 'mysqli') {
  $con = mysqli_connect("localhost", "crmairin_crm", "VFlB305WHV+j", "crmairin_crm") or die(mysqli_connect_errno());
} else if ($connection_type == 'pdo') {
  try {
    $con = new PDO("mysql:host=localhost;dbname=crmairin_crm", "crmairin_crm", "VFlB305WHV+j");
  } catch(PDOException $e) {
    print "Error!" . $e -> getMessage();
    die();
  }
} else {
  echo "No Database Connection!";
  die();
}

$limit = ( isset($_GET['limit'])) ? $_GET['limit'] : 5;
if (strtolower($limit) == 'all') {
  $limit = 'all';
} else {
  $limit = filter_var($limit, FILTER_SANITIZE_NUMBER_INT);
  if (trim($limit) == '') {
    $limit = 5;
  }
}

$page = ( isset($_GET['page'])) ? $_GET['page'] : 1;
$page = filter_var($page, FILTER_SANITIZE_NUMBER_INT);

$links = ( isset($_GET['links'])) ? $_GET['links'] : 1;
$links = filter_var($links, FILTER_SANITIZE_NUMBER_INT);


require_once 'paginator_class.php';
$query = "SELECT * FROM airinfot_newcustomercontact";
$Paginator = new Paginator($connection_type, $con, $query);


$results = $Paginator -> getData($limit, $page);
?>
<!DOCTYPE html>
<head>
  <title>PHP Pagination</title>
  
  <style type="text/css">
    .pagination {
      margin: 0;
      padding: 0;
      text-align: center;
      width: 100%;
      display: inline-block !important;
    }
    .pagination ul {
      margin: 0;
      padding: 0;
      border: 0;
      outline: 0;
      vertical-align: baseline;
      background: 0 0;
    }
    .pagination li {
      display: inline;
      margin: 0;
      padding: 0;
    }
    .pagination li a {
      border: 1px solid #c9c9c9;
      color: #242424;
      text-decoration: none;
      display: inline-block;
      transition: all .8s ease 0s;
      height: 40px;
      line-height: 40px;
      margin: 0 10px 0 0;
      padding: 0;
      text-align: center;
      width: 40px;
      margin-top: 10px;
    }
    .pagination li a:hover {
      background-color: #4CAF50;
      color: white;
    }
    .pagination li.active a {
      background-color: #4CAF50;
      color: white;
    }
    .pagination li.arrows a {
      background-color: #4C5BAF;
      color: white;
      font-size: 14px;
    }

</style>
<script>
  function changelimit(v) {
    window.location.href = "index.php?limit=" + v + "&page=1";
    return false;
  }
</script>
</head>
<body>
  <div class="container-fluid">
    <br />
      <div class="col-sm-12 text-center">
        <div class="row">
          <a href="https://www.learningoceanteam.com/pagination-in-php" class="btn btn-success">Visit This Tutorial</a>
        </div>
      </div>
    <div class="col-md-12 ">
      
          
      <br /><br />
      <div class="col-md-12">
        <div class="row">
          
          <div class="col-md-6">
            <h1 style="font-size: 20px;font-weight:bold;">PHP Pagination</h1>
          </div>
          <div class="col-md-6 text-right">
            <b>Results Shown:</b> <select onchange="changelimit(this.value)" class="col-sm-2">
            <?php
            $limits = array(5, 10, 15, 20, 25, 50, 100, 'all');
            foreach ($limits as $key => $value) {
              if ($value == $limit) {
                echo '<option selected="selected" value="' . $value . '">' . ucfirst($value) . '</option>';
              } else {
                echo '<option value="' . $value . '">' . ucfirst($value) . '</option>';
              }
            }
            ?>
            
          </select>
          </div>
        </div>
      </div>
      <br />
      <div class="table-responsive">
      <table class="table table-striped  table-bordered table-rounded">
        <thead>
          <tr>
            <th>Sl no.</th>
            <th>customerName</th>
            <th width="20%">Phone</th>
            <th width="20%">City</th>
            <th width="25%">State</th>
          </tr>
        </thead>
        
        <tbody>
          
          
          <?php 
          
          if(count( $results->data )>0)
          {
            if($limit!='all')
            {
              $vs=  (($limit * $page) - $limit) + 1;
            }
            else {
              $vs=1;
            }
            
            for( $i = 0; $i < count( $results->data ); $i++ ) { ?>
                  <tr>
                     <td><?php echo $vs; ?></td>
                          <td><?php echo $results -> data[$i]['customerName']; ?></td>
                          <td><?php echo $results -> data[$i]['phone']; ?></td>
                          <td><?php echo $results -> data[$i]['city']; ?></td>
                          <td><?php echo $results -> data[$i]['state']; ?></td>
                  </tr>
          <?php
          $vs++;
          }
          }
          else {
          echo "<tr><td colspan='5'>No data found</td></tr>";
          }
          ?>
        </tbody>
        <tfoot>
          <tr>
            <th colspan="5">
            <div class="pagination text-center">
              <?php echo $Paginator -> createPaginationLinks($links, 'advanced'); ?> 
            </div>
            </th>
          </tr>
        </tfoot>
      </table>
      </div>
    </div>
  </div>
</body>
</html>
