<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
 
   <title>New Order Form</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- <script src="assets/jquery.min.js"></script>  -->
    <?php include("head.php");?>
   <script>
      $(document).ready(function(){
        $('#forms').addClass('active');
        $('#new_client_form').addClass('active');
      });
   </script> 
<script type="text/javascript">
      $(document).ready(function(){
          $('#country').on('change',function(){
              var countryID = $(this).val();
              if(countryID){
                  $.ajax({
                      type:'POST',
                      url:'countrySubmit.php',
                      data:'country_id='+countryID,
                      success:function(html){
                          $('#state').html(html);
                          $('#city').html('<option value="">Select state first</option>'); 
                      }
                  }); 
              }else{
                  $('#state').html('<option value="">Select country first</option>');
                  $('#city').html('<option value="">Select state first</option>'); 
              }
          });
          $('#state').on('change',function(){
              var stateID = $(this).val();
              if(stateID){
                  $.ajax({
                      type:'POST',
                      url:'countrySubmit.php',
                      data:'state_id='+stateID,
                      success:function(html){
                          $('#city').html(html);
                      }
                  }); 
              }else{
                  $('#city').html('<option value="">Select state first</option>'); 
              }
          });
      });
</script>
   <style>
    label{color:#337ab7!important;font-size:12px!important}.col-md-12{padding:0!important}.payment_new_label{font-size:11px!important}.table>tbody>tr>td{border-top:none!important}input.cheque{border:none}input.error{border:1px solid red;margin-left:10px}.error{text-align:left!important}#updation_search-error{margin-left:80px;margin-top:5px;line-height:28px}.Search_div{width:100%;float:left;margin-bottom:20px}.Search_div label{font-size:14px;line-height:30px}.ms-options-wrap>button:focus,.ms-options-wrap>button{position:relative;border-bottom:1px solid #ccd0d4!important;width:90%;text-align:left;border:none;background-color:#fff;padding:5px 20px 5px 5px;margin-top:0;font-size:13px;color:black;outline-offset:0;white-space:nowrap}.ms-options-wrap>.ms-options{width:90%}#tab1default,#tab2default{margin-top:0}.new_head_title{font-size:16px!important;margin-bottom:20px}.head_reg{margin-bottom:15px;width:100%;float:left;text-align:center;font-size:20px}.register-content .form-control{display:block;width:90%;height:25px;padding:0;border:none;border-bottom:1px solid #ccd0d4;border-radius:0px!important;font-size:12px}.register-buttons{width:12%;margin:3% auto;float:none;text-align:center}.btn-group-lg>.btn,.btn-lg{padding:4px 16px;font-size:15px}.m-b-15{margin-bottom:5px!important}.updation_search_class{width:80%;border-radius:5px;margin-left:15px;padding:10px;border:1px solid #ccc}.wrapper_login{background:linear-gradient(rgba(20,20,20,.5),rgba(20,20,20,.5)),url(img/images.jpg) 100% no-repeat}.reset_filed{margin-left:15px;padding:5px 20px}.register{width:50%}input[type=radio]{margin:0 6px 0px!important}.payment_label{margin:0 15px}.payment_box{color:#fff;padding:20px;display:none;margin-top:20px}.payment_1{display:block}
    #bank_name{padding:4px}.by_cheque{padding:2% 0}.btn-file{position:relative;overflow:hidden}.btn-file input[type=file]{position:absolute;top:0;right:0;min-width:100%;min-height:100%;font-size:100px;text-align:right;filter:alpha(opacity=0);opacity:0;outline:none;s background:white;cursor:inherit;display:block}#bank-lists{position:absolute;width:17%;float:left;list-style:none;margin:0;padding:0;z-index:1}#country-lists{float:left;list-style:none;margin:0;padding:0;width:86%;z-index:1;position:absolute}#country-lists li,#bank-lists li{padding:10px;cursor:pointer;background:#f0f0f0;border-bottom:#bbb9b9 1px solid}#country-lists li:hover{background:#F0F0F0}#town{padding:4px}.btn-file{position:relative;overflow:hidden}.btn-file input[type=file]{position:absolute;top:0;right:0;min-width:100%;min-height:100%;font-size:100px;text-align:right;filter:alpha(opacity=0);opacity:0;outline:none;background:white;cursor:inherit;display:block}#img-upload{width:100%}</style><style type="text/css">.btn-bs-file{position:relative}.btn-bs-file input[type="file"]{position:absolute;top:-9999999;filter:alpha(opacity=0);opacity:0;width:0;height:0;outline:none;cursor:inherit}.error {
  color: red !important;
    text-align: left !important;
    }
     input[type=text] {
    color:gray !important;
   }
   
</style>
   <!-- <script src="ajax/countyState.js"></script> -->
   <!-- <script src="ajax/CheckData45.js"></script> -->
   <script>
        $(function() {
            $('#other_field').hide(); 
            $('#source').change(function(){
                if($('#source').val() == 'other') {
                    $('#other_field').show(); 
                } else {
                    $('#other_field').hide(); 
                } 
            });
        });
   </script>  
   <?php include_once("header.php"); ?>
   <?php include_once("sidebar.php"); ?>  
   <!-- begin #content -->
<script src="ajax/countyState.js"></script>
   <div id="content" class="content">
      <!-- begin breadcrumb -->
      <ol class="breadcrumb" style="width: 100%;">
         <li><a href="http://crm.airinfotech.in/telecaller/">Home</a></li>
         <li><a href="http://crm.airinfotech.in/telecaller/">Dashboard</a></li>
         <li class="active" style="color: green;">New Client</li>
      </ol>
      <!-- end breadcrumb -->
      <div class="details">
         <h3 class="col-sm-8" style="margin-top: 0px; color: green;"><span>New Client Form</span>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://crm.airinfotech.in/telecaller/" class="btn btn-warning btn-xs">Back To Home Page</a>
          &nbsp;&nbsp;&nbsp;</h3>
         <?php include("timezone.php");?>
        <hr style="float: left;width: 100%;">
         <?php if(isset($_POST['new_client'])){
            extract($_POST);
            $date = date("d-m-Y");
            $time = date("H:i:s");
            $date_time = date("d-m-Y H:i:s");           
            $inseertname = mysql_query("INSERT INTO `airinfot_newcustomermaster`(`name`,`last_name`,`ex_id`,`entrydate`)VALUES('".$name."','".$last_name."','".$_SESSION['user_id']."','".$date_time."')");
            $lastId = mysql_insert_id();
            $satee = mysql_query("SELECT * FROM `states` WHERE `state_id`='".$state_name."'");
            $selSatte = mysql_fetch_array($satee);
            $cityname = mysql_query("SELECT * FROM `cities` WHERE `city_id`='".$city."'");
            $selcity =mysql_fetch_array($cityname);
            $insertdata = mysql_query("INSERT INTO `airinfot_newcustomercontact`(`custid`,`name`,`last_name`,`email`,`personal_mobile`,`personal_phone`,`work_city`,`work_state`,`personal_street`,`personal_city`,`personal_state`,`pincode`,`new_Product`,`ex_id`,`cust_status`)VALUES('".$lastId."','".$name."','".$last_name."','".$email."','".$mobile."','".$phone_no."','".$selcity['city_name']."','".$selSatte['state_name']."','".addslashes($address)."','".$selcity['city_name']."','".$selSatte['state_name']."','".$pincode."','".$new_Product."','".$_SESSION['user_id']."','New Client')");             
            $insert = mysql_query("INSERT INTO `telecaller_client`(`name`,`last_name`,`email`,`mobile`,`phone_no`,`address`,`pin_code`,`state`,`city`,`district`,`source`,`other`,`date`,`time`,`date_time`,`status`,`ex_id`,`clientid`)VALUES('".$name."','".$last_name."','".$email."','".$mobile."','".$phone_no."','".addslashes($address)."','".$pincode."','".$state_name."','".$city."','".$town."','".$source."','".$other."','".$date."','".$time."','".$date_time."','".$status."','".$_SESSION['user_id']."','".$lastId ."')");
            if($insert){?>
               <script type="text/javascript">
                 alert("Client Added Successfully --!");
                 window.location.href='new_client_list.php';
               </script>
              <?php }
         }?>
         <div class="col-md-12">
                    <div class="right-content">
                      <!-- begin register-content -->
                      <div class="register-content">
                        <form action="" method="POST" enctype="multipart/form-data" id="myform1">
                          <div class="new_head_title" style="color: green;"><b>Basic Information</b></div>
                          <hr>
                          <div class="row row-space-10">
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label"> First Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="name" id="name" value="<?php echo $_POST['name'];?>" onkeyup="cust_name()"   autocomplete="OFF"/>
                              <div style="color:red; font-size:12px; text-align:left" id="error_fname"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Last Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="last_name" value="<?php echo $_POST['last_name'];?>" id="lname" onkeyup="cust_name()"  autocomplete="OFF"/>
                              <div style="color:red; font-size:12px; text-align:left" id="error_lname"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Email<span class="text-danger">*</span></label>
                              <input type="text" class="form-control"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" id="email" name="email" value="<?php echo $_POST['email'];?>" autocomplete="OFF"/>
                              <div style="color:red; font-size:12px; text-align:left" id="error_email"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Mobile No. <span class="text-danger">*</span></label>
                              <input type="text" class="form-control"  id="mobile" onkeypress="return isNumber(event)" maxlength="10" name="mobile" value="<?php echo $_POST['mobile'];?>" autocomplete="OFF">
                              <div style="color:red; font-size:12px; text-align:left" id="error_mobile"></div>
                            </div>
                          </div>
                          <div class="row row-space-10">                            
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Phone No. <span class="text-danger"></span></label>
                              <input type="text" class="form-control"  id="rphone" maxlength="12"  name="phone_no" value="<?php echo $_POST['phone_no'];?>" autocomplete="OFF"/>
                              <div style="color:red; font-size:12px; text-align:left" id="error_phone(r)"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Address<span class="text-danger"></span></label>
                              <textarea class="form-control" rows="3" id="add" name="address" autocomplete="OFF"><?php echo $_POST['address'];?></textarea>
                              <div style="color:red; font-size:12px; text-align:left" id="error_add"></div>
                            </div>                       
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Pin Code<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="pincode" value="<?php echo $_POST['pincode'];?>" autocomplete="OFF"/>
                              <div style="color:red; font-size:12px; text-align:left" id="error_pin"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Select State<span class="text-danger"></span></label>
                              <?php $query = mysql_query("SELECT * FROM states WHERE country_id = 100 ORDER BY state_name ASC");
                                   //Count total number of rows
                                   $rowCount = mysql_num_rows($query);?>
                              <select class="form-control" id="state" name="state_name" autocomplete="OFF">
                                 <option value="<?php echo $_POST['state_name'];?>">Select State</option>
                                 <?php
                                    if($rowCount > 0){
                                    while($row = mysql_fetch_assoc($query)){ 
                                            echo '<option value="'.$row['state_id'].'">'.$row['state_name'].'</option>';
                                        }
                                    }else{
                                        echo '<option value="">Country not available</option>';
                                    }?>
                              </select>
                              <div style="color:red; font-size:12px; text-align:left" id="error_state"></div>
                            </div>
                            </div>
                          <div class="row row-space-10">
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Select District<span class="text-danger"></span></label>
                              <select class="form-control" id="city" name="city" value="<?php echo $_POST['city'];?>" autocomplete="OFF">
                                 <option value="">---Select District first---</option>
                              </select>
                              <div style="color:red; font-size:12px; text-align:left" id="error_city"></div>
                            </div>

                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Enter City/Town<span class="text-danger">*</span></label>
                              <input type="text" class="form-control email_check"  id="town" name="town" value="<?php echo $_POST['town'];?>" autocomplete="OFF" placeholder="Enter Town / City" />
                                  <div id="suggesstion-town"></div>
                            </div>                            
                             <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label" for="source">Source<span class="text-danger"></span></label>                      
                              <select class="form-control" id="source" name="source">
                                <option value="">----Select Source-----</option>
                                <option value="Facebook">Facebook</option>
                                <option value="WhatsApp">WhatsApp</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Google">Google</option>
                                <option value="Gmail">Gmail</option>
                                <option value="Instagram">Instagram</option>
                                <option value="Pinterest">Pinterest</option>
                                <option value="other" id="admOption">other</option>
                              </select>
                              </div>
                            </div>
                              <div class="col-sm-3 col-xs-6 m-b-15" id="other_field">
                              <label class="control-label">Other<span class="text-danger">*</span></label>
                              <input type="text" class="form-control email_check"  id="other" name="other" value="" autocomplete="OFF" placeholder="Enter other source" />                             
                            </div>                       
                          <div class="register-buttons">
                              <input type="submit" class="btn btn-primary" id="" value="Submit" name="new_client">
                          </div>
                        
                      </div>
                      </div> 
                    </div>
                    <hr>
                  </div>
              </form>
          </div>
        </div>
      </div>                  
    </div>
</div>
   </div>              
   <!-- ================== BEGIN BASE JS ================== -->
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

$( "#myform1" ).validate({
  rules: {
    name: {
      required: true,
      minlength: 1
    },
    last_name: {
      required: true,
      minlength: 1
    },
    email: {
      required: true,
      email: true
    },
    mobile:
    {
      required: true,
      number: true ,
      minlength:10,
      maxlength:10
    },
    
    pincode:
    {
      required: true,
      number: true,
      minlength:6,
      maxlength:6
    }
  }
});
</script>

<!-- <script>

$( "#myform1" ).validate({
  rules: {
    name: {
      required: true,
      minlength: 2
    },
    last_name: {
      required: true,
      minlength: 2
    },
    email: {
      required: true,
      email: true
    },
    mobile:
    {
      minlength:10,
       maxlength:10,
      required: true,
      number: true
    },
    rphone1:
    {
      required: true,
      number: true
    },
     pincode:
    {
      minlength: 6,
      maxlength: 6,
      // required: true,
      number: true
    }
  }
});
</script> -->
<script>
$(document).ready(function(){
  $("#town").keyup(function(){
    $.ajax({
    type: "POST",
    url: "townSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#town").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      $("#suggesstion-town").show();
      $("#suggesstion-town").html(data);
      $("#town").css("background","#FFF");
    }
    });
  });
});
function selecttown(val) {
$("#town").val(val);
$("#suggesstion-town").hide();
}
</script>
<script>
$(document).ready(function(){
  $("#bank_name").keyup(function(){
    $.ajax({
    type: "POST",
    url: "banknameSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      $("#suggesstion-bank").show();
      $("#suggesstion-bank").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbank(val) {
$("#bank_name").val(val);
$("#suggesstion-bank").hide();
}
$(document).ready(function(){
  $("#branch_name").keyup(function(){
    var bank = $("#bank_name").val();
    $.ajax({
    type: "POST",
    url: "branchSearch.php",
    data:'keyword='+$(this).val() +'&bank='+bank,
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      $("#suggesstion-branch").show();
      $("#suggesstion-branch").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbranch(val) { 
  var branch = val;
  var bank = $("#bank_name").val();
    $.ajax({
            type:'POST',
            url:'ifscSearch.php',
            data:'branchname='+branch + '&bank='+bank,
            success:function(data){
                  /*alert(data);*/
                  $("#ifsc_code").val(data);           
                }
            }); 
  $("#branch_name").val(val);
  $("#suggesstion-branch").hide();}
</script>
<script type="text/javascript">
$(document).ready(function(){
      $("#displays").keyup(function(){
          $.ajax({
          type: "POST",
          url: "displays.php",
          data:'clientSearch='+$(this).val(),
          beforeSend: function(){
          $("#displays").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
          },
          success: function(data){
              $("#client-box").show();
              $("#client-box").html(data);
              $("#displays").css("background","#FFF");
              $('#DivHide').hide();
          }
          });
      });
  });
  $(document).ready( function() {
      $(document).on('change', '.btn-file :file', function() {
    var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
    });
    $('.btn-file :file').on('fileselect', function(event, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = label;
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imgInp").change(function(){
        readURL(this);
    });   
  });
</script>
 <?php include("footer.php");?>
 
   </body>
</html>
