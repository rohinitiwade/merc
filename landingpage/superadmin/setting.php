<?php error_reporting(0);?>
<head>
    <?php include("head.php");?>
</head>
<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>  
 <script>
    $(document).ready(function() {
    $('#data-tableview').DataTable();
} );
  $(document).ready(function(){
    $("#all_report").addClass("active");
    $("#sms_getway").addClass("active");
  });
</script>
<div id="content" class="content">
            <ol class="breadcrumb" style="width: 100%;">
                <li><a href="index.php">Home</a></li>
                <li class="active"><a href="index.php">Dashboard</a></li>
                <li class="active"><a href="#">Setting Page</a></li>
            </ol>
            <div class="details">
  <h3 class="col-md-8" style="margin-top: 0px; color: green;">Setting Page &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.php" class="btn btn-primary btn-xs">Back To Home Page</a></h3>
  <?php include("timezone.php");?>
  <hr style="float: left;width: 100%;">
 <div class="col-md-12">
    <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th style="background-color: #275FA9; color: white;">Sr.No.</th>
        <th style="background-color: #275FA9; color: white;">Organisation User</th>
        <th style="background-color: #275FA9; color: white;">Individual</th>
        <!-- <th style="background-color: #275FA9; color: white;">Validity</th>
        <th style="background-color: #275FA9; color: white;">Username</th>
        <th style="background-color: #275FA9; color: white;">Password</th>
        <th style="background-color: #275FA9; color: white;">Login Url</th> -->
      </tr>
    </thead>
    <tbody> 
            <tr>
            <td>1</td>
            <td><b><?php $organisation = mysqli_query($db, "SELECT COUNT(reg_id) As organisation FROM `law_registration` WHERE `account`='Organisation'");
                     $selorganisation = mysqli_fetch_array($organisation); echo $selorganisation['organisation'];?></b></td>
            <td><?php $Individual = mysqli_query($db, "SELECT COUNT(reg_id) As Individual FROM `law_registration` WHERE `account`='Individual'");
                     $seIndividual = mysqli_fetch_array($Individual); echo $seIndividual['Individual'];?></td>
            <!-- <td><?php echo $validity;?></td>
            <td><?php echo $smsusername;?></td>
            <td><?php echo $smspassword;?></td> -->
            <!-- <td><a href="https://login.bulksmsgateway.in/" target="_blank"><b>Click to Login</b></a></td> -->
        </tr>
    </tbody>
    </table>
  </div>
  <div class="container">
  <div class="row">
        <div class="col-lg-12 col-md-5 col-sm-8 col-xs-9 bhoechie-tab-container">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center">
                  <h4 class="glyphicon glyphicon-envelope"></h4><br/>Trial Period
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-envelope"></h4><br/>Expired User
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-envelope"></h4><br/>Active User
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-envelope"></h4><br/>Suspended User
                </a>
                <a href="#" class="list-group-item text-center">
                  <h4 class="glyphicon glyphicon-envelope"></h4><br/>AIRINF SMS
                </a>
              </div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
            <div class="bhoechie-tab-content active">
                <h3 style="margin-top: 0;color:#55518a">Trial Period</h3>
                   
  <div class="col-md-">
    <form>
  <div class="form-group">
    <label for="exampleInputEmail1">Select Trial Period</label>
    <select class="form-control" name="trial_period" required="">
      <option value="<?php echo $_POST['trial_period'];?>"><?php if($_POST['trial_period']!=""){ echo $_POST['trial_period']; }else{?>--Select Trial Period--<?php } ?></option>
      <option value="10">10 Day</option>
      <option value="15">15 Day</option>
      <option value="25">25 Day</option>
      <option value="30">30 Day</option>
    </select>
    <small id="emailHelp" class="form-text text-muted">Select trial Period.</small>
  </div>
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
  </div></h3>
      </center>
        </div>
        <div class="bhoechie-tab-content">
    <h3 style="margin-top: 0;color:#55518a">Online SMS</h3>
        <div class="col-md-">
    <table id="data-online" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th style="background-color: #008000; color: white;">Sr.No.</th>
        <th style="background-color: #008000; color: white;">Send From</th>
        <th style="background-color: #008000; color: white;">Mobile Number</th>
        <th style="background-color: #008000; color: white;">Send Date</th>
        <th style="background-color: #008000; color: white;">Message</th>
        <th style="background-color: #008000; color: white;">Status</th>
      </tr>
    </thead>
    <tbody>
    <tr>
    <td><?php  ?></td>
    <td><b><?php echo $selreg1['name'];?>  <?php echo $selreg1['last_name'];?></b></td>
    <td><?php echo $sentsms1['mobile_number'];?></td>
    <td><?php echo $sentsms1['date_time'];?></td>
    <td><?php echo $sentsms1['sms'];?></td>
    <td><?php echo $sentsms1['status'];?></td>
</tr>
    </tbody>
    </table>
  </div>
</div>
<div class="bhoechie-tab-content">
<center>
      <h3 style="margin-top: 0;color:#55518a">Shopping Cart SMS</h3>
      <div class="col-md-">
    <table id="data-shoppingdvd" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th style="background-color: #FB9517; color: white;">Sr.No.</th>
        <th style="background-color: #FB9517; color: white;">Send From</th>
        <th style="background-color: #FB9517; color: white;">Mobile Number</th>
        <th style="background-color: #FB9517; color: white;">Send Date</th>
        <th style="background-color: #FB9517; color: white;">Message</th>
        <th style="background-color: #FB9517; color: white;">Status</th>
      </tr>
    </thead>
    <tbody>
    
    <tr>
    <td><?php  ?></td>
    <td><b><?php echo $selreg1['name'];?>  <?php echo $selreg1['last_name'];?></b></td>
    <td><?php echo $sentsms1['mobile_number'];?></td>
    <td><?php echo $sentsms1['date_time'];?></td>
    <td><?php echo $sentsms1['sms'];?></td>
    <td><?php echo $sentsms1['status'];?></td>
</tr>
    </tbody>
    </table>
  </div>
        </center>
    </div>
    <div class="bhoechie-tab-content">
       <center>
      <h3 style="margin-top: 0;color:#55518a">AIR DVD SMS</h3>
      <div class="col-md-">
    <table id="data-shopping" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th style="background-color: red; color: white;">Sr.No.</th>
        <th style="background-color: red; color: white;">Send From</th>
        <th style="background-color: red; color: white;">Mobile Number</th>
        <th style="background-color: red; color: white;">Send Date</th>
        <th style="background-color: red; color: white;">Message</th>
        <th style="background-color: red; color: white;">Status</th>
      </tr>
    </thead>
    <tbody>
    
    <tr>
    <td><?php  ?></td>
    <td><b><?php echo $selreg1['name'];?>  <?php echo $selreg1['last_name'];?></b></td>
    <td><?php echo $sentsms1['mobile_number'];?></td>
    <td><?php echo $sentsms1['date_time'];?></td>
    <td><?php echo $sentsms1['sms'];?></td>
    <td><?php echo $sentsms1['status'];?></td>
</tr>
    </tbody>
    </table>
  </div>
        </center>
    </div>
    <div class="bhoechie-tab-content">
        <center>
      <h3 style="margin-top: 0;color:#55518a">AIRINF SMS</h3>
      <div class="col-md-">
    <table id="data-shoppingsms" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th style="background-color: purple; color: white;">Sr.No.</th>
        <th style="background-color: purple; color: white;">Send From</th>
        <th style="background-color: purple; color: white;">Mobile Number</th>
        <th style="background-color: purple; color: white;">Send Date</th>
        <th style="background-color: purple; color: white;">Message</th>
        <th style="background-color: purple; color: white;">Status</th>
      </tr>
    </thead>
    <tbody>
    
    <tr>
    <td><?php  ?></td>
    <td><b><?php echo $selreg1['name'];?>  <?php echo $selreg1['last_name'];?></b></td>
    <td><?php echo $sentsms1['mobile_number'];?></td>
    <td><?php echo $sentsms1['date_time'];?></td>
    <td><?php echo $sentsms1['sms'];?></td>
    <td><?php echo $sentsms1['status'];?></td>
</tr>
    </tbody>
    </table>
  </div>
        </center>
    </div>
</div>
</div>
  </div>
</div>
  <hr/>
  </div>
  </div>

<style>
  div.bhoechie-tab-container{z-index:10;background-color:#fff;padding:0!important;border-radius:4px;-moz-border-radius:4px;border:1px solid #ddd;margin-top:20px;margin-left:0;-webkit-box-shadow:0 6px 12px rgba(0,0,0,.175);box-shadow:0 6px 12px rgba(0,0,0,.175);-moz-box-shadow:0 6px 12px rgba(0,0,0,.175);background-clip:padding-box;opacity:.97;filter:alpha(opacity=97)}div.bhoechie-tab-menu{padding-right:0;padding-left:0;padding-bottom:0}div.bhoechie-tab-menu div.list-group,div.bhoechie-tab-menu div.list-group>a{margin-bottom:0}div.bhoechie-tab-menu div.list-group>a .fa,div.bhoechie-tab-menu div.list-group>a .glyphicon{color:#5A55A3}div.bhoechie-tab-menu div.list-group>a:first-child{border-top-right-radius:0;-moz-border-top-right-radius:0}div.bhoechie-tab-menu div.list-group>a:last-child{border-bottom-right-radius:0;-moz-border-bottom-right-radius:0}div.bhoechie-tab-menu div.list-group>a.active,div.bhoechie-tab-menu div.list-group>a.active .fa,div.bhoechie-tab-menu div.list-group>a.active .glyphicon{background-color:#5A55A3;background-image:#5A55A3;color:#fff}div.bhoechie-tab-menu div.list-group>a.active:after{content:'';position:absolute;left:100%;top:50%;margin-top:-13px;border-bottom:13px solid transparent;border-top:13px solid transparent;border-left:10px solid #5A55A3}div.bhoechie-tab-content{background-color:#fff;padding-left:20px;padding-top:10px}div.bhoechie-tab div.bhoechie-tab-content:not(.active){display:none}
</style>
<script>
  $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>
<script>
    function viewProfile(id) {
      var id =+id;
      //var approved =+ids;
      //alert(id);
      $.ajax({
        type:"post",
        url:"ajax/view_profile.php",
        data:"name="+id,
        success:function(data){
         // alert(data);
        $("#viewProfile").html(data);
      }

   });

    }
  </script>
 <?php include("footer.php");?>