<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
   <title>Updation Order Form</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" type="text/css" href="css/style.css">
   <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
   <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="https://rawgit.com/nobleclem/jQuery-MultiSelect/master/jquery.multiselect.css" />
   <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
 
   <link href="assets/css/style.min.css" rel="stylesheet" />
   <script src="https://rawgit.com/nobleclem/jQuery-MultiSelect/master/jquery.multiselect.js"></script>
   <style>
    label {
   color: #337ab7 !important;
font-size: 12px !important;
}
.col-md-12
{
  padding: 0 !important;
}
select.form-control
{
  border:1px solid #ccc !important;
}
.payment_new_label
{
  font-size: 11px !important;

}
.table>tbody>tr>td
{
  border-top: none !important;
}
input.cheque {
    border: none;
    }
input.error {
    border: 1px solid red;
    margin-left: 10px;
}
.error {
    text-align: left !important;
    }
    #updation_search-error
    {
          margin-left: 80px;
    margin-top: 5px;
    line-height: 28px;
    }
.Search_div
{
width: 100%;
float: left;
margin-bottom: 20px;
}
.Search_div label
{
  font-size: 14px;
      line-height: 30px;
}
.ms-options-wrap > button:focus, .ms-options-wrap > button {
    position: relative;
        border-bottom: 1px solid #ccd0d4 !important;
    width: 90%;
    text-align: left;
    border: none;
    background-color: #fff;
    padding: 5px 20px 5px 5px;
    margin-top: 0px;
    font-size: 13px;
    color: black;
    outline-offset: 0px;
    white-space: nowrap;
}
.ms-options-wrap > .ms-options {
    width: 90%;
  }
#tab1default,#tab2default
{
  margin-top: 0px;
}
.new_head_title
{
font-size: 16px !important;
margin-bottom: 20px;
}
.head_reg
{
    margin-bottom: 15px;
    width: 100%;
    float: left;
    text-align: center;
    font-size: 20px;
}
.register-content .form-control {
    display: block;
    width: 90%;
    height: 25px;
    padding: 0px;
    border: none;
    border-bottom: 1px solid #ccd0d4;
    border-radius: 0px !important;
    font-size: 12px;
    }
    .register-buttons {
    width: 12%;
    margin: 3% auto;
    float: none;
    text-align: center;
}
.btn-group-lg>.btn, .btn-lg {
    padding: 4px 16px;
    font-size: 15px;
    }
    .m-b-15 {
    margin-bottom: 5px!important;
}
.updation_search_class
{
     width: 80%;
    border-radius: 5px;
    margin-left: 15px;
    padding: 10px;
    border: 1px solid #ccc;
}
      .wrapper_login
{
background: linear-gradient(
                     rgba(20,20,20, .5), 
                     rgba(20,20,20, .5)),
                     url('img/images.jpg') 100% no-repeat;

} 
.reset_filed
{
  margin-left: 15px;
  padding: 5px 20px;
}
.register {
    width: 50%;
    }
    input[type=radio] {
    margin: 0px 6px 0px !important;
  }
    .payment_div
    {
    }
    .payment_label
    {margin: 0px 15px;
    }
     .payment_box{
        color: #fff;
        padding: 20px;
        display: none;
        margin-top: 20px;
    }
    .payment_1
    {
      display: block;
    }
</style>

<script type="text/javascript">
$(document).ready(function(){
  var t = 0;
var i = $('.orderform_table tr').length;
$(".add-row").on('click',function(){
  html = '<tr>';
  html += '<td style="vertical-align: text-bottom;"><input type="checkbox" name="record"/></td>';
  html += '<td><label class="control-label payment_new_label">Cheque No</label><input name="che_no[]" type="text" class="form-control" id="che_no_'+i+'" placeholder="Cheque No"  autocomplete="OFF"></td>';
  html += '<td style="width: 200px;"><label class="control-label payment_new_label">Bank Name</label><input name="bank_name[]" type="text" class="form-control" id="bank_name_'+i+'" placeholder="Bank Name"  autocomplete="OFF"><div id="suggesstion-bank_'+i+'"></div></td>';
  html += '<td style="width: 200px;"><label class="control-label payment_new_label">Branch Name</label><input name="branch_name[]" type="text" class="form-control" id="bank_name_'+i+'" placeholder="Branch Name"  autocomplete="OFF"><div id="suggesstion-branch'+i+'"></div></td>';
  html += '<td> <label class="control-label payment_new_label">IFSC Code</label> <input name="ifsc_code[]" type="text" class="form-control" id="ifsc_code_'+i+'" placeholder="IFSC Code"  autocomplete="OFF"><div id="suggesstion-ifc_'+i+'"></div> </td>';
  html += '<td> <label class="control-label payment_new_label">Amount Received</label> <input name="cashamt[]" type="text" class="form-control amount_recieve" id="cash_amt_'+i+'" placeholder="Amount Received" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"  autocomplete="OFF"> </td>';
  html += '<td> <label class="control-label payment_new_label">PDC Date</label> <input type="date" name="next_due_date[]" class="form-control" id="next_due_date_'+i+'" placeholder="Cheque Date" id="datepicker" autocomplete="OFF"> </td>';
  html += '<td> <div class="form-group"> <label>Cheque Image</label> <input type="file" class="form-control" id="cheque_'+i+'" name="cheque[]"></div></td>';
  html += '</tr>';
  i++;
  t++;

  $('.orderform_table').append(html);
  //alert(t);
  
  $("#bank_name_"+t).keyup(function(){
    //alert("OK");
    //debugger;
    $.ajax({
    type: "POST",
    url: "banknameSearch1.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#bank_name_"+t).css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      $("#suggesstion-bank_"+t).show();
      $("#suggesstion-bank_"+t).html(data);
      $("#bank_name_"+t).css("background","#FFF");
      /*selectbankname(<?php echo $country["bank_name"]; ?>);*/
      }
    });
  });
$(document).on("click","#bank_id",function(){
  //var name = document.getElementById("selectbankname").value;
  var name =$("#bank_id").text();
  alert(name);
  $("#bank_name_"+t).val(name);
  $("#suggesstion-bank_"+t).hide();
});
  $("#branch_name_"+t).keyup(function(){
    //alert("OK");
    var bank = $("#bank_name_"+t).val();
    //alert(bank);
    $.ajax({
    type: "POST",
    url: "branchSearch.php",
    data:'keyword='+$(this).val() +'&bank='+bank,
    beforeSend: function(){
      $("#bank_name_"+t).css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
     //alert(data);
      $("#suggesstion-branch_"+t).show();
      $("#suggesstion-branch_"+t).html(data);
      $("#bank_name_"+t).css("background","#FFF");
    }
    });
  });
function selectbranch(val) { 
  var branch = val;
  var bank = $("#bank_name_"+t).val();
    $.ajax({
            type:'POST',
            url:'ifscSearch.php',
            data:'branchname='+branch + '&bank='+bank,
            success:function(data){
                  /*alert(data);*/
                  $("#ifsc_code_"+t).val(data);           
                }
            }); 

  //alert(branch);
  $("#branch_name_"+t).val(val);
  $("#suggesstion-branch_"+t).hide();
}
});
// Find and remove selected table rows
  $(".delete-row").click(function(){
    $(".orderform_table").find('input[name="record"]').each(function(){
    if($(this).is(":checked")){
    $(this).parents("tr").remove();
    calculation();
    }
  });
});
}); 
</script>
<script>
 $(document).on("change keyup blur", ".amount_recieve1", function() {
      calculation();
  });
  function calculation(){
    debugger;
   var sum = 0;
    $(".amount_recieve1").each(function(){
        sum += +$(this).val();
    });
    $("#subTotal1").val(sum); 
    $(document).on("change keyup blur onload click", function(){ 
      var getTotal= $("#amount1").val();
      var getRemainingAmount = $("#subTotal1").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount1").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount1").val('');
        }
   }); 
  }  
</script>
   <script src="ajax/countyState.js"></script>
   <script src="ajax/CheckData45.js"></script>
   <script>
     $(function() {
  // from http://stackoverflow.com/questions/45888/what-is-the-most-efficient-way-to-sort-an-html-selects-options-by-value-while
  var my_options = $('.facilities select option');
  var selected = $('.facilities').find('select').val();
  my_options.sort(function(a,b) {
    if (a.text > b.text) return 1;
    if (a.text < b.text) return -1;
    return 0
  })
  $('.facilities').find('select').empty().append( my_options );
  $('.facilities').find('select').val(selected);
  // set it to multiple
  $('.facilities').find('select').attr('multiple', true);
  // remove all option
  $('.facilities').find('select option[value=""]').remove();
  // add multiple select checkbox feature.
  $('.facilities').find('select').multiselect();
})
   </script>
   <script>
      $(document).ready(function(){
        $('#forms').addClass('active');
        $('#order_form_side').addClass('active');
      });
   </script>
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
   <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />
   <script type="text/javascript">
      $(document).ready(function(){
          $("#name").autocomplete({
              source:'autocomplete.php',
              minLength:1
          });
      });
   </script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".payment_box").not(targetBox).hide();
        $(targetBox).show();
    });
});
</script>
   <?php include_once("header.php"); ?>
   <?php include_once("sidebar.php"); ?>  
   <!-- begin #content -->
<script>
 $(document).on("change keyup blur", ".amount_recieve2", function() {
      calculation();
  });

  function calculation(){
    debugger;       
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount2").val();
      var getRemainingAmount = $("#subTotal2").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount2").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount2").val('');
        }
   }); 
  }
    
    
</script>
   <div id="content" class="content">
      <!-- begin breadcrumb -->
      <ol class="breadcrumb" style="width: 100%;">
         <li><a href="index.php">Home</a></li>
         <li><a href="index.php">Dashboard</a></li>
         <li class="active" style="color: green;">Order Form</li>
      </ol>
      <!-- end breadcrumb -->
      <div class="details">
          <h3 class="col-md-8" style="margin-top: 0px; color: green;">New Order Form
          &nbsp;&nbsp;&nbsp;<a href="exportORDERdata.php" class="btn btn-primary btn-xs">Back to Home Page</a>
        <a href="order_form_reports.php" class="btn btn-primary btn-xs">Order Form Report</a></h3>
         <?php include("timezone.php");?>
        <hr style="float: left;width: 100%;">

         <div class="col-md-12">
            <div class="panel with-nav-tabs panel-default">
             
                <ul class="nav nav-tabs">
                  <!-- <li class="active"><a href="#tab1default" data-toggle="tab">New Order Form</a></li> -->
                  <li class="active"><a href="#tab2default" data-toggle="tab">Updation</a></li>
                </ul>
              
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="tab1default">
                    <div class="right-content">
                      <!-- begin register-content -->
                      <div class="register-content">
                        <form action="submit.php?flag=invoice" method="POST" enctype="multipart/form-data" id="myform1">
                          <div class="new_head_title" style="color: green;"><b>Basic Information</b></div>
                          <hr>
                          <div class="row row-space-10">
                            <div class="col-md-3 m-b-15">
                              <label class="control-label"> First Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="name" id="name" value="<?php echo $_POST['name'];?>" onkeyup="cust_name()"   autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_fname"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Last Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="last_name" value="<?php echo $_POST['last_name'];?>" id="lname" onkeyup="cust_name()"  autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_lname"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Email<span class="text-danger">*</span></label>
                              <input type="text" class="form-control email_check"  id="email" name="email" value="<?php echo $_POST['email'];?>"  autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_email"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Mobile No. <span class="text-danger">*</span></label>
                              <input type="text" class="form-control"  id="mobile" onkeypress="return isNumber(event)" maxlength="10" name="mobile" value="<?php echo $_POST['mobile'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_mobile"></div>
                            </div>
                          </div>

                          <div class="row row-space-10">
                            
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Phone No. <span class="text-danger"></span></label>
                              <input type="text" class="form-control"  id="rphone" maxlength="10"  name="rphone" value="<?php echo $_POST['rphone'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_phone(r)"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Address<span class="text-danger">*</span></label>
                              <textarea class="form-control" rows="3" id="add" name="address" autocomplete="OFF"><?php echo $_POST['address'];?></textarea>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_add"></div>
                            </div>
                        
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Pin Code<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="pincode" value="<?php echo $_POST['pincode'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_pin"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Select State<span class="text-danger">*</span></label>
                              <?php 
                                 $query = mysql_query("SELECT * FROM states WHERE country_id = 100 ORDER BY state_name ASC");
                                   //Count total number of rows
                                   $rowCount = mysql_num_rows($query);?>
                              <select class="form-control" id="state" name="state_name" autocomplete="OFF">
                                 <option value="<?php echo $_POST['state_name'];?>">Select State</option>
                                 <?php
                                    if($rowCount > 0){
                                        while($row = mysql_fetch_assoc($query)){ 
                                            echo '<option value="'.$row['state_id'].'">'.$row['state_name'].'</option>';
                                        }
                                    }else{
                                        echo '<option value="">Country not available</option>';
                                    }
                                    ?>
                              </select>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_state"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Select District<span class="text-danger">*</span></label>
                              <select class="form-control" id="city" name="city" value="<?php echo $_POST['city'];?>" autocomplete="OFF">
                                 <option value="">---Select State first---</option>
                              </select>
                            
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Enter City/Town<span class="text-danger">*</span></label>
                              <input type="text" class="form-control"  id="" name="town" value="<?php echo $_POST['town'];?>" autocomplete="OFF" placeholder="Enter Town / City" autocomplete="OFF"/>
                                  <!-- <div id="suggesstion-town"></div> -->
                            </div>
                            
                             <div class="col-md-3 m-b-15">
                             <!--  <label class="control-label">Add Order Form<span class="text-danger">*</span></label><br>
                              <label class="btn-bs-file btn btn-xs btn-warning">Order Form Image
                                <input type="file"  name="orderform" class="form-control" />
                              </label> -->
                              <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <span class="btn-bs-file btn btn-xs btn-warning btn-file">
                                       Upload Order Form Image<input type="file" id="imgInp" name="orderform" value="">
                                    </span>
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                            <img id='img-upload'/>
                        </div>
                            </div>
                          </div>
                           <div class="new_head_title" style="color: green;"><b>Product Details</b></div>
                           <hr>
                          <div class="row row-space-10">
                              <div class="col-md-3 m-b-15">
                                <label class="control-label">Business Associate Name</label>
                                <?php 
                                   $sql= mysql_query("select * from `registration` where `id`='".$_SESSION['user_id']."'");
                                              $row = mysql_fetch_array($sql);?>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $row['name']; ?> <?php echo $row['last_name']; ?>" readonly>
                              </div>
                              <div class="col-md-3 m-b-15">
                                  <label class="control-label">Dongal Type</label>
                                  <select class="form-control" name="dongletype_0" value="<?php echo $_POST['dongletype_0'];?>"  placeholder="Dongle Type" autocomplete="OFF">
                                  <option value="">Select Type</option>
                                  <option value="Individual">Individual</option>
                                  <option value="Network">Network</option>
                                  <option value="Scheme">Scheme</option>
                                  <option value="Bulk">Bulk</option>
                                  <option value="Complimentary">Complimentary</option>
                                  </select>
                              </div>
                              <div class="col-md-3 m-b-15">
                                <label class="control-label">Select Product</label>
                               <!--  <?php
                                   $qry = "select CONCAT(dtm.type,' + ',pm.name) as title,pm.productid from product pm, dvdtitle_type_master dtm where dtm.titleid = pm.titleid and pm.name!='ALL' order by dtm.type,pm.name";
                                   $result = mysql_query($qry) or die(mysql_error());
                                   
                                   $dropdown = "<div class='facilities'><span class='input'><select name='product_0[]' id='product_0' class='form-control'>";
                                   $dropdown .= '<option value="" onclick="alert(\'Please select product!\');">Select Product</option>';
                                     while($row = mysql_fetch_array($result))
                                   {
                                       $dropdown .= "\r\n<option value ='{$row['title']}'> {$row['title']} </option>";
                                     }
                                   $dropdown .= "\r\n</select></span></div>";
                                   echo $dropdown;
                                   mysql_close($dbcon);
                                   ?> 
                                </select> -->
                              <div class='facilities'><span class='input'>
                                <select name="product_0[]">
                                   <option value="" onclick="alert(\'Please select product!\');">Select Product</option>
                                   <option value="Lesearch Comprehensive (LE-COMP)">Lesearch Comprehensive</option>
                                   <option value="Lesearch Individual (SC)">Lesearch Individual (SC)</option>
                                   <option value="Lesearch Individual (HC)">Lesearch Individual (HC)</option>
                                   <option value="Lesearch Individual (CRILJ)">Lesearch Individual (CRILJ)</option>
                                   <option value="BHCR (Bombay High Court)">BHCR (Bombay High Court)</option>
                                   <option value="Allahabad High Court (ALJ)">Allahabad High Court (ALJ)</option>
                                   <option value="Privi Council (PC)">Privi Council (PC)</option>
                                   <option value="Case Law Navigator (CASE-LAW)">Case Law Navigator (CASE-LAW)</option>
                                </select>
                              </div>
                              </div>
                              <div class="col-md-3 m-b-15">
                                <label class="control-label">Select Mode</label>
                                <select class="form-control" name="mode_0" value="<?php echo $_POST['mode_0'];?>" placeholder="Mode">
                                  <option value="">---- Select the mode ----</option>
                                  <!-- <option value="BB">Updation BB</option>  -->      
                                  <option value="DIR">Updation DIR</option>
                                  <option value="DEMO">DEMO</option>
                                </select>
                              </div>
                          </div>
                            <div class="row row-space-10">
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Select Scheme</label>
                              <select class="form-control" name="scheme_0">
                              <option title="Bar Council of India" value="">Select Scheme</option>
                              <option title="Bar Council of India"> BCI </option>
                              <option title="Bar Council Maharashtra Goa"> BCMG </option>
                              <option title="CENTINARY SCHME"> CENT </option>
                              <option title="DEMO DVD PROJECT"> DEMO-PRO </option>
                              <option title="Journal Scheme"> JRNL </option>
                              <option title="Air Case Navigator"> NAVIGATOR </option>
                              <option title="Open General License Scheme"> OGL </option>
                              <option title="ONLINE"> ONLINE </option>
                              </select>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">HASP ID</label>
                               <input type="text" class="form-control" name="haspid" id="haspid" value="<?php echo $_POST['haspid'];?>">
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">DVD No.<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="dvdno" id="dvdno" value="<?php echo $_POST['dvdno'];?>">
                            </div>
                            <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">BB DVD No.</label>
                              <input type="text" class="form-control" name="bbdvd_0" id="bbdvd_0" value="<?php echo $_POST['bbdvd_0'];?>">
                            </div> -->
                             <div class="col-md-3 m-b-15">
                              <label class="control-label">DVD Year<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="year_0" id="year_0" value="<?php echo $_POST['year_0'];?>">
                            </div>
                          </div>

                           <div class="new_head_title" style="color: green;"><b>Payment Mode</b></div>
                           <hr>
                              <div class="row row-space-10w">
                                <div class="col-md-12 m-b-15">
                                  <div class="payment_div">
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_1" checked="checked">By Cheque</label>
                                    <!-- <label class="payment_label"><input type="radio" name="colorRadio" value="payment_2">By DD</label> -->
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_3">By NEFT / Patym</label>
                                  </div>
                                  <div class="payment_1 payment_box">
                                      
                                    <table class="table orderform_table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><input type="checkbox" name="record"></td>
                                          <td> 
                                            <label class="control-label payment_new_label">Cheque No</label> 
                                            <input name="che_no[]" type="text" class="form-control" id="che_no_1" placeholder="Cheque No" autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                          <input type="text" class="form-control" id="bank_name" name="bank_name[]" value="<?php echo $_POST['bank_name'];?>" autocomplete="OFF" placeholder="Enter Bank Name" />
                                          <div id="suggesstion-bank"></div>
                                           <!--  <input name="bank_name[]" type="text" class="form-control" id="bank_name_1" placeholder=""  autocomplete="OFF"> -->
                                          </td>
                                          <td style="width: 200px;"> 
                                         <label class="control-label payment_new_label">Branch Name</label>  
                                          <input type="text" class="form-control"  id="branch_name" name="branch_name[]" value="<?php echo $_POST['branch_name'];?>" autocomplete="OFF" placeholder="Enter Branch name"/>
                                          <div id="suggesstion-branch"></div>
                                           <!--  <input name="bank_name[]" type="text" class="form-control" id="bank_name_1" placeholder=""  autocomplete="OFF"> -->
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control" id="ifsc_code" placeholder="IFSC Code" autocomplete="OFF">
                                             <div id="suggesstion-ifc"></div>
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cashamt[]" type="text" class="form-control amount_recieve1" id="cash_amt_1" placeholder="Amount Received" onkeypress="return IsNumeric(event);" placeholder="Amount Received" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                          </td>
                                        
                                          <td> 
                                            <label class="control-label payment_new_label">Cheque Date</label> 
                                            <input type="date" name="next_due_date[]" class="form-control" id="next_due_date_1" placeholder="Cheque Date" id="datepicker" autocomplete="OFF">
                                          </td>
                                          <td> 
                                           <div class='form-group'> <label>Cheque Image</label> <input type='file'  class='form-control' id="cheque_1" name="cheque[]"></div>
                                          </td>

                                        </tr>
                                      </tbody>
                                    </table>

                                        <input type="button" class="add-row btn btn-primary btn-xs" value="Add Cheque">
                                        <input type="button" class="delete-row btn  btn-primary btn-xs" value="Delete Cheque"> 

                                     <div class="col-sm-12 by_cheque">
                                      <div class="col-md-3 m-b-15">
                              <label class="control-label">Total Amount</label>
                              <input type="text" class="form-control" name="total_amount" id="amount_dis" value="<?php echo $_POST['amount'];?>" onkeypress="return IsNumeric(event);" placeholder="Total Amount" ondrop="return false;" onpaste="return false;" autocomplete="OFF" required>
                          </div>
                          <div class="col-md-3 m-b-15">
                              <label class="control-label">Total Amount Received</label>
                              <input type="text" class="form-control" id="subTotal_dis" name="cash_amt" placeholder=" Total Received Amount" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                          </div>
                           <div class="col-md-3 m-b-15">
                              <label class="control-label">Remaining Amount</label>
                              <input type="text" class="form-control" name="remianing_amount_dis" id="remianing_amount" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                          </div>
                          <div class="register-buttons">
                              <input type="submit" class="btn btn-primary" id="submit_dsr" value="Submit" name="order_form">
                          </div>
                        </div>
                                  </div>
                                  <div class="payment_2 payment_box">
                                    <table class="table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><!-- <input type="checkbox" name="record"> --></td>
                                          <td> 
                                            <label class="control-label payment_new_label">DD No</label> 
                                            <input name="che_no[]" type="text" class="form-control" placeholder="DD No"  autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                            <input name="bank_name[]" type="text" class="form-control" placeholder="Bank Name" >
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control" placeholder="IFSC Code"  autocomplete="OFF">
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cashamt[]" type="text" class="form-control" placeholder="Amount Received"  autocomplete="OFF">
                                          </td>
                                          <!-- <td>
                                            <label class="control-label payment_new_label">Total amount</label> 
                                            <input type="text" name="total_amount[]" class="form-control" autocomplete="OFF" placeholder="" >
                                          </td> -->
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="payment_3 payment_box">
                                   <div class="col-md-3 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve2" name="recAmount" id="amount2" value="<?php echo $_POST['amount'];?>" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" required>
                                </div>
                                <div class="col-md-3 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control" id="subTotal2" name="cash_amt" placeholder="Total Amount Received" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                                </div>
                               <div class="col-md-3 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control" name="remianing_amount2" id="remianing_amount2" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                          <div class="register-buttons">
                              <input type="submit" class="btn btn-success" id="submit_dsr" value="Make Payment" name="order_form_rtgs">
                          </div>
                                  </div> 
                                </div>
                                <hr>
                              </div>
                         
                          

                        </form>
                      </div>
                    </div>
                     <!-- end register-content -->
                  </div>
                  <div class="tab-pane fade" id="tab2default">
                    <div class="right-content">
                            <div class="register-content">
                      <!-- begin register-content -->
                             <div class="Search_div">
                              <label class="control-label">Search Here :</label>
                               <input type="text" name="search" id="displays" class="updation_search_class" placeholder="Search by haspkey / Mobile / Email Id" autocomplete="OFF">
                            </div>
                            <?php 
                            $select = mysql_query("SELECT * FROM `order_form` WHERE `id`='".$_GET['userId']."'");
                            $haspids = mysql_fetch_array($select);
                            $query = "SELECT * FROM `order_form` WHERE `haspid` ='". $haspids["haspid"] ."'";
                          $result = mysql_query($query);
                          if(mysql_num_rows($result)!="") {
                            $fetch= mysql_fetch_array($result);?>
                            <div id="client-box"></div>
                            <div id="DivHide">
                        <div class="col-md-12">
                          <div class="col-md-9">
                          <div class="new_head_title"><b style="color: green;">Basic Informatio</b></div>
                          <div class="row row-space-10">
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">First Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="name" id="name" value="<?php echo $fetch['name'];?>" onkeyup="cust_name()"  autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_fname"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Last Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="last_name" value="<?php echo $fetch['last_name'];?>" id="lname" onkeyup="cust_name()" required />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_lname"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Email<span class="text-danger">*</span></label>
                              <input type="text" class="form-control email_check"  id="email" name="email" value="<?php echo $fetch['email'];?>"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_email"></div>
                            </div>
                          </div>
                        
                          <div class="row row-space-10">
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Mobile No. <span class="text-danger">*</span></label>
                              <input type="text" class="form-control"  id="mobile" onkeypress="return isNumber(event)" maxlength="10" name="mobile" value="<?php echo $fetch['mobile'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_mobile"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Phone No. <span class="text-danger">*</span></label>
                              <input type="text" class="form-control"  id="pin" maxlength="10"  name="rphone" value="<?php echo $fetch['rphone'];?>"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_phone(r)"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Address<span class="text-danger">*</span></label>
                              <textarea class="form-control" rows="3" id="add" name="address" ><?php echo $fetch['address'];?></textarea>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_add"></div>
                            </div>
                          </div>

                          <div class="row row-space-10">
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">Pin Code<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="pincode" value="<?php echo $fetch['pincode'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_pin"></div>
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label">State<span class="text-danger">*</span></label>
                              <?php $state = mysql_query("SELECT * FROM `states` WHERE `state_id`='".$fetch['state_name']."'");
                              $selState = mysql_fetch_array($state);?>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="state_name" value="<?php echo $selState['state_name'];?>" />
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label"> District<span class="text-danger">*</span></label>
                              <?php $districts = mysql_query("SELECT * FROM `cities` WHERE `city_id`='".$fetch['city']."'");
                              $seldistrict = mysql_fetch_array($districts);?>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="state_name" value="<?php echo $seldistrict['city_name'];?>" />
                             
                            </div>
                            <div class="col-md-3 m-b-15">
                              <label class="control-label"> City/Town<span class="text-danger">*</span></label>
                             <?php echo $fetch['district'];?>
                        
                            </div>
                          </div>
                           </div>
                               <fieldset class="for-panel">
                              <div id="hide-dive">
                              <?php  $report = mysql_query("SELECT * FROM `order_form` WHERE `id`='".$fetch['id']."'");
                              $res = mysql_fetch_array($report);?>
                              <?php  $uppayment = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `client_id`='".$fetch['id']."'");
                              $ressel = mysql_fetch_array($uppayment);
                              $totalCal = $res['total_amount']+$ressel['SUM(total_amount)'];?>
                              <div class="col-sm-12">
                              <label><h5 style="color: green;"><b>Purchase Details:</b></h5></label><br>
                              <div class="pur_b">
                                <?php  $purTotal = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$fetch['id']."'");?>
                                <?php $SelPur = mysql_fetch_array($purTotal);?>
                                <div class="form-group">
                                  <label><b>Total Amount :</b>&nbsp;</label>
                                  <span><?php echo number_format($res['total_amount'],2); ?> + <?php echo number_format($ressel['SUM(total_amount)'],2);?> = <?php echo number_format($totalCal, 2);?></span>
                                </div>
                                <div class="form-group">
                                  <label><b>Received Amount :</b>&nbsp;</label>
                                  <span><?php echo number_format($SelPur['SUM(reccived_amt)'],2); ?><span>
                                </div>
                                <div class="form-group">
                                  <label><b style="color: red;">Outstanding Amount :</b>&nbsp;</label>
                                  <span><?php echo number_format($totalCal-$SelPur['SUM(reccived_amt)'], 2);?></span>
                                </div>
                              </div>
                             </div>
                            </fieldset>
                            </div>

                          <div class="row row-space-10">
                            <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">Attach Order Form<span class="text-danger">*</span></label>
                              <input type="file" name="orderform" value="<?php echo $res['attach_Ordeform'];?>">
                                 <a href="file/orderform/<?php echo $res['attach_Ordeform'];?>" class="highslide" onclick="return hs.expand(this)">
                                  <img src="file/orderform/<?php echo $res['attach_Ordeform'];?>" width="120">
                                </a>
                              <input type="hidden" value="<?php echo $res['attach_Ordeform'];?>" name="orderform">
                            </div> -->
                            <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">Attach Cheque Image<span class="text-danger">*</span></label>
                              <input type="file" name="cheque" class="cheque" value="<?php echo $res['attach_cheque'];?>">
                              <a href="file/cheque/<?php echo $res['attach_cheque'];?>" class="highslide" onclick="return hs.expand(this)">
                                    <img src="file/cheque/<?php echo $res['attach_cheque'];?>" width="120">
                                  </a>
                              <input type="hidden" value="<?php echo $res['attach_cheque'];?>" name="cheque">
                            </div> -->
                          </div>
                          <div class="new_head_title"><b style="color: green;">Client History</b></div>
                          <hr>
                          <div class="row row-space-10">
                              <div class="col-md-3 m-b-15">
                                <label class="control-label">Business Associate Name<span class="text-danger">*</span></label>
                                <?php 
                                   $sqlss= mysql_query("select * from `registration` where `id`='".$fetch['ex_id']."'");
                                   $fetchs = mysql_fetch_array($sqlss);?>
                                   <input type="hidden" name="EXID" value="<?php echo $fetch['ex_id'];?>">
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $fetchs['name']; ?> <?php echo $fetchs['last_name']; ?>" readonly autocomplete="OFF">
                              </div>
                              <div class="col-md-2 m-b-15">
                                  <label class="control-label">Client From<span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="exname" id="exname" value="<?php echo date("d-m-Y",strtotime($fetch['date_time']));?>" readonly autocomplete="OFF">
                              </div>
                              <div class="col-md-2 m-b-15">
                                  <label class="control-label">HASP ID<span class="text-danger">*</span></label>
                                  <input type="text" class="form-control" name="exname" id="haspid" value="<?php echo $fetch['haspid']; ?>" readonly autocomplete="OFF">
                              </div>
                              <div class="col-md-2 m-b-15">
                              <label class="control-label">DVD No.<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="dvdno" id="dvdno" value="<?php echo $fetch['dvdno'];?>" autocomplete="OFF">
                            </div>
                             <!-- <div class="col-md-2 m-b-15">
                              <label class="control-label">BB DVD No.<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="bbdvd_0" id="bbdvd_0" value="<?php echo $fetch['bbdvd_0'];?>" autocomplete="OFF">
                            </div> -->
                          </div>
                          <div class="row row-space-10">
                             <!-- <div class="col-md-2 m-b-15">
                              <label class="control-label">PC BB DVD<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="pcbbdvd" id="pcbbdvd" value="<?php echo $fetch['pcbbdvd'];?>" autocomplete="OFF">
                            </div> -->
                             <div class="col-md-2 m-b-15">
                              <label class="control-label">DVD Year<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="year_0" id="year_0" value="<?php echo $fetch['year_0'];?>" autocomplete="OFF">
                            </div>
                            <div class="col-md-2 m-b-15">
                              <label class="control-label">Licence Days<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="licensed" id="licensed" value="<?php echo $fetch['licensed'];?>" autocomplete="OFF">
                            </div>
                           <!--  <div class="col-md-2 m-b-15">
                              <label class="control-label"> DVD No<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="bhcr" id="bhcr" value="<?php echo $fetch['bhcr'];?>" autocomplete="OFF">
                            </div> -->
                            <div class="col-md-7 m-b-15">
                              <label class="control-label">Product Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="bhcr" id="bhcr" value="<?php echo $fetch['product_0'];?>" autocomplete="OFF">
                            </div>
                          </div>
                          <div class="new_head_title"><b style="color: green;">Payment History</b></div>
                          <hr>
                          <?php  $ORDERPAYment = mysql_query("SELECT * FROM `order_form_payment_mode` WHERE `client_id`='".$fetch['id']."'");?>
                          <?php while($selectQuery = mysql_fetch_array($ORDERPAYment)){?>
                          <div class="row row-space-10">
                              <div class="col-md-2 m-b-15">
                                <label class="control-label">Cheque No.<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['cheque_no']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-md-2 m-b-15">
                                <label class="control-label">Bank Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['bank_name']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-md-2 m-b-15">
                                <label class="control-label">Reccived Amount<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['reccived_amt']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-md-2 m-b-15">
                                <label class="control-label">Cheque Date<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo date("d-m-Y",strtotime($selectQuery['due_date']));?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-md-2 m-b-15">
                                <label class="control-label">Cheque Status<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['cheque_status'];?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-md-2 m-b-15">
                                <label class="control-label">Cheque Against<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['payment_status'];?>" readonly autocomplete="OFF">
                           </div>
                          </div>

                          <?php } ?>
                          <div class="row row-space-10">
                          </div>
                          <div class="register-buttons">
                          <!-- <input type="submit" class="btn btn-warning" id="submit_dsr" data-toggle="modal" data-target="#transfer" onclick="trnasferTO(<?php echo $fetch['id'];?>);" value="Transfer To" name="order_form">&nbsp;&nbsp;&nbsp; -->
                          <!-- <a href="checkOut.php?id=<?php echo $fetch['id'];?>" class="btn btn-success">Make Payment</a> 
                          -->
                          <a class="btn btn-success"  data-toggle="modal" data-target="#makePayment">Make Payment</a>
                          </div>
                      <?php }else{
                        ?>
                        <h4><b style="color: red;">Sorry Data Not Found!</b></h4>
                      <?php } ?>
                         <style>
               fieldset.for-panel {
          background-color: #fcfcfc;
        border: 1px solid #999;
        border-radius: 4px; 
        padding:15px 10px;
        background-color: #d9edf7;
          border-color: #bce8f1;
        background-color: #f9fdfd;
        margin-bottom:12px;
      }
      fieldset.for-panel legend {
          background-color: #fafafa;
          border: 1px solid #ddd;
          border-radius: 5px;
          color: #4381ba;
          font-size: 14px;
          font-weight: bold;
          line-height: 10px;
          margin: inherit;
          padding: 7px;
          width: auto;
        background-color: #d9edf7;
        margin-bottom: 0;
      }
      .cheque_image_view
      {
        margin: 10px;
        float: left; 
        width: 30%;
      }
      .cheque_image_view img
      {
        width: 100%;

      }
      .payment_mode_modal .modal-dialog {
    width: 80%;
    margin: 30px auto;
}
.payment_mode_modal .modal-body
{
width: 100%;
float: left;
}
.payment_mode_modal h4 {
    width: 94%;
    float: left;
    color: #23527c;
    margin-left: 1%;
    text-align: center;
    font-weight: bold;
}
.button_div
{
  width: 100%;
  text-align: center;
}
</style>
<script>
 $(document).on("change keyup blur", ".amount_recieve", function() {
      calculation();
  });

  function calculation(){
    debugger;
   var sum = 0;
    $(".amount_recieve").each(function(){
        sum += +$(this).val();
    });
    $("#subTotal1").val(sum); 
    
    $(document).on("change keyup blur onload click", function(){ 
      var getTotal= $("#amount1").val();
      var getRemainingAmount = $("#subTotal1").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount1").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount1").val('');
        }
   }); 
  }
    
    
</script>
<script>
 $(document).on("change keyup blur", ".amount_recieve_dis", function() {
      calculation();
  });

  function calculation(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount_dis").val();
      var getRemainingAmount = $("#subTotal_dis").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount_dis").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount_dis").val('');
        }
   }); 
  }
    
    
</script>
<div id="makePayment" class="modal fade payment_mode_modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class=".modal-title">Select Purchase mode</h4>
      </div>
      <hr/>
      <div class="modal-body" style="border: none;">
       <div class="row row-space-10w">
                                <div class="col-md-12 m-b-15">
                                  <div class="payment_div">
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_1" checked="checked">By Cheque</label>
                                    <!-- <label class="payment_label"><input type="radio" name="colorRadio" value="payment_2">By DD</label> -->
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_3">By NEFT/Patym</label>
                                  </div>
                                  <div class="payment_1 payment_box">
                                  <form name="formsd" method="POST" action="submit.php" enctype="multipart/form-data">
                                    <table class="table orderform_table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><input type="checkbox" name="record"></td>
                                          <td> 
                                            <label class="control-label payment_new_label">Cheque No</label> 
                                            <input name="che_no[]" type="text" class="form-control" placeholder="Cheque No" required="required" autocomplete="OFF">
                                          </td>
                                          <td style="width: 150px;"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                            <input name="bank_name[]" type="text" class="form-control" placeholder="Bank Name" required="required" autocomplete="OFF">
                                          </td>
                                           <td style="width: 150px;"> 
                                          <label class="control-label payment_new_label">Branch Name</label>  
                                            <input name="branch_name[]" type="text" class="form-control" placeholder="Branch Name" required="required" autocomplete="OFF">
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control" placeholder="IFSC Code" required="required" autocomplete="OFF">
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cash_amt[]" type="text" class="form-control amount_recieve_dis" placeholder="Amount Received" id="cash_amt_1" required="required" autocomplete="OFF">
                                          </td>
                                        
                                          <!-- <td> 
                                            <label class="control-label payment_new_label">Remaining Amount</label> 
                                            <input type="text" name="remaining_amt[]" class="form-control" placeholder="" autocomplete="OFF">
                                          </td> -->
                                          <td> 
                                            <label class="control-label payment_new_label">PDC Date</label> 
                                            <input type="date" name="next_due_date[]" class="form-control" placeholder="" id="datepicker" autocomplete="OFF">
                                          </td>
                                          <td> 
                                            <div class='form-group'> <label>Cheque Image</label> 
                                              <input type='file'  class='form-control' name="cheque[]"></div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <input type="hidden" name="clientID" value="<?php echo $fetch['id'];?>">
                                    <input type="button" class="add-row btn-xs btn-primary" value="Add Cheque">
                                    <input type="button" class="delete-row btn-xs  btn-primary" value="Delete Row"> 
                                    <div class="col-sm-12 new_amt">
                                    <div class="col-sm-4">
                                      <label>Total Amount</label>
                                      <input type="text" name="" class="form-control" id="amount_dis" placeholder="Enter Total Amount" required>
                                    </div>
                                    <div class="col-sm-4">
                                      <label>Received Amount</label>
                                      <input type="text" name="" class="form-control" id="subTotal_dis" placeholder="Enter Received Amount" required>
                                    </div>
                                    <div class="col-sm-4">
                                      <label>Remaining Amount</label>
                                      <input type="text" name="" class="form-control" id="remianing_amount1" placeholder="Enter Remaining Amount" required>
                                    </div>
                                  </div>
                                     <br>
                                     <div class="button_div">
                                      <input type="submit" value="Submit" class="btn btn-success" name="submit_payment"></div>
                                  </div>
                                </form>
                                  <!-- <div class="payment_2 payment_box">
                                    <table class="table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"></td>
                                          <td> 
                                            <label class="control-label payment_new_label">DD No</label> 
                                            <input name="che_no[]" type="text" class="form-control" placeholder="" required="required" autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                            <input name="bank_name[]" type="text" class="form-control" placeholder="" required="required">
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control" placeholder="" required="required" autocomplete="OFF">
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cash_amt[]" type="text" class="form-control" placeholder="" required="required" autocomplete="OFF">
                                          </td>
                                        
                                        </tr>
                                      </tbody>
                                    </table>
                                    <br>
                                     <div class="button_div"><input type="submit" name="submit_dd" value="Submit" class="btn btn-success text-center" ></div>
                                  </div> -->
                                  <div class="payment_3 payment_box">
                                    <form action="CheckOut.php?id=<?php echo $fetch['id'];?>" method="POST">
                                    <table class="table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><!-- <input type="checkbox" name="record"> --></td>
                                          <!-- <td> 
                                            <label class="control-label payment_new_label">Transaction No</label> 
                                            <input name="che_no[]" type="text" class="form-control" placeholder="" required="required" autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                            <input name="bank_name[]" type="text" class="form-control" placeholder="" required="required" autocomplete="OFF">
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control" placeholder="" required="required" autocomplete="OFF">
                                          </td> -->
                                           <td>
                                            <label class="control-label payment_new_label">Total Amount</label> 
                                            <input name="cashAmount" type="text" class="form-control amount_recieve" id="amount_dis" placeholder="Total Amount" required="required" autocomplete="OFF">
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="recAmount" type="text" class="form-control" id="subTotal" placeholder="Amount Received" required="required" autocomplete="OFF">
                                          </td>
                                           <td>
                                            <label class="control-label payment_new_label">Remaining Amount</label> 
                                            <input name="cashAmount" type="text" class="form-control" id="remianing_amount" placeholder="Remaining Amount" required="required" autocomplete="OFF">
                                          </td>
                                          <!-- <td>
                                            <label class="control-label payment_new_label">Total Amount</label> 
                                            <input type="text" name="total_amount[]" class="form-control" placeholder="" required>
                                          </td> -->
                                        </tr>
                                      </tbody>
                                    </table>
                                    <br>
                                     <div class="button_div"><input type="submit" name="submitpayment" value="Proceed to Checkout" class="btn btn-success"></div>
                                  </div>
                                    </form>
                                </div>

                              </div>
      </div>
      <div class="modal-footer" style="border: none;">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<style>
  .new_amt{
    padding:2% 0;
  }
  .new_amt input{
    padding: 0;
  }
</style>
<script type="text/javascript">
$(document).ready(function(){
$(".add-row").click(function(){
var markup = "<tr> <td style='vertical-align: text-bottom;'><input type='checkbox' name='record'></td><td> <label class='control-label payment_new_label'>Cheque No</label> <input name='che_no[]' type='text' class='form-control' placeholder='Cheque No' required='required' autocomplete='OFF'> </td><td style='width: 200px;'> <label class='control-label payment_new_label'>Bank Name</label> <input name='bank_name[]' type='text' class='form-control' placeholder='Bank Name' required='required' autocomplete='OFF'> </td><td> <label class='control-label payment_new_label'>Bank Branch</label> <input type='text' name='bank_branch[]' class='form-control' placeholder='' autocomplete='OFF'> </td><td> <label class='control-label payment_new_label'>IFSC Code</label> <input name='ifsc_code[]' type='text' class='form-control' placeholder='' required='required' autocomplete='OFF'> </td><td> <label class='control-label payment_new_label'>Amount Received</label> <input name='cash_amt[]' type='text' class='form-control' placeholder='Amount Received' id='cash_amt' required='required' autocomplete='OFF'> </td><td> <label class='control-label payment_new_label'>Cheque Date</label> <input type='date' name='next_due_date[]' class='form-control' placeholder='' id='datepicker' autocomplete='OFF'> </td><td> <div class='form-group'> <label>Cheque Image</label> <input type='file'  class='form-control' name='cheque[]'></div></td></tr>";
$(".orderform_table").append(markup);
});
// Find and remove selected table rows
$(".delete-row").click(function(){
$(".orderform_table").find('input[name="record"]').each(function(){
if($(this).is(":checked")){
$(this).parents("tr").remove();
}
});
});
}); 
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".payment_box").not(targetBox).hide();
        $(targetBox).show();
    });
});
</script>
<script>
function trnasferTO(id)
  {
    var id=+id;
    //alert(id);
    $.ajax({
    type: "POST",
    url: "ajax/transfer.php",
    data: "id="+id,
    cache: false,
    success: function(result){
      //alert(result);
    $( '#infoorder' ).html(result);
  }
});
    //alert("OK");
  }
</script>

<div id="transfer" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
            <div id="infoorder"></div>
  </div>
</div>
                        </form>
                      </div>
                    </div>
                     <!-- end register-content -->
                  </div>
                </div>
              </div>
            </div>
          </div>

      </div>
      <!-- end right-content -->

   </div>

   
   <script type="text/javascript">
      $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
   </script>            
   <!-- ================== BEGIN BASE JS ================== -->
   <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
   <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
   <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
   <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
   <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
   <!-- ================== END BASE JS ================== -->
   <!-- ================== BEGIN PAGE LEVEL JS ================== -->
   <script src="assets/plugins/sparkline/jquery.sparkline.js"></script>
   <script src="assets/js/login-v2.demo.min.js"></script>
   <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script src="assets/js/form-plugins.demo.min.js"></script>
   <script src="assets/js/apps.min.js"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   <link rel="stylesheet" href="/resources/demos/style.css">
   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <!-- ================== END PAGE LEVEL JS ================== -->
   <script>
      $(document).ready(function() {
        App.init();
          FormPlugins.init();
          FormPlugins1.init();
          FormPlugins6.init();
      });
   </script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
   <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

$( "#myform1" ).validate({
  rules: {
    name: {
      required: true,
      minlength: 2
    },
    last_name: {
      required: true,
      minlength: 2
    },
    email: {
      required: true,
      email: true
    },
    mobile:
    {
      required: true,
      number: true
    },
    rphone1:
    {
      required: true,
      number: true
    },
    pincode:
    {
      required: true,
      number: true
    }
  }
});
</script>
<script>

$( "#myform" ).validate({
  rules: {
    name: {
      required: true,
      minlength: 2
    }
    ,
    last_name: {
      required: true,
      minlength: 2
    },
    email: {
      required: true,
      email: true
    },
    mobile:
    {
      required: true,
      number: true
    },
    rphone1:
    {
      required: true,
      number: true
    },
    pincode:
    {
      required: true,
      number: true
    }
  }
});

$(document).ready(function(){
  $("#town").keyup(function(){
    //alert("OK");
    $.ajax({
    type: "POST",
    url: "townSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#town").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      //alert(data);
      $("#suggesstion-town").show();
      $("#suggesstion-town").html(data);
      $("#town").css("background","#FFF");
    }
    });
  });
});
//To select country name
function selecttown(val) {
  //alert(val);
$("#town").val(val);
$("#suggesstion-town").hide();
}


$(document).ready(function(){
  $("#bank_name").keyup(function(){
    //alert("OK");
    $.ajax({
    type: "POST",
    url: "banknameSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
     // alert(data);
      $("#suggesstion-bank").show();
      $("#suggesstion-bank").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbank(val) {
$("#bank_name").val(val);
$("#suggesstion-bank").hide();
}

$(document).ready(function(){
  $("#branch_name").keyup(function(){
    //alert("OK");
    var bank = $("#bank_name").val();
    //alert(bank);
    $.ajax({
    type: "POST",
    url: "branchSearch.php",
    data:'keyword='+$(this).val() +'&bank='+bank,
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
     //alert(data);
      $("#suggesstion-branch").show();
      $("#suggesstion-branch").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbranch(val) { 
  var branch = val;
  var bank = $("#bank_name").val();
    $.ajax({
            type:'POST',
            url:'ifscSearch.php',
            data:'branchname='+branch + '&bank='+bank,
            success:function(data){
                  /*alert(data);*/
                  $("#ifsc_code").val(data);           
                }
            }); 

  //alert(branch);
  $("#branch_name").val(val);
  $("#suggesstion-branch").hide();


}
/*function selectifc(val) { 
  $("#ifc_name").val(val);
  $("#suggesstion-ifc").hide();
}*/
/*var bank = val;

if(bank){
            $.ajax({
                type:'POST',
                url:'branchSearch.php?meg=branch',
                data:'branchname='+bank,
                 beforeSend: function(){
                    $("#branch_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                  },
                success:function(data){
                 
                    $("#suggesstion-branch").show();
                    $('#suggesstion-branch').html(data);
                    $("#branch_name").css("background","#FFF");                  
                }
            }); 
           
        }else{
            $('#branch_name').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }*/

 //var myElement = document.getElementById(".bank_name"); 

</script>


<script type="text/javascript">
$(document).ready(function(){
                    $("#displays").keyup(function(){
                    