<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
<?php include("head.php");?>
<script>
  $(document).ready(function(){
    $('#data-newclientlist').dataTable();
  });
  $(document).ready(function(){
    $('.deletebtn').click(function(){
        alert('Confirm Delete');
    });
  });
  $(document).ready(function(){
    $("#all_report").addClass("active");
    $("#new_client_report").addClass("active");
  });
</script>
<style>
.clr_box{width:1%;height:10px;background:#99dede;float:left;margin-top:6px}.note_converted p{width:2%;float:left}.note_converted{font-size:13px}
.table-striped tbody tr:nth-child(odd) td,
.table-striped tbody tr:nth-child(odd) th {
/*  background-color: #DFF0D8;*/
background-color: #f8e5be !important;
}
</style>
 <script>
$('#purpose').on('change', function () {
 if(this.value == 'converted'){
  $(".alloted_client_body tr").addClass("success");
 }
  });
</script>
</head>
    <?php include_once("header.php"); ?>
    <?php include_once("sidebar.php"); ?>  
  <div id="content" class="content">
      <ol class="breadcrumb" style="width: 100%;">
          <li><a href="http://crm.airinfotech.in/telecaller/">Home</a></li>
          <li><a href="http://crm.airinfotech.in/telecaller/">Dashboard</a></li>
          <li class="active">New Client List</li>
      </ol>
<div class="details">
  <h3 class="col-sm-8" style="margin-top: 0px; color: green;"><span>New Client List &nbsp;&nbsp;</span><a href="http://crm.airinfotech.in/telecaller/" class="btn btn-primary btn-xs">Back To Home Page</a>&nbsp;&nbsp;<a href="new_client.php" class="btn btn-warning btn-xs">Back To Add New Client</a>
  </h3>  
   <?php include("timezone.php");?>
  <hr style="float: left;width: 100%;">
  <div class="col-sm-12 table-responsive dsr_form_reports_tb">
    <table id="data-newclientlist" class="datatable table table-striped table-bordered">
    <thead>
    <tr>
      <th>Sr.No.</th>
      <th>Client Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Phone No</th>
      <th>Address</th>
      <th>Client Status</th>
      <th>Called Details</th>
      <th>View Details</th>
      <th>Source</th>
    </tr>
</thead>
    <tbody class="alloted_client_body"> 
     <?php     
   $sql = mysql_query("SELECT * FROM `airinfot_newcustomercontact` WHERE `cust_status`='New Client' AND `ex_id`='".$_SESSION['user_id']."'");
      $i=1;
      while($row=mysql_fetch_array($sql)){ 
        $clientdate = mysql_query("SELECT * FROM `airinfot_newcustomermaster` WHERE `custid`='".$row['custid']."'");
        $selclientdaat = mysql_fetch_array($clientdate);?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo ucfirst($selclientdaat['name']);?> <?php echo ucfirst($selclientdaat['last_name']);?></td>
        <td><?php echo $row['email'];?></td>
        <td><?php echo $row['personal_mobile'];?></td>
        <!-- <td><?php echo $fetch3['work_street'];?></td> -->
        <td><?php echo $row['personal_phone']?></td>
        <td><?php echo $row['personal_street']?></td>
        <td><a data-toggle="modal" data-target="#myModalDSR" class="btn btn-success btn-xs" onclick="Status(<?php echo $row['custid'];?>);">Add Client Status</a>
        </td>
        <td>
          <?php $clientStatus = mysql_query("SELECT * FROM `clientstatus` WHERE `ex_id`='".$_SESSION['user_id']."' AND `client_id`='".$row['custid']."'"); $selClient = mysql_num_rows($clientStatus); if($selClient>0){?>
          <a data-toggle="modal" data-target="#history" class="btn btn-warning btn-xs" onclick="history(<?php echo $row['custid'];?>);">Called Details</a>
          <?php } ?>
        </td>
        <td><button class="btn btn-info btn-xs" type="button" data-target="#callingModal" onclick="details(<?php echo $row['custid'];?>)" data-toggle="modal">View Details</button></td>
        <td><?php echo $row['cust_status']?></td>
      </tr>
      <script>
      $(document).ready(function(){
      $("#called_for").on("change",function(){
        var this_val = $(this).val();
        if(this_val == "other"){
            $("#other_reason_inp").show();
          }
          else{
            $("#other_reason_inp").hide();
          }
      })
      });
    </script>
     <?php $i++;} ?>
    </tbody>
    </table>
  </div>
  <div class="col-sm-12"><b style="float: left;">NOTE:</b> <p style="float:left;">The </p>&nbsp;<div class="clr_box"></div>&nbsp; denotes <b>Client</b> is <b>Converted</b>.</div>
</div>
<div id="reasonModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
<div id="calledfor"></div>
  </div>
</div>


<div id="callingModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">View Client Details</h3>
      </div>
      <div class="modal-body">
      <div id="details"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="history" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="his"></div>
  </div>
</div>
</div>
<?php include("footer.php");?>
<div id="myModalDSR" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="info"></div>
  </div>
</div>

<script>
  function calledfor(id){
   var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/calledfor.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#calledfor' ).html(result);
  }
});
  }
  function Status(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/dsrStatus.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#info' ).html(result);
  }
});
  }
   function details(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/details.php",
    data: "id="+id,
    cache: false,
    success: function(data){
    $( '#details' ).html(data);
  }
});
  }
    function history(id) {
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/dsrhistory.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#his' ).html(result);
  }
});
  }
</script>

