<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Editable Invoice</title>
	<link rel="stylesheet" type="text/css" href="style_invoice.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
   <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
   <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
   <!-- ================== END PAGE LEVEL STYLE ================== -->
   $_GET["id"];?>
</head>
<body>
	<style>
		.wrapper
		{
			height: auto !important;
		}
	</style>
<div class="wrapper">
	<div class="table_box">
		<div class="head_invocie"><h4 style="color: red;">Tax Invoice</h4></div>
		<input type="hidden" name="client_id" value="'.$_GET["id"].'">
<table border="1" cellpadding="1" cellspacing="1" style="border-collapse: collapse;width: 100%;">
	<tbody>
		<tr>
			<td colspan="5" rowspan="3">
				<span class="ff1 ">M/s AIR INFOTECH<!--  (19014-2016) Current - (From 1-Apr-2016) --></span><br />
				Flat No. 401, B B Tower,<br />
				58/B Shankar Nagar East<br />
				Behind Sai Vatika, Nagpur<br />
				GSTIN/UIN: 27AAGFA0718H1ZR<br />
				Contact : 0712 - 2521858, 2544058<br />
				E-Mail : info@airinfotech.in</td>
			<td colspan="3">
				Invoice No.<br />
				<input type="text" name="invoice_no" class="half_inputfield" placeholder="AIRINFO/17-18/001" value="AIRINFO/17-18/'.$_GET["id"].'" readonly style="color: red;">
				</td>
			<td>
				Dated<br />
				<input type="text" name="dated" class="half_inputfield" placeholder="Select Date" id="" value="'.$dated.'">
				</td>
		</tr>
		<tr>
			<td colspan="3">
				Delivery Note<br/>
				<input type="text" name="delivery_note" class="half_inputfield" value="'. $delivery_note.'">
			</td>
			<td>
				Mode/Terms of Payment<br />
				<input type="text" name="payment_mode" class="half_inputfield" value="'.$payment_mode.'">
				</td>
		</tr>
		<tr>
			<td colspan="3">
				Supplier&rsquo;s Ref.<br />
				<input type="text" name="supplier" class="half_inputfield" placeholder="AIRINFO/BB/17-18" value="'.$supplier.'"></td>
			<td>
				Other Reference(s)<br />
				<input type="text" name="other_reference" class="half_inputfield" value="'.$other_reference.'">
				</td>
		</tr>
		<tr>
			<td colspan="5" rowspan="4">
				Buyer,<br />
				<textarea class="buyer_text" style="color: green;" name="buyer_details">'.$buyer_details.'</textarea>
				<!-- Khapre R L<br />
				Nagpur<br /> -->
				<br>
				PAN/IT No : <input type="text" name="pan_no" placeholder="" class="half_inputfield" value="'.$pan_no.'"><br />
				Place of Supply : <input type="text" name="buyer_details" placeholder="Maharashtra" class="half_inputfield value="'.$buyer_details.'"></td>
			<td colspan="3">
				Buyer&rsquo;s Order No.<br />
				<input type="text" name="order_no" class="half_inputfield" value="'.$order_no.'">
				</td>
			<td>
				Dated<br />
				<input type="text" name="Dated1" class="half_inputfield" value="'.$Dated1.'">
				</td>
		</tr>
		<tr>
			<td colspan="3">
				Despatch Document No.<br />
				<input type="text" name="despatch_document_no" class="half_inputfield" value="'.$despatch_document_no.'">
				</td>
			<td>
				Delivery Note Date<br />
				<input type="text" name="delivery_note_date" class="half_inputfield" value="'.$delivery_note_date>
				</td>
		</tr>
		<tr>
			<td colspan="3">
				Despatched through<br />
				<input type="text" name="despatch_through" class="half_inputfield" value="<?php echo $selinvoice["despatch_through"]; ?>">
				</td>
			<td>
				Destination<br />
				<input type="text" name="designation" class="half_inputfield" value="<?php echo $selinvoice["designation"]; ?>">
				</td>
		</tr>
		<tr>
			<td colspan="4" rowspan="1">
				Terms of Delivery
				<br/>
				<textarea class="terms_textarea" name="term_of_delivery" value="<?php echo $selinvoice["term_of_delivery"]; ?>">
					
				</textarea>
			</td>
		</tr>
		<tr>
			<td rowspan="1" width="30">
				Sr.No</td> 
			<td rowspan="1" colspan="2" width="600">
				Description of Goods</td>
			<td rowspan="1" width="80">
				HSN/SAC</td>
			<td rowspan="1">
				Quantity</td>
			<td rowspan="1" width="80">
				Rate</td>
			<td rowspan="1">
				per</td>
			<td rowspan="1">
				Disc. %</td>
			<td rowspan="1" width="100">
				Amount</td>
		</tr>
		<?php if($selinvoice["air_dvd_alj"]!="" && $selinvoice["air_dvd_alj"]!=0){?>
		<tr class="tr_new" <?php if($selinvoice["air_dvd_alj"]=="" && $selinvoice["air_dvd_alj"]==0){ echo "style=display:none;"; } ?>>
			<td>1</td>
			<td colspan="2">AIR DVD ALJ<input type="hidden" name="air_dvd_alj" value="AIR DVD ALJ"></td>
			<td><input type="text" name="HSN_SAC1" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC1"];?>"></td>
			<td><input type="text" name="qty1"  id="qty1" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty1"];?>"></td>
			<td><input type="text" name="rate1" id="rate1" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate1"];?>"></td>
			<td><input type="text" name="per1" id="per1" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc1" id="disc1" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc1"];?>"></td>
			<td><input type="text" name="total1"  id="total1" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_alj"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_bhcr"]!="" && $selinvoice["air_dvd_bhcr"]!=0){?>
		<tr class="tr_new">
			<td>2</td>
			<td colspan="2">AIR DVD BHCR <input type="hidden" name="air_dvd_bhcr" value="AIR DVD BHCR"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC2"];?>"></td>
			<td><input type="text" name="qty2"  id="qty2" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty2"];?>"></td>
			<td><input type="text" name="rate2" id="rate2" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate2"];?>"></td>
			<td><input type="text" name="per2" id="per2" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc2" id="disc2" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc2"];?>"></td>
			<td><input type="text" name="total2"  id="total2" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_bhcr"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_crilj"]!="" && $selinvoice["air_dvd_crilj"]!=0){?>
		<tr class="tr_new">
			<td>3</td>
			<td colspan="2">AIR DVD CRILJ<input type="hidden" name="air_dvd_crilj" value="AIR DVD CRILJ"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC3"];?>"></td>
			<td><input type="text" name="qty3"  id="qty3" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty3"];?>"></td>
			<td><input type="text" name="rate3" id="rate3" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate3"];?>"></td>
			<td><input type="text" name="per3" id="per3" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc3" id="disc3" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc3"];?>"></td>
			<td><input type="text" name="total3"  id="total3" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_crilj"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_hc"]!="" && $selinvoice["air_dvd_hc"]!=0){?>
		<tr class="tr_new">
			<td>4</td>
			<td colspan="2">AIR DVD HC<input type="hidden" name="air_dvd_hc" value="AIR DVD HC"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC4"];?>"></td>
			<td><input type="text" name="qty4"  id="qty4" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty4"];?>"></td>
			<td><input type="text" name="rate4" id="rate4" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate3"];?>"></td>
			<td><input type="text" name="per4" id="per4" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc4" id="disc4" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc4"];?>"></td>
			<td><input type="text" name="total4"  id="total4" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_hc"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_hc_crilj"]!="" && $selinvoice["air_dvd_hc_crilj"]!=0){?>
		<tr class="tr_new">
			<td>5</td>
			<td colspan="2">AIR DVD HC/CRILJ<input type="hidden" name="air_dvd_hc_crilj" value="AIR DVD HC/CRILJ"> </td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC5"];?>"></td>
			<td><input type="text" name="qty5"  id="qty5" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty5"];?>"></td>
			<td><input type="text" name="rate5" id="rate5" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate4"];?>"></td>
			<td><input type="text" name="per5" id="per5" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc5" id="disc5" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc5"];?>"></td>
			<td><input type="text" name="total5"  id="total5" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_hc_crilj"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_hc_dir"]!="" && $selinvoice["air_dvd_hc_dir"]!=0){?>
		<tr class="tr_new">
			<td>6</td>
			<td colspan="2">AIR DVD HC DIR<input type="hidden" name="air_dvd_hc_dir" value="AIR DVD HC DIR"> </td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC6"];?>"></td>
			<td><input type="text" name="qty6"  id="qty6" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty6"];?>"></td>
			<td><input type="text" name="rate6" id="rate6" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate6"];?>"></td>
			<td><input type="text" name="per6" id="per6" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc6" id="disc6" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc6"];?>"></td>
			<td><input type="text" name="total6"  id="total6" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_hc_dir"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_hc_pc"]!="" && $selinvoice["air_dvd_hc_pc"]!=0){?>
		<tr class="tr_new">
			<td>7</td>
			<td colspan="2">AIR DVD HC/PC<input type="hidden" name="air_dvd_hc_pc" value="AIR DVD HC PC"> </td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC7"];?>"></td>
			<td><input type="text" name="qty7"  id="qty7" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty7"];?>"></td>
			<td><input type="text" name="rate7" id="rate7" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate7"];?>"></td>
			<td><input type="text" name="per7" id="per7" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc7" id="disc7" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc7"];?>"></td>
			<td><input type="text" name="total7"  id="total7" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_hc_pc"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_pc"]!="" && $selinvoice["air_dvd_pc"]!=0){?>
		<tr class="tr_new">
			<td>8</td>
			<td colspan="2">AIR DVD PC <input type="hidden" name="air_dvd_pc" value="AIR DVD PC"> </td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC8"];?>"></td>
			<td><input type="text" name="qty8"  id="qty8" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty8"];?>"></td>
			<td><input type="text" name="rate8" id="rate8" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate8"];?>"></td>
			<td><input type="text" name="per8" id="per8" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc8" id="disc8" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc8"];?>"></td>
			<td><input type="text" name="total8"  id="total8" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_pc"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_pc_dir"]!="" && $selinvoice["air_dvd_pc_dir"]!=0){?>
		<tr class="tr_new">
			<td>9</td>
			<td colspan="2">AIR DVD PC DIR<input type="hidden" name="air_dvd_pc_dir" value="AIR DVD PC DIR"> </td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC9"];?>"></td>
			<td><input type="text" name="qty9"  id="qty9" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty9"];?>"></td>
			<td><input type="text" name="rate9" id="rate9" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate9"];?>"></td>
			<td><input type="text" name="per9" id="per9" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc9" id="disc9" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc9"];?>"></td>
			<td><input type="text" name="total9"  id="total9" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_pc_dir"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc"]!="" && $selinvoice["air_dvd_sc"]!=0){?>
		<tr class="tr_new">
			<td>10</td>
			<td colspan="2">AIR DVD SC<input type="hidden" name="air_dvd_sc" value="AIR DVD SC"> </td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC10"];?>"></td>
			<td><input type="text" name="qty10"  id="qty10" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty10"];?>"></td>
			<td><input type="text" name="rate10" id="rate10" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate10"];?>"></td>
			<td><input type="text" name="per10" id="per10" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc10" id="disc10" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc10"];?>"></td>
			<td><input type="text" name="total10"  id="total10" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_crilj"]!="" && $selinvoice["air_dvd_sc_crilj"]!=0){?>
		<tr class="tr_new">
			<td>11</td>
			<td colspan="2"> AIR DVD SC/CRILJ <input type="hidden" name="air_dvd_sc_crilj" value="AIR DVD SC/CRILJ"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC11"];?>"></td>
			<td><input type="text" name="qty11"  id="qty11" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty11"];?>"></td>
			<td><input type="text" name="rate11" id="rate11" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate11"];?>"></td>
			<td><input type="text" name="per11" id="per11" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc11" id="disc11" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc11"];?>"></td>
			<td><input type="text" name="total11"  id="total11" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_crilj"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_dir"]!="" && $selinvoice["air_dvd_sc_dir"]!=0){?>
		<tr class="tr_new">
			<td>12</td>
			<td colspan="2">AIR DVD SC DIR<input type="hidden" name="air_dvd_sc_dir" value="AIR DVD SC DIR"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC12"];?>"></td>
			<td><input type="text" name="qty12"  id="qty12" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty12"];?>"></td>
			<td><input type="text" name="rate12" id="rate12" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate12"];?>"></td>
			<td><input type="text" name="per12" id="per12" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc12" id="disc12" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc12"];?>"></td>
			<td><input type="text" name="total12"  id="total12" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_dir"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_hc"]!="" && $selinvoice["air_dvd_sc_hc"]!=0){?>
		<tr class="tr_new">
			<td>13</td>
			<td colspan="2"> AIR DVD SC/HC<input type="hidden" name="air_dvd_sc_hc" value="AIR DVD SC/HC"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC13"];?>"></td>
			<td><input type="text" name="qty13"  id="qty13" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty13"];?>"></td>
			<td><input type="text" name="rate13" id="rate13" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate13"];?>"></td>
			<td><input type="text" name="per13" id="per13" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc13" id="disc13" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc13"];?>"></td>
			<td><input type="text" name="total13"  id="total13" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_hc"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_hc_crilj"]!="" && $selinvoice["air_dvd_sc_hc_crilj"]!=0){?>
		<tr class="tr_new">
			<td>14</td>
			<td colspan="2">AIR DVD SC/HC/CRILJ<input type="hidden" name="air_dvd_sc_hc_crilj" value="AIR DVD SC/HC/CRILJ"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC14"];?>"></td>
			<td><input type="text" name="qty14"  id="qty14" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty14"];?>"></td>
			<td><input type="text" name="rate14" id="rate14" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate14"];?>"></td>
			<td><input type="text" name="per14" id="per14" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc14" id="disc14" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc14"];?>"></td>
			<td><input type="text" name="total14"  id="total14" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_hc_crilj"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_hc_crilj_dir"]!="" && $selinvoice["air_dvd_sc_hc_crilj_dir"]!=0){?>
		<tr class="tr_new">
			<td>15</td>
			<td colspan="2"> AIR DVD SC/HC/CRILJ DIR<input type="hidden" name="air_dvd_sc_hc_crilj_dir" value="AIR DVD SC/HC/CRILJ DIR"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC15"];?>"></td>
			<td><input type="text" name="qty15"  id="qty15" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty15"];?>"></td>
			<td><input type="text" name="rate15" id="rate15" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate15"];?>"></td>
			<td><input type="text" name="per15" id="per15" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc15" id="disc15" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc15"];?>"></td>
			<td><input type="text" name="total15"  id="total15" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_hc_crilj_dir"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_hc_crilj_pc"]!="" && $selinvoice["air_dvd_sc_hc_crilj_pc"]!=0){?>
		<tr class="tr_new">
			<td>16</td>
			<td colspan="2">AIR DVD-SC/HC/CRILJ/ PC <input type="hidden" name="air_dvd_sc_hc_crilj_pc" value="AIR DVD-SC/HC/CRILJ/ PC"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC16"];?>"></td>
			<td><input type="text" name="qty16"  id="qty16" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty16"];?>"></td>
			<td><input type="text" name="rate16" id="rate16" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate16"];?>"></td>
			<td><input type="text" name="per16" id="per16" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc16" id="disc16" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc16"];?>"></td>
			<td><input type="text" name="total16"  id="total16" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_hc_crilj_pc"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_hc_crilj_pc_dir"]!="" && $selinvoice["air_dvd_sc_hc_crilj_pc_dir"]!=0){?>
		<tr class="tr_new">
			<td>17</td>
			<td colspan="2"> AIR DVD SC/HC/CRILJ/PC-DIR<input type="hidden" name="air_dvd_sc_hc_crilj_pc_dir" value="AIR DVD SC/HC/CRILJ/PC-DIR"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC17"];?>"></td>
			<td><input type="text" name="qty17"  id="qty17" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty17"];?>"></td>
			<td><input type="text" name="rate17" id="rate17" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate17"];?>"></td>
			<td><input type="text" name="per17" id="per17" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc17" id="disc17" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc17"];?>"></td>
			<td><input type="text" name="total17"  id="total17" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_hc_crilj_pc_dir"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_hc_dir"]!="" && $selinvoice["air_dvd_sc_hc_dir"]!=0){?>
		<tr class="tr_new">
			<td>18</td>
			<td colspan="2">AIR DVD SC/HC DIR<input type="hidden" name="air_dvd_sc_hc_dir" value="AIR DVD SC/HC DIR"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC18"];?>"></td>
			<td><input type="text" name="qty18"  id="qty18" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty18"];?>"></td>
			<td><input type="text" name="rate18" id="rate18" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate18"];?>"></td>
			<td><input type="text" name="per18" id="per18" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc18" id="disc18" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc18"];?>"></td>
			<td><input type="text" name="total18"  id="total18" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_hc_dir"];?>"></td>
		</tr>
		<?php } ?>
		<?php if($selinvoice["air_dvd_sc_pc"]!="" && $selinvoice["air_dvd_sc_pc"]!=0){?>
		<tr class="tr_new" >
			<td>19</td>
			<td colspan="2">AIR DVD SC/HC/PC<input type="hidden" name="air_dvd_sc_pc" value="AIR DVD SC/HC/PC"></td>
			<td><input type="text" name="" class="inputfield" placeholder="HSN/SAC" value="<?php echo $HSN_SACS["HSN_SAC19"];?>"></td>
			<td><input type="text" name="qty19"  id="qty19" class="inputfield" placeholder="Quantity" value="<?php echo $selQuantity["qty19"];?>"></td>
			<td><input type="text" name="rate19" id="rate19" class="inputfield" placeholder="Rate" value="<?php echo $Rates["rate19"];?>"></td>
			<td><input type="text" name="per19" id="per19" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc19" id="disc19" class="inputfield" placeholder="Discount %" value="<?php echo $invoice_discs["disc19"];?>"></td>
			<td><input type="text" name="total19"  id="total19" class="inputfield total_value" placeholder="Total" value="<?php echo $selinvoice["air_dvd_sc_pc"];?>"></td>
		</tr>
		<?php } ?>
		<tr style="font-weight: bold;border-top: 1px solid black;">
		    <td colspan="8" style="text-align: right;padding-right: 10px;">Sub Total</td>
		    <td onclick="all_total();" id="all_total"><?php echo $total =$selinvoice["air_dvd_alj"]+$selinvoice["air_dvd_bhcr"]+$selinvoice["air_dvd_crilj"]+$selinvoice["air_dvd_hc"]+$selinvoice["air_dvd_hc_crilj"]+$selinvoice["air_dvd_hc_dir"]+$selinvoice["air_dvd_hc_pc"]+$selinvoice["air_dvd_pc"]+$selinvoice["air_dvd_pc_dir"]+$selinvoice["air_dvd_sc"]+$selinvoice["air_dvd_sc_crilj"]+$selinvoice["air_dvd_sc_dir"]+$selinvoice["air_dvd_sc_hc"]+$selinvoice["ir_dvd_sc_hc_crilj"]+$selinvoice["air_dvd_sc_hc_crilj_dir"]+$selinvoice["air_dvd_sc_hc_crilj_pc"]+$selinvoice["air_dvd_sc_hc_crilj_pc_dir"]+$selinvoice["air_dvd_sc_hc_dir"]+$selinvoice["air_dvd_sc_pc"]?></td>
		 </tr>
		 <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output CGST</td>
		  	<td colspan="5"><input type="text" name="" id="cgst"  class="new_filed"><?php echo $selinvoice["output_gst"]; ?>%</td>
		  	<td style="font-weight: bold;" id="cgst_total"><?php echo $cgsttotalss = $total*$selinvoice["output_gst"]/100;?></td>
		  </tr>

		  <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output SGST</td>
		  	<td colspan="5"><input type="text" name="" id="sgst"  class="new_filed"><?php echo $selinvoice["output_sgst"]; ?>%</td>
		  	<td style="font-weight: bold;" id="sgst_total"><?php echo $cgsttotal = $total*$selinvoice["output_sgst"]/100;?></td>
		  </tr>

		  <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output IGST</td>
		  	<td colspan="5"><input type="text" name="" id="igst"  class="new_filed"><?php echo $selinvoice["output_igst"]; ?>%</td>
		  	<td style="font-weight: bold;" id="igst_total"><?php echo $cgsttotals = $total*$selinvoice["output_igst"]/100;?></td>
		  </tr>
		<tr style="font-weight: bold;border-top: 1px solid black;">
		      <td class=""> </td>
		      <td colspan="2" class="" style="text-align: right;padding-right: 10px;">Total</td>
		      <td class=""> </td> 
		      <td class=""><?php echo $totalnos=$selQuantity["qty1"]+$selQuantity["qty2"]+$selQuantity["qty3"]+$selQuantity["qty4"]+$selQuantity["qty5"]+$selQuantity["qty6"]+$selQuantity["qty7"]+$selQuantity["qty8"]+$selQuantity["qty9"]+$selQuantity["qty10"]+$selQuantity["qty11"]+$selQuantity["qty12"]+$selQuantity["qty13"]+$selQuantity["qty14"]+$selQuantity["qty15"]+$selQuantity["qty16"]+$selQuantity["qty17"]+$selQuantity["qty18"]+$selQuantity["qty19"]?> Nos.</td>
		      <td class=""> </td>
		      <td class=""> </td>
		      <td class=""> </td>
		      <td  id="gross_total"><?php echo $total; ?></td>
		  </tr>
		  <tr>
		  	<td colspan="9">
		  		<?php $ex = mysql_query("SELECT * FROM `registration` WHERE `id`="".$row["ex_id"].""");?>
		  		<?php $selEx = mysql_fetch_array($ex);?>
		  		<b style="color: red;">Executive Sales Name - <?php echo $selEx["name"];?> <?php echo $selEx["last_name"];?></b>
		  	</td>
		  </tr>
		<tr>
			<td colspan="9">Amount Chargeable (in words)<?php
  /**
   * Created by PhpStorm.
   * User: sakthikarthi
   * Date: 9/22/14
   * Time: 11:26 AM
   * Converting Currency Numbers to words currency format
   */
$number = $total;
   $no = round($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array("0" => "", "1" => "one", "2" => "two",
    "3" => "three", "4" => "four", "5" => "five", "6" => "six",
    "7" => "seven", "8" => "eight", "9" => "nine",
    "10" => "ten", "11" => "eleven", "12" => "twelve",
    "13" => "thirteen", "14" => "fourteen",
    "15" => "fifteen", "16" => "sixteen", "17" => "seventeen",
    "18" => "eighteen", "19" =>"nineteen", "20" => "twenty",
    "30" => "thirty", "40" => "forty", "50" => "fifty",
    "60" => "sixty", "70" => "seventy",
    "80" => "eighty", "90" => "ninety");
   $digits = array("", "hundred", "thousand", "lakh", "crore");
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? "s" : null;
        $hundred = ($counter == 1 && $str[0]) ? " and " : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode("", $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : "";
  echo $result . "Rupees  " . $points . " Paise";
 ?> 
				<!-- <input type="text" name="number" placeholder="Number OR Amount" onkeyup="word.innerHTML=convertNumberToWords(this.value)" /> -->
				<span style="text-align: right;float: right;padding-right: 10px;">E. & O.E</span><br>
		 		<b>INR &nbsp;<span id="word"></span></b>
		 	
		 </td>
		</tr>
			<tr style="text-align: center;" >
				<td rowspan="2" colspan="2">HSN/SAC</td>
				<td rowspan="2">Taxable Value </td>
				<td colspan="2">Central Tax </td>
				<td colspan="2">Integrated Tax </td>
				<td colspan="2">State Tax</td>
			</tr>
			<tr style="text-align: center;">
				<td>Rate</td>
				<td>Amount</td>
				<td>Rate</td>
				<td>Amount</td>
				<td>Rate</td>
				<td>Amount</td>
			</tr>
			<tr class="tr_new">
				<td style="text-align: left;padding-left: 10px;" colspan="2"><input type="text" name="" placeholder="HSN/SAC" class="inputfield"></td>
				<td id="all_total1"><?php echo $total; ?></td>
				<td id="cgst1"><?php echo $selinvoice["output_gst"]; ?></td>
				<td id="cgst_total1"><?php echo $cgsttotalss = $total*$selinvoice["output_gst"]/100;?></td>
				<td id="igst1"><?php echo $selinvoice["output_igst"]; ?></td>
				<td id="igst_total1"><?php echo $cgsttota = $total*$selinvoice["output_igst"]/100;?></td>
				<td id="sgst1"><?php echo $selinvoice["output_sgst"]; ?></td>
				<td id="sgst_total1"><?php echo $cgsttotasd = $total*$selinvoice["output_sgst"]/100;?></td>
			</tr>
		<tr>
			<td colspan="12">
				<div class="last_1">
				<div class="last2">
					<p class="last_p">Tax Amount (in words) :<b></b></p>
					<p style="font-size: 12px;width: 75%;padding-left: 5px;">Company’s PAN : &nbsp;&nbsp;&nbsp;<b>AAGFA0718H</b><br><br>
					<u><b>Declaration</b></u><br>
					We declare that this invoice shows the actual price of
					the goods described and that all particulars are true and
					correct.</p>
				</div>
				<div class="last3">
					Company’s Bank Details<br>
					<table style="border: none;width: 100%">
						<tr>
							<td>Bank Name </td>
							<td>:</td>
							<td><b>H.D.F.C. Bank Ltd. (Curr A/c)</b></td>
						</tr>
						<tr>
							<td>A/c No.</td>
							<td>:</td>
							<td><b>10098630000151</b></td>
						</tr>
						<tr>
							<td>Branch & IFS Code</td>
							<td>:</td>
							<td><b>Dhantoli & HDFC0001009</b></td>
						</tr>
					</table>
					<div class="last4"><b>for  M/s AIR INFOTECH <!-- (2014-2016) Current - (From 1-Apr-2016) --></b>
						<div class="last4_aut">Authorised Signatory</div>
					</div>
				</div>
			</div>
			</td>
		</tr>
	</tbody>
</table>
<div class="wrapper_div">
		<p>SUBJECT TO NAGPUR JURISDICTION</p>
		<p>This is a Computer Generated Invoice</p>
	</div>
</div>
</div>
</form>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
</body>
</html>'