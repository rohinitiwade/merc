<?php session_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<title>Editable Invoice</title>
	<link rel='stylesheet' type='text/css' href='style_invoice.css' />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
   <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
   <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
   <!-- ================== END PAGE LEVEL STYLE ================== -->
   <?php  $_GET['id'];?>
	<script>
		$(document).ready(function(){
			$('input[name]').on("keyup", function(){
		
			qty1 = Number(document.getElementById('qty1').value);
			rate1 = Number(document.getElementById('rate1').value);
			subtotal1= qty1 * rate1;
			disc1 = Number(document.getElementById('disc1').value);
			minus_disc1= disc1 * 0.01;
			mis1 = subtotal1 * (minus_disc1);
			c1 = subtotal1 - mis1;
			document.getElementById('total1').value = c1;
			document.getElementById('total1').innerHTML = c1;

			qty2 = Number(document.getElementById('qty2').value);
			rate2 = Number(document.getElementById('rate2').value);
			subtotal2= qty2 * rate2;
			disc2 = Number(document.getElementById('disc2').value);
			minus_disc2= disc2 * 0.01;
			mis2 = subtotal2 * (minus_disc2);
			c2 = subtotal2 - mis2;
			document.getElementById('total2').value = c2;
		
			qty3 = Number(document.getElementById('qty3').value);
			rate3 = Number(document.getElementById('rate3').value);
			subtotal3= qty3 * rate3;
			disc3 = Number(document.getElementById('disc3').value);
			minus_disc3= disc3 * 0.01;
			mis3 = subtotal3 * (minus_disc3);
			c3 = subtotal3 - mis3;
			document.getElementById('total3').value = c3;
		
			qty4 = Number(document.getElementById('qty4').value);
			rate4 = Number(document.getElementById('rate4').value);
			subtotal4= qty4 * rate4;
			disc4 = Number(document.getElementById('disc4').value);
			minus_disc4= disc4 * 0.01;
			mis4 = subtotal4 * (minus_disc4);
			c4 = subtotal4 - mis4;
			document.getElementById('total4').value = c4;
	
			qty5 = Number(document.getElementById('qty5').value);
			rate5 = Number(document.getElementById('rate5').value);
			subtotal5= qty5 * rate5;
			disc5 = Number(document.getElementById('disc5').value);
			minus_disc5= disc5 * 0.01;
			mis5 = subtotal5 * (minus_disc5);
			c5 = subtotal5 - mis5;
			document.getElementById('total5').value = c5;

			qty6 = Number(document.getElementById('qty6').value);
			rate6 = Number(document.getElementById('rate6').value);
			subtotal6= qty6 * rate6;
			disc6 = Number(document.getElementById('disc6').value);
			minus_disc6= disc6 * 0.01;
			mis6 = subtotal6 * (minus_disc6);
			c6 = subtotal6 - mis6;
			document.getElementById('total6').value = c6;

			qty7 = Number(document.getElementById('qty7').value);
			rate7 = Number(document.getElementById('rate7').value);
			subtotal7= qty7 * rate7;
			disc7 = Number(document.getElementById('disc7').value);
			minus_disc7= disc7 * 0.01;
			mis7 = subtotal7 * (minus_disc7);
			c7 = subtotal7 - mis7;
			document.getElementById('total7').value = c7;
		
			qty8 = Number(document.getElementById('qty8').value);
			rate8 = Number(document.getElementById('rate8').value);
			subtotal8= qty8 * rate8;
			disc8 = Number(document.getElementById('disc8').value);
			minus_disc8= disc8 * 0.01;
			mis8 = subtotal8 * (minus_disc8);
			c8 = subtotal8 - mis8;
			document.getElementById('total8').value = c8;
	
			qty9 = Number(document.getElementById('qty9').value);
			rate9 = Number(document.getElementById('rate9').value);
			subtotal9= qty9 * rate9;
			disc9 = Number(document.getElementById('disc9').value);
			minus_disc9= disc9 * 0.01;
			mis9 = subtotal9 * (minus_disc9);
			c9 = subtotal9 - mis9;
			document.getElementById('total9').value = c9;
		
			qty10 = Number(document.getElementById('qty10').value);
			rate10 = Number(document.getElementById('rate10').value);
			subtotal10= qty10 * rate10;
			disc10 = Number(document.getElementById('disc10').value);
			minus_disc10= disc10 * 0.01;
			mis10 = subtotal10 * (minus_disc10);
			c10 = subtotal10 - mis10;
			document.getElementById('total10').value = c10;
	
			qty11 = Number(document.getElementById('qty11').value);
			rate11 = Number(document.getElementById('rate11').value);
			subtotal11= qty11 * rate11;
			disc11 = Number(document.getElementById('disc11').value);
			minus_disc11= disc11 * 0.01;
			mis11 = subtotal11 * (minus_disc11);
			c11 = subtotal11 - mis11;
			document.getElementById('total11').value = c11;
		
			qty12 = Number(document.getElementById('qty12').value);
			rate12 = Number(document.getElementById('rate12').value);
			subtotal12= qty12 * rate12;
			disc12 = Number(document.getElementById('disc12').value);
			minus_disc12= disc12 * 0.01;
			mis12 = subtotal12 * (minus_disc12);
			c12 = subtotal12 - mis12;
			document.getElementById('total12').value = c12;
		
			qty13 = Number(document.getElementById('qty13').value);
			rate13 = Number(document.getElementById('rate13').value);
			subtotal13= qty13 * rate13;
			disc13 = Number(document.getElementById('disc13').value);
			minus_disc13= disc13 * 0.01;
			mis13 = subtotal13 * (minus_disc13);
			c13 = subtotal13 - mis13;
			document.getElementById('total13').value = c13;
		
			qty14 = Number(document.getElementById('qty14').value);
			rate14 = Number(document.getElementById('rate14').value);
			subtotal14= qty14 * rate14;
			disc14 = Number(document.getElementById('disc14').value);
			minus_disc14= disc14 * 0.01;
			mis14 = subtotal14 * (minus_disc14);
			c14 = subtotal14 - mis14;
			document.getElementById('total14').value = c14;
	
			qty15 = Number(document.getElementById('qty15').value);
			rate15 = Number(document.getElementById('rate15').value);
			subtotal15= qty15 * rate15;
			disc15 = Number(document.getElementById('disc15').value);
			minus_disc15= disc15 * 0.01;
			mis15 = subtotal15 * (minus_disc15);
			c15 = subtotal15 - mis15;
			document.getElementById('total15').value = c15;
		
			qty16 = Number(document.getElementById('qty16').value);
			rate16 = Number(document.getElementById('rate16').value);
			subtotal16= qty16 * rate16;
			disc16 = Number(document.getElementById('disc16').value);
			minus_disc16= disc16 * 0.01;
			mis16 = subtotal16 * (minus_disc16);
			c16 = subtotal16 - mis16;
			document.getElementById('total16').value = c16;
		
			qty17 = Number(document.getElementById('qty17').value);
			rate17 = Number(document.getElementById('rate17').value);
			subtotal17= qty17 * rate17;
			disc17 = Number(document.getElementById('disc17').value);
			minus_disc17= disc17 * 0.01;
			mis17 = subtotal17 * (minus_disc17);
			c17 = subtotal17 - mis17;
			document.getElementById('total17').value = c17;

			qty18 = Number(document.getElementById('qty18').value);
			rate18 = Number(document.getElementById('rate18').value);
			subtotal18= qty18 * rate18;
			disc18 = Number(document.getElementById('disc18').value);
			minus_disc18= disc18 * 0.01;
			mis18 = subtotal18 * (minus_disc18);
			c18 = subtotal18 - mis18;
			document.getElementById('total18').value = c18;
		
			qty19 = Number(document.getElementById('qty19').value);
			rate19 = Number(document.getElementById('rate19').value);
			subtotal19= qty19 * rate19;
			disc19 = Number(document.getElementById('disc19').value);
			minus_disc19= disc19 * 0.01;
			mis19 = subtotal19 * (minus_disc19);
			c19 = subtotal19 - mis19;
			document.getElementById('total19').value = c19;

			total_quat = qty1+qty2+qty3+qty4+qty5+qty6+qty7+qty8+qty9+qty10+qty11+qty12+qty13+qty14+qty15+qty16+qty17+qty18+qty19;
			document.getElementById('total_quat').value = total_quat;
			document.getElementById('total_quat').innerHTML = total_quat;


			result = c1+c2+c3+c4+c5+c6+c7+c8+c9+c10+c11+c12+c13+c14+c15+c16+c17+c18+c19;
			document.getElementById('all_total').value = result;
			document.getElementById('all_total').innerHTML = result;

			document.getElementById('all_total1').value = result;
			document.getElementById('all_total1').innerHTML = result;

			 igst = Number(document.getElementById('igst').value);
			 igst_total = result * (igst / 100);

			 document.getElementById('igst_total').value = igst_total;
			 document.getElementById('igst_total').innerHTML = igst_total;

			 sgst = Number(document.getElementById('sgst').value);
			 sgst_total = result * (sgst / 100);
	
			 document.getElementById('sgst_total').value = sgst_total;
			 document.getElementById('sgst_total').innerHTML = sgst_total;

			 cgst = Number(document.getElementById('cgst').value);
			 cgst_total = result * (cgst / 100);
	
			 document.getElementById('cgst_total').value = cgst_total;
			 document.getElementById('cgst_total').innerHTML = cgst_total;
			
			Gross_Total = result + igst_total + cgst_total + sgst_total;

			document.getElementById('gross_total').value = Gross_Total;
			document.getElementById('gross_total').innerHTML = Gross_Total;

			document.getElementById('sgst1').innerHTML = sgst;
			document.getElementById('cgst1').innerHTML = cgst;
			document.getElementById('igst1').innerHTML = igst;
			document.getElementById('igst_total1').innerHTML = igst_total;
			document.getElementById('sgst_total1').innerHTML = sgst_total;
			document.getElementById('cgst_total1').innerHTML = cgst_total;

		});
		});
		
	</script>
	<script>
		function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
	</script>
</head>
<body>
	<style>
		.wrapper
		{
			height: auto !important;
		}
	</style>
	<?php $sql = mysql_query("SELECT `name`,`last_name`,`address`,`id`,`state_name`,`city`,`ex_id` FROM `order_form` WHERE `id`='".$_GET['id']."'");
	$row = mysql_fetch_array($sql);?>
<form name="MyForm" action="invoice/submit.php" method="POST">
<div class="wrapper">
	<div class="table_box">
		<div class="head_invocie"><h4 style="color: red;">Tax Invoice</h4></div>
		<input type="hidden" name="client_id" value="<?php echo $_GET['id'];?>">
<table border="1" cellpadding="1" cellspacing="1" style="border-collapse: collapse;width: 100%;">
	<tbody>
		<tr>
			<td colspan="5" rowspan="3">
				<span class="ff1 ">M/s AIR INFOTECH<!--  (19014-2016) Current - (From 1-Apr-2016) --></span><br />
				Flat No. 401, B B Tower,<br />
				58/B Shankar Nagar East<br />
				Behind Sai Vatika, Nagpur<br />
				GSTIN/UIN: 27AAGFA0718H1ZR<br />
				Contact : 0712 - 2521858, 2544058<br />
				E-Mail : info@airinfotech.in</td>
			<td colspan="3">
				Invoice No.<br />
				<input type="text" name="invoice_no" class="half_inputfield" placeholder="AIRINFO/17-18/001" value="AIRINFO/17-18/<?php echo $_GET['id'];?>" readonly style="color: red;">
				</td>
			<td>
				Dated<br />
				<input type="text" name="dated" class="half_inputfield" placeholder="Select Date" id="datepicker">
				</td>
		</tr>
		<tr>
			<td colspan="3">
				Delivery Note<br/>
				<input type="text" name="delivery_note" class="half_inputfield">
			</td>
			<td>
				Mode/Terms of Payment<br />
				<input type="text" name="payment_mode" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="3">
				Supplier&rsquo;s Ref.<br />
				<input type="text" name="supplier" class="half_inputfield" placeholder="AIRINFO/BB/17-18"></td>
			<td>
				Other Reference(s)<br />
				<input type="text" name="other_reference" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="5" rowspan="4">
				Buyer,<br />
				<textarea class="buyer_text" style="color: green;" name="buyer_details"><?php echo $row['name'];?> <?php echo $row['last_name'];?>
					<?php echo $row['address'];?>, <?php  $state = mysql_query("SELECT * FROM `states` WHERE `state_id`='".$row['state_name']."'"); $selstate = mysql_fetch_array($state);?><?php echo $selstate['state_name']; ?><?php  $city = mysql_query("SELECT * FROM `cities` WHERE `city_id`='".$row['city']."'"); $selcity = mysql_fetch_array($city);?>,  <?php echo $selcity['city_name']; ?></textarea>
				<!-- Khapre R L<br />
				Nagpur<br /> -->
				<br>
				PAN/IT No : <input type="text" name="pan_no" placeholder="" class="half_inputfield"><br />
				Place of Supply : <input type="text" name="" placeholder="Maharashtra" class="half_inputfield"></td>
			<td colspan="3">
				Buyer&rsquo;s Order No.<br />
				<input type="text" name="order_no" class="half_inputfield" value="<?php echo $_GET['id'];?>" style="color: red;" readonly>
				</td>
			<td>
				Dated<br />
			<input type="text" name="Dated1" class="half_inputfield" placeholder="Select Date" id="datepicker1">

<!-- 				<input type="text" name="Dated1" class="half_inputfield" >
 -->				</td>
		</tr>
		<tr>
			<td colspan="3">
				Despatch Document No.<br />
				<input type="text" name="despatch_document_no" class="half_inputfield" >
				</td>
			<td>
				Delivery Note Date<br />
				<input type="text" name="delivery_note_date" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="3">
				Despatched through<br />
				<input type="text" name="despatch_through" class="half_inputfield" >
				</td>
			<td>
				Destination<br />
				<input type="text" name="designation" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="4" rowspan="1">
				Terms of Delivery
				<br/>
				<textarea class="terms_textarea" name="term_of_delivery">
					
				</textarea>
			</td>
		</tr>
		<tr>
			<td rowspan="1" width="30">
				Sr.No</td> 
			<td rowspan="1" colspan="2" width="600">
				Description of Goods</td>
			<td rowspan="1" width="80">
				HSN/SAC</td>
			<td rowspan="1">
				Quantity</td>
			<td rowspan="1" width="80">
				Rate</td>
			<td rowspan="1">
				per</td>
			<td rowspan="1">
				Disc. %</td>
			<td rowspan="1" width="100">
				Amount</td>
		</tr>
		<tr class="tr_new">
			<td>1</td>
			<td colspan="2">AIR DVD ALJ<input type="hidden" name="air_dvd_alj" value="AIR DVD ALJ"></td>
			<td><input type="text" name="HSN_SAC1" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty1"  id="qty1" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate1" id="rate1" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per1" id="per1" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc1" id="disc1" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total1"  id="total1" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>2</td>
			<td colspan="2">AIR DVD BHCR <input type="hidden" name="air_dvd_bhcr" value="AIR DVD BHCR"></td>
			<td><input type="text" name="HSN_SAC2" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty2"  id="qty2" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate2" id="rate2" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per2" id="per2" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc2" id="disc2" class="inputfield" placeholder="Discount %" ></td>
			<td><input type="text" name="total2"  id="total2" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>3</td>
			<td colspan="2">AIR DVD CRILJ<input type="hidden" name="air_dvd_crilj" value="AIR DVD CRILJ"></td>
			<td><input type="text" name="HSN_SAC3" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty3"  id="qty3" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate3" id="rate3" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per3" id="per3" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc3" id="disc3" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total3"  id="total3" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>4</td>
			<td colspan="2">AIR DVD HC<input type="hidden" name="air_dvd_hc" value="AIR DVD HC"></td>
			<td><input type="text" name="HSN_SAC4" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty4"  id="qty4" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate4" id="rate4" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per4" id="per4" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc4" id="disc4" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total4"  id="total4" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>5</td>
			<td colspan="2">AIR DVD HC/CRILJ<input type="hidden" name="air_dvd_hc_crilj" value="AIR DVD HC/CRILJ"> </td>
			<td><input type="text" name="HSN_SAC5" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty5"  id="qty5" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate5" id="rate5" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per5" id="per5" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc5" id="disc5" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total5"  id="total5" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>6</td>
			<td colspan="2">AIR DVD HC DIR<input type="hidden" name="air_dvd_hc_dir" value="AIR DVD HC DIR"> </td>
			<td><input type="text" name="HSN_SAC6" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty6"  id="qty6" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate6" id="rate6" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per6" id="per6" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc6" id="disc6" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total6"  id="total6" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>7</td>
			<td colspan="2">AIR DVD HC/PC<input type="hidden" name="air_dvd_hc_pc" value="AIR DVD HC PC"> </td>
			<td><input type="text" name="HSN_SAC7" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty7"  id="qty7" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate7" id="rate7" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per7" id="per7" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc7" id="disc7" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total7"  id="total7" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>8</td>
			<td colspan="2">AIR DVD PC <input type="hidden" name="air_dvd_pc" value="AIR DVD PC"> </td>
			<td><input type="text" name="HSN_SAC8" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty8"  id="qty8" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate8" id="rate8" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per8" id="per8" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc8" id="disc8" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total8"  id="total8" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>9</td>
			<td colspan="2">AIR DVD PC DIR<input type="hidden" name="air_dvd_pc_dir" value="AIR DVD PC DIR"> </td>
			<td><input type="text" name="HSN_SAC9" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty9"  id="qty9" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate9" id="rate9" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per9" id="per9" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc9" id="disc9" class="inputfield" placeholder="Discount %" ></td>
			<td><input type="text" name="total9"  id="total9" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>10</td>
			<td colspan="2">AIR DVD SC<input type="hidden" name="air_dvd_sc" value="AIR DVD SC"> </td>
			<td><input type="text" name="HSN_SAC10" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty10"  id="qty10" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate10" id="rate10" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per10" id="per10" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc10" id="disc10" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total10"  id="total10" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>11</td>
			<td colspan="2"> AIR DVD SC/CRILJ <input type="hidden" name="air_dvd_sc_crilj" value="AIR DVD SC/CRILJ"></td>
			<td><input type="text" name="HSN_SAC11" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty11"  id="qty11" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate11" id="rate11" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per11" id="per11" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc11" id="disc11" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total11"  id="total11" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>12</td>
			<td colspan="2">AIR DVD SC DIR<input type="hidden" name="air_dvd_sc_dir" value="AIR DVD SC DIR"></td>
			<td><input type="text" name="HSN_SAC12" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty12"  id="qty12" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate12" id="rate12" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per12" id="per12" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc12" id="disc12" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total12"  id="total12" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>13</td>
			<td colspan="2"> AIR DVD SC/HC<input type="hidden" name="air_dvd_sc_hc" value="AIR DVD SC/HC"></td>
			<td><input type="text" name="HSN_SAC13" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty13"  id="qty13" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate13" id="rate13" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per13" id="per13" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc13" id="disc13" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total13"  id="total13" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>14</td>
			<td colspan="2">AIR DVD SC/HC/CRILJ<input type="hidden" name="air_dvd_sc_hc_crilj" value="AIR DVD SC/HC/CRILJ"></td>
			<td><input type="text" name="HSN_SAC14" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty14"  id="qty14" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate14" id="rate14" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per14" id="per14" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc14" id="disc14" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total14"  id="total14" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>15</td>
			<td colspan="2"> AIR DVD SC/HC/CRILJ DIR<input type="hidden" name="air_dvd_sc_hc_crilj_dir" value="AIR DVD SC/HC/CRILJ DIR"></td>
			<td><input type="text" name="HSN_SAC15" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty15"  id="qty15" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate15" id="rate15" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per15" id="per15" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc15" id="disc15" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total15"  id="total15" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>16</td>
			<td colspan="2">AIR DVD-SC/HC/CRILJ/ PC <input type="hidden" name="air_dvd_sc_hc_crilj_pc" value="AIR DVD-SC/HC/CRILJ/ PC"></td>
			<td><input type="text" name="HSN_SAC16" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty16"  id="qty16" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate16" id="rate16" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per16" id="per16" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc16" id="disc16" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total16"  id="total16" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>17</td>
			<td colspan="2"> AIR DVD SC/HC/CRILJ/PC-DIR<input type="hidden" name="air_dvd_sc_hc_crilj_pc_dir" value="AIR DVD SC/HC/CRILJ/PC-DIR"></td>
			<td><input type="text" name="HSN_SAC17" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty17"  id="qty17" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate17" id="rate17" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per17" id="per17" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc17" id="disc17" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total17"  id="total17" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>18</td>
			<td colspan="2">AIR DVD SC/HC DIR<input type="hidden" name="air_dvd_sc_hc_dir" value="AIR DVD SC/HC DIR"></td>
			<td><input type="text" name="HSN_SAC18" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty18"  id="qty18" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate18" id="rate18" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per18" id="per18" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc18" id="disc18" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total18"  id="total18" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr class="tr_new">
			<td>19</td>
			<td colspan="2">AIR DVD SC/HC/PC<input type="hidden" name="air_dvd_sc_pc" value="AIR DVD SC/HC/PC"></td>
			<td><input type="text" name="HSN_SAC19" class="inputfield" placeholder="HSN/SAC"></td>
			<td><input type="text" name="qty19"  id="qty19" class="inputfield" placeholder="Quantity"></td>
			<td><input type="text" name="rate19" id="rate19" class="inputfield" placeholder="Rate" ></td>
			<td><input type="text" name="per19" id="per19" class="inputfield" placeholder="Per"></td>
			<td><input type="text" name="disc19" id="disc19" class="inputfield" placeholder="Discount %"></td>
			<td><input type="text" name="total19"  id="total19" class="inputfield total_value" placeholder="Total"></td>
		</tr>
		<tr style="font-weight: bold;border-top: 1px solid black;">
		    <td colspan="8" style="text-align: right;padding-right: 10px;">Sub Total</td>
		    <td onclick="all_total();" id="all_total"></td>
		 </tr>
		 <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output CGST</td>
		  	<td colspan="5"><input type="text" name="output_gst" id="cgst"  class="new_filed">%</td>
		  	<td style="font-weight: bold;" id="cgst_total"></td>
		  </tr>

		  <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output SGST</td>
		  	<td colspan="5"><input type="text" name="output_sgst" id="sgst"  class="new_filed">%</td>
		  	<td style="font-weight: bold;" id="sgst_total"></td>
		  </tr>

		  <tr class="tr_new" >
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output IGST</td>
		  	<td colspan="5"><input type="text" name="output_igst" id="igst"  class="new_filed">%</td>
		  	<td style="font-weight: bold;" id="igst_total"></td>
		  </tr>
		<tr style="font-weight: bold;border-top: 1px solid black;">
		      <td class=""> </td>
		      <td colspan="2" class="" style="text-align: right;padding-right: 10px;">Total</td>
		      <td class=""> </td> 
		      <td class="" id="total_quat">Nos.</td>
		      <td class=""> </td>
		      <td class=""> </td>
		      <td class=""> </td>
		      <td  id="gross_total"></td>
		  </tr>
		  <tr>
		  	<td colspan="9">
		  		<?php $ex = mysql_query("SELECT * FROM `registration` WHERE `id`='".$row['ex_id']."'");?>
		  		<?php $selEx = mysql_fetch_array($ex);?>
		  		<b style="color: red;">Executive Sales Name - <?php echo $selEx['name'];?> <?php echo $selEx['last_name'];?></b>
		  	</td>
		  </tr>
		<tr>
			<td colspan="9">Amount Chargeable (in words)
				<input type="text" name="number" placeholder="Number OR Amount" onkeyup="word.innerHTML=convertNumberToWords(this.value)" />
				<span style="text-align: right;float: right;padding-right: 10px;">E. & O.E</span><br>
		 		<b>INR &nbsp;<span id="word"></span></b>
		 	
		 </td>
		</tr>
			<tr style="text-align: center;" >
				<td rowspan="2" colspan="2">HSN/SAC</td>
				<td rowspan="2">Taxable Value </td>
				<td colspan="2">Central Tax </td>
				<td colspan="2">Integrated Tax </td>
				<td colspan="2">State Tax</td>
			</tr>
			<tr style="text-align: center;">
				<td>Rate</td>
				<td>Amount</td>
				<td>Rate</td>
				<td>Amount</td>
				<td>Rate</td>
				<td>Amount</td>
			</tr>
			<tr class="tr_new">
				<td style="text-align: left;padding-left: 10px;" colspan="2"><input type="text" name="" placeholder="HSN/SAC" class="inputfield"></td>
				<td id="all_total1"></td>
				<td id="cgst1"></td>
				<td id="cgst_total1"></td>
				<td id="igst1"></td>
				<td id="igst_total1"></td>
				<td id="sgst1"></td>
				<td id="sgst_total1"></td>
			</tr>
		<tr>
			<td colspan="12">
				<div class="last_1">
				<div class="last2">
					<p class="last_p">Tax Amount (in words) :<b><input type="text" name="" class="inputfield" placeholder="in words"></b></p>
					<p style="font-size: 12px;width: 75%;padding-left: 5px;">Company’s PAN : &nbsp;&nbsp;&nbsp;<b>AAGFA0718H</b><br><br>
					<u><b>Declaration</b></u><br>
					We declare that this invoice shows the actual price of
					the goods described and that all particulars are true and
					correct.</p>
				</div>
				<div class="last3">
					Company’s Bank Details<br>
					<table style="border: none;width: 100%">
						<tr>
							<td>Bank Name </td>
							<td>:</td>
							<td><b>H.D.F.C. Bank Ltd. (Curr A/c)</b></td>
						</tr>
						<tr>
							<td>A/c No.</td>
							<td>:</td>
							<td><b>10098630000151</b></td>
						</tr>
						<tr>
							<td>Branch & IFS Code</td>
							<td>:</td>
							<td><b>Dhantoli & HDFC0001009</b></td>
						</tr>
					</table>
					<div class="last4"><b>for  M/s AIR INFOTECH <!-- (2014-2016) Current - (From 1-Apr-2016) --></b>
						<div class="last4_aut">Authorised Signatory</div>
					</div>
				</div>
			</div>
			</td>
		</tr>
	</tbody>
</table>
<div class="wrapper_div">
		<p>SUBJECT TO NAGPUR JURISDICTION</p>
		<p>This is a Computer Generated Invoice</p>
	</div>
</div>
</div>
<input type="submit" value="Submit" class="btn btn-primary" style="text-align: center;" name="invoice">
</form>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker();
    $( "#datepicker1" ).datepicker();
  } );
  </script>
</body>
</html>