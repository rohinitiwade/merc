<?php include("../includes/connection.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<title>Editable Invoice</title>
	<link rel='stylesheet' type='text/css' href='style_invoice.css' />

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
   <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
   <link href="assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <link href="assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
   <!-- ================== END PAGE LEVEL STYLE ================== -->
</head>
<body>
<div class="wrapper">
	<div class="table_box">
		<div class="head_invocie">Tax Invoice</div>
<form id="form" method="POST" action="">
<table border="1" cellpadding="1" cellspacing="1" style="border-collapse: collapse;width: 100%;">
	<tbody>
		<tr>
			<td colspan="5" rowspan="3">
				<span class="ff1 ">M/s AIR INFOTECH (2014-2016) Current - (From 1-Apr-2016)</span><br />
				Flat No. 401, B B Tower,<br />
				58/B Shankar Nagar East<br />
				Behind Sai Vatika, Nagpur<br />
				GSTIN/UIN: 27AAGFA0718H1ZR<br />
				Contact : 0712 - 2521858, 2544058<br />
				E-Mail : info@airinfotech.in</td>
			<td colspan="4">
				Invoice No.<br />
				<input type="text" name="" class="half_inputfield" placeholder="AIRINFO/17-18/001">
				</td>
			<td>
				Dated<br />
				<input type="text" name="" class="half_inputfield" placeholder="14-Nov-2017">
				</td>
		</tr>
		<tr>
			<td colspan="4">
				Delivery Note<br/>
				<input type="text" name="" class="half_inputfield">
			</td>
			<td>
				Mode/Terms of Payment<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="4">
				Supplier&rsquo;s Ref.<br />
				<input type="text" name="" class="half_inputfield" placeholder="AIRINFO/BB/17-18"></td>
			<td>
				Other Reference(s)<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="5" rowspan="4">
				Buyer<br />
				<textarea class="buyer_text"></textarea>
				<!-- Khapre R L<br />
				Nagpur<br /> -->
				<br>
				PAN/IT No : <input type="text" name="" placeholder="" class="half_inputfield"><br />
				Place of Supply : <input type="text" name="" placeholder="Maharashtra" class="half_inputfield"></td>
			<td colspan="4">
				Buyer&rsquo;s Order No.<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
			<td>
				Dated<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="4">
				Despatch Document No.<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
			<td>
				Delivery Note Date<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="4">
				Despatched through<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
			<td>
				Destination<br />
				<input type="text" name="" class="half_inputfield" >
				</td>
		</tr>
		<tr>
			<td colspan="5" rowspan="1">
				Terms of Delivery
				<br/>
				<textarea class="terms_textarea">
					
				</textarea>
			</td>
		</tr>
		<tr>
			<td rowspan="1" width="30">
				Sr.No</td> 
			<td rowspan="1" colspan="2" width="600">
				Description of Goods</td>
			<td rowspan="1" width="80">
				HSN/SAC</td>
			<td rowspan="1">
				GST Rate</td>
			<td rowspan="1">
				Quantity</td>
			<td rowspan="1" width="80">
				Rate</td>
			<td rowspan="1">
				per</td>
			<td rowspan="1">
				Disc. %</td>
			<td rowspan="1" width="100">
				Amount</td>
		</tr>
		<?php $sql = mysql_query("SELECT * FROM `product_name`");
		$i=1;
		while($row = mysql_fetch_array($sql)){?>
		<tr class="tr_new">
			<td><?php echo $i;?></td>
			<td colspan="2"><?php echo $row['productName'];?></td>
			<td><input type="text" name="" class="inputfield" placeholder="85238020"></td>
			<td><input type="text" name="GST" id="GST<?php echo $row['id'];?>" class="inputfield" placeholder="gst %"></td>
			<td><input type="text" class="inputfield" placeholder="qty" name="QTY" id="QTY<?php echo $row['id'];?>" ></td>
			<td><input type="text" name="PPRICE" id="PPRICE<?php echo $row['id'];?>"  class="inputfield" placeholder="rate"></td>
			<td><input type="text" name="per_price" id="per_price<?php echo $row['id'];?>" class="inputfield" placeholder="per"></td>
			<td><input type="text" name="disc" id="disc<?php echo $row['id'];?>" class="inputfield" placeholder="12" onKeyUp="multiply<?php echo $row['id'];?>()"></td>
			<td><input type="text" name="TOTAL" id="TOTAL<?php echo $row['id'];?>" class="inputfield" placeholder="Amount"></td>
		</tr>
		<script>
			function multiply<?php echo $row['id'];?>() {
  qty1 = Number(document.getElementById('QTY<?php echo $row['id'];?>').value);
  price1 = Number(document.getElementById('PPRICE<?php echo $row['id'];?>').value);
  subtotal1= qty1 * price1;
  GST1 = Number(document.getElementById('GST<?php echo $row['id'];?>').value);
  cal_gst1 = subtotal1 * (GST1 / 100);
  per_price1 = Number(document.getElementById('per_price<?php echo $row['id'];?>').value);
  sub_Amount1= subtotal1 + cal_gst1 + per_price1;
  disc1 = Number(document.getElementById('disc<?php echo $row['id'];?>').value);
  minus_disc1= disc1 * 0.01;
  mis1 = sub_Amount1 * (minus_disc1);
  c1 = sub_Amount1 - mis1;
  document.getElementById('TOTAL<?php echo $row['id'];?>').value = c1;
  priceTot = 0;
  priceTot = c1 + c1;
  document.getElementById('all_total').value=priceTot;
    document.getElementById('all_total').innerHTML=priceTot;
 }
/* function all_total(){
 	alert(priceTot);
 }*/

		</script>
		
		<?php $i++; } ?>
		
		 <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output CGST</td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td style="font-weight: bold;"><input type="text" name="" class="inputfield" placeholder="180.00"></td>
		  </tr>

		  <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output SGST</td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td style="font-weight: bold;"><input type="text" name="" class="inputfield" placeholder="180.00"></td>
		  </tr>

		  <tr class="tr_new">
		  	<td></td>
		  	<td colspan="2" style="font-weight: bold;text-align: right;padding-right: 10px;">Output IGST</td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td></td>
		  	<td style="font-weight: bold;"><input type="text" name="" class="inputfield" placeholder="180.00"></td>
		  </tr>
		<tr style="font-weight: bold;border-top: 1px solid black;">
		      <td class=""> </td>
		      <td colspan="2" class="" style="text-align: right;padding-right: 10px;">Total</td>
		      <td class=""> </td>
		      <td class=""> </td> 
		      <td class=""> 1 Nos.</td>
		      <td class=""> </td>
		      <td class=""> </td>
		      <td class=""> </td>
		      <td onclick="all_total();" id="all_total">2,720.00</td>
		  </tr>
		  <tr>
		  	<td colspan="10">
		  		Executive Sales Name - Vilas Choudhary
		  	</td>
		  </tr>
		<tr>
			<td colspan="10">Amount Chargeable (in words)<span style="text-align: right;float: right;padding-right: 10px;">E.& O.E</span><br>
		 		<b>INR Two Thousand Seven Hundred Twenty Only</b>
		 	
		 </td>
		</tr>
		<tr style="text-align: center;" >
				<td rowspan="2" colspan="5">HSN/SAC</td>
				<td rowspan="2">Taxable Value </td>
				<td colspan="2">Central Tax </td>
				<td colspan="2">State Tax</td>
			</tr>
			<tr style="text-align: center;">
				<td>Rate</td>
				<td>Amount</td>
				<td>Rate</td>
				<td>Amount</td>
			</tr>
			<tr class="tr_new">
				<td style="text-align: left;padding-left: 10px;" colspan="5">85238020</td>
				<td><input type="text" name="" class="inputfield" placeholder="2,000.00"></td>
				<td>18%</td>
				<td>360.00</td>
				<td>18%</td>
				<td>360.00</td>
			</tr>
			<tr class="tr_new">
				<td style="text-align: left;padding-left: 10px;" colspan="5">85238020</td>
				<td><input type="text" name="" class="inputfield" placeholder="2,000.00"></td>
				<td>18%</td>
				<td>360.00</td>
				<td>18%</td>
				<td>360.00</td>
			</tr>
			<tr style="font-weight: bold;">
				<td style="text-align: right;padding-right: 10px;" colspan="5">Total</td>
				<td><input type="text" name="" class="inputfield" placeholder="2,000.00"></td>
				<td></td>
				<td>360.00</td>
				<td>18%</td>
				<td>360.00</td>
			</tr>
		
		<tr>
			<td colspan="12">
				<div class="last_1">
				<div class="last2">
					<p class="last_p">Tax Amount (in words) :<b> INR Three Hundred Sixty Only</b></p>
					<p style="font-size: 12px;width: 75%;padding-left: 5px;">Company’s PAN : &nbsp;&nbsp;&nbsp;<b>AAGFA0718H</b><br><br>
					<u><b>Declaration</b></u><br>
					We declare that this invoice shows the actual price of
					the goods described and that all particulars are true and
					correct.</p>
				</div>
				<div class="last3">
					Company’s Bank Details<br>
					<table style="border: none;width: 100%">
						<tr>
							<td>Bank Name </td>
							<td>:</td>
							<td><b>H.D.F.C. Bank Ltd. (Curr A/c)</b></td>
						</tr>
						<tr>
							<td>A/c No.</td>
							<td>:</td>
							<td><b>10098630000151</b></td>
						</tr>
						<tr>
							<td>Branch & IFS Code</td>
							<td>:</td>
							<td><b>Dhantoli & HDFC0001009</b></td>
						</tr>
					</table>
					<div class="last4"><b>for M/s AIR INFOTECH (2014-2016) Current - (From 1-Apr-2016)</b>
						<div class="last4_aut">Authorised Signatory</div>
					</div>
				</div>
			</div>
			</td>
		</tr>
	</tbody>
</table>
</form>
<div class="wrapper_div">
		<p>SUBJECT TO NAGPUR JURISDICTION</p>
		<p>This is a Computer Generated Invoice</p>
	</div>
</div>
</div>
</body>
</html>