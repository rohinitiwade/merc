<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
<?php include("head.php");?>
<script>
  $(document).ready(function(){
    $('#data-tableReminder').dataTable();
  });
  $(document).ready(function(){
    $("#all_report").addClass("active");
    $("#converted_list").addClass("active");
  });
</script>
</head>
<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>  
    <div id="content" class="content">
        <ol class="breadcrumb" style="width: 100%;">
            <li><a href="http://crm.airinfotech.in/telecaller/">Home / </a></li>
            <li><a href="http://crm.airinfotech.in/telecaller/">Dashboard / </a></li>
            <li class="active">Reminder Client List / </li>
        </ol>
<div class="details">
  <h3 class="col-md-8" style="margin-top: 0px; color: green;">Reminder Client List&nbsp;&nbsp;<a href="http://crm.airinfotech.in/telecaller/" class="btn btn-primary btn-xs">Back To Home Page</a>
  </h3>  
   <?php include("timezone.php");?>
  <hr style="float: left;width: 100%;">
  <div class="col-md-12 table-responsive dsr_form_reports_tb">
    <table id="data-tableReminder" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
      <th>Sr.No.</th>
      <th>Client Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>Address</th>
      <th>Last Call</th>
      <th>More Day</th>
     <!--  <th>Remark</th> -->
      <th>Call History</th>
       <th>Status</th>
    </tr>
</thead>
    <tbody> 
     <?php
       $seladte = date('Y-m-d'); //6/4/2018
      $reminder =mysql_query("SELECT DISTINCT email, e.id, e.reminder_date,e.name,e.last_name,e.address,e.mobile,e.email, u.reminder_call,u.ex_id,u.client_id FROM `order_form` AS e INNER JOIN `reminder_clientstatus` AS u ON e.id = u.client_id WHERE  u.reminder_call<='".$seladte."' AND u.ex_id='".$_SESSION['user_id']."'");
      while($row=mysql_fetch_array($reminder)){ 
         $startTimeStamp = strtotime($row['reminder_call']);
         $endTimeStamp = strtotime(date("Y-m-d"));
         $timeDiff = abs($endTimeStamp - $startTimeStamp);
         $numberDays = $timeDiff/86400;  ?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo ucfirst($row['name']);?> <?php echo ucfirst($row['last_name']);?></td>
        <td><?php echo $row['email'];?></td>
        <td><?php echo $row['mobile'];?></td>
        <td><?php echo $row['address'];?></td>
        <td><?php echo $remClient['call_date'];?></td>
       <td><?php echo $numberDays; ?></td>
       <td>
          <a data-toggle="modal" data-target="#history" class="btn btn-warning btn-xs" onclick="history(<?php echo $row['updt_clientId'];?>);">Client History</a>
        </td>
         <td>
          <a data-toggle="modal" data-target="#myModalDSR" class="btn btn-primary btn-xs" onclick="Status(<?php echo $row['id'];?>,<?php echo $row['updt_clientId'];?>);">Add Reminder Status</a>
        </td>
      </tr>
     <?php  $i++;} mysql_close($connect);?>
    </tbody>
    </table>
  </div>

</div>
</div>
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #b8dbba;">
        <h4 class="modal-title" id="exampleModalLongTitle"><b style="color: black;">Quotation</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="Quotations"></div>
    </div>
  </div>
</div>


<div class="modal fade" id="count" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true"">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #b8dbba;">
        <h4 class="modal-title" id="exampleModalLongTitle"><b style="color: black;">Count Quotation </b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="countQuot"></div>
    </div>
  </div>
</div>

  <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
    
    <div id="myModalDSR" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="info"></div>
  </div>
</div>
 
<div id="history" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="his"></div>
  </div>
</div>
<script>
  function Status(id,clientid)  {
    var id=+id;
    var clientid=+clientid;
    $.ajax({
    type: "POST",
    url: "ajax/telecallerStatus.php",
    data: "id="+id+"&&ids="+clientid,
    cache: false,
    success: function(result){
    $( '#info' ).html(result);
  }
});
  }
   function countQuot(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/countQuot.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#countQuot' ).html(result);
  }
});
  }
    function history(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/dsrhistory.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#his' ).html(result);
  }
});
  }

   function Quotation(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/Quotation_send.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#Quotations' ).html(result);
  }
});
  }
</script>
<?php include("footer.php");?>
