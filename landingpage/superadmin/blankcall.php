<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
<?php include("head.php");?>
<script>
  $(document).ready(function(){
    $('#data-tableDaily').dataTable();
  });
  $(document).ready(function(){
    $("#all_report").addClass("active");
    $("#blankcall_report").addClass("active");
    $("#callDate").datepicker({
        todayHighlight: !0,
        autoclose: true,
        format:'dd/mm/yyyy'
    })
  });
</script>
</head>
    <?php include_once("header.php"); ?>
    <?php include_once("sidebar.php"); ?>  
    <div id="content" class="content">
        <ol class="breadcrumb" style="width: 100%;">
            <li><a href="http://crm.airinfotech.in/telecaller/">Home</a>/</li>
            <li><a href="http://crm.airinfotech.in/telecaller/">Dashboard</a>/</li>
            <li class="active">Blank Call List</li>
        </ol>
<div class="details">
  <h3 class="col-sm-8" style="margin-top: 0px; color: green;"><span>Blank Call List &nbsp;&nbsp;</span><a href="http://crm.airinfotech.in/telecaller/" class="btn btn-primary btn-xs">Back To Home Page</a>
  </h3>  
   <?php include("timezone.php");?>
  <hr style="float: left;width: 100%;">
  <div id="pageData"></div>
<span class="flash"></span>
     <script>
      $(document).ready(function(){
      $("#called_for").on("change",function(){
        var this_val = $(this).val();
        if(this_val == "other"){
            $("#other_reason_inp").show();          }
          else{
            $("#other_reason_inp").hide();
          }
      })
      });
    </script>
  <br/><br/><br/>
<script type="text/javascript">
$(document).ready(function(){
changePagination('0');  
});
function changePagination(pageId){
     $(".flash").show();
     $(".flash").fadeIn(400).html
                ('Loading <img src="img/loading5.gif" />');
     var dataString = 'pageId='+ pageId;
     $.ajax({
           type: "POST",
           url: "loadData.php",
           data: dataString,
           cache: false,
           success: function(result){
           $(".flash").hide();
                 $("#pageData").html(result);
           }
      });
}
</script>
</div>
</div>
<div id="callingModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">View Client Details</h3>
      </div>
      <div class="modal-body">
      <div id="details"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Modal title</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<style>
div.pagination{padding:3px;margin:3px;text-align:center}div.pagination a{padding:2px 5px 2px 5px;margin:2px;border:1px solid #AAD;text-decoration:none;color:#009}div.pagination a:hover,div.digg a:active{border:1px solid #009;color:#000}div.pagination span.current{padding:2px 5px 2px 5px;margin:2px;border:1px solid #009;font-weight:700;background-color:#009;color:#FFF}div.pagination span.disabled{padding:2px 5px 2px 5px;margin:2px;border:1px solid #EEE;color:#DDD}ul.tsc_pagination{margin:4px 0;padding:0;height:100%;overflow:hidden;font:12px \'Tahoma\';list-style-type:none}ul.tsc_pagination li{float:left;margin:0;padding:0;margin-left:5px}ul.tsc_pagination li:first-child{margin-left:0}ul.tsc_pagination li a{color:black;display:block;text-decoration:none;padding:7px 10px 7px 10px}ul.tsc_pagination li a img{border:none}ul.tsc_paginationC li a{color:#707070;background:#FFF;border-radius:3px;-moz-border-radius:3px;-webkit-border-radius:3px;border:solid 1px #DCDCDC;padding:6px 9px 6px 9px}ul.tsc_paginationC li{padding-bottom:1px}ul.tsc_paginationC li a:hover,ul.tsc_paginationC li a.current{color:#FFF;box-shadow:0 1px #EDEDED;-moz-box-shadow:0 1px #EDEDED;-webkit-box-shadow:0 1px #EDEDED}ul.tsc_paginationC01 li a:hover,ul.tsc_paginationC01 li a.current{color:#893A00;text-shadow:0 1px #FFEF42;border-color:#FFA200;background:#FFC800;background:-moz-linear-gradient(top,#FFFFFF 1px,#FFEA01 1px,#FFC800);background:-webkit-gradient(linear,0 0,0 100%,color-stop(.02,#FFFFFF),color-stop(.02,#FFEA01),color-stop(1,#FFC800))}ul.tsc_paginationC li a.In-active{pointer-events:none;cursor:default}
</style>
  <script type="text/javascript">
  $(document).ready(function(){
  function Display_Load()
  {
      $("#loading").fadeIn(900,0);
    $("#loading").html("<img src='bigLoader.gif' />");
  }
  function Hide_Load()
  {
    $("#loading").fadeOut('slow');
  };
  $("#pagination li:first").css({'color' : '#FF0084'}).css({'border' : 'none'});
  Display_Load();
  $("#content").load("pagination_data.php?page=1", Hide_Load());
  $("#pagination li").click(function(){
    Display_Load();
    $("#pagination li")
    .css({'border' : 'solid #dddddd 1px'})
    .css({'color' : '#0063DC'});
    $(this)
    .css({'color' : '#FF0084'})
    .css({'border' : 'none'});
    var pageNum = this.id;
    $("#content").load("pagination_data.php?page=" + pageNum, Hide_Load());
  });
  
});
  </script>
<style>
a:hover{color:#DF3D82;text-decoration:underline}#loading{width:100%;position:absolute}#pagination{text-align:center;margin-left:120px}
</style>

  <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
  <?php include("footer.php");?>
