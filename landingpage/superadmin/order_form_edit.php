<?php error_reporting(0); session_start(); $_SESSION['user_id']; include("head.php");?>
<head>
   <title>Order Form Edit</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel="stylesheet" type="text/css" href="css/style.css">
   <link href="assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
   <link href="assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
   <link rel="stylesheet" type="text/css" href="https://rawgit.com/nobleclem/jQuery-MultiSelect/master/jquery.multiselect.css" />
   <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
   <link href="assets/css/style.min.css" rel="stylesheet" />
   <script src="https://rawgit.com/nobleclem/jQuery-MultiSelect/master/jquery.multiselect.js"></script> -->
   <style>
    label {
   color: #337ab7 !important;
font-size: 12px !important;
}
.col-md-12
{
  padding: 0 !important;
}
select.form-control
{
  border:1px solid #ccc !important;
}
.payment_new_label
{
  font-size: 11px !important;

}
.table>tbody>tr>td
{
  border-top: none !important;
}
input.cheque {
    border: none;
    }
input.error {
    border: 1px solid red;
    margin-left: 10px;
}
.error {
    text-align: left !important;
    }
    #updation_search-error
    {
          margin-left: 80px;
    margin-top: 5px;
    line-height: 28px;
    }
.Search_div
{
width: 100%;
float: left;
margin-bottom: 20px;
}
.Search_div label
{
  font-size: 14px;
      line-height: 30px;
}
.ms-options-wrap > button:focus, .ms-options-wrap > button {
    position: relative;
        border-bottom: 1px solid #ccd0d4 !important;
    width: 90%;
    text-align: left;
    border: none;
    background-color: #fff;
    padding: 5px 20px 5px 5px;
    margin-top: 0px;
    font-size: 13px;
    color: black;
    outline-offset: 0px;
    white-space: nowrap;
}
.ms-options-wrap > .ms-options {
    width: 90%;
  }
#tab1default,#tab2default
{
  margin-top: 0px;
}
.new_head_title
{
font-size: 16px !important;
margin-bottom: 20px;
}
.head_reg
{
    margin-bottom: 15px;
    width: 100%;
    float: left;
    text-align: center;
    font-size: 20px;
}
.register-content .form-control {
    display: block;
    width: 90%;
    height: 25px;
    padding: 0px;
    border: none;
    border-bottom: 1px solid #ccd0d4;
    border-radius: 0px !important;
    font-size: 12px;
    }
    .register-buttons {
    width: 12%;
    margin: 3% auto;
    float: none;
    text-align: center;
}
.btn-group-lg>.btn, .btn-lg {
    padding: 4px 16px;
    font-size: 15px;
    }
    .m-b-15 {
    margin-bottom: 5px!important;
}
.updation_search_class
{
     width: 80%;
    border-radius: 5px;
    margin-left: 15px;
    padding: 10px;
    border: 1px solid #ccc;
}
      .wrapper_login
{
background: linear-gradient(
                     rgba(20,20,20, .5), 
                     rgba(20,20,20, .5)),
                     url('img/images.jpg') 100% no-repeat;

} 
.reset_filed
{
  margin-left: 15px;
  padding: 5px 20px;
}
.register {
    width: 50%;
    }
    input[type=radio] {
    margin: 0px 6px 0px !important;
  }
    .payment_div
    {

    }
    .payment_label
    {margin: 0px 15px;

    }
     .payment_box{
        color: #fff;
        padding: 20px;
        display: none;
        margin-top: 20px;
    }
    .payment_1
    {
      display: block;
    }
    
</style>
<script>
 $(document).on("change keyup blur", ".amount_recieve", function() {
  calculation();
  });

  function calculation(){
   var sum = 0;
    $(".amount_recieve").each(function(){
        sum += +$(this).val();
    });
    $("#subTotal").val(sum); 
    
    $(document).on("change keyup blur onload click", function(){ 
      var getTotal= $("#amount").val();
      var getRemainingAmount = $("#subTotal").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount").val('');
        }
   }); 
  }
  
       $(document).ready( function() {
      $(document).on('change', '.btn-file :file', function() {
    var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
    });
    $('.btn-file :file').on('fileselect', function(event, label) {
        var input = $(this).parents('.input-group').find(':text'),
            log = label;
        if( input.length ) {
            input.val(log);
        } else {
            // if( log ) alert(log);
        }
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#img-upload').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function(){
        readURL(this);
    });   
  });  
    
</script>
<script type="text/javascript">
$(document).ready(function(){
                    $("#displays").keyup(function(){
                      
                        $.ajax({
                        type: "POST",
                        url: "displays.php",
                        data:'clientSearch='+$(this).val(),
                        beforeSend: function(){
                        $("#displays").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data){
                         //alert(data);
                           /* $("#client-box").show();
                            $("#client-box").html(data);
*/
                            $("#displays").css("background","#FFF");
                           //$('#DivHide').hide();
                        }
                        });
                    });
                });

</script>
<script type="text/javascript">
$(document).ready(function(){
$(".add-row").click(function(){
var markup = "<tr> <td style='vertical-align: text-bottom;'><input type='checkbox' name='record'></td><td> <label class='control-label payment_new_label'>Cheque No</label> <input name='che_no[]' type='text' class='form-control' required='required'> </td><td style='width:200px;'> <label class='control-label payment_new_label'>Bank Name</label> <input name='bank_name[]' type='text' class='form-control' required='required'> </td><td> <label class='control-label payment_new_label'>IFSC Code</label> <input name='ifsc_code[]' type='text' class='form-control' required='required'> </td><td> <label class='control-label payment_new_label'>Amount Received</label> <input name='cash_amt[]' type='text' class='form-control' required='required'> </td><td> <label class='control-label payment_new_label'>Remaining Amount</label> <input type='text' name='remaining_amt[]' class='form-control' > </td><td> <label class='control-label payment_new_label'>Next Deposited Date</label> <input type='date' name='next_due_date[]' class='form-control' id='datepicker'> </td><td>  <label class='control-label'>Add Cheque Image<span class='text-danger'>*</span></label><label class='btn-bs-file btn btn-xs btn-warning'>Cheque Image<input type='file' name='cheque[]'' class='form-control'/></label></td></tr>'";
$(".orderform_table").append(markup);
});
// Find and remove selected table rows
$(".delete-row").click(function(){
$(".orderform_table").find('input[name="record"]').each(function(){
if($(this).is(":checked")){
$(this).parents("tr").remove();
}
});
});
}); 
</script>
   <script src="ajax/countyState.js"></script>
   <script src="ajax/CheckData45.js"></script>
   <script>
     $(function() {
  // from http://stackoverflow.com/questions/45888/what-is-the-most-efficient-way-to-sort-an-html-selects-options-by-value-while
  var my_options = $('.facilities select option');
  var selected = $('.facilities').find('select').val();

  my_options.sort(function(a,b) {
    if (a.text > b.text) return 1;
    if (a.text < b.text) return -1;
    return 0
  })

  $('.facilities').find('select').empty().append( my_options );
  $('.facilities').find('select').val(selected);
  
  // set it to multiple
  $('.facilities').find('select').attr('multiple', true);
  
  // remove all option
  $('.facilities').find('select option[value=""]').remove();
  // add multiple select checkbox feature.
  $('.facilities').find('select').multiselect();
})
   </script>

   <script>
      $(document).ready(function(){
        $('#forms').addClass('active');
        $('#order_form_side').addClass('active');
      });
   </script>
   <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
   <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />
   <script type="text/javascript">
      $(document).ready(function(){
          $("#name").autocomplete({
              source:'autocomplete.php',
              minLength:1
          });
      });
   </script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".payment_box").not(targetBox).hide();
        $(targetBox).show();
    });
});
</script>
   <?php include_once("header.php"); ?>
   <?php include_once("sidebar.php"); ?>  
   <!-- begin #content -->

   <div id="content" class="content">
      <!-- begin breadcrumb -->
      <ol class="breadcrumb" style="width: 100%;">
         <li><a href="index.php">Home</a></li>
         <li><a href="index.php">Dashboard</a></li>
         <li class="active" style="color: green;">Edit Order Form</li>
      </ol>
      <!-- end breadcrumb -->
      <div class="details">
        <h3 class="col-sm-8" style="margin-top: 0px; color: green;">Edit Order Form&nbsp;&nbsp;&nbsp;&nbsp;<a href="index.php" class="btn btn-primary btn-xs">Back to Home Page</a>&nbsp;&nbsp;&nbsp;<a href="order_form_reports.php" class="btn btn-warning btn-xs">Order Form Report</a></h3>      
   <?php include("timezone.php");?>
  <hr style="float: left;width: 100%;">
        <?php $sql = mysql_query("SELECT * FROM `order_form` WHERE `id`='".$_GET['id']."'");?>
        <?php $res = mysql_fetch_array($sql); ?>
         <div class="col-sm-12">
            <div class="panel with-nav-tabs panel-default">
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="tab1default">
                    <div class="right-content">
                      <!-- begin register-content -->
                      <div class="register-content">
                        <form action="submit.php?id=<?php echo $_GET['id'];?>" method="POST" enctype="multipart/form-data" id="myform1">
                          <div class="new_head_title">Basic Information</div>
                          <div class="row row-space-10">
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label"> First Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="name" id="name" value="<?php echo $res['name'];?>" onkeyup="cust_name()"  required autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_fname"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Last Name <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="last_name" value="<?php echo $res['last_name'];?>" id="lname" onkeyup="cust_name()" required autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_lname"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Email<span class="text-danger">*</span></label>
                              <input type="text" class="form-control email_check"  id="email" name="email" value="<?php echo $res['email'];?>" required autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_email"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Mobile No. <span class="text-danger">*</span></label>
                              <input type="text" class="form-control" onkeypress="return isNumber(event)" maxlength="10" name="mobile" value="<?php echo $res['mobile'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_mobile"></div>
                            </div>
                          </div>

                          <div class="row row-space-10">
                            
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Phone No. <span class="text-danger"></span></label>
                              <input type="text" class="form-control" maxlength="12"  name="rphone" value="<?php echo $res['rphone'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_phone(r)"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Address<span class="text-danger">*</span></label>
                              <textarea class="form-control" rows="3" id="add" name="address"><?php echo $res['address'];?></textarea>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_add"></div>
                            </div>
                        
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Pin Code<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="pincode" value="<?php echo $res['pincode'];?>" autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_pin"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Select State<span class="text-danger">*</span></label>
                              <?php 
                                 $query = mysql_query("SELECT * FROM states WHERE country_id = 100 ORDER BY state_name ASC");
                                   //Count total number of rows
                                   $resCount = mysql_num_rows($query);?>
                              <select class="form-control" id="state" name="state_name" autocomplete="OFF">
                                 <option value="<?php echo $res['state_name'];?>">
                                  <?php if($res['state_name']!=""){ $statename = mysql_query("SELECT * FROM `states` WHERE `state_id`='".$res['state_name']."'"); $selStae =mysql_fetch_array($statename); echo $selStae['state_name'];}else{?>---Select State---<?php } ?></option>
                                 <?php
                                    if($resCount > 0){
                                        while($rows = mysql_fetch_assoc($query)){ 
                                            echo '<option value="'.$rows['state_id'].'">'.$rows['state_name'].'</option>';
                                        }
                                    }else{
                                        echo '<option value="">State not available</option>';
                                    }
                                    ?>
                              </select>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_state"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Select District<span class="text-danger">*</span></label>
                              <?php 
                                $query1 = mysql_query("SELECT * FROM cities WHERE `state_id` ='".$res['state_name']."' ORDER BY city_name ASC");
                                   //Count total number of rows
                                   $resCount1 = mysql_num_rows($query1);?>
                              <select class="form-control" id="city" name="city" autocomplete="OFF">
                                 <option value="<?php echo $res['city'];?>"><?php  $citnames = mysql_query("SELECT * FROM `cities` WHERE `city_id`='".$res['city']."'"); $selCityName = mysql_fetch_array($citnames); if($res['city']!=""){ echo $selCityName['city_name']; }else{?>---Select State first---<<?php } ?></option>
                                  <?php
                                    if($resCount1 > 0){
                                        while($rows1 = mysql_fetch_assoc($query1)){ 
                                            echo '<option value="'.$rows1['city_id'].'">'.$rows1['city_name'].'</option>';
                                        }
                                    }else{
                                        echo '<option value="">City not available</option>';
                                    }
                                    ?>
                              </select>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_city"></div>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Enter City/Town<span class="text-danger">*</span></label>
                              <input type="text" class="form-control email_check"  id="town" name="town" value="<?php echo $res['district'];?>" autocomplete="OFF" placeholder="Enter Town / City" autocomplete="OFF"/>
                                  <div id="suggesstion-town"></div>
                            </div>
                             <div class="col-sm-3 col-xs-6 m-b-15">
                             <!--  <label class="control-label">Add Order Form<span class="text-danger">*</span></label><br>
                              <label class="btn-bs-file btn btn-xs btn-warning">Order Form Image
                                <input type="file"  name="orderform" class="form-control" />
                              </label> -->
                              <div class="form-group">
                            <label>Add Order from</label>
                            <div class="input-group">
                              <input type="hidden" value="<?php echo $res['attach_Ordeform']; ?>" name="orderform">
                                <span class="input-group-btn">
                                    <span class="btn-bs-file btn btn-xs btn-warning btn-file">
                                        Browse… <input type="file" id="imgInp" name="orderform">
                                      </span>
                                        <?php if($res['attach_Ordeform']!=''){?>
                                        <img src="../nationalmanager/file/orderform/<?php echo $res['attach_Ordeform'];?>" id='img-upload' width="120">
                                        <?php } else {?>
                                        <img src="img/noimage.png" width="120" height="132px" id='img-upload' style="border: 1px solid grey;">
                                        <?php } ?>
                                         
                                </span>
                                <input type="text" class="form-control" readonly>
                            </div>
                           
                        </div>
                            </div>
                          </div>
                           <div class="new_head_title" style="color: green;"><b>Product Details</b></div>
                           <hr>
                          <div class="row row-space-10">
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                <label class="control-label">Business Associate Name<span class="text-danger">*</span></label>
                                <?php 
                                   $sqlres= mysql_query("select * from `registration` where `id`='".$_SESSION['user_id']."'");
                                              $rowres = mysql_fetch_array($sqlres);?>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $rowres['name']; ?> <?php echo $rowres['last_name']; ?>" readonly>
                              </div>
                              <?php $orderForm = mysql_query("SELECT * FROM `order_form` WHERE `id`='".$_GET['id']."'");
                              $selOrder = mysql_fetch_array($orderForm);?>
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Dongal Type<span class="text-danger">*</span></label>
                                  <select class="form-control" name="dongletype_0" placeholder="Dongle Type" autocomplete="OFF">
                                  <option value="<?php echo $res['dongletype_0'];?>"><?php if($selOrder['dongletype_0']!=""){ echo $selOrder['dongletype_0']; }else{?>--Select Type--<?php } ?></option>
                                  <option value="Individual">Individual</option>
                                  <option value="Network">Network</option>
                                  <option value="Scheme">Scheme</option>
                                  <option value="Bulk">Bulk</option>
                                  <option value="Complimentary">Complimentary</option>
                                  </select>
                              </div>
                              <div class="col-sm-3 col-xs-6 m-b-15 displayDropdown">
                                <label class="control-label">Select Product<span class="text-danger">*</span></label>
                              <div class='facilities'><span class='input'>
                                <select name="product_0[]">
                                   <option value="<?php echo $selOrder['product_0'];?>" onclick="alert(\'Please select product!\');"><?php if($selOrder['product_0']!=""){ echo $selOrder['product_0']; }else{?>Select Product<?php } ?></option>
                                   <option value="Lesearch Comprehensive (LE-COMP)">Lesearch Comprehensive</option>
                                   <option value="Lesearch Individual (SC)">Lesearch Individual (SC)</option>
                                   <option value="Lesearch Individual (HC)">Lesearch Individual (HC)</option>
                                   <option value="Lesearch Individual (CRILJ)">Lesearch Individual (CRILJ)</option>
                                   <option value="BHCR (Bombay High Court)">BHCR (Bombay High Court)</option>
                                   <option value="Allahabad High Court (ALJ)">Allahabad High Court (ALJ)</option>
                                   <option value="Privi Council (PC)">Privi Council (PC)</option>
                                   <option value="Case Law Navigator (CASE-LAW)">Case Law Navigator (CASE-LAW)</option>
                                </select>
                              </div>
                              </div>
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                <label class="control-label">Select Mode<span class="text-danger">*</span></label>
                                <select class="form-control" name="mode_0" placeholder="Mode">
                                  <option value="<?php echo $selOrder['mode_0'];?>"><?php if($selOrder['mode_0']!=""){ echo $selOrder['mode_0']; }else{?>---- Select the mode ----<?php } ?></option>
                                  <!-- <option value="BB">Updation BB</option>  -->      
                                  <option value="DIR">Updation DIR</option>
                                  <option value="DEMO">DEMO</option>
                                </select>
                              </div>
                          </div>
                            <div class="row row-space-10">
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Select Scheme<span class="text-danger">*</span></label>
                              <select class="form-control" name="scheme_0">
                              <option title="Bar Council of India" value="<?php echo $selOrder['scheme_0'];?>"><?php if($selOrder['scheme_0']!=""){ echo $selOrder['scheme_0']; }else{?>--Select Scheme--<?php } ?></option>
                              <option title="Bar Council of India"> BCI </option>
                              <option title="Bar Council Maharashtra Goa"> BCMG </option>
                              <option title="CENTINARY SCHME"> CENT </option>
                              <option title="DEMO DVD PROJECT"> DEMO-PRO </option>
                              <option title="Journal Scheme"> JRNL </option>
                              <option title="Air Case Navigator"> NAVIGATOR </option>
                              <option title="Open General License Scheme"> OGL </option>
                              <option title="ONLINE"> ONLINE </option>
                              </select>
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">HASP ID<span class="text-danger">*</span></label>
                             <input type="text" class="form-control" name="haspid" id="haspid" value="<?php echo $selOrder['haspid'];?>" autocomplete="OFF">
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">DVD No.<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="dvdno" id="dvdno" value="<?php echo $selOrder['dvdno'];?>" autocomplete="OFF">
                            </div>
                            <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">BB DVD No.<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="bbdvd_0" id="bbdvd_0" value="<?php echo $res['bbdvd_0'];?>">
                            </div> -->
                             <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">DVD Year<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="year_0" id="year_0" value="<?php echo $selOrder['year_0'];?>" autocomplete="OFF">
                            </div>
                          </div>
                          <div class="new_head_title" style="color: green;"><b>Payment Mode</b></div>
                           <div class="col-sm-12 by_cheque">
                                      <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Total Amount</label>
                              <input type="text" class="form-control" name="total_amount" id="amount" value="<?php echo $selOrder['total_amount'];?>" onkeypress="return IsNumeric(event);" placeholder="Total Amount" ondrop="return false;" onpaste="return false;" autocomplete="OFF" >
                          </div>
                          <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Total Amount Received</label>
                              <a href="view_ordrform_reports.php?id=<?php echo $res['id'];?>" class="btn btn-warning btn-xs">Edit Received Amount</a>
                            <!--   <input type="text" class="form-control" id="subTotal" name="cash_amt" placeholder=" Total Received Amount" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF" > -->
                          </div>
                          <!--  <div class="col-md-3 m-b-15">
                              <label class="control-label">Remaining Amount</label>
                              <input type="text" class="form-control" name="remianing_amount" id="remianing_amount" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                          </div> -->
                           </hr>
                         <!--  <div class="col-md-3 m-b-15">
                              <label class="control-label">Total Amount<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="total_amount" id="amount" value="<?php echo $selOrder['total_amount'];?>" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF" required>
                          </div> -->
                          <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">Total Amount Received<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="subTotal" name="cash_amt" placeholder=" Total" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $selOrder['reccive_amt'];?>" required>
                          </div> -->
                           <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">Remaining Amount<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" name="remianing_amount" id="remianing_amount" value="<?php echo $selOrder['remaining_amt'];?>" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" required>
                          </div> -->
                          <div class="register-buttons">
                          <input type="submit" class="btn btn-primary" id="submit_dsr" value="Submit" name="order_form_edit">
                          </div>

                        </form>
                      </div>
                    </div>

                     <!-- end register-content -->
                  </div>
                  <div class="tab-pane fade" id="tab2default">
                    <div class="right-content">
                            <div class="register-content">
                      <!-- begin register-content -->
                        <form action="" method="POST" enctype="multipart/form-data" id="myform" name="MyForm">
                             <div class="Search_div">
                              <label class="control-label">Search Here :</label>
                               <input type="text" name="search" id="displays" class="updation_search_class" placeholder="Search by haspkey / Mobile / Email Id" autocomplete="OFF">
                            </div>

                            <div id="client-box"></div>
                        </form>
                      </div>
                    </div>
                     <!-- end register-content -->
                  </div>
                </div>
              </div>
            </div>
          </div>

      </div>
      <!-- end right-content -->

   </div>
   <script type="text/javascript">
      $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
   </script>            
   <?php include("footer.php");?>
<script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
   <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

$( "#myform" ).validate({
  rules: {
    search: {
      required: true,
      number: true
    },
    name: {
      required: true,
      minlength: 2,
    lettersonly:true
    }
    ,
    last_name: {
      required: true,
      minlength: 2,
    lettersonly:true
    },
    email: {
      required: true,
      email: true
    },
    mobile:
    {
      required: true,
      number: true
    },
    rphone1:
    {
      required: true,
      number: true
    },
    pincode:
    {
      required: true,
      number: true
    }
  }
});

$(document).ready(function(){
  $("#town").keyup(function(){
    //alert("OK");
    $.ajax({
    type: "POST",
    url: "townSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#town").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      //alert(data);
      $("#suggesstion-town").show();
      $("#suggesstion-town").html(data);
      $("#town").css("background","#FFF");
    }
    });
  });
});
//To select country name
function selectCountry(val) {
$("#town").val(val);
$("#suggesstion-town").hide();
}
</script>

 <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
<script src="https://rawgit.com/nobleclem/jQuery-MultiSelect/master/jquery.multiselect.js"></script>
   </body>
</html>

<style>
#country-lists{
  float: left;
list-style: none;
margin: 0;
padding: 0;
width: 86%;
z-index: 1;
position: absolute;
}
#country-lists li{
  padding: 10px; 
  background:#FAFAFA;
  border-bottom:#F0F0F0 1px solid;
  cursor: pointer;
}
#country-lists li:hover{
  background:#F0F0F0;
}
#town{
  padding: 4px;
  /*border: #F0F0F0 1px solid;*/
}
</style>
 <style type="text/css">
                        .btn-bs-file{
                              position:relative;
                          }
                          .btn-bs-file input[type="file"]{
                              position: absolute;
                              top: -9999999;
                              filter: alpha(opacity=0);
                              opacity: 0;
                              width:0;
                              height:0;
                              outline: none;
                              cursor: inherit;
                          }
                                      </style>