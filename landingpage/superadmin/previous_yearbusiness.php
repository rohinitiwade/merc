<?php error_reporting(0);?>
<head>
  <?php include("head.php");?>
     <script src="js/highcharts.js"></script>
     <script src="js/exporting.js"></script>
</head>
<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>  
<script>
  $(document).ready(function(){
   $("#bus_det").addClass("active");
   $('#data-tableDSRrepor').dataTable();
   $('#data-tabledaily').dataTable();
   $('#data-new_sale_table').dataTable();
   $("#table-updation").dataTable();
   $("#table_totalsale").dataTable();
   $("#rm_received").dataTable();
   $("#rm_outstanding").dataTable();
   $("#rm_deposite").dataTable();
   $("#rm_bounce").dataTable();
   $("#tabledaily").dataTable();
   $("#tabledailydsr").dataTable();
   $("#tabledailyorder").dataTable();

var currentMonth = (new Date).getMonth() + 1;
if(currentMonth == 1 || currentMonth == 2 || currentMonth == 3){
$("#tab1").addClass("active");
}
else if(currentMonth == 4 || currentMonth == 5 || currentMonth == 6){
$("#tab2").addClass("active");
}
else if(currentMonth == 7 || currentMonth == 8 || currentMonth == 9){
$("#tab3").addClass("active");
}
else{
$("#tab4").addClass("active");
}
});
</script>
<style>
  .star_rate{font-size:20px}.rating-stars ul{list-style-type:none;padding:0;text-align:left;-moz-user-select:none;-webkit-user-select:none}.rating-stars ul>li.star{display:inline-block}.new_table{margin-bottom:13px;border:2px solid gray}#bounce_table,#deposite_table,#new_sale_table1,#outstanding_table,#received_table,#total_sale_table,#updation_table{padding:5px;display:block;border:2px solid gray}#new_buss,#order_form{padding:5px;display:block;border:1px solid gray}.rating-stars ul>li.star>i.fa{font-size:23px;color:#ccc}.rating-stars ul>li.star.selected>i.fa{color:#FF912C} #mydiv{position:absolute;z-index:9999;background-color:#f1f1f1;border:1px solid #d3d3d3;text-align:center;top:127px}.mydiv{width:100px;float:right;margin:0 auto}#mydivheader{padding:10px;cursor:move;z-index:10;background-color:#2196F3;color:#fff} .panel-body,.panel-heading{float:left;border:1px solid #d3d3d3}#dsrform,#orderform,.ul_nav{width:100%;float:left}.download_btn input{margin-bottom:1%}.my_carousel{width:80%;position:relative;text-align:center;text-shadow:none;opacity:1;font-size:12px}.panel-heading{width:100%}.panel-body{border-top:0}.month_table{margin-bottom:0} div.bhoechie-tab-container{z-index:10;background-color:#fff;padding:0!important;-moz-border-radius:4px;border:1px solid #ddd;margin-top:20px;-webkit-box-shadow:0 6px 12px rgba(0,0,0,.175);box-shadow:0 6px 12px rgba(0,0,0,.175);-moz-box-shadow:0 6px 12px rgba(0,0,0,.175);background-clip:padding-box;opacity:.97;filter:alpha(opacity=97)}div.bhoechie-tab-menu{padding-right:0;padding-left:0;padding-bottom:0}div.bhoechie-tab-menu div.list-group,div.bhoechie-tab-menu div.list-group>a{margin-bottom:0}div.bhoechie-tab-menu div.list-group>a .fa,div.bhoechie-tab-menu div.list-group>a .glyphicon{color:#5A55A3}div.bhoechie-tab-menu div.list-group>a:first-child{border-top-right-radius:0;-moz-border-top-right-radius:0}div.bhoechie-tab-menu div.list-group>a:last-child{border-bottom-right-radius:0;-moz-border-bottom-right-radius:0}div.bhoechie-tab-menu div.list-group>a.active,div.bhoechie-tab-menu div.list-group>a.active .fa,div.bhoechie-tab-menu div.list-group>a.active .glyphicon{background-color:#5A55A3;background-image:#5A55A3;color:#fff}div.bhoechie-tab div.bhoechie-tab-content:not(.active){display:none} .no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
  background: url(images/loader-64x/Preloader_2.gif) center no-repeat #fff;
}
</style>
<script>
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
  });
  </script>
<script>
  $(document).ready(function(){
    $("#new_sale_table1").hide();
      $("#updation_table").hide();
      $("#total_sale_table").hide();
      $("#received_table").hide();
      $("#outstanding_table").hide();
      $("#deposite_table").hide();
      $("#bounce_table").hide();

    $("#new_sale").click(function(){
      $("#new_sale_table1").fadeIn();
      $("#updation_table").hide();
      $("#total_sale_table").hide();
      $("#received_table").hide();
      $("#outstanding_table").hide();
      $("#deposite_table").hide();
      $("#bounce_table").hide();
    });
    $("#updation1").click(function(){
      
      $("#new_sale_table1").hide();
      $("#updation_table").fadeIn();
      $("#total_sale_table").hide();
      $("#received_table").hide();
      $("#outstanding_table").hide();
      $("#deposite_table").hide();
      $("#bounce_table").hide();
    });
     $("#total_sale1").click(function(){
      $("#new_sale_table1").hide();
      $("#updation_table").hide();
      $("#total_sale_table").fadeIn();
      $("#received_table").hide();
      $("#outstanding_table").hide();
      $("#deposite_table").hide();
      $("#bounce_table").hide();
    });
    $("#received_amt").click(function(){
      $("#new_sale_table1").hide();
      $("#updation_table").hide();
      $("#total_sale_table").hide();
      $("#received_table").fadeIn();
      $("#outstanding_table").hide();
      $("#deposite_table").hide();
      $("#bounce_table").hide();
    });
    $("#outstanding").click(function(){
      $("#new_sale_table1").hide();
      $("#updation_table").hide();
      $("#total_sale_table").hide();
      $("#received_table").hide();
      $("#outstanding_table").fadeIn();
      $("#deposite_table").hide();
      $("#bounce_table").hide();
    });
     $("#deposite").click(function(){
      $("#new_sale_table1").hide();
      $("#updation_table").hide();
      $("#total_sale_table").hide();
      $("#received_table").hide();
      $("#outstanding_table").hide();
      $("#deposite_table").fadeIn();
      $("#bounce_table").hide();
    });
      $("#bounce").click(function(){
      $("#new_sale_table1").hide();
      $("#updation_table").hide();
      $("#total_sale_table").hide();
      $("#received_table").hide();
      $("#outstanding_table").hide();
      $("#deposite_table").hide();
      $("#bounce_table").fadeIn();
    })
  })
</script>
<div id="content" class="content">
    <ol class="breadcrumb" style="width: 100%;">
        <li><a href="http://crm.airinfotech.in/rm/">Home</a></li>
        <li  href="http://crm.airinfotech.in/rm/">Dashboard</li>
    </ol>
            <!-- end breadcrumb -->
<div class="details">
  <h3 class="col-sm-8 details11" style="margin-top: 0px; color: green;">Business Analysis Report&nbsp;&nbsp;&nbsp;<a href="index.php" class="btn btn-primary btn-xs">Back To Home Page</a></h3>
  <?php include("timezone.php");?>
  <hr>
  <?php if(isset($_POST['submit'])){
              extract($_POST);
              $sel_year=$_POST['sel_year'];
  }?>  
<div class="col-sm-12">   
  <form method="POST" style="margin: 0;">  
  <div class="col-sm-1">
         <label class="control-label">Select Year</label>
  </div>
  <div class="col-sm-3">
         <select class="form-control" name="sel_year">
          <option><?php if($_POST['sel_year']!=''){ echo $_POST['sel_year'];} else {?>------- Select Year --------<?php } ?></option>
          <?php 
          $curr=date('Y');
          foreach (range($curr, $curr-12) as $value){
            // $current_year, $current_year+10
            echo "<option>".$value."</option>";
          }?>
          </select>
  </div> 
  <?php
  if($sel_year!=''){
              $pyear=$sel_year;
  }else{
              $pyear=date("Y",strtotime("-1 year"));
  }?>
  <div class="col-sm-1">
    <input type="submit" name="submit" value="Submit" class="btn btn-primary btn-sm">
  </div>
  </form>
  <div class="col-sm-5">
    <?php $sql = mysql_query("SELECT * FROM `registration` WHERE `approve`=0 AND `id`='".$_SESSION['user_id']."'");
          $res = mysql_fetch_array($sql);?>
      <h5><b>Total Business From:</b> &nbsp;&nbsp;<span style="color: green;">01-01-<?php echo $pyear;?></span>&nbsp;&nbsp; To:&nbsp;&nbsp; <span style="color: green;">31-12-<?php echo $pyear;?></span></h5>
  </div>
</div>
  
<div class="mydiv">
<div id="mydiv">
  <!--Include a header DIV with the same name as the draggable DIV, followed by "header":-->
  <!-- <div id="mydivheader">My Avatar</div> -->
        <img src="../nationalmanager/uploadImages/userProfile/<?php echo $res['image'];?>" width="100" height="80">
</div>
</div>
<script>
dragElement(document.getElementById(("mydiv")));
function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    elmnt.onmousedown = dragMouseDown;
  }
  function dragMouseDown(e) {
    e = e || window.event;
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }
  function elementDrag(e) {
    e = e || window.event;
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }
  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}
</script> 
<?php $registerd = mysql_query("SELECT *, AES_DECRYPT(name,'$key'), AES_DECRYPT(last_name,'$key'),AES_DECRYPT(mobile,'$key'),AES_DECRYPT(email_id,'$key') FROM `registration` WHERE `id`='".$_SESSION['user_id']."'"); 
    $SelName = mysql_fetch_array($registerd);
    $tot = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."'"); 
    $totrow = mysql_fetch_array($tot); 
    $amount = $totrow['SUM(total_amount)'];;
    setlocale(LC_MONETARY, 'en_IN');
    $amount = money_format('%!i', $amount);?>
<div class="col-sm-">
    <div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center Report">
                  <h4 class="glyphicon glyphicon-signal"></h4><br/>My Business
                </a>
                <a href="#" class="list-group-item text-center Report">
                  <h4 class="glyphicon glyphicon-signal"></h4><br/>New Business
                </a>
                <a href="#" class="list-group-item text-center Report">
                  <h4 class="glyphicon glyphicon-signal"></h4><br/>DSR Report
                </a>
                <a href="#" class="list-group-item text-center Report">
                  <h4 class="glyphicon glyphicon-signal"></h4><br/>Order Form Report
                </a>
                <a href="#" class="list-group-item text-center Report">
                  <h4 class="glyphicon glyphicon-signal"></h4><br/>Remaining Client
                </a>
              </div>
            </div>          
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 bhoechie-tab">
                <div class="bhoechie-tab-content active">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col" style="background-color: #8992d6">Name</th>
                      <th scope="col" style="background-color: #8992d6">Mobile No.</th>
                      <th scope="col" style="background-color: #8992d6">Email Id</th>
                      <th scope="col" style="background-color: #8992d6">Total Business</th>
                      <th scope="col" style="background-color: #8992d6">Registration Date</th>
                      <th scope="col" style="background-color: #8992d6">Rating</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>                              
                      <td style="background-color: #b0b1b7"><?php echo ucfirst($SelName["AES_DECRYPT(name,'$key')"]);?>  <?php echo ucfirst($SelName["AES_DECRYPT(last_name,'$key')"]);?></td>
                      <td style="background-color: #b0b1b7"><?php echo $SelName["AES_DECRYPT(mobile,'$key')"];?></td>
                      <td style="background-color: #b0b1b7"><?php echo $SelName["AES_DECRYPT(email_id,'$key')"];?></td>
                      <td style="background-color: #b0b1b7"><b style="color:green;"><?php $newsale = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
                      $selnewsale = mysql_fetch_array($newsale);?></b>
                    <b style="color:green;">
                      <?php  $updates = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_SESSION['user_id']."' AND `ex_id`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
                      $updateSel = mysql_fetch_array($updates);

                      $myup = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_SESSION['user_id']."' AND `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
                      $myupdate = mysql_fetch_array($myup);

                      $selfupdates = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `update_by`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
                      $selfupdateSel = mysql_fetch_array($selfupdates);?> </b> 

                    <b style="color:green;cursor: pointer;" id="total_sale"><?php echo number_format($updateSel['SUM(total_amount)']+$myupdate['SUM(total_amount)']+$selnewsale['SUM(total_amount)']); ?></b></td>
                      <td style="background-color: #b0b1b7"><?php echo $newDate = date("d-m-Y", strtotime($SelName['date_time']));?></td>
                      <td style="background-color: #b0b1b7">8</td>
                    </tr>
                  </tbody>
                </table>
                </div>
                <div class="col-sm-4 col-xs-12 month_div" style="padding: 0px; background-color: #77d180; border: 2px solid gray;">
                  <p style="text-align: center;"><b>Monthly Sale</b></p>
                  <div class="col-sm-3 col-xs-3" style="padding: 0px">
                  <table class="table table-bordered month_table"  bgcolor="red">
                    <thead>
                      <tr>
                        <th bgcolor="orange">Month</th>
                      </tr>
                      <tr>
                        <th data-toggle="tooltip" title="Total Amount" style="padding-left: 3px;"><span style="font-size: 11px; color: green;"><b>Total</b></span></th>
                      </tr>
                      <tr>
                        <th data-toggle="tooltip" title="Received Amount" style="padding-left: 3px;"><span style="font-size: 11px; color: green;"><b>Received</b></span></th>
                      </tr>
                      <tr>
                        <th data-toggle="tooltip" title="Outstanding Amount" style="padding-left: 3px;"><span style="font-size: 11px; color: green;"><b>Outstanding</b></span></th>
                      </tr>
                    </thead>
                  </table>
                </div>
                <div class="col-sm-9 col-xs-9" style="padding: 0px">
                <div class="carousel slide" id="myCarousel" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner myCarousel_inner">
                  <div class="item" id="tab1">                   
                  <table class="table table-bordered month_table">
                    <thead>
                        <tr>                       
                          <th>Jan</th>
                          <th>Feb</th>
                          <th>Mar</th>
                        </tr>
                    </thead>
                    <tbody>
                      <tr>                                  
                          <td><?php  $totalbusijan =  mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-01-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                            $rowsjan = mysql_fetch_array($totalbusijan);?> <b style="color: red;"><?php echo number_format($rowsjan['SUM(total_amount)']); ?></b>
                          </td>

                          <td><?php  $totalbusifeb = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-02-01' AND '".$pyear."-02-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                            $rowsfeb = mysql_fetch_array($totalbusifeb);?> <b style="color: red;"><?php echo number_format($rowsfeb['SUM(total_amount)']); ?></b>
                          </td>

                          <td><?php  $totalbusimar = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-03-01' AND '".$pyear."-03-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                            $rowsmar = mysql_fetch_array($totalbusimar);?> <b style="color: red;"><?php echo number_format($rowsmar['SUM(total_amount)']); ?></b>
                          </td>
                      </tr>
                      <tr>                       
                          <td><?php  $totalbusirecjan = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-01-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                           $totalbusirecjan1 = mysql_fetch_array($totalbusirecjan);?> <b style="color: red;"><?php echo number_format($totalbusirecjan1['SUM(reccived_amt)']); ?></b>
                          </td>

                          <td> <?php $totalbusirefeb= mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-02-01' AND '".$pyear."-02-31'");
                            $totalbusirefeb1 = mysql_fetch_array($totalbusirefeb);?><b style="color:red;"><?php echo number_format($totalbusirefeb1['SUM(reccived_amt)']);?></b>
                          </td>

                          <td><?php $totalbusiremar = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-03-01' AND '".$pyear."-03-31'");
                            $totalbusiremar1 = mysql_fetch_array($totalbusiremar);?><b style="color:red;"><?php echo number_format($totalbusiremar1['SUM(reccived_amt)']);?>
                         </td>
                      </tr>

                      <tr>                       
                        <td><?php $totaloutjan = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-01-31'");
                          $totaloutjant1 = mysql_fetch_array($totaloutjan); 
                          $totaloutjantot = $rowsjan['SUM(total_amount)']+$totaloutjan1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totaloutjantot)-$totalbusirecjan1['SUM(reccived_amt)'];?></b>
                        </td>
                        <td><?php $totaloutfeb = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-02-01' AND '".$pyear."-02-31'");
                          $totaloutfeb1 = mysql_fetch_array($totaloutfeb);
                          $totalCalfeb = $rowsfeb['SUM(total_amount)']+$totaloutfeb1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totalCalnov-$totalbusirenov1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td><?php $totaloutmar = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-03-01' AND '".$pyear."-03-31'");
                          $totaloutmar1 = mysql_fetch_array($totaloutmar);
                          $totalCalmar = $rowsmar['SUM(total_amount)']+$totaloutmar11['SUM(total_amount)'];?>
                          <b style="color:red;"><?php  echo number_format($totalCalmar-$totalbusiremar1['SUM(reccived_amt)']);?></b>
                        </td>
                      </tr>
                    </tbody>
                  </table>
</div>
<div class="item" id="tab2">
                   <table class="table table-bordered month_table">
                    <thead>
                      <tr>                        
                        <th>Apr</th>
                        <th>May</th>
                        <th>June</th>
                      </tr>
                    </thead>
                    <tbody>
                     <tr>                        
                        <td><?php  $totalbusiapr = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-04-01' AND '".$pyear."-04-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsapr = mysql_fetch_array($totalbusiapr);?> <b style="color: red;"><?php echo number_format($rowsapr['SUM(total_amount)']); ?></b>
                        </td>
                        <td><?php  $totalbusimay = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-05-01' AND '".$pyear."-05-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsmay = mysql_fetch_array($totalbusimay);?> <b style="color: red;"><?php echo number_format($rowsmay['SUM(total_amount)']); ?></b>
                        </td>
                        <td><?php  $totalbusijun = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-06-01' AND '".$pyear."-06-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsmay = mysql_fetch_array($totalbusimay);?> <b style="color: red;"><?php echo number_format($rowsmay['SUM(total_amount)']); ?></b>
                        </td>
                      </tr>

                      <tr>                       
                        <td><?php  $totalbusirecapr = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `date_time` BETWEEN '".$pyear."-04-01' AND '".$pyear."-04-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $totalbusirecapr1 = mysql_fetch_array($totalbusirecapr);?> <b style="color: red;"><?php echo number_format($totalbusirecapr1['SUM(reccived_amt)']); ?></b>
                        </td>
                        <td> <?php $totalbusiremay = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-05-01' AND '".$pyear."-05-31'");
                          $totalbusiremay1 = mysql_fetch_array($totalbusiremay);?><b style="color:red;"><?php echo number_format($totalbusiremay1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td> <?php $totalbusiremay = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-06-01' AND '".$pyear."-06-31'");
                         $totalbusiremay1 = mysql_fetch_array($totalbusiremay);?><b style="color:red;"><?php echo number_format($totalbusiremay1['SUM(reccived_amt)']);?>
                        </td>
                      </tr>

                      <tr>                      
                        <td><?php $totaloutapr = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-04-01' AND '".$pyear."-04-31'");
                          $totaloutapr1 = mysql_fetch_array($totaloutapr); 
                              $totaloutaprtot = $rowsapr['SUM(total_amount)']+$totaloutapr1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totaloutaprtot)-$totalbusirecapr1['SUM(reccived_amt)'];?></b>
                        </td>
                        <td><?php $totaloutmay = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-05-01' AND '".$pyear."-05-31'");
                          $totaloutmay1 = mysql_fetch_array($totaloutmay);
                          $totalCalmay = $rowsmay['SUM(total_amount)']+$totaloutmay1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totalCalmay-$totalbusiremay1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td><?php $totaloutjun = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-06-01' AND '".$pyear."-06-31'");
                          $totaloutjun1 = mysql_fetch_array($totaloutjun);
                          $totalCaljun= $rowsjun['SUM(total_amount)']+$totaloutjun11['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totalCaljun-$totalbusirejun1['SUM(reccived_amt)']);?></b>
                        </td>
                      </tr>                       
                    </tbody>
                  </table>
                </div>
<div class="item" id="tab3">
                   <table class="table table-bordered month_table">
                    <thead>
                      <tr>                        
                        <th>Jul</th>
                        <th>Aug</th>
                        <th>Sept</th>
                      </tr>
                    </thead>
                    <tbody>
                     <tr>                       
                        <td><?php  $totalbusijul = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-07-01' AND '".$pyear."-07-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsjul = mysql_fetch_array($totalbusijul);?> <b style="color: red;"><?php echo number_format($rowsjul['SUM(total_amount)']); ?></b>
                        </td>
                        <td><?php  $totalbusiaug = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-08-01' AND '".$pyear."-08-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsaug = mysql_fetch_array($totalbusiaug);?> <b style="color: red;"><?php echo number_format($rowsaug['SUM(total_amount)']); ?></b>
                        </td>
                        <td><?php  $totalbusisep = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-09-01' AND '".$pyear."-09-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowssep = mysql_fetch_array($totalbusisep);?> <b style="color: red;"><?php echo number_format($rowssep['SUM(total_amount)']); ?></b>
                        </td>
                      </tr>

                      <tr>                       
                        <td><?php  $totalbusirecjul = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `date_time` BETWEEN '".$pyear."-07-01' AND '".$pyear."-07-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $totalbusirecjul1 = mysql_fetch_array($totalbusirecjul);?> <b style="color: red;"><?php echo number_format($totalbusirecoct1['SUM(reccived_amt)']); ?></b>
                        </td>
                        <td> <?php $totalbusireaug = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-08-01' AND '".$pyear."-08-31'");
                          $totalbusireaug1 = mysql_fetch_array($totalbusireaug);?><b style="color:red;"><?php echo number_format($totalbusireaug1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td> <?php $totalbusiresep = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-09-01' AND '".$pyear."-09-31'");
                          $totalbusiresep1 = mysql_fetch_array($totalbusiresep);?><b style="color:red;"><?php echo number_format($totalbusiresep1['SUM(reccived_amt)']);?>
                        </td>
                      </tr>

                      <tr>                       
                        <td><?php $totaloutjul = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-07-01' AND '".$pyear."-07-31'");
                          $totaloutjul1 = mysql_fetch_array($totaloutoct); 
                              $totaloutjultot = $rowsjul['SUM(total_amount)']+$totaloutjul1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totaloutjultot-$totalbusirecjul1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td><?php $totaloutaug = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-08-01' AND '".$pyear."-08-31'");
                          $totaloutaug1 = mysql_fetch_array($totaloutaug);
                          $totalCalaug = $rowsaug['SUM(total_amount)']+$totaloutaug1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totalCalaug-$totalbusireaug1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td><?php $totaloutsep = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-09-01' AND '".$pyear."-09-31'");
                          $totaloutsep1 = mysql_fetch_array($totaloutsep);
                          $totalCalsep = $rowssep['SUM(total_amount)']+$totaloutsep11['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totalCalsep-$totalbusiresep1['SUM(reccived_amt)']);?></b>
                        </td>
                      </tr>                     
                    </tbody>
                  </table>
                </div>
<div class="item" id="tab4">
                   <table class="table table-bordered month_table">
                    <thead>
                      <tr>                        
                        <th>Oct</th>
                        <th>Nov</th>
                        <th>Dec</th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr>                        
                        <td><?php  $totalbusioct = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-10-01' AND '".$pyear."-10-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsoct = mysql_fetch_array($totalbusioct);?> <b style="color: red;"><?php echo number_format($rowsoct['SUM(total_amount)']); ?></b>
                        </td>
                        <td><?php  $totalbusinov = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-11-01' AND '".$pyear."-11-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsnov = mysql_fetch_array($totalbusinov);?> <b style="color: red;"><?php echo number_format($rowsnov['SUM(total_amount)']); ?></b>
                        </td>
                        <td><?php  $totalbusidec = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-12-01' AND '".$pyear."-12-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $rowsdec = mysql_fetch_array($totalbusidec);?> <b style="color: red;"><?php echo number_format($rowsdec['SUM(total_amount)']); ?></b>
                        </td>
                    </tr>

                    <tr>                       
                        <td><?php  $totalbusirecoct = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `date_time` BETWEEN '".$pyear."-10-01' AND '".$pyear."-10-31' AND `ex_id`='".$_SESSION['user_id']."'"); 
                          $totalbusirecoct1 = mysql_fetch_array($totalbusirecoct);?> <b style="color: red;"><?php echo number_format($totalbusirecoct1['SUM(reccived_amt)']); ?></b>
                        </td>
                        <td> <?php $totalbusirenov = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-11-01' AND '".$pyear."-11-31'");
                         $totalbusirenov1 = mysql_fetch_array($totalbusirenov);?><b style="color:red;"><?php echo number_format($totalbusirenov1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td> <?php $totalbusiredec = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."'  AND`date_time` BETWEEN '".$pyear."-12-01' AND '".$pyear."-12-31'");
                        $totalbusiredec1 = mysql_fetch_array($totalbusiredec);?><b style="color:red;"><?php echo number_format($totalbusiredec1['SUM(reccived_amt)']);?>
                        </td>
                    </tr>
                    <tr>                       
                        <td><?php $totaloutoct = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-10-01' AND '".$pyear."-10-31'");
                          $totaloutoct1 = mysql_fetch_array($totaloutoct); 
                          $totaloutocttot = $rowsoct['SUM(total_amount)']+$totaloutoct1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totaloutocttot-$totalbusirecoct1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td><?php $totaloutnov = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-11-01' AND '".$pyear."-11-31'");
                          $totaloutnov1 = mysql_fetch_array($totaloutnov);
                          $totalCalnov = $rowsnov['SUM(total_amount)']+$totaloutnov1['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totalCalnov-$totalbusirenov1['SUM(reccived_amt)']);?></b>
                        </td>
                        <td><?php $totaloutdec = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-12-01' AND '".$pyear."-12-31'");
                          $totaloutdec1 = mysql_fetch_array($totaloutdec);
                          $totalCaldec = $rowsdec['SUM(total_amount)']+$totaloutdec11['SUM(total_amount)']; ?>
                          <b style="color:red;"><?php  echo number_format($totalCaldec-$totalbusiredec1['SUM(reccived_amt)']);?></b>
                        </td>
                    </tr>                     
                    </tbody>
                  </table>
                </div>
              </div>
                
              </div>

                </div>
                <div class="col-sm-12 col-xs-12" style="padding: 0px;">
                  <div class="col-sm-4 col-xs-4" style="padding: 0px;">
                  <button class=" btn btn-primary btn-xs carousel-control my_carousel" href="#myCarousel" class="" id="previous" data-slide="prev"><b style="color: white">Previous</b></button>
                  </div>
                  <div class="col-sm-5 col-xs-5" style="padding-left: 0px;">
                  <a data-toggle="modal" data-target="#yearwise" style="cursor: pointer;" onclick="year(<?php echo $_SESSION['user_id'];?>);">Year Wise Reports</a>
                  </div>
                  <div class="col-sm-3 col-xs-3" style="padding-right: 0px;text-align: right;">
                    <a href="#myCarousel" class="carousel-control my_carousel" id="next" data-slide="next"><button class=" btn btn-primary btn-xs"><b style="color: white">Next</b></button></a>
                  </div>
                </div>
                </div>
  <div class="col-sm-8 table-responsive">
  <table class="table table-bordered new_table">
  <thead>
    <tr>
      <th colspan="7" style="text-align: center; background-color: orange;">New Sale</th>
    </tr>
    <tr>      
      <th scope="col">New Sale</th>
      <th scope="col">Updation</th>
      <th scope="col">Total Sale</th>
      <th scope="col">Received Amount</th>
      <th scope="col">Outstanding Amount</th>
      <th scope="col">Credited</th>
      <th scope="col">Bounce</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td><b style="color:green;">
            <?php  $newsale = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `client_status`='New' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
            $selnewsale = mysql_fetch_array($newsale);?></b> <a href="previous_yearbusiness.php#new_sale_table1"><b style="color: green;cursor: pointer;" id="new_sale"><?php echo number_format($selnewsale['SUM(total_amount)']); ?></b></a>
        </td>

        <td><b style="color:green;">
          <?php  $updates = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_SESSION['user_id']."' AND `ex_id`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
          $updateSel = mysql_fetch_array($updates);

          $myup = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_SESSION['user_id']."' AND `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
          $myupdate = mysql_fetch_array($myup);

          $selfupdates = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `update_by`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
          $selfupdateSel = mysql_fetch_array($selfupdates);

          $newsales = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
          $selnewsales = mysql_fetch_array($newsales);?> </b> <a href="previous_yearbusiness.php#updation_table"><b style="color: purple;cursor: pointer;" id="updation1"><?php echo number_format($updateSel['SUM(total_amount)']+$myupdate['SUM(total_amount)']); ?></b></a>
        </td>

        <td><a href="previous_yearbusiness.php#total_sale_table"><b style="color:green;cursor: pointer;" id="total_sale1"><?php echo number_format($updateSel['SUM(total_amount)']+$myupdate['SUM(total_amount)']+$selnewsales['SUM(total_amount)']); ?></b></a>
        </td>

        <td><b style="color:green;cursor: pointer;">
          <?php  $received = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `update_by`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
          $receivedSel = mysql_fetch_array($received);
          
          $orderreceived = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."' AND `update_by`=0 AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
          $orderreceivedSel = mysql_fetch_array($orderreceived);?>
           </b> <a href="previous_yearbusiness.php#received_table"><b style="color: orange;cursor: pointer;" id="received_amt"><?php echo number_format($receivedSel['SUM(reccived_amt)']+$orderreceivedSel['SUM(reccived_amt)']); ?></b></a>
        </td>

        <td> <a href="previous_yearbusiness.php#outstanding_table"><b style="color:red;cursor: pointer;" id="outstanding">
          <?php  $totalOut = $updateSel['SUM(total_amount)']+$myupdate['SUM(total_amount)']+$selnewsales['SUM(total_amount)']; 
          $totalDeduct = $receivedSel['SUM(reccived_amt)']+$orderreceivedSel['SUM(reccived_amt)']; 
          echo number_format($CalculataOutstand = $totalOut-$totalDeduct);?> </b></a>
        </td>

        <td><b style="color:purple;cursor: pointer;" id="deposite"><?php $deposite = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."' AND(`ex_id`!='' AND `cheque_status`='Credited' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31')"); 
          $deposite1 = mysql_fetch_array($deposite);
          setlocale(LC_MONETARY, 'en_IN');
          echo  $amount1stsd = money_format('%!i', $deposite1['SUM(reccived_amt)']);?></b>
        </td>

        <td><a href="previous_yearbusiness.php#bounce_table"><b style="color:brown;cursor: pointer;" id="bounce"> <?php  $bounce = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."' AND `cheque_status`='Bounce' AND `ex_id`!='' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
          $bounce = mysql_fetch_array($bounce);
          setlocale(LC_MONETARY, 'en_IN');
          echo  $amount1stsdB = money_format('%!i', $bounce['SUM(reccived_amt)']);
          ?></b> </a>
        </td>
    </tr>
  </tbody>
</table>
</div>
<br>
<div class="col-sm-8 table-responsive">
  <table class="table table-bordered new_table">
  <thead>
    <tr>
      <th colspan="" rowspan="" style="text-align: center; background-color: #4295d6;" width="100">New Sale</th>
      <th colspan="3" style="text-align: center; background-color: #4295d6;">Updation(Share)</th>
      <th rowspan="" style="text-align: center; background-color: #4295d6;" width="100">Total Revenue</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th style="text-align: center;border-bottom: 1px solid gray;"></th>
      <th style="text-align: center;border-bottom: 1px solid gray;"><h6>Self Client(100%)</h6></th>
      <th style="text-align: center;border-bottom: 1px solid gray;"><h6>Other SE Client(60%)</h6></th>
      <th style="text-align: center;border-bottom: 1px solid gray;"><h6>Updated by other SE(40%)</h6></th>
      <th style="text-align: center;border-bottom: 1px solid gray;"></th>
    </tr>
    <tr>
      <td style="text-align: center;border-bottom: 1px solid gray;"><b style="color:green;"><?php  $newsale = mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
        $selnewsale = mysql_fetch_array($newsale);?> <b style="color: green;"><?php echo number_format($selnewsale['SUM(total_amount)']); ?></b>
      </td>

      <td style="text-align: center;border-bottom: 1px solid gray;"><b style="color:green;">
        <?php   $hundred = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `update_by`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
        $hundredSel = mysql_fetch_array($hundred);?> <b style="color: red;"><?php echo number_format($hundredSel['SUM(total_amount)']); ?></b>
      </td>

      <td style="text-align: center;border-bottom: 1px solid gray;"><b style="color:orange;"><?php   $sixty = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`!='".$_SESSION['user_id']."' AND `update_by`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
        $sixtySel = mysql_fetch_array($sixty);?></b> <b style="color: red;"><?php echo number_format($sixtySel['SUM(total_amount)']*60/100); ?></b><br/>(60% of <?php echo number_format($sixtySel['SUM(total_amount)']); ?>)
      </td>

      <td style="text-align: center;border-bottom: 1px solid gray;"><b style="color:orange;"><?php   $fourty = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `update_by`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
        $fourtySel = mysql_fetch_array($fourty);?></b> <b style="color: red;"><?php echo number_format($fourtySel['SUM(total_amount)']*40/100); ?></b><br/>(40% of <?php echo number_format($fourtySel['SUM(total_amount)']); ?>)
      </td>
      <td style="text-align: center;border-bottom: 1px solid gray;"><h3 style="color: green; font-family: bold;"><?php echo $revenue = number_format($selnewsale['SUM(total_amount)']+$hundredSel['SUM(total_amount)']+$sixtySel['SUM(total_amount)']*60/100+$fourtySel['SUM(total_amount)']*40/100);?>  </h3>
      </td>
    </tr>
  </tbody>
</table>
</div>

    <div class="col-sm-12">
      <!-- New Sale Table -->
      <div class="col-sm-12 table-responsive" id="new_sale_table1">
      <table class="table-bordered table month_div" style="background-color: #77d180;" id="data-new_sale_table">
        <thead>
          <tr class="bag">
            <th style="background-color:#42d651;">Sr No</th>
            <th style="background-color:#42d651;">Client Name</th>
            <th style="background-color:#42d651;">Email Id</th>
            <th style="background-color:#42d651;">Mobile No.</th>
            <th style="background-color:#42d651;">Total Amount</th>
            <th style="background-color:#42d651;">Received Amount</th>
            <th style="background-color:#42d651;">Outstanding Amount</th>
            <th style="background-color:#42d651;">View History</th>
          </tr>
        </thead>
        <tbody>
          <?php $order_form = mysql_query("SELECT * FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `client_status`='New' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
          $i=1;
          while($selOrderform = mysql_fetch_array($order_form)){
            $order_form_payment_mode = mysql_query("SELECT  SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$selOrderform['id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $selectOrderform = mysql_fetch_array($order_form_payment_mode);?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $selOrderform['name'];?> <?php echo $selOrderform['last_name'];?></td>
            <td><?php echo $selOrderform['email'];?></td>
            <td><?php echo $selOrderform['mobile'];?></td>
            <td><b style="color: red;"><?php echo $selOrderform['total_amount'];?></b></td>
            <td><?php echo $selectOrderform['SUM(reccived_amt)'];?></td>
            <td><?php echo $selOrderform['remaining_amt'];?></td>
            <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selOrderform['id'];?>);">History</button></td>
          </tr>
          <?php $i++;  }?>
        </tbody>
      </table>
</div>
      <!-- Updation Table -->
      <div class="col-sm-12 table-responsive" id="updation_table">
       <table class="table-bordered table month_div" style="background-color: #77d180;" id="table-updation">
        <thead>
          <tr class="bag">
            <th style="background-color:purple;color: white;">Sr No</th>
            <th style="background-color:purple;color: white;">Client Name</th>
            <th style="background-color:purple;color: white;">Email Id</th>
            <th style="background-color:purple;color: white;">Mobile No.</th>
            <th style="background-color:purple;color: white;">Total Amount</th>
            <th style="background-color:purple;color: white;">Received Amount</th>
            <th style="background-color:purple;color: white;">Outstanding Amount</th>
            <th style="background-color:purple;color: white;">View History</th>
          </tr>
        </thead>
        <tbody>         
          <tr>
            <td></td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
            <td> </td>
          </tr>         
        </tbody>
      </table>
       
</div>
      <!-- Total Sale Table -->
      <div class="col-sm-12 table-responsive" id="total_sale_table">
      <table class="table-bordered table " id="table_totalsale">
        <thead>
          <tr class="bag">
            <th style="background-color:#42d651;">Sr No</th>
            <th style="background-color:#42d651;">Client Name</th>
            <th style="background-color:#42d651;">Email Id</th>
            <th style="background-color:#42d651;">Mobile No.</th>
            <th style="background-color:#42d651;">Total Amount</th>
            <th style="background-color:#42d651;">Received Amount</th>
            <th style="background-color:#42d651;">Outstanding Amount</th>
            <th style="background-color:#42d651;">View History</th>
          </tr>
        </thead>
         <tbody>
          <?php $order_form = mysql_query("SELECT * FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
          $i=1;
          while($selOrderform = mysql_fetch_array($order_form)){
            $order_form_payment_mode = mysql_query("SELECT  SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$selOrderform['id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $selectOrderform = mysql_fetch_array($order_form_payment_mode);

            $updatesamtnew = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_SESSION['user_id']."' AND `ex_id`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
             $updateSelamtnew = mysql_fetch_array($updatesamtnew);?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $selOrderform['name'];?> <?php echo $selOrderform['last_name'];?></td>
            <td><?php echo $selOrderform['email'];?></td>
            <td><?php echo $selOrderform['mobile'];?></td>
            <td><b style="color: green;"><?php echo $selOrderform['total_amount']+$updateSelamtnew['SUM(total_amount)'];?></b></td>
            <td><?php echo $selectOrderform['SUM(reccived_amt)'];?></td>
            <td><b style="color: red;"><?php $totalAmts = $selOrderform['total_amount']+$updateSelamtnew['SUM(total_amount)'];
            echo $totalAmts-$selectOrderform['SUM(reccived_amt)'];?></b></td>
            <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selOrderform['id'];?>);">History</button></td>
          </tr>
          <?php $i++;  }?>
        </tbody>
      </table>
</div>
      <!-- Receivd Amount Table -->
      
         <div class="col-sm-12 table-responsive" id="received_table">
      <table class="table-bordered table " id="rm_received">
        <thead>
         <tr class="bag">
            <th style="background-color:orange;">Sr No</th>
            <th style="background-color:orange;">Client Name</th>
            <th style="background-color:orange;">Email Id</th>
            <th style="background-color:orange;">Mobile No.</th>
            <th style="background-color:orange;">Total Amount</th>
            <th style="background-color:orange;">Received Amount</th>
            <th style="background-color:orange;">Outstanding Amount</th>
            <th style="background-color:orange;">View History</th>
          </tr>
        </thead>
        <tbody>
          <?php $order_form = mysql_query("SELECT * FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
          $i=1;
          while($selOrderform = mysql_fetch_array($order_form)){
            $order_form_payment_mode = mysql_query("SELECT  SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$selOrderform['id']."' AND `reccived_amt`!='' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $selectOrderform = mysql_fetch_array($order_form_payment_mode);

            $updatesamtnew = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_SESSION['user_id']."' AND `ex_id`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
             $updateSelamtnew = mysql_fetch_array($updatesamtnew);?>             
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $selOrderform['name'];?> <?php echo $selOrderform['last_name'];?></td>
            <td><?php echo $selOrderform['email'];?></td>
            <td><?php echo $selOrderform['mobile'];?></td>
            <td><b style="color: green;"><?php echo $selOrderform['total_amount']+$updateSelamtnew['SUM(total_amount)'];?></b></td>
            <td><?php echo $selectOrderform['SUM(reccived_amt)'];?></td>
            <td><b style="color: red;"><?php $totalAmts = $selOrderform['total_amount']+$updateSelamtnew['SUM(total_amount)'];
            echo $totalAmts-$selectOrderform['SUM(reccived_amt)'];?></b></td>
            <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selOrderform['id'];?>);">History</button></td>
          </tr>
          <?php $i++;  }  ?>
        </tbody>
      </table>
</div>
      <!-- Outstanding Amount Table -->
      <div class="col-sm-12 table-responsive" id="outstanding_table">
      <table class="table-bordered table " id="rm_outstanding">
        <thead>
         <tr class="bag">
            <th style="background-color:red; color: white;">Sr. No.</th>
            <th style="background-color:red; color: white;">Client Name</th>
            <th style="background-color:red; color: white;">Email Id</th>
            <th style="background-color:red; color: white;">Mobile No.</th>
            <th style="background-color:red; color: white;">Total Amount</th>
            <th style="background-color:red; color: white;">Received Amount</th>
            <th style="background-color:red; color: white;">Outstanding Amount</th>
            <th style="background-color:red; color: white;">View History</th>
          </tr>
        </thead>
        <tbody>
         <?php $order_form = mysql_query("SELECT * FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
          $i=1;
          while($selOrderform = mysql_fetch_array($order_form)){
            $order_form_payment_mode = mysql_query("SELECT  SUM(reccived_amt),`client_id` FROM `order_form_payment_mode` WHERE `client_id`='".$selOrderform['id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $selectOrderforms = mysql_fetch_array($order_form_payment_mode);
            $clientname = mysql_query("SELECT * FROM `order_form` WHERE `id`='".$selectOrderforms['client_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $countorder = mysql_fetch_array($clientname);

            $updatesamt = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_SESSION['user_id']."' AND `ex_id`!='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
             $updateSelamt = mysql_fetch_array($updatesamt);
            if($countorder['total_amount']!=$selectOrderforms['SUM(reccived_amt)']){?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $countorder['name'];?> <?php echo $countorder['last_name'];?></td>
            <td><?php echo $countorder['email'];?></td>
            <td><?php echo $countorder['mobile'];?></td>
            <td><b style="color: red;"><?php echo $selOrderform['total_amount']+$updateSelamt['SUM(total_amount)'];?></b></td>
            <td><?php echo $selectOrderforms['SUM(reccived_amt)'];?></td>
            <td><?php $totalAmt = $selOrderform['total_amount']+$updateSelamt['SUM(total_amount)'];
            echo $totalAmt-$selectOrderforms['SUM(reccived_amt)'];?></td>
            <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selOrderform['id'];?>);">History</button></td>
          </tr>
          <?php } $i++;  }?>
        </tbody>
      </table>
</div>
      <!-- Deposite Table -->
      <div class="col-sm-12 table-responsive" id="deposite_table">
       <table class="table-bordered table month_div" style="background-color: #77d180;" id="rm_deposite">
        <thead>
          <tr class="bag">
            <th style="background-color:purple; color: white;">Sr No</th>
            <th style="background-color:purple; color: white;">Client Name</th>
            <th style="background-color:purple; color: white;">Email Id</th>
            <th style="background-color:purple; color: white;">Mobile No.</th>
            <th style="background-color:purple; color: white;">Total Amount</th>
            <th style="background-color:purple; color: white;">Received Amount</th>
            <th style="background-color:purple; color: white;">Outstanding Amount</th>
            <th style="background-color:purple; color: white;">View History</th>
          </tr>
        </thead>
        <tbody>
         <?php 
         $order_form = mysql_query("SELECT * FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."' AND `cheque_status`='Credited' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
          $i=1;
          while($selOrderform = mysql_fetch_array($order_form)){
            $order_form_payment_mode = mysql_query("SELECT * FROM `order_form` WHERE `id`='".$selOrderform['client_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $selectOrderform = mysql_fetch_array($order_form_payment_mode);

            $order_forms = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$selectOrderform['ex_id']."' AND `cheque_status`='Credited' AND `client_id`='".$selectOrderform['id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $order_formsSel = mysql_fetch_array($order_forms);

              $updatesamt1 = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_POST['exname']."' AND `ex_id`!='".$_POST['exname']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
             $updateSelamt1 = mysql_fetch_array($updatesamt1);?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $selectOrderform['name'];?> <?php echo $selectOrderform['last_name'];?></td>
            <td><?php echo $selectOrderform['email'];?></td>
            <td><?php echo $selectOrderform['mobile'];?></td>
            <td><b style="color: red;"><?php echo $selectOrderform['total_amount'];?></b></td>
            <td><?php echo $order_formsSel['SUM(reccived_amt)'];?></td>
            <td><?php $totalAmtas = $selectOrderform['total_amount']+$updatesamt1['SUM(total_amount)'];
            echo $totalAmtas-$order_formsSel['SUM(reccived_amt)'];?></td>
            <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selectOrderform['id'];?>);">History</button></td>
          </tr>
          <?php $i++;  }?>
        </tbody>
      </table>
</div>
      <!-- Bounce Table -->
     
       <div class="col-sm-12 table-responsive" id="bounce_table">
       <table class="table-bordered table month_div" style="background-color: #77d180;" id="rm_bounce">
        <thead>
          <tr class="bag">
            <th style="background-color:brown; color: white;">Sr No</th>
            <th style="background-color:brown; color: white;">Client Name</th>
            <th style="background-color:brown; color: white;">Email Id</th>
            <th style="background-color:brown; color: white;">Mobile No.</th>
            <th style="background-color:brown; color: white;">Total Amount</th>
            <th style="background-color:brown; color: white;">Received Amount</th>
            <th style="background-color:brown; color: white;">Outstanding Amount</th>
            <th style="background-color:brown; color: white;">View History</th>
          </tr>
        </thead>
        <tbody>
         <?php //$order_form = mysql_query"SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_POST['exname']."' AND `cheque_status`='Credited'");
         $order_form = mysql_query("SELECT * FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."' AND `cheque_status`='Bounce' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
          $i=1;
          while($selOrderform = mysql_fetch_array($order_form)){
            $order_form_payment_mode = mysql_query("SELECT * FROM `order_form` WHERE `id`='".$selOrderform['client_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $selectOrderform = mysql_fetch_array($order_form_payment_mode);

            $order_forms = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$selectOrderform['ex_id']."' AND `cheque_status`='Credited' AND `client_id`='".$selectOrderform['id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $order_formsSel = mysql_fetch_array($order_forms);

              $updatesamt1 = mysql_query("SELECT SUM(total_amount),SUM(reccived_amt) FROM `updation_amount` WHERE `update_by`='".$_POST['exname']."' AND `ex_id`!='".$_POST['exname']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'"); 
             $updateSelamt1 = mysql_fetch_array($updatesamt1);?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $selectOrderform['name'];?> <?php echo $selectOrderform['last_name'];?></td>
            <td><?php echo $selectOrderform['email'];?></td>
            <td><?php echo $selectOrderform['mobile'];?></td>
            <td><b style="color: red;"><?php echo $selectOrderform['total_amount'];?></b></td>
            <td><?php echo $order_formsSel['SUM(reccived_amt)'];?></td>
            <td><?php $totalAmtas = $selectOrderform['total_amount']+$updatesamt1['SUM(total_amount)'];
            echo $totalAmtas-$order_formsSel['SUM(reccived_amt)'];?></td>
            <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selectOrderform['id'];?>);">History</button></td>
          </tr>
          <?php $i++;  }?>
        </tbody>
      </table>
</div>
</div>
</div>
<div class="bhoechie-tab-content">
<div class="container">
<div class="col-sm-12 table-responsive" id="new_buss">
    <table id="tabledaily" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="background-color:#8992d6;color: white;">Sr No</th>
            <th style="background-color:#8992d6;color: white;">Client Name</th>
            <th style="background-color:#8992d6;color: white;">Email Id</th>
            <th style="background-color:#8992d6;color: white;">Mobile No.</th>
            <th style="background-color:#8992d6;color: white;">Total Amount</th>
            <th style="background-color:#8992d6;color: white;">Received Amount</th>
            <th style="background-color:#8992d6;color: white;">Outstanding Amount</th>
            <th style="background-color:#8992d6;color: white;">View History</th>
        </tr>
       </thead>
        <tbody>
          <?php $order_form = mysql_query("SELECT * FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `client_status`='New' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
          $i=1;
          while($selOrderform = mysql_fetch_array($order_form)){
            $order_form_payment_mode = mysql_query("SELECT  SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$selOrderform['id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
            $selectOrderform = mysql_fetch_array($order_form_payment_mode);?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php echo $selOrderform['name'];?> <?php echo $selOrderform['last_name'];?></td>
            <td><?php echo $selOrderform['email'];?></td>
            <td><?php echo $selOrderform['mobile'];?></td>
            <td><b style="color: red;"><?php echo $selOrderform['total_amount'];?></b></td>
            <td><?php echo $selectOrderform['SUM(reccived_amt)'];?></td>
            <td><?php echo ($selOrderform['total_amount']-$selectOrderform['SUM(reccived_amt)']);?></td>
            <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selOrderform['id'];?>);">History</button></td>
          </tr>
          <?php $i++;  }?>
        </tbody>
    </table>
 </div>
 <div class="col-md-12 table-responsive">
    <table id="data-tablessss" class="table table-striped table-bordered table-hover">
      <?php   $totalAmountCal =  mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `client_status`='New' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
      $selCalculate = mysql_fetch_array($totalAmountCal);
       $uppayments = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
       $ressels = mysql_fetch_array($uppayments);
           $recAmount = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
       $selRecamount = mysql_fetch_array($recAmount);?>
      <tr>
        <td><b><h4 style="color: green;">Total Amount :-  <?php echo $totalCals = number_format($selCalculate['SUM(total_amount)']+$ressels['SUM(total_amount)']);?></h4></b></td>
        <td><b><h4 style="color: blue;">Received Amount :- <?php echo $recamt = number_format($selRecamount['SUM(reccived_amt)']);?></h4></b></td>
        <td><b><h4 style="color: red;">Outstanding Amount :- <?php echo $out = number_format($selCalculate['SUM(total_amount)']+$ressels['SUM(total_amount)']-$selRecamount['SUM(reccived_amt)']);?></h4></b></td>          
       
      </tr>
    </table>
  </div>
</div>
                </div>
                <div class="bhoechie-tab-content">
                   <div class="col-sm-12 table-responsive" id="new_buss">
    <table id="tabledailydsr" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                  <th style="background-color:#8992d6;color: white;">Sr.No.</th>
                  <th style="background-color:#8992d6;color: white;">Client Name</th>
                  <th style="background-color:#8992d6;color: white;">Email</th>
                  <th style="background-color:#8992d6;color: white;">Mobile</th>
                  <th style="background-color:#8992d6;color: white;">Place Of Meeting</th>
                  <th style="background-color:#8992d6;color: white;">Dsr Fill Date</th>             
                  <th style="background-color:#8992d6;color: white;">Converted / Not Converted</th>
                   
            </tr>
       </thead>
       <tbody> 
     <?php  $select = mysql_query("SELECT * FROM `dsr` WHERE `remove_status`=1  AND `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '01-01-".$pyear."' AND '12-01-".$pyear."' ORDER BY id DESC");    
      $i=1;
      while($fetch=mysql_fetch_array($select)){?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo ucfirst($fetch['client_name']);?> <?php echo ucfirst($fetch['client_lname']);?></td>
        <td><?php echo $fetch['email'];?></td>
        <td><?php echo $fetch['mobile'];?></td>
        <td><?php echo $fetch['meet'];?></td>
        <td><?php echo $fetch['dates1'];?></td>       
        <td><?php echo $fetch['converted_status'];?></td>         
      </tr>      
     <?php $i++;} ?>
    </tbody>
    </table>
 </div>
                </div>
      <div class="bhoechie-tab-content">
            <div class="col-sm-12 table-responsive" id="order_form">
                <table id="tabledailyorder" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="background-color:#8992d6;color: white;">Sr No</th>
                        <th style="background-color:#8992d6;color: white;">Client Name</th>
                        <th style="background-color:#8992d6;color: white;">Email Id</th>
                        <th style="background-color:#8992d6;color: white;">Mobile No.</th>
                        <th style="background-color:#8992d6;color: white;">Total Amount</th>
                        <th style="background-color:#8992d6;color: white;">Received Amount</th>
                        <th style="background-color:#8992d6;color: white;">Outstanding Amount</th>
                        <th style="background-color:#8992d6;color: white;">View History</th>
                    </tr>
                   </thead>
                    <tbody>
                      <?php $order_form = mysql_query("SELECT * FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
                      $i=1;
                      while($selOrderform = mysql_fetch_array($order_form)){
                        $order_form_payment_mode = mysql_query("SELECT  SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$selOrderform['id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
                        $selectOrderform = mysql_fetch_array($order_form_payment_mode);?>
                      <tr>
                        <td><?php echo $i;?></td>
                        <td><?php echo $selOrderform['name'];?> <?php echo $selOrderform['last_name'];?></td>
                        <td><?php echo $selOrderform['email'];?></td>
                        <td><?php echo $selOrderform['mobile'];?></td>
                        <td><b style="color: red;"><?php echo $selOrderform['total_amount'];?></b></td>
                        <td><?php echo $selectOrderform['SUM(reccived_amt)'];?></td>
                        <td><?php echo ($selOrderform['total_amount']-$selectOrderform['SUM(reccived_amt)']);?></td>
                        <td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#userview" onclick="historyclient(<?php echo $selOrderform['id'];?>);">History</button></td>
                      </tr>
                      <?php $i++;  }?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 table-responsive">
                <table id="data-tablessss" class="table table-striped table-bordered table-hover">
                   <?php  $totalAmountCal =  mysql_query("SELECT SUM(total_amount) FROM `order_form` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
                   $selCalculate = mysql_fetch_array($totalAmountCal);
                   $uppayments = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
                   $ressels = mysql_fetch_array($uppayments);
                   $recAmount = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `ex_id`='".$_SESSION['user_id']."' AND `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-12-31'");
                   $selRecamount = mysql_fetch_array($recAmount);?>
                  <tr>
                    <td><b><h4 style="color: green;">Total Amount :-  <?php echo $totalCals = number_format($selCalculate['SUM(total_amount)']+$ressels['SUM(total_amount)']);?></h4></b></td>
                    <td><b><h4 style="color: blue;">Received Amount :- <?php echo $recamt = number_format($selRecamount['SUM(reccived_amt)']);?></h4></b></td>
                    <td><b><h4 style="color: red;">Outstanding Amount :- <?php echo $out = number_format($selCalculate['SUM(total_amount)']+$ressels['SUM(total_amount)']-$selRecamount['SUM(reccived_amt)']);?></h4></b></td>                             
                  </tr>
                </table>
            </div>
      </div>
                <div class="bhoechie-tab-content">
                    <center>
                      <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;color:#55518a"></h1>
                      <h2 style="margin-top: 0;color:#55518a">Coming Soon</h2>
                      <h3 style="margin-top: 0;color:#55518a"></h3>
                    </center>
                </div>
            </div>
        </div>
  </div>
</div>
<div class="modal fade" id="yearwise" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><b style="color: red;">Year Wise Report</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="col-sm-12">
  <table id="data-tableDSRrepor" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <thead>
            <tr>
              <th>Sr.No.</th>
              <th>Year</th>
              <th>Date Time</th>
              <th>Total Business</th>
              <th>Outstanding Amount</th>
              <th>Received Amount</th>
             <!--  <th>Action</th> -->
            </tr>
    </thead>
    <tbody>
              <?php
                $sqlW = mysql_query("SELECT * FROM `order_form` WHERE `date_time` BETWEEN '".$pyear."-01-01' AND '".$pyear."-01-31' AND `ex_id`='".$_SESSION['user_id']."'");
              $i=1;
              while($selsql = mysql_fetch_array($sqlW)){?>
          <tr>
            <input type="hidden" value="<?php echo $ids;?>" id="<?php echo $_POST['ids'];?>" name="cid">
            <td><?php echo $i;?></td>
            <td><?php echo $selsql['message'];?></td>
            <td><?php echo $selsql['date_time'];?></td>
            <td><?php echo $selsql['next_appointment'];?></td>
            <td><?php echo $selsql['callStatus'];?></td>
            <td><?php echo $selsql['callStatus'];?></td>
          </tr>
        <?php $i++; } ?>
    </tbody>
  </table>
</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<style>

</style>
<script type="text/javascript">
    $(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>
</div>
</div> 
</div>
<div id="" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div id=""></div>
  </div>
</div>
<div id="userview" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
       <div id="userhistory"></div>
    </div>
  </div>
</div>
<script>
    function historyclient(id){
      var id =+id;
      $.ajax({
        type:"post",
        url:"ajax/userhistory.php",
        data:"name="+id,
        success:function(data){
         //alert(data);
        $("#userhistory").html(data);
      }
   });
    }
    function year(id){
      var id =+id;
      $.ajax({
        type:"post",
        url:"ajax/yearresult.php",
        data:"name="+id,
        success:function(data){
        $("#yearresult").html(data);
      }

   });
    }
</script>
<script>
    $(document).ready(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            pause: true,
            interval: false
        });
    });
})
  </script>
  <?php include("footer.php");?>
   <script>
        $(document).ready(function() {
            App.init();
            //TableManageButtons.init();
            FormPlugins.init();
            FormPlugins2.init();
            FormPlugins6.init();
        });
    </script>
<?php mysql_close($connect); ?>
