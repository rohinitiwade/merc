<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
<?php include("head.php");?>
<script>
  $(document).ready(function(){
    $('#data-tableDaily').dataTable();
  });
  $(document).ready(function(){
    $("#all_report").addClass("active");
    $("#nextappointment_report").addClass("active");
    $("#callDate").datepicker({
        todayHighlight: !0,
        autoclose: true,
        format:'dd/mm/yyyy'
    })
  });
</script>
</head>
<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>  
    <div id="content" class="content">
        <ol class="breadcrumb" style="width: 100%;">
            <li><a href="http://crm.airinfotech.in/telecaller/">Home</a>/</li>
            <li><a href="http://crm.airinfotech.in/telecaller/">Dashboard</a>/</li>
            <li class="active">Next Appointment List</li>
        </ol>
<div class="details">
  <h3 class="col-sm-8" style="margin-top: 0px; color: green;"><span>Next Appointment List &nbsp;&nbsp;</span><a href="http://crm.airinfotech.in/telecaller/" class="btn btn-primary btn-xs">Back To Home Page</a>&nbsp;&nbsp;<a href="allotedclient2.php" class="btn btn-warning btn-xs">Go to Alloted Client</a>
  </h3>  
   <?php include("timezone.php");?>
  <hr style="float: left;width: 100%;">
<?php if(isset($_POST['submit'])){
  $dates = $_POST['date'];
} ?>
<form name="" method="POST">
<div class="row">
  <div class="col-sm-1 col-xs-3"><h5>Select Date</h5></div>
  <div class="col-sm-2 col-xs-5"><input type="text"  name="date" id="callDate" class="form-control" value="<?php echo $dates; ?>" placeholder="Select Date Here" autocomplete="OFF" ></div>
  <div class="col-sm-4 col-xs-3"><input type="submit" value="Submit" name="submit" class="btn btn-primary btn-sm"></div>
</div>
</form>
  <hr style="float: left;width: 100%;">
  <div class="col-sm-12 table-responsive dsr_form_reports_tb">
    <table id="data-tableDaily" class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
      <th>Sr.No.</th>
      <th>Client Name</th>
      <th>Email</th>
      <th>Mobile</th>
      <th>City</th>
      <th>Product</th>
       <th>Haspid</th>
      <th>Client Status</th>
      <th>Called Details</th>
      <th>Appintment Date</th>
      <th>Called For</th>
      <th>View Details</th>
      <th>Source</th>
    </tr>
</thead>
    <tbody> 
     <?php
     if($dates==""){
     $sql = mysql_query("SELECT DISTINCT `client_id`,`next_appointment` FROM `clientstatus` WHERE `ex_id`='".$_SESSION['user_id']."' AND `next_appointment`='".date("d/m/Y")."' ORDER BY id DESC");
    }else{
     $sql=mysql_query("SELECT DISTINCT `client_id`,`next_appointment` FROM `clientstatus` WHERE `ex_id`='".$_SESSION['user_id']."' AND `next_appointment`='".$dates."' ORDER BY id DESC");
    }
      $i=1;
      while($row=mysql_fetch_array($sql)) { 
        $sel2=mysql_query("SELECT * FROM `airinfot_newcustomercontact` WHERE `custid`='".$row['client_id']."'");
        $fetch3=mysql_fetch_array($sel2);
        ?>
      <tr>
        <td><?php echo $i;?></td>
        <td><?php echo ucfirst($fetch3['name']);?> <?php echo ucfirst($fetch3['last_name']);?></td>
        <td><?php echo $fetch3['email'];?></td>
        <td><?php echo $fetch3['personal_mobile'];?> <?php echo $fetch3['personal_phoness'];?> <?php echo $fetch3['work_phone'];?></td>
        <!-- <td><?php echo $fetch3['work_street'];?></td> -->
        <td><?php echo $fetch3['personal_city']?></td>
        <td><?php echo $fetch3['new_Product']?></td>
        <td><?php echo $fetch3['Haspid']?></td>
        <td><a data-toggle="modal" data-target="#myModalDSR" class="btn btn-success btn-xs" onclick="Status(<?php echo $fetch3['custid'];?>);">Add Remark</a>
        </td>
        <td>
          <?php  $clientStatus = mysql_query("SELECT * FROM `clientstatus` WHERE `ex_id`='".$_SESSION['user_id']."' AND `client_id`='".$row['client_id']."'"); $selClient = mysql_num_rows($clientStatus); if($selClient>0){?>
          <a data-toggle="modal" data-target="#history" class="btn btn-warning btn-xs" onclick="history(<?php echo $fetch3['custid'];?>);">Called Details</a>
          <?php } ?>
        </td>
        <td><?php echo $row['next_appointment']?></td>
        <td id="change_input<?php echo $i;?>"><input type="button" onclick="calledfor(<?php echo $fetch3['custid'];?>)" value="Called For" data-target="#reasonModal" data-toggle="modal" class="btn btn-xs btn-primary" name=""></td>
        <td><button class="btn btn-info btn-xs" type="button" data-target="#callingModal" onclick="details(<?php echo $fetch3['custid'];?>)" data-toggle="modal">View Details</button></td>
        <td><?php echo $fetch3['cust_status']?></td>
      </tr>
      <script>
      $(document).ready(function(){
      $("#called_for").on("change",function(){
        var this_val = $(this).val();
        if(this_val == "other"){
            $("#other_reason_inp").show();
          }
          else{
            $("#other_reason_inp").hide();
          }
      })
      });
    </script>
     <?php $i++;} mysql_close($connect);?>
    </tbody>
    </table>
  </div>
</div>
</div>
<div id="callingModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">View Client Details</h3>
      </div>
      <div class="modal-body">
      <div id="details"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div id="callin" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Employee Information</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="telecaller_report_box">
            <div class="tele_heading">Employee Information</div>
            <div class="TeleCaller_div">
               <input type="hidden" value="<?php echo $row['id'];?>" name="ids">
               <div class="col-sm-12">
                  <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">First Name : </span>
                           <input type="text" class="form-control email_check" placeholder="abc@gmail.com" id="email" name="email" value="<?php echo $row['name'];?>" required/>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Last name : </span>
                           <input type="text" class="form-control email_check" placeholder="abc@gmail.com" id="email" name="email" value="<?php echo $row['last_name'];?>" required/>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Email : </span>
                           <input type="text" class="form-control email_check" placeholder="abc@gmail.com" id="email" name="email" value="<?php echo $row['email'];?>" required/>
                        </div>
                     </div>
                  </div>
                   <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Pin Code : </span>
                           <input type="text" class="form-control" placeholder="444567" id="pin" maxlength="6" name="pincode" value="<?php echo $row['pincode'];?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Mobile : </span>
                           <input type="text" class="form-control" placeholder="444567" id="pin" maxlength="6" name="mobile" value="<?php echo $row['mobile'];?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Pin Code : </span>
                           <input type="text" class="form-control" placeholder="444567" id="pin" maxlength="6" name="pincode" value="<?php echo $row['pincode'];?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Address : </span>
                           <input class="form-control" id="add" name="address" placeholder="10/233,dhantoli" value="<?php echo $row['address'];?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">State : </span>
                           <input class="form-control" id="add" name="address" placeholder="10/233,dhantoli" value="">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">City Name : </span>
                           <input class="form-control" id="add" name="address" placeholder="dhantoli" value="">
                           <div class="col-md-12">
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_city"></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Phone(R) : </span>
                           <input type="text" class="form-control" placeholder="0712 678899" id="pin" maxlength="10"  name="rphone" value="<?php echo $row['rphone'];?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Phone(O) : </span>
                           <input type="text" class="form-control" placeholder="xxxx xxxx" id="pin" maxlength="10" name="ophone" value="<?php echo $row['ophone'];?>">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-2">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Mobile : </span>
                           <input type="text" class="form-control" placeholder="xx xx xxxxxx" id="mobile" value="<?php echo $row['mobile'];?>" name="mobile">
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="row m-b-15">
                        <div class="col-md-12">
                           <span class="tele_span">Fax No : </span>
                           <input type="text" class="form-control" placeholder="xxxxxxxxxxxxxxx" id="fax" name="fax" value="<?php echo $row['fax'];?>">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tele_heading">Payment Details : </div>
            <div class="col-sm-12 form-group">
               <div class="col-sm-4" >
                  <span class="tele_span">Cheque / Transaction Id/ NEFT Details :</span>
                  <input name="che_no" type="text" class="form-control" placeholder="" required="required" value="<?php echo $row['che_no'];?>">
               </div>
               <div class="col-sm-4" >
                  <span class="tele_span">Bank Name :</span>
                  <input name="bank_name" type="text" class="form-control" placeholder="" required="required" value="<?php echo $row['bank_name'];?>">
               </div>
               <div class="col-sm-4" >
                  <span class="tele_span">Amount Reccived : </span>
                  <input name="reccive_amt" type="text" class="form-control" placeholder="" required="required" value="<?php echo $row['reccive_amt'];?>">
               </div>
            </div>
            <div class="col-sm-12 form-group">
               <div class="col-sm-4" >
                  <span class="tele_span">Total amount : </span>
                  <input type="text" name="total_amount" class="form-control" placeholder="" value="<?php echo $row['total_amount'];?>">
               </div>
               <div class="col-sm-4" >
                  <span class="tele_span">Remaining Amount : </span>
                  <input type="text" name="remaining_amt" class="form-control" placeholder="" value="<?php echo $row['remaining_amt'];?>">
               </div>
               <div class="col-sm-4" >
                  <span class="tele_span">Next Due Date : </span>
                  <input type="text" name="next_due_date" class="form-control" placeholder="" id="datepicker-disabled_cheque" value="<?php echo $row['next_due_date'];?>">
               </div>
            </div>
            <div class="col-sm-12">
               <div class="tele_heading">Product Details : </div>
               <div class="col-sm-4" >
                  <span class="tele_span">Executive Name</span>
                  <?php  $reg = mysql_query("SELECT `name`,`last_name` FROM `registration` WHERE `id`='".$row['ex_id']."'");
                     $SselReg = mysql_fetch_array($reg);?>
                  <input type="hidden" value="<?php echo $row['ex_id'];?>" name="exID">
                  <input type="text" class="form-control" name="" id="exname" placeholder="" value="<?php echo $SselReg['name'];?> <?php echo $SselReg['last_name'];?>" readonly>
                  <br>
                  <span class="tele_span">HASP ID : </span>
                  <input type="text" class="form-control" name="haspid" id="haspid" placeholder="" value="<?php echo $row['haspid'];?>">
                  <br>
                  <span class="tele_span">BB Against : </span>
                  <input type="text" class="form-control" name="bbname_0" id="bbname_0" placeholder="" value="<?php echo $row['bbname_0'];?>">
                  <br>
                  <span class="tele_span">PC DVD No : </span>
                  <input type="text" class="form-control" name="pcdvdno" id="pcdvdno" placeholder="" value="<?php echo $row['pcdvdno'];?>">
                  <br>
                  <span class="tele_span">Select Scheme : </span>
                  <br>
                  <!-- <label>DVD Year</label> -->
                  <span class="tele_span">DVD Year : </span>
                  <input type="text" class="form-control" name="year_0" id="year_0" placeholder="" value="<?php echo $row['year_0'];?>" >
               </div>
               <div class="col-sm-4" >
                  <span class="tele_span">Select Type : </span>
                  <br>
                  <br>  
                  <span class="tele_span">DVD Year : </span>
                  <input type="text" class="form-control" name="dvdno" id="dvdno" value="<?php echo $row['dvdno'];?>" placeholder="DVD No.">
                  <br>      
                  <span class="tele_span">PC BB DVD : </span>
                  <input type="text" class="form-control" name="pcbbdvd" id="pcbbdvd" placeholder="" value="<?php echo $row['pcbbdvd'];?>">
                  <br>
                  <span class="tele_span">Licence Days : </span>
                  <input type="text" class="form-control" name="licensed" id="licensed" placeholder="" value="<?php echo $row['licensed'];?>">
                  <br>
                  <span class="tele_span">Amount : </span>
                  <input type="text" class="form-control" name="amount" id="amount" placeholder="" value="<?php echo $row['amount'];?>">
               </div>
               <div class="col-sm-4">
                  <span class="tele_span">Users : </span>
                  <input type="text" class="form-control" placeholder="No of users" name="users_0" id="users_0" placeholder="" value="<?php echo $row['users_0'];?>">
                  <br>
                  <span class="tele_span">Select the mode : </span>
                  <br>
                  <span class="tele_span">BB DVD No : </span>
                  <input type="text" class="form-control" name="bbdvd_0" id="bbdvd_0"  placeholder="" value="<?php echo $row['bbdvd_0']; ?>">
                  <br>
                  <span class="tele_span">BHCR DVD No : </span>
                  <input type="text" class="form-control" name="bhcr" id="bhcr"  placeholder="" value="<?php echo $row['bhcr']; ?>">
                  <br>
                  <span class="tele_span">BHCR DVD No : </span>
                  <input type="text" class="form-control" name="freeupdation_0" id="freeupdation_0" placeholder="" value="<?php echo $row['freeupdation_0']; ?>">
               </div>
            </div>
            <div class="register-buttons">
               <input type="submit" class="btn btn-success"  value="Edit Profile of <?php echo $row['name'];?> <?php echo $row['last_name'];?>" name="order_form_edit" onclick="return confirm('Are You Sure Want to Edit Profile');">
               <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong-<?php echo $row['id'];?>" onclick="sendMAil(<?php echo $row['ex_id'];?>);">Send To Executive</button>
            </div>
            </div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
 <!--  <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a> -->
</div>
 <?php include("footer.php");?>
<div id="myModalDSR" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="info"></div>
  </div>
</div>
<div id="history" class="modal fade" role="dialog">
  <div class="modal-dialog">
        <div id="his"></div>
  </div>
</div>

<div id="reasonModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
<div id="calledfor"></div>
  </div>
</div>
<script>
  $(document).ready(function(){
    $("#called_for").on("change",function(){
      var this_val = $(this).val();
      if(this_val == "other"){
        $("#change_input").html("<input type='text' class='form-control' placeholder='Enter The reason here..'>")
      }
    })
  });
  function Status(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/dsrStatus.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#info' ).html(result);
  }
});
  }
   function details(id) {
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/details.php?flag=next",
    data: "id="+id,
    cache: false,
    success: function(data){
    $('#details' ).html(data);
  }
});
  }
  function calledfor(id) {
   var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/calledfor.php?flag=next",
    data: "id="+id,
    cache: false,
    success: function(result){
    $('#calledfor' ).html(result);
  }
});
  }
    function history(id){
    var id=+id;
    $.ajax({
    type: "POST",
    url: "ajax/dsrhistory.php",
    data: "id="+id,
    cache: false,
    success: function(result){
    $( '#his' ).html(result);
  }
});
  }
</script>
<style>
#data-tableDaily_filter{
  text-align: right;
}
</style>

 