<?php error_reporting(0); session_start(); $_SESSION['user_id'];?>
<head>
  <?php include("head.php");?>
  <script>
    $(document).ready(function(){
      $('#data-tabledaily').dataTable();
    });
    $( function() {
      $( "#datepicker" ).datepicker();
    } );
    $(document).ready(function(){
      $("#tracking").addClass("active");
    });
  </script>
</head>
<?php include_once("header.php"); ?>
<?php include_once("sidebar.php"); ?>  
<div id="content" class="content">
  <ol class="breadcrumb" style="width: 100%;">
    <li><a href="index.php">Home</a></li>
    <li> <a href="index.php">Dashboard</a></li>
    <li class="active">Organisationt</li>
  </ol>
  <div class="details">
    <h3 class="col-sm-8" style="margin-top: 0px; color: green;">
      <span>Organisation Report&nbsp;&nbsp;
      </span>
      <a href="index.php" class="btn btn-primary btn-xs">Back To Home Page
      </a>
    </h3>  
    <?php include("timezone.php");?>
    <hr style="float: left;width: 100%;">
    <?php $name = $_POST['name'];
    $telename = mysql_query("SELECT * FROM `law_registration` WHERE `id`='".$name."'");
    $telnameSel = mysql_fetch_array($telename);?>
    <form name="MyForm" method="POST">
      <div class="row">
        <div class="col-sm-3 col-xs-6 selectDate">
          <h5>
            <b style="color:green;">Select Client Name</b>
          </h5>
        </div>
        <div class="col-sm-3 col-xs-6 business_name">
          <select class="form-control" name="name" id="selUser">
            <option value="">--Select Business Partner Name--</option>
            <?php $sql = mysqli_query($db,"SELECT * FROM `law_registration` WHERE `account`='Organisation' AND `approve`=1");
            while($row = mysqli_fetch_array($sql)){?>
            <option value="<?php echo $row['reg_id'];?>"><?php echo ucfirst($row["name"]);?> <?php echo ucfirst($row["last_name"]);?>
            </option>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-2 col-xs-2 selectDate">
          <input type="submit" class="btn btn-primary btn-sm" value="Submit">
        </div>
        </form> 
      <hr>
      <div class="col-sm-12 table-responsive">
        <table id="data-tabledaily" class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th style="color: green;">Sr.No.</th>
              <th style="color: green;">Client name</th>
              <th style="color: green;">Mobile</th>
              <th style="color: green;">Email</th>
              <th style="color: green;">Address</th>
              <th style="color: green;">Gender</th>
              <th style="color: green;">Profile Image</th>
              <th style="color: green;">Registration Status</th>
            </tr>
          </thead>
          <tbody> 
            <?php
            $sqlwe=mysqli_query($db, "SELECT * FROM `law_registration` WHERE `account`='Organisation'  AND `approve`=1 ORDER BY reg_id DESC");
            $i=1;
            while($sqlweas=mysqli_fetch_array($sqlwe)){ 
            ?>
            <tr>
              <td> <?php echo $i;?></td>
              <td><?php echo $sqlweas['name'];?>  <?php echo $sqlweas['last_name'];?></td>
              <td><?php echo $sqlweas['mobile'];?></td>
              <td><?php echo $sqlweas['email'];?></td>
              <td><?php echo $sqlweas['address'];?></td>
              <td><?php echo $sqlweas['gender'];?></td>
              <td><?php echo $sqlweas['image'];?></td>
              <td><?php echo $sqlweas['basis'];?></td>
            </tr>
            <?php $i++;} mysql_close($connect);?>
          </tbody>
        </table>
      </div>
      </div>
  </div>
</div>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
  <i class="fa fa-angle-up">
  </i>
</a>
</div>
<div id="history" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div id="his">
    </div>
  </div>
</div>
<div id="callingModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
        <h5 class="modal-title">Add Status
        </h5>
      </div>
      <div class="modal-body">
        <div id="details">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close
        </button>
      </div>
    </div>
  </div>
</div>    
<div id="myModalDSR" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div id="infoDSR">
    </div>
  </div>
</div>
<script>
  function dsrStatus(id) {
    var id=+id;
    $.ajax({
      type: "POST",
      url: "ajax/addTrack.php",
      data: "id="+id,
      cache: false,
      success: function(result){
        $( '#infoDSR' ).html(result);
      }
    }
          );
  }
  function details(id){
    var id=+id;
    $.ajax({
      type: "POST",
      url: "ajax/details.php",
      data: "id="+id,
      cache: false,
      success: function(data){
        $( '#details' ).html(data);
      }
    }
          );
  }
  function history(id){
    var id=+id;
    $.ajax({
      type: "POST",
      url: "ajax/dsrdetailshistory.php",
      data: "id="+id,
      cache: false,
      success: function(result){
        $( '#his' ).html(result);
      }
    }
          );
  }
  function trnasfer(id){
    var id=+id;
    $.ajax({
      type: "POST",
      url: "ajax/transfer.php",
      data: "id="+id,
      cache: false,
      success: function(result){
        $( '#info' ).html(result);
      }
    }
          );
  }
</script>
<?php include("footer.php");?>
