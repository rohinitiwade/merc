<?php include("../includes/connection.php");?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
<link href="quotation/style.css" rel="stylesheet">
</head>
<body>
  <?php  $id = $_POST['p_id']; ?>
  <form name="" action="quotation/submit.php?id=<?php echo $_GET['id']; ?>" method="POST">
    <!-- Begin page content -->
    <div class="container">
      <div class='row'>
        <h3 class="text-center title">Quotation</h3>
      </div>
<?php $sql = mysql_query("SELECT * FROM `quotation` WHERE `client_id`='".$id."'");?>
<?php $row = mysql_fetch_array($sql);?>
      <div class='row'>
        <div class="col-md-4" style="float: left;">
          <h5>From,</h5>
          <div class="form-group" style="font-size: 12px;">
            <?php echo $row['froms'];?>
        </div>
        </div>
        <div class="col-md-4" style="float: right;">
          <h5>To,</h5>
          <div class="form-group">
            <?php echo $row['to'];?>
        </div>
        <div class="form-group">
          <h5>Address</h5>
          <?php echo $row['client_Address'];?>
        <div class="form-group">
          <label>Quotation No.</label>
          <?php echo $row['quotation_no'];?>
        </div>
        <div class="form-group">
          <?php echo $row['dates'];?>        
        </div>
        </div>
      </div>
        
        <div class='row'>
          <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
            <table class="table table-bordered table-hover table_qat">
          <thead>
            <tr>
              <th width="2%"><input id="check_all" class="formcontrol" type="checkbox"/></th>
              <th width="15%">Item No</th>
              <th width="38%">Item Name</th>
              <th width="15%">Price</th>
              <th width="15%">Quantity</th>
              <th width="15%">Total</th>
            </tr>
          </thead>
          <tbody>
            <?php $QProduct = mysql_query("SELECT * FROM `quotation_product` WHERE `quotation_id`='".$row['id']."'");
            $i=1;?>
            <?php while($selQproduct = mysql_fetch_array($QProduct)){?>
            <tr>
              <td><input class="case" type="checkbox"/></td>
              <td><input type="text" data-type="productCode" name="itemNo[]" id="itemNo_1" class="form-control autocomplete_txt" autocomplete="off" value="<?php echo $i;?>" readonly></td>
              <td><input type="text" data-type="productName" name="itemName[]" id="itemName_1" class="form-control autocomplete_txt" autocomplete="off" value="<?php echo $selQproduct['item_name'];?>" readonly></td>
              <td><input type="text" name="price[]" id="price_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $selQproduct['price'];?>" readonly></td>
              <td><input type="text" name="quantity[]" id="quantity_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $selQproduct['quantity'];?>" readonly></td>
              <td><input type="text" name="total[]" id="total_1" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $selQproduct['total'];?>" readonly></td>
            </tr>
            <?php $i++;} ?>
          </tbody>
        </table>
          </div>
        </div>
        <div class='row'>
          <div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
            <!-- <input class="btn btn-danger delete" type="button" value="- Delete" name="delete">
            <input class="btn btn-success addmore" type="button" value="+ Add More" name="add"> -->
            <h5>Notes: </h5>
            <div class="form-group">
<textarea class="form-control" rows='5' id="notes" placeholder="Your Notes" name="notes" readonly><?php echo $row['notes'];?></textarea>
        </div>
        
          </div>
          <div class='col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-5 col-md-5 col-lg-5'>
        <form class="form-inline">
          <div class="form-group">
            <label>Gross Total: &nbsp;</label>
            <div class="input-group">
              <div class="input-group-addon">Rs.</div>
              <input type="text" class="form-control" id="subTotal" name="gross_total" placeholder="Gross Total" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $row['subtotal'];?>" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>GST: &nbsp;</label>
            <div class="input-group">
              <div class="input-group-addon">Rs.</div>
              <input type="text" class="form-control" id="tax" name="gst" placeholder="Tax" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $row['gst'];?>" readonly>
            </div>
          </div>
          <div class="form-group">
            <label>Tax Amount: &nbsp;</label>
            <div class="input-group">
              <input type="text" class="form-control" id="taxAmount"  name="taxAmount" placeholder="Tax" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $row['tax_amount'];?>" readonly>
              <div class="input-group-addon">%</div>
            </div>
          </div>
          <div class="form-group total_form">
            <label>Total: &nbsp;</label>
            <div class="input-group">
              <div class="input-group-addon">Rs.</div>
              <input type="text" class="form-control" id="totalAftertax" name="total" placeholder="Total" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" value="<?php echo $row['total'];?>" readonly>
            </div>
          </div>
          <!-- <div class="form-group submit_btn_qua">
            <input type="submit" name="submit_quotation"  value="Submit" class="btn btn-success">
          </div> -->
          <!-- <div class="form-group">
            <label>Amount Paid: &nbsp;</label>
            <div class="input-group">
              <div class="input-group-addon">$</div>
              <input type="number" class="form-control" id="amountPaid" placeholder="Amount Paid" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
            </div>
          </div>
          <div class="form-group">
            <label>Amount Due: &nbsp;</label>
            <div class="input-group">
              <div class="input-group-addon">$</div>
              <input type="number" class="form-control amountDue" id="amountDue" placeholder="Amount Due" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;">
            </div>
          </div> -->
        </form>
      </div>
        </div>
        
    </div>
    <script>
      var i=$('.table_qat tr').length;
$(".addmore").on('click',function(){
  html = '<tr>';
  html += '<td><input class="case" type="checkbox"/></td>';
  html += '<td><input type="text" data-type="productCode" name="itemNo[]" id="itemNo_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
  html += '<td><input type="text" data-type="productName" name="itemName[]" id="itemName_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
  html += '<td><input type="text" name="price[]" id="price_'+i+'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
  html += '<td><input type="text" name="quantity[]" id="quantity_'+i+'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
  html += '<td><input type="text" name="total[]" id="total_'+i+'" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
  html += '</tr>';
  $('.table_qat').append(html);
  i++;
});

//to check all checkboxes
$(document).on('change','#check_all',function(){
  $('input[class=case]:checkbox').prop("checked", $(this).is(':checked'));
});

//deletes the selected table rows
$(".delete").on('click', function() {
  $('.case:checkbox:checked').parents("tr").remove();
  $('#check_all').prop("checked", false); 
  calculateTotal();
});


var prices = ["1|AIR DVD ALJ|12000","2|AIR DVD BHCR |12000","3|AIR DVD CRILJ|12000","4|AIR DVD HC|10000","5|AIR DVD HC/CRILJ |12000","6|AIR DVD HC DIR|12000","7|AIR DVD HC/PC |12000","8|AIR DVD PC|12000","9|AIR DVD PC DIR|12000","10|AIR DVD SC|15000","11| AIR DVD SC/CRILJ|8000","12|AIR DVD SC DIR|7000","13|AIR DVD SC/HC|5000","14|AIR DVD SC/HC/CRILJ|15000","15| AIR DVD SC/HC/CRILJ DIR|12000","16|AIR DVD-SC/HC/CRILJ/ PC|9000","17|AIR DVD SC/HC/CRILJ/PC-DIR|12000","18|AIR DVD SC/HC DIR|10000","19|AIR DVD SC/HC/PC|20000"];

//autocomplete script
$(document).on('focus','.autocomplete_txt',function(){
  type = $(this).data('type');
  
  if(type =='productCode' )autoTypeNo=0;
  if(type =='productName' )autoTypeNo=1;  
  
  $(this).autocomplete({
    source: function( request, response ) {  
       var array = $.map(prices, function (item) {
                 var code = item.split("|");
                 return {
                     label: code[autoTypeNo],
                     value: code[autoTypeNo],
                     data : item
                 }
             });
             //call the filter here
             response($.ui.autocomplete.filter(array, request.term));
    },
    autoFocus: true,          
    minLength: 2,
    select: function( event, ui ) {
      var names = ui.item.data.split("|");            
      id_arr = $(this).attr('id');
        id = id_arr.split("_");
      $('#itemNo_'+id[1]).val(names[0]);
      $('#itemName_'+id[1]).val(names[1]);
      $('#quantity_'+id[1]).val(1);
      $('#price_'+id[1]).val(names[2]);
      $('#total_'+id[1]).val( 1*names[2] );
      calculateTotal();
    }           
  });
});

//price change
$(document).on('change keyup blur','.changesNo',function(){
  id_arr = $(this).attr('id');
  id = id_arr.split("_");
  quantity = $('#quantity_'+id[1]).val();
  price = $('#price_'+id[1]).val();
  if( quantity!='' && price !='' ) $('#total_'+id[1]).val( (parseFloat(price)*parseFloat(quantity)).toFixed(2) ); 
  calculateTotal();
});

$(document).on('change keyup blur','#tax',function(){
  calculateTotal();
});

//total price calculation 
function calculateTotal(){
  subTotal = 0 ; total = 0; 
  $('.totalLinePrice').each(function(){
    if($(this).val() != '' )subTotal += parseFloat( $(this).val() );
  });
  $('#subTotal').val( subTotal.toFixed(2) );
  tax = $('#tax').val();
  if(tax != '' && typeof(tax) != "undefined" ){
    taxAmount = subTotal * ( parseFloat(tax) /100 );
    $('#taxAmount').val(taxAmount.toFixed(2));
    total = subTotal + taxAmount;
  }else{
    $('#taxAmount').val(0);
    total = subTotal;
  }
  $('#totalAftertax').val( total.toFixed(2) );
  calculateAmountDue();
}

$(document).on('change keyup blur','#amountPaid',function(){
  calculateAmountDue();
});

//due amount calculation
function calculateAmountDue(){
  amountPaid = $('#amountPaid').val();
  total = $('#totalAftertax').val();
  if(amountPaid != '' && typeof(amountPaid) != "undefined" ){
    amountDue = parseFloat(total) - parseFloat( amountPaid );
    $('.amountDue').val( amountDue.toFixed(2) );
  }else{
    total = parseFloat(total).toFixed(2);
    $('.amountDue').val( total);
  }
}


//It restrict the non-numbers
var specialKeys = new Array();
specialKeys.push(8,46); //Backspace
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    console.log( keyCode );
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    return ret;
}

//datepicker
$(function () {
  $.fn.datepicker.defaults.format = "dd-mm-yyyy";
    $('#invoiceDate').datepicker({
        startDate: '-3d',
        autoclose: true,
        clearBtn: true,
        todayHighlight: true
    });
});
    </script>
  </body>
</html>