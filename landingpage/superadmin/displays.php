<?php
include("../includes/connection.php");
?>
  <script>
     $(function() {
  var my_options = $('.fac1 select option');
  var selected = $('.fac1').find('select').val();

  my_options.sort(function(a,b) {
    if (a.text > b.text) return 1;
    if (a.text < b.text) return -1;
    return 0
  })

  $('.fac1').find('select').empty().append( my_options );
  $('.fac1').find('select').val(selected);
  
  // set it to multiple
  $('.fac1').find('select').attr('multiple', true);
  
  // remove all option
  $('.fac1').find('select option[value=""]').remove();
  // add multiple select checkbox feature.
  $('.fac1').find('select').multiselect();
})
   </script>
    
   <script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".payment_box").not(targetBox).hide();
        $(targetBox).show();
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  var t = 0;
var i = $('.orderform_table1 tr').length;
$(".add-Updationrow").on('click',function(){
  html = '<tr>';
  html += '<td style="vertical-align: text-bottom;"><input type="checkbox" name="record"/></td>';
  html += '<td><label class="control-label payment_new_label">Cheque No</label><input name="che_no[]" type="text" class="form-control" id="che_no_'+i+'" placeholder="Cheque No"  autocomplete="OFF"></td>';
  html += '<td style="width: 200px;"><label class="control-label payment_new_label">Bank Name</label><input name="bank_name[]" type="text" class="form-control" id="bank_name_'+i+'" placeholder="Bank Name"  autocomplete="OFF"><div id="suggesstion-bank_'+i+'"></div></td>';
  html += '<td style="width: 200px;"><label class="control-label payment_new_label">Branch Name</label><input name="branch_name[]" type="text" class="form-control" id="bank_name_'+i+'" placeholder="Branch Name"  autocomplete="OFF"><div id="suggesstion-branch'+i+'"></div></td>';
  html += '<td> <label class="control-label payment_new_label">IFSC Code</label> <input name="ifsc_code[]" type="text" class="form-control" id="ifsc_code_'+i+'" placeholder="IFSC Code"  autocomplete="OFF"><div id="suggesstion-ifc_'+i+'"></div> </td>';
  html += '<td> <label class="control-label payment_new_label">Amount Received</label> <input name="cashamt[]" type="text" class="form-control amount_recieve" id="cash_amt_'+i+'" placeholder="Amount Received" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"  autocomplete="OFF"> </td>';
  html += '<td> <label class="control-label payment_new_label">PDC Date</label> <input type="date" name="next_due_date[]" class="form-control" id="next_due_date_'+i+'" placeholder="Cheque Date" id="datepicker" autocomplete="OFF"> </td>';
  html += '<td> <div class="form-group"> <label>Cheque Image</label> <input type="file" class="form-control" id="cheque_'+i+'" name="cheque[]"></div></td>';
  html += '</tr>';
  i++;
  t++;

  $('.orderform_table1').append(html);
});
// Find and remove selected table rows
  $(".delete-Updationrow").click(function(){
    $(".orderform_table1").find('input[name="record"]').each(function(){
    if($(this).is(":checked")){
    $(this).parents("tr").remove();
    calculation();
    }
  });
});
}); 
</script>
<script>
 $(document).on("change keyup blur", ".amount_recieve3", function() {
      calculation3();
  });

  function calculation3(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount3").val();
      var getRemainingAmount = $("#subTotal3").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount3").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount3").val('');
        }
   }); 
  }    
</script>

<script>
 $(document).on("change keyup blur", ".amount_recieve4", function() {
      calculation4();
  });

  function calculation4(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount4").val();
      var getRemainingAmount = $("#subTotal4").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount4").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount4").val('');
        }
   }); 
  }    
</script>

<script>
 $(document).on("change keyup blur", ".amount_recieve5", function() {

      calculation5();
  });

  function calculation5(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount5").val();
      var getRemainingAmount = $("#subTotal5").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount5").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount5").val('');
        }
   }); 
  }   
</script>

<script>
 $(document).on("change keyup blur", ".amount_recieve6", function() {
      calculation6();
  });

  function calculation6(){      
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount6").val();
      var getRemainingAmount = $("#subTotal6").val();
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount6").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount6").val('');
        }
   }); 
  }   
</script>
<script>
 $(document).on("change keyup blur", ".amount_recieve", function() {
  calculation();
  });

  function calculation(){
   var sum = 0;
    $(".amount_recieve").each(function(){
      // debugger;
        sum += +$(this).val().replace(/,/g, '');
    });
    $("#subTotal").val(sum); 
    
    $(document).on("change keyup blur onload click", function(){ 
      var getTotal= $("#amount").val();
      var getRemainingAmount = $("#subTotal").val();
        if(getTotal !==''){
        var remianing_amount = getTotal.replace(/,/g, '') - getRemainingAmount;
        $("#remianing_amount").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount").val('');
        }
   }); 
  }
</script>
<script>
 $(document).on("change keyup blur", ".amount_recieve2", function() {
      calculation2();
  });

  function calculation2(){
         
    $(document).on("change keyup blur load click", function(){ 
      var getTotal= $("#amount2").val().replace(/,/g, '');
      var getRemainingAmount = $("#subTotal2").val().replace(/,/g, '');
        if(getTotal !==''){
        var remianing_amount = getTotal - getRemainingAmount;
        $("#remianing_amount2").val(remianing_amount);
        }
        else
        {
          $("#remianing_amount2").val('');
        }
   }); 
  }
    
    
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".payment_box").not(targetBox).hide();
        $(targetBox).show();
        if(inputValue=='payment_1'){
          $('.payment_3').val('');
          $('.payment_4').val('');
          $('.payment_5').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_3'){
          $('.payment_5').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_4'){
          $('.payment_5').val('');
          $('.payment_3').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_5'){
          $('.payment_3').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_6'){
          $('.payment_5').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_3').val('');
          $('.payment_7').val('');
        }
        else if(inputValue=='payment_7'){
          $('.payment_5').val('');
          $('.payment_4').val('');
          $('.payment_1').val('');
          $('.payment_6').val('');
          $('.payment_3').val('');
        }
    });
});
</script>
<?php 
if(!empty($_POST["ids"])) {
       $query = "SELECT * FROM `order_form` WHERE `id`='". $_POST["ids"] ."' ";
$result = mysql_query($query);
if(mysql_num_rows($result)!="") {
  $fetch= mysql_fetch_array($result);?>
                        <div id="update_clients">
                          <div class="col-sm-9 info_client">
                          <div class="new_head_title">Basic Information</div>
                          <div class="row row-space-10">
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label">First Name <span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="name" id="name" value="<?php echo $fetch['name'];?>" onkeyup="cust_name()"  autocomplete="OFF"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_fname"></div>
                            </div>
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label">Last Name <span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="last_name" value="<?php echo $fetch['last_name'];?>" id="lname" onkeyup="cust_name()">
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_lname"></div>
                            </div>
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label">Email<span class="text-danger"></span></label>
                              <input type="text" class="form-control email_check"  id="email" name="email" value="<?php echo $fetch['email'];?>" >
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_email"></div>
                            </div>
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label">Mobile & Phone<span class="text-danger"></span></label>
                              <input type="text" class="form-control email_check"  id="email" name="mobile" value="<?php echo $fetch['mobile'];?>, <?php echo $fetch['rphone'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_email"></div>
                            </div>
                            <div class="col-sm-4 col-xs-6 m-b-15" style="height: 25px;">
                              <label class="control-label">Address<span class="text-danger"></span></label>
                              <textarea class="form-control" rows="2" id="add" name="address" ><?php echo $fetch['address'];?></textarea>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_add"></div>
                            </div>
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label">Pin Code<span class="text-danger"></span></label>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="pincode" value="<?php echo $fetch['pincode'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_pin"></div>
                            </div>
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label">State<span class="text-danger"></span></label>
                              <?php $state = mysql_query("SELECT * FROM `states` WHERE `state_id`='".$fetch['state_name']."'");
                              $selState = mysql_fetch_array($state);?>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="state_name" value="<?php echo $selState['state_name'];?>" />
                            </div>
                          <!-- <div class="row row-space-10"> -->
                            <!-- <div class="col-md-4 m-b-15">
                              <label class="control-label">Mobile No. <span class="text-danger"></span></label>
                              <input type="text" class="form-control"  id="mobile" onkeypress="return isNumber(event)" maxlength="10" name="mobile" value="<?php //echo $fetch['mobile'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_mobile"></div>
                            </div> -->
                            <!-- <div class="col-md-4 m-b-15">
                              <label class="control-label">Phone No. <span class="text-danger"></span></label>
                              <input type="text" class="form-control"  id="pin" maxlength="10"  name="rphone" value="<?php echo $fetch['rphone'];?>"/>
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_phone(r)"></div>
                            </div> -->
                          <!-- </div> -->
                          <!-- <div class="row row-space-10"> -->
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label"> District<span class="text-danger"></span></label>
                              <?php $districts = mysql_query("SELECT * FROM `cities` WHERE `city_id`='".$fetch['city']."'");
                              $seldistrict = mysql_fetch_array($districts);?>
                              <input type="text" class="form-control" id="pin" maxlength="6" name="state_name" value="<?php echo $seldistrict['city_name'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_city"></div>
                            </div>
                            <div class="col-sm-4 col-xs-6 m-b-15">
                              <label class="control-label"> City/Town<span class="text-danger"></span></label>
                             <input type="text" class="form-control" id="pin" maxlength="6" name="city" value="<?php echo $fetch['district'];?>" />
                              <div style="color:#F00; font-size:12px; text-align:left" id="error_city"></div>
                            </div>
                          <!-- </div> -->
                          </div>
                           </div>
                           <div class="col-sm-3  info_client">
                               <fieldset class="for-panel">
                              <div class="col-sm-">
                                <div class="row row-space-10">
                                   <div class="new_head_title" style="color: red;"><b>Payment Details</b></div>
                                  <!--  <div class="col-md-12 m-b-15">
                                      <label class="control-label" style="color: green;">Total Amount</label>
                                      <input type="text" class="form-control" name="" value="<?php echo $fetch['total_amount'];?>" id="" autocomplete="OFF" readonly>
                                    </div> -->
                     <!--  <?php  $outstanding = mysql_query("SELECT * FROM `order_form_payment_mode` WHERE `client_id`='".$fetch['id']."'  ORDER BY id DESC");?>
                                    <?php $SelOutst = mysql_fetch_array($outstanding); ?>
                                    <div class="col-md-12 m-b-15">
                                      <label class="control-label" style="color: red !important;">Outstanding Amount</label>
                                      <input type="text" class="form-control" name="" value="<?php echo $SelOutst['remaining_amount'];?>" id="" autocomplete="OFF" readonly>
                                    </div> -->
                                    <div class="pur_b">
                                <?php  $purTotal = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$fetch['id']."' AND `payment_status`=''");
                                 $SelPur = mysql_fetch_array($purTotal);
                                  $uppayment = mysql_query("SELECT SUM(total_amount) FROM `updation_amount` WHERE `client_id`='".$fetch['id']."'");
                              $ressel = mysql_fetch_array($uppayment);
                               $totalCal = $fetch['total_amount']+$ressel['SUM(total_amount)'];
                                $Updation = mysql_query("SELECT SUM(reccived_amt) FROM `order_form_payment_mode` WHERE `client_id`='".$fetch['id']."' AND `payment_status`='Updation'");
                                 $UpdationSel = mysql_fetch_array($Updation);
                                $tataoUp = $SelPur['SUM(reccived_amt)']+$UpdationSel['SUM(reccived_amt)'];
                                ?>
                                <div class="form-group">
                                  <label><b>Total Amount :</b>&nbsp;</label>
                                  <span><?php echo number_format($fetch['total_amount'],2); ?> + <b style="color: #f44271;"><?php echo number_format($ressel['SUM(total_amount)'],2);?></b> = <?php echo number_format($totalCal, 2);?></span>
                                </div>
                                <div class="form-group">
                                  <label><b>Received Amount :</b>&nbsp;</label>
                                  <span><?php echo number_format($SelPur['SUM(reccived_amt)'],2); ?> + <b style="color: #f44271;"><?php echo number_format($UpdationSel['SUM(reccived_amt)'],2); ?> </b> = <?php echo number_format($tataoUp, 2);?><span>
                                </div>
                                <div class="form-group">
                                  <label><b style="color: red;">Outstanding Amount :</b>&nbsp;</label>
                                  <span><?php echo number_format($totalCal-$tataoUp, 2);?></span>
                                </div>
                              </div>
                                </div>
                              </div>
                            </fieldset>
                          </div>
                            </div>

                          <div class="row row-space-10">
                            <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">Attach Order Form<span class="text-danger"></span></label>
                              <input type="file" name="orderform" value="<?php //echo $res['attach_Ordeform'];?>">
                                 <a href="file/orderform/<?php //echo $res['attach_Ordeform'];?>" class="highslide" onclick="return hs.expand(this)">
                                  <img src="file/orderform/<?php //echo $res['attach_Ordeform'];?>" width="120">
                                </a>
                              <input type="hidden" value="<?php //echo $res['attach_Ordeform'];?>" name="orderform">
                            </div> -->
                            <!-- <div class="col-md-3 m-b-15">
                              <label class="control-label">Attach Cheque Image<span class="text-danger"></span></label>
                              <input type="file" name="cheque" class="cheque" value="<?php //echo $res['attach_cheque'];?>">
                              <a href="file/cheque/<?php //echo $res['attach_cheque'];?>" class="highslide" onclick="return hs.expand(this)">
                                    <img src="file/cheque/<?php //echo $res['attach_cheque'];?>" width="120">
                                  </a>
                              <input type="hidden" value="<?php //echo $res['attach_cheque'];?>" name="cheque">
                            </div> -->
                          </div>
                          <div class="new_head_title"><b style="color: green;">Client History</b></div>
                          <hr>
                          <div class="row row-space-10">
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                <label class="control-label">Business Associate Name<span class="text-danger"></span></label>
                                <?php 
                                   $sqlss= mysql_query("select *, AES_DECRYPT(name,'$key'),AES_DECRYPT(last_name,'$key') from `registration` where `id`='".$fetch['ex_id']."'");
                                              $fetchs = mysql_fetch_array($sqlss);?>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $fetchs["AES_DECRYPT(last_name,'$key')"]; ?> <?php echo $fetchs["AES_DECRYPT(name,'$key')"]; ?>" readonly autocomplete="OFF">
                                

                              </div>
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Client From<span class="text-danger"></span></label>
                                  <input type="text" class="form-control" name="exname" id="exname" value="<?php echo date("d-m-Y",strtotime($fetch['date_time']));?>" readonly autocomplete="OFF">
                              </div>
                              <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">HASP ID<span class="text-danger"></span></label>
                                  <input type="text" class="form-control" name="exname" id="haspid" value="<?php echo $fetch['haspid']; ?>" readonly autocomplete="OFF">
                              </div>
                              <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">DVD No.<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="dvdno" id="dvdno" value="<?php echo $fetch['dvdno'];?>" autocomplete="OFF">
                            </div>
                            <!--  <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">BB DVD No.<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="bbdvd_0" id="bbdvd_0" value="<?php echo $fetch['bbdvd_0'];?>" autocomplete="OFF">
                            </div> -->
                          <!-- </div> -->
                          <!-- <div class="row row-space-10"> -->
                             <!-- <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">PC BB DVD<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="pcbbdvd" id="pcbbdvd" value="<?php echo $fetch['pcbbdvd'];?>" autocomplete="OFF">
                            </div> -->
                             <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">DVD Year<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="year_0" id="year_0" value="<?php echo $fetch['year_0'];?>" autocomplete="OFF">
                            </div>
                            <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label">Licence Days<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="licensed" id="licensed" value="<?php echo $fetch['licensed'];?>" autocomplete="OFF">
                            </div>
                           <!--  <div class="col-sm-3 col-xs-6 m-b-15">
                              <label class="control-label"> DVD No<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="bhcr" id="bhcr" value="<?php echo $fetch['bhcr'];?>" autocomplete="OFF">
                            </div> -->
                            <div class="col-sm-3 col-xs-6 m-b-15 displayDropdown">
                              <label class="control-label">Select Product Name<span class="text-danger"></span></label>
                               <!--  <input type="text" class="form-control" name="bhcr" id="bhcr" value="<?php echo $fetch['product_0'];?>" autocomplete="OFF"> -->
                                <div class='facilities fac1'><span class='input'>
                                <select name="product_0[]" >
                                  <option value="<?php echo $fetch['product_0'];?>" onclick="alert(\'Please select product!\');"><?php if($fetch['product_0']!=""){echo $fetch['product_0'];}else{?>----Select Product----<?php } ?></option>
                                   <option value="Lesearch Comprehensive (LE-COMP)">Lesearch Comprehensive</option>
                                   <option value="Lesearch Individual (SC)">Lesearch Individual (SC)</option>
                                   <option value="Lesearch Individual (HC)">Lesearch Individual (HC)</option>
                                   <option value="Lesearch Individual (CRILJ)">Lesearch Individual (CRILJ)</option>
                                   <option value="BHCR (Bombay High Court)">BHCR (Bombay High Court)</option>
                                   <option value="Allahabad High Court (ALJ)">Allahabad High Court (ALJ)</option>
                                   <option value="Privi Council (PC)">Privi Council (PC)</option>
                                   <option value="Case Law Navigator (CASE-LAW)">Case Law Navigator (CASE-LAW)</option>
                                </select>
                              </span>
                              </div>
                               
                            </div>
                          <!-- </div> -->
                        </div>
                          <div class="new_head_title"><b style="color: green;">Payment History</b></div>
                          <hr>
                          <?php  $ORDERPAYment = mysql_query("SELECT * FROM `order_form_payment_mode` WHERE `client_id`='".$fetch['id']."'");?>
                          <?php while($selectQuery = mysql_fetch_array($ORDERPAYment)){?>
                          <div class="row row-space-10">
                            <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Payment Status<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="exname" id="exname" value="<?php if($selectQuery['payment_status']!=''){echo $selectQuery['payment_status'];}else{echo "New";} ?>" readonly autocomplete="OFF">
                           </div>
                          <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Payment Mode<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['payment_mode']; ?>" readonly autocomplete="OFF">
                           </div>
                              <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Cheque No.<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['cheque_no']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Bank Name<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['bank_name']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Reccived Amount<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['reccived_amt']; ?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Post Dated Cheque<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php if($selectQuery['due_date']!=''){echo date("d-m-Y",strtotime($selectQuery['due_date']));}else{echo "";}?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Status<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo $selectQuery['cheque_status'];?>" readonly autocomplete="OFF">
                           </div>
                           <div class="col-sm-2 col-xs-6 m-b-15">
                                <label class="control-label">Date Time<span class="text-danger"></span></label>
                                <input type="text" class="form-control" name="exname" id="exname" value="<?php echo date("d-m-Y",strtotime($selectQuery['date_time']));?>" readonly autocomplete="OFF">
                           </div>
                          </div>

                          <?php } ?>
                          <form name="myform" method="POST" action="submit_data.php" enctype="multipart/form-data">
                          <input type="hidden" name="exnameId" value="<?php echo $fetch['ex_id'];?>">
                          <div class="new_head_title" style="color: green;"><b> Add Payment</b></div>
                           <hr>
                              <div class="row row-space-10w">
                                <div class="col-sm-12 m-b-15">
                                  <div class="payment_div">
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_1" checked="checked">By Cheque</label>
                                    <!-- <label class="payment_label"><input type="radio" name="colorRadio" value="payment_2">By DD</label> -->
                                  <!--   <label class="payment_label"><input type="radio" name="colorRadio" value="payment_neft">By NEFT</label> -->
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_3">By NEFT</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_4">By RTGS</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_5">Bank Deposit</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_6">Paytm</label>
                                    <label class="payment_label"><input type="radio" name="colorRadio" value="payment_7">Bhim App</label>
                                  </div>
                                  <div class="payment_1 payment_box">
                                      
                                    <table class="table orderform_table1">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><input type="checkbox" name="record"></td>
                                          <td> 
                                            <label class="control-label payment_new_label">Cheque No</label> 
                                            <input name="che_no[]" type="text" class="form-control payment_1" id="che_no_1" placeholder="Cheque No" autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;" class="displayDropdown"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                          <input type="text" class="form-control payment_1" id="bank_name" name="bank_name[]" value="<?php echo $_POST['bank_name'];?>" autocomplete="OFF" placeholder="Enter Bank Name" />
                                          <div id="suggesstion-bank"></div>
                                           <!--  <input name="bank_name[]" type="text" class="form-control" id="bank_name_1" placeholder=""  autocomplete="OFF"> -->
                                          </td>
                                          <td style="width: 200px;" class="displayDropdown"> 
                                         <label class="control-label payment_new_label">Branch Name</label>  
                                          <input type="text" class="form-control payment_1"  id="branch_name" name="branch_name[]" value="<?php echo $_POST['branch_name'];?>" autocomplete="OFF" placeholder="Enter Branch name"/>
                                          <div id="suggesstion-branch"></div>
                                           <!--  <input name="bank_name[]" type="text" class="form-control" id="bank_name_1" placeholder=""  autocomplete="OFF"> -->
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control payment_1" id="ifsc_code" placeholder="IFSC Code" autocomplete="OFF">
                                             <div id="suggesstion-ifc"></div>
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cashamt[]" type="text" class="form-control amount_recieve payment_1" id="cash_amt_1" placeholder="Amount Received" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" 
                                             autocomplete="OFF">
                                          </td>
                                        
                                          <td> 
                                            <label class="control-label payment_new_label">Cheque Date</label> 
                                            <input type="date" name="next_due_date[]" class="form-control payment_1" id="next_due_date_1" placeholder="Cheque Date" id="datepicker" autocomplete="OFF">
                                          </td>
                                          <td> 
                                           <div class='form-group'> <label>Cheque Image</label> <input type='file'  class='form-control' id="cheque_1" name="cheque[]"></div>
                                          </td>

                                        </tr>
                                      </tbody>
                                    </table>

                                        <input type="button" class="add-Updationrow btn btn-primary btn-xs" value="Add Cheque">
                                        <input type="button" class="delete-Updationrow btn  btn-primary btn-xs" value="Delete Cheque"> 

                                     <div class="col-sm-12 by_cheque">
                                      <div class="col-sm-3 m-b-15">
                              <label class="control-label">Total Amount</label>
                              <input type="text" class="form-control payment_1" name="total_amount" id="amount" value="<?php echo $_POST['total_amount'];?>" onkeypress="return IsNumeric(event);" placeholder="Total Amount" ondrop="return false;" onpaste="return false;" autocomplete="OFF" >
                          </div>
                          <div class="col-sm-3 m-b-15">
                              <label class="control-label">Total Amount Received</label>
                              <input type="text" class="form-control payment_1" id="subTotal" name="cash_amtc" placeholder=" Total Received Amount" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF" >
                          </div>
                           <div class="col-sm-3 m-b-15">
                              <label class="control-label">Remaining Amount</label>
                              <input type="text" class="form-control payment_1" name="remianing_amount" id="remianing_amount" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                          </div>
                         <!--  <div class="register-buttons">
                              <input type="submit" class="btn btn-primary" id="submit_dsr" value="Submit" name="order_form">
                          </div> -->
                        </div>
                                  </div>
   
                                  <div class="payment_2 payment_box">
                                    <table class="table">
                                      <tbody>
                                        <tr>
                                          <td style="vertical-align: text-bottom;"><!-- <input type="checkbox" name="record"> --></td>
                                          <td> 
                                            <label class="control-label payment_new_label">DD No</label> 
                                            <input name="che_no[]" type="text" class="form-control" onkeypress="return IsNumeric(event);" placeholder="DD No"  autocomplete="OFF">
                                          </td>
                                          <td style="width: 200px;"> 
                                          <label class="control-label payment_new_label">Bank Name</label>  
                                            <input name="bank_name[]" type="text" class="form-control" placeholder="Bank Name" >
                                          </td>
                                           <td>
                                           <label class="control-label payment_new_label">IFSC Code</label>   
                                            <input name="ifsc_code[]" type="text" class="form-control" placeholder="IFSC Code"  autocomplete="OFF">
                                          </td>
                                          <td>
                                            <label class="control-label payment_new_label">Amount Received</label> 
                                            <input name="cashamt[]" type="text" class="form-control" placeholder="Amount Received"  autocomplete="OFF">
                                          </td>
                                          <!-- <td>
                                            <label class="control-label payment_new_label">Total amount</label> 
                                            <input type="text" name="total_amount[]" class="form-control" autocomplete="OFF" placeholder="" >
                                          </td> -->
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="payment_3 payment_box">
                                   <div class="col-sm-3 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve2 payment_3" name="reccived_amountNEFT" id="amount2" value="<?php echo $_POST['amount'];?>" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 m-b-15">
                                    <label class="control-label"> Amount Received</label>
                                    <input type="text" class="form-control payment_3" id="subTotal2" name="reccived_amountNEFTrec" placeholder="  Amount Received" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_3" name="remianing_amountNEFT" id="remianing_amount2" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                              <div class="col-sm-3 m-b-15">
                                  <label class="control-label">Reference No. / UTR No.</label>
                                  <input type="text" class="form-control payment_3" name="utr_noneft" id="utr_no" value="" placeholder="Reference No. / UTR No." autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                  </div>  

                                <div class="payment_4 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve3 payment_4" name="totalRTGS" id="amount3" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_4" id="subTotal3" name="cash_amtRTGS" placeholder="Total Amount Received" onkeypress="return IsNumeric(event);" value="" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_4" name="remianing_amountRTGS" id="remianing_amount3" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>UTR No.</label>
                                      <input type="text" class="form-control payment_4" id="utr_no" name="utr_noRTGS" placeholder="UTR No." autocomplete="OFF">
                                    </div>
                                   <!--  <div class="register-buttons">
                                         <input type="submit" class="btn btn-primary" id="submit_dsr" value="Submit" name="order_form">
                                    </div> -->
                                  </div>

                                     <div class="payment_5 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve4 payment_5" name="recAmountBankAccount" id="amount4" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_5" id="subTotal4" name="cash_amtBankAccount" placeholder="Total Amount Received" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_5" name="remianing_amountBankAccount" id="remianing_amount4" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                  <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>Deposite Receipt</label>
                                      <input type="file" class="form-control payment_5" name="deposite_receiptBankAccount" id="deposite_receipt" value="" autocomplete="OFF">
                                      
                                    </div>
                          <!-- <div class="register-buttons">
                              <input type="submit" class="btn btn-primary" id="submit_dsr" value="Submit" name="order_form">
                          </div> -->
                                  </div>

                                  <div class="payment_6 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve5 payment_6" name="recAmount" id="amount5" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_6" id="subTotal5" name="cash_amt" placeholder="Total Amount Received" onkeypress="return IsNumeric(event);" value="" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div> 
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_6" name="remianing_amount5" id="remianing_amount5" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>Order No.</label>
                                      <input type="text" class="form-control payment_6" id="utr_no" name="utr_no" placeholder="UTR No." autocomplete="OFF">
                                    </div>
                                   <!--  <div class="register-buttons">
                                        <input type="submit" class="btn btn-success" id="submit_dsr" value="Submit" name="order_form">
                                    </div> -->
                                  </div>

                                  <div class="payment_7 payment_box">
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount</label>
                                    <input type="text" class="form-control amount_recieve6 payment_7" name="recAmountb" id="amount6" value="" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" placeholder="Total Amount" autocomplete="OFF" >
                                </div>
                                <div class="col-sm-3 col-xs-6 m-b-15">
                                    <label class="control-label">Total Amount Received</label>
                                    <input type="text" class="form-control rec_require payment_7" id="subTotal6" name="cash_amtb" placeholder="Total Amount Received" onkeypress="return IsNumeric(event);" value="" ondrop="return false;" onpaste="return false;" autocomplete="OFF">
                                </div>
                               <div class="col-sm-3 col-xs-6 m-b-15">
                                  <label class="control-label">Remaining Amount</label>
                                  <input type="text" class="form-control payment_7" name="remianing_amount7" id="remianing_amount6" value="" placeholder="Remaining Amount" autocomplete="OFF" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;" >
                              </div>
                                    <div class="col-sm-3 col-xs-6 m-b-15">
                                      <label>Order No.</label>
                                      <input type="text" class="form-control payment_7" id="utr_no" name="utr_nobhim" placeholder="Order No." autocomplete="OFF">
                                    </div>
                                     

                                  </div>
                                   <!-- <h4 class="show_note">Note : Haspid, Sales by and Client status fields are required</h4> -->
                                </div>
                                <hr>
                              </div>
                          <div class="row row-space-10 status">
                            <div class="col-sm-4 col-xs-6 m-b-15">
                            <label class="control-label">Enter Client Status Here<span class="text-danger">*</span></label>
                          <textarea class="form-control" name="resolve" required></textarea>
                          </div>
                        </div>
                          <div class="register-buttons">
                          &nbsp;&nbsp;&nbsp;
                            <input type="hidden" value="<?php echo $fetch['id'];?>" name="fetchID">
                          <input type="submit" class="btn btn-primary" value="Submit" name="payment_updation">
                          </div>
                             </form>

                      <?php }else{
                        ?>
                        <h4><b style="color: red;">Sorrry Data Not Found!</b></h4>
                      <?php } } ?>
              <style>fieldset.for-panel{border:1px solid #bce8f1;border-radius:4px;padding:15px 10px;background-color:#f9fdfd;margin-bottom:12px}fieldset.for-panel legend{border:1px solid #ddd;border-radius:5px;color:#4381ba;font-size:14px;font-weight:700;line-height:10px;margin:inherit;padding:7px;width:auto;background-color:#d9edf7;margin-bottom:0}.cheque_image_view{margin:10px;float:left;width:30%}.cheque_image_view img{width:100%}.info_client{margin-top:20px}

#bank_name{
  padding: 4px;
  /*border: #F0F0F0 1px solid;*/
}
/*.by_cheque{
  padding: 2% 0;
}*/
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;s
    background: white;
    cursor: inherit;
    display: block;
}
#bank-lists{
  position: absolute;
width: 17%;
float: left;
list-style: none;
margin: 0;
padding: 0;
z-index: 1;
}
#country-lists{
  float: left;
list-style: none;
margin: 0;  
padding: 0;
width: 40%;
z-index: 1;
position: relative;
height: 310px;
overflow: auto;
}
#country-lists li,#bank-lists li{
  padding: 10px; 
  cursor: pointer;
  background: #f0f0f0;
border-bottom: #bbb9b9 1px solid;
}
#country-lists li:hover{
  background:#F0F0F0;
}
#town{
  padding: 4px;
  /*border: #F0F0F0 1px solid;*/
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.pur_b{border:1px solid #ddd;padding:10px}
#img-upload{
    width: 100%;
}
 
     .btn-bs-file{
        position:relative;
   }
   .btn-bs-file input[type="file"]{
      position: absolute;
      top: -9999999;
      filter: alpha(opacity=0);
      opacity: 0;
      width:0;
      height:0;
      outline: none;
      cursor: inherit;
   }
  .client_list{
    margin-left: 5%;
  }
</style>

 
<script>
function trnasferTO(id)
  {
    var id=+id;
    //alert(id);
    $.ajax({
    type: "POST",
    url: "ajax/transfer.php",
    data: "id="+id,
    cache: false,
    success: function(result){
      //alert(result);
    $( '#infoorder' ).html(result);
  }
});
    //alert("OK");
  }
  $(document).ready(function(){
  $("#bank_name").keyup(function(){
    //alert("OK");
    $.ajax({
    type: "POST",
    url: "banknameSearch.php",
    data:'keyword='+$(this).val(),
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
     // alert(data);
      $("#suggesstion-bank").show();
      $("#suggesstion-bank").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbank(val) {
$("#bank_name").val(val);
$("#suggesstion-bank").hide();
}

$(document).ready(function(){
  $("#branch_name").keyup(function(){
    //alert("OK");
    var bank = $("#bank_name").val();
    //alert(bank);
    $.ajax({
    type: "POST",
    url: "branchSearch.php",
    data:'keyword='+$(this).val() +'&bank='+bank,
    beforeSend: function(){
      $("#bank_name").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
     //alert(data);
      $("#suggesstion-branch").show();
      $("#suggesstion-branch").html(data);
      $("#bank_name").css("background","#FFF");
    }
    });
  });
});
function selectbranch(val) { 
  var branch = val;
  var bank = $("#bank_name").val();
    $.ajax({
            type:'POST',
            url:'ifscSearch.php',
            data:'branchname='+branch + '&bank='+bank,
            success:function(data){
                  /*alert(data);*/
                  $("#ifsc_code").val(data);           
                }
            }); 

  //alert(branch);
  $("#branch_name").val(val);
  $("#suggesstion-branch").hide();


}
</script>

<div id="transfer" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
            <div id="infoorder"></div>


  </div>
</div>
 