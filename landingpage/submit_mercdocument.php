<?php
session_start();
include("includes/dbconnect.php");
include("phpfile/sql_home.php");
header('Access-Control-Allow-Origin: *');
$params = $_POST['courtdetail'];
$data   = json_decode($params, true);
$get    = array();
if ($_FILES["file"]["name"] != 'undefined' && $_FILES["file"]["name"] != '') {
    // echo "fcgh";
    $target_dir    = "upload_document/";
    $target_file   = $target_dir . basename($_FILES["file"]["name"]);
    $uploadOk      = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    $target_file1  = $target_dir . basename($_FILES["file"]["name"]);
    $imagetarget   = $target_dir . basename(time($_FILES["file"]["name"]));
    $upload        = basename(time($_FILES["file"]["name"]) . "." . $imageFileType);
    $imagetarget1  = $imagetarget . "." . $imageFileType;
    $ext           = pathinfo($_FILES["file"]["name"], PATHINFO_FILENAME);
    $file          = $_FILES["file"]["name"];
    $image_name    = basename(time($_FILES["file"]["name"]) . "_" . $file);
    $target        = $target_dir . $image_name;
    $filesize      = filesize($file); // bytes
    $size          = round($filesize / 1024 / 1024, 1);
    if ($_FILES["file"]["size"] > 5000000) {
        // echo "Sorry, your file is too large.";
        $get      = array(
            'status' => 'Sorry, your file is too large.'
        );
        $uploadOk = 0;
    }
    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf" && $imageFileType != "docx" && $imageFileType != "doc") {
        // echo "Sorry, only JPG, JPEG, PNG & GIF Pdf files are allowed.";
        $get      = array(
           'status' => 'Sorry, only JPG, JPEG, PNG, DOC, DOCX & Pdf files are allowed.'
        );
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        // echo "Sorry, your file was not uploaded.";
        $get = array(
            'status' => 'Sorry, your file was not uploaded.'
        );
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["file"]["tmp_name"], $target)) {
            // echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
              $insert  =mysqli_query($connection,  "INSERT INTO `add_document` (`size`,`image`,`date_time`,`case_no`,`user_id`,`case_id`,`type`,`image_name`,`link_document`,`organisation`)VALUES('" . $_FILES["file"]["size"] . "','" . $image_name . "','" . date("Y-m-d H:i:s") . "','" . $_POST['case_no'] . "','" . $_SESSION['user_id'] . "','" . $_POST['case_id'] . "','" . $_POST['doc_type'] . "','".$_FILES["file"]["name"]."','Yes','merc')");
            $doclast = mysqli_insert_id($connection);
            
            // $activity = mysqli_query($connection, "INSERT INTO `activity_log`(`user_id`, `case_id`, `CNR`, `case_no`,`date_time`,`image`,`activity_log_type`,`doc_id`) VALUES ('" . $_SESSION['user_id'] . "','" . $_POST['case_id'] . "','" . $cnr . "','" . $_POST['case_no'] . "','" . date("Y-m-d H:i:s") . "','" . $image_name . "','a','" . $doclast . "')");
            
            $document = mysqli_query($connection, "SELECT `user_id`,`image`,`image_name`,`doc_id`,`type`,`size`,`date_time`,`case_no`,`case_id` FROM `add_document` WHERE ".$adddoc." AND `case_id`='" . $_POST['case_id'] . "' ");
            while ($fetch_document = mysqli_fetch_array($document)) {
                // $fetch_doc[] = $fetch_document;
                $create_by                  = mysqli_query($connection, "SELECT `name`,`last_name` FROM `law_registration` WHERE `reg_id`='" . $fetch_document['user_id'] . "'");
                $doc_count                  = mysqli_num_rows($document);
                $create_by_fetch            = mysqli_fetch_array($create_by);
                $full_name                  = $create_by_fetch['name'] . ' ' . $create_by_fetch['last_name'];
                $doc_path                   = $fetch_document['image'];
                $doc_data_object            = new stdClass();
                $doc_data_object->user_id   = $fetch_document['user_id'];
                $doc_data_object->doc_id    = $fetch_document['doc_id'];
                $doc_data_object->type      = $fetch_document['type'];
                $doc_data_object->size      = $fetch_document['size'];
                $doc_data_object->doc_path  = $doc_path;
                $doc_data_object->date_time = date('F d, Y',strtotime($fetch_document['date_time']));
                $doc_data_object->case_no   = $fetch_document['case_no'];
                $doc_data_object->case_id   = $fetch_document['case_id'];
                
                $doc_data_object->uploaded_by       = $full_name;
                $doc_data_object->file_name         = $fetch_document['image_name'];
                $doc_data_object->encrypted_case_id = base64_encode($fetch_document['doc_id']);
                $doc_data_object->doc_count         = $doc_count;
                $docArr[]                           = $doc_data_object;
                
                
            }
            $get = array(
                'status' => 'success',
                'data' => $docArr
            );
            
        } else {
            // echo "Sorry, there was an error uploading your file.";
            $get = array(
                'status' => 'Sorry, there was an error uploading your file.'
            );
        }
    }
} elseif($_POST['type'] == 'm' && $_POST['case_id']!='' && $_POST['case_id']!='undefined') {
    $document  = mysqli_query($connection, "SELECT `user_id`,`image`,`image_name`,`doc_id`,`type`,`size`,`date_time`,`case_no`,`case_id` FROM `add_document` WHERE ".$adddoc." AND `case_id`='" . $_POST['case_id'] . "'");
    $doc_count = mysqli_num_rows($document);
    while ($fetch_document = mysqli_fetch_array($document)) {
        // $fetch_doc[] = $fetch_document;
        $create_by                  = mysqli_query($connection, "SELECT `name`,`last_name` FROM `law_registration` WHERE `reg_id`='" . $fetch_document['user_id'] . "'");
        $create_by_fetch            = mysqli_fetch_array($create_by);
        $full_name                  = $create_by_fetch['name'] . ' ' . $create_by_fetch['last_name'];
        $doc_path                   = $fetch_document['image'];
        $doc_data_object            = new stdClass();
        $doc_data_object->user_id   = $fetch_document['user_id'];
        $doc_data_object->doc_id    = $fetch_document['doc_id'];
        $doc_data_object->type      = $fetch_document['type'];
        $doc_data_object->size      = $fetch_document['size'];
        $doc_data_object->doc_path  = $doc_path;
        $doc_data_object->date_time = date('F d, Y',strtotime($fetch_document['date_time']));
        $doc_data_object->case_no   = $fetch_document['case_no'];
        $doc_data_object->case_id   = $fetch_document['case_id'];
        
        $doc_data_object->uploaded_by       = $full_name;
        $doc_data_object->file_name         = $fetch_document['image_name'];
        $doc_data_object->encrypted_case_id = base64_encode($fetch_document['doc_id']);
        $doc_data_object->doc_count         = $doc_count;
        $docArr[]                           = $doc_data_object;
        // array_push($get,array('user_id' => $fetch_document['user_id'],'doc_id' => $fetch_document['doc_id'],'type' => $fetch_document['type'],'size' => $fetch_document['size'],'doc_path' => $doc_path,'date_time' => $fetch_document['date_time'],'type' => $fetch_document['type'],'case_no' => $fetch_document['case_no'],'case_id' => $fetch_document['case_id'],'uploaded_by' => $full_name,'file_name' => $fetch_document['image'],'encrypted_case_id' => base64_encode($fetch_document['doc_id']),'doc_count' => $doc_count));
        // }
        // $get = array('document' => $fetch_doc);
    }
    $get = array(
        'status' => 'success',
        'data' => $docArr
    );
}

elseif($_POST['type'] == 'm' && $_POST['case_id']=='' || $_POST['case_id']=='undefined') {
    $document  = mysqli_query($connection, "SELECT `user_id`,`image`,`image_name`,`doc_id`,`type`,`size`,`date_time`,`case_no`,`case_id` FROM `add_document` WHERE ".$adddoc." ORDER BY `doc_id` DESC");
    $doc_count = mysqli_num_rows($document);
    while ($fetch_document = mysqli_fetch_array($document)) {
        // $fetch_doc[] = $fetch_document;
        $create_by                  = mysqli_query($connection, "SELECT `name`,`last_name` FROM `law_registration` WHERE `reg_id`='" . $fetch_document['user_id'] . "'");
        $create_by_fetch            = mysqli_fetch_array($create_by);
        $full_name                  = $create_by_fetch['name'] . ' ' . $create_by_fetch['last_name'];
        $doc_path                   = $fetch_document['image'];
        $doc_data_object            = new stdClass();
        $doc_data_object->user_id   = $fetch_document['user_id'];
        $doc_data_object->doc_id    = $fetch_document['doc_id'];
        $doc_data_object->type      = $fetch_document['type'];
        $doc_data_object->size      = $fetch_document['size'];
        $doc_data_object->doc_path  = $doc_path;
        $doc_data_object->date_time = date('F d, Y',strtotime($fetch_document['date_time']));
        $doc_data_object->case_no   = $fetch_document['case_no'];
        $doc_data_object->case_id   = $fetch_document['case_id'];
        
        $doc_data_object->uploaded_by       = $full_name;
        $doc_data_object->file_name         = $fetch_document['image_name'];
        $doc_data_object->encrypted_case_id = base64_encode($fetch_document['doc_id']);
        $doc_data_object->doc_count         = $doc_count;
        $docArr[]                           = $doc_data_object;
        // array_push($get,array('user_id' => $fetch_document['user_id'],'doc_id' => $fetch_document['doc_id'],'type' => $fetch_document['type'],'size' => $fetch_document['size'],'doc_path' => $doc_path,'date_time' => $fetch_document['date_time'],'type' => $fetch_document['type'],'case_no' => $fetch_document['case_no'],'case_id' => $fetch_document['case_id'],'uploaded_by' => $full_name,'file_name' => $fetch_document['image'],'encrypted_case_id' => base64_encode($fetch_document['doc_id']),'doc_count' => $doc_count));
        // }
        // $get = array('document' => $fetch_doc);
    }
    $get = array(
        'status' => 'successsss',
        'data' => $docArr
    );
}
echo json_encode($get, true);
?>
