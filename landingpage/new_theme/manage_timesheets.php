<?php include("header.php");?>
<link rel="stylesheet" href="vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<?php include("menu.php");?>
<div class="main-panel">
  	<div class="content-wrapper">
    <?php include("slider.php");?>
    	<div class="card form-group">
        <?php if(isset($_POST['submit'])){
    extract($_POST);
  }?>
      		<div class="card-body">
        		<h4 class="card-header" style="padding-top: 0px"><b style="color: green;">My Timesheets</b></h4>
        		<a href="add-timesheets.php" class="btn" style="padding: 6px;position: absolute;left: 594px;z-index: 1;background-color:#ECECEC;top: 87px;"><i class="fa fa-plus" style="position: relative;left: -3px;"></i>Add Timesheets</a>
				<button class="btn" id="myBtn" data-target="#filter" data-toggle="modal" style="position: absolute;left: 728px;z-index: 1;background-color: #ECECEC;padding: 6px 9px;top: 87px;">Filter</button>
        		<button class="btn" id="myBtn" data-target="#export" data-toggle="modal" style="position: absolute;left: 785px;z-index: 1;background-color: #ECECEC;padding: 6px 9px;top: 87px;">Export</button>
        		<div class="row" style="margin-top: 25px;">
            		<div class="col-12">
            		    <div class="table-responsive">
            		        <table id="order-listing" class="table">
            		          	<thead>
            		            	<tr>
            		                	<th style="background-color: #353eb3; color: white;width: 5px!important;">Sr.No.</th>
            		            	    <th style="background-color: #353eb3; color: white;">Case</th>
            		            	    <th style="background-color: #353eb3; color: white;">Date</th>
            		            	    <th style="background-color: #353eb3; color: white;">Particulars</th>
            		            	    <th style="background-color: #353eb3; color: white;width: 100px!important;">Time Spent (in hours)</th>
            		            	    <th style="background-color: #353eb3; color: white;">Type</th>
            		            	    <th style="background-color: #353eb3; color: white;width: 45px!important;">Actions</th>
            		            	</tr>
            		          	</thead>
            		          	<tbody>
                              <?php
                               if($_POST['submitFil']!=""){
                              $seltimesheet=mysqli_query($connection, "SELECT * FROM `add_timesheet` WHERE `case`='".$_POST['caseno']."' OR `Type`='".$_POST['Type']."' OR `datetime`   BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."' AND `division`='".$_SESSION['cityName']."'");
                              }else{
                              $i=1;
                               $seltimesheet=mysqli_query($connection,"SELECT * from `add_timesheet` where `userid`='".$_SESSION['user_id']."' ORDER BY id DESC");
                             }
                              while($row=mysqli_fetch_array($seltimesheet)){
                                  $casedocuments =mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE  `case_id`='".$row['case_id']."'");
                                 $selcses = mysqli_fetch_array($casedocuments);
                                ?>
                              <tr>
                                 <td><?php echo $i?></td>
                                  <td style="word-break:break-all;" width="200"> <?php echo $selcses['case_type'];?> / <?php echo $selcses['case_no'];?> / <?php echo $selcses['case_no_year'];?> / <?php echo $selcses['case_title'];?></td>
                                  <td><?php echo $row['edate'];?></td>
                                  <td style="word-wrap: break-word;"><?php echo $row['particulars'];?></td>
                                  <td><?php echo $row['timespent'];?> : <?php echo $row['timeinmin'];?></td>
                                  <td><?php echo $row['type'];?></td>
                                  <td>
            		                  		<a href="update-timesheet.php?id=<?php echo $row['id'];?>"><i class="fa fa-edit action-css" style="margin-right: 5px" title="Edit Advocate"></i></a>
            		                  		<a href="delete-timesheet.php?id=<?php echo $row['id'];?>"><i class="fa fa-trash action-css" style="margin-right: 5px" onclick="return confirm('Are you sure you want to delete?');" title="Delete"></i></a>
            		                	</td>
            		            	</tr> 
                            <?php $i++;}?>
            		          	</tbody>
            		        </table>
            		        <tr>
            		           	<td colspan="10">
            		           	</td>
            		        </tr>
            		    </div>
            		</div>

        		</div>
                 
      		</div>
        

    	</div>
        <?php include("footer.php");?>
  	</div>
  	<div class="modal" id="export">
          <form name="myform" method="POST" action="downloadTimeSheet.php">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-left: -185px;width: 180%;">
                <div class="modal-header" style="background-color: #2d4866;color: white;">
                    <h4>Export</h4>
                    <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                      <div class="col-sm-3">
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="`case`" checked="checked" />Case<br>
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="`timespent`" />Time Spent (in hours)<br>
                    </div>
                    <div class="col-sm-3">
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="`datetime`" checked="checked" />Date<br>
                      
                    </div>
                      <div class="col-sm-3">
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="`particulars`" />Particulars<br>
                      </div>
                      <div class="col-sm-3">
                        <input type="checkbox" name='checkboxvar[]' style="margin-right: 5px;" value="`type`" checked="checked"/>Type
                      </div>
                  </div><br><br>
                  <div class="row">
                      <h4>Download as:</h4>
                       <input type="radio" name="downloadas" value="pdf" style="margin-top: 4px;margin-left: 20px;">PDF
                      <input type="radio" name="downloadas" value="excel" style="margin-top: 4px;margin-left: 10px;" checked="checked">Excel
                      <button value="submit" style="margin-top: -9px;border: 0px;margin-left: 35px;padding: 5px;background-color: #2d4866;color: white;">Download</button>
                              
                  </div>
                </div>
            </div>
        </div>
   
  </form>
    </div>
        <div class="modal" id="filter">
          <div class="modal-dialog">
            <div class="modal-content" style="margin-left: -225px;width: 195%;">
              <div class="modal-header" style="background-color: #2d4866;color: white;">
                <h4>Filter</h4>
                <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
              </div>
              <div class="modal-body">
                <form method="POST">
                <div class="row">
                  <div class="col-sm-4">
                    <label>Cases</label><br>
                    <select class="form-control" name="caseno" id="state" required>
                        <option value="">Please Case No</option>
                        <?php $caseno = mysqli_query($connection, "SELECT * FROM `add_timesheet` WHERE `case`!='' AND `division`='".$_SESSION['cityName']."'"); 
                             while($selcases = mysqli_fetch_array($caseno)){
                                 $casedocumentsq =mysqli_query($connection, "SELECT * FROM `reg_cases` WHERE  `case_id`='".$selcases['case']."'");
                                 $selcsesd = mysqli_fetch_array($casedocumentsq);
                              ?>
                        <option value="<?php echo $selcsesd['case'];?>"><?php echo $selcsesd['case_type'];?> / <?php echoselcsesdselcses['case_no'];?> / <?php echo $selcsesd['case_no_year'];?> / <?php echo $selcsesd['case_title'];?></option>
                        <?php } ?>
                        
                      </select>
                    <label>From</label><br>
                    <input type="text" name="from_date" id="datepicker" class="form-control" placeholder="Enter From Date">
                  </div>
                  <div class="col-sm-4">
                    <label>Team Member</label><br>
                    <select name="team_member" class="form-control">
                <option value="">--Select Team Member--</option>
                <?php $team = mysqli_query($connection, "SELECT * FROM `law_registration` WHERE `parent_id`='".$_SESSION['user_id']."'");
                      while($selteam = mysqli_fetch_array($team)){?>
                <option value="<?php echo $selteam['reg_id'];?>"><?php echo $selteam['name'];?> <?php echo $selteam['last_name'];?></option>
                <?php } ?>
              </select>
                    <label>To</label><br>
                    <input type="text" name="to_date" id="datepicker1" class="form-control" placeholder="Enter To Date">
                  </div>
                  <div class="col-sm-4">
                    <label>Type</label><br>
                    <select class="form-control" name="Type">
                      <option value="">All</option>
                      <option value="Effective">Effective</option>
                      <option value="Non Effective / Procedural Appearance">Non Effective / Procedural Appearance</option>
                    </select>
                  </div>
                </div><br><br>
                <div class="row">
                  <input type="reset" name="Reset" style="margin-top: -9px;border: 0px;margin-left: 25px;padding: 5px;background-color: #2d4866;color: white;">
            <input type="submit" style="margin-top: -9px;border: 0px;margin-left: 5px;padding: 5px;background-color: #2d4866;color: white;" value="Submit" name="submitFil">
                </div>
              </form>
              </div>
            </div>
 
          </div>
  
        </div>
      

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
  $( function() {
    $( "#datepicker" ).datepicker({
    dateFormat: 'yy-mm-dd',
          changeYear: true,
          changeMonth: true
           });
     $( "#datepicker1" ).datepicker({
    dateFormat: 'yy-mm-dd',
          changeYear: true,
          changeMonth: true
           });
  } );
  </script>
