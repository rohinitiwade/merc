<?php include 'header.php';
  $ip = $ip_server = $_SERVER['SERVER_ADDR']; ; ?>
<div class="main" id="main">
<div class="hero-section app-hero">
<div class="hero-content app-hero-content text-center">
<div class="row justify-content-md-center">
  <div class="col-md-12">
<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="assets/images/banner1.jpg" alt="Los Angeles">
      <!-- <div class="carousel-caption">
        <h3>Los Angeles</h3>
        <p>We had such a great time in LA!</p>
      </div> -->   
    </div>
    <div class="carousel-item">
      <img src="assets/images/banner1.jpg" alt="Los Angeles">
      <!-- <div class="carousel-caption">
        <h3>Chicago</h3>
        <p>Thank you, Chicago!</p>
      </div> -->   
    </div>
    <div class="carousel-item">
      <img src="assets/images/banner1.jpg" alt="Los Angeles">
      <!-- <div class="carousel-caption">
        <h3>New York</h3>
        <p>We love the Big Apple!</p>
      </div>  -->  
    </div>
  </div>
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
</div>
<!-- <div class="col-md-12">
<div class="hero-image">
<img class="img-fluid" src="assets/images/app_hero_1.png" alt="" />
</div>
</div> -->
</div>
</div>
</div>
<div class="services-section text-center" id="features">

<div class="container">
<div class="row  justify-content-md-center">
<div class="col-md-8">
<div class="services-content">
<h1 class="wow fadeInUp" data-wow-delay="0s">Case Management System</h1>
<p class="wow fadeInUp" data-wow-delay="0.2s">
Highly Secured web Based Application which Helps us Manage Cases,Legal, Client Relations and Teamwork with Legal Audit,  Document Management, Reminders and Calendar on which we can work from anywhere.
</p>
</div>
</div>
<div class="col-md-12 text-center">
<div class="services">
<div class="row">
<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
<div class="services-icon">
<img src="assets/logos/icon1.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Matter Management</h1>
<p>
Adminty is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
</p>
</div>
</div>
<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.3s">
<div class="services-icon">
<img class="icon-2" src="assets/logos/icon2.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Customized Cause List</h1>
<p>
We are contantly working on Adminty and improve its performance too. Your definitely give higher rating to Adminty for its performance.
</p>
</div>
</div>
<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.4s">
<div class="services-icon">
<img class="icon-3" src="assets/logos/icon3.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Hearing Dates Management</h1>
<p>
Adminty is first ever admin dashboard template which release in Bootstrap 4 framework. Intuitive feature rich design concept and color combination.
</p>
</div>
</div>


<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
<div class="services-icon">
<img src="assets/logos/icon1.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>To Do List Management</h1>
<p>
Adminty is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
</p>
</div>
</div>

<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
<div class="services-icon">
<img src="assets/logos/icon1.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Sync with the courts</h1>
<p>
Adminty is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
</p>
</div>
</div>

<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
<div class="services-icon">
<img src="assets/logos/icon1.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Easy Legal Billing / Invoice</h1>
<p>
Adminty is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
</p>
</div>
</div>

<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
<div class="services-icon">
<img src="assets/logos/icon1.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Document Management</h1>
<p>
Adminty is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
</p>
</div>
</div>

<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
<div class="services-icon">
<img src="assets/logos/icon1.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Firm and User Management</h1>
<p>
Adminty is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
</p>
</div>
</div>

<div class="col-sm-4 wow fadeInUp" data-wow-delay="0.2s">
<div class="services-icon">
<img src="assets/logos/icon1.png" height="60" width="60" alt="Service" />
</div>
<div class="services-description">
<h1>Client Management</h1>
<p>
Adminty is one of unique dashboard template which come with tons of ready to use feature. We continuous working on it to provide latest updates in digital market.
</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="client-section" id="services">
<div class="container text-center">
<div class="clients owl-carousel owl-theme">
<div class="single"><img src="assets/images/services/airlogos1.png" alt="" /></div>
<div class="single"><img src="assets/images/services/airlogos2.png" alt="" /></div><div class="single">
<img src="assets/images/services/airlogos3.png" alt="" /></div>
<div class="single"><img src="assets/images/services/airlogos4.png" alt="" /></div>
<div class="single">
<img src="assets/images/services/airlogos5.png" alt="" /></div>
<div class="single">
<img src="assets/images/services/airlogos6.png" alt="" /></div>
</div>
</div>
</div>

<div class="testimonial-section" id="contact-us">
<div class="row text-center col-sm-12">
<div class="col-sm-8">
  <div class="mapouter">
    <div class="gmap_canvas">
      <img src="imagesdata/LMS.png" width="100%">
    </div>
</div>
</div>
<div class="col-sm-4 wrapper">
      <!-- begin login -->
      <div class="login login-v2" data-pageload-addclass="animated fadeIn">
        <div class="login-content">
          <form name="mYform" method="POST">
            <div class="form-group m-b-20">
              <label>First Name:</label>
              <input type="text" class="form-control input-lg" placeholder=" Enter First Name">
            </div>
            <div class="form-group m-b-20">
              <label>Last Name:</label>
              <input type="text" class="form-control input-lg" placeholder="Enter Last Name">
            </div>
            <div class="form-group m-b-20">
              <label>Mobile no:</label>
              <input type="number" class="form-control input-lg" placeholder="Enter Mobile No.">
            </div>
            <div class="form-group m-b-20">
              <label>Message:</label>
              <textarea class="form-control input-lg" placeholder=""></textarea>
            </div>
            <div class="login-buttons">
              <input type="submit" class="btn btn-md btn-success" value="Submit" name="submit">
            </div>
          </form>
        </div>
      </div>
      <!-- end login -->
  </div>
</div>
</div>

<?php include 'footer.php'?>;
