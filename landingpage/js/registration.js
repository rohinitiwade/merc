
  // Initialize select2
  $(document).ready(function(){
    $("#state").select2();
    $("#states").select2();
    $("#city").select2();
    $("#selUser4").select2();
    $("#selUser5").select2();
  // Read selected option
  $('#but_read').click(function(){
  	var username = $('#selUser option:selected').text();
  	var userid = $('#selUser').val();

  	$('#result').html("id : " + userid + ", name : " + username);

  });
});
   $('#state').on('change',function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'country_id='+countryID,
                success:function(html){
                    $('#states').html(html);
                    $('#city').html('<option value="">Select District first</option>'); 
                }
            }); 
        }else{
            $('#states').html('<option value="">Select State first</option>');
            $('#city').html('<option value="">Select District first</option>'); 
        }
    });
    $('#states').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'state_id='+stateID,
                success:function(html){
                $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select District first</option>'); 
        }
    });
$('#division').on('change',function(){
    var division = $(this).val();
    if(division){
      $.ajax({
        type:'POST',
        url:'ajaxData.php',
        data:'division='+division,
        success:function(html){
          $('#division_district').html(html);
          // $('#city').html('<option value="">Select District first</option>'); 
        }
      }); 
    }else{
      // $('#states').html('<option value="">Select State first</option>');
      // $('#city').html('<option value="">Select Divisio first</option>'); 
    }
  });
 $("#designation").on('change',function(){
  var getdeputy = $(this).val();
  if(getdeputy == 'Deputy Secretary'){
    $("#div_deputy").show();
     $("#div_divisional").hide();
     $("#div_ceo").hide();
  }
  else if(getdeputy == 'Divisional Commissioner'){
    $("#div_divisional").show();
     $("#comm_division").show();
    $("#div_deputy").hide();
    $("#div_ceo").hide();
  }
  else if(getdeputy == 'CEO ZP'){
    $("#div_ceo").show();
    $("#comm_division").show();
    $("#div_deputy").hide();
     $("#div_divisional").hide();
  }
  else if(getdeputy == 'Nodal / HOD ZP'){
    $("#div_ceo").show();
    $("#comm_division").show();
    $("#div_deputy").hide();
     $("#div_divisional").hide();
  }
 }); 