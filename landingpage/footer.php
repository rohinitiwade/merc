<div class="footer">
<div class="container">
<div class="col-md-12 text-center">
 <img src="assets/logos/AIR VI Logo JPG.jpg" alt="MHLMS Logo" style="width: 9%;">
<ul class="footer-menu">
<li><a href="http://demo.com/">Site</a></li>
<li><a href="#">Support</a></li>
<li><a href="#">Terms</a></li>
<li><a href="#">Privacy</a></li>
</ul>
<div class="footer-text">
<!-- <p>
Copyright © 2017 Adminty. All Rights Reserved.
</p> -->
</div>
</div>
</div>
</div>

<a id="back-top" class="back-to-top page-scroll" href="#main">
<i class="ion-ios-arrow-thin-up"></i>
</a>
</div></div>


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
<script src="assets/js/jquery-2.1.1.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/menu.js"></script>
<script src="assets/js/custom.js"></script>
<!-- <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js"></script> -->



<script>
  $(document).ready(function(){
    $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
      if (this.hash !== "") {
        event.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 900, function(){
          window.location.hash = hash;
        });
      } // End if
    });
  })
</script>
</body>
</html>