CREATE TABLE m_app_webtoken(
  ID int NOT NULL,
  userid int(20) NOT NULL,
  deviceid varchar(25) NOT NULL,
  webtoken varchar(100) NOT NULL,
  PRIMARY KEY(ID)
);

ALTER TABLE `m_app_webtoken` ADD FOREIGN KEY (`userid`) REFERENCES `law_registration`(`reg_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;



CREATE TABLE `master_priority` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_priority`
--

INSERT INTO `master_priority` (`id`, `name`, `status`) VALUES
(1, 'Super Critical', 1),
(2, 'Critical', 1),
(5, 'Important', 1),
(6, 'Routine', 1),
(7, 'Others', 1),
(8, 'Normal', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_priority`
--
ALTER TABLE `master_priority`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--
