<?php /*session_start();*/
include("header.php");?>
<link rel="stylesheet" href="/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
<link href="/csssheet/contact_us.css" rel="stylesheet">
<div class="main-panel">
  <div class="content-wrapper">
    <?php include("slider.php");?>
    <div class="card">
      <div class="card-body">
        <h2 class="card-header header">Contact us</h2>
        <div class="row contact-us" style="margin-top: 25px;">
          <body class="wrapper_login">
<!--   <?php
    echo $ipAddress = $_SERVER['REMOTE_ADDR'];
    $ip_key = "AIzaSyBn7K8xI_883w9qPaISQf1_JzNii3Ke_I8";

    echo $query = "http://api.ipinfodb.com/v3/ip-city/?key=" . $ip_key . "&ip=" . $ipAddress . "&format=json";
    $json = file_get_contents($query);
    $data = json_decode($json, true);

    if ($data['statusCode'] == "OK") {
        echo '<pre>';
        echo "IP Address: " . $ipAddress;
        echo "Country: " . $data['countryName'];
        echo "Region: " . $data['regionName'];
        echo "City: " . $data['cityName'];
        echo "Latitude: " . $data['latitude'];
        echo "Longitude: " . $data['longitude'];
        echo '</pre>';
    } else {
        echo $data['statusCode']." ".$data['statusMessage'];
    }
    ?> -->
    <div style="width: 100%"><iframe width="100%"  id="map" height="600" src="https://maps.google.com/maps?width=100%&height=600&hl=en&coord=21.1458° N,&q=(All%20India%20Reporter%20Pvt.Ltd.%20Nagpur)&ie=UTF8&t=&z=14&iwloc=B&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><br />
    <div class="wrapper">
      <!-- begin login -->
      <div class="login login-v2" data-pageload-addclass="animated fadeIn">
        <div class="login-content">
          <form name="mYform" method="POST">
            <div class="form-group m-b-20">
              <label>First Name:</label>
              <input type="text" class="form-control input-lg" placeholder=" Enter First Name">
            </div>
            <div class="form-group m-b-20">
              <label>Last Name:</label>
              <input type="text" class="form-control input-lg" placeholder="Enter Last Name">
            </div>
            <div class="form-group m-b-20">
              <label>Mobile no:</label>
              <input type="number" class="form-control input-lg" placeholder="Enter Mobile No.">
            </div>
            <div class="form-group m-b-20">
              <label>Message:</label>
              <textarea class="form-control input-lg" placeholder=""></textarea>
            </div>
            <div class="login-buttons">
              <input type="submit" class="btn btn-md btn-success" value="Submit" name="submit">
            </div>
          </form>
        </div>
      </div>
      <!-- end login -->
    </div>
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="assets/plugins/jquery/jquery-1.9.1.min.js"></script>
    <script src="assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    <script src="assets/js/login-v2.demo.min.js"></script>
    <script src="assets/js/apps.min.js"></script>
    <!-- ================== END PAGE LEVEL JS ================== -->
    <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 1,
          center: {lat: 35.717, lng: 139.731}
        });
      }
    </script>
    <style>
      #outer_bor h2 {
        margin: 0 auto;
        padding: 0px 0px 0px 6px;
        font: bolder 14px/32px Cambria, serif;
        color: #006064;
        background-color: #ebebeb;
        text-align: left;
      }
      .crm_support_id {
        margin: 0 auto;
        padding: 0px 0px 0px 6px;
        font: bolder 14px/32px Cambria, serif;
        color: #006064;
        background-color: #ebebeb;
        text-align: left;
      }
      .profile_sec td
      {
        padding: 8px;
      }
      .show {
        display:block;
      }
      #outer_bor {
        width: 100%;
        margin: 0 auto;
        padding: -1px;
        border: 1px solid #d5d5d5;
        background-color: #FFFFFF;
      }
      .profile_sec {
        margin: 0 auto;
        padding: 0px;
        border: 1px solid #e6e6e6;
        background-color: #f7f7f7;
        border: 1px solid #d7d7d7;
        text-align: center;
      }
      .header{
        padding-top: 0px
      }
      .submit_login{
      background-color: #2d4866;
     border-color: #2d4866;
      }
    </style>
  </body>
</div>
</div>
</div>
</div> 
<?php include("footer.php");?>