 <?php include("header.php");?>
 <link rel="stylesheet" href="csssheet/registration.css">
 <script src="https://kit.fontawesome.com/a076d05399.js"></script>
 
 <section id="about-us">
 	<div class="container">
 		<div class="registration" style=""> 
 			<div class="ribbon-wrapper h2 ribbon-red">
 				<div class="ribbon-front">
 					<h2>Registration</h2>
 				</div>
 				<div class="ribbon-edge-topleft2"></div>
 				<div class="ribbon-edge-bottomleft"></div>
 			</div>
 			<form accept-charset="UTF-8" role="form" autocomplete="OFF" method="POST">
 				<span class="msg"><b><?php echo $msg;?></b></span>
 				<div class="">
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon glyphicon glyphicon-user"></i>
 							<input type="text" id="name" class="form-control" placeholder="Name" autocomplete="OFF" name="name" value="<?php echo $_POST['name'];?>" required>
 							<!-- <label class="form-control-placeholder" for="name">Name</label> -->
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon glyphicon glyphicon-user"></i>
 							<input type="text" id="last_name" class="form-control"  placeholder="Last Name" name="last_name"  value="<?php echo $_POST['last_name'];?>"  required>
 							<!--  <label class="form-control-placeholder" for="last_name">Last Name</label> -->
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon glyphicon glyphicon-earphone"></i>
 							<input type="text" id="mobile" class="form-control" placeholder="Mobile Number" name="mobile"  value="<?php echo $_POST['mobile'];?>"  required>
 							<!--  <label class="form-control-placeholder" for="mobile">Mobile Number</label> -->
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon glyphicon glyphicon-envelope"></i>
 							<input type="text" id="email" class="form-control" placeholder="Email Address" name="email" value="<?php echo $_POST['email'];?>" required>
 							<!--  <label class="form-control-placeholder" for="email">Enter Email Address</label> -->
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon glyphicon glyphicon-lock"></i>
 							<input type="password" id="password" class="form-control" placeholder="Password" name="password" value="<?php echo $_POST['password'];?>" required>
 							<!-- <label class="form-control-placeholder" for="password">Password</label> -->
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon glyphicon glyphicon-lock"></i>
 							<input type="password" id="confirm_passwords" class="form-control" placeholder="Confirm Password" name="confirm_password" value="<?php echo $_POST['confirm_password'];?>"   required>
 							<!-- <label class="form-control-placeholder" for="confirm_password">Confirm Password</label> -->
 							<span id='message'></span>
 						</div>
 					</div>

 					<div class="col-md-6 mx-auto">
 						<div class="form-group">
 							<select  name="state" id="state" class="form-control" style="margin: 0px;">
 								<option  value="<?php echo $_POST['state'];?>">-- Select State--</option>
 								<?php $satte = mysqli_query($connection,"SELECT * FROM `geo_locations` WHERE `location_type` ='STATE'  ORDER BY `name` ASC");
 								while($selsatte = mysqli_fetch_array($satte)){?>
 									<option value="<?php echo $selsatte['id'];?>"><?php echo $selsatte['name'];?></option>
 								<?php } ?>
 							</select>
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group">
 							<select id="states" name="district" class="form-control" required="">
 								<option value="<?php echo $_POST['states'];?>">Select State first</option>
 							</select>
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group">
 							<select id="city" name="city" class="form-control">
 								<option value="<?php echo $_POST['city'];?>">Select District First</option>
 							</select>
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i class="icon fas fa-venus-mars"></i>
 							<select id="gender" class="form-control dropdwn" name="gender" required="">
 								<option value="<?php echo $_POST['gender'];?>"><i href="#" class="icon glyphicon glyphicon-user"></i>Select Gender</option>
 								<option value="Male">Male</option>
 								<option value="Female">Female</option>
 								<option value="other">Other</option>
 							</select>
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i class="icon fas fa-venus-mars"></i>
 							<select id="division" class="form-control dropdwn" name="division" required="">
 								<option value="<?php echo $_POST['division'];?>"><i href="#" class="icon glyphicon glyphicon-user"></i>Select Division</option>
 								<?php $sel_division = mysqli_query($connection,"SELECT *  FROM `division`  ORDER BY `division` ASC");
 								while($sels_division = mysqli_fetch_array($sel_division)){?>
 									<option value="<?php echo $sels_division['division'];?>"><?php echo $sels_division['division'];?></option>

 								<?php } ?>
 							</select>
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i class="icon fas fa-venus-mars"></i>
 							<select id="department" class="form-control dropdwn" name="department" required="">
 								<option value="<?php echo $_POST['department'];?>"><i href="#" class="icon glyphicon glyphicon-user"></i>Select Department</option>
 								<option value="Establishment">Establishment</option>
 								<option value="Development">Development</option>
 							</select>
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon far fa-id-badge"></i>
 							<input type="text" id="designation" class="form-control" placeholder="Designation" name="designation" value="<?php echo $_POST['designation'];?>"  required>
 							<!--  <label class="form-control-placeholder" for="password">Designation</label> -->
 						</div>
 					</div>
 					<div class="col-md-6 mx-auto">
 						<div class="form-group"><i href="#" class="icon glyphicon glyphicon-map-marker"></i>
 							<input type="text" id="zip" class="form-control" placeholder="Zip/Postal Code" name="zip" value="<?php echo $_POST['zip'];?>"  required>
 							<!--   <label class="form-control-placeholder" for="confirm_password">Zip/Postal Code</label> -->
 							<span id='message'></span>
 						</div>
 					</div>
 					<div class="col-md-12 mx-auto">
 						<div class="form-group"><i class="icon fas fa-users"></i>
 							<!--  <input type="text" id="confirm_passwords" class="form-control" placeholder="Zip/Postal Code" name="zip" value="<?php echo $_POST['zip'];?>"  required> -->
 							<select class="form-control dropdwn" name="account" id="account">
 								<option value="<?php echo $_POST['account'];?>">--Select Option --</option>
 								<option value="Individual">Individual</option>
 								<option value="Organisation">Organisation</option>
 							</select>
 						</div>
 					</div>
 					<div class="col-md-12 mx-auto">
 						<div class="form-group inputwithlogo"><i href="#" class="icon glyphicon glyphicon-home"></i>
 							<textarea name="address" id="address" class="form-control" autocomplete="OFF" placeholder="Address"><?php echo $_POST['address'];?></textarea>
 						</div>
 					</div>

 					<div class="col-md-12 mx-auto">
 						<div class="form-group">
 							<div class="row">
 								<input type="checkbox" value=""  required="">
 								<b>By signing up, you agree to the Terms of Service and Privacy Policy</b></a>
 							</div>

 							<h5 style="color: red;"><a href="login.php" style="color: red;">Already have an account? <span class="log-in">Log In</span></a></h5>

 						</div>
 					</div>

 				</div>
 				<span class="msg-error error"></span>
 				<div class="submit">
 					<input type="submit" class="btn btn-primary btn-lg" id="submit" name="submittrial" value="Sign Up" onclick = "return Validate()">
 				</div>

 			</form>
 		</div>
 	</div>
 </section>


 <?php include("footer.php");?>

 <script type="text/javascript">
 	$(document).ready(function(){
 		$('#state').on('change',function(){
 			var countryID = $(this).val();
 			if(countryID){
 				$.ajax({
 					type:'POST',
 					url:'ajaxData.php',
 					data:'country_id='+countryID,
 					success:function(html){
 						$('#states').html(html);
 						$('#city').html('<option value="">Select District first</option>'); 
 					}
 				}); 
 			}else{
 				$('#states').html('<option value="">Select State first</option>');
 				$('#city').html('<option value="">Select District first</option>'); 
 			}
 		});
 		$('#states').on('change',function(){
 			var stateID = $(this).val();
 			if(stateID){
 				$.ajax({
 					type:'POST',
 					url:'ajaxData.php',
 					data:'state_id='+stateID,
 					success:function(html){
 						$('#city').html(html);
 					}
 				}); 
 			}else{
 				$('#city').html('<option value="">Select District first</option>'); 
 			}
 		});
 	});
 </script>

 <script src='select2/dist/js/select2.min.js' type='text/javascript'></script>
 <link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
 <script>
 	$(document).ready(function(){

  // Initialize select2
  $("#state").select2();
  $("#states").select2();
  $("#city").select2();
  $("#selUser4").select2();
  $("#selUser5").select2();
  // Read selected option
  $('#but_read').click(function(){
  	var username = $('#selUser option:selected').text();
  	var userid = $('#selUser').val();

  	$('#result').html("id : " + userid + ", name : " + username);

  });
});
</script>
<style type="text/css">
form-control::placeholder {
	color: black !important;
	opacity: 1;
}
</style>
<script>
  $(document).ready(function(){
$("#submit").click(function(){
var name = $("#name").val();
var last_name = $("#last_name").val();
var mobile = $("#mobile").val();
var email = $("#email").val();
var password = $("#password").val();
var confirm_passwords = $("#confirm_passwords").val();
var state = $("#state").val();
var district = $("#states").val();
var city = $("#city").val();
var gender = $("#gender").val();
var department = $("#department").val();
var division = $("#division").val();
var address = $("#address").val();
var designation = $("#designation").val();
var account = $("#account").val();
var zip = $("#zip").val();
// Returns successful data submission message when the entered information is stored in database.
var dataString = 'name1='+ name + '&last_name1='+ last_name + '&mobile1='+ mobile + '&email1='+ email+ '&password1='+ password+ '&confirm_passwords1='+ confirm_passwords + '&state1='+ state + '&district1='+ district + '&city1='+ city+ '&gender1='+ gender+ '&department1='+ department+ '&division1='+ division+ '&address1='+ address+'&designation1='+ designation+'&account1='+ account+'&zip1='+ zip;
alert(dataString);
if(name==''||last_name==''||mobile==''||email=='')
{
alert("Please Fill All Fields");
}
else 
{
// AJAX Code To Submit Form.
$.ajax({
type: "POST",
url: "ajaxsubmit.php",
data: dataString,
cache: false,
success: function(result){
}
});
}
return false;
});
});
  </script>