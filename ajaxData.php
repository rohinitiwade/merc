<?php
//Include the database configuration file
include 'includes/connection.php';

if(!empty($_POST["country_id"])){
    //Fetch all state data
      $query = mysqli_query($connection, "SELECT `name`,`id` FROM `geo_locations` WHERE parent_id = ".$_POST['country_id']." AND `location_type`='DISTRICT'  ORDER BY name ASC");
    //Count total number of rows
    $rowCount = mysqli_num_rows($query);
    
    //State option list
    if($rowCount > 0){
        echo '<option value="">Select District</option>';
        while($row = mysqli_fetch_assoc($query)){ 
            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
    }else{
        echo '<option value="">State not available</option>';
    }
}elseif(!empty($_POST["division"])){
    //Fetch all state data
      $query = mysqli_query($connection, "SELECT `division_name` FROM `under_division` WHERE division_id = '".$_POST['division']."' ORDER BY division_name ASC");
    //Count total number of rows
    $rowCount = mysqli_num_rows($query);
    
    //State option list
    if($rowCount > 0){
        echo '<option value="">Select Under District</option>';
        while($row = mysqli_fetch_assoc($query)){ 
            echo '<option value="'.$row['division_name'].'">'.$row['division_name'].'</option>';
        }
    }else{
        echo '<option value="">Under District not available</option>';
    }
}elseif(!empty($_POST["state_id"])){
    //Fetch all city data
    $query = mysqli_query($connection, "SELECT `name`,`id` FROM geo_locations WHERE parent_id = ".$_POST['state_id']." ORDER BY name ASC");
    $rowCount = mysqli_num_rows($query);
    //Count total number of rows
    //City option list
    if($rowCount > 0){
        echo '<option value="">Select Village</option>';
        while($row = $query->fetch_assoc()){ 
            echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
        }
    }else{
        echo '<option value="">City not available</option>';
    }
}
?>