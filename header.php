<?php session_start();include("includes/connection.php");
if($_SESSION['user_id']!=""){
	//session_destroy();?>
	<script>
		window.location.href='http://15.206.144.246/mhlms/admin/';
	</script>
<?php } ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Theme Made By www.w3schools.com -->
	<title>MHLMS - Landing Page</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="csssheet/bootstrap.min.css" defer>
	<link rel="icon" href="images/favicon(2).ico" type="image/x-icon">
	<script src="js/jquery.min.js" defer></script>
	<script src="js/bootstrap.min.js" defer></script>
	<link href="csssheet/animations.css" rel="stylesheet" defer>
	<link href="csssheet/font-awesome.min.css" rel="stylesheet" defer>
	<link rel="stylesheet" type="text/css" href="csssheet/index_style.css" defer>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

	<nav class="navbar navbar-default navbar-fixed-top">

		<div class="topnav"  id='accessibility'  role='navigation' aria-label='Primary' style="">
			<div class="col-sm-12 topcontent" > 
				<ul>
					<li class="email-add" style=""><i class="fa fa-phone" style=""></i><span>0712-2234413</span></li>
					<li class="email-add" style=""><i class="email_logo fa fa-envelope" style=""></i><span>info@maharashtra-lms.in</span></li>
				</ul>
			</div>
		</div>

		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>                        
				</button>
				<a class="navbar-brand" href="#myPage"><img src="images/Maharashtra Litigation Management System1.png" style=""></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav navbar-right">
					<li class="home"><a href="index">Home</a></li>
					<li class="abt-us"><a href="about-us" title="About Us">About Us</a></li>
					<li class="pricing"><a href="registration" title="registration">Registration</a></li>
					<li class="pricing"><a href="pricing" title="Pricing">Pricing</a></li>
					<li><a href="#" title="Our Servicses">Our Servicses</a></li>
					<li><a href="#">Testimonial</a></li>
					<li><a href="login" title="District Courts">Login</a></li>
					<li><a href="contact_us.php">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<script src="js/jquery.min.js"></script>
	<script  type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#state').on('change',function(){
				var countryID = $(this).val();
				if(countryID){
					$.ajax({
						type:'POST',
						url:'ajaxData.php',
						data:'country_id='+countryID,
						success:function(html){
							$('#states').html(html);
							$('#city').html('<option value="">Select District first</option>'); 
						}
					}); 
				}else{
					$('#states').html('<option value="">Select State first</option>');
					$('#city').html('<option value="">Select District first</option>'); 
				}
			});

			$('#states').on('change',function(){
				var stateID = $(this).val();
				if(stateID){
					$.ajax({
						type:'POST',
						url:'ajaxData.php',
						data:'state_id='+stateID,
						success:function(html){
							$('#city').html(html);
						}
					}); 
				}else{
					$('#city').html('<option value="">Select District first</option>'); 
				}
			});
		});
	</script>
