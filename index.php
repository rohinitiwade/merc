  <?php include ("header.php");?>

    <div class="slider_div text-center">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">
            <img src="images/bannerde.jpg" alt="Los Angeles" style="width:100%;">
          </div>
          <div class="item">
            <img src="images/banner1.jpg" alt="Los Angeles" style="width:100%;">
          </div>
            <div class="item">
            <img src="images/banner1.jpg" alt="Los Angeles" style="width:100%;">
          </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <!-- Container (About Section) -->
    <div id="about" class="container-fluid">
     <div id="main-body-index">
      <div class="container">
        <div class="col-sm-12 row">
          <div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
            <div class="flip-card-inner">
              <div class="flip-card-front container">
               <h4>Matter Management</h4>
               <p class=" slideUp" >Comming Soon</p>
               <!-- <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Matter">View More</a></p> -->
             </div>
             <div class="flip-card-back">
              <h3>Matter Management</h3>
              <i class="fa fa-file"></i>
            </div>
          </div>
        </div>
        <div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
          <div class="flip-card-inner">
            <div class="flip-card-front container">
             <h4>Customized Cause List</h4>
             <p class=" slideUp" >Comming Soon</p>
                <!-- <p>
                  <a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Customized">View More</a>
                </p> -->
              </div>
              <div class="flip-card-back">
               <h3>Customized Cause List</h3> 
               <i class="fa fa-file-text"></i>
             </div>
           </div>
         </div>
         <div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
          <div class="flip-card-inner">
            <div class="flip-card-front container">
             <h4>Hearing Dates Management</h4>
             <p class=" slideUp" >Comming Soon</p>
             <!-- <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Hearing">View More</a></p> -->
           </div>
           <div class="flip-card-back">
             <h3>Hearing Dates Management</h3> 
             <i class="fa fa-calendar-o"></i>
           </div>
         </div>
       </div>
       <div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
        <div class="flip-card-inner">
          <div class="flip-card-front container">
           <h4>To-Dos Management</h4>
           <p class=" slideUp" >Comming Soon </p>
           <!--  <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#To-Dos">View More</a></p></p> -->
         </div>
         <div class="flip-card-back">
           <h3>To-Dos Management</h3> 
           <i class="fa fa-calendar-check-o"></i>
         </div>
       </div>
     </div>
     <div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
      <div class="flip-card-inner">
        <div class="flip-card-front container">
         <h4>Sync with the courts</h4>
         <p class=" slideUp" >Comming Soon</p>
         <!-- <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Sync">View More</a></p> -->
       </div>
       <div class="flip-card-back">
        <h3>Sync with the courts</h3> 
        <i class="fa fa-institution"></i>
      </div>
    </div>
  </div>
  <div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
    <div class="flip-card-inner">
      <div class="flip-card-front container">
        <h4>Easy Legal Billing / Invoice</h4>
        <p class=" slideUp" >Comming Soon</p>
        <!-- <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Easy">View More</a></p> -->
      </div>
      <div class="flip-card-back">
       <h3>Easy Legal Billing / Invoice</h3> 
       <i class="fa fa-gavel"></i>
     </div>
   </div>
 </div>
 <div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
  <div class="flip-card-inner">
    <div class="flip-card-front container">
     <h4>Documents Management</h4>
     <p class=" slideUp" >Comming Soon</p>
     <!-- <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Documents">View More</a></p> -->
   </div>
   <div class="flip-card-back">
     <h3>Documents Management</h3> 
     <i class="fa fa-folder-open"></i>
   </div>
 </div>
</div>
<div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
  <div class="flip-card-inner">
    <div class="flip-card-front container">
     <h4>Firm and User Management</h4>
     <p class=" slideUp" >Comming Soon</p>
     <!-- <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Firm">View More</a></p> -->
   </div>
   <div class="flip-card-back">
     <h3>Firm and User Management</h3> 
     <i class="fa fa-group"></i>
   </div>
 </div>
</div>
<div class="flip-card col-md-4 col-sm-6 col-xs-12 form-group">
  <div class="flip-card-inner">
    <div class="flip-card-front container">
     <h4>Clients Management</h4>
     <p class=" slideUp" >Comming Soon</p>
     <!-- <p><a href="#"  style="float: right;"  class="btn vm" data-toggle="modal" data-target="#Clients">View More</a></p> -->
   </div>
   <div class="flip-card-back">
    <h3>Clients Management</h3> 
    <i class="fa fa-male"></i>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
<p id="back-top" class="pulll-right" style="">
  <a href="#myPage"><span class="top-icon"><i class="fa fa-angle-double-up" aria-hidden="true"></i></span><em class="back">Back to top</em></a>
</p>

<!-- Image of location/map -->
<?php include("footer.php");?>

<script>
  $(document).ready(function(){
   /* var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
      showSlides(slideIndex += n);
    }

    function currentSlide(n) {
      showSlides(slideIndex = n);
    }

    function showSlides(n) {
      var i;
      var slides = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("dot");
      if (n > slides.length) {slideIndex = 1}    
        if (n < 1) {slideIndex = slides.length}
          for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";  
          }
          for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
          }
          slides[slideIndex-1].style.display = "block";  
          dots[slideIndex-1].className += " active";
          setTimeout(showSlides, 2000);
        }*/
    // Add smooth scrolling to all links in navbar + footer link
    $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {
        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 900, function(){

          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      } // End if
    });

    $(window).scroll(function() {
      $(".slideanim").each(function(){
        var pos = $(this).offset().top;

        var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
      });
    });
  })
</script>

</body>
</html>
