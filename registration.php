<?php include("header.php");?>
 <link rel="stylesheet" href="csssheet/registration.css">
 <section id="about-us">
     <div class="container">
         <div class="registration" style=""> 
             <div class="ribbon-wrapper h2 ribbon-red">
                 <div class="ribbon-front">
                     <h2>Registration</h2>
                 </div>
                 <div class="ribbon-edge-topleft2"></div>
                 <div class="ribbon-edge-bottomleft"></div>
             </div>
             <form accept-charset="UTF-8" role="form" autocomplete="OFF" action="submit.php" method="POST" onsubmit="return Validate()">
                 <span class="msg"><b><?php echo $msg; ?></b></span>
                 <div class="">
                     <div class="col-md-6 mx-auto">
                         <div class="form-group"><i href="#" class="icon glyphicon glyphicon-user"></i>
                             <input type="text" id="name" class="form-control" placeholder="Name" autocomplete="OFF" name="name" value="<?php echo $_POST['name'];?>" required>
                         </div>
                     </div>
                     <div class="col-md-6 mx-auto">
                         <div class="form-group"><i href="#" class="icon glyphicon glyphicon-user"></i>
                             <input type="text" id="last_name" class="form-control"  placeholder="Last Name" name="last_name"  value="<?php echo $_POST['last_name'];?>"  required>
                         </div>
                     </div>
                     <div class="col-md-6 mx-auto">
                         <div class="form-group"><i href="#" class="icon glyphicon glyphicon-earphone"></i>
                             <input type="text" id="mobile" class="form-control" placeholder="Mobile Number" name="mobile"  value="<?php echo $_POST['mobile'];?>"  required>
                         </div>
                     </div>
                     <div class="col-md-6 mx-auto">
                         <div class="form-group"><i href="#" class="icon glyphicon glyphicon-envelope"></i>
                             <input type="text" id="email" class="form-control" placeholder="Email Address" name="email" value="<?php echo $_POST['email'];?>" required>
                         </div>
                     </div>
                     <div class="col-md-6 mx-auto">
                         <div class="form-group"><i href="#" class="icon glyphicon glyphicon-lock"></i>
                             <input type="password" id="password1" class="form-control" placeholder="Password" name="password" value="<?php echo $_POST['password'];?>" required>
                         </div>
                     </div>
                     <div class="col-md-6 mx-auto">
                         <div class="form-group"><i href="#" class="icon glyphicon glyphicon-lock"></i>
                             <input type="password" id="confirm_passwords" class="form-control" placeholder="Confirm Password" name="confirm_password" value="<?php echo $_POST['confirm_password'];?>"   required>
                             <span id='message'></span>
                         </div>
                     </div>
                     <div class="col-md-6 mx-auto">
                         <div class="form-group">
                             <select  name="state" id="state" class="form-control dropdwn" style="margin: 0px;">
                                 <option  value="<?php echo $_POST['state']; ?>">-- Select State--</option>
                                 <?php
                              $satte = mysqli_query($connection, "SELECT * FROM `geo_locations` WHERE `location_type` ='STATE' AND `id`=21  ORDER BY `name` ASC");
                                while ($selsatte = mysqli_fetch_array($satte)) {
                                ?>
                                  <option value="<?php echo $selsatte['id']; ?>"><?php echo $selsatte['name'];?></option>
                              <?php } ?>
                            </select>
                         </div>
                     </div>
          <div class="col-md-6 mx-auto">
            <div class="form-group"><i class="icon fas fa-venus-mars"></i>
              <select id="designation" class="form-control dropdwn" name="designation" required="">
                <option value="<?php
                echo $_POST['gender'];
                ?>"><i href="#" class="icon glyphicon glyphicon-user"></i>Select Designation</option>
                                 <?php
                $sel_desg = mysqli_query($connection, "SELECT `designation`  FROM `designation_master` WHERE `status`=1 ORDER BY `designation` ASC");
                while ($sels_desg = mysqli_fetch_array($sel_desg)) {
                ?>
                                 <option value="<?php echo $sels_desg['designation']; ?>"><?php echo $sels_desg['designation'];?></option>

                <?php }?>
             </select>
            </div>
          </div>
          <div class="col-md-6 mx-auto" id="div_deputy" style="display: none;">
            <div class="form-group"><i class="icon fas fa-venus-mars"></i>
              <select id="department" class="form-control dropdwn" name="department" >
                <option value=""><i href="#" class="icon glyphicon glyphicon-user"></i>Select department</option>
                <?php
                $sel_division = mysqli_query($connection, "SELECT `department_name`  FROM `department` ORDER BY `department_name` ASC");
                while ($sels_division = mysqli_fetch_array($sel_division)) {?>
                    <option value="<?phpecho $sels_division['department_name'];?>"><?php echo $sels_division['department_name'];?></option>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="col-md-6 mx-auto" id="comm_division" style="display: none;">
            <div class="form-group"><i class="icon fas fa-venus-mars"></i>
              <select id="division" class="form-control dropdwn" name="division" >
                <option value="<?php
                echo $_POST['gender'];
                ?>"><i href="#" class="icon glyphicon glyphicon-user"></i>Select Division</option>
                <?php $sel_division = mysqli_query($connection, "SELECT *  FROM `division`  ORDER BY `division` ASC");
                while ($sels_division = mysqli_fetch_array($sel_division)) {
                ?>
                  <option value="<?php echo $sels_division['division'];?>"><?php echo $sels_division['division'];?></option>
                <?php } ?>
             </select>
            </div>
          </div>
          <div class="col-md-6 mx-auto" id="div_divisional" style="display: none;">
           <div class="form-group"><i class="icon fas fa-venus-mars"></i>
            <select id="divisional" class="form-control dropdwn" name="divisional">
              <option value="<?php echo $_POST['gender'];?>"><i href="#" class="icon glyphicon glyphicon-user"></i>Select Department</option>
              <option value="Establishment">Establishment</option>
              <option value="Development">Development</option>
            </select>
          </div>
        </div>
        <div class="col-md-6 mx-auto" id="div_ceo" style="display: none;">
          <div class="form-group">
             <select id="division_district" name="division_district" class="form-control dropdwn">
              <option value="<?php echo $_POST['states'];?>">Select Division first</option>
            </select>
          </div>
        </div>
       <div class="col-md-6 mx-auto">
         <div class="form-group"><i class="icon fas fa-venus-mars"></i>
              <select id="gender" class="form-control dropdwn" name="gender" required="">
                <option value="<?php echo $_POST['gender'];?>"><i href="#" class="icon glyphicon glyphicon-user"></i>Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="other">Other</option>
              </select>
            </div>
          </div>
        <div class="col-md-6 mx-auto">
         <div class="form-group"><i href="#" class="icon glyphicon glyphicon-map-marker"></i>
          <input type="text" id="emp_id" class="form-control" placeholder="Emp Id" name="emp_id" value="<?php echo $_POST['emp_id'];?>"  required>
          <span id='message'></span>
        </div>
      </div>
      <div class="col-md-6 mx-auto">
        <div class="form-group"><i href="#" class="icon glyphicon glyphicon-map-marker"></i>
          <input type="text" id="confirm_passwords" class="form-control" placeholder="Zip/Postal Code" name="zip" value="<?php echo $_POST['zip'];?>"  required>
          <span id='message'></span>
        </div>
      </div>
      <div class="col-md-6 mx-auto">
       <div class="form-group"><i class="icon fas fa-users"></i>
        <select class="form-control dropdwn" name="account">
         <option value="">--Select Option --</option>
         <option value="Individual">Individual</option>
         <option value="Organisation">Organisation</option>
       </select>
     </div>
   </div>
   <div class="col-md-12 mx-auto">
     <div class="form-group inputwithlogo"><i href="#" class="icon glyphicon glyphicon-home"></i>
      <textarea name="address" class="form-control" autocomplete="OFF" required="" placeholder="Address"><?php echo $_POST['gender'];?></textarea>
    </div>
  </div>
  <div class="col-md-12 mx-auto">
   <div class="form-group">
    <div class="row">
     <input type="checkbox" value=""  required="">
     <b>By signing up, you agree to the Terms of Service and Privacy Policy</b></a>
   </div>
   <h5 style="color: red;"><a href="login.php" style="color: red;">Already have an account? <span class="log-in">Log In</span></a></h5>
 </div>
</div>
</div>
<span class="msg-error error"></span>
<div class="submit">
  <input type="submit" class="btn btn-primary btn-lg" id="submitbtn" name="submittrial" value="Sign Up">
</div>
</form>
</div>
</div>
</section>
<?php
include("footer.php");
?>
<script src='select2/dist/js/select2.min.js' type='text/javascript'></script>
<script src='js/registration.js' type='text/javascript'></script>
<link href='select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<style type="text/css">
  form-control::placeholder {
   color: black !important;
   opacity: 1;
 }
</style>