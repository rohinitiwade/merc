<?php session_start(); error_reporting(0);

date_default_timezone_set('Asia/Calcutta'); 
$dates =  date("Y-m-d"); // time in India
$time =  date("H:i:s"); // time in India
$date_time =  date("Y-m-d H:i:s"); // time in India

function db_connect() {

        // Define connection as a static variable, to avoid connecting more than once 
    static $connection;

        // Try and connect to the database, if a connection has not been established yet
    if(!isset($connection)) {
             // Load configuration as an array. Use the actual location of your configuration file
        //$config = parse_ini_file('config.ini'); 
        $connection = mysqli_connect('localhost','root','root','case_tracker');
    }

        // If connection was not successful, handle the error
    if($connection === false) {
            // Handle error - notify administrator, log to a file, show an error screen, etc.
        return mysqli_connect_error(); 
    }
    return $connection;
}

// Connect to the database
$connection = db_connect();

// Check connection
if ($connection->connect_error) {
    die("Connection failed: " . $connection->connect_error);
}else{
    echo "Connect";
}
?> 
